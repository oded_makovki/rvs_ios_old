//
//  RVSAppMomentListCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentListCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppMomentListCell() <RVSAppClipPlayerViewDelegate>
@end

@implementation RVSAppMomentListCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppMomentListCell];
    }
    
    return self;
}

-(void)initRVSAppMomentListCell
{
    [self loadContentsFromNib];
    
//    self.momentView.layer.cornerRadius = 12;
//    self.momentView.layer.masksToBounds = YES;
    
    self.momentView.shouldParseHotwords = NO;
    self.momentView.isUsingLongTimeFormat = YES;
    self.momentView.topPostView.clipPlayerView.delegate = self;
    
    self.momentView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.momentView.topPostView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.momentView.topPostView.programNameLabel.textColor = [UIColor rvsProgramTextColor];    
    self.momentView.topPostView.textLabel.preferredMaxLayoutWidth = 215;
    
    self.momentView.likeInfoLabel.textColor = [UIColor rvsTintColor];
    self.momentView.commentInfoLabel.textColor = [UIColor rvsTintColor];
    self.momentView.commentInfoLabel.preferredMaxLayoutWidth = 240;
    
    self.momentView.topPostsLabel.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"top post title", nil)];
}

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSAppClipPlayerViewState)newState
{
//    NSLog(@"RVSAppClipPlayerViewState: %d", (int)newState);
}

@end
