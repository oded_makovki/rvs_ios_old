//
//  RVSAppTranscriptView.h
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "UIView+NibLoading.h"
#import "RVSAppAcceleratorView.h"

@interface RVSAppTranscriptAcceleratorView : RVSAppAcceleratorView
@property (nonatomic) NSTimeInterval timeSyncOffset;


-(void)setSelectionStart:(NSDate *)selectionStart selectionEnd:(NSDate *)selectionEnd animated:(BOOL)animated;
-(BOOL)scrollTo:(NSDate *)date animated:(BOOL)animated;
-(void)scrollToSelection;
@end
