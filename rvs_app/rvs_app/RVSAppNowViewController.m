//
//  RVSAppNowViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppNowViewController.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>
#import "RVSApplication.h"

@interface RVSAppNowViewController ()
@property (nonatomic) RVSNotificationObserver *currentChannelObserver;
@end

@implementation RVSAppNowViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"now title", nil);
    
    __weak RVSAppNowViewController *weakSelf = self;
    
    self.currentChannelObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationCurrentChannelDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf currentChannelDidChange:notification];
    }];
    
    if ([RVSApplication application].currentChannel)
    {
        self.momentListFactory  = [[RVSAppMomentListFactory alloc] initWithChannel:[RVSApplication application].currentChannel];    
        [self reloadMoments];
    }
}

- (void)currentChannelDidChange:(NSNotification *)notification
{
    NSLog(@"Current channel changed...");
    
    self.momentListFactory  = [[RVSAppMomentListFactory alloc] initWithChannel:[RVSApplication application].currentChannel];
    
    [self.dataListView clear];
    [self reloadMoments];
}

@end
