//
//  TTTAttributedLabel+RVSApplication.h
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TTTAttributedLabel.h"

@interface TTTAttributedLabel (RVSApplication)

-(void)setDefaultLinkStyleAttributes;

@end
