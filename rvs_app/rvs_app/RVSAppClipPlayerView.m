//
//  RVSAppClipPlayerView.m
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppClipPlayerView.h"
#import "RVSAppClipPlayerView_Private.h"
#import <UIFont+FontAwesome.h>
#import <NSString+FontAwesome.h>
#import "RVSApplication.h"

@interface RVSAppClipPlayerView() <RVSClipViewDelegate>
@property (nonatomic) UIImageView *fsImageView;
@property (nonatomic) UIActivityIndicatorView *fsSpinner;

@end

@implementation RVSAppClipPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppClipPlayer];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppClipPlayer];
}

-(void)initRVSAppClipPlayer
{
    _playerState = RVSAppClipPlayerViewStateReset;
    
    self.backgroundColor = [UIColor blackColor];
    
    self.clipsToBounds = YES;
    self.fullFrame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        
    if (!self.thumbnailContainer)
    {
        UIView *thumbnailContainer = [[UIView alloc] initWithFrame:self.fullFrame];
        thumbnailContainer.backgroundColor = [UIColor clearColor];
        
        self.thumbnailContainer = thumbnailContainer;
        [self addSubview:self.thumbnailContainer];
    }
    
    //image view
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.thumbnailContainer.frame.size.width, self.thumbnailContainer.frame.size.height)];
    [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
    [self.imageView setBackgroundColor:[UIColor clearColor]];
    [self.imageView setClipsToBounds:YES];
    [self.thumbnailContainer insertSubview:self.imageView atIndex:0];
    
    //video container (clipView will be added later to it)
    if (!self.videoContainer)
    {
        UIView *videoContainer = [[UIView alloc] initWithFrame:self.fullFrame];
        videoContainer.backgroundColor = [UIColor clearColor];
        
        self.videoContainer = videoContainer;
        [self addSubview:self.videoContainer];
    }
    
    self.clipView = [[RVSClipView alloc] initWithFrame:self.videoContainer.frame];
    self.clipView.delegate = self;
    
    //hide and add
    self.clipView.hidden = YES;
    [self.videoContainer insertSubview:self.clipView atIndex:0];
    
    if (!self.overlayContainer)
    {
        UIView *overlayContainer = [[UIView alloc] initWithFrame:self.fullFrame];
        overlayContainer.backgroundColor = [UIColor clearColor];
        
        self.overlayContainer = overlayContainer;
        [self addSubview:self.overlayContainer];
    }
    
    //spinner
    self.spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.spinner.center = CGPointMake(self.overlayContainer.frame.size.width / 2,
                                      self.overlayContainer.frame.size.height / 2);
    self.spinner.hidden = YES;
    [self.overlayContainer addSubview:self.spinner];
    
    //play overlay (label/image)
    self.playOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0,
                                                                self.overlayContainer.frame.size.width,
                                                                self.overlayContainer.frame.size.height)];
    self.playOverlay.backgroundColor = [UIColor clearColor];
    [self.overlayContainer addSubview:self.playOverlay];
    
    UILabel *label = [[UILabel alloc] initWithFrame:self.playOverlay.frame];
    label.textAlignment = NSTextAlignmentCenter;
    [self.playOverlay addSubview:label];
    
    label.text = [NSString fontAwesomeIconStringForEnum:FAIconPlay];
    label.textColor = [UIColor colorWithWhite:1 alpha:0.2];
    label.font = [UIFont fontWithName:kFontAwesomeFamilyName size:60];
    
    //tap view
    self.tapView = [[UIView alloc] initWithFrame:self.fullFrame];
    self.tapView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.tapView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePlayback)];
    [self.tapView addGestureRecognizer:tap];
    
    //move overlay to top
    [self insertSubview:self.overlayContainer belowSubview:self.tapView];
    
    //clipview subviews:
    self.fsImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [self.fsImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.fsImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    self.fsSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [self.fsSpinner setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.clipView.overlayView addSubview:self.fsSpinner];
    [self.clipView.overlayView addConstraint:[NSLayoutConstraint constraintWithItem:self.fsSpinner
                                                                           attribute:NSLayoutAttributeCenterX
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.clipView.overlayView
                                                                           attribute:NSLayoutAttributeCenterX
                                                                          multiplier:1.0
                                                                            constant:0.0]];
    [self.clipView.overlayView addConstraint:[NSLayoutConstraint constraintWithItem:self.fsSpinner
                                                                           attribute:NSLayoutAttributeCenterY
                                                                           relatedBy:NSLayoutRelationEqual
                                                                              toItem:self.clipView.overlayView
                                                                           attribute:NSLayoutAttributeCenterY
                                                                          multiplier:1.0
                                                                            constant:0.0]];
    
    [self.clipView.backgroundView addSubview:self.fsImageView];
    [self.clipView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[fsImageView]|" options:0 metrics:nil views:@{@"fsImageView":self.fsImageView}]];
    [self.clipView.backgroundView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[fsImageView]|" options:0 metrics:nil views:@{@"fsImageView":self.fsImageView}]];
    
    
    self.fullscreen = [RVSApplication application].deviceInLandscape;
}

#pragma mark - ClipView delegate

-(void)clipViewStateDidChange:(RVSClipView *)clipView
{
    if (self.playerState == RVSAppClipPlayerViewStateReset)
        return;
    
    switch (clipView.state)
    {
        case RVSClipViewStateStopped:
            self.playerState = RVSAppClipPlayerViewStateStopped;
            break;
            
        case RVSClipViewStateLoading:
            self.playerState = RVSAppClipPlayerViewStateLoading;
            break;
            
        case RVSClipViewStatePlaying:
            self.playerState = RVSAppClipPlayerViewStatePlaying;
            break;
            
        default:
            self.playerState = RVSAppClipPlayerViewStateReset;
            break;
    }
}

#pragma mark - Playback

-(BOOL)activateIfNeeded
{
    if (self != [RVSApplication application].activeClipPlayerView &&
        self.isPlaying == NO)
    {
        [self play];
        return YES;
    }

    return NO;
}

-(void)play
{
    if (!self.clip)
        return;
    
    //if needed
    [[RVSApplication application].activeClipPlayerView stop];
    
    [RVSApplication application].activeClipPlayerView = self;
    self.playerState = RVSClipViewStateLoading;
    
    //play
    [self.clipView play];
}

-(void)stop
{
    self.isPlaying = NO;
    self.isLoading = NO;
    
    //stop
    [self.clipView stop];
    self.playerState = RVSClipViewStateStopped;
    
    //remove as active
    if ([RVSApplication application].activeClipPlayerView == self)
    {
        [RVSApplication application].activeClipPlayerView = nil;
    }
}

-(void)togglePlayback
{
    NSLog(@"togglePlayback");
    if (self.isPlaying || self.isLoading)
    {
        [self stop];
    }
    else
    {
        [self play];
    }
}

-(void)reset
{
    self.playerState = RVSAppClipPlayerViewStateReset;
    
    if ([RVSApplication application].activeClipPlayerView == self)
        [RVSApplication application].activeClipPlayerView = nil;
    
//    self.clip = nil; //don't.
    self.thumbnail = nil;
    self.imageView.image = nil;
    self.fsImageView.image = nil;
}

#pragma mark - State

-(void)setPlayerState:(RVSAppClipPlayerViewState)playerState
{
    if (playerState == _playerState)
        return;
    
    RVSAppClipPlayerViewState oldPlayerState = _playerState;
    //will change
    [self playerStateWillChangeToState:playerState oldState:oldPlayerState];
    _playerState = playerState;
    //did change
    [self playerStateDidChangeToState:playerState oldState:oldPlayerState];
}

-(void)playerStateWillChangeToState:(RVSAppClipPlayerViewState)newState oldState:(RVSAppClipPlayerViewState)oldState
{
    
}

-(void)playerStateDidChangeToState:(RVSAppClipPlayerViewState)newState oldState:(RVSAppClipPlayerViewState)oldState
{
    switch (newState)
    {
        case RVSAppClipPlayerViewStateLoading:
            self.isLoading = YES;
            self.isPlaying = NO;
            
            self.imageView.hidden = NO;
            self.fsImageView.hidden = !self.fullscreen;
            
            self.clipView .hidden = YES;
            self.playOverlay.hidden = YES;
            self.spinner.hidden = NO;
            [self.spinner startAnimating];
            
            self.fsSpinner.hidden = NO;
            [self.fsSpinner startAnimating];
            break;
            
        case RVSAppClipPlayerViewStatePlaying:
            self.isLoading = NO;
            self.isPlaying = YES;
            
            self.imageView.hidden = YES;
            self.fsImageView.hidden = YES;
            
            self.clipView.hidden = NO;
            self.playOverlay.hidden = YES;
            self.spinner.hidden = YES;
            [self.spinner stopAnimating];
            
            self.fsSpinner.hidden = YES;
            [self.fsSpinner stopAnimating];
            break;
            
        case RVSAppClipPlayerViewStateReset: //on reset
        case RVSAppClipPlayerViewStateStopped:
            self.isLoading = NO;
            self.isPlaying = NO;
            
            self.imageView.hidden = NO;
            self.fsImageView.hidden = !self.fullscreen;
            
            self.clipView.hidden = YES;
            self.playOverlay.hidden = NO;
            self.spinner.hidden = YES;
            [self.spinner stopAnimating];
            
            self.fsSpinner.hidden = YES;
            [self.fsSpinner stopAnimating];
            break;
            
        default:
            break;
    }
    
    //fw to delegate
    if ([self.delegate respondsToSelector:@selector(clipPlayerView:stateDidChange:)])
    {
        [self.delegate clipPlayerView:self stateDidChange:newState];
    }
}

#pragma mark - Setters

-(void)setClip:(NSObject <RVSClip> *)clip
{
    self.clipView.clip = clip;
}

-(NSObject<RVSClip> *)clip
{
    return self.clipView.clip;
}

-(void)setThumbnail:(NSObject <RVSAsyncImage> *)thumbnail
{
    //clear old to cancel
    _thumbnail = nil;
    _thumbnail = thumbnail;
    
    if (_thumbnail)
    {
        __weak RVSAppClipPlayerView *weakSelf = self;
        [weakSelf.thumbnail imageWithSize:weakSelf.imageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
            weakSelf.imageView.image = image;
            weakSelf.fsImageView.image = image;
        }];
    }
}

-(void)setFullscreen:(BOOL)fullscreen
{
    _fullscreen = fullscreen;
    self.clipView.fullscreen = fullscreen;
    
    if (fullscreen && self.playerState != RVSAppClipPlayerViewStatePlaying)
    {
        self.fsImageView.hidden = NO;
    }
    else if (!fullscreen)
    {
        self.fsImageView.hidden = YES;
    }
}

@end
