//
//  RVSAppUserView.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserView.h"
#import <rvs_sdk_api helpers.h>
#import "UIColor+RVSApplication.h"
#import "RVSApplication.h"

@interface RVSAppUserView()
@property (nonatomic) RVSKeyPathObserver *userPropertiesObserver;
@property (nonatomic) NSObject <RVSAsyncError> *followAsyncError;
@property (nonatomic) NSObject <RVSAsyncImage> *asyncImage;
@end

@implementation RVSAppUserView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUserView];
}

-(void)initRVSAppUserView
{
    //round corners (if will have subclasses, do it there).
    self.userThumbnail.layer.cornerRadius = 5;
    self.userThumbnail.layer.masksToBounds = YES;
    
    [self.userThumbnail.layer setMinificationFilter:kCAFilterTrilinear];
    self.userThumbnail.contentMode = UIViewContentModeScaleAspectFill;
    
    self.postsTitleLabel.text = NSLocalizedString(@"user detail posts count title", nil);
    self.followersTitleLabel.text = NSLocalizedString(@"user detail followers count title", nil);
    self.followingTitleLabel.text = NSLocalizedString(@"user detail following count title", nil);
    
    if (self.userMentionsButton)
    {
        self.userMentionsButton.hidden = YES;
        [self.userMentionsButton addTarget:self action:@selector(showMentions:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.followToggleButton)
    {
        self.followToggleButton.hidden = YES;
        [self.followToggleButton addTarget:self action:@selector(toggleFollow:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.userSettingsButton)
    {
        self.userSettingsButton.hidden = YES;
        [self.userSettingsButton addTarget:self action:@selector(showSettings:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    if (self.postsCountTapView)
        [self addTapGestureToView:self.postsCountTapView target:self action:@selector(postsCountTapped:)];
    
    if (self.followingCountTapView)
        [self addTapGestureToView:self.followingCountTapView target:self action:@selector(followingCountTapped:)];
    
    if (self.followersCountTapView)
        [self addTapGestureToView:self.followersCountTapView target:self action:@selector(followersCountTapped:)];
    
}


-(void)addTapGestureToView:(UIView *)view target:(id)target action:(SEL)action
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    [view addGestureRecognizer:tap];
}

-(void)reset
{
    _user = nil;
    _userPropertiesObserver = nil;
    
    self.followToggleButton.hidden = YES;
    self.userSettingsButton.hidden = YES;
    self.userMentionsButton.hidden = YES;
    
    self.userHandleLabel.text = @"";
    self.userNameLabel.text = @"";
    
    self.followersCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
    self.followingCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
    self.postsCountLabel.text =     NSLocalizedString(@"counter placeholder", nil);
    
    self.userThumbnail.image = [UIImage imageNamed:@"default_profile"];
}

-(void)setUser:(NSObject<RVSUser> *)user
{
    [self setUser:user isSizingView:NO];
}

-(void)setUser:(NSObject<RVSUser> *)user isSizingView:(BOOL)isSizingView
{
    if (user  && _user == user) //reset even if nil
        return;
    
    [self reset];
    _user = user;
    
    if (!_user)
        return;
    
    self.userHandleLabel.text = [NSString stringWithFormat:@"@%@", user.userId]; //handle = '@' + userId
    
    self.userNameLabel.text = user.name;
    self.followersCountLabel.text = [self pretyNumber:user.followerCount];
    self.followingCountLabel.text = [self pretyNumber:user.followingCount];
    self.postsCountLabel.text = [self pretyNumber:user.postCount];
    
    self.asyncImage = [user.profileImage asyncImage];
    
    //is my user?
    if ([[RVSApplication application] isMyUserId:user.userId])
    {
        //my user
        self.followToggleButton.hidden = YES;
        self.userSettingsButton.hidden = NO;
        self.userMentionsButton.hidden = NO;
    }
    else
    {
        //not
        self.followToggleButton.hidden = NO;
        self.userSettingsButton.hidden = YES;
        self.userMentionsButton.hidden = YES;
    }
    
    [self updateActionButtons];
    [self setKvo];
}

- (void)setKvo
{
    __weak RVSAppUserView *weakSelf = self;
    self.userPropertiesObserver = [self.user newObserverForKeyPath:@"isFollowedByMe" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
        [weakSelf updateActionButtons];
    }];    
}

-(void)updateActionButtons
{
    self.followToggleButton.selected = [self.user.isFollowedByMe boolValue];
}

-(void)postsCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userView:didSelectPostsCountForUser:)])
    {
        [self.delegate userView:self didSelectPostsCountForUser:self.user];
    }
}

-(void)followersCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userView:didSelectFollowersCountForUser:)])
    {
        [self.delegate userView:self didSelectFollowersCountForUser:self.user];
    }
}

-(void)followingCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userView:didSelectFollowingCountForUser:)])
    {
        [self.delegate userView:self didSelectFollowingCountForUser:self.user];
    }
}

-(void)showSettings:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(userView:didSelectSettingsForUser:)])
    {
        [self.delegate userView:self didSelectSettingsForUser:self.user];
    }
}

-(void)showMentions:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(userView:didSelectMentionsForUser:)])
    {
        [self.delegate userView:self didSelectMentionsForUser:self.user];
    }
}

-(void)toggleFollow:(UIButton *)sender
{
    self.followToggleButton.selected = !self.followToggleButton.selected;
    BOOL shouldNotify = NO;
    
    if (![self.user.isFollowedByMe boolValue])
    {
        shouldNotify = YES;
        self.followAsyncError = [[RVSSdk sharedSdk] followUser:self.user.userId];
    }
    else
    {
        self.followAsyncError = [[RVSSdk sharedSdk] unfollowUser:self.user.userId];
    }
    
    
    __weak RVSAppUserView *weakSelf = self;
    
    [self.followAsyncError errorUsingBlock:^(NSError *error) {
        if (error)
        {
            NSLog(@"error: %@", error);
        }
        else if (shouldNotify)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationUserFollowDidChangeNotification object:nil];
        }
        
        //anyway
        [weakSelf updateActionButtons];
    }];
}

-(void)setAsyncImage:(NSObject<RVSAsyncImage> *)asyncImage
{
    __weak RVSAppUserView *weakSelf = self;
    
    _asyncImage = asyncImage;
    [_asyncImage imageWithSize:weakSelf.userThumbnail.frame.size usingBlock:^(UIImage *image, NSError *error) {
        if (error || !image)
        {
            weakSelf.userThumbnail.image = [UIImage imageNamed:@"default_profile"];
        }
        else
        {
            weakSelf.userThumbnail.image = image;
        }
    }];    
}

-(NSString *)pretyNumber:(NSNumber *)number
{
    if (!number)
        return NSLocalizedString(@"counter placeholder", nil);
    
    if ([number integerValue] < 1000)
        return [NSString stringWithFormat:@"%d", [number integerValue]];
    
    float num = [number integerValue] / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fK", num];
    
    num = num / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fM", num];
    
    num = num / 1000;
    return [NSString stringWithFormat:@"%.1fG", num];
}



@end
