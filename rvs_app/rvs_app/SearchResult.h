//
//  SearchResult.h
//  InnovaTV
//
//  Created by Barak Harel on 1/26/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SearchResult;
@protocol SearchResultDelegate <NSObject>

-(void)shareToTwitterWithData:(SearchResult *)data andImage:(UIImage *)image;
-(void)playClipWithData:(SearchResult *)data andImage:(UIImage *)image;

@end

@interface SearchResult : NSObject

@property (nonatomic) NSString *channelImageName;
@property (nonatomic) NSString *videoBaseUrl;

@property (nonatomic) NSString *programTitle;
@property (nonatomic) NSDate *programStart;
@property (nonatomic) NSDate *programStop;

@property (nonatomic) NSString *channelId;
@property (nonatomic) NSString *channelName;

@property (nonatomic) NSString *imageURL;
@property (nonatomic) NSDate *time;

@property (nonatomic) NSAttributedString *attText;

@property (weak, nonatomic) id <SearchResultDelegate> delegate;

@end
