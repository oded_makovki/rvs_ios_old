//
//  RVSAppHashTagsAcceleratorView.m
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTextAcceleratorView.h"
#import "UIView+NibLoading.h"
#import "RVSAppTextAcceleratorCell.h"
#import "RVSAppLeftAlignFlowLayout.h"

@interface RVSAppTextAcceleratorView()  <UICollectionViewDataSource, UICollectionViewDelegate>

@property (strong, nonatomic) RVSAppTextAcceleratorCell *sizingCell;
@end

@implementation RVSAppTextAcceleratorView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppTextAcceleratorView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppTextAcceleratorView];
    }
    return self;
}

-(void)initRVSAppTextAcceleratorView
{
    //load from nib
    [self loadContentsFromNib];
    
    self.loadingText = @"Loading...";
    self.noDataText  = @"Not available...";    
    
    self.errorLabel.text = self.loadingText;
    self.errorView.hidden = NO;
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 1000;
    layout.minimumInteritemSpacing = 2;
    layout.sectionInset = UIEdgeInsetsMake(2, 2, 2, 2);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    [self.collectionView setCollectionViewLayout:layout];
    
    self.sizingCell = [[RVSAppTextAcceleratorCell alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.frame.size.width, 20)];
    [self.collectionView registerClass:[RVSAppTextAcceleratorCell class] forCellWithReuseIdentifier:@"Cell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;    
}

//-(NSString *)textPrefix
//{
//    return @"#";
//}

-(void)reloadData
{
    if (!self.data)
    {
        self.errorLabel.text = self.loadingText;
        self.errorView.hidden = NO;
    }
    else if ([self.data count] == 0)
    {
        self.errorLabel.text = self.noDataText;
        self.errorView.hidden = NO;
    }
    else
    {
        self.errorView.hidden = YES;
    }
    
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
}

-(void)invalidateLayout
{
    [super invalidateLayout];
    [self.collectionView.collectionViewLayout invalidateLayout];
}

-(void)scrollToTop
{
    [self.collectionView setContentOffset:CGPointZero];
}

#pragma mark - CollectionView


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.data count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppTextAcceleratorCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSString *hashTag = self.data[indexPath.row];
    cell.textLabel.text = hashTag;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *hashTag = self.data[indexPath.row];
    self.sizingCell.textLabel.text = hashTag;
    
    CGSize size = [self.sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return CGSizeMake(size.width, size.height);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    NSString *text = self.data[indexPath.row];
    
    [self.delegate acceleratorView:self didSelectText:text];
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end
