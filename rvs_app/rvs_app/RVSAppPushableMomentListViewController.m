//
//  RVSAppPushableMomentListViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableMomentListViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import <rvs_sdk_api.h>
#import "RVSAppMomentListCell.h"
#import "RVSAppMomentListFooterCell.h"
#import "RVSApplication.h"

#import "RVSAppMomentDataObject.h"

@interface RVSAppPushableMomentListViewController () <RVSAppDataListViewDelegate, RVSAppMomentViewDelegate>
@property (nonatomic) NSObject <RVSAsyncMomentList> *momentList;
@property (nonatomic) BOOL shouldClearMomentsData;

@property (nonatomic) NSInteger momentsSection;
@end

@implementation RVSAppPushableMomentListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.momentsSection = 0;
    self.dataListView.numberOfSections = 1;
    
    if (self.headerClass)
    {
        self.momentsSection = 1;
        self.dataListView.numberOfSections = 2; //just moments
        [self.dataListView setSectionsCellClass:self.headerClass forSection:0];
    }
    
    [self.dataListView setSectionsCellClass:[RVSAppMomentListCell class] forSection:self.momentsSection];
    
    self.dataListView.footerCellClass = [RVSAppMomentListFooterCell class];
    self.dataListView.delegate = self;
    
    self.dataListView.nextPageTriggerSection = self.momentsSection;

    if (self.headerData)
    {
        [self loadHeader];
    }
    
    //already set - load data
    if (self.momentListFactory)
        [self loadMoments];
}

-(void)loadHeader
{
    [self.dataListView addData:@[self.headerData] toSection:0];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.dataListView activateIfNeeded];
}


- (void)reloadMoments
{
    self.shouldClearMomentsData = YES;
    [self loadMoments];
}

- (void)loadMoments
{
    self.momentList = nil;
    self.momentList = [self.momentListFactory momentList];
    
    NSInteger momentCount = 30;
    self.dataListView.nextPageTriggerDistance = momentCount / 2;
    
    __weak RVSAppPushableMomentListViewController *weakSelf = self;
    
    [self.momentList momentsUsingBlock:^(NSArray *moments, NSError *error) {
        
        if (weakSelf.shouldClearMomentsData)
        {
            weakSelf.shouldClearMomentsData = NO;
            [weakSelf.dataListView clearDataInSection:weakSelf.momentsSection];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;

            [weakSelf.dataListView addData:nil toSection:weakSelf.momentsSection];
        }
        else
        {
            if (!moments || [moments count] == 0)
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            }
            else
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:weakSelf];
            
            [weakSelf.dataListView addData:[RVSAppMomentDataObject dataObjectsFromMoments:moments
                                                                               momentViewDelegate:weakSelf]
                                 toSection:weakSelf.momentsSection];
        }
        
    } count:momentCount];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    [self.momentList nextMoments];
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadMoments];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if ([data isKindOfClass:[RVSAppMomentDataObject class]])
    {
        [self pushMomentDetailViewControlWithMoment:[(RVSAppMomentDataObject *)data moment] title:NSLocalizedString(@"moment title", nil)];
    }
}

-(void)networkStatusDidChange
{
    [super networkStatusDidChange];
    if (self.dataListView.error && [RVSApplication application].hasNetworkConnection)
    {
        self.dataListView.error = nil;
        self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault;
        
        [self.dataListView clear];
        [self reloadMoments];
    }
}

-(void)composeDidCompleteWithMoment:(NSObject <RVSMoment> *)moment inReplyToMoment:(NSObject <RVSMoment> *)replyToMoment
{
    //TBD
}

-(NSString *)descriptionForDataListView:(RVSAppDataListView *)dataListView
{
    return self.navigationItem.title;
}

@end
