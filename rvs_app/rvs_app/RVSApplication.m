//
//  RVSAppData.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import "UIColor+RVSApplication.h"
#import "RVSAppPortraitNavigationController.h"
#import "RVSAppChannelSelectViewController.h"
#import "RVSAppComposeViewController.h"
#import "SearchViewController.h"

#import <SVModalWebViewController.h>
#import <UIAlertView+BlocksKit.h>

NSString *RVSApplicationNetworkStatusDidChangeNotification = @"RVSApplicationNetworkStatusDidChangeNotification";

NSString *RVSApplicationChannelsDidChangeNotification = @"RVSApplicationChannelsDidChangeNotification";
NSString *RVSApplicationCurrentChannelDidChangeNotification = @"RVSApplicationCurrentChannelDidChangeNotification";

NSString *RVSApplicationNowTimeOffsetChangeNotification = @"RVSApplicationNowTimeOffsetChangeNotification";

NSString *RVSApplicationTopBarDidChangeNotification = @"RVSApplicationTopBarDidChangeNotification";
NSString *RVSApplicationComposeDidCompleteNotification = @"RVSApplicationComposeDidCompleteNotification";
NSString *RVSApplicationComposeDidCompleteNewPostUserInfoKey = @"RVSApplicationComposeDidCompleteNewPostUserInfoKey";
NSString *RVSApplicationComposeDidCompleteReplyToPostUserInfoKey = @"RVSApplicationComposeDidCompleteReplyToPostUserInfoKey";

NSString *RVSApplicationUserFollowDidChangeNotification = @"RVSApplicationUserFollowDidChangeNotification";


NSString *RVSApplicationUserDefaultsLastCurrentChannelIdKey = @"lastCurrentChannelId";


@interface RVSApplication() <RVSAppChannelSelectViewControllerDelegate, RVSAppComposeViewControllerDelegate>
@property (nonatomic) RVSNotificationObserver *channelListObserver;
@property (nonatomic) RVSNotificationObserver *deviceOrientationObserver;
@property (nonatomic) RVSNotificationObserver *connectionObserver;
@property (nonatomic) RVSNotificationObserver *topBarDidChangeObserver;

@property (nonatomic) RVSNotificationObserver *screenDidConnectObserver;
@property (nonatomic) RVSNotificationObserver *screenDidDisconnectbserver;

@property (nonatomic) RVSNotificationObserver *channelDetectorMatchObserver;
@property (nonatomic) RVSNotificationObserver *channelDetectorStatusObserver;

@property (nonatomic) NSObject <RVSAsyncPost> *asyncPost;

@property (nonatomic) BOOL hasNetworkConnection;
@property (nonatomic) BOOL shouldDetectChannel;

@property (nonatomic) NSDateFormatter *utcDateFormatter;
@property (nonatomic) NSTimeInterval nowTimeOffset;
@property (nonatomic) NSTimeInterval defaultNowTimeOffset;
@end


@implementation RVSApplication
@synthesize mainStoryboard = _mainStoryboard;
@synthesize currentChannel = _currentChannel;
@synthesize topSearchButton = _topSearchButton;

+(RVSApplication *)application
{
    static RVSApplication* _application;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        _application = [[RVSApplication alloc] init];
    });
    
    return  _application;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        __weak RVSApplication *weakSelf = self;
        self.hasNetworkConnection = YES; //lets be positive

        self.settingsDetectChannel = YES;
        
        self.defaultNowTimeOffset = 0;
        self.nowTimeOffset = self.defaultNowTimeOffset;
        
        self.deviceOrientationObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIDeviceOrientationDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf handleOrientationChangeNotification:notification];
        }];
        
        self.channelListObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelListChangedNotfication object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf handleChannelListChangedNotfication:notification];
        }];
        
        self.connectionObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceNetworkStatusChangedNotification object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf handleConnectionStatusChangeNotification:notification];
        }];
        
        
        self.screenDidConnectObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIScreenDidConnectNotification object:Nil usingBlock:^(NSNotification *notification) {
            weakSelf.isMirroring = NO;
        }];
        
        self.screenDidDisconnectbserver = [RVSNotificationObserver newObserverForNotificationWithName:UIScreenDidDisconnectNotification object:Nil usingBlock:^(NSNotification *notification) {
            weakSelf.isMirroring = YES;
        }];
        
        self.channelDetectorMatchObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
            [weakSelf handleChannelDetectorMatchNotification:notification];
        }];        
        
        [self setupMenu];
    }
    
    return self;
}

#pragma mark - Connection Status

-(void)refreshNetworkStatusAfterDelay:(NSTimeInterval)delay
{
    [self performSelector:@selector(refreshNetworkStatus) withObject:nil afterDelay:delay];
}

-(void)refreshNetworkStatus
{
    //cancel previous
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshNetworkStatus) object:nil];
    
    NSLog(@"Refreshed network status: %d",[RVSSdk sharedSdk].networkStatus);
    
    BOOL wasConnected = self.hasNetworkConnection;
    
    if ([RVSSdk sharedSdk].networkStatus == RVSServiceNetworkStatusNoNetwork ||
        [RVSSdk sharedSdk].networkStatus == RVSServiceNetworkStatusDisconnected)
    {
        self.hasNetworkConnection = NO;
        [self.topBar showNoNetworkStatusAnimated:YES afterDelay:0];
    }
    else
    {
        self.hasNetworkConnection = YES;
        [self.topBar hideNoNetworkStatusAnimated:YES afterDelay:1];
        //just in case the connection will die again, add a delay.
    }
    
    //notify if needded
    if (self.hasNetworkConnection != wasConnected)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationNetworkStatusDidChangeNotification object:self];
    }
}

-(void)handleConnectionStatusChangeNotification:(NSNotification *)notification
{
    [self refreshNetworkStatus];
}

#pragma mark - Channel detector

-(void)handleChannelDetectorMatchNotification:(NSNotification *)notification
{
    NSObject <RVSChannel> *channel = notification.userInfo[RVSSdkChannelDetectorChannelUserInfoKey];
    if (channel)
    {
        NSTimeInterval timeOffset = self.defaultNowTimeOffset;
        
//        //update nowTimeOffset
//        NSDate *time = notification.userInfo[RVSSdkChannelDetectorTimeUserInfoKey];
//        if (time)
//        {
//            timeOffset = [time timeIntervalSinceNow];
//            NSLog(@"time offset: %.2f", timeOffset);
//        }
        
        [self setCurrentChannel:channel withTimeOffset:timeOffset];
    }
}

#pragma mark - Player

-(void)handleOrientationChangeNotification:(NSNotification *)notification
{
    UIDeviceOrientation toInterfaceOrientation = [[UIDevice currentDevice] orientation];
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.deviceInLandscape = NO;
    }
    else if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
              toInterfaceOrientation == UIDeviceOrientationLandscapeRight)
    {
        self.deviceInLandscape = YES;
    }
}

-(void)setDeviceInLandscape:(BOOL)deviceInLandscape
{
    _deviceInLandscape = deviceInLandscape;
    
    if (self.activeClipPlayerView)
    {
        [self.activeClipPlayerView setFullscreen:_deviceInLandscape];
    }
    
//    NSLog(@"deviceInLandscape: %d", deviceInLandscape);
}

#pragma mark - Now Time Offset

-(void)setNowTimeOffset:(NSTimeInterval)nowTimeOffset
{
    _nowTimeOffset = nowTimeOffset;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationNowTimeOffsetChangeNotification object:self];
}

#pragma mark - Channels

-(void)setCurrentChannel:(NSObject <RVSChannel> *)channel withTimeOffset:(NSTimeInterval)timeOffset
{
    self.nowTimeOffset = timeOffset;
    self.currentChannel = channel;
}

- (void)handleChannelListChangedNotfication:(NSNotification *)notification
{
    NSLog(@"****** onChannelsDidChange ******");
    
    _channelsList = [[RVSSdk sharedSdk].channelList copy];
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationChannelsDidChangeNotification object:self];
    
    NSString *lastChannelId = [[NSUserDefaults standardUserDefaults] objectForKey:RVSApplicationUserDefaultsLastCurrentChannelIdKey];

    NSObject <RVSChannel> *previousChannel;
    
    if (lastChannelId)
    {
         previousChannel = [self getChannelById:lastChannelId];
    }
    
    //fallback
    if (!previousChannel && [_channelsList count] > 0)
    {
        previousChannel = _channelsList[0];
    }
    
    [self setCurrentChannel:previousChannel withTimeOffset:self.defaultNowTimeOffset];

    //can start detecting
    [self updateDetectChannelState];
}

- (NSObject <RVSChannel> *) getChannelById:(NSString *)channelId
{
    if (!channelId)
        return nil;
    
    for (NSObject <RVSChannel> *channel in self.channelsList)
    {
        if ([channel.channelId isEqualToString:channelId])
        {
            return channel;
        }
    }
    
    return nil;        
}

-(void)setCurrentChannel:(NSObject<RVSChannel> *)currentChannel
{
    _currentChannel = currentChannel;
    
    [[NSUserDefaults standardUserDefaults] setObject:_currentChannel.channelId forKey:RVSApplicationUserDefaultsLastCurrentChannelIdKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationCurrentChannelDidChangeNotification
                                                        object:self];
}

-(NSObject <RVSChannel> *)currentChannel
{
    if (!_currentChannel && [_channelsList count] > 0)
    {
        _currentChannel = _channelsList[0];
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationCurrentChannelDidChangeNotification object:self];
    }
    
    return _currentChannel;
}

#pragma mark - Channel Detection

-(void)setSettingsDetectChannel:(BOOL)settingsDetectChannel
{
    _settingsDetectChannel = settingsDetectChannel;
    [self updateDetectChannelState];
}

-(void)updateDetectChannelState
{
    self.shouldDetectChannel = (self.channelsList && self.isMirroring && self.settingsDetectChannel);
}

-(void)setShouldDetectChannel:(BOOL)shouldDetectChannel
{
    NSLog(@"setShouldDetectChannel: %@", shouldDetectChannel?@"YES":@"NO");
    
    _shouldDetectChannel = shouldDetectChannel;
    [[RVSSdk sharedSdk] enableChannelDetector:_shouldDetectChannel];
}

#pragma mark - Other

- (UIStoryboard *)mainStoryboard
{
    if (!_mainStoryboard)
    {
        _mainStoryboard = [UIStoryboard storyboardWithName:@"iPhoneMain" bundle:nil];
    }
    return _mainStoryboard;
}

-(void)setIsMirroring:(BOOL)shouldDetectChannel
{
    _isMirroring = shouldDetectChannel;
    [self updateDetectChannelState];
}

-(BOOL)isMyUserId:(NSString *)userId
{
    return [[[RVSSdk sharedSdk].myUserId lowercaseString] isEqualToString:[userId lowercaseString]];
}

-(NSDateFormatter *)utcDateFormatter
{
    if (!_utcDateFormatter)
    {
        _utcDateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [_utcDateFormatter setTimeZone:timeZone];
        [_utcDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    }
    
    return _utcDateFormatter;
}


-(void)setActiveViewController:(RVSAppCommonViewController *)activeViewController
{
    if (_activeViewController == activeViewController)
        return;
    
    [self.activeClipPlayerView stop];
    [self.topBar hide];
    
    _activeViewController = activeViewController;
    NSLog(@"activeViewController: %@", activeViewController.navigationItem.title);
}

#pragma mark - Top Bar

-(void)showTopBar
{
    [self.topBar show];
}

-(void)hideTopBar
{
    [self.topBar hide];
}

#pragma mark - Main Delegate

-(void)logout
{
    NSLog(@"Do logout");
    [[RVSSdk sharedSdk] signOut];
}

- (void)showChannelSelect
{
    RVSAppChannelSelectViewController *channelsViewController = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppChannelSelectViewController"];
    channelsViewController.delegate = self;
    
    RVSAppPortraitNavigationController *nc = [[RVSAppPortraitNavigationController alloc] initWithRootViewController:channelsViewController];
    
    [self.mainViewController presentViewController:nc animated:YES completion:nil];
}

-(void)showCompose
{
    [self.topBar hide];
    [self showReplyTo:nil];
}

-(void)showReplyTo:(NSObject <RVSPost> *)post
{
    [self.activeClipPlayerView stop];
    
    RVSAppComposeViewController *compose = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppComposeViewController"];
    
    compose.delegate = self;
    compose.replyToPost = post;
    
    [self.activeViewController.navigationController pushViewController:compose animated:YES];
}

-(void)showWebViewWithURL:(NSURL *)url
{
    [self.activeClipPlayerView stop];
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.navigationBar.translucent = NO;
    webViewController.navigationBar.barTintColor = [UIColor blackColor];
    
    //add line to nav bar
//    UIImageView *line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greyLine"]];
//    [line setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
//    [webViewController.view addSubview:line];
//    [webViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[line]-0-|" options:0 metrics:nil views:@{@"line":line}]];
//    
//    [webViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
//    
//    [webViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:webViewController.navigationBar attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    
    
    webViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    webViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    //set text style
    [webViewController.topViewController.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    
    [self.mainViewController presentViewController:webViewController animated:YES completion:nil];
    
    //override color (after presenting)
    [webViewController.navigationBar setTintColor:[UIColor rvsTintColor]];
}


#pragma mark - RVSAppChannelSelectViewController Delegate

-(void)channelSelectViewController:(RVSAppChannelSelectViewController *)viewController didSelectChannel:(NSObject <RVSChannel> *)channel
{
    if (channel)
    {
        //manual select - reset offset
        [self setCurrentChannel:channel withTimeOffset:self.defaultNowTimeOffset];
    }
    
    //dismiss
    [viewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - RVSAppComposeViewController Delegate

-(void)composeViewController:(RVSAppComposeViewController *)sender didCompleteWithComposePostFactory:(RVSAppComposePostFactory *)postFactory thumbnail:(NSObject<RVSAsyncImage> *)thumbnail inReplyToPost:(NSObject<RVSPost> *)replyToPost
{
    NSLog(@"pop");
    __weak RVSApplication *weakSelf = self;
   
    //dismiss
    [sender.navigationController popViewControllerAnimated:YES];
     
    if (postFactory)
    {
        //post
        [weakSelf.topBar showPostProgressWithThumbnail:thumbnail];
        
        //wait with post
        double delayInSeconds = 0.3;
        dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
        
        dispatch_after(delay, dispatch_get_main_queue(), ^(void){
            [weakSelf tryPostingWithFactory:postFactory inReplyToPost:replyToPost];
        });
    }
}

-(void)tryPostingWithFactory:(RVSAppComposePostFactory *)postFactory inReplyToPost:(NSObject <RVSPost> *)replyToPost
{
    //keep pointer
    self.asyncPost = [postFactory asyncPost];
    
    __weak RVSApplication *weakSelf = self;
    
     [self.asyncPost postUsingBlock:^(NSObject <RVSPost> *post, NSError *error)
     {
         if (!error)
         {
             [weakSelf.topBar hidePostProgressWithSuccess:YES delay:0.5];
             
             //notify
             NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:post,RVSApplicationComposeDidCompleteNewPostUserInfoKey, replyToPost, RVSApplicationComposeDidCompleteReplyToPostUserInfoKey, nil];
             
             [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationComposeDidCompleteNotification object:weakSelf userInfo:userInfo];
         }
         else
         {
             [UIAlertView bk_showAlertViewWithTitle:NSLocalizedString(@"compose post error title",nil)
                                         message:NSLocalizedString(@"compose post error message",nil)
                               cancelButtonTitle:NSLocalizedString(@"compose post error cancel",nil)
                               otherButtonTitles:@[NSLocalizedString(@"compose post error retry",nil)]
                                         handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                             if (buttonIndex == 0) //cancel
                                             {
                                                 [weakSelf.topBar hidePostProgressWithSuccess:NO delay:0.5];
                                             }
                                             else //retry
                                             {
                                                 [weakSelf tryPostingWithFactory:postFactory inReplyToPost:replyToPost];
                                             }
                                         }];
         }
     }];
}

#pragma mark - Search

-(UIBarButtonItem *)topSearchButton
{
    if (!_topSearchButton)
    {
        _topSearchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_search"] style:UIBarButtonItemStylePlain target:self action:@selector(topSearchButtonPressed)];
    }
    
    return _topSearchButton;
}

-(void)topSearchButtonPressed
{
    if (self.menu.isOpen)
        [self.menu close];
    
    [self.activeClipPlayerView stop];    
    SearchViewController *vc = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    
    [self.activeViewController.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Menu

#define DARK_WHITE [UIColor colorWithRed:242/255.0 green:242/255.0 blue:242/255.0 alpha:1]

-(UIBarButtonItem *)newTopMenuButton
{
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(topMenuButtonPressed)];
    
    return button;
}

-(void)topMenuButtonPressed
{
    if (self.menu.isOpen)
    {
        [self.menu close];
    }
    else
    {
        [self.activeClipPlayerView stop];
        
        CGRect frame = self.mainViewController.view.frame;
        frame.origin.y = 64;
        [self.menu showFromRect:frame inView:self.mainViewController.view];
    }
}

-(void)setupMenu
{
    _menu = [[REMenu alloc] initWithItems:[self menuItems]];
    
    //look
    self.menu.waitUntilAnimationIsComplete = NO;
    self.menu.itemHeight = 44;
    self.menu.bounce = NO;
    self.menu.font = [UIFont boldSystemFontOfSize:18];
    self.menu.subtitleFont = [UIFont systemFontOfSize:12];
    self.menu.separatorHeight = 1;
    self.menu.shadowOffset = CGSizeMake(0, 1);
    self.menu.imageOffset = CGSizeMake(5, 0);
    self.menu.textOffset = CGSizeMake(0, 1);
    self.menu.subtitleTextOffset = CGSizeMake(0, -1);
    
    self.menu.backgroundColor = [UIColor rvsTintColor];
    self.menu.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];
    self.menu.textColor = [UIColor colorWithWhite:1 alpha:0.9];
    self.menu.textShadowColor = [UIColor blackColor];
    self.menu.textShadowOffset = CGSizeMake(0, -1);
    
    self.menu.highlightedBackgroundColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
    self.menu.highlightedSeparatorColor = [UIColor colorWithRed:28/255.0 green:28/255.0 blue:27/255.0 alpha:1];
    self.menu.highlightedTextColor = [UIColor colorWithRed:128/255.0 green:126/255.0 blue:124/255.0 alpha:1];
    self.menu.highlightedTextShadowColor = [UIColor blackColor];
    self.menu.highlightedTextShadowOffset = CGSizeMake(0, -1);
    
    self.menu.subtitleTextColor = [UIColor colorWithWhite:0.425 alpha:1.000];
    self.menu.subtitleTextShadowColor = [UIColor blackColor];
    self.menu.subtitleTextShadowOffset = CGSizeMake(0, -1);
    self.menu.subtitleHighlightedTextColor = [UIColor colorWithRed:0.389 green:0.384 blue:0.379 alpha:1.000];
    self.menu.subtitleHighlightedTextShadowColor = [UIColor blackColor];
    self.menu.subtitleHighlightedTextShadowOffset = CGSizeMake(0, -1);
    
    self.menu.shadowColor = [UIColor lightGrayColor];
    self.menu.shadowOpacity = 0.2;
    
    self.menu.borderWidth = 0;
    self.menu.borderColor =  [UIColor colorWithWhite:0.5 alpha:0.5];
    
//    self.menu.liveBlur = YES;
//    self.menu.liveBlurTintColor = [[UIColor rvsTintColor] colorWithAlphaComponent:0.8];
}

-(NSArray *)menuItems
{
    __typeof (&*self) __weak weakSelf = self;
    
    NSMutableArray *items = [NSMutableArray array];
    REMenuItem *item;
    
    item = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"discovery title" , nil)
                                    subtitle:nil
                                       image:[UIImage imageNamed:@"tab_home1"]
                            highlightedImage:nil
                                      action:^(REMenuItem *item) {
                                          weakSelf.mainViewController.selectedIndex = 0;
                                      }];
    
    [items addObject:item];
    
    item = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"profile title" , nil)
                                    subtitle:nil
                                       image:[UIImage imageNamed:@"tab_me1"]
                            highlightedImage:nil
                                      action:^(REMenuItem *item) {
                                          weakSelf.mainViewController.selectedIndex = 2;
                                      }];
    
    [items addObject:item];
    
//    item = [[REMenuItem alloc] initWithTitle:NSLocalizedString(@"more title" , nil)
//                                    subtitle:nil
//                                       image:[UIImage imageNamed:@"tab_more1"]
//                            highlightedImage:nil
//                                      action:^(REMenuItem *item) {
//                                          weakSelf.mainViewController.selectedIndex = 3;
//                                      }];
//    
//    [items addObject:item];
    
    return items;
}

@end
