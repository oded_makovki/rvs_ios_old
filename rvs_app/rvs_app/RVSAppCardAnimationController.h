//
//  RVSAppCardAnimationController.h
//

#import <Foundation/Foundation.h>

@interface RVSAppCardAnimationController : NSObject <UIViewControllerAnimatedTransitioning>

/**
 The direction of the animation.
 */
@property (nonatomic, assign) BOOL reverse;

/**
 The animation duration.
 */
@property (nonatomic, assign) NSTimeInterval duration;

@end
