//
//  RVSAppChannelDataCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppChannelDataObject.h"

@interface RVSAppChannelDataCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppChannelView *channelView;
@property (nonatomic) RVSAppChannelDataObject *data;

-(void)reset;
@end
