//
//  RVSAppChannelSelectViewController2.m
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelSelectViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import "RVSAppChannelSelectCollectionView.h"
#import "RVSAppChannelSelectCollectionViewCell.h"
#import "RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import <rvs_sdk_api.h>

@interface RVSAppChannelSelectViewController () <UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet RVSAppChannelSelectCollectionView *collectionView;
@property (strong, nonatomic) UIBarButtonItem *cancelButton;
@property (nonatomic) RVSNotificationObserver *channelsDidChangeObserver;
@end

@implementation RVSAppChannelSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"channel select title", nil);
    self.navigationItem.leftBarButtonItem = self.cancelButton;
    
    __weak RVSAppChannelSelectViewController *weakSelf = self;
    
    self.channelsDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationChannelsDidChangeNotification object:nil usingBlock:^(NSNotification *notification)
    {
        [weakSelf channelsDidChange:notification];
    }];
    
	self.collectionView.delegate = self;
    [self refresh];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


-(void)viewDidLayoutSubviews
{
    if ([RVSApplication application].currentChannel)
    {
        [self.collectionView scrollToChannelId:[RVSApplication application].currentChannel.channelId animated:NO];
    }
}

- (void)channelsDidChange:(NSNotification *)notification
{
    [self refresh];
}

- (void)refresh
{
    self.collectionView.channels = [[RVSApplication application].channelsList copy];
    [self.collectionView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject <RVSChannel> *selectedChannel = self.collectionView.channels[indexPath.row];
    NSLog(@"Channel selected: %@", selectedChannel.name);
    
    [self.delegate channelSelectViewController:self didSelectChannel:selectedChannel];    
}

-(void)searchPressed:(id)sender
{
    NSLog(@"Search Channels");
}

- (void)cancel:(id)sender
{
    NSLog(@"Channel selected - Canceled");
    [self.delegate channelSelectViewController:self didSelectChannel:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Button

-(UIBarButtonItem *)cancelButton
{
    if (!_cancelButton)
    {
        _cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
    }
    
    return _cancelButton;
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end
