//
//  RVSAppPushableUserViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppCommonDataViewController.h"

@interface RVSAppPushableUserDetailViewController : RVSAppCommonDataViewController

@property (nonatomic) NSString *userId;
@property (nonatomic) NSObject <RVSUser> *user;

- (void)reloadPosts;
@end
