//
//  RVSAppTopBarView.m
//  rvs_app
//
//  Created by Barak Harel on 11/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTopBarView.h"
#import "RVSApplication.h"
#import "RVSAppChannelView.h"
#import "RVSAppTopBarPostProgressView.h"
#import "RVSAppTopBarNetworkStatusView.h"
#import "RVSAppTopBarPreComposeView.h"
#import <rvs_sdk_api helpers.h>

@interface RVSAppTopBarView()
@property (weak, nonatomic) IBOutlet UIView *touchView;

@property (weak, nonatomic) IBOutlet RVSAppTopBarPreComposeView *preComposeView;
@property (weak, nonatomic) IBOutlet RVSAppTopBarPostProgressView *postProgressView;
@property (weak, nonatomic) IBOutlet RVSAppTopBarNetworkStatusView *networkStatusView;

@property (weak, nonatomic) IBOutlet UIImageView *bottomLine;

@property (weak, nonatomic) NSLayoutConstraint *preComposeViewTopConstraint;
@property (weak, nonatomic) NSLayoutConstraint *postProgressViewTopConstraint;
@property (weak, nonatomic) NSLayoutConstraint *networkStatusViewTopConstraint;
@property (weak, nonatomic) NSLayoutConstraint *bottomLineTopConstraint;

//private overwrite:
@property (nonatomic) BOOL isShown;
@property (nonatomic) CGFloat barHeight;
@end

@implementation RVSAppTopBarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.clipsToBounds = YES;

        [self getTopConstraints];
        self.bottomLineTopConstraint.constant = 0;
        [self hidePostProgressAnimated:NO afterDelay:0];
        [self hideNoNetworkStatusAnimated:NO afterDelay:0];
        
        UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hide)];
        [self.touchView addGestureRecognizer:backTap];
        
        //start hidden
        self.isShown = YES;
        [self hideAnimated:NO];
    }
    return self;
}

-(void)getTopConstraints
{
    self.preComposeViewTopConstraint = [self getTopConstraintOf:self.preComposeView];
    self.postProgressViewTopConstraint = [self getTopConstraintOf:self.postProgressView];
    self.networkStatusViewTopConstraint = [self getTopConstraintOf:self.networkStatusView];
    self.bottomLineTopConstraint = [self getTopConstraintOf:self.bottomLine];
}

- (NSLayoutConstraint *)getTopConstraintOf:(id)object
{
    for (NSLayoutConstraint *constraint in self.constraints)
    {
        if ((constraint.firstItem == object && constraint.firstAttribute == NSLayoutAttributeTop) ||
            (constraint.secondItem == object && constraint.secondAttribute == NSLayoutAttributeTop))
        {
            return constraint;
        }
    }
    
    return nil;
}

//container
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    //get hitting view
    UIView *hitView = [super hitTest:point withEvent:event];
    
    //if not self, return it
    if (hitView != self)
        return hitView;
    
    return nil;
}

#pragma mark - Network View

-(void)hideNoNetworkStatusAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay
{
    self.networkStatusViewTopConstraint.constant = -30;
    
    if (animated)
    {
        __weak RVSAppTopBarView *weakSelf = self;
        [UIView animateWithDuration:0.3 delay:delay options:0 animations:^{
            [weakSelf layoutIfNeeded];
        } completion:nil];
    }
    else
    {
        [self layoutIfNeeded];
    }
    
    [self update];
}

-(void)showNoNetworkStatusAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay
{
    self.networkStatusViewTopConstraint.constant = 0;
    
    if (animated)
    {
        __weak RVSAppTopBarView *weakSelf = self;
        [UIView animateWithDuration:0.3 delay:delay options:0 animations:^{
            [weakSelf layoutIfNeeded];
        } completion:nil];
    }
    else
    {
        [self layoutIfNeeded];
    }
    
    [self update];
}

#pragma mark - Post Progresss View

-(void)hidePostProgressAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay
{
    self.postProgressViewTopConstraint.constant = -40;
    [self.postProgressView.spinner stopAnimating];
    
    if (animated)
    {
        __weak RVSAppTopBarView *weakSelf = self;
        [UIView animateWithDuration:0.2 delay:delay options:0 animations:^{
            [weakSelf layoutIfNeeded];
        } completion:nil];
    }
    else
    {
        [self layoutIfNeeded];
    }
    
    [self update];
}

-(void)hidePostProgressWithSuccess:(BOOL)success delay:(NSTimeInterval)delay
{
    if (success)
    {
        self.postProgressView.resultImageView.image = [UIImage imageNamed:@"iconPostResultGood"];
    }
    else
    {
        self.postProgressView.resultImageView.image = [UIImage imageNamed:@"iconPostResultBad"];
    }
    
    [self hidePostProgressAnimated:YES afterDelay:(NSTimeInterval)delay];
}

-(void)showPostProgressWithThumbnail:(NSObject <RVSAsyncImage> *)thumbnail
{
    [self.postProgressView.spinner startAnimating];
    self.postProgressView.imageView.image = nil;
    self.postProgressView.resultImageView.image = nil;
    self.postProgressView.thumbnail = thumbnail;
    
    self.postProgressView.textLabel.text = NSLocalizedString(@"compose post progress", nil);
    
    self.postProgressViewTopConstraint.constant = 0;
    
    __weak RVSAppTopBarView *weakSelf = self;
    [UIView animateWithDuration:0.2 animations:^{
        [weakSelf layoutIfNeeded];
    }];
    
    [self update];
}

#pragma mark - PreCompose View

-(void)showAnimated:(BOOL)animated
{
    if (self.isShown)
        return;
    
    self.isShown = YES;
    self.preComposeViewTopConstraint.constant = 0;
    
    [[RVSApplication application].activeClipPlayerView stop];
    [self.preComposeView startCurrentChannelTimer];
    
    if (animated)
    {
        __weak RVSAppTopBarView *weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.touchView.alpha = 1;
            [weakSelf layoutIfNeeded];
        }];
    }
    else
    {
        self.touchView.alpha = 1;
        [self layoutIfNeeded];
    }
    
    [self update];
}

-(void)hideAnimated:(BOOL)animated
{
    if (!self.isShown)
        return;
    
    self.isShown = NO;
    self.preComposeViewTopConstraint.constant = -240;

    [self.preComposeView stopCurrentChannelTimer];
    
    if (animated)
    {
        __weak RVSAppTopBarView *weakSelf = self;
        [UIView animateWithDuration:0.2 animations:^{
            weakSelf.touchView.alpha = 0;
            [weakSelf layoutIfNeeded];
        }];
    }
    else
    {
        self.touchView.alpha = 0;
        [self layoutIfNeeded];
    }
    
    [self update];
}

-(void)show
{
    [self showAnimated:YES];
}

-(void)hide
{
    [self hideAnimated:YES];
}

#pragma mark - Common

-(void)update
{
    CGFloat height = 0;
//    if (self.preComposeViewTopConstraint.constant == 0)
//        height += self.preComposeView.frame.size.height;
//    
//    if (self.postProgressViewTopConstraint.constant == 0)
//        height += self.postProgressView.frame.size.height;
    
    if (self.networkStatusViewTopConstraint.constant == 0)
        height += self.networkStatusView.frame.size.height;
    
    
    self.barHeight = height;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationTopBarDidChangeNotification object:self];
}


@end
