//
//  RVSAppPostCommentCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostCommentCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppPostCommentCell() <RVSAppClipPlayerViewDelegate>
@end

@implementation RVSAppPostCommentCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostCommentCell];
    }
    
    return self;
}

-(void)initRVSAppPostCommentCell
{
    [self loadContentsFromNib];
    self.postView.shouldParseHotwords = NO;
    self.postView.isUsingLongTimeFormat = YES;
    
    self.postView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.postView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.postView.programNameLabel.textColor = [UIColor rvsProgramTextColor];
    
    self.postView.textLabel.preferredMaxLayoutWidth = 215;
}

@end
