//
//  RVSAppStyledViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppStyledViewController.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppStyledViewController ()

@end

@implementation RVSAppStyledViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor darkGrayColor], NSForegroundColorAttributeName,nil]];
    
    self.navigationController.navigationBar.tintColor = [UIColor rvsTintColor];
    self.view.backgroundColor = [UIColor rvsBackgroundColor];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@""
                                style:UIBarButtonItemStylePlain
                                target:nil action:nil];
    
    self.navigationItem.backBarButtonItem = btnBack;    
}

@end
