//
//  RVSAppTopBarToggleButtonInnerView.m
//  rvs_app
//
//  Created by Barak Harel on 1/14/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppTopBarToggleButtonInnerView.h"

@interface RVSAppTopBarToggleButtonInnerView()

@end

@implementation RVSAppTopBarToggleButtonInnerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _button = [[UIButton alloc] initWithFrame:self.bounds];
        [_button setImage:[UIImage imageNamed:@"icon_snap_enabled_front"] forState:UIControlStateSelected]; //| UIControlStateDisabled | UIControlStateHighlighted | UIControlStateSelected];
        
        [_button setImage:[UIImage imageNamed:@"icon_snap_disabled"] forState:UIControlStateNormal]; //| UIControlStateDisabled | UIControlStateHighlighted | UIControlStateSelected];
        
        _backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_snap_enabled_back"]];
        
        [self addSubview:_backgroundImageView];
        [self addSubview:_button];
        
        self.selected = YES;        
    }
    return self;
}

-(void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [self.button addTarget:target action:action forControlEvents:controlEvents];
}

-(void)setSelected:(BOOL)selected
{
    self.button.selected = selected;
}

-(BOOL)selected
{
    return self.button.selected;
}

-(void)showMatchAnimation
{
    __weak RVSAppTopBarToggleButtonInnerView *weakSelf = self;

    [UIView animateKeyframesWithDuration:1.2 delay:2.0 options:UIViewKeyframeAnimationOptionCalculationModeLinear |
     UIViewAnimationOptionCurveEaseInOut animations:^{
        
        int turns = 2;
        float step = 1.0 / (turns * 4);
        
        for (int i = 0; i < turns; i++)
        {
            [UIView addKeyframeWithRelativeStartTime:step * (0 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(M_PI / 2);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (1 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(M_PI);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (2 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(-M_PI/2);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (3 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(0);
            }];
        }
        
    } completion:nil];
}

-(void)showDetectingAnimation
{
    __weak RVSAppTopBarToggleButtonInnerView *weakSelf = self;
    
    [UIView animateKeyframesWithDuration:5 delay:0 options:UIViewKeyframeAnimationOptionCalculationModeLinear |
     UIViewAnimationOptionCurveLinear animations:^{
        
        int turns = 3;
        float step = 1.0 / (turns * 4);
        
        for (int i = 0; i < turns; i++)
        {
            [UIView addKeyframeWithRelativeStartTime:step * (0 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(M_PI / 5);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (1 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(0);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (2 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(-M_PI / 5);
            }];
            
            [UIView addKeyframeWithRelativeStartTime:step * (3 + i * 4) relativeDuration:step animations:^{
                weakSelf.backgroundImageView.transform = CGAffineTransformMakeRotation(0);
            }];
        }
        
    } completion:nil];
}

@end
