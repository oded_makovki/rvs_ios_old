//
//  RVSAppChannelDetailCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/18/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDetailCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppChannelDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppChannelDetailCell];
    }
    return self;
}

-(void)initRVSAppChannelDetailCell
{
    [self loadContentsFromNib];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 60);
}

@end
