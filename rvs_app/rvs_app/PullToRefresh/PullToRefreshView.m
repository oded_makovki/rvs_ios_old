//
//  PullToRefreshView.m
//  Grant Paul (chpwn)
//
//  (based on EGORefreshTableHeaderView)
//
//  Created by Devin Doty on 10/14/09October14.
//  Copyright 2009 enormego. All rights reserved.
//
//
// The MIT License (MIT)
// Copyright © 2012 Sonny Parlin, http://sonnyparlin.com
//
// //  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "PullToRefreshView.h"
#import <AudioToolbox/AudioToolbox.h>
#import "NSDate+RVSApplication.h"

#define FLIP_ANIMATION_DURATION 0.18f


@interface PullToRefreshView()

@property (nonatomic) UILabel *lastUpdatedLabel;
@property (nonatomic) UILabel *statusLabel;
@property (nonatomic) CALayer *arrowImage;
@property (nonatomic) UIActivityIndicatorView *activityView;
@property (nonatomic, assign) PullToRefreshViewState state;
@end

@implementation PullToRefreshView
@synthesize textColor = _textColor;
@synthesize shadowColor = _shadowColor;

- (id)initWithScrollView:(UIScrollView *)scroll
{
    CGRect frame = CGRectMake(0.0f, 0.0f - scroll.bounds.size.height, scroll.bounds.size.width, scroll.bounds.size.height);
    
    if ((self = [super initWithFrame:frame]))
    {
        self.scrollView = scroll;
        [self.scrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:NULL];
        self.startingContentInset = self.scrollView.contentInset;
        
		self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		self.backgroundColor = [UIColor colorWithRed:226.0/255.0 green:231.0/255.0 blue:237.0/255.0 alpha:1.0];
        
		self.lastUpdatedLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, frame.size.height - 30.0f, self.frame.size.width, 20.0f)];
        [self addSubview:self.lastUpdatedLabel];
        
		self.statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, frame.size.height - 48.0f, self.frame.size.width, 20.0f)];
        [self addSubview:self.statusLabel];
        
		self.arrowImage = [[CALayer alloc] init];
		[self.layer addSublayer:self.arrowImage];
        
        self.activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
		[self addSubview:self.activityView];
		
        self.playSounds = YES;
        self.enabled = YES;
		[self setState:PullToRefreshViewStateNormal];
    }
    
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = CGRectMake(0.0f, 0.0f - self.frame.size.height, self.frame.size.width, self.frame.size.height);
    
    self.lastUpdatedLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.lastUpdatedLabel.font = [UIFont systemFontOfSize:12.0f];
    self.lastUpdatedLabel.textColor = self.textColor;
    self.lastUpdatedLabel.shadowColor = self.shadowColor;
    self.lastUpdatedLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.lastUpdatedLabel.backgroundColor = [UIColor clearColor];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
    self.lastUpdatedLabel.textAlignment = NSTextAlignmentCenter;
#else
    lastUpdatedLabel.textAlignment = UITextAlignmentCenter;
#endif

    self.statusLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.statusLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    self.statusLabel.textColor = self.textColor;
    self.statusLabel.shadowColor = self.shadowColor;
    self.statusLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    self.statusLabel.backgroundColor = [UIColor clearColor];
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
#else
    statusLabel.textAlignment = UITextAlignmentCenter;
#endif

    self.arrowImage.frame = CGRectMake(10.0f, frame.size.height - 60.0f, 24.0f, 52.0f);
    self.arrowImage.contentsGravity = kCAGravityResizeAspect;
    self.arrowImage.contents = (id)[UIImage imageNamed:@"arrow"].CGImage;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 40000
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        self.arrowImage.contentsScale = [[UIScreen mainScreen] scale];
    }
#endif

    self.activityView.frame = CGRectMake(10.0f, frame.size.height - 38.0f, 20.0f, 20.0f);
}

#pragma mark -
#pragma mark Setters

-(UIColor *)textColor
{
    if (!_textColor)
    {
        _textColor = [UIColor colorWithRed:(87.0/255.0) green:(108.0/255.0) blue:(137.0/255.0) alpha:1.0];
    }
    return _textColor;
}

-(void)setTextColor:(UIColor *)textColor
{
    if (textColor != _textColor)
    {
        _textColor = textColor;
        [self setNeedsLayout];
    }
}

-(UIColor *)shadowColor
{
    if (!_shadowColor)
    {
        _shadowColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
    }
    
    return _shadowColor;
}

-(void)setShadowColor:(UIColor *)shadowColor
{
    if (shadowColor != _shadowColor)
    {
        _shadowColor = shadowColor;
        [self setNeedsLayout];
    }
}

- (void)showActivity:(BOOL)shouldShow animated:(BOOL)animated
{
    if (shouldShow) [self.activityView startAnimating];
    else [self.activityView stopAnimating];
    
    [UIView animateWithDuration:(animated ? 0.1f : 0.0) animations:^{
        self.arrowImage.opacity = (shouldShow ? 0.0 : 1.0);
    }];
}

- (void)setImageFlipped:(BOOL)flipped
{
    [UIView animateWithDuration:0.1f animations:^{
        self.arrowImage.transform = (flipped ? CATransform3DMakeRotation(M_PI * 2, 0.0f, 0.0f, 1.0f) : CATransform3DMakeRotation(M_PI, 0.0f, 0.0f, 1.0f));
    }];
}

- (void)setEnabled:(BOOL)enabled
{
	if (enabled == _enabled)
		return;
	
	_enabled = enabled;
	[UIView animateWithDuration:0.25
                     animations:
	 ^{
		 self.alpha = enabled ? 1 : 0;
	 }];
}

//- (void)refreshLastUpdatedDate
//{
//    NSDate *date;
//    
//	if ([self.delegate respondsToSelector:@selector(pullToRefreshViewLastUpdated:)])
//		date = [self.delegate pullToRefreshViewLastUpdated:self];
//    
//    if (!date)
//        date = [NSDate date];
//    
//    NSString *format = NSLocalizedString(@"refresh control last update format", nil);
//    self.lastUpdatedLabel.text = [NSString stringWithFormat:format, [date timeLongStringSinceNow]];    
//}

- (void)refreshLastUpdatedDate
{
    NSDate *date = [NSDate date];
    
    if ([self.delegate respondsToSelector:@selector(pullToRefreshViewLastUpdated:)])
        date = [self.delegate pullToRefreshViewLastUpdated:self];
    
    if (!date)
        date = [NSDate date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    self.lastUpdatedLabel.text = [NSString stringWithFormat:@"Last Updated: %@", [formatter stringFromDate:date]];
}

- (void)setState:(PullToRefreshViewState)state {
    _state = state;
    
	switch (_state) {
		case PullToRefreshViewStateReady:
			self.statusLabel.text = NSLocalizedString(@"refresh control state ready", nil);
			[self showActivity:NO animated:NO];
            [self setImageFlipped:YES];
            self.scrollView.contentInset = self.startingContentInset;
            break;
            
		case PullToRefreshViewStateNormal:
			self.statusLabel.text = NSLocalizedString(@"refresh control state normal", nil);
			[self showActivity:NO animated:NO];
            [self setImageFlipped:NO];
			[self refreshLastUpdatedDate];
            self.scrollView.contentInset = self.startingContentInset;
			break;
            
		case PullToRefreshViewStateLoading:
			self.statusLabel.text = NSLocalizedString(@"refresh control state loading", nil);
			[self showActivity:YES animated:YES];
            [self setImageFlipped:NO];
            self.scrollView.contentInset = UIEdgeInsetsMake(60.0f, 0.0f, 0.0f, 0.0f);
			break;
            
		default:
			break;
	}
}

#pragma mark -
#pragma mark UIScrollView

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"contentOffset"] && self.enabled)
    {
        if (self.scrollView.isDragging)
        {
            if (self.state == PullToRefreshViewStateReady)
            {
                if (self.scrollView.contentOffset.y > -65.0f && self.scrollView.contentOffset.y < 0.0f)
                {
                    [self setState:PullToRefreshViewStateNormal];
                }
            }
            else if (self.state == PullToRefreshViewStateNormal)
            {
                if (self.scrollView.contentOffset.y < -65.0f)
                {
                    if (self.playSounds)
                        [self playSound:@"psst1" withExt:@"wav"];
                    
                    [self setState:PullToRefreshViewStateReady];
                }
            }
            else if (self.state == PullToRefreshViewStateLoading)
            {
                if (self.scrollView.contentOffset.y >= 0)
                {
                    self.scrollView.contentInset = self.startingContentInset;
                }
                else
                {
                    self.scrollView.contentInset = UIEdgeInsetsMake(MIN(-self.scrollView.contentOffset.y, 60.0f), 0, 0, 0);
                }
            }
        }
        else
        {
            if (self.state == PullToRefreshViewStateReady)
            {
                [UIView animateWithDuration:0.2f animations:^{
                    [self setState:PullToRefreshViewStateLoading];
                }];
                
                if ([self.delegate respondsToSelector:@selector(pullToRefreshViewShouldRefresh:)])
                {
                    [self.delegate pullToRefreshViewShouldRefresh:self];
                }
            }
        }
        self.frame = CGRectMake(self.scrollView.contentOffset.x, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
    }
}

- (void)finishedLoading
{
    if (self.state == PullToRefreshViewStateLoading)
    {
        if (self.playSounds)
            [self playSound:@"pop" withExt:@"wav"];
        
        [UIView animateWithDuration:0.3f animations:^{
            [self setState:PullToRefreshViewStateNormal];
        }];
    }
}

-(void) playSound:(NSString *)fName withExt:(NSString *) ext
{
    SystemSoundID completeSound;
    NSURL *audioPath = [[NSBundle mainBundle] URLForResource:fName withExtension:ext];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)audioPath, &completeSound);
    AudioServicesPlaySystemSound (completeSound);
}

#pragma mark -
#pragma mark Dealloc

- (void)dealloc
{
	[self.scrollView removeObserver:self forKeyPath:@"contentOffset"];
	self.scrollView = nil;
}

@end
