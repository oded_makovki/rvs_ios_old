//
//  RVSAppLoadingPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataListFooterCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppDataListFooterCell()
@property (nonatomic) RVSAppDataListFooterType type;
@property (nonatomic) NSMutableArray *textForType;
@end

@implementation RVSAppDataListFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppDataListFooterCell];
    }
    return self;
}

-(void)initRVSAppDataListFooterCell
{
    self.textForType = [NSMutableArray arrayWithObjects:
                        NSLocalizedString(@"footer default", nil),
                        NSLocalizedString(@"footer loading more", nil),
                        NSLocalizedString(@"footer end of list", nil),
                        nil];
    
    [self reset];
}

-(void)setData:(RVSAppDataListFooterData *)data
{
    [super setData:data];
    self.type = data.type;
}

-(void)setType:(RVSAppDataListFooterType)type
{
    _type = type;
    
    self.msgLabel.textColor = [UIColor rvsFooterTextColor];
    
    switch (_type)
    {
        case RVSAppDataListFooterTypeDefault:
            self.msgLabel.text = self.textForType[_type];
            break;
            
        case RVSAppDataListFooterTypeLoading:
            self.msgLabel.text = self.textForType[_type];
            break;
            
        case RVSAppDataListFooterTypeEndOfList:
            self.msgLabel.text = self.textForType[_type];
            break;
            
        default:
            self.msgLabel.text = @"";
            break;
    }
}

-(void)reset
{
    self.type = RVSAppDataListFooterTypeLoading;
}

-(void)setText:(NSString *)text forType:(RVSAppDataListFooterType)type
{
    self.textForType[type] = text;
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 40);
}

@end
