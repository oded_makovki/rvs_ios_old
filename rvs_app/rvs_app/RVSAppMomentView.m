//
//  RVSAppMomentCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentView.h"
#import "RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import "RVSAppUserMiniView.h"

@interface RVSAppMomentView() <TTTAttributedLabelDelegate, RVSAppUserMiniViewDelegate>
@property (nonatomic) NSMutableArray *userMiniViews;

@property (nonatomic) NSObject <RVSAsyncPostList> *asyncTopPostList;
@end

@implementation RVSAppMomentView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppMomentView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppMomentView];
    }
    
    return self;
}

-(void)initRVSAppMomentView
{
    self.shouldParseHotwords = NO;
    self.isUsingLongTimeFormat = NO;
    self.minimumUsersImagesSpacing = 2.0;
    
    if (self.topPostView.textLabel)
    {
        self.topPostView.textLabel.textColor        = [UIColor rvsMomentTextColor];
        self.topPostView.timeLabel.textColor        = [UIColor rvsMomentTimestampColor];
    }
    
    if (self.topPostsLabel)
    {
        self.topPostsLabel.textColor                = [UIColor rvsMomentTopPostsColor];
        self.topPostsLabel.text = NSLocalizedString(@"top posts title", nil);
    }
    
    if (self.usersImagesContainer && !self.userMiniViews)
    {
        [self createUsersMiniViews];
    }
    
    self.postsTitleLabel.text = NSLocalizedString(@"moment detail posts count title", nil);
    self.favoritesTitleLabel.text = NSLocalizedString(@"moment detail favorites count title", nil);
    self.repostsTitleLabel.text = NSLocalizedString(@"moment detail reposts count title", nil);
    
    if (self.postsCountTapView)
        [self addTapGestureToView:self.postsCountTapView target:self action:@selector(postsCountTapped:)];
    
    if (self.favoritesCountTapView)
        [self addTapGestureToView:self.favoritesCountTapView target:self action:@selector(favoritesCountTapped:)];
    
    if (self.repostsCountTapView)
        [self addTapGestureToView:self.repostsCountTapView target:self action:@selector(repostsCountTapped:)];
}

-(void)addTapGestureToView:(UIView *)view target:(id)target action:(SEL)action
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    [view addGestureRecognizer:tap];
}

-(void)createUsersMiniViews
{
    if (!self.usersImagesContainer)
        return;
    
    //remove old
    for (UIView *view in self.usersImagesContainer.subviews)
    {
        [view removeFromSuperview];
    }
    
    CGFloat size = self.usersImagesContainer.frame.size.height;
    
    NSInteger num = (self.usersImagesContainer.frame.size.width + self.minimumUsersImagesSpacing) / (size + self.minimumUsersImagesSpacing);
    
    CGFloat spacing = 0;

    //calculate spacing:
    //total space = width - [size x number of images]
    //spacing = [total space] / [number of images + 1]
    
    if (num > 1)
        spacing = (self.usersImagesContainer.frame.size.width - (size * num)) / (num - 1);
    
    
    self.userMiniViews = [NSMutableArray arrayWithCapacity:num];
    
    for (int i = 0; i < num; i++)
    {
        RVSAppUserMiniView *miniView = [[RVSAppUserMiniView alloc] initWithFrame:CGRectMake(self.usersImagesContainer.frame.size.width - (i+1) * (size + spacing), 0, size, size)];

        miniView.delegate = self;
        [self.userMiniViews addObject:miniView];
        
        [self.usersImagesContainer addSubview:miniView];
    }
}

-(void)didSelectUserMiniView:(RVSAppUserMiniView *)userMiniView
{    
    if ([self.delegate respondsToSelector:@selector(momentView:didSelectUser:)])
    {
        [self.delegate momentView:self didSelectUser:userMiniView.user];
    }
}

-(void)reset
{
    _moment = nil;
    [self.topPostView reset];
    
    self.repostsCountLabel.text   = NSLocalizedString(@"counter placeholder", nil);
    self.favoritesCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
    self.postsCountLabel.text     = NSLocalizedString(@"counter placeholder", nil);
    
    //remove old
    for (RVSAppUserMiniView *miniView in self.usersImagesContainer.subviews)
    {
        [miniView reset];
    }
}

-(void)setMoment:(NSObject<RVSMoment> *)moment
{
    [self setMoment:moment isSizingView:NO];
}

-(void)setMoment:(NSObject <RVSMoment> *)moment isSizingView:(BOOL)isSizingView
{
    if (moment && _moment == moment) //reset even if nil
        return;
    
    [self reset];    
    _moment = moment;   
    
    if (!_moment)
        return;
    
    
    //set top post
    if ([_moment.topPosts count] > 0)
    {
        [self.topPostView setPost:_moment.topPosts[0] isSizingView:isSizingView];
    }
    
    if (self.commentInfoLabel)
    {
        NSString *usersString = @"no comments";
        NSArray *users = [self.moment.topPosts valueForKey:@"author"];
        NSMutableArray *userNames = [NSMutableArray array];
        
        BOOL hasMyUser = NO;
        
        for (NSObject <RVSUser> *user in users)
        {
            if ([[RVSApplication application] isMyUserId:user.userId])
            {
                hasMyUser = YES;
            }
            else if ([userNames indexOfObject:user.name] == NSNotFound)
            {
                [userNames addObject:user.name];
            }
        }
        
        //always first
        if (hasMyUser)
            [userNames insertObject:@"You" atIndex:0];
        
        if ([userNames count] > 2)
        {
            usersString = [NSString stringWithFormat:@"%@, %@ and %d more people commented on this", userNames[0], userNames[1], [userNames count] - 2];
        }
        else if ([userNames count] == 2)
        {
            usersString = [NSString stringWithFormat:@"%@ and %@ commented on this", userNames[0], userNames[1]];
        }
        else if ([userNames count] == 1)
        {
            usersString = [NSString stringWithFormat:@"%@ commented on this", userNames[0]];
        }
        
        self.commentInfoLabel.text = usersString;
    }
    
    if (self.likeInfoLabel)
    {
        self.likeInfoLabel.text = [NSString stringWithFormat:@"%d likes", [self.moment.favoriteCount integerValue]];
    }
    
    if (!isSizingView)
    {
        self.repostsCountLabel.text = [self pretyNumber:moment.repostCount];
        self.favoritesCountLabel.text = [self pretyNumber:moment.favoriteCount];
        self.postsCountLabel.text = [self pretyNumber:moment.postCount];
        
        //for now
        if (self.usersImagesContainer)
            [self getTopUsers];
    }
}

-(void)getTopUsers
{
//    NSLog(@"all users: %@", [self.moment.topPosts valueForKey:@"author"]);
    NSSet *users = [NSSet setWithArray:[self.moment.topPosts valueForKey:@"author"]];
//    NSLog(@"unique users: %@", users);
    
    int i = 0;
    for (NSObject <RVSUser> *user in users)
    {
        if (i<self.userMiniViews.count)
        {
            RVSAppUserMiniView *miniView = self.userMiniViews[i];
            miniView.user = user;
            
            i++;
        }
        else
        {
            break;
        }
    }
}

-(void)setShouldParseHotwords:(BOOL)shouldParseHotwords
{
    _shouldParseHotwords = shouldParseHotwords;
    self.topPostView.shouldParseHotwords = _shouldParseHotwords;
}

-(void)setIsUsingLongTimeFormat:(BOOL)isUsingLongTimeFormat
{
    _isUsingLongTimeFormat = isUsingLongTimeFormat;
    self.topPostView.isUsingLongTimeFormat = isUsingLongTimeFormat;
    
}

-(void)setDelegate:(id<RVSAppMomentViewDelegate>)delegate
{
    _delegate = delegate;
    self.topPostView.delegate = delegate;
}


-(void)postsCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(momentView:didSelectPostsCountForMoment:)])
    {
        [self.delegate momentView:self didSelectPostsCountForMoment:self.moment];
    }
}

-(void)favoritesCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(momentView:didSelectFavoritesCountForMoment:)])
    {
        [self.delegate momentView:self didSelectFavoritesCountForMoment:self.moment];
    }
}

-(void)repostsCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(momentView:didSelectRepostsCountForMoment:)])
    {
        [self.delegate momentView:self didSelectRepostsCountForMoment:self.moment];
    }
}

-(NSString *)pretyNumber:(NSNumber *)number
{
    if (!number)
        return NSLocalizedString(@"counter placeholder", nil);
    
    if ([number integerValue] < 1000)
        return [NSString stringWithFormat:@"%d", [number integerValue]];
    
    float num = [number integerValue] / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fK", num];
    
    num = num / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fM", num];
    
    num = num / 1000;
    return [NSString stringWithFormat:@"%.1fG", num];
}


@end
