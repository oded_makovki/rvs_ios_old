//
//  RVSAppUserMiniView.h
//  rvs_app
//
//  Created by Barak Harel on 1/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+NibLoading.h"
#import <rvs_sdk_api.h>

@class RVSAppUserMiniView;
@protocol RVSAppUserMiniViewDelegate <NSObject>

@optional
-(void)didSelectUserMiniView:(RVSAppUserMiniView *)userMiniView;
@end

@interface RVSAppUserMiniView : NibLoadedView
@property (weak, nonatomic) IBOutlet UIImageView *userThumbnail;
@property (nonatomic) NSObject <RVSUser> *user;
@property (weak, nonatomic) id <RVSAppUserMiniViewDelegate> delegate;

-(void)reset;
@end
