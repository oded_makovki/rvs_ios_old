//
//  UIColor+RVSApplication.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "UIColor+RVSApplication.h"
#import "UIColor+HexString.h"

@implementation UIColor (RVSApplication)

+(UIColor *)rvsTintColor
{
    return [UIColor colorWithRGBValue:0x7b2b75];
    //[UIColor colorWithRed:64.0/255 green:138.0/255 blue:190.0/255 alpha:1];
    //[UIColor colorWithRGBValue:0x0072EF];
}

+(UIColor *)rvsBackgroundColor
{
    return [UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:233.0/255.0 alpha:1.0];
    //[UIColor colorWithRGBValue:0xd1d1d1];
}

+(UIColor *)rvsFooterTextColor
{
    return [UIColor darkGrayColor];
}

//cc
+ (UIColor *)rvsTranscriptUnselectedColor
{
    return [UIColor colorWithWhite:1 alpha:0.3];
}

+ (UIColor *)rvsTranscriptSelectedColor
{
    return [UIColor whiteColor];
}

//posts
+(UIColor *)rvsPostLinkColor
{
    return [UIColor rvsTintColor]; //[UIColor colorWithRed:64.0/255 green:138.0/255 blue:190.0/255 alpha:1];
}

+(UIColor *)rvsPostTextColor
{
    return [UIColor colorWithRGBValue:0x25292A]; //0xB5CCE6
}

+(UIColor *)rvsPostUserNameColor
{
    return [self rvsTintColor]; //[UIColor colorWithRGBValue:0x25292A]; //0xE4F0FE
}

+(UIColor *)rvsPostUserHandleColor
{
    return [UIColor colorWithRGBValue:0x8ea2b1]; //0x8EA2B1
}

+(UIColor *)rvsPostTimestampColor
{
    return [UIColor colorWithRGBValue:0x8ea2b1]; //0x8EA2B1
}

#pragma mark - Reply

+(UIColor *)rvsReplyLinkColor
{
    return [self rvsPostLinkColor];
}

+(UIColor *)rvsReplyTextColor
{
    return [UIColor colorWithRGBValue:0xB5CCE6]; //
}

+(UIColor *)rvsReplyUserNameColor
{
    return [UIColor colorWithRGBValue:0xE4F0FE]; //
}

+(UIColor *)rvsReplyUserHandleColor
{
    return [UIColor colorWithRGBValue:0x8EA2B1]; //
}

+(UIColor *)rvsReplyTimestampColor
{
    return [UIColor colorWithRGBValue:0x8EA2B1]; //
}

+(UIColor *)rvsMomentLinkColor
{
    return [self rvsPostLinkColor];
}

+(UIColor *)rvsMomentTextColor
{
    return [self rvsPostTextColor];
}

+(UIColor *)rvsMomentTopPostsColor
{
    return [UIColor whiteColor];
}

+(UIColor *)rvsMomentTimestampColor
{
    return [self rvsPostTimestampColor];
}

+(UIColor *)rvsMomentUserHandleColor
{
    return [self rvsPostUserHandleColor];
}

+(UIColor *)rvsMomentUserNameColor
{
    return [self rvsPostUserHandleColor];
}

#pragma mark - User

+(UIColor *)rvsUserHandleColor
{
    return [UIColor colorWithRGBValue:0xB5CCE6]; //
}

+(UIColor *)rvsUserNameColor
{
    return [UIColor colorWithRGBValue:0xE4F0FE]; //
}

#pragma mark - Program

+(UIColor *)rvsProgramTimestampColor
{
    return [UIColor colorWithRGBValue:0xababab];
}

+(UIColor *)rvsProgramTextColor
{
    return [UIColor whiteColor];
}

@end
