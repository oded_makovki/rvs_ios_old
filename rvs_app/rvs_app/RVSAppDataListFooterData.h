//
//  RVSAppDataListFooterData.h
//  rvs_app
//
//  Created by Barak Harel on 1/5/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RVSAppDataListFooterType)
{
    RVSAppDataListFooterTypeDefault,
    RVSAppDataListFooterTypeLoading,
    RVSAppDataListFooterTypeEndOfList
};

@interface RVSAppDataListFooterData : NSObject
@property (nonatomic) RVSAppDataListFooterType type;
@end
