//
//  RVSAppTabViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTabNavigationController.h"

@interface RVSAppTabNavigationController ()
@end

@implementation RVSAppTabNavigationController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
