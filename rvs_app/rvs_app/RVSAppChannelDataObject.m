//
//  RVSAppChannelDataObject.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDataObject.h"

@implementation RVSAppChannelDataObject


-(id)init
{
    self = [super init];
    if (self)
    {
        self.channel = nil;
        self.channelViewDelegate = nil;
    }
    return self;
}

-(id)initWithChannel:(NSObject<RVSChannel> *)channel channelViewDelegate:(id<RVSAppChannelViewDelegate>)channelViewDelegate
{
    self = [self init];
    if (self)
    {
        self.channel = channel;
        self.channelViewDelegate = channelViewDelegate;
    }
    
    return self;
}

+(RVSAppChannelDataObject *)dataObjectsFromChannel:(NSObject<RVSChannel> *)channel channelViewDelegate:(id<RVSAppChannelViewDelegate>)channelViewDelegate
{
    RVSAppChannelDataObject *data = [[RVSAppChannelDataObject alloc] initWithChannel:channel channelViewDelegate:channelViewDelegate];
    
    return data;
}

+(NSArray *)dataObjectsFromChannels:(NSArray *)channels channelViewDelegate:(id<RVSAppChannelViewDelegate>)channelViewDelegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:channels.count];
    for (NSObject <RVSChannel> *channel in channels)
    {
        [dataArray addObject:[self dataObjectsFromChannel:channel channelViewDelegate:channelViewDelegate]];
    }
    
    return dataArray;
}



@end
