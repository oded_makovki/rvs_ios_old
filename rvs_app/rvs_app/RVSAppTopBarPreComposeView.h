//
//  RVSAppTopBarPreComposeView.h
//  rvs_app
//
//  Created by Barak Harel on 1/19/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppChannelView.h"
#import "UIView+NibLoading.h"

@interface RVSAppTopBarPreComposeView : NibLoadedView
@property (weak, nonatomic) IBOutlet RVSAppChannelView *channelView;
@property (weak, nonatomic) IBOutlet UILabel *nowWatchingLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeChannelLabel;

@property (weak, nonatomic) IBOutlet UIButton *changeChannelButton;
@property (weak, nonatomic) IBOutlet UIButton *showComposeButton;

@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UILabel *loadingChannelsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *currentChannelImageView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *currentChannelImageSpinner;

- (void)refresh;
- (void)startCurrentChannelTimer;
- (void)stopCurrentChannelTimer;
@end
