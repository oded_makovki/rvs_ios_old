//
//  RVSAppLoadingPostCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppDataListFooterData.h"

@interface RVSAppDataListFooterCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet UILabel *msgLabel; //needs to be init or set
@property (nonatomic) RVSAppDataListFooterData *data;

-(void)reset;
-(void)setText:(NSString *)text forType:(RVSAppDataListFooterType)type;

@end
