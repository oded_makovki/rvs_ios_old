//
//  RVSAppProgramDataCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppProgramDataObject.h"

@interface RVSAppProgramDataCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppProgramView *programView;
@property (nonatomic) RVSAppProgramDataObject *data;

-(void)reset;
@end
