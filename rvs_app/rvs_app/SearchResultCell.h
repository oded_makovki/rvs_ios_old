//
//  SearchResultCell.h
//  InnovaTV
//
//  Created by Barak Harel on 1/26/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchResult.h"

@interface SearchResultCell : UICollectionViewCell
@property (nonatomic) SearchResult *data;
@end
