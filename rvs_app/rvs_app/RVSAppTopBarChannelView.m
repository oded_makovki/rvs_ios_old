//
//  RVSAppTopBarChannelView.m
//  rvs_app
//
//  Created by Barak Harel on 11/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTopBarChannelView.h"

@implementation RVSAppTopBarChannelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setChannel:(NSObject <RVSChannel> *)channel
{
    [super setChannel:channel];
    
    NSString *format = NSLocalizedString(@"channel name format", nil);
    self.channelLabel.text = [NSString stringWithFormat:format, self.channelLabel.text];
}

@end
