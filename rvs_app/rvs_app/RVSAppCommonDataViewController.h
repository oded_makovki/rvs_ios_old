//
//  RVSAppCommonDataViewController.h
//  rvs_app
//
//  Created by Barak Harel on 1/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"
#import "RVSAppDataListView.h"

@interface RVSAppCommonDataViewController : RVSAppCommonViewController
@property (weak, nonatomic) IBOutlet RVSAppDataListView *dataListView;

@end
