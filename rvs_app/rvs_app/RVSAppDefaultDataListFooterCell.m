//
//  RVSAppDefaultDataListFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDefaultDataListFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppDefaultDataListFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppDefaultDataListFooterCell];
    }
    return self;
}

-(void)initRVSAppDefaultDataListFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 40);
}

@end
