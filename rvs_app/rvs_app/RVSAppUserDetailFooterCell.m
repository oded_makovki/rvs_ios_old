//
//  RVSAppProfilePostFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDetailFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppUserDetailFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppProfilePostFooterCell];
    }
    return self;
}

-(void)initRVSAppProfilePostFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    [self setText:NSLocalizedString(@"user detail footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"user detail footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"user detail footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeZero; //force auto size
}

@end
