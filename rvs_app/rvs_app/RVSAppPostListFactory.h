//
//  RVSAppPostListFactory.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppPostListFactoryType)
{
    RVSAppPostListFactoryTypeMyFeed,
    RVSAppPostListFactoryTypeMyMentions,
    RVSAppPostListFactoryTypeFavoritesOfUser,
    RVSAppPostListFactoryTypeUser,
    RVSAppPostListFactoryTypeMoment
};

@interface RVSAppPostListFactory : NSObject
@property (nonatomic, readonly) RVSAppPostListFactoryType type;

-(id)initWithUser:(NSString *)userId;
-(id)initWithMyFeed;
-(id)initWithMyMentions;
-(id)initWithMyFavoritesOfUser:(NSString *)userId;
-(id)initWithMoment:(NSString *)momentId;

-(NSObject<RVSAsyncPostList> *)postList;
@end
