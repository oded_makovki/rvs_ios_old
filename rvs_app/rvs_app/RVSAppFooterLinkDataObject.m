//
//  RVSAppFooterLinkDataObject.m
//  rvs_app
//
//  Created by Barak Harel on 1/6/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppFooterLinkDataObject.h"

@interface RVSAppFooterLinkDataObject()
@end

@implementation RVSAppFooterLinkDataObject

-(id)initWithChannel:(NSObject<RVSChannel> *)channel
{
    self = [super init];
    if (self)
    {
        _type = RVSAppFooterLinkTypeChannel;
        _channel = channel;
    }
    
    return self;
}

-(id)initWithProgram:(NSObject<RVSProgram> *)program
{
    self = [super init];
    if (self)
    {
        _type = RVSAppFooterLinkTypeProgram;
        _program = program;
    }
    
    return self;
}

-(id)initWithUsersOfMoment:(NSObject<RVSMoment> *)moment
{
    self = [super init];
    if (self)
    {
        _type = RVSAppFooterLinkTypeMomentUsers;
        _moment = moment;
    }
    
    return self;
}

@end
