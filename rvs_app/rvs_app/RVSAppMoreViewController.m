//
//  RVSAppMoreViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppMoreViewController.h"
#import "UIColor+RVSApplication.h"
#import "RVSApplication.h"

@interface RVSAppMoreViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonSettings;

@end

@implementation RVSAppMoreViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = NSLocalizedString(@"more title", nil);
    
    self.buttonSettings.tintColor = [UIColor rvsTintColor];
    
    NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (!version)
        version = @"0.0.0";
    
    self.versionLabel.text = [NSString stringWithFormat:@"Alpha 1 v%@", version];
}

-(NSArray *)navigationRightButtons
{
    return @[self.buttonSettings];
}

@end
