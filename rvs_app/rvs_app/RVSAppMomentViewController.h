//
//  RVSAppMomentViewController.h
//  rvs_app
//
//  Created by Barak Harel on 12/18/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableMomentListViewController.h"
#import "RVSAppPagesViewController.h"

@interface RVSAppMomentViewController : RVSAppPushableMomentListViewController <RVSAppContainedPageViewController>

@end
