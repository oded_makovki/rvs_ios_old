//
//  HomeViewController.m
//  InnovaTV
//
//  Created by Barak Harel on 1/27/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "SearchViewController.h"
#import "SearchResultsController.h"
#import <AFNetworking.h>
#import "UIColor+RVSApplication.h"
#import "RVSApplication.h"
#import "RVSAppTopBarToggleButtonInnerView.h"
#import <rvs_sdk_api helpers.h>


@interface SearchViewController () <UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic) UIBarButtonItem *navBrowseButton;

@property (weak, nonatomic) IBOutlet UITableView *autocompleteTableView;
@property (nonatomic) AFHTTPRequestOperation *autocompleteRequest;
@property (nonatomic) NSMutableArray *autocompleteData;

@property (nonatomic) UIBarButtonItem *topBarToggleButton;
@property (nonatomic) RVSAppTopBarToggleButtonInnerView *topBarToggleButtonInnerView;

@property (nonatomic) RVSNotificationObserver *channelDetectorMatchObserver;
@property (nonatomic) RVSNotificationObserver *channelDetectorStatusObserver;
@property (nonatomic) RVSNotificationObserver *topBarDidChangeObserver;
@property (nonatomic) RVSNotificationObserver *channelsDidChangeObserver;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@property (nonatomic) IBOutlet UIView *contentView;
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.hidesBottomBarWhenPushed = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = NSLocalizedString(@"search title", nil);
    
    [[UITextField appearance] setKeyboardAppearance:UIKeyboardAppearanceDark];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor blackColor]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setFont:[UIFont fontWithName:@"Helvetica" size:14]];

    self.searchBar.tintColor = [UIColor rvsTintColor];
    self.searchBar.delegate = self;
    
    self.view.backgroundColor = [UIColor rvsBackgroundColor];
    
    [self hideAutocompleteResults];
    self.autocompleteTableView.delegate = self;
    self.autocompleteTableView.dataSource = self;
    self.autocompleteTableView.backgroundColor = [UIColor rvsBackgroundColor];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    __weak SearchViewController *weakSelf = self;
    
    self.channelDetectorMatchObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorMatchNotification:notification];
    }];
    
    self.channelDetectorStatusObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorStatusNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorStatusNotification:notification];
    }];
    
    self.topBarDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationTopBarDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {                                            [weakSelf topBarUpdatedAnimated:YES];
    }];
    
    self.channelsDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationChannelsDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf channelsDidChange:notification];
    }];
    
    [self channelsDidChange:nil];
}

-(NSArray *)navigationRightButtons
{
    return @[self.topBarToggleButton];
}

-(void)channelsDidChange:(NSNotification *)notification
{
    if ([[RVSApplication application].channelsList count] > 0)
    {
        self.topBarToggleButton.enabled = YES;
        self.topBarToggleButtonInnerView.selected = YES;
    }
    else
    {
        self.topBarToggleButton.enabled = NO;
        self.topBarToggleButtonInnerView.selected = NO;
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.searchBar becomeFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self topBarUpdatedAnimated:NO];
}

#pragma mark - Search

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length > 0)
    {
        [self showAutocompleteWithFragment:searchText];
    }
    else
    {
        if (self.autocompleteRequest)
            [self.autocompleteRequest cancel];
        
        [self hideAutocompleteResults];
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"Search: %@", self.searchBar.text);
    [self showSearchWithQuery:searchBar.text];
}

-(void)showSearchWithQuery:(NSString *)query
{
    if (self.autocompleteRequest)
        [self.autocompleteRequest cancel];
    
    [self.searchBar resignFirstResponder];
    
    SearchResultsController *searchController = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"SearchResultsController"];
    searchController.query = query;
    [self.navigationController pushViewController:searchController animated:YES];
}

#pragma mark - Autocomplete

-(void)showAutocompleteWithFragment:(NSString *)fragment
{
    
    if (self.autocompleteRequest)
        [self.autocompleteRequest cancel];
    
    NSString *apiuri = @"https://api-staging.boxfish.com/v4/autocomplete/mentions/?";
    NSString *token = @"eyJjIjo2MzUxNjEzNjk5MTA0MjgwMDAsInMiOiJJOF9GOGdCOUxCMTBaYUpDaHBQY3lXVzUxd2MiLCJpZCI6IlNPQ0RQSDdGZTBDMEhHVHV2UUY5TUEiLCJuIjoid2ZIcUI1REh2TVNBVEEiLCJyIjoid0c2RG8xZlcxaHp3aGciLCJrIjoiT0F1dGgifQ";
    NSString *limit = @"10";
    
    NSString * requestUrl = [NSString stringWithFormat:@"%@fragment=%@&token=%@&limit=%@",apiuri, [fragment stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], token, limit];
    
    __weak SearchViewController *weakSelf = self;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    self.autocompleteRequest = [manager GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [weakSelf handleAutocompleteResponse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        NSLog(@"Error: %@", error);
        
    }];
}

-(void)hideAutocompleteResults
{
    NSLog(@"******* CLEAR *******");
    [UIView animateWithDuration:0.3 animations:^{
            self.autocompleteTableView.alpha = 0;
    }];
}

-(void)handleAutocompleteResponse:(id)responseObject
{
    //    NSLog(@"JSON: %@", responseObject);
    if ([responseObject isKindOfClass:[NSDictionary class]])
    {
        [self parseAutocompleteResults:@[responseObject]];
    }
    else if ([responseObject isKindOfClass:[NSArray class]])
    {
        [self parseAutocompleteResults:responseObject];
    }
}

-(void)parseAutocompleteResults:(NSArray *)results
{
    NSLog(@"Got %lu results...", (unsigned long)[results count]);
    
    self.autocompleteData = [NSMutableArray arrayWithCapacity:[results count]];
    
    for (NSDictionary *result in results)
    {
        NSString *mention = result[@"mention"];
        if (mention && mention.length > 0)
        {
            NSLog(@"found mention: %@", mention);
            [self.autocompleteData addObject:mention];
        }
    }
    
    self.autocompleteTableView.alpha = 1;
    [self.autocompleteTableView setContentOffset:CGPointZero animated:YES];
    [self.autocompleteTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Other

- (IBAction)showMosaic:(id)sender
{
    UINavigationController *mosaicController = [self.storyboard instantiateViewControllerWithIdentifier:@"MosaicNavigationController"];
//    mosaicController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:mosaicController animated:YES completion:nil];

}

-(UIBarButtonItem *)navBrowseButton
{
    if (!_navBrowseButton)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 32, 28)];
        button.backgroundColor = [UIColor colorWithWhite:1 alpha:0.9];
        button.layer.cornerRadius = 2.0;
        [button setImage:[UIImage imageNamed:@"metrix_icon"] forState:UIControlStateNormal];
        
        [button addTarget:self action:@selector(showMosaic:) forControlEvents:UIControlEventTouchUpInside];
        
        _navBrowseButton = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    
    return _navBrowseButton;
}


-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.autocompleteData count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.textLabel.text = self.autocompleteData[indexPath.row];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *query = self.autocompleteData[indexPath.row];
    [self showSearchWithQuery:query];
}

#pragma mark - Ketboard

- (void)keyboardDidShow:(NSNotification*)notification
{
    NSDictionary *info = [notification userInfo];

    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    
    CGRect frame = self.autocompleteTableView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y - height;
    self.autocompleteTableView.frame = frame;
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    CGRect frame = self.autocompleteTableView.frame;
    frame.size.height = self.view.frame.size.height - frame.origin.y;
    self.autocompleteTableView.frame = frame;
}


#pragma mark - Top bar

-(UIBarButtonItem *)topBarToggleButton
{
    if (!_topBarToggleButton)
    {
        self.topBarToggleButtonInnerView = [[RVSAppTopBarToggleButtonInnerView alloc] initWithFrame:CGRectMake(0, 0, 32, 32)];
        
        [self.topBarToggleButtonInnerView addTarget:self action:@selector(toggleTopBar) forControlEvents:UIControlEventTouchUpInside];
        
        _topBarToggleButton = [[UIBarButtonItem alloc] initWithCustomView:self.topBarToggleButtonInnerView];
    }
    
    return _topBarToggleButton;
}

-(void)toggleTopBar
{
    [[RVSApplication application] showCompose];
}

-(void)topBarUpdatedAnimated:(BOOL)animated
{    
    [self setContentTopConstraintconstant:[RVSApplication application].topBar.barHeight animated:animated];
}

-(void)setContentTopConstraintconstant:(CGFloat)constant animated:(BOOL)animated
{
    self.contentTopConstraint.constant = constant;
    
    if (animated)
    {
        [UIView animateWithDuration:0.2f animations:^{
            [self.contentView layoutIfNeeded];
        }];
    }
}

-(void)handleChannelDetectorMatchNotification:(NSNotification *)notification
{
    NSObject <RVSChannel> *channel = notification.userInfo[RVSSdkChannelDetectorChannelUserInfoKey];
    if (channel)
    {
        [self.topBarToggleButtonInnerView showMatchAnimation];        
    }
}

-(void)handleChannelDetectorStatusNotification:(NSNotification *)notification
{
    RVSSdkChannelDetectorStatus status = [notification.userInfo[RVSSdkChannelDetectorStatusUserInfoKey] integerValue];
    
    switch (status)
    {
        case RVSSdkChannelDetectorStatusDetecting:
            [self.topBarToggleButtonInnerView showDetectingAnimation];
            break;
            
        default:
            break;
    }
}

@end
