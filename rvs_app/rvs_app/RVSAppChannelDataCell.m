//
//  RVSAppUserDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDataCell.h"

@implementation RVSAppChannelDataCell


-(void)setData:(RVSAppChannelDataObject *)data
{
    [super setData:data];
    self.channelView.channel = data.channel;
    self.channelView.delegate = data.channelViewDelegate;
}

-(void)activate
{
    //TBD
}

-(void)reset
{
    [self.channelView reset];
}

@end
