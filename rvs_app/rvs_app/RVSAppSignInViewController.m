//
//  RVSAppLoginViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSignInViewController.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

@interface RVSAppSignInViewController () <UITextFieldDelegate>
@property (nonatomic) RVSNotificationObserver *signOutObserver;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end

@implementation RVSAppSignInViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [self.backgroundView addGestureRecognizer:tap];
    
    __weak RVSAppSignInViewController *weakSelf = self;
    
    self.signOutObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedOutNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification)
    {
        NSError *error = notification.userInfo[RVSServiceSignedOutErrorUserInfoKey];
        [weakSelf signInFailedWithError:error];
    }];
    
    self.usernameTextField.text = [RVSSdk sharedSdk].myUserId;
    self.usernameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    
    [self.activityIndicator stopAnimating];
    [self.signInButton setTitle:NSLocalizedString(@"button log in", nil) forState:UIControlStateNormal];
    self.signInButton.enabled = [self canSignIn];
}

-(void)closeKeyboard
{
    [self.view endEditing:NO];
}

-(void)signInFailedWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"login error title", nil)
                                                        message:[NSString stringWithFormat:NSLocalizedString(@"login error code", nil), error.code]
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"alert ok", nil)
                                              otherButtonTitles:nil];
    
    
    if (error.domain == RVSErrorDomain && error.code == RVSErrorCodeInvalidCredentials)
    {
        alertView.message = NSLocalizedString(@"login error credentials", nil);
    }
    
    [alertView show];
    
    [self.activityIndicator stopAnimating];
    [self.signInButton setTitle:NSLocalizedString(@"button log in", nil) forState:UIControlStateNormal];
    self.signInButton.enabled = [self canSignIn];
}

- (BOOL)canSignIn
{
    return ![self.usernameTextField.text isEmpty] && ![self.passwordTextField.text isEmpty];
}

- (IBAction)signIn:(id)sender
{
    [self.signInButton setTitle:NSLocalizedString(@"button logging in", nil) forState:UIControlStateNormal];
    [self.activityIndicator startAnimating];
    self.signInButton.enabled = NO;
    
    [[RVSSdk sharedSdk] signIn:self.usernameTextField.text password:self.passwordTextField.text];
}

- (IBAction)editingChanged:(UITextField *)sender
{
     self.signInButton.enabled = [self canSignIn];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.usernameTextField)
    {
        [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
    }
    else if ([self canSignIn])
    {
        [textField resignFirstResponder];
        [self signIn:nil];
    }
    
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
