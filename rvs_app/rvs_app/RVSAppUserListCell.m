//
//  RVSAppUserListCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserListCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@implementation RVSAppUserListCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppUserListCell];
    }
    return self;
}

-(void)initRVSAppUserListCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    self.userView.userThumbnail.layer.cornerRadius = 4;
    self.userView.userThumbnail.layer.masksToBounds = YES;
    
    self.userView.userNameLabel.textColor = [UIColor rvsPostUserNameColor];
    
    self.userView.postsCountLabel.text = NSLocalizedString(@"user detail posts count title", nil);
    self.userView.followersCountLabel.text = NSLocalizedString(@"user detail followers count title", nil);
    self.userView.followingCountLabel.text = NSLocalizedString(@"user detail following count title", nil);
    
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 47);
}

@end
