//
//  RVSAppClipPlayerView_Private.h
//  rvs_app
//
//  Created by Barak Harel on 12/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppClipPlayerView.h"

@interface RVSAppClipPlayerView ()
@property (nonatomic) BOOL isPlaying;
@property (nonatomic) BOOL isLoading;
@property (nonatomic) RVSAppClipPlayerViewState playerState;

@property (nonatomic) RVSClipView *clipView;
@property (nonatomic) UIImageView *imageView;
@property (nonatomic) UIActivityIndicatorView *spinner;
@property (nonatomic) UIView *playOverlay;
@property (nonatomic) UIView *tapView;

@property (nonatomic) CGRect fullFrame;

-(void)playerStateWillChangeToState:(RVSAppClipPlayerViewState)newState oldState:(RVSAppClipPlayerViewState)oldState;
-(void)playerStateDidChangeToState:(RVSAppClipPlayerViewState)newState oldState:(RVSAppClipPlayerViewState)oldState;

@end
