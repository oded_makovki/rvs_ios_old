//
//  RVSAppMomentDataCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppDataCell.h"
#import "RVSAppMomentView.h"
#import "RVSAppMomentDataObject.h"

@interface RVSAppMomentDataCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppMomentView *momentView; //needs to be init or set
@property (nonatomic) RVSAppMomentDataObject *data;

-(void)reset;
@end
