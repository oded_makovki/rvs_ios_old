//
//  RVSAppPortraitNavigationController.h
//  rvs_app
//
//  Created by Barak Harel on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppStyledNavigationController.h"

@interface RVSAppPortraitNavigationController : RVSAppStyledNavigationController

@end
