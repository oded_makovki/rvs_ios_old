//
//  HomeViewController.h
//  InnovaTV
//
//  Created by Barak Harel on 1/27/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppCommonViewController.h"

@interface SearchViewController : RVSAppCommonViewController
@end
