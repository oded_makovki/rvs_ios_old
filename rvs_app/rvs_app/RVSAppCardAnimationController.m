//
//  RVSAppCardAnimationController.m
//

#import "RVSAppCardAnimationController.h"

@implementation RVSAppCardAnimationController

- (id)init {
    if (self = [super init]) {
        self.duration = 0.5f;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return self.duration;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *toView = toVC.view;
    UIView *fromView = fromVC.view;
    
    [self animateTransition:transitionContext fromVC:fromVC toVC:toVC fromView:fromView toView:toView];
}


- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    if(self.reverse){
        [self executeReverseAnimation:transitionContext fromVC:fromVC toVC:toVC fromView:fromView toView:toView];
    } else {
        [self executeForwardsAnimation:transitionContext fromVC:fromVC toVC:toVC fromView:fromView toView:toView];
    }
    
}

-(void)executeForwardsAnimation:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    UIView* containerView = [transitionContext containerView];
    
    // positions the to- view off the bottom of the sceen
    CGRect frame = [transitionContext initialFrameForViewController:fromVC];
    CGRect offScreenFrame = frame;
    
    offScreenFrame.origin.x = offScreenFrame.size.width;
    toView.frame = offScreenFrame;
    
    [containerView insertSubview:toView aboveSubview:fromView];
    
    CATransform3D t = CATransform3DIdentity;
    t = CATransform3DScale(t, 0.95, 0.95, 1);
    
    [UIView animateKeyframesWithDuration:self.duration delay:0.0 options:0 animations:^{
        
        [UIView addKeyframeWithRelativeStartTime:0.0f relativeDuration:0.3f animations:^{
            fromView.layer.transform = t;
            fromView.alpha = 0.6;
        }];
        
        [UIView addKeyframeWithRelativeStartTime:0.2f relativeDuration:0.8f animations:^{
            toView.frame = frame;
        }];

    } completion:^(BOOL finished) {
        fromView.alpha = 1;
        fromView.layer.transform = CATransform3DIdentity;
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
    
    
}

-(void)executeReverseAnimation:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    UIView* containerView = [transitionContext containerView];
    
    // positions the to- view behind the from- view
    CGRect frame = [transitionContext initialFrameForViewController:fromVC];
    toView.frame = frame;
    
    CATransform3D t = CATransform3DIdentity;
    toView.layer.transform = CATransform3DScale(t, 0.95, 0.95, 1);
    toView.alpha = 0.6;
    
    [containerView insertSubview:toView belowSubview:fromView];
    
    CGRect frameOffScreen = frame;
    frameOffScreen.origin.x = frame.size.width;
    
    [UIView animateKeyframesWithDuration:self.duration delay:0 options:0 animations:^{

        // push the from- view off the bottom of the screen
        [UIView addKeyframeWithRelativeStartTime:0.0f relativeDuration:0.5f animations:^{
            fromView.frame = frameOffScreen;
        }];
        
        // animate the to- view into place
        [UIView addKeyframeWithRelativeStartTime:0.2f relativeDuration:0.5f animations:^{
            toView.layer.transform = CATransform3DIdentity;
            toView.alpha = 1.0;
        }];
        
    } completion:^(BOOL finished) {
        toView.layer.transform = CATransform3DIdentity;
        toView.alpha = 1.0;
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}


@end
