//
//  NSDate+RVSApplication.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "NSDate+RVSApplication.h"

#define MAX_SECONDS_CONSIDERED_AS_NOW 10

@implementation NSDate (RVSApplication)

-(NSString *)rvsTimeStringSinceDate:(NSDate *)anotherDate
{
    NSInteger seconds = [self timeIntervalSinceDate:anotherDate];
    BOOL isPastTime = NO;
    if (seconds < 0)
    {
        isPastTime = YES;
        seconds = fabs(seconds);
    }
    
    NSInteger weeks = seconds / (3600 * 24 * 7);
    NSInteger days = seconds / (3600 * 24);
    NSInteger hours = seconds / (3600);
    NSInteger minutes = seconds / 60;
    
    NSString *component1;
    NSInteger value1 = 0;
    NSString *component2;
    NSInteger value2 = 0;

    
    if (weeks > 0)
    {
        component1 = @"w";
        component2 = @"d";
        
        value1 = weeks;
        value2 = days % 7;
    }
    else if (days > 0)
    {
        component1 = @"d";
        component2 = @"h";
        
        value1 = days;
        value2 = hours % 24;
    }
    else if (hours > 0)
    {
        component1 = @"h";
        component2 = @"m";
        
        value1 = hours;
        value2 = minutes % 60;
    }
    else if (minutes > 0)
    {
        component1 = @"m";
        component2 = @"s";
        
        value1 = minutes;
        value2 = seconds % 60;
    }
    else //seconds
    {
        component1 = @"s";
        component2 = nil;
        
        value1 = seconds;
        value2 = 0;
    }
    
    //    //plural?
    //    if (value1 > 1)
    //        component1 = [component1 stringByAppendingString:@"s"];
    //    if (value2 > 1)
    //        component2 = [component2 stringByAppendingString:@"s"];
    
    if (component2 && value2 > 0)
    {
        return [NSString stringWithFormat:@"Posted %d%@%d%@%@", value1, component1, value2, component2, isPastTime ? @" ago":@""];
    }
    else
    {
        return [NSString stringWithFormat:@"Posted %d%@%@", value1, component1, isPastTime ? @" ago":@""];
    }
}

-(NSString *)timeStringSinceDate:(NSDate *)anotherDate isShortString:(BOOL)isShortString
{
    NSTimeInterval seconds = [self timeIntervalSinceDate:anotherDate];
    BOOL isPastTime = NO;
    if (seconds < 0)
    {
        isPastTime = YES;
        seconds = fabs(seconds);
    }
    
    NSInteger weeks = seconds / (3600 * 24 * 7);
    NSInteger days = seconds / (3600 * 24);
    NSInteger hours = seconds / (3600);
    NSInteger minutes = seconds / 60;
    
    NSString *component;
    NSString *format;
    NSInteger value = 0;
    
    if (seconds <= MAX_SECONDS_CONSIDERED_AS_NOW)
    {
        return [self stringForNowUsingShortString:isShortString];
    }
    else if (weeks > 2)
    {
        if (weeks == 1)
        {
            component = @"week";
        }
        else
        {
            component = @"weeks";
        }
        
        value = weeks;
    }
    else if (days > 2)
    {
        if (days == 1)
        {
            component = @"day";
        }
        else
        {
            component = @"days";
        }
        
        value = days;
    }
    else if (hours > 2)
    {
        if (hours == 1)
        {
            component = @"hour";
        }
        else
        {
            component = @"hours";
        }
        
        value = hours;
    }
    else if (minutes > 2)
    {
        if (minutes == 1)
        {
            component = @"minute";
        }
        else
        {
            component = @"minutes";
        }
        
        value = minutes;
    }
    else if (seconds > 0)
    {
        if (seconds == 1)
        {
            component = @"second";
        }
        else
        {
            component = @"seconds";
        }
        
        value = seconds;
    }
    
    format = [self formatForComponent:component isPastTime:isPastTime isShortString:isShortString];
    return [NSString stringWithFormat:format, value];
}

-(NSString *)stringForNowUsingShortString:(BOOL)isShortString
{
    NSString *l = (isShortString ? @"short" : @"long");
    NSString *string = [NSString stringWithFormat:@"time format now %@", l];
    
    return NSLocalizedString(string ,nil);
}


-(NSString *)formatForComponent:(NSString *)component isPastTime:(BOOL)isPastTime isShortString:(BOOL)isShortString
{
//    "days long time past format"
    NSString *t = (isPastTime ? @"past" : @"future");
    NSString *l = (isShortString ? @"short" : @"long");
    
    NSString *format = [NSString stringWithFormat:@"time format %@ %@ %@", component, t, l];
    return NSLocalizedString(format, nil);
}

-(NSString *)timeShortStringSinceNow
{
    return [self timeShortStringSinceDate:[NSDate date]];
}

-(NSString *)timeShortStringSinceDate:(NSDate *)anotherDate
{
    return [self timeStringSinceDate:anotherDate isShortString:YES];
}

-(NSString *)rvsTimeLongStringSinceNow
{
    return [self rvsTimeStringSinceDate:[NSDate date]];
}

-(NSString *)timeLongStringSinceNow
{
    return [self timeLongStringSinceDate:[NSDate date]];
}

-(NSString *)timeLongStringSinceDate:(NSDate *)anotherDate
{
    return [self timeStringSinceDate:anotherDate isShortString:NO];
}

@end
