//
//  RVSAppTabViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppPortraitNavigationController.h"

@class RVSAppTabNavigationController;

@interface RVSAppTabNavigationController : RVSAppPortraitNavigationController
@end
