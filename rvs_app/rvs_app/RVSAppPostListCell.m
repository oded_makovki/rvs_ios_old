//
//  RVSAppPostListCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostListCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppPostListCell() <RVSAppClipPlayerViewDelegate>
@end

@implementation RVSAppPostListCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostListCell];
    }
    
    return self;
}

-(void)initRVSAppPostListCell
{
    [self loadContentsFromNib];
    
//    self.postView.layer.cornerRadius = 12;
//    self.postView.layer.masksToBounds = YES;
    
    self.postView.shouldParseHotwords = NO;
    self.postView.isUsingLongTimeFormat = YES;
    self.postView.clipPlayerView.delegate = self;
    
    self.postView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.postView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.postView.programNameLabel.textColor = [UIColor rvsProgramTextColor];
    
    self.postView.textLabel.preferredMaxLayoutWidth = 215;
    self.postView.timeLabel.preferredMaxLayoutWidth = 240;
}

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSAppClipPlayerViewState)newState
{
//    NSLog(@"RVSAppClipPlayerViewState: %d", (int)newState);
}


@end
