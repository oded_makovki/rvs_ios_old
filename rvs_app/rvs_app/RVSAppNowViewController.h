//
//  RVSAppNowViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableMomentListViewController.h"
#import "RVSAppPagesViewController.h"

@interface RVSAppNowViewController : RVSAppPushableMomentListViewController <RVSAppContainedPageViewController>

@end
