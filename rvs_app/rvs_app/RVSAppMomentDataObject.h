//
//  RVSAppMomentDataObject.h
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>
#import "RVSAppMomentView.h"

@interface RVSAppMomentDataObject : NSObject
@property (weak, nonatomic) id <RVSAppMomentViewDelegate> momentViewDelegate;
@property (strong, nonatomic) NSObject <RVSMoment> *moment;

-(id)initWithMoment:(NSObject <RVSMoment> *)moment momentViewDelegate:(id <RVSAppMomentViewDelegate>)momentViewDelegate;

#pragma mark - Helpers

+(NSArray *)dataObjectsFromMoments:(NSArray *)moments
                momentViewDelegate:(id<RVSAppMomentViewDelegate>)momentViewDelegate;

+(RVSAppMomentDataObject *)dataObjectFromMoment:(NSObject <RVSMoment> *)moment
                           momentViewDelegate:(id<RVSAppMomentViewDelegate>)momentViewDelegate;

@end
