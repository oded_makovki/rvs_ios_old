//
//  RVSAppPushablePostListViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushablePostListViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>
#import "RVSAppPostListCell.h"
#import "RVSAppPostListFooterCell.h"
#import "RVSApplication.h"

#import "RVSAppPostDataObject.h"

@interface RVSAppPushablePostListViewController () <RVSAppDataListViewDelegate, RVSAppPostViewDelegate>
@property (nonatomic) NSObject <RVSAsyncPostList> *postList;
@property (nonatomic) BOOL shouldClearPostsData;

@property (nonatomic) NSInteger postsSection;

@property (nonatomic) RVSNotificationObserver *followUserDidChangeObserver; //RVSApplicationUserFollowDidChangeNotification
@end

@implementation RVSAppPushablePostListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppPushablePostListViewController];
    }
    return self;
}

-(void)initRVSAppPushablePostListViewController
{
    self.postsClass = [RVSAppPostListCell class];
    self.footerClass = [RVSAppPostListFooterCell class];
}

-(void)setIsMyFeed:(BOOL)isMyFeed
{
    _isMyFeed = isMyFeed;
    if (isMyFeed)
    {
        __weak RVSAppPushablePostListViewController *weakSelf = self;
        
        self.followUserDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationUserFollowDidChangeNotification object:Nil usingBlock:^(NSNotification *notification) {
            [weakSelf reloadPosts];
        }];
    }
    else
    {
        self.followUserDidChangeObserver = nil;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.postsSection = 0;
    self.dataListView.numberOfSections = 1;
    
    if (self.headerClass)
    {
        self.postsSection = 1;
        self.dataListView.numberOfSections = 2; //just posts
        [self.dataListView setSectionsCellClass:self.headerClass forSection:0];
    }
    
    [self.dataListView setSectionsCellClass:self.postsClass forSection:self.postsSection];
    
    self.dataListView.footerCellClass = self.footerClass;
    self.dataListView.delegate = self;
    
    self.dataListView.nextPageTriggerSection = self.postsSection;
    
    if (self.headerData)
    {
        [self loadHeader];
    }
    
    //already set - load data
    if (self.postListFactory)
        [self loadPosts];
}

-(void)loadHeader
{
    [self.dataListView addData:@[self.headerData] toSection:0];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.dataListView activateIfNeeded];
}


- (void)reloadPosts
{
    self.shouldClearPostsData = YES;
    [self loadPosts];
}

- (void)loadPosts
{
    self.postList = nil;
    self.postList = [self.postListFactory postList];
    
    NSInteger postCount = 10;
    if (self.postListFactory.type == RVSAppPostListFactoryTypeMoment)
        postCount = 30;
    
    self.dataListView.nextPageTriggerDistance = postCount / 2;
    
    __weak RVSAppPushablePostListViewController *weakSelf = self;
    
    [self.postList postsUsingBlock:^(NSArray *posts, NSError *error) {
        
        if (weakSelf.shouldClearPostsData)
        {
            weakSelf.shouldClearPostsData = NO;
            [weakSelf.dataListView clearDataInSection:weakSelf.postsSection];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;

            [weakSelf.dataListView addData:nil toSection:weakSelf.postsSection];
        }
        else
        {
            if (!posts || [posts count] == 0)
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            }
            else
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:weakSelf];
            
            [weakSelf.dataListView addData:[RVSAppPostDataObject dataObjectsFromPosts:posts
                                                                               postViewDelegate:weakSelf]
                                 toSection:weakSelf.postsSection];
        }
        
    } count: postCount];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    [self.postList nextPosts];
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadPosts];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if ([data isKindOfClass:[RVSAppPostDataObject class]])
    {
        [self pushPostDetailViewControlWithPost:[(RVSAppPostDataObject *)data post] title:NSLocalizedString(@"post title", nil)];
    }
}

-(void)networkStatusDidChange
{
    [super networkStatusDidChange];
    if (self.dataListView.error && [RVSApplication application].hasNetworkConnection)
    {
        self.dataListView.error = nil;
        self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault;
        
        [self.dataListView clear];
        [self reloadPosts];
    }
}

-(void)composeDidCompleteWithPost:(NSObject <RVSPost> *)post inReplyToPost:(NSObject <RVSPost> *)replyToPost
{
    if (self.isMyFeed)
    {
        [self reloadPosts];
    }
}

@end
