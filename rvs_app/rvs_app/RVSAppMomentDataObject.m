//
//  RVSAppMomentDataObject.m
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentDataObject.h"

@implementation RVSAppMomentDataObject

-(id)initWithMoment:(NSObject<RVSMoment> *)moment momentViewDelegate:(id<RVSAppMomentViewDelegate>)momentViewDelegate
{
    self = [self init];
    if (self)
    {
        self.momentViewDelegate = momentViewDelegate;
        self.moment = moment;
    }
    
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        self.moment = nil;
        self.momentViewDelegate = nil;
    }
    return self;
}

+(RVSAppMomentDataObject *)dataObjectFromMoment:(NSObject<RVSMoment> *)moment momentViewDelegate:(id<RVSAppMomentViewDelegate>)momentViewDelegate
{
    RVSAppMomentDataObject *data = [[RVSAppMomentDataObject alloc] initWithMoment:moment momentViewDelegate:momentViewDelegate];
    
    return data;
}

+(NSArray *)dataObjectsFromMoments:(NSArray *)moments momentViewDelegate:(id<RVSAppMomentViewDelegate>)momentViewDelegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:moments.count];
    for (NSObject <RVSMoment> *moment in moments)
    {
        [dataArray addObject:[self dataObjectFromMoment:moment momentViewDelegate:momentViewDelegate]];
    }
    
    return dataArray;
}

@end
