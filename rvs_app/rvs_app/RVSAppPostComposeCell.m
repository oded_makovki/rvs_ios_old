//
//  RVSAppComposePostContentCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostComposeCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppPostComposeCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppReplyPostContentCell];
    }
    
    return self;
}

-(void)initRVSAppReplyPostContentCell
{
    [self loadContentsFromNib];
    self.postView.shouldParseHotwords = NO;
    self.backgroundColor = [UIColor clearColor];
    
    self.postView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.postView.programNameLabel.textColor = [UIColor rvsProgramTextColor];
    
    self.postView.textLabel.preferredMaxLayoutWidth = 251;
    self.postView.isUsingLongTimeFormat = YES;
}

@end
