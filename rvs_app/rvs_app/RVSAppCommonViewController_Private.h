//
//  RVSAppCommonViewController_Private.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"

@interface RVSAppCommonViewController () <RVSAppPostViewDelegate, RVSAppMomentViewDelegate, RVSAppUserViewDelegate, RVSAppChannelViewDelegate, RVSAppProgramViewDelegate>

-(void)networkStatusDidChange;
-(void)composeDidCompleteWithPost:(NSObject <RVSPost> *)post inReplyToPost:(NSObject <RVSPost> *)replyToPost;

//post
-(void)pushPostDetailViewControlWithPost:(NSObject <RVSPost> *)post title:(NSString *)title;
-(void)pushPostListViewControlWithPostsByUser:(NSObject <RVSUser> *)user title:(NSString *)title;
-(void)pushPostListViewControlWithFavoritesOfUser:(NSObject <RVSUser> *)user title:(NSString *)title;
-(void)pushPostListViewControlWithPostsOfMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title;

//user
-(void)pushUserDetailViewControlWithCachedUser:(NSObject <RVSUser> *)cachedUser userId:(NSString *)userId title:(NSString *)title;
-(void)pushUserListViewControlWithFollowersOfUserId:(NSString *)userId title:(NSString *)title;
-(void)pushUserListViewControlWithFollowedByUserId:(NSString *)userId title:(NSString *)title;
-(void)pushUserListViewControlWithFavoritesOfMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title;
-(void)pushUserListViewControlWithRepostsOfMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title;


//channel
-(void)pushChannelDetailViewControllerWithChannel:(NSObject <RVSChannel> *)channel title:(NSString *)title;

//program
-(void)pushProgramDetailViewControllerWithProgram:(NSObject <RVSProgram> *)program title:(NSString *)title;

//moment
-(void)pushMomentDetailViewControlWithMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title;
-(void)pushMomentDetailViewControlRelatedToPost:(NSObject <RVSPost> *)post title:(NSString *)title;
-(void)pushMomentListViewControlWithChannel:(NSObject <RVSChannel> *)channel title:(NSString *)title;
-(void)pushMomentListViewControlWithProgram:(NSObject <RVSProgram> *)program title:(NSString *)title;
-(void)pushMomentListViewControlWithTopic:(NSString *)topic title:(NSString *)title;


@end
