//
//  TVXClipFrameCell.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTimelineCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+NibLoading.h"

#define DEF_BACK_IMAGE @"frame_back_lines"

@interface RVSAppTimelineCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation RVSAppTimelineCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        self.clipsToBounds = YES;
        [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
            }
    return self;
}

-(void)setThumbnail:(NSObject <RVSAsyncImage> *)thumbnail
{
    //clear old to cancel
    _thumbnail = nil;
    _thumbnail = thumbnail;
    self.imageView.image = nil;
    
    __weak RVSAppTimelineCell *weakSelf = self;
    
    [weakSelf.thumbnail imageWithSize:weakSelf.imageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
        weakSelf.imageView.image = image;
    }];
}

@end
