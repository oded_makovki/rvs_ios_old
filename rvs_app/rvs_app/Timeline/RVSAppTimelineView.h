//
//  TVXTimeLine.h
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+NibLoading.h"
#import <rvs_sdk_api.h>

typedef enum
{
    AutoScrollTypeNone,
    AutoScrollTypeSmart,
    AutoScrollTypeAlways
}
AutoScrollType;

@class RVSAppTimelineView;

@protocol RVSAppTimelineViewDataSource <NSObject>
-(NSObject <RVSAsyncImage> *)timeline:(RVSAppTimelineView *)timeline thumbnailForDate:(NSDate *)date;
@end

@protocol RVSAppTimelineViewDelegate <NSObject>
@optional
-(void)timeline:(RVSAppTimelineView *)timeline didChangeWithStart:(NSDate *)start end:(NSDate *)end;
-(void)timeline:(RVSAppTimelineView *)timeline didSelectFrameWithDate:(NSDate *)date;
-(void)timelineDidBeginDragging;
-(void)timelineDidEndDragging;
@end

@interface RVSAppTimelineView : NibLoadedView

@property (nonatomic) NSInteger delegateTimeintervalSensitivity;

@property (nonatomic) BOOL isCentered;
@property (nonatomic, readwrite) BOOL isDragging;

@property (weak, nonatomic) id <RVSAppTimelineViewDelegate> delegate;
@property (weak, nonatomic) id <RVSAppTimelineViewDataSource> dataSource;

-(void)updateWithNow:(NSDate *)now start:(NSDate *)start end:(NSDate *)end;
-(BOOL)scrollTo:(NSDate *)date animated:(BOOL)animated;
@end
