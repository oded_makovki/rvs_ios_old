//
// RVSAppTrimControl.m
// RVSAppTrimControl
//

#import "RVSAppTrimControl.h"

#define RANGESLIDER_THUMB_SIZE 40

@interface RVSAppTrimControl()
@property (strong, nonatomic) UIViewContainer *sliderMiddleView;
@property (strong, nonatomic) UIImageView *leftThumbView;
@property (strong, nonatomic) UIImageView *rightThumbView;

@property (strong, nonatomic) RVSAppTrimPopover *leftPopover;
@property (strong, nonatomic) RVSAppTrimPopover *rightPopover;

@property (strong, nonatomic) UIViewContainer *leftMask;
@property (strong, nonatomic) UIViewContainer *rightMask;

@property (strong, nonatomic) UIView *popoverViewLong;
@property (strong, nonatomic) UILabel *timeLabelLong;
@property (nonatomic) NSInteger maxValue;
@property (nonatomic) NSInteger minValue;

@property (strong, nonatomic) NSString *resourceBundle;

@property (strong, nonatomic) NSString *pastTimeFormat;
@property (strong, nonatomic) NSString *futureTimeFormat;

@end

@implementation RVSAppTrimControl
@synthesize rightValue = _rightValue;
@synthesize leftValue = _leftValue;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppTrimControl];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppTrimControl];
}

-(void)initRVSAppTrimControl
{
    _resourceBundle = @"RVSAppTrimControl.bundle";
    
    self.pastTimeFormat = NSLocalizedString(@"trim time past format", nil);
    self.futureTimeFormat = NSLocalizedString(@"trim time future format", nil);
    
    _leftMask = [[UIViewContainer alloc] initWithFrame:CGRectMake(0, 0, 0, 64)];
    _leftMask.backgroundColor = [UIColor colorWithWhite:1 alpha:0.3];
    [self addSubview:_leftMask];
    
    _rightMask = [[UIViewContainer alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, 0, 64)];
    _rightMask.backgroundColor = [UIColor colorWithWhite:1 alpha:0.3];
    [self addSubview:_rightMask];
    
    _sliderMiddleView = [[UIViewContainer alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _sliderMiddleView.backgroundColor = [UIColor colorWithPatternImage:[self bundleImageNamed:@"SliderMiddle2"]];
    [self addSubview:_sliderMiddleView];
    
    UIPanGestureRecognizer *middlePan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleMiddlePan:)];
    [_sliderMiddleView addGestureRecognizer:middlePan];
    
    _maxValue = 100;
    _minValue = 0;
    
    _leftValue = 7;
    _rightValue = 93;
    
    _minLength = 2;
    _length = 15;
    
    _leftThumbView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, RANGESLIDER_THUMB_SIZE, 64)];
    _leftThumbView.image = [self bundleImageNamed:@"Slider2"];
    _leftThumbView.contentMode = UIViewContentModeLeft;
    _leftThumbView.userInteractionEnabled = YES;
    _leftThumbView.clipsToBounds = YES;
    [self addSubview:_leftThumbView];
    
    UIPanGestureRecognizer *leftPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftPan:)];
    [_leftThumbView addGestureRecognizer:leftPan];
    
//    UILongPressGestureRecognizer *leftTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLeftPress:)];
//    leftTap.minimumPressDuration = 0.2;
//    [leftTap requireGestureRecognizerToFail:leftPan];
//    [_leftThumbView addGestureRecognizer:leftTap];
    
    _rightThumbView = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - RANGESLIDER_THUMB_SIZE, 0, RANGESLIDER_THUMB_SIZE, 64)];
    _rightThumbView.image = [self bundleImageNamed:@"Slider2"];
    _rightThumbView.contentMode = UIViewContentModeRight;
    _rightThumbView.userInteractionEnabled = YES;
    _rightThumbView.clipsToBounds = YES;
    [self addSubview:_rightThumbView];
    
    UIPanGestureRecognizer *rightPan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightPan:)];
    [_rightThumbView addGestureRecognizer:rightPan];
    
//    UILongPressGestureRecognizer *rightTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleRightPress:)];
//    rightTap.minimumPressDuration = 0.2;
//    [rightTap requireGestureRecognizerToFail:rightPan];
//    [_rightThumbView addGestureRecognizer:rightTap];
    
    _leftPopover = [[RVSAppTrimPopover alloc] initWithFrame:CGRectMake(-9, -28, 40, 28)];
    [self addSubview:_leftPopover];
    
    _rightPopover = [[RVSAppTrimPopover alloc] initWithFrame:CGRectMake(-9, -28, 40, 28)];
    [self addSubview:_rightPopover];
    
    _popoverViewLong = [[UIView alloc] initWithFrame:CGRectMake(-9, -28, 90, 28)];
    _popoverViewLong.backgroundColor = [UIColor colorWithPatternImage:[self bundleImageNamed:@"PopoverLong"]];
    _timeLabelLong = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, 90, 10)];
    _timeLabelLong.font = [UIFont boldSystemFontOfSize:10];
    _timeLabelLong.backgroundColor = [UIColor clearColor];
    _timeLabelLong.textColor = [UIColor whiteColor];
    _timeLabelLong.textAlignment = NSTextAlignmentCenter;
    
    [_popoverViewLong addSubview:_timeLabelLong];
    _popoverViewLong.alpha = 0;
    [self addSubview:_popoverViewLong];
}

- (void)layoutSubviews
{
    CGFloat availableWidth = self.frame.size.width - RANGESLIDER_THUMB_SIZE;
    CGFloat inset = RANGESLIDER_THUMB_SIZE / 2;

    CGFloat range = _maxValue - _minValue;

    CGFloat left = floorf((_leftValue - _minValue) / range * availableWidth);
    CGFloat right = floorf((_rightValue - _minValue) / range * availableWidth);

    if (isnan(left)) left = 0;
    if (isnan(right)) right = 0;

    _leftThumbView.center = CGPointMake(inset + left, 64/2.0);
    _rightThumbView.center = CGPointMake(inset + right, 64/2.0);
    
    _leftMask.frame = CGRectMake(0, 0, _leftThumbView.frame.origin.x, 64);
    _rightMask.frame = CGRectMake(_rightThumbView.frame.origin.x + _rightThumbView.frame.size.width, 0, self.frame.size.width - (_rightThumbView.frame.origin.x + _rightThumbView.frame.size.width), 64);

    CGRect frame = CGRectMake(_leftThumbView.frame.origin.x + RANGESLIDER_THUMB_SIZE, 0, _rightThumbView.frame.origin.x - _leftThumbView.frame.origin.x - RANGESLIDER_THUMB_SIZE, 64);
    _sliderMiddleView.frame = frame;
}

- (UIImage *)bundleImageNamed:(NSString *)imageName
{
    return [UIImage imageNamed:[NSString stringWithFormat:@"%@/%@", self.resourceBundle, imageName]];
}

#pragma mark -
#pragma mark Styling

- (void)setFont:(UIFont *)font
{
    _font = font;
    _timeLabelLong.font = font;
    _rightPopover.timeLabel.font = font;
    _leftPopover.timeLabel.font = font;
}

- (void)setTextColor:(UIColor *)textColor
{
    _textColor = textColor;
    _timeLabelLong.textColor = textColor;
    _rightPopover.timeLabel.textColor = textColor;
    _leftPopover.timeLabel.textColor = textColor;
}

- (void)setTextBackgroundColor:(UIColor *)textBackgroundColor
{
    _textBackgroundColor = textBackgroundColor;
    _timeLabelLong.backgroundColor = textBackgroundColor;
    _rightPopover.timeLabel.backgroundColor = textBackgroundColor;
    _leftPopover.timeLabel.backgroundColor = textBackgroundColor;
}

- (void)setTextVerticalOffset:(NSInteger)textVerticalOffset
{
    _textVerticalOffset = textVerticalOffset;
    _timeLabelLong.frame = CGRectMake(0, 6 + textVerticalOffset, 90, 10);
    _rightPopover.timeLabel.frame = CGRectMake(0, 6 + textVerticalOffset, 40, 10);
    _leftPopover.timeLabel.frame = CGRectMake(0, 6 + textVerticalOffset, 40, 10);
}

#pragma mark -
#pragma mark UIGestureRecognizer delegates

- (void)handleMiddlePan:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan || gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gesture translationInView:self];
        CGFloat range = _maxValue - _minValue;
        CGFloat availableWidth = self.frame.size.width - RANGESLIDER_THUMB_SIZE;

        if (_leftValue + translation.x / availableWidth * range < 0) {
            CGFloat diff = _rightValue - _leftValue;
            _leftValue = 0;
            _rightValue = diff;
        }
        else if (_rightValue + translation.x / availableWidth * range > 100) {
            CGFloat diff = _rightValue - _leftValue;
            _leftValue = 100 - diff;
            _rightValue = 100;
        }
        else
        {
            _leftValue += translation.x / availableWidth * range;
            _rightValue += translation.x / availableWidth * range;
        }

        [gesture setTranslation:CGPointZero inView:self];

        [self setNeedsLayout];

        _popoverViewLong.alpha = 1;
        CGRect frame = _popoverViewLong.frame;
        frame.origin.x = _leftThumbView.frame.origin.x - 34 + floor((_rightThumbView.frame.origin.x - _leftThumbView.frame.origin.x) / 2);
        _popoverViewLong.frame = frame;
        
        _timeLabelLong.text = [NSString stringWithFormat:@"%@  —  %@", [self stringFromTime:_leftValue * _length / 100.0f], [self stringFromTime:_rightValue * _length / 100.0f]];

        [self notifyDelegate];
    }

    if (gesture.state == UIGestureRecognizerStateEnded)
        [self hidePopover:_popoverViewLong];
}

- (void)handleLeftPan:(UIPanGestureRecognizer *)gesture
{       
    if (gesture.state == UIGestureRecognizerStateBegan || gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gesture translationInView:self];
        CGFloat range = _maxValue - _minValue;
        CGFloat availableWidth = self.frame.size.width - RANGESLIDER_THUMB_SIZE;
        _leftValue += translation.x / availableWidth * range;
        if (_leftValue < 0) _leftValue = 0;
        
        float minValue = 100.0f * _minLength / _length;
        
        if (_rightValue - _leftValue < minValue) _leftValue = _rightValue - minValue;

        [gesture setTranslation:CGPointZero inView:self];

        [self setNeedsLayout];

        _leftPopover.alpha = 1;
        CGRect frame = _leftPopover.frame;
        frame.origin.x = _leftThumbView.frame.origin.x + RANGESLIDER_THUMB_SIZE / 2.0 - frame.size.width / 2.0;
        if (frame.origin.x < 2)
            frame.origin.x = 2;
        
        _leftPopover.frame = frame;

        _leftPopover.timeLabel.text = [self stringFromTime:_leftValue * _length / 100.0f];

        [self notifyDelegate];
    }

    if (gesture.state == UIGestureRecognizerStateEnded)
        [self hidePopover:_leftPopover];
}

- (void)handleRightPan:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan || gesture.state == UIGestureRecognizerStateChanged) {
        CGPoint translation = [gesture translationInView:self];
        CGFloat range = _maxValue - _minValue;
        CGFloat availableWidth = self.frame.size.width - RANGESLIDER_THUMB_SIZE;
        _rightValue += translation.x / availableWidth * range;
        
        float minValue = 100.0f * _minLength / _length;
        
        if (_rightValue > 100) _rightValue = 100;
        if (_rightValue - _leftValue < minValue) _rightValue = _leftValue + minValue;

        [gesture setTranslation:CGPointZero inView:self];

        [self setNeedsLayout];

        _rightPopover.alpha = 1;
        CGRect frame = _rightPopover.frame;
        frame.origin.x = _rightThumbView.frame.origin.x + RANGESLIDER_THUMB_SIZE / 2.0 - frame.size.width / 2.0;
        if (frame.origin.x + frame.size.width > 320 - 2)
            frame.origin.x = 320 - 2 - frame.size.width;
        
        _rightPopover.frame = frame;
        
        _rightPopover.timeLabel.text = [self stringFromTime:_rightValue * _length / 100.0f];
        
        [self notifyDelegate];
    }

    if (gesture.state == UIGestureRecognizerStateEnded)
        [self hidePopover:_rightPopover];
}

- (void)handleLeftPress:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
        _leftPopover.alpha = 1;
    
    if (gesture.state == UIGestureRecognizerStateEnded)
        [self hidePopover:_leftPopover];
}

- (void)handleRightPress:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
        _rightPopover.alpha = 1;
        
    if (gesture.state == UIGestureRecognizerStateEnded)
        [self hidePopover:_rightPopover];
}


#pragma mark - Utilities

- (NSString *)stringFromTime:(NSInteger)time
{
    NSInteger offsetTime = time + self.timeOffset;
    
    if ((int)offsetTime == 0)
    {
        return NSLocalizedString(@"trim time now", nil);
    }
    else if (offsetTime < 0)
    {
        return [NSString stringWithFormat:self.pastTimeFormat, fabs(offsetTime)];
    }
    else
    {
        return [NSString stringWithFormat:self.futureTimeFormat, fabs(offsetTime)];
    }
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    BOOL isNagative = NO;
    if (totalSeconds < 0)
    {
        isNagative = YES;
        totalSeconds = fabs(totalSeconds);
    }
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%@%02d:%02d",(isNagative ? @"-" : @"+"), minutes, seconds];
}

- (void)hidePopover:(UIView *)popover
{    
    [UIView animateWithDuration:0.3
                          delay:0
                        options:UIViewAnimationCurveEaseIn | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void) {

                         popover.alpha = 0;
                     }
                     completion:nil];
}

- (void)notifyDelegate
{
    if ([self.delegate respondsToSelector:@selector(trimControl:didChangeLeftValue:rightValue:)])
        [self.delegate trimControl:self didChangeLeftValue:self.leftValue rightValue:self.rightValue];
}

#pragma mark -
#pragma mark Properties

- (CGFloat)leftValue
{
    return _leftValue * _length / 100.0f;
}

- (CGFloat)rightValue
{
    return _rightValue * _length / 100.0f;
}

@end
