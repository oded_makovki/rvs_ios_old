//
//  RVSAppTrimPopover.h
//  RVSAppTrimPopover
//


#import <UIKit/UIKit.h>

@interface RVSAppTrimPopover:UIView

@property (strong, readonly, nonatomic) UILabel *timeLabel;

@end
