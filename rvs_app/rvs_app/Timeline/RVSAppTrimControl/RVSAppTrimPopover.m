//
//  RVSAppTrimPopover.m
//  RVSAppTrimPopover
//

#import "RVSAppTrimPopover.h"

@implementation RVSAppTrimPopover

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppTrimPopover];
    }
    return self;
}

-(void)initRVSAppTrimPopover
{
    self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@/Popover", @"RVSAppTrimControl.bundle"]]];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 6, 40, 10)];
    _timeLabel.font = [UIFont boldSystemFontOfSize:10];
    _timeLabel.backgroundColor = [UIColor clearColor];
    _timeLabel.textColor = [UIColor whiteColor];
    _timeLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:_timeLabel];
    self.alpha = 0;
}

@end
