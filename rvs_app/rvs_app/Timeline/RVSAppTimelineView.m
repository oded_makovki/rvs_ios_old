//
//  TVXTimeLine.m
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTimelineView.h"
#import "RVSAppTimelineCell.h"
#import "RVSAppTrimControl.h"
#import "UIViewContainer.h"
#import "UIView+Snapshot.h"
#import <rvs_sdk_api.h>

#define MAX_SELECTION_TIME 15.0     //30 sec
#define MIN_SELECTION_TIME 2.0      //5 sec

#define TIME_FRAME_SEC  3.0         //every 6 sec
#define SIZE_FRAME_PNT  64.0        //64 pnts

#define TIME_WINDOW_FRAMES self.frame.size.width / SIZE_FRAME_PNT
#define TIME_WINDOW_SEC TIME_WINDOW_FRAMES * TIME_FRAME_SEC

#define DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY 1.0

#define PNT_PER_SEC SIZE_FRAME_PNT / TIME_FRAME_SEC
#define SEC_PER_PNT TIME_FRAME_SEC / SIZE_FRAME_PNT

@interface RVSAppTimelineView () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIGestureRecognizerDelegate, RVSAppTrimControlDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet RVSAppTrimControl *trimControl;

@property (strong, nonatomic) NSDate *notifiedSelectionStart;
@property (strong, nonatomic) NSDate *notifiedSelectionEnd;

@property (strong, nonatomic) NSDate *now;
@property (strong, nonatomic) NSDate *start;
@property (strong, nonatomic) NSDate *end;
@property (nonatomic) NSTimeInterval length;

@property (strong, nonatomic) NSDate *currentStart;
@property (strong, nonatomic) NSDate *currentEnd;

@property (strong, nonatomic) NSDate *selectionStart;
@property (strong, nonatomic) NSDate *selectionEnd;
@property (nonatomic) float selectionLength;

@property (strong, nonatomic) NSString *pastTimeFormat;
@property (strong, nonatomic) NSString *futureTimeFormat;
@end

@implementation RVSAppTimelineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppTimelineView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppTimelineView];
}

-(void)initRVSAppTimelineView
{
    self.delegateTimeintervalSensitivity = DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY;
    
    [self.collectionView registerClass:[RVSAppTimelineCell class] forCellWithReuseIdentifier:@"timelineCell"];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.trimControl.delegate = self;
    
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    self.pastTimeFormat = NSLocalizedString(@"timeline time past format", nil);
    self.futureTimeFormat = NSLocalizedString(@"timeline time future format", nil);    
}

-(void)updateWithNow:(NSDate *)now start:(NSDate *)start end:(NSDate *)end
{
    self.now = now;
    self.start = start;
    self.end = end;
    
    self.length = [self.end timeIntervalSinceDate:self.start];
    
    NSLog(@"Now: %@ Start: %@ End: %@",self.now, self.start, self.end);
    [self.collectionView reloadData];
}

#pragma mark - Scroll To Time

-(BOOL)scrollTo:(NSDate *)date animated:(BOOL)animated
{
    BOOL canScroll = YES;
    
    NSDate * screenEnd = [NSDate dateWithTimeInterval:-TIME_WINDOW_SEC sinceDate:self.end];
    NSLog(@"scroll to: %@ start: %@ end: %@ screen: %@", date, self.start, self.end, screenEnd);
    
    if ([screenEnd timeIntervalSinceDate:date] < 0)
    {
        NSLog(@"scroll outside screen, updated time: %@", screenEnd);
        canScroll = NO;
        date = screenEnd;
    }
    
    NSTimeInterval diff = [date timeIntervalSinceDate:self.start];
    float offsetX = PNT_PER_SEC * diff;
    
    if (canScroll && self.isCentered)
        offsetX -= self.frame.size.width /2;
    
    [self.collectionView setContentOffset:CGPointMake(offsetX, 0) animated:animated];
    
    return canScroll;
}

#pragma mark - Bounds/Selection

-(NSDate *)getCurrentStart
{
    float startPt = self.collectionView.contentOffset.x;
    float startFrames = startPt / SIZE_FRAME_PNT;
    float startTimeSec = startFrames * TIME_FRAME_SEC;
    
    return [NSDate dateWithTimeInterval:startTimeSec sinceDate:self.start];
}

-(void)updateCurrentBounds
{
    if (!self.start || !self.end)
        return;
    
    self.currentStart = [self getCurrentStart];
    self.currentEnd = [NSDate dateWithTimeInterval:TIME_WINDOW_SEC sinceDate:self.currentStart];
    
    _trimControl.timeOffset = [self.currentStart timeIntervalSinceDate:self.now];
}

-(void)updateSelectionBounds
{
    if (!self.start || !self.end)
        return;
    
    self.selectionStart = [self.currentStart dateByAddingTimeInterval:self.trimControl.leftValue];
    self.selectionEnd = [self.currentStart dateByAddingTimeInterval:self.trimControl.rightValue];
    self.selectionLength = self.trimControl.rightValue - self.trimControl.leftValue;
    
    //if bound change bigger then sensitivity
    if (!self.notifiedSelectionStart ||
        fabsf([self.notifiedSelectionStart timeIntervalSinceDate:self.selectionStart]) > self.delegateTimeintervalSensitivity ||
        !self.notifiedSelectionEnd ||
        fabsf([self.notifiedSelectionEnd timeIntervalSinceDate:self.selectionEnd]) > self.delegateTimeintervalSensitivity)
    {
        self.notifiedSelectionStart = self.selectionStart;
        self.notifiedSelectionEnd = self.selectionEnd;
        
        if ([self.delegate respondsToSelector:@selector(timeline:didChangeWithStart:end:)])
        {
            [self.delegate timeline:self didChangeWithStart:self.selectionStart end:self.selectionEnd];
        }
    }
}

#pragma mark - RVSAppTrimControl Delegate

-(void)trimControl:(RVSAppTrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue
{
    [self updateSelectionBounds];
}

#pragma mark - UICollectionView Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self totalFrames];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SIZE_FRAME_PNT, SIZE_FRAME_PNT);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppTimelineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"timelineCell" forIndexPath:indexPath];
    
    NSDate *date = [self.start dateByAddingTimeInterval:indexPath.row * TIME_FRAME_SEC];
    cell.thumbnail = [self.dataSource timeline:self thumbnailForDate:date];
    
    NSTimeInterval diff = [date timeIntervalSinceDate:self.now];
    
    if ((int)diff == 0)
    {
        cell.timeLabel.text = NSLocalizedString(@"timeline time now", nil);
    }
    else if ((int)diff % 9 == 0)
    {
        if (diff < 0)
        {
            cell.timeLabel.text = [NSString stringWithFormat:self.pastTimeFormat, fabs(diff)];
        }
        else
        {
            cell.timeLabel.text = [NSString stringWithFormat:self.futureTimeFormat, fabs(diff)];
        }
    }
    else
    {
        cell.timeLabel.text = @"";
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    BOOL isNagative = NO;
    if (totalSeconds < 0)
    {
        isNagative = YES;
        totalSeconds = fabs(totalSeconds);
    }
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%@%02d:%02d",(isNagative ? @"-" : @"+"), minutes, seconds];
}

#pragma mark - UIScrollView Delegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (!_isDragging)
    {
        _isDragging = YES;
        if ([self.delegate respondsToSelector:@selector(timelineDidBeginDragging)])
            [self.delegate timelineDidBeginDragging];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateCurrentBounds];
    [self updateSelectionBounds];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self onScrollEnded:scrollView];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self onScrollEnded:scrollView];
    
    if (_isDragging)
    {
        _isDragging = NO;
        if ([self.delegate respondsToSelector:@selector(timelineDidEndDragging)])
            [self.delegate timelineDidEndDragging];
    }
}

-(void)onScrollEnded:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x >= scrollView.contentSize.width - scrollView.frame.size.width)
    {
        //TBD
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(timeline:didSelectFrameWithDate:)])
    {
        NSDate *date = [self.start dateByAddingTimeInterval:indexPath.row * TIME_FRAME_SEC];
        
        [self.delegate timeline:self didSelectFrameWithDate:date];
    }
}

-(float)totalFrames
{
    return self.length / TIME_FRAME_SEC;
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end
