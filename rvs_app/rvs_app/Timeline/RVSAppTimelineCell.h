//
//  TVXClipFrameCell.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@interface RVSAppTimelineCell : UICollectionViewCell

@property (strong, nonatomic) NSObject <RVSAsyncImage> *thumbnail;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
