//
//  RVSAppPushableUserListViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableUserListViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import <rvs_sdk_api.h>
#import "RVSAppUserListCell.h"
#import "RVSAppUserListFooterCell.h"
#import "RVSApplication.h"

#import "RVSAppUserDataObject.h"

@interface RVSAppPushableUserListViewController () <RVSAppDataListViewDelegate>
@property (nonatomic) NSObject <RVSAsyncUserList> *userList;
@property (nonatomic) BOOL shouldClearUsersData;
@end

@implementation RVSAppPushableUserListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataListView.numberOfSections = 1; //just posts
    
    [self.dataListView setSectionsCellClass:[RVSAppUserListCell class] forSection:0];
    self.dataListView.footerCellClass = [RVSAppUserListFooterCell class];
    self.dataListView.delegate = self;

    //already set - load data
    if (self.userListFactory)
        [self loadUsers];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.dataListView activateIfNeeded];
}

- (void)reloadUsers
{
    self.shouldClearUsersData = YES;
    [self loadUsers];
}

- (void)loadUsers
{
    self.userList = nil;
    self.userList = [self.userListFactory userList];
    
    __weak RVSAppPushableUserListViewController *weakSelf = self;
    
    [self.userList usersUsingBlock:^(NSArray *users, NSError *error) {
        
        if (weakSelf.shouldClearUsersData)
        {
            weakSelf.shouldClearUsersData = NO;
            [weakSelf.dataListView clearDataInSection:0];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;

            [weakSelf.dataListView addData:nil toSection:0];
        }
        else
        {
            if (!users || [users count] == 0)
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            }
            else
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:weakSelf];
            
            [weakSelf.dataListView addData:[RVSAppUserDataObject dataObjectsFromUsers:users userViewDelegate:weakSelf]
                                 toSection:0];
        }
        
    } count:10];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    [self.userList nextUsers];
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadUsers];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if ([data isKindOfClass:[RVSAppUserDataObject class]])
    {
        NSObject <RVSUser> *user = ((RVSAppUserDataObject *)data).user;        
        [self pushUserDetailViewControlWithCachedUser:user userId:nil title:user.name];
    }
}

-(void)networkStatusDidChange
{
    [super networkStatusDidChange];
    if (self.dataListView.error && [RVSApplication application].hasNetworkConnection)
    {
        self.dataListView.error = nil;
        self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault;
        
        [self.dataListView clear];
        [self reloadUsers];
    }
}

-(void)composeDidCompleteWithPost:(NSObject <RVSPost> *)post inReplyToPost:(NSObject <RVSPost> *)replyToPost
{
    //TBD
}

@end
