//
//  SearchController.m
//  InnovaTV
//
//  Created by Barak Harel on 1/26/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "SearchResultsController.h"
#import <AFNetworking.h>
#import <Social/Social.h>

#import "SearchViewController.h"

#import "SearchResult.h"
#import "SearchResultCell.h"

#import "ChannelController.h"
#import "Channel.h"

#import "UIColor+RVSApplication.h"

@interface SearchResultsController () <UICollectionViewDelegateFlowLayout, SearchResultDelegate>
@property (nonatomic) UIActivityIndicatorView *activityIndicator;
@property (nonatomic) UILabel *noVideoLabel;

@property (nonatomic) NSDateFormatter *utcDateFormatter;
@property (nonatomic) NSArray *data;
@property (nonatomic) NSDictionary *channelInfos;

@property (nonatomic) AFHTTPRequestOperation *request;
@end

@implementation SearchResultsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupChannelsInfos];
    
	self.navigationItem.title = [NSString stringWithFormat:@"Search: %@", self.query];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor rvsBackgroundColor];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    self.activityIndicator.hidesWhenStopped = YES;    
    [self.activityIndicator setColor:[UIColor darkGrayColor]];
    
    [self.view addSubview:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
    
    [self search];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.activityIndicator.center = self.collectionView.center;  
}

- (void)setupChannelsInfos
{
    //known channels
    self.channelInfos = @{
                                        @"us-fxhd": @{@"title" : @"FX", @"logo" : @"fx", @"video" : @"http://54.235.209.41/cgi-bin/217011/tvix_fx/variant.m3u8", @"category" : @"entertainment"},
                                        @"us-aplhd": @{@"title" : @"ANIMAL PLANET", @"logo" : @"animal_planet", @"video" : @"http://54.235.209.41/cgi-bin/217217/tvix_animal/variant.m3u8", @"category" : @"travel"},
                                        @"us-hbo": @{@"title" : @"HBO", @"logo" : @"hbo", @"video" : @"http://54.235.209.41/cgi-bin/217010/tvix_hbo/variant.m3u8", @"category" : @"entertainment"},
                                        @"us-cnnhd": @{@"title" : @"CNN", @"logo" : @"cnn", @"video" : @"http://54.235.209.41/cgi-bin/217008/tvix_cnn/variant.m3u8", @"category" : @"news"},
                                        @"us-espnhd": @{@"title" : @"ESPN", @"logo" : @"espn", @"video" : @"http://54.235.209.41/cgi-bin/217218/tvix_espn/variant.m3u8", @"category" : @"sports"},
                                        @"us-science": @{@"title" : @"SCIENCE", @"logo" : @"science_channel", @"video" : @"http://54.235.209.41/cgi-bin/217009/tvix_science/variant.m3u8", @"category" : @"entertainment"},
                                        
                                        @"us-amc": @{@"title" : @"BBC", @"logo" : @"amc", @"video" : @"", @"category" : @"entertainment"},
                                        @"us-nbatvhd": @{@"title" : @"NBA TV", @"logo" : @"nba", @"video" : @"", @"category" : @"sports"},
                                        @"ABC": @{@"title" : @"ABC", @"logo" : @"abc", @"video" : @"", @"category" : @"entertainment"}, //NO ID ?
                                        @"us-eentertainment": @{@"title" : @"E!", @"logo" : @"Eentertainment", @"video" : @"", @"category" : @"entertainment"},
                                        @"NFLHD": @{@"title" : @"NFL", @"logo" : @"nfl_channel", @"video" : @"", @"category" : @"sports"},//NO ID ?
                                        @"us-weather": @{@"title" : @"WEATHER", @"logo" : @"weather_channel", @"video" : @"", @"category" : @"weather"},
                                        @"us-usahd": @{@"title" : @"USA", @"logo" : @"usa_channel", @"video" : @"", @"category" : @"entertainment"},
                                        @"us-foodnetwork": @{@"title" : @"FOOD", @"logo" : @"food_channel", @"video" : @"", @"category" : @"food"},
                                        @"us-bravohd": @{@"title" : @"BRAVO", @"logo" : @"bravo", @"video" : @"", @"category" : @"relationships"},
                                        @"TNTHD": @{@"title" : @"TNT", @"logo" : @"tnt", @"video" : @"", @"category" : @"entertainment"},//NO ID ?
                                        @"us-foxsports1": @{@"title" : @"FOX SPORTS", @"logo" : @"fox_sports", @"video" : @"", @"category" : @"sports"},
                                        @"us-halmrk": @{@"title" : @"HALLMARK", @"logo" : @"hallmark", @"video" : @"", @"category" : @"entertainment"},
                                        @"CBSHD": @{@"title" : @"CBS", @"logo" : @"cbs", @"video" : @"", @"category" : @"entertainment"}, //NO ID
                                        @"NBCSPORTSHD": @{@"title" : @"NBC SPORTS", @"logo" : @"nbc", @"video" : @"", @"category" : @"weather"},//NO ID
                                        @"us-msnbc": @{@"title" : @"MSNBC", @"logo" : @"msnbc", @"video" : @"", @"category" : @"news"},
                                        @"us-cnbchd": @{@"title" : @"CNBC", @"logo" : @"cnbc", @"video" : @"", @"category" : @"news"}
                                        };
}

-(void)search
{
    NSString *apiuri = @"https://api-staging.boxfish.com/v4/search/mentions/?";
    NSString *lineup = @"cf3042d7-afeb-4f85-a233-2ea5ede39269";
    NSString *token =@"eyJjIjo2MzUxNjEzNjk5MTA0MjgwMDAsInMiOiJJOF9GOGdCOUxCMTBaYUpDaHBQY3lXVzUxd2MiLCJpZCI6IlNPQ0RQSDdGZTBDMEhHVHV2UUY5TUEiLCJuIjoid2ZIcUI1REh2TVNBVEEiLCJyIjoid0c2RG8xZlcxaHp3aGciLCJrIjoiT0F1dGgifQ";
    NSString *limit = @"16";
    
    NSString * requestUrl = [NSString stringWithFormat:@"%@&query=%@&lineup=%@&token=%@&limit=%@",apiuri, [self.query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], lineup, token, limit];
    
    __weak SearchResultsController *weakSelf = self;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    self.request = [manager GET:requestUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [weakSelf.activityIndicator stopAnimating];
        [weakSelf handleResponse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        
        [weakSelf.activityIndicator stopAnimating];
        
        NSDictionary *userInfo = [error userInfo];
        NSError *underlyingError = [userInfo objectForKey:NSUnderlyingErrorKey];
        NSString *underlyingErrorDescription = [underlyingError localizedDescription];
        
        [weakSelf showNoResultsWithReason:underlyingErrorDescription];
    }];
}

-(void)handleResponse:(id)responseObject
{
//    NSLog(@"JSON: %@", responseObject);
    if ([responseObject isKindOfClass:[NSDictionary class]])
    {
        [self parseResults:@[responseObject]];
    }
    else if ([responseObject isKindOfClass:[NSArray class]])
    {
        [self parseResults:responseObject];
    }
}

- (NSMutableAttributedString *)attTextFromResultText:(NSString *)text
{
    NSLog(@"\n********************\nin: %@", text);
    
    NSString *stripedText =
    [[[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUTF8StringEncoding]
                                             options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                       NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]}                               documentAttributes:nil error:nil] string];
    
    NSString *linePrefix = @"▸\t";
    
    stripedText = [stripedText stringByReplacingOccurrencesOfString:@">> " withString:@""]; //remove arrow + space
    stripedText = [stripedText stringByReplacingOccurrencesOfString:@">>" withString:@""]; //remove arrows
    stripedText = [stripedText stringByReplacingOccurrencesOfString:@"\n" withString:[NSString stringWithFormat:@"\n%@",linePrefix]]; //break
    
    if (![stripedText hasPrefix:linePrefix]) //add linePrefix if needed
        stripedText = [NSString stringWithFormat:@"%@%@", linePrefix, stripedText];
    
    if ([stripedText hasSuffix:linePrefix]) //remove extra linePrefix
        stripedText = [stripedText substringToIndex:stripedText.length - linePrefix.length];
    
//    NSLog(@"\nstriped: %@\n********************\n", stripedText);
    
    NSMutableParagraphStyle *pStyle = [[NSMutableParagraphStyle alloc] init];
    CGFloat tabWidth = 15.0f;
    
    [pStyle setDefaultTabInterval:tabWidth];
    [pStyle setHeadIndent:tabWidth];
    [pStyle setTabStops:[NSArray array]]; //MUST! (reset tabs)
    [pStyle setLineBreakMode:NSLineBreakByWordWrapping];
    
    NSDictionary *attributes = @{
                                 NSFontAttributeName: [UIFont systemFontOfSize:12],
                                 NSForegroundColorAttributeName: [UIColor blackColor],
                                 NSParagraphStyleAttributeName: pStyle
                                 };
    
    
    NSMutableAttributedString *attText = [[NSMutableAttributedString alloc]
                                          initWithString:stripedText
                                          attributes:attributes];
    
    // Detect all query in string
    NSString *pattern = [NSString stringWithFormat:@"%@\\b", self.query];
    NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    
    [hashRegex enumerateMatchesInString:stripedText options:0 range:NSMakeRange(0,stripedText.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         [attText addAttribute:NSForegroundColorAttributeName
                         value:[UIColor rvsTintColor]
                         range:match.range];

         [attText addAttribute:NSFontAttributeName
                        value:[UIFont boldSystemFontOfSize:12]
                        range:match.range];
         
     }];
    
    NSLog(@"\nout: %@\n********************\n", attText);
    
    return attText;
}

-(void)parseResults:(NSArray *)results
{
    NSLog(@"Got %lu results...", (unsigned long)[results count]);
    
    NSMutableArray *data = [NSMutableArray arrayWithCapacity:[results count]];
    
    for (NSDictionary *result in results)
    {
//        NSLog(@"Result: %@", result);
        
        //get channel info
        NSString *channelId = [result[@"channel"][@"id"] lowercaseString];
        NSDictionary *channelInfo = self.channelInfos[channelId];
        
        //build result
        SearchResult *searchResult = [[SearchResult alloc] init];

        searchResult.channelImageName = channelInfo[@"logo"];
        searchResult.videoBaseUrl = channelInfo[@"video"];
        
        searchResult.imageURL = result[@"image"];
        searchResult.time = [self.utcDateFormatter dateFromString:result[@"time"]];
        
        searchResult.attText = [self attTextFromResultText:result[@"text"]];;
        
        searchResult.channelId = result[@"channel"][@"id"];
        searchResult.channelName = result[@"channel"][@"name"];
        
        searchResult.programTitle = result[@"program"][@"name"];
        searchResult.programStart = [self.utcDateFormatter dateFromString:result[@"program"][@"start"]];
        searchResult.programStop = [self.utcDateFormatter dateFromString:result[@"program"][@"stop"]];
        
        searchResult.delegate = self;
        
        [data addObject:searchResult];
    }
    
    NSArray *sortedData = [data sortedArrayUsingComparator:^NSComparisonResult(id a, id b)
    {
        NSString *first = [(SearchResult*)a videoBaseUrl];
        NSString *second = [(SearchResult*)b videoBaseUrl];
        
        if (first.length > 0 && second.length == 0)
        {
            return NSOrderedAscending;
        }
        else if (first.length == 0 && second.length > 0)
        {
            return NSOrderedDescending;
        }
        else
        {
            return NSOrderedSame;
        }
    }];
    
    self.data = sortedData;
    [self.collectionView reloadData];
    
    if (self.data.count == 0)
    {
        [self showNoResultsWithReason:nil];
    }
    else //updated background color
    {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
            self.collectionView.backgroundColor = [UIColor lightGrayColor];
        } completion:nil];
    }
}

-(void)showNoResultsWithReason:(NSString *)reason
{
    self.noVideoLabel = [[UILabel alloc] init];
    self.noVideoLabel.backgroundColor = [UIColor clearColor];
    self.noVideoLabel.textColor = [UIColor darkGrayColor];
    self.noVideoLabel.textAlignment = NSTextAlignmentLeft;
    self.noVideoLabel.font = [UIFont systemFontOfSize:15.0];
    
    if (reason)
    {
        self.noVideoLabel.text = [NSString stringWithFormat:@"No results found:\n%@", reason];
    }
    else
    {
        self.noVideoLabel.text = @"No results found...";
    }
    
    //auto size
    self.noVideoLabel.numberOfLines = 0;
    CGFloat margins = 80; //TO DO: iPad size?
    CGSize size = [self.noVideoLabel sizeThatFits:CGSizeMake(self.view.frame.size.width - margins, self.view.frame.size.height - margins)];
    self.noVideoLabel.frame = CGRectMake(20, 20, size.width, size.height);
    //center
    self.noVideoLabel.center = self.collectionView.center;
    
    [self.view addSubview:self.noVideoLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.data count];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SearchResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.data = self.data[indexPath.row];
    
    return cell;
}

-(NSDateFormatter *)utcDateFormatter
{
    if (!_utcDateFormatter)
    {
        _utcDateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [_utcDateFormatter setTimeZone:timeZone];
        [_utcDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    }
    
    return _utcDateFormatter;
}

-(BOOL)shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(void)playClipWithData:(SearchResult *)data andImage:(UIImage *)image
{
    SearchResult *searchResult = data;
    NSString *video = @"";
    
    if (searchResult.videoBaseUrl.length)
    {
        NSInteger start = [searchResult.time timeIntervalSince1970] - 2;
        NSInteger end = start + 32;
        
        video = [NSString stringWithFormat:@"%@?start=%ld&end=%ld", searchResult.videoBaseUrl, (long)start, (long)end];
    }
    
    Channel *fakeChannel = [[Channel alloc] init];
    fakeChannel.channelInfo = @{@"category" : @"none", @"title" : searchResult.programTitle, @"video" : video};
    
    ChannelController *channelController = [self.storyboard instantiateViewControllerWithIdentifier:@"ChannelController"];
    
    channelController.channel = fakeChannel;
    channelController.searchResultData = data;
    channelController.searchResultImage = image;
    
    [self.navigationController pushViewController:channelController animated:YES];
}

-(void)shareToTwitterWithData:(SearchResult *)data andImage:(UIImage *)image
{
    SLComposeViewController *tweetSheet = [SLComposeViewController
                                           composeViewControllerForServiceType:SLServiceTypeTwitter];
    [tweetSheet addImage:image];
    [tweetSheet setInitialText:[NSString stringWithFormat:@"Watching %@ on #whip!", data.programTitle]];
    [self presentViewController:tweetSheet animated:YES completion:nil];
}

-(void)dealloc
{
    NSLog(@"SearchController dealloc");
    [self.request cancel];
}

@end
