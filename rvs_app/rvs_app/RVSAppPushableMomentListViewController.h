//
//  RVSAppPushableMomentListViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppMomentListFactory.h"

@interface RVSAppPushableMomentListViewController : RVSAppCommonDataViewController

@property (nonatomic) Class headerClass;
@property (nonatomic) NSObject *headerData;
@property (nonatomic) RVSAppMomentListFactory *momentListFactory;

- (void)reloadMoments;
@end
