//
//  RVSAppPushablePostListViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppPostListFactory.h"


@interface RVSAppPushablePostListViewController : RVSAppCommonDataViewController

@property (nonatomic) Class headerClass;
@property (nonatomic) Class postsClass;
@property (nonatomic) Class footerClass;

@property (nonatomic) NSObject *headerData;
@property (nonatomic) RVSAppPostListFactory *postListFactory;

@property (nonatomic) BOOL isMyFeed;

- (void)reloadPosts;
@end
