//
//  RVSAppHashTagsCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTextAcceleratorCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@implementation RVSAppTextAcceleratorCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        self.textLabel.numberOfLines = 1;
//        self.layer.borderWidth = 1.f;
        self.backgroundColor = [UIColor rvsTintColor];
        self.layer.cornerRadius = 5.0f;
    }
    return self;
}

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    
    if (highlighted)
    {
        self.backgroundColor = [[UIColor rvsTintColor] colorWithAlphaComponent:0.7];
    }
    else
    {
        self.backgroundColor = [UIColor rvsTintColor];
    }
}

@end
