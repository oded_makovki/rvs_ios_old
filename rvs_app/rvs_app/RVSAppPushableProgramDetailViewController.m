//
//  RVSAppPushableProgramDetailViewController.m
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableProgramDetailViewController.h"

#import "RVSAppCommonViewController_Private.h"
#import "RVSAppProgramDetailCell.h"
#import "RVSAppFooterLinkCell.h"
#import "RVSAppProgramDetailFooterCell.h"
#import "RVSAppMomentListCell.h"
#import "RVSApplication.h"

#import <rvs_sdk_api.h>

#define HEADER_SECTION 0
#define ITEMS_LIST_SECTION 1
#define PROGRAM_LINK_SECTION 2

@interface RVSAppPushableProgramDetailViewController () <RVSAppDataListViewDelegate>
@property (nonatomic) NSObject <RVSAsyncMomentList> *momentList;
@property (nonatomic) BOOL shouldClearMomentsData;

@property (nonatomic) NSObject <RVSAsyncError> *followAsyncError;
@end

@implementation RVSAppPushableProgramDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataListView.numberOfSections = 3; //program, moments, link
    
    [self.dataListView setSectionsCellClass:[RVSAppProgramDetailCell class] forSection:HEADER_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppMomentListCell class] forSection:ITEMS_LIST_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppFooterLinkCell class] forSection:PROGRAM_LINK_SECTION];
    
    self.dataListView.footerCellClass = [RVSAppProgramDetailFooterCell class];
    self.dataListView.delegate = self;
    
    //load default program
//    [self loadProgram];
    [self loadMoments];
    [self loadLinks];
}

-(void)loadLinks
{
    [self.dataListView clearDataInSection:PROGRAM_LINK_SECTION];
    RVSAppFooterLinkDataObject *programLinkData = [[RVSAppFooterLinkDataObject alloc] initWithProgram:self.program];
    [self.dataListView addData:@[programLinkData] toSection:PROGRAM_LINK_SECTION];
}

-(void)loadProgram
{
    //new data from program
    RVSAppProgramDataObject *data = [[RVSAppProgramDataObject alloc] init];
    
    if (self.program)
    {
        data.program = self.program;
        data.programViewDelegate = self;
    }
    //add data
    [self.dataListView addData:@[data] toSection:HEADER_SECTION];
}

-(void)reloadMoments
{
    self.shouldClearMomentsData = YES;
    [self loadMoments];
}

- (void)loadMoments
{
    self.momentList = nil;
    self.momentList = [self.momentListFactory momentList];
    
    __weak RVSAppPushableProgramDetailViewController *weakSelf = self;
    
    [self.momentList momentsUsingBlock:^(NSArray *moments, NSError *error) {
        
        if (weakSelf.shouldClearMomentsData)
        {
            weakSelf.shouldClearMomentsData = NO;
            [weakSelf.dataListView clearDataInSection:ITEMS_LIST_SECTION];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;

            [weakSelf.dataListView addData:nil toSection:ITEMS_LIST_SECTION];
        }
        else
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:weakSelf];
            
            [weakSelf.dataListView addData:[RVSAppMomentDataObject dataObjectsFromMoments:moments
                                                                     momentViewDelegate:weakSelf]
                                 toSection:ITEMS_LIST_SECTION];
        }
        
    } count:3];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    //disabled
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadMoments];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if (indexPath.section == ITEMS_LIST_SECTION)
    {
        [self pushMomentDetailViewControlWithMoment:[(RVSAppMomentDataObject *)data moment] title:NSLocalizedString(@"moment title", nil)];
    }
    else if (indexPath.section == PROGRAM_LINK_SECTION)
    {
        if (self.program)
        {
            [self pushMomentListViewControlWithProgram:self.program title:NSLocalizedString(@"moments title", nil)];
        }
    }
}
@end
