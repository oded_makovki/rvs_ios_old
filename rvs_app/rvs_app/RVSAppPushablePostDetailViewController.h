//
//  RVSAppPushablePostDetailViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import <rvs_sdk_api.h>

@interface RVSAppPushablePostDetailViewController : RVSAppCommonDataViewController
@property (nonatomic) NSObject <RVSPost> *post;
@end
