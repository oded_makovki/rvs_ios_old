//
//  RVSAppHashTagsAcceleratorView.m
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppHashTagsAcceleratorView.h"
#import "UIView+NibLoading.h"

@implementation RVSAppHashTagsAcceleratorView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppHashTagsAcceleratorView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppHashTagsAcceleratorView];
    }
    return self;
}

-(void)initRVSAppHashTagsAcceleratorView
{
    self.loadingText = NSLocalizedString(@"acc loading hashtags" , nil);
    self.noDataText  = NSLocalizedString(@"acc no hashtags" , nil);
    
    self.errorLabel.text = self.loadingText;
}

@end
