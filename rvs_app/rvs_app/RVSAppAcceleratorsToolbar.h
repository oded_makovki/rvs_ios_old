//
//  RVSAppAcceleratorsView.h
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "UIView+NibLoading.h"
#import "RVSAppAcceleratorView.h"
#import "RVSAppTranscriptAcceleratorView.h"
#import "RVSAppUsersAcceleratorView.h"
#import "RVSAppHashTagsAcceleratorView.h"

@interface RVSAppAcceleratorsToolbar : NibLoadedView
@property (strong, nonatomic, readonly) RVSAppTranscriptAcceleratorView *transcriptView;
@property (strong, nonatomic, readonly) RVSAppHashTagsAcceleratorView *hashTagsView;
@property (strong, nonatomic, readonly) RVSAppUsersAcceleratorView *usersView;

-(void)setLayoutInEditVideo:(BOOL)inEditVideo animateWithDuration:(float)duration;
-(void)showTranscriptAccelerator;
-(void)showHashTagsAccelerator;
-(void)showUsersAccelerator;
@end
