//
//  RVSAppUserView.h
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@class RVSAppUserView;
@protocol RVSAppUserViewDelegate <NSObject>

@optional
-(void)userView:(RVSAppUserView *)userView didSelectSettingsForUser:(NSObject <RVSUser> *)user;

-(void)userView:(RVSAppUserView *)userView didSelectMentionsForUser:(NSObject <RVSUser> *)user;

-(void)userView:(RVSAppUserView *)userView didSelectPostsCountForUser:(NSObject <RVSUser> *)user;
-(void)userView:(RVSAppUserView *)userView didSelectFollowersCountForUser:(NSObject <RVSUser> *)user;
-(void)userView:(RVSAppUserView *)userView didSelectFollowingCountForUser:(NSObject <RVSUser> *)user;
@end

@interface RVSAppUserView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *userThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *userHandleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *followToggleButton;

@property (weak, nonatomic) IBOutlet UILabel *postsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *postsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *postsCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followersCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followingCountTapView;

@property (weak, nonatomic) IBOutlet UIButton *userSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *userMentionsButton;

@property (nonatomic) NSObject <RVSUser> *user;
@property (weak, nonatomic) id <RVSAppUserViewDelegate> delegate;
-(void)reset;

/**
 *  set user for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param user         user object
 *  @param isSizingView set to YES for optimiziation
 */
-(void)setUser:(NSObject <RVSUser> *)user isSizingView:(BOOL)isSizingView;

/**
 *  set user for view. same as setUser:user isSizingView:NO
 *
 *  @param user user object
 */
-(void)setUser:(NSObject <RVSUser> *)user;

@end
