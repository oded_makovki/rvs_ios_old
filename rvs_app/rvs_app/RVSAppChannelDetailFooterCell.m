//
//  RVSAppChannelDetailFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/18/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDetailFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppChannelDetailFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppChannelDetailFooterCell];
    }
    return self;
}

-(void)initRVSAppChannelDetailFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    [self setText:NSLocalizedString(@"channel detail footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"channel detail footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"channel detail footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 5);
}

@end
