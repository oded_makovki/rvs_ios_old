//
//  RVSAppChannelSelectCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppChannelView.h"

@interface RVSAppChannelSelectCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) NSObject <RVSChannel> *channel;
@end
