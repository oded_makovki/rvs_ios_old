//
//  RVSAppPostListFactory.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostListFactory.h"

@interface RVSAppPostListFactory()
@property (nonatomic) id parameter;
@end

@implementation RVSAppPostListFactory

-(id)initWithMyFeed
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeMyFeed;
    }
    return self;
}

-(id)initWithMyMentions
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeMyMentions;
    }
    return self;
}

-(id)initWithUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeUser;
        _parameter = userId;
    }
    return self;
}

-(id)initWithMyFavoritesOfUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeFavoritesOfUser;
        _parameter = userId;
    }
    return self;
}

-(id)initWithMoment:(NSString *)momentId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeMoment;
        _parameter = momentId;
    }
    return self;
}

-(NSObject<RVSAsyncPostList> *)postList
{
    switch (self.type)
    {
        case RVSAppPostListFactoryTypeMyFeed:
            return [[RVSSdk sharedSdk] myNewsFeed];
            break;
            
        case RVSAppPostListFactoryTypeMyMentions:
            return [[RVSSdk sharedSdk] myMentions];
            break;
            
        case RVSAppPostListFactoryTypeUser:
            return [[RVSSdk sharedSdk] postsForUser:self.parameter];
            break;
            
        case RVSAppPostListFactoryTypeFavoritesOfUser:
            return [[RVSSdk sharedSdk] favoritePostsForUser:self.parameter];
            break;
            
        case RVSAppPostListFactoryTypeMoment:
            return [[RVSSdk sharedSdk] postsForMoment:self.parameter];
            break;
            
        default:
            return nil;
            break;
    }
}

@end
