//
//  RVSAAppDelegate.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDelegate.h"
#import "RVSApplication.h"
#import "RVSAppMainViewController.h"
#import "RVSAppSignInViewController.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>
#import <Crashlytics/Crashlytics.h>
#import "TestFlight.h"

NSString *RVSAppTestFlightToken = @"RVSAppTestFlightToken";
NSString *RVSAppCrashlyticsApiKey = @"RVSAppCrashlyticsApiKey";

@interface RVSAppDelegate()
@property (nonatomic) RVSNotificationObserver *signInObserver;
@property (nonatomic) RVSNotificationObserver *signOutObserver;
@end


@implementation RVSAppDelegate

- (void)signedOut
{
    NSLog(@"signedOut");
    
    if ([self.window.rootViewController isKindOfClass:[RVSAppSignInViewController class]])
        return;
    
    RVSAppSignInViewController *signInViewController = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"signInViewController"];
    
    NSLog(@"Adding RVSAppSignInViewController: %@", signInViewController);
    
    self.window.rootViewController = signInViewController;
    [self.window makeKeyAndVisible];
}

- (void)signedIn
{
    NSLog(@"signedIn");
    
    if ([self.window.rootViewController isKindOfClass:[RVSAppMainViewController class]])
        return;
    
    RVSAppMainViewController *mainViewController = [[RVSApplication application].mainStoryboard instantiateInitialViewController];
    
    NSLog(@"Adding RVSAppMainViewController: %@", mainViewController);
    
    
    self.window.rootViewController = mainViewController;
    [self.window makeKeyAndVisible];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //Test Flight
    NSString* testFlightAppToken = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSAppTestFlightToken];
    if (testFlightAppToken && testFlightAppToken.length > 0)
    {
        [TestFlight takeOff:testFlightAppToken];
    }
    
    //crashlytics!
    NSString* crashlyticsApiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSAppCrashlyticsApiKey];
    if (crashlyticsApiKey && crashlyticsApiKey.length > 0)
    {
        [Crashlytics startWithAPIKey:RVSAppCrashlyticsApiKey];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    __weak RVSAppDelegate *weakSelf = self;
    
    self.signInObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedInNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf signedIn];
    }];    
    
    self.signOutObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedOutNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf signedOut];
    }];
    
    if ([RVSSdk sharedSdk].isSignedOut)
    {
        [self signedOut];
    }
    else
    {
        [self signedIn];
    }
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
