//
//  RVSAppTopBarView.h
//  rvs_app
//
//  Created by Barak Harel on 11/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "UIView+NibLoading.h"

@interface RVSAppTopBarView : NibLoadedView

@property (nonatomic, readonly) BOOL isShown;
@property (nonatomic, readonly) CGFloat barHeight;

-(void)showAnimated:(BOOL)animated;
-(void)hideAnimated:(BOOL)animated;

-(void)show;
-(void)hide;

-(void)showPostProgressWithThumbnail:(NSObject <RVSAsyncImage> *)thumbnail;
-(void)hidePostProgressWithSuccess:(BOOL)success delay:(NSTimeInterval)delay;

-(void)showNoNetworkStatusAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay;
-(void)hideNoNetworkStatusAnimated:(BOOL)animated afterDelay:(NSTimeInterval)delay;

@end
