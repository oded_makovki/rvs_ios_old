//
//  RVSAppProgramView.m
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppProgramView.h"

@interface RVSAppProgramView()
@property (strong, nonatomic) NSObject <RVSAsyncImage> *programAsyncImage;
@end


@implementation RVSAppProgramView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)reset
{
    //clear
    self.programImageView.image = nil;
    self.programAsyncImage = nil;
    
    self.programLabel.text = NSLocalizedString(@"program placeholder", nil);
    [self.programImageView.layer setMinificationFilter:kCAFilterTrilinear];
}

-(void)setProgram:(NSObject<RVSProgram> *)program
{
    if (_program == program)
        return;
    
    [self reset];
    _program = program;
    
    if (!_program)
        return;
    
    //update metadata
    NSObject <RVSAsyncImageFactory> *imageFactory = self.program.content.image;
    self.channelAsyncImage = [imageFactory asyncImage];
    
    
    //update name
    NSString *programName = self.program.content.name;
    
    if (self.programLabel && programName && programName.length > 0)
        self.programLabel.text = [programName uppercaseString];
    
}

-(void)setChannelAsyncImage:(NSObject <RVSAsyncImage> *)programAsyncImage
{
    if (_programAsyncImage == programAsyncImage)
        return;
    
    _programAsyncImage = programAsyncImage;
    self.programImageView.image = nil;
    
    __weak RVSAppProgramView *weakSelf = self;
    [weakSelf.programAsyncImage imageWithSize:weakSelf.programImageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
        weakSelf.programImageView.image = image;
    }];
}


@end
