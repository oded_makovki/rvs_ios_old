//
//  MoviePlayer.m
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "MoviePlayer.h"

@interface MoviePlayer ()

@property (nonatomic) UIActivityIndicatorView *activityIndicator;

@end

@implementation MoviePlayer

- (id)init
{
    self = [super init];
    
    __weak MoviePlayer *weakSelf = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:weakSelf selector:@selector(updateActivityIndicator:) name:MPMoviePlayerLoadStateDidChangeNotification object:weakSelf];
    [[NSNotificationCenter defaultCenter] addObserver:weakSelf selector:@selector(updateActivityIndicator:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:weakSelf];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:MPMoviePlayerDidExitFullscreenNotification object:weakSelf queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification *notification) {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           [weakSelf play];
                           weakSelf.view.frame = weakSelf.view.superview.bounds;
                       });
    }];
    
    return self;
}

- (void)dealloc
{
    NSLog(@"MoviePlayer dealloc");
    [self stop];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerLoadStateDidChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackStateDidChangeNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerDidExitFullscreenNotification object:self];
}

- (void)setContentURL:(NSURL *)contentURL
{
    [super setContentURL:contentURL];
}

- (void)updateActivityIndicator:(NSNotification*)notification
{
    if (!self.activityIndicator)
    {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        self.activityIndicator.center = self.view.center;
        [self updateActivityIndicator:nil];
        [self.view addSubview:self.activityIndicator];
    }
    
    self.activityIndicator.hidden = (self.loadState & MPMovieLoadStatePlayable) || self.playbackState != MPMoviePlaybackStatePlaying;
    
    if (self.activityIndicator.hidden)
        [self.activityIndicator stopAnimating];
    else
        [self.activityIndicator startAnimating];
}

@end
