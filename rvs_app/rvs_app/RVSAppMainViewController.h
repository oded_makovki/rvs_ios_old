//
//  RVSAppMainViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppTabNavigationController.h"

@interface RVSAppMainViewController : UITabBarController

@property (strong, nonatomic) RVSAppTabNavigationController *tabDiscovery;
@property (strong, nonatomic) RVSAppTabNavigationController *tabFeed;
@property (strong, nonatomic) RVSAppTabNavigationController *tabProfile;
@property (strong, nonatomic) RVSAppTabNavigationController *tabMore;

@end
