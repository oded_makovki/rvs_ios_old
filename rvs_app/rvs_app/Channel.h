//
//  Channel.h
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import UIKit;

@class ChannelInfo;

@interface Channel : NSObject

@property (nonatomic) NSDictionary *channelInfo;
@property (nonatomic, readonly) UIImage *logo;

@end
