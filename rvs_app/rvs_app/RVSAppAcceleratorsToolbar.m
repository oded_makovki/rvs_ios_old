//
//  RVSAppAcceleratorsView.m
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppAcceleratorsToolbar.h"

@interface RVSAppAcceleratorsToolbar()

@property (weak, nonatomic) IBOutlet UIScrollView *accScrollView;
@property (weak, nonatomic) IBOutlet UIView *accContainer;

@end

@implementation RVSAppAcceleratorsToolbar


-(void)awakeFromNib
{
    [super awakeFromNib];
    
    CGRect frame = CGRectMake(0, 0, self.accContainer.frame.size.width, self.accScrollView.frame.size.height);
    
//    _transcriptView = [[RVSAppTranscriptAcceleratorView alloc] initWithFrame:frame];
    _hashTagsView = [[RVSAppHashTagsAcceleratorView alloc] initWithFrame:frame];
    _usersView = [[RVSAppUsersAcceleratorView alloc] initWithFrame:frame];
    
    [self.accScrollView addSubview:_transcriptView];
    [self.accScrollView addSubview:_hashTagsView];
    [self.accScrollView addSubview:_usersView];
    
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_accContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self.accContainer addConstraints:@[ //_transcriptView.widthConstraint,
                                        _hashTagsView.widthConstraint,
                                        _usersView.widthConstraint]];
    
//    [self.accContainer addConstraints:_transcriptView.horizontalConstraints];
    [self.accContainer addConstraints:_hashTagsView.horizontalConstraints];
    [self.accContainer addConstraints:_usersView.horizontalConstraints];
    
//    [self.accContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[transcript]-(0)-[hash]-(0)-[users]-(0)-|" options:0 metrics:nil views:@{@"transcript":_transcriptView, @"hash":_hashTagsView, @"users":_usersView}]];
    
    [self.accContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(0)-[hash]-(0)-[users]-(0)-|"
                                                                              options:0 metrics:nil
                                                                                views:@{ @"hash":_hashTagsView, @"users":_usersView}]];
    
    self.accScrollView.contentOffset = CGPointZero;
    self.accScrollView.pagingEnabled = YES;
    
    [self layoutIfNeeded];
}

-(void)setLayoutInEditVideo:(BOOL)inEditVideo animateWithDuration:(float)duration
{
    
}
//{
//    __weak RVSAppAcceleratorsToolbar *weakSelf = self;
//    
//    self.inEditVideo = inEditVideo;
//    
//    if (inEditVideo)
//    {
//        self.accContainerLeadingConstraint.constant = -self.accThumbnailContainer.frame.size.width;
//    }
//    else
//    {
//        self.accContainerLeadingConstraint.constant = 0;
//    }
//    
//    //update constrains
//    _transcriptView.widthConstraint.constant =
//    _hashTagsView.widthConstraint.constant =
//    _usersView.widthConstraint.constant = self.frame.size.width - (self.accThumbnailContainer.frame.size.width + self.accContainerLeadingConstraint.constant);
//    
//    [_transcriptView invalidateLayout];
//    [_hashTagsView invalidateLayout];
//    [_usersView invalidateLayout];
//    
//    if (duration > 0)
//    {        
//        [UIView animateWithDuration:duration
//                              delay:0
//                            options:0
//                         animations:^{
//                             [weakSelf layoutIfNeeded];
//                             weakSelf.accScrollView.contentOffset = CGPointZero;
//                         }
//                         completion:^(BOOL finished) {
//                             //delay this:
//                             [weakSelf.hashTagsView scrollToTop];
//                             [weakSelf.transcriptView scrollToSelection];
//                         }];
//    }
//    else
//    {
//        [weakSelf layoutIfNeeded];
//        weakSelf.accScrollView.contentOffset = CGPointZero;
//        [weakSelf.hashTagsView scrollToTop];
//        [weakSelf.transcriptView scrollToSelection];
//    }
//}

-(void)showTranscriptAccelerator
{
    [self.accScrollView scrollRectToVisible:self.transcriptView.frame animated:YES];
}

-(void)showUsersAccelerator
{
    [self.accScrollView scrollRectToVisible:self.usersView.frame animated:YES];
    [self.usersView scrollToTop];
}

-(void)showHashTagsAccelerator
{
    [self.accScrollView scrollRectToVisible:self.hashTagsView.frame animated:YES];
    [self.hashTagsView scrollToTop];
}


@end
