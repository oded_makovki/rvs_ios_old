//
//  RVSAppTopBarPostProgressView.m
//  rvs_app
//
//  Created by Barak Harel on 11/27/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTopBarPostProgressView.h"

@implementation RVSAppTopBarPostProgressView


-(void)setThumbnail:(NSObject <RVSAsyncImage> *)thumbnail
{
    //clear old to cancel
    _thumbnail = nil;
    _thumbnail = thumbnail;
    
    if (_thumbnail)
    {
        __weak RVSAppTopBarPostProgressView *weakSelf = self;
        [weakSelf.thumbnail imageWithSize:weakSelf.imageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
            weakSelf.imageView.image = image;
        }];
    }
}

@end
