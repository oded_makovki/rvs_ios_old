//
//  RVSAppFooterLinkCell.h
//  rvs_app
//
//  Created by Barak Harel on 1/6/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import <rvs_sdk_api.h>
#import "RVSAppFooterLinkDataObject.h"

@interface RVSAppFooterLinkCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet UIView *miniImageViewsContainer;
@property (weak, nonatomic) IBOutlet UIImageView *fullImageView;
@property (weak, nonatomic) IBOutlet UIImageView *smallImageView;
@property (weak, nonatomic) IBOutlet UILabel *msgLabel;
@property (weak, nonatomic) IBOutlet UIView *topView;

@property (nonatomic) RVSAppFooterLinkDataObject *data;
-(void)reset;
@end
