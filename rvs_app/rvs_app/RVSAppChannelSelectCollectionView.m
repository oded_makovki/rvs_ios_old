//
//  RVSAppChannelsCollectionView.m
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelSelectCollectionView.h"
#import "RVSApplication.h"
#import "RVSAppChannelSelectCollectionViewCell.h"

@interface RVSAppChannelSelectCollectionView() <UICollectionViewDataSource>

@end

@implementation RVSAppChannelSelectCollectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    self.dataSource = self;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.channels count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"channelCell";
    RVSAppChannelSelectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.channel = self.channels[indexPath.row];
    
    return cell;
}

-(CGPoint)centerWithContentOffset:(CGPoint)offset
{
    CGPoint center = CGPointMake(offset.x, offset.y);
    center.x += self.frame.size.width /2;
    center.y += self.frame.size.height /2;
    
    return center;
}

-(void)scrollToChannelId:(NSString *)channelId animated:(BOOL)animated
{
    NSIndexPath *indexPath = [self indexPathForItemAtPoint:[self centerWithContentOffset:self.contentOffset]];
    NSObject <RVSChannel> *channel;
    
    for (int i=0; i < [self.channels count]; i++)
    {
        channel = self.channels[i];
        if ([channel.channelId isEqualToString:channelId])
        {
            indexPath = [NSIndexPath indexPathForRow:i inSection:0];
            break;
        }
    }
    
    if (indexPath)
        [self scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionNone animated:animated];
}


@end
