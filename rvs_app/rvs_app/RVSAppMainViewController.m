//
//  RVSAppMainViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMainViewController.h"
#import "RVSAppSignInViewController.h"
#import "RVSAppTabNavigationController.h"
#import "RVSAppTopBarView.h"
#import "RVSApplication.h"
#import "RVSAppPushablePostListViewController.h"
#import "RVSAppPushableUserDetailViewController.h"
//#import "RVSAppDiscoveryViewController.h"
#import "RVSAppPushableMomentListViewController.h"
#import "RVSAppCommonViewController.h"
#import "UIColor+RVSApplication.h"

#define MAIN_TAB_DISCOVERY  @"tabDiscovery"
#define MAIN_TAB_FEED       @"tabFeed"
#define MAIN_TAB_PROFILE    @"tabProfile"
#define MAIN_TAB_MORE       @"tabMore"

@interface RVSAppMainViewController () <UITabBarControllerDelegate>
@property (nonatomic) RVSAppTopBarView *topBar;
@end

@implementation RVSAppMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.delegate = self;
    
    [self updateTabs];
    self.tabBar.tintColor = [UIColor rvsTintColor];
    
    CGRect frame = self.view.frame;
    frame.origin.y = 64;
    frame.size.height -=64;
    
    self.topBar = [[RVSAppTopBarView alloc] initWithFrame:frame];
    [self.view addSubview:self.topBar];
    
    [RVSApplication application].topBar = self.topBar;
    [RVSApplication application].mainViewController = self;
    [[RVSApplication application] refreshNetworkStatusAfterDelay:5.0];
    
    if ([UIScreen screens].count > 1)
    {
        NSLog(@"Mirroring ON");
        [RVSApplication application].isMirroring = NO;
    }
    else
    {
        NSLog(@"Mirroring OFF");
        [RVSApplication application].isMirroring = YES;
    }
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [[RVSApplication application].activeClipPlayerView stop];
}


#pragma mark - Tabs

-(void)updateTabs
{
    //no worries, tabs are lazy init
    self.viewControllers = @[self.tabDiscovery, self.tabFeed, self.tabProfile, self.tabMore];
    self.selectedIndex = 0; //initial tab
}

-(RVSAppTabNavigationController *)tabDiscovery
{
    if (!_tabDiscovery)
    {
        _tabDiscovery = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:MAIN_TAB_DISCOVERY];
        _tabDiscovery.tabBarItem.title = NSLocalizedString(@"discovery tab", nil);
        
//        RVSAppDiscoveryViewController *vc = [[RVSAppDiscoveryViewController alloc] init];
//        vc.navigationItem.title = NSLocalizedString(@"discovery title", nil);
//        vc.navigationItem.leftBarButtonItem = [RVSApplication application].topMenuButton;
        
        RVSAppPushableMomentListViewController *vc = [[RVSAppPushableMomentListViewController alloc] init];
        vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithTopMoments];
        vc.navigationItem.title = NSLocalizedString(@"discovery title", nil);
        vc.navigationItem.leftBarButtonItem = [[RVSApplication application] newTopMenuButton];
        
        [_tabDiscovery setViewControllers:@[vc] animated:NO];
    }
    return _tabDiscovery;
}

-(RVSAppTabNavigationController *)tabFeed
{
    if (!_tabFeed)
    {
        _tabFeed = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:MAIN_TAB_FEED];
        _tabFeed.tabBarItem.title = NSLocalizedString(@"myfeed tab", nil);
        
        RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
        
        vc.postListFactory = [[RVSAppPostListFactory alloc] initWithMyFeed];
        vc.navigationItem.title = NSLocalizedString(@"myfeed title", nil);
        vc.navigationItem.leftBarButtonItem = [[RVSApplication application] newTopMenuButton];
        vc.isMyFeed = YES;
        
        [_tabFeed setViewControllers:@[vc] animated:NO];
    }
    return _tabFeed;
}

-(RVSAppTabNavigationController *)tabProfile
{
    if (!_tabProfile)
    {
        _tabProfile = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:MAIN_TAB_PROFILE];
        _tabProfile.tabBarItem.title = NSLocalizedString(@"profile tab", nil);
        
        RVSAppPushableUserDetailViewController *vc = [[RVSAppPushableUserDetailViewController alloc] init];
        
        vc.userId = [RVSSdk sharedSdk].myUserId;
        vc.navigationItem.title = [RVSSdk sharedSdk].myUserId;
        vc.navigationItem.leftBarButtonItem = [[RVSApplication application] newTopMenuButton];
        
        [_tabProfile setViewControllers:@[vc] animated:NO];
    }
    return _tabProfile;
}

-(RVSAppTabNavigationController *)tabMore
{
    if (!_tabMore)
    {
        _tabMore = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:MAIN_TAB_MORE];
        _tabMore.tabBarItem.title = NSLocalizedString(@"more tab", nil);        
    }
    return _tabMore;
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}


@end
