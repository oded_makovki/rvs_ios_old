//
//  RVSAppSettingsViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/13/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppDevSettingsViewController.h"
#import "RVSApplication.h"
#import "RVSSocialManager.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import <RMPickerViewController.h>

extern NSString *RVSSdkBaseUrlUserDefaultsKey;

@interface RVSAppDevSettingsViewController () <RMPickerViewControllerDelegate, UIActionSheetDelegate>
@property (weak, nonatomic) IBOutlet UISwitch *channelDetectSwitch;
@property (weak, nonatomic) IBOutlet UIImageView *channelDetectImageView;
@property (weak, nonatomic) IBOutlet UIButton *backendServerButton;
@property (weak, nonatomic) IBOutlet UILabel *channelDetectTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelDetectChannelNameLabel;

@property (nonatomic) RVSNotificationObserver *channelDetectorMatchObserver;
@property (nonatomic) RVSNotificationObserver *channelDetectorStatusObserver;
@property (nonatomic) RVSNotificationObserver *channelDetectorErrorObserver;

//can also use KVO
@property (nonatomic) RVSNotificationObserver *applicationCurrentChannelObserver;
@property (nonatomic) RVSNotificationObserver *applicationNowTimeOffsetObserver;

@property (weak, nonatomic) IBOutlet UILabel *applicationCurrentChannelLabel;
@property (weak, nonatomic) IBOutlet UILabel *applicationNowTimeOffsetLabel;


@property (nonatomic) NSDictionary *settings;

@property (nonatomic) RMPickerViewController *backendPickerView;
@property (nonatomic) NSMutableArray *backendServers;
@property (nonatomic) NSInteger currentBackendServerIndex;

@property (nonatomic) RVSKeyPathObserver *appPropertiesObserver;

@property (nonatomic) UIActionSheet *shareSheet;
@end

@implementation RVSAppDevSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadSettings];
    [self loadServers];
    
    [self getCurrentBackendServer];
    
    self.channelDetectSwitch.onTintColor = [UIColor rvsTintColor];
    self.navigationItem.title = @"Settings";
    
    self.channelDetectImageView.image = [self.channelDetectImageView.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    
    self.channelDetectImageView.tintColor = [UIColor rvsTintColor];
    self.backendServerButton.backgroundColor = [UIColor rvsTintColor];
    
    [self setKvo];
    [self setObservers];
}

-(NSArray *)navigationRightButtons
{
    return nil; //don't
}

-(void)loadSettings
{
    NSString * settingsPath = [[NSBundle mainBundle] pathForResource:@"RVSAppDevSettings" ofType:@"plist"];
    self.settings = [NSDictionary dictionaryWithContentsOfFile:settingsPath];
}

-(void)loadServers
{
    //default
    NSDictionary *defaultServer = @{@"title" : @"Default"};
    
    self.backendServers = [NSMutableArray arrayWithObject:defaultServer];
    [self.backendServers addObjectsFromArray:self.settings[@"servers"]];
}

-(void)setObservers
{
    __weak RVSAppDevSettingsViewController *weakSelf = self;
    
    self.channelDetectorMatchObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorMatchNotification:notification];
    }];
    
    self.channelDetectorStatusObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorStatusNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorStatusNotification:notification];
    }];
    
    self.channelDetectorErrorObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorErrorNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorErrorNotification:notification];
    }];
    
    self.applicationCurrentChannelObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationCurrentChannelDidChangeNotification object:[RVSApplication application] usingBlock:^(NSNotification *notification) {
        weakSelf.applicationCurrentChannelLabel.text = [NSString stringWithFormat:@"Current channel: %@", [RVSApplication application].currentChannel];
    }];
    
    self.applicationNowTimeOffsetObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationNowTimeOffsetChangeNotification object:[RVSApplication application] usingBlock:^(NSNotification *notification) {
        weakSelf.applicationNowTimeOffsetLabel.text = [NSString stringWithFormat:@"Now time offset: %.2f", [RVSApplication application].nowTimeOffset];
    }];
}

- (void)setKvo
{
    __weak RVSAppDevSettingsViewController *weakSelf = self;
    
    NSArray *props = @[@"settingsDetectChannel",
                       @"isDetectingChannel"];
    
    self.appPropertiesObserver = [[RVSApplication application] newObserverForKeyPaths:props
                                                                              options:NSKeyValueObservingOptionInitial
                                                                           usingBlock:^(id obj, NSString *keyPath, NSDictionary *change) {
                                                                               [weakSelf syncAppProperties];
                                                                           }];
}

-(void)syncAppProperties
{
    self.channelDetectSwitch.on = [RVSApplication application].settingsDetectChannel;
    [self autoSetChannelDetectImageViewTint];
}

#pragma mark - Notifications

-(void)handleChannelDetectorMatchNotification:(NSNotification *)notification
{
    NSObject <RVSChannel> *channel = notification.userInfo[RVSSdkChannelDetectorChannelUserInfoKey];
    
    if (channel)
    {
        [self setChannelDetectImageViewTempTint:[UIColor greenColor]];
        self.channelDetectChannelNameLabel.text = channel.name;
    }
    else //no match
    {
        [self setChannelDetectImageViewTempTint:[UIColor orangeColor]];
        self.channelDetectChannelNameLabel.text = @"Channel: n/a";
    }
    
    NSDate *time = notification.userInfo[RVSSdkChannelDetectorTimeUserInfoKey];
    
    if (time)
    {
        self.channelDetectTimeLabel.text = [time description];
    }
    else
    {
        self.channelDetectTimeLabel.text = @"Time: n/a";
    }
}

-(void)handleChannelDetectorStatusNotification:(NSNotification *)notification
{
    RVSSdkChannelDetectorStatus status = [notification.userInfo[RVSSdkChannelDetectorStatusUserInfoKey] integerValue];
    
    switch (status)
    {
        case RVSSdkChannelDetectorStatusDetecting:
            [self setChannelDetectImageViewTempTint:[UIColor yellowColor]];
            break;
            
            //        case RVSSdkChannelDetectorStatusIdle:
            //            [self setChannelDetectImageViewTempTint:[UIColor cyanColor]];
            //            break;
            //
            //        case RVSSdkChannelDetectorStatusMatching:
            //            [self setChannelDetectImageViewTempTint:[UIColor blueColor]];
            //            break;
            
        default:
            break;
    }
}

-(void)handleChannelDetectorErrorNotification:(NSNotification *)notification
{
    NSError *error = notification.userInfo[RVSSdkChannelDetectorErrorUserInfoKey];
    
    [self setChannelDetectImageViewTint:[UIColor redColor]];
    NSLog(@"Gracenote error: %@", error);
}

#pragma mark - Set color

-(void)autoSetChannelDetectImageViewTint
{
    if ([RVSApplication application].shouldDetectChannel)
    {
        
        [self setChannelDetectImageViewTint:[UIColor rvsTintColor]];
    }
    else
    {
        [self setChannelDetectImageViewTint:[UIColor colorWithWhite:1 alpha:0.8]];
    }
}

-(void)setChannelDetectImageViewTint:(UIColor *)tint
{
    __weak RVSAppDevSettingsViewController *weakSelf = self;
    
    [UIView animateWithDuration:0.3 animations:^{
        weakSelf.channelDetectImageView.tintColor = tint;
    } completion:nil];
}

-(void)setChannelDetectImageViewTempTint:(UIColor *)tint
{
    __weak RVSAppDevSettingsViewController *weakSelf = self;
    
    [UIView animateWithDuration:0.3 delay:0 options:0 animations:^{
        weakSelf.channelDetectImageView.tintColor = tint;
    } completion:^(BOOL finished) {
        [weakSelf autoSetChannelDetectImageViewTint];
    }];
}

- (IBAction)channelDetectValueChanged:(UISwitch *)sender
{
    [RVSApplication application].settingsDetectChannel = sender.on;
}

- (IBAction)logger:(id)sender
{
    RVSLoggerConfigController *loggerController = [[RVSLoggerConfigController alloc] init];
    [self.navigationController pushViewController:loggerController animated:YES];
}

#pragma mark - Servers

- (void)getCurrentBackendServer
{
    self.currentBackendServerIndex = -1;
    
    NSString *baseUrlString = [[NSUserDefaults standardUserDefaults] objectForKey:RVSSdkBaseUrlUserDefaultsKey];
    
    if (!baseUrlString) //not set, assume first server.
    {
        self.currentBackendServerIndex = 0;
        return;
    }
    else
    {
        for (int i = 0; i < [self.backendServers count]; i++)
        {
            if ([self.backendServers[i][@"url"] isEqualToString:baseUrlString])
            {
                self.currentBackendServerIndex = i;
                break;
            }
        }
        
        //fallback
        if (self.currentBackendServerIndex == -1)
        {
            NSDictionary *otherServer = @{@"title" : @"Other", @"url" : baseUrlString};
            [self.backendServers addObject:otherServer];
            self.currentBackendServerIndex = [self.backendServers count] - 1;
        }
    }
}

-(void)setCurrentBackendServerIndex:(NSInteger)currentBackendServerIndex
{
    [self setCurrentBackendServerIndex:currentBackendServerIndex save:NO];
}

-(void)setCurrentBackendServerIndex:(NSInteger)currentBackendServerIndex save:(BOOL)save
{
    if (_currentBackendServerIndex == currentBackendServerIndex)
        return;
    
    //update
    _currentBackendServerIndex = currentBackendServerIndex;
    
    //load data if needed
    if (_currentBackendServerIndex >= 0 && _currentBackendServerIndex < [self.backendServers count])
    {
        NSDictionary *server = self.backendServers[currentBackendServerIndex];
        [self.backendServerButton setTitle:server[@"title"] forState:UIControlStateNormal];
        
        if (save && server) //don't check url as it can be nil (default)
        {
            //save new
            [[NSUserDefaults standardUserDefaults] setObject:server[@"url"] forKey:RVSSdkBaseUrlUserDefaultsKey]; //note - can be nil (will remove the entry)
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Saved backend server url: %@", server[@"url"]);
            
            //prompt user to kill the app
            [self promptForUserRestartWithTitle:[NSString stringWithFormat:@"Will use - %@", server[@"title"]]];
        }
    }
}

- (IBAction)selectBackendServer:(UIButton *)sender
{
    self.backendPickerView = [RMPickerViewController pickerController];
    self.backendPickerView.delegate = self;
    
    //You can enable or disable bouncing and motion effects
    //self.backendPickerView.disableBouncingWhenShowing = YES;
    //self.backendPickerView.disableMotionEffects = YES;
    
    [self.backendPickerView show];
    [self.backendPickerView.picker selectRow:self.currentBackendServerIndex inComponent:0 animated:NO];
}


-(void)promptForUserRestartWithTitle:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:@"Please restart the application to apply your new settings" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

#pragma mark - Picker View Delegate

- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows
{
    NSLog(@"Successfully selected rows: %@", selectedRows);
    if ([selectedRows count])
    {
        NSInteger row = [selectedRows[0] integerValue];
        [self setCurrentBackendServerIndex:row save:YES];
    }
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc
{
    NSLog(@"Selection was canceled");
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == self.backendPickerView.picker)
    {
        return [self.backendServers count];
    }
    else
    {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.backendServers[row][@"title"];
}

#pragma mark - Share test

- (IBAction)showShareOptions:(UIButton *)sender {
    
    self.shareSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook - Quiet", @"Twitter - Quiet",@"Facebook - Dialog", @"Twitter - Dialog", @"Actions Dialog", nil];
    
    [self.shareSheet showInView:[RVSApplication application].mainViewController.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *message = @"Testing 123...";
    NSString *link = @"http://barakharel.com/fb";
    
    RVSSocialManagerRequestCompletionHandler completion = ^(BOOL success, NSError *error)
    {
        UIAlertView *alertView;
        if (success)
        {
            alertView = [[UIAlertView alloc] initWithTitle:@"Shared Successfuly" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        else
        {
             alertView = [[UIAlertView alloc] initWithTitle:@"Share Failed/Canceled" message:error.description delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        }
        
        [alertView show];
    };
    
    switch (buttonIndex)
    {
        case 0:
            [[RVSSocialManager sharedManager] shareToFacebookWithMessage:message link:link completion:completion];
            break;
            
        case 1:
            [[RVSSocialManager sharedManager] shareToTwitterWithMessage:message link:link completion:completion];
            break;
            
        case 2:
            [[RVSSocialManager sharedManager] showShareDialogToFacebookWithMessage:message link:link completion:completion];
            break;
            
        case 3:
            [[RVSSocialManager sharedManager] showShareDialogToTwitterWithMessage:message link:link completion:completion];
            break;
            
        case 4:
            [self showShareActionSheet];
            break;
            
        default:
            break;
    }
}

-(void)showShareActionSheet
{
    UIActivityViewController *activityVC = [[RVSSocialManager sharedManager] activityControllerForMessage:@"Testing..." link:@"http://barakharel.com/fb"];
    
    if (activityVC)
    {
        [activityVC setCompletionHandler:^(NSString *activityType, BOOL completed) {
            NSLog(@"completed dialog - activity: %@ - finished flag: %d", activityType, completed);
            
            NSString *title = [NSString stringWithFormat:@"Action %@", completed ? @"Completed" : @"Canceled"];
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:activityType delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }];
        
        [[RVSApplication application].mainViewController presentViewController:activityVC animated:YES completion:nil];
    }
}


@end
