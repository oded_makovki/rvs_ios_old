//
//  RVSAppAnimatedClipPlayerView.m
//  rvs_app
//
//  Created by Barak Harel on 12/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppAnimatedClipPlayerView.h"
#import "RVSAppClipPlayerView_Private.h"

@interface RVSAppAnimatedClipPlayerView()
@property (nonatomic) CGRect stopFrame;
@property (nonatomic) CGRect playFrame;
@end

@implementation RVSAppAnimatedClipPlayerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppAnimatedClipPlayerView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppAnimatedClipPlayerView];
}

-(void)initRVSAppAnimatedClipPlayerView
{
    self.playFrame = self.videoContainer.frame;
    self.stopFrame = self.thumbnailContainer.frame;
    
    self.clipsToBounds = YES;
}

-(void)playerStateDidChangeToState:(RVSAppClipPlayerViewState)newState oldState:(RVSAppClipPlayerViewState)oldState
{
    [super playerStateDidChangeToState:newState oldState:oldState];

    if (newState == RVSClipViewStatePlaying)
    {
        self.clipView.frame = CGRectMake(self.thumbnailContainer.frame.origin.x -
                                         self.videoContainer.frame.origin.x,
                                         self.thumbnailContainer.frame.origin.y -
                                         self.videoContainer.frame.origin.y,
                                         self.stopFrame.size.width,
                                         self.stopFrame.size.height);
        self.imageView.hidden = YES;
        self.clipView.hidden = NO;
        
        [UIView animateWithDuration:0.7 animations:^{
            self.clipView.frame = CGRectMake(0,
                                             0,
                                             self.playFrame.size.width,
                                             self.playFrame.size.height);
        }];
    }
    else if (newState == RVSClipViewStateStopped || newState == RVSAppClipPlayerViewStateReset)
    {
        self.imageView.hidden = NO;
        self.clipView.hidden = YES;
        
        if (oldState == RVSClipViewStatePlaying && newState != RVSAppClipPlayerViewStateReset)
        {
            self.imageView.frame = CGRectMake(self.videoContainer.frame.origin.x -
                                              self.thumbnailContainer.frame.origin.x,
                                              self.videoContainer.frame.origin.y -
                                              self.thumbnailContainer.frame.origin.y,
                                              self.playFrame.size.width,
                                              self.playFrame.size.height);
            
            [UIView animateWithDuration:0.7 animations:^{
                self.imageView.frame = CGRectMake(0,
                                                  0,
                                                  self.stopFrame.size.width,
                                                  self.stopFrame.size.height);
            }];
        }
        else
        {
            self.imageView.frame = CGRectMake(0,
                                              0,
                                              self.stopFrame.size.width,
                                              self.stopFrame.size.height);
        }
    }
}


@end
