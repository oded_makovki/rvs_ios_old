//
//  RVSAppProgramDataObject.m
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppProgramDataObject.h"

@implementation RVSAppProgramDataObject

-(id)init
{
    self = [super init];
    if (self)
    {
        self.program = nil;
        self.programViewDelegate = nil;
    }
    return self;
}

-(id)initWithProgram:(NSObject<RVSProgram> *)program programViewDelegate:(id<RVSAppProgramViewDelegate>)programViewDelegate
{
    self = [self init];
    if (self)
    {
        self.program = program;
        self.programViewDelegate = programViewDelegate;
    }
    
    return self;
}

+(RVSAppProgramDataObject *)dataObjectsFromProgram:(NSObject<RVSProgram> *)program programViewDelegate:(id<RVSAppProgramViewDelegate>)programViewDelegate
{
    RVSAppProgramDataObject *data = [[RVSAppProgramDataObject alloc] initWithProgram:program programViewDelegate:programViewDelegate];
    return data;
}

+(NSArray *)dataObjectsFromPrograms:(NSArray *)programs programViewDelegate:(id<RVSAppProgramViewDelegate>)programViewDelegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:programs.count];
    for (NSObject <RVSProgram> *program in programs)
    {
        [dataArray addObject:[self dataObjectsFromProgram:program programViewDelegate:programViewDelegate]];
    }
    
    return dataArray;
}


@end
