//
//  RVSAppDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"

@implementation RVSAppDataCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(id)initSizingCellWithFrame:(CGRect)frame
{
    self = [self initWithFrame:frame];
    if (self)
    {
        _isSizingCell = YES;
    }
    return self;
}

-(void)activate
{
    //override this
}

-(void)activateWithVisibleFrame:(CGRect)visibleFrame
{
    //override this
    [self activate];
}


+(CGSize)preferredCellSize
{
    //override this, if zero: will be ignored.
    return CGSizeZero;
}

@end
