//
//  RVSAppPostListFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostListFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppPostListFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostListFooterCell];
    }
    return self;
}

-(void)initRVSAppPostListFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    [self setText:NSLocalizedString(@"post list footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"post list footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"post list footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 40);
}


@end
