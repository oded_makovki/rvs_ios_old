//
//  RVSAppCommonViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppStyledViewController.h"
#import "RVSAppPostView.h"
#import "RVSAppMomentView.h"
#import "RVSAppChannelView.h"
#import "RVSAppProgramView.h"

extern NSString *RVSAppCommonViewControllerReadyNotification;

@interface RVSAppCommonViewController : RVSAppStyledViewController
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@property (nonatomic) IBOutlet UIView *contentView;

-(NSArray *)navigationRightButtons;
@end
