//
//  RVSAppChannelDetailPostsLinkCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDetailMomentsLinkCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppChannelDetailMomentsLinkCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadContentsFromNib];
        self.msgLabel.text = NSLocalizedString(@"channel detail view more" , nil);
        self.backgroundColor = [UIColor darkGrayColor];
    }
    return self;
}


@end
