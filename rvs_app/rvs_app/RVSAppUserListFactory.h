//
//  RVSAppUserListFactory.h
//  rvs_app
//
//  Created by Barak Harel on 12/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppUserListFactoryType)
{
    RVSAppUserListFactoryTypeUserFollowers,
    RVSAppUserListFactoryTypeUserFollowing,
    RVSAppUserListFactoryTypeFavoritedMoment,
    RVSAppUserListFactoryTypeRepostedMoment
};

@interface RVSAppUserListFactory : NSObject

@property (nonatomic, readonly) RVSAppUserListFactoryType type;

-(id)initWithFollowersOfUser:(NSString *)userId;
-(id)initWithFollowedByUser:(NSString *)userId;
-(id)initWithFavoritedMoment:(NSString *)momentId;
-(id)initWithRepostedMoment:(NSString *)momentId;

-(NSObject<RVSAsyncUserList> *)userList;
@end
