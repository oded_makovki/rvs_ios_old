//
//  TTTAttributedLabel+RVSApplication.m
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TTTAttributedLabel+RVSApplication.h"
#import "UIColor+RVSApplication.h"

@implementation TTTAttributedLabel (RVSApplication)

-(void)setDefaultLinkStyleAttributes
{
    self.activeLinkAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [NSNumber numberWithInt: kCTUnderlineStyleSingle], kCTUnderlineStyleAttributeName,
                                 [UIColor rvsPostLinkColor], kCTForegroundColorAttributeName, nil];
    
    self.linkAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithInt: kCTUnderlineStyleNone], kCTUnderlineStyleAttributeName,
                           [UIColor rvsPostLinkColor], kCTForegroundColorAttributeName, nil];
}

@end


