//
//  RVSAppPushableChannelDetailViewController.h
//  rvs_app
//
//  Created by Barak Harel on 12/18/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppMomentListFactory.h"

@interface RVSAppPushableChannelDetailViewController : RVSAppCommonDataViewController

@property (nonatomic) NSObject <RVSChannel> *channel;
@property (nonatomic) RVSAppMomentListFactory *momentListFactory;

- (void)reloadMoments;
@end
