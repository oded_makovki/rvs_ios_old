//
//  RVSAppProgramDetailCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppProgramDetailCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppProgramDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppProgramDetailCell];
    }
    return self;
}

-(void)initRVSAppProgramDetailCell
{
    [self loadContentsFromNib];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 180);
}

@end
