//
//  RVSAppUsersAcceleratorView.m
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUsersAcceleratorView.h"
#import "UIView+NibLoading.h"

@implementation RVSAppUsersAcceleratorView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUsersAcceleratorView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUsersAcceleratorView];
    }
    return self;
}

-(void)initRVSAppUsersAcceleratorView
{
    self.loadingText = NSLocalizedString(@"acc loading users" , nil);
    self.noDataText  = NSLocalizedString(@"acc no users" , nil);
    
    self.errorLabel.text = self.loadingText;
}

@end
