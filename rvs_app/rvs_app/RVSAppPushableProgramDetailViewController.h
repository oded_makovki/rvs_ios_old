//
//  RVSAppPushableProgramDetailViewController.h
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppMomentListFactory.h"

@interface RVSAppPushableProgramDetailViewController : RVSAppCommonDataViewController

@property (nonatomic) NSObject <RVSProgram> *program;
@property (nonatomic) RVSAppMomentListFactory *momentListFactory;

- (void)reloadMoments;
@end
