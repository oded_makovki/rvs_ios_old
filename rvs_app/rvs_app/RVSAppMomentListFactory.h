//
//  RVSAppMomentListFactory.h
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppMomentListFactoryType)
{
    RVSAppMomentListFactoryTypeTopMoments,
    RVSAppMomentListFactoryTypeChannel,
    RVSAppMomentListFactoryTypeProgram,
    RVSAppMomentListFactoryTypeTopic
};

@interface RVSAppMomentListFactory : NSObject
@property (nonatomic, readonly) RVSAppMomentListFactoryType type;

-(id)initWithTopMoments;
-(id)initWithChannel:(NSObject <RVSChannel> *)channel;
-(id)initWithProgram:(NSObject <RVSProgram> *)program;
-(id)initWithTopic:(NSString *)topic;

-(NSObject<RVSAsyncMomentList> *)momentList;

@end
