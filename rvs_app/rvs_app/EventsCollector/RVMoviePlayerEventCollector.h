//
//  RVMoviePlayerEventCollector.h
//  ios_player_sdk
//
//  Created by Barak Harel on 8/26/13.
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "RVMoviePlayerEvent.h"

@interface RVMoviePlayerEventCollector : NSObject

@property (weak, nonatomic) MPMoviePlayerController *moviePlayerController;
@property (nonatomic) NSTimeInterval sampleRate;
@property (strong, nonatomic) NSDictionary *movieInfo;

@property (strong, nonatomic, readonly) NSDate *startTime;
@property (strong, nonatomic, readonly) NSDate *stopTime;
@property (nonatomic, readonly) BOOL isStarted;
@property (nonatomic, readonly) BOOL isCollecting;

-(id)initWithMoviePlayerController:(MPMoviePlayerController *)moviePlayerController;

-(BOOL)start;
-(BOOL)startWithMovieInfo:(NSDictionary *)movieInfo;
-(void)stop;
//-(void)pause;
//-(void)resume;
-(void)clear;
-(NSDictionary *)parseEvents;
-(void)addRecoveryEvent;
-(void)addOtherEventWithText:(NSString *)text;

@end
