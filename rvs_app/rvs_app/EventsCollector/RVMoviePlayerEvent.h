//
//  RVMoviePlayerEvent.h
//  ios_player_sdk
//
//  Created by Barak Harel on 8/27/13.
//
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

typedef enum
{
    RVMoviePlayerEventTypeOther = -1,
    RVMoviePlayerEventTypeAccessLog = 0,
    RVMoviePlayerEventTypeRecovery = -1
}
RVMoviePlayerEventType;

@interface RVMoviePlayerEvent : NSObject
@property (nonatomic, readonly) RVMoviePlayerEventType eventType;
//time stamp
@property (nonatomic) NSTimeInterval dTime;
@property (nonatomic) NSTimeInterval lastPlaybackTime;

//from MPMovieAccessLogEvent
@property (nonatomic) NSTimeInterval segmentsDownloadedDuration;
@property (nonatomic) NSTimeInterval durationWatched;
@property (nonatomic) NSInteger numberOfStalls;
@property (nonatomic) int64_t numberOfBytesTransferred;
@property (nonatomic) double observedBitrate;
@property (nonatomic) double indicatedBitrate;
//from SDK
@property (nonatomic) double bufferSize;
//text
@property (strong, nonatomic) NSString *text;

-(instancetype)initWithEventType:(RVMoviePlayerEventType)eventType;
-(instancetype)initWithAccessLogEvent:(MPMovieAccessLogEvent *)event bufferSize:(double)bufferSize;
-(instancetype)initWithRecoveryEvent;

-(NSString *)stringValue;
-(NSString *)csvValues;
+(NSString *)csvHeaders;

@end
