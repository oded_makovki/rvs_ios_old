//
//  RVMoviePlayerEventCollector.m
//  ios_player_sdk
//
//  Created by Barak Harel on 8/26/13.
//
//

#import "RVMoviePlayerEventCollector.h"

#pragma mark - Consts

#define RVEC_DEFAULT_SAMPLE_RATE                    1.0f
#define RVEC_UNKNOWN_MOVIE_NAME                     @"unknown_movie"

#define RVEC_SETTINGS_FILE                          @"rayv_player_settings.plist"

#define RVEC_SETTINGS_KEY_ROOT                      @"Events collector"
#define RVEC_SETTINGS_KEY_LOGS_ROOT                 @"Logs"
#define RVEC_SETTINGS_KEY_SUMO_ROOT                 @"Sumo"

#define RVEC_SETTINGS_KEY_SERVICE_ENABLED           @"Service enabled"
#define RVEC_SETTINGS_KEY_TIME_ZONE_NAME            @"Time zone name"
#define RVEC_SETTINGS_KEY_DATE_FORMAT               @"Date format"
#define RVEC_SETTINGS_KEY_SAVE_FOLDER_NAME          @"Folder name"
#define RVEC_SETTINGS_KEY_DATA_FILE_NAME_FORMAT     @"Data filename format"
#define RVEC_SETTINGS_KEY_INFO_FILE_NAME_FORMAT     @"Info filename format"
#define RVEC_SETTINGS_KEY_SERVER_ADDRESS            @"Server address"
#define RVEC_SETTINGS_KEY_SHOW_POST_DIALOG          @"Show post dialog"
#define RVEC_SETTINGS_KEY_POST_DIALOG_TITLE         @"Post dialog title"

#pragma mark - Private Interface

@interface RVMoviePlayerEventCollector() <NSURLConnectionDataDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSMutableArray *data; //of RVMoviePlayerEvents
@property (strong, nonatomic) NSString *sessionId;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) NSURLConnection *postConnection;
@property (strong, nonatomic) UIAlertView *postDialog;

@property (strong, nonatomic) NSString *documentsDirectory;
@property (strong, nonatomic) NSDictionary *settings;

@property (strong, nonatomic) NSDictionary *logsSettings;
@property (strong, nonatomic) NSDictionary *sumoSettings;

@end

#pragma mark - Implementation

@implementation RVMoviePlayerEventCollector

-(id)initWithMoviePlayerController:(MPMoviePlayerController *)moviePlayerController
{
    self = [super init];
    if (self)
    {
        _moviePlayerController = moviePlayerController;
        _sampleRate = RVEC_DEFAULT_SAMPLE_RATE;
    }
    
    return self;
}

#pragma mark - Activation: start/stop/pause/resume

-(BOOL)start
{
    if (!_moviePlayerController)
       return NO;
    
    if (_sampleRate <= 0)
        return NO;
    
    if (!self.settings)
        return NO;
    
    [self clear]; //will also stop if collecting
    
    _startTime = [NSDate date];
    _sessionId = [self getSessionId];
    _isStarted = YES;
    
    [self startPooling];
        
    return YES;
}

-(BOOL)startWithMovieInfo:(NSDictionary *)movieInfo
{
    _movieInfo = movieInfo;
    return [self start];
}

-(void)stop
{
    [self stopPooling];
    
    _stopTime = [NSDate date];
    _isStarted = NO;
}

//-(void)pause
//{
//    [self stopPooling];
//}
//
//-(void)resume
//{
//    [self stopPooling];
//}

-(void)startPooling
{
    if (!_isStarted)
        return;
    
    _timer = [NSTimer scheduledTimerWithTimeInterval:_sampleRate target:self selector:@selector(collectLogEvent) userInfo:nil repeats:YES];
    _isCollecting = YES;
}

-(void)stopPooling
{
    if (!_isCollecting)
        return;
    
    [_timer invalidate];
    _timer = nil;
    
    _isCollecting = NO;
}

#pragma mark - Data

-(void)clear
{
    [self stop];
    
    _data = [NSMutableArray array];
    _startTime = 0;
    _stopTime = 0;
}

-(void)collectLogEvent
{
    if (!_moviePlayerController || !_isCollecting)
        return;
    
    MPMovieAccessLogEvent *lastEvent = [_moviePlayerController.accessLog.events lastObject];
    if (lastEvent)
    {
        NSTimeInterval bufferSize = _moviePlayerController.playableDuration - _moviePlayerController.currentPlaybackTime;
        
        if (bufferSize < 0 || isnan(bufferSize))
            bufferSize = 0;
        
        RVMoviePlayerEvent *logEvent = [[RVMoviePlayerEvent alloc] initWithAccessLogEvent:lastEvent bufferSize:bufferSize];
        
        [self addEvent:logEvent];
    }
}

-(void)addRecoveryEvent
{
    if (!_isCollecting)
        return;
    
    RVMoviePlayerEvent *recoveryEvent = [[RVMoviePlayerEvent alloc] initWithRecoveryEvent];
    [self addEvent:recoveryEvent];
}

-(void)addOtherEventWithText:(NSString *)text
{
    if (!_isCollecting)
        return;
    
    RVMoviePlayerEvent *otherEvent = [[RVMoviePlayerEvent alloc] initWithEventType:RVMoviePlayerEventTypeOther];
    otherEvent.text = text;
    [self addEvent:otherEvent];
}

-(void)addEvent:(RVMoviePlayerEvent *)event
{
    if (!_isCollecting)
        return;
    
    //timestamp
    event.dTime = [[NSDate date] timeIntervalSinceDate:_startTime];
    
    //add to data
    [_data addObject:event];
}

-(NSDictionary *)parseEvents
{
    if (!_data || [_data count] <= 0)
        return nil;
    
    //current time for performance info
    CFAbsoluteTime t = CFAbsoluteTimeGetCurrent();
    
    NSArray * dataToParse = [_data copy];
    NSMutableDictionary *parsedData = [NSMutableDictionary dictionary];
    
    NSLog(@"[RVMoviePlayerEventCollector] Parsing %d events...", [dataToParse count]);
    
    if ([[self.logsSettings objectForKey:RVEC_SETTINGS_KEY_SERVICE_ENABLED] boolValue] == YES)
        [self saveCSVFileWithEvents:dataToParse];
    
    if ([[self.sumoSettings objectForKey:RVEC_SETTINGS_KEY_SERVICE_ENABLED] boolValue] == YES)
        [self postDataToSumoFromEvents:dataToParse];
    
    NSLog(@"[RVMoviePlayerEventCollector] Done parsing collected data [%.2f seconds]", CFAbsoluteTimeGetCurrent() - t);

    return parsedData;
}

#pragma mark - Helpers

-(NSString *)getSessionId
{
    CFUUIDRef newUniqueId = CFUUIDCreate(kCFAllocatorDefault);
    NSString * uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(kCFAllocatorDefault, newUniqueId);
    CFRelease(newUniqueId);
    
    return uuidString;
}

#pragma mark - CSV

-(BOOL)saveCSVFileWithEvents:(NSArray *)events
{
    //file handler, init only if data folder exists
    NSFileHandle *fileHandle;
    NSString *saveFolderName = [self.logsSettings objectForKey:RVEC_SETTINGS_KEY_SAVE_FOLDER_NAME];
    if (!saveFolderName)
        return NO;
    
    NSString *savePath = [self.documentsDirectory stringByAppendingPathComponent:saveFolderName];
    
    //if sub folder doesnt exists - skip writing
    if (![[NSFileManager defaultManager] fileExistsAtPath:savePath])
    {
        NSError *error = nil;
        [[NSFileManager defaultManager] createDirectoryAtPath:savePath withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error)
            return NO;
    }
    
    NSString *infoFileNameFormat = [self.logsSettings objectForKey:RVEC_SETTINGS_KEY_INFO_FILE_NAME_FORMAT];
    if (_movieInfo && infoFileNameFormat)
    {
        //open movie info file
        NSString *infoFilename = [NSString stringWithFormat:infoFileNameFormat, [_startTime timeIntervalSinceReferenceDate]];
        NSString *infoSavePath = [savePath stringByAppendingPathComponent:infoFilename];
        
        fileHandle = [self openFileForWritingInPath:infoSavePath];
        if (fileHandle)
        {
            //movie info header
            [fileHandle writeData:[@"Movie Info:\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            //write movie info
            [_movieInfo enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                [fileHandle writeData:[[NSString stringWithFormat:@"%@,%@\n", key, obj] dataUsingEncoding:NSUTF8StringEncoding]];
            }];
            
            //add sessionId to movie info
            [fileHandle writeData:[[NSString stringWithFormat:@"sessionId,%@", self.sessionId] dataUsingEncoding:NSUTF8StringEncoding]];
            
            //close file
            [fileHandle closeFile];
            fileHandle = nil;
        }
    }
    
    
    NSString *dataFileNameFormat = [self.logsSettings objectForKey:RVEC_SETTINGS_KEY_DATA_FILE_NAME_FORMAT];
    if (dataFileNameFormat)
    {
        //open data file
        NSString *dataFilename = [NSString stringWithFormat:dataFileNameFormat, [_startTime timeIntervalSinceReferenceDate]];
        NSString *dataSavePath = [savePath stringByAppendingPathComponent:dataFilename];
        
        fileHandle = [self openFileForWritingInPath:dataSavePath];
        
        //write headers
        [fileHandle writeData:[[RVMoviePlayerEvent csvHeaders] dataUsingEncoding:NSUTF8StringEncoding]];
        
        for (RVMoviePlayerEvent *event in events)
        {
            //write event values (csv)
            [fileHandle writeData:[[event csvValues] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        //close file
        [fileHandle closeFile];
    }
    return YES;
}


-(NSFileHandle *)openFileForWritingInPath:(NSString *)filePath
{
    //create if doesn't exist (probably always)
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:nil attributes:nil];
    
    //open file
    return [NSFileHandle fileHandleForWritingAtPath:filePath];
}

#pragma mark - SUMO

-(BOOL)postDataToSumoFromEvents:(NSArray *)events
{
    //date formatter
    NSDateFormatter *dateFormatter = [self dateFormatterFromSettings:self.sumoSettings];
    NSString *serverAddress = [self.sumoSettings objectForKey:RVEC_SETTINGS_KEY_SERVER_ADDRESS];
    BOOL showPostDialog = [[self.sumoSettings objectForKey:RVEC_SETTINGS_KEY_SHOW_POST_DIALOG] boolValue];
    NSString *postDialogTitle = [self.sumoSettings objectForKey:RVEC_SETTINGS_KEY_POST_DIALOG_TITLE];
    
    //tmp var
    NSDate *eventDate = nil;
    //create one big string
    NSString *stringData = @"";
    
    for (RVMoviePlayerEvent *event in events)
    {
        if ([stringData length] > 0)
            stringData = [NSString stringWithFormat:@"%@\n", stringData];
        
        eventDate = [_startTime dateByAddingTimeInterval:event.dTime];
        stringData = [NSString stringWithFormat:@"%@%@ session_id=%@ %@", stringData, [dateFormatter stringFromDate:eventDate], self.sessionId, [event stringValue]];
    }
    
    if (stringData)
    {
        NSData *data = [stringData dataUsingEncoding:NSUTF8StringEncoding];
        [self postData:data toServer:serverAddress showDialog:showPostDialog withTitle:postDialogTitle];
    }
    
    return YES;
}

-(NSDateFormatter *)dateFormatterFromSettings:(NSDictionary *)settings
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSString *timeZoneName = [settings objectForKey:RVEC_SETTINGS_KEY_TIME_ZONE_NAME];
    if (timeZoneName && [timeZoneName length] > 0)
    {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:timeZoneName]];
    }
    else
    {
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    }
    
    NSString *dateFormat = [settings objectForKey:RVEC_SETTINGS_KEY_DATE_FORMAT];
    if (dateFormat && [dateFormat length] > 0)
    {
        [dateFormatter setDateFormat:dateFormat];
    }
    else
    {
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    }
    
    return dateFormatter;
}

#pragma mark - Post data to server

-(void)postData:(NSData *)data toServer:(NSString *)serverAddress showDialog:(BOOL)showDialog withTitle:(NSString *)title
{
    if (showDialog)
    {
        self.postDialog = [[UIAlertView alloc] initWithTitle:title message:@"Progress: 0%" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles: nil];
        [self.postDialog show];
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:serverAddress]];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:data];
 
    // Create url connection and fire request
    self.postConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

#pragma mark - NSURLConnection Delegate

-(void)connection:(NSURLConnection *)connection didSendBodyData:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite
{
    NSLog(@"[RVMoviePlayerEventCollector] Sending events data to server, written: %d out of: %d", totalBytesWritten, totalBytesExpectedToWrite);
    
    //dialog visible - update message
    if (self.postDialog)
    {
        float perc = 100.0f * totalBytesWritten / totalBytesExpectedToWrite;
        if (perc > 100)
            perc = 100;
        
        [self.postDialog setMessage:[NSString stringWithFormat:@"Progress: %.1f%%", perc]];
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.postConnection = nil;
   NSLog(@"[RVMoviePlayerEventCollector] Completed posting events data to server");
    
    [self.postDialog dismissWithClickedButtonIndex:0 animated:YES];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.postConnection = nil;
   NSLog(@"[RVMoviePlayerEventCollector] Error while posting data to server, error: %@", error.description);
    
    [self.postDialog dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - Documents Directory / Settings

-(NSString *)documentsDirectory
{
    if (!_documentsDirectory)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _documentsDirectory = [paths objectAtIndex:0];
    }
    
    return _documentsDirectory;
}

-(NSDictionary *)settings
{
    if (!_settings)
    {
        //overkill - release services (should not have a value anyway)
        _logsSettings = nil;
        _sumoSettings = nil;
        
        NSDictionary *playerSettings = [NSDictionary dictionaryWithContentsOfFile:[self.documentsDirectory stringByAppendingPathComponent:RVEC_SETTINGS_FILE]];
        if (playerSettings)
        {
            _settings = [playerSettings objectForKey:RVEC_SETTINGS_KEY_ROOT];
            //get services settings
            if (_settings)
                _logsSettings = [_settings objectForKey:RVEC_SETTINGS_KEY_LOGS_ROOT];
                _sumoSettings = [_settings objectForKey:RVEC_SETTINGS_KEY_SUMO_ROOT];
        }
    }
    
    return _settings;
}

#pragma mark - UIAlertView Delegate

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.postConnection)
    {
        [self.postConnection cancel];
        self.postConnection = nil;
    }
}

@end
