//
//  RVMoviePlayerEvent.m
//  ios_player_sdk
//
//  Created by Barak Harel on 8/27/13.
//
//

#import "RVMoviePlayerEvent.h"

@implementation RVMoviePlayerEvent

-(instancetype)initWithEventType:(RVMoviePlayerEventType)eventType
{
    self = [super init];
    if (self)
    {
        _eventType = eventType;
        _text = @"";
    }
    
    return self;
}

-(instancetype)initWithRecoveryEvent
{
    return [self initWithEventType:RVMoviePlayerEventTypeRecovery];
}

-(instancetype)initWithAccessLogEvent:(MPMovieAccessLogEvent *)event bufferSize:(double)bufferSize
{
    self = [self initWithEventType:RVMoviePlayerEventTypeAccessLog];
    if (self)
    {
        self.segmentsDownloadedDuration = event.segmentsDownloadedDuration;
        self.durationWatched = event.durationWatched;
        self.numberOfStalls = event.numberOfStalls;
        self.numberOfBytesTransferred = event.numberOfBytesTransferred;
        self.observedBitrate = event.observedBitrate;
        self.indicatedBitrate = event.indicatedBitrate;
        self.bufferSize = bufferSize;
    }
    return self;
}

-(NSString *)csvValues
{
    NSString *keys = [NSString stringWithFormat:
                      @"%.2f,"
                      "%.2f,"
                      "%d,"
                      "%.2f,"
                      "%.2f,"
                      "%d,"
                      "%.2lld,"
                      "%.2f,"
                      "%.2f,"
                      "%.2f,"
                      "%@\n",
                      
                      _dTime,
                      _lastPlaybackTime,
                      _eventType,
                      _segmentsDownloadedDuration,
                      _durationWatched,
                      _numberOfStalls,
                      _numberOfBytesTransferred,
                      _observedBitrate,
                      _indicatedBitrate,
                      _bufferSize,
                      _text];
    return keys;
}

+(NSString *)csvHeaders
{
    NSString *headers = [NSString stringWithFormat:
                         @"dTime,"
                         "lastPlaybackTime,"
                         "eventType,"
                         "segmentsDownloadedDuration,"
                         "durationWatched,"
                         "numberOfStalls,"
                         "numberOfBytesTransferred,"
                         "observedBitrate,"
                         "indicatedBitrate,"
                         "bufferSize,"
                         "text\n"];
    return headers;
}

-(NSString *)description
{
    NSString *desc = [NSString stringWithFormat:
                      @"dTime: %.2f\n"
                      "lastPlaybackTime: %.2f\n"
                      "eventType: %d\n"
                      "segmentsDownloadedDuration: %.2f\n"
                      "durationWatched: %.2f\n"
                      "numberOfStalls: %d\n"
                      "numberOfBytesTransferred: %.2lld\n"
                      "observedBitrate: %.2f\n"
                      "indicatedBitrate: %.2f\n"
                      "bufferSize: %.2f\n"
                      "text: %@\n",
                      
                      _dTime,
                      _lastPlaybackTime,
                      _eventType,
                      _segmentsDownloadedDuration,
                      _durationWatched,
                      _numberOfStalls,
                      _numberOfBytesTransferred,
                      _observedBitrate,
                      _indicatedBitrate,
                      _bufferSize,
                      _text];
    return desc;
}

-(NSString *)stringValue
{
    NSString *value = [NSString stringWithFormat:
                      @"dTime=%.2f "
                      "lastPlaybackTime=%.2f "
                      "eventType=%d "
                      "segmentsDownloadedDuration=%.2f "
                      "durationWatched=%.2f "
                      "numberOfStalls=%d "
                      "numberOfBytesTransferred=%.2lld "
                      "observedBitrate=%.2f "
                      "indicatedBitrate=%.2f "
                      "bufferSize=%.2f "
                      "text=%@",
                      
                      _dTime,
                      _lastPlaybackTime,
                      _eventType,
                      _segmentsDownloadedDuration,
                      _durationWatched,
                      _numberOfStalls,
                      _numberOfBytesTransferred,
                      _observedBitrate,
                      _indicatedBitrate,
                      _bufferSize,
                      _text];
    return value;
}

@end
