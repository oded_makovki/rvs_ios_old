//
//  RVSAppCompostPostFactory.m
//  rvs_app
//
//  Created by Barak Harel on 1/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppComposePostFactory.h"

@interface RVSAppComposePostFactory()
@property (nonatomic) NSString *text;
@property (nonatomic) NSObject<RVSClip> *clip;
@property (nonatomic) NSObject<RVSProgram> *program;
@property (nonatomic) NSObject<RVSPost> *replyToPost;
@end

@implementation RVSAppComposePostFactory

-(id)initWithPostWithText:(NSString *)text clip:(NSObject<RVSClip> *)clip program:(NSObject<RVSProgram> *)program
{
    self = [super init];
    if (self)
    {
        _type = RVSAppCompostPostFactoryTypePost;
        self.text = text;
        self.clip = clip;
        self.program = program;
    }
    
    return self;
}

-(id)initWithReplyTo:(NSObject<RVSPost> *)replyToPost text:(NSString *)text
{
    self = [super init];
    if (self)
    {
        _type = RVSAppCompostPostFactoryTypeReply;
        self.replyToPost = replyToPost;
        self.text = text;
    }
    
    return self;
}

-(NSObject<RVSAsyncPost> *)asyncPost
{
    switch (self.type)
    {
        case RVSAppCompostPostFactoryTypePost:
            return [[RVSSdk sharedSdk] createPostWithText:self.text clip:self.clip program:self.program];
            break;
            
        case RVSAppCompostPostFactoryTypeReply:
            return [[RVSSdk sharedSdk] createPostInReplyTo:self.replyToPost text:self.text];
            break;
            
        default:
            return nil;
            break;
    }
}

@end
