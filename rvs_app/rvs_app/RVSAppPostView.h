//
//  RVSAppPostCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import <TTTAttributedLabel.h>
#import "RVSAppClipPlayerView.h"
#import "RVSAppUserView.h"

@class RVSAppPostView;
@protocol RVSAppPostViewDelegate <NSObject>

@optional
-(void) postView:(RVSAppPostView *)postView didSelectLinkWithURL:(NSURL *)url;
-(void) postView:(RVSAppPostView *)postView didSelectChannel:(NSObject <RVSChannel> *)channel;
-(void) postView:(RVSAppPostView *)postView didSelectProgram:(NSObject <RVSProgram> *)program;
-(void) postView:(RVSAppPostView *)postView didSelectUser:(NSObject <RVSUser> *)user;
-(void) postView:(RVSAppPostView *)postView didSelectMomentRelatedToPost:(NSObject <RVSPost> *)post;
@end

@interface RVSAppPostView : UIView

@property (weak, nonatomic) IBOutlet RVSAppClipPlayerView *clipPlayerView;

@property (weak, nonatomic) IBOutlet UIView *metadataContainer;
@property (weak, nonatomic) IBOutlet UIButton *channelButton;
@property (weak, nonatomic) IBOutlet UILabel *programNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *programTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *metadataEpisodeLabel;

@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIButton *replyButton;
@property (weak, nonatomic) IBOutlet UIButton *repostToggleButton;
@property (weak, nonatomic) IBOutlet UIButton *favoriteToggleButton;

@property (weak, nonatomic) IBOutlet UIButton *relatedButton;

@property (weak, nonatomic) id <RVSAppPostViewDelegate> delegate;
@property (strong, nonatomic) NSObject <RVSPost> *post;

@property (nonatomic) BOOL shouldParseHotwords;
@property (nonatomic) BOOL isUsingLongTimeFormat;

-(void)reset;

/**
 *  set post for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param post         post object
 *  @param isSizingView set to YES for optimiziation
 */
-(void)setPost:(NSObject <RVSPost> *)post isSizingView:(BOOL)isSizingView;

/**
 *  set post for view. same as setPost:post isSizingView:NO
 *
 *  @param post           post object
 */
-(void)setPost:(NSObject<RVSPost> *)post;
@end
