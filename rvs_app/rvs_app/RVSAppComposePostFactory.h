//
//  RVSAppCompostPostFactory.h
//  rvs_app
//
//  Created by Barak Harel on 1/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppCompostPostFactoryType)
{
    RVSAppCompostPostFactoryTypePost,
    RVSAppCompostPostFactoryTypeReply
};

@interface RVSAppComposePostFactory : NSObject
@property (nonatomic, readonly) RVSAppCompostPostFactoryType type;


-initWithReplyTo:(NSObject <RVSPost> *)replyToPost text:(NSString *)text;
-initWithPostWithText:(NSString *)text clip:(NSObject <RVSClip> *)clip program:(NSObject <RVSProgram> *)program;

-(NSObject <RVSAsyncPost> *)asyncPost;

@end
