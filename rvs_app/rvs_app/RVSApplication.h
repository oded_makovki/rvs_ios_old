//
//  RVSAppData.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>
#import "RVSAppTopBarView.h"
#import "RVSAppClipPlayerView.h"
#import "RVSAppCommonViewController.h"
#import <REMenu.h>

extern NSString *RVSApplicationChannelsDidChangeNotification;
extern NSString *RVSApplicationCurrentChannelDidChangeNotification;

extern NSString *RVSApplicationNowTimeOffsetChangeNotification;

extern NSString *RVSApplicationComposeDidCompleteNotification;
extern NSString *RVSApplicationComposeDidCompleteNewPostUserInfoKey;
extern NSString *RVSApplicationComposeDidCompleteReplyToPostUserInfoKey;

extern NSString *RVSApplicationTopBarDidChangeNotification;
extern NSString *RVSApplicationNetworkStatusDidChangeNotification;

extern NSString *RVSApplicationUserFollowDidChangeNotification;

/*!
 Recieve post
 @param post post object. Can be nil if error
 @param error error information or nil if no error
 */
typedef void (^RVSAppPostResultBlock)(BOOL completed, NSObject <RVSPost> *post);

@interface RVSApplication : NSObject
@property (nonatomic, readonly) UIStoryboard *mainStoryboard;
@property (weak, nonatomic) UITabBarController *mainViewController;

@property (weak, nonatomic) RVSAppTopBarView *topBar;

@property (nonatomic, readonly) BOOL hasNetworkConnection; //to the internet

@property (nonatomic, readonly) NSArray *channelsList;
@property (nonatomic, readonly) NSObject <RVSChannel> *currentChannel;

@property (weak, nonatomic) RVSAppClipPlayerView *activeClipPlayerView;
@property (weak, nonatomic) RVSAppCommonViewController *activeViewController;

@property (nonatomic) BOOL isMirroring;

@property (nonatomic) BOOL settingsDetectChannel;
@property (nonatomic, readonly) BOOL shouldDetectChannel; //the result

@property (nonatomic, readonly) BOOL deviceInLandscape;

@property (nonatomic, readonly) NSDateFormatter *utcDateFormatter;
@property (nonatomic, readonly) NSTimeInterval nowTimeOffset;

@property (nonatomic, readonly) UIBarButtonItem *topSearchButton;
@property (nonatomic, readonly) REMenu *menu;

+ (RVSApplication*)application;
- (NSObject <RVSChannel> *)getChannelById:(NSString *)channelId;

-(UIBarButtonItem *)newTopMenuButton;

-(void)showTopBar;
-(void)hideTopBar;

-(void)refreshNetworkStatus;
-(void)refreshNetworkStatusAfterDelay:(NSTimeInterval)delay;

-(BOOL)isMyUserId:(NSString *)userId;

-(void)showChannelSelect;
-(void)showCompose;
-(void)showReplyTo:(NSObject <RVSPost> *)post;
-(void)showWebViewWithURL:(NSURL *)url;
-(void)logout;
@end
