//
//  RVSAppUserDataCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppUserDataObject.h"

@interface RVSAppUserDataCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;
@property (nonatomic) RVSAppUserDataObject *data;

-(void)reset;
@end
