//
//  RVSAppPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostView.h"
#import "RVSApplication.h"
#import "NSDate+RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import "TTTAttributedLabel+RVSApplication.h"
#import <rvs_sdk_api helpers.h>

@interface RVSAppPostView() <TTTAttributedLabelDelegate>
@property (nonatomic) RVSKeyPathObserver *postPropertiesObserver;

@property (nonatomic) NSObject <RVSAsyncError> *favoriteAsyncError;
@property (nonatomic) NSObject <RVSAsyncError> *repostAsyncError;

@property (nonatomic) NSObject <RVSAsyncImage> *asyncChannelImage;
@property (nonatomic) NSObject <RVSAsyncImage> *asyncProgramImage;

@end

@implementation RVSAppPostView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostView];
    }
    
    return self;
}

-(void)initRVSAppPostView
{
    self.shouldParseHotwords = YES;
    self.isUsingLongTimeFormat = NO;
    
    if (self.textLabel)
    {
        self.textLabel.delegate = self;
        self.textLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
        [self.textLabel setDefaultLinkStyleAttributes];
        
        self.textLabel.textColor        = [UIColor rvsPostTextColor];
        self.timeLabel.textColor        = [UIColor rvsPostTimestampColor];
        self.textLabel.preferredMaxLayoutWidth = self.textLabel.frame.size.width;
        self.textLabel.verticalAlignment = TTTAttributedLabelVerticalAlignmentTop;
    }
    
    if (self.metadataEpisodeLabel)
    {
        self.metadataEpisodeLabel.textColor = [UIColor rvsPostTimestampColor];
    }
    
    if (self.channelButton)
    {
        [self.channelButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
        
        if (self.channelButton.userInteractionEnabled)
        {
            [self.channelButton addTarget:self action:@selector(channelButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    if (self.programNameLabel)
    {
        self.programNameLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:self.programNameLabel.font.pointSize];
        
        self.programNameLabel.numberOfLines = 2;
        self.programNameLabel.preferredMaxLayoutWidth = self.programNameLabel.frame.size.width;
        self.programNameLabel.textColor = [UIColor rvsProgramTextColor];
        
        if (self.programNameLabel.userInteractionEnabled)
        {
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(programLabelSelected)];
            [self.programNameLabel addGestureRecognizer:tap];
        }
    }
    
    if (self.replyButton)
        [self.replyButton addTarget:self action:@selector(reply:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.repostToggleButton)
        [self.repostToggleButton addTarget:self action:@selector(toggleRepost:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.favoriteToggleButton)
        [self.favoriteToggleButton addTarget:self action:@selector(toggleFavorite:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.relatedButton)
        [self.relatedButton addTarget:self action:@selector(relatedButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    if (self.userView.userNameLabel)
        self.userView.userNameLabel.textColor = [UIColor rvsPostUserNameColor];
    
    
    if (self.userView.userThumbnail)
    {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userViewTapped:)];
        [self.userView.userThumbnail addGestureRecognizer:tap];
    }
}

-(void)reset
{
    //clear post
    _post = nil;
    _postPropertiesObserver = nil;
    
    self.textLabel.text = @"";
    self.timeLabel.text = @"";
    self.userView.user = nil;
    
    self.metadataEpisodeLabel.text = @"";
    self.programNameLabel.text = @"";
    
    //set def contraint for size of images
    if (self.channelButton)
    {
        [self.channelButton setImage:nil forState:UIControlStateNormal];
    }
    
    self.relatedButton.enabled = NO;
    self.replyButton.enabled = NO;
    self.repostToggleButton.enabled = NO;
    self.favoriteToggleButton.enabled = NO;
    self.channelButton.enabled = NO;
    
    //clear
    [self.clipPlayerView reset];
}

-(void)setPost:(NSObject<RVSPost> *)post
{
    [self setPost:post isSizingView:NO];
}

-(void)setPost:(NSObject <RVSPost> *)post isSizingView:(BOOL)isSizingView
{
    if (post && _post == post) //reset even if nil
        return;
    
    [self reset];
    _post = post;
    
    if (!_post)
        return;
    
    self.textLabel.preferredMaxLayoutWidth = self.textLabel.frame.size.width;
    self.textLabel.text = [_post.text decodeHtmlEntities];
    
    //updated labels
    [self updateTimeLabelsIsSizingView:isSizingView];
    
    //skip this (for performance optimization)
    if (!isSizingView)
    {
        self.relatedButton.enabled = YES;
        self.replyButton.enabled = YES;
        self.repostToggleButton.enabled = YES;
        self.favoriteToggleButton.enabled = YES;
        self.channelButton.enabled = YES;
        
        if ([[RVSApplication application] isMyUserId:post.author.userId])
        {
            self.repostToggleButton.enabled = NO;
        }
        
        if (self.userView)
        {
            [self.userView setUser:_post.author];
        }        

        if (self.channelButton)
        {
            [self.channelButton.imageView.layer setMinificationFilter:kCAFilterTrilinear];
            
            self.asyncChannelImage = [self.post.channel.logo asyncImage];
        }
        
        if (self.programNameLabel)
        {
            self.programNameLabel.text = self.post.program.content.name;  // [uppercaseString]; //@"PROGRAM PROGRAM PROGRAM";
        }        
        
        if (self.clipPlayerView)
        {
            NSObject <RVSAsyncImageFactory> *imageFactory = _post.thumbnail;
            self.clipPlayerView.thumbnail = [imageFactory asyncImage];
            self.clipPlayerView.clip = _post.clip;
        }
        
        [self parseText];
        [self updateActionButtons];
        [self setKvo];
    }
}

- (void)setKvo
{
    __weak RVSAppPostView *weakSelf = self;
    
    NSArray *props = @[@"isFavoritedByMe", @"isRepostedByMe"];
    self.postPropertiesObserver = [self.post newObserverForKeyPaths:props options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSString *keyPath, NSDictionary *change) {
        [weakSelf updateActionButtons];
    }];
    
}

-(void)updateActionButtons
{
    self.repostToggleButton.selected = [self.post.isRepostedByMe boolValue];
    self.favoriteToggleButton.selected = [self.post.isFavoritedByMe boolValue];
}


-(void)updateTimeLabelsIsSizingView:(BOOL)isSizingView
{
    if (isSizingView)
    {
        self.timeLabel.text = @"t";
        self.programTimeLabel.text = @"t";
        
        if (self.timeLabel.numberOfLines > 1 && self.post.repostingUser)
            self.timeLabel.text = @"t\nu";

        return;
    }
    
    if (self.timeLabel)
    {
        if (!self.post.creationTime)
        {
            self.timeLabel.text = @"";
        }
        else
        {
            if (self.isUsingLongTimeFormat)
            {
                self.timeLabel.text = [self.post.creationTime rvsTimeLongStringSinceNow];
            }
            else
            {
                self.timeLabel.text = [self.post.creationTime timeShortStringSinceNow];
            }
        }
        
        if (self.timeLabel.numberOfLines > 1 && self.post.repostingUser)
        {
            NSString *repostSuffixFormat = NSLocalizedString(@"repost suffix format", nil);
            NSString *repostSuffix = [NSString stringWithFormat:repostSuffixFormat, self.post.repostingUser.name];
            
            self.timeLabel.text = [NSString stringWithFormat:@"%@%@%@",
                                   self.timeLabel.text,
                                   (self.timeLabel.text.length > 0 ? @"\n" : @""),
                                   repostSuffix];
        }
    }
    
    if (self.programTimeLabel)
    {
        if (!self.post.clip.start)
        {
            self.programTimeLabel.text = @"";
        }
        else
        {
            self.programTimeLabel.text = [self.post.clip.start timeShortStringSinceNow];
        }
    }
    
//    NSLog(@"programTimeLabel: %@ [%@]", self.programTimeLabel.text, self.post.program.start);
}

#pragma mark - Parse post

- (void)parseText
{
    if (!self.textLabel.attributedText)
        return;
    
    NSInteger length = self.textLabel.attributedText.length;
    NSString *text = self.textLabel.attributedText.string;
    
    if (self.shouldParseHotwords)
    {
        // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
        
        [userRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "@xxx" user mention found, add a custom link:
             NSString* user = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"user://%@", user]; // build the "user:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];

        // Detect all "#xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
        
        [hashRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "#xxx" hash found, add a custom link:
             NSString* topic = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"hash://%@", topic]; // build the "hash:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];        
    }
}

#pragma mark - Actions

-(void)userViewTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(postView:didSelectUser:)])
    {
        [self.delegate postView:self didSelectUser:self.post.author];
    }
}

-(void)relatedButtonPressed:(UIButton *)sender
{
    if (!self.post)
        return;
    
    if ([self.delegate respondsToSelector:@selector(postView:didSelectMomentRelatedToPost:)])
    {
        [self.delegate postView:self didSelectMomentRelatedToPost:self.post];
    }
}

-(void)channelButtonPressed:(UIButton *)sender
{
    if (!self.post || !self.post.channel)
        return;
    
    if ([self.delegate respondsToSelector:@selector(postView:didSelectChannel:)])
    {
        [self.delegate postView:self didSelectChannel:self.post.channel];
    }
}

-(void)programLabelSelected
{
    if (!self.post || !self.post.program)
        return;
    
    if ([self.delegate respondsToSelector:@selector(postView:didSelectProgram:)])
    {
        [self.delegate postView:self didSelectProgram:self.post.program];
    }
}

-(void)reply:(UIButton *)sender
{
    [[RVSApplication application] showReplyTo:self.post];
}

-(void)toggleRepost:(UIButton *)sender
{
    self.repostToggleButton.selected = !self.repostToggleButton.selected;
    
    __weak RVSAppPostView *weakSelf = self;
    
    self.repostAsyncError = [[RVSSdk sharedSdk] createRepostForPost:self.post.postId];
    [self.repostAsyncError errorUsingBlock:^(NSError *error) {
        
        if (error)
        {
            NSLog(@"error: %@", error);
        }
        
        [weakSelf updateActionButtons];
    }];
}

-(void)toggleFavorite:(UIButton *)sender
{
    self.favoriteToggleButton.selected = !self.favoriteToggleButton.selected;
    
    __weak RVSAppPostView *weakSelf = self;
    
    if (![self.post.isFavoritedByMe boolValue])
        self.favoriteAsyncError = [[RVSSdk sharedSdk] createFavoriteForPost:self.post.postId];
    else
        self.favoriteAsyncError = [[RVSSdk sharedSdk] deleteFavoriteForPost:self.post.postId];
    
    [self.favoriteAsyncError errorUsingBlock:^(NSError *error) {
        
        if (error)
        {
            NSLog(@"error: %@", error);
        }
        
        [weakSelf updateActionButtons];
    }];
}

#pragma mark - Setters

-(void)setAsyncChannelImage:(NSObject<RVSAsyncImage> *)asyncChannelImage
{
    if (asyncChannelImage == _asyncChannelImage)
        return;
    
    _asyncChannelImage = asyncChannelImage;
    
    __weak RVSAppPostView *weakSelf = self;
    [self.asyncChannelImage imageWithSize:weakSelf.channelButton.frame.size usingBlock:^(UIImage *image, NSError *error) {
        
        [weakSelf.channelButton setImage:image forState:UIControlStateNormal];
        
//        [weakSelf addWidthConstraintView:weakSelf.channelButton forImage:image minRatio:0.6];
    }];
}

-(void)addWidthConstraintView:(UIView *)view forImage:(UIImage *)image minRatio:(CGFloat)minRatio
{
    if (!view)
        return;
    
    [view removeConstraints:view.constraints];
    
    CGFloat ratio;
    
    if (image)
        ratio = image.size.height / image.size.width;
    else
        ratio = minRatio;
    
    
    if (ratio < minRatio)
        ratio = minRatio;
    
    NSLayoutConstraint *con1 = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:0 toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1  constant:view.frame.size.height];
    
    NSLayoutConstraint *con2 = [NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeHeight relatedBy:0 toItem:view attribute:NSLayoutAttributeWidth multiplier:ratio constant:0];
    
    [view addConstraints:@[con1,con2]];
}

#pragma mark - TTTAttributedLabel Delegate

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self tryToOpenUrl:url];
}

-(void)tryToOpenUrl:(NSURL *)url
{
    if (url && [self.delegate respondsToSelector:@selector(postView:didSelectLinkWithURL:)])
    {
        [self.delegate postView:self didSelectLinkWithURL:url];
    }
}


@end
