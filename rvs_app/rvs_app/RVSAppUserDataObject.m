//
//  RVSAppUserHeaderData.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDataObject.h"

@implementation RVSAppUserDataObject


-(id)init
{
    self = [super init];
    if (self)
    {
        self.user = nil;
        self.userViewDelegate = nil;
    }
    return self;
}

-(id)initWithUser:(NSObject<RVSUser> *)user userViewDelegate:(id<RVSAppUserViewDelegate>)userViewDelegate
{
    self = [self init];
    if (self)
    {
        self.user = user;
        self.userViewDelegate = userViewDelegate;
    }
    
    return self;
}

+(RVSAppUserDataObject *)dataObjectsFromUser:(NSObject<RVSUser> *)user userViewDelegate:(id<RVSAppUserViewDelegate>)userViewDelegate
{
    RVSAppUserDataObject *data = [[RVSAppUserDataObject alloc] initWithUser:user userViewDelegate:userViewDelegate];
    
    return data;
}

+(NSArray *)dataObjectsFromUsers:(NSArray *)users userViewDelegate:(id<RVSAppUserViewDelegate>)userViewDelegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:users.count];
    for (NSObject <RVSUser> *user in users)
    {
        [dataArray addObject:[self dataObjectsFromUser:user userViewDelegate:userViewDelegate]];
    }
    
    return dataArray;
}



@end
