//
//  RVSAppDiscoveryViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDiscoveryViewController.h"
#import "RVSApplication.h"
#import "RVSAppMomentViewController.h"
#import "RVSAppPushablePostListViewController.h"

@interface RVSAppDiscoveryViewController () <RVSAppPagesViewControllerDataSource, RVSAppPagesViewControllerDelegate>
@property (nonatomic) NSMutableArray *viewControllers;
@end

@implementation RVSAppDiscoveryViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataSource = self;
    self.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.viewControllers)
        [self loadViewControllers];
    
    [self activateViewControllerAtIndex: self.tvPageControl.currentPage];
}

-(void)loadViewControllers
{
    self.viewControllers = [NSMutableArray array];
    
    RVSAppMomentViewController *momentsViewController = [[RVSAppMomentViewController alloc] init];    
    momentsViewController.momentListFactory = [[RVSAppMomentListFactory alloc] initWithTopMoments];
    momentsViewController.navigationItem.title = NSLocalizedString(@"discovery title", nil);
    [self addViewController:momentsViewController];
    
    RVSAppPushablePostListViewController *feedViewController = [[RVSAppPushablePostListViewController alloc] init];
    feedViewController.postListFactory = [[RVSAppPostListFactory alloc] initWithMyFeed];
    feedViewController.navigationItem.title = NSLocalizedString(@"myfeed title", nil);
    feedViewController.isMyFeed = YES;
    
    [self addViewController:feedViewController];
    
    [self loadPages];
}

-(void)addViewController:(RVSAppCommonViewController *)viewController
{
    [self.viewControllers addObject:viewController];
    [self addChildViewController:viewController];
    
    //force layout
    [viewController.view setNeedsLayout];
    
    //disable scroll to top
    RVSAppDataListView *dataListView = [self dataListViewFrom:viewController];
    if (dataListView)
    {
        dataListView.scrollsToTop = NO;
    }
}

#pragma mark - Data Source

-(NSInteger)numberOfViewsFor:(RVSAppPagesViewController *)pageViewController
{
    return [self.viewControllers count];
}

-(UIView *)pageViewController:(RVSAppPagesViewController *)pageViewController viewForIndex:(NSInteger)index
{
    if (index < [self.viewControllers count])
    {
        return ((RVSAppCommonViewController *)self.viewControllers[index]).view;
    }
    
    return nil;
}


-(NSString *)pageViewController:(RVSAppPagesViewController *)pageViewController titleForIndex:(NSInteger)index
{
    if (index < [self.viewControllers count])
    {
        return ((RVSAppCommonViewController *)self.viewControllers[index]).navigationItem.title;
    }
    
    return nil;
}

#pragma mark - Delegate

-(void)pageViewController:(RVSAppPagesViewController *)pageViewController didScrollToIndex:(NSInteger)index
{
    if (index < [self.viewControllers count])
        [self activateViewControllerAtIndex:index];
}

-(void)activateViewControllerAtIndex:(NSInteger)index
{
    [RVSApplication application].activeViewController = self.viewControllers[index];
    
    for (int i = 0; i < [self.viewControllers count]; i++)
    {
        RVSAppCommonViewController *viewController = self.viewControllers[i];
        
        //check if has data list view
        RVSAppDataListView *dataListView = [self dataListViewFrom:viewController];
        
        //activate
        if (dataListView)
        {
            if (i == index)
            {
                dataListView.scrollsToTop = YES;
                [dataListView activateIfNeeded];
            }
            else
            {
                dataListView.scrollsToTop = NO;
            }
        }
    }
}

-(RVSAppDataListView *)dataListViewFrom:(id)object
{
    if ([object respondsToSelector:@selector(dataListView)])
    {
        id dataListView = [object dataListView];
        if ([dataListView isKindOfClass:[RVSAppDataListView class]])
        {
            return dataListView;
        }
        else
        {
            return nil;
        }
    }
    
    return nil;
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
