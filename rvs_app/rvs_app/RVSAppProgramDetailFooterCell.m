//
//  RVSAppProgramDetailFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/18/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppProgramDetailFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppProgramDetailFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppProgramDetailFooterCell];
    }
    return self;
}

-(void)initRVSAppProgramDetailFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    [self setText:NSLocalizedString(@"program detail footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"program detail footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"program detail footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 5);
}

@end
