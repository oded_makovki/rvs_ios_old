//
//  RVSAppAcceleratorView.h
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RVSAppAcceleratorView;
@protocol RVSAppAcceleratorViewDelegate <NSObject>
-(void)acceleratorView:(RVSAppAcceleratorView *)acceleratorView didSelectText:(NSString *)text;
@end


@interface RVSAppAcceleratorView : UIView
@property (weak, nonatomic) id <RVSAppAcceleratorViewDelegate> delegate;

@property (strong, nonatomic, readonly) NSArray *data;
@property (strong, nonatomic, readonly) NSLayoutConstraint *widthConstraint;
@property (strong, nonatomic, readonly) NSArray *horizontalConstraints;

@property (weak, nonatomic) IBOutlet UIView *errorView;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

-(void)loadData:(NSArray *)data;
-(void)reloadData;

-(void)invalidateLayout;
-(NSString *)textPrefix;
-(NSString *)textSuffix;
@end
