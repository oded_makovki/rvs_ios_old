//
//  RVSAppMomentListFactory.m
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentListFactory.h"

@interface RVSAppMomentListFactory()
@property (nonatomic) id parameter;
@end

@implementation RVSAppMomentListFactory

-(id)initWithChannel:(NSObject<RVSChannel> *)channel
{
    self = [super init];
    if (self)
    {
        _type = RVSAppMomentListFactoryTypeChannel;
        _parameter = channel.channelId;
    }
    return self;
}

-(id)initWithProgram:(NSObject<RVSProgram> *)program
{
    self = [super init];
    if (self)
    {
        _type = RVSAppMomentListFactoryTypeProgram;
        _parameter = program.content.contentId;
    }
    return self;
}

-(id)initWithTopic:(NSString *)topic
{
    self = [super init];
    if (self)
    {
        _type = RVSAppMomentListFactoryTypeTopic;
        _parameter = topic;
    }
    return self;
}

-(id)initWithTopMoments
{
    self = [super init];
    if (self)
    {
        _type = RVSAppMomentListFactoryTypeTopMoments;
    }
    return self;
}


-(NSObject<RVSAsyncMomentList> *)momentList
{
    switch (self.type) {
        case RVSAppMomentListFactoryTypeTopMoments:
            return [[RVSSdk sharedSdk] topMoments];
            break;
            
        case RVSAppMomentListFactoryTypeTopic:
            return [[RVSSdk sharedSdk] momentsForTopic:self.parameter];
            break;
            
        case RVSAppMomentListFactoryTypeChannel:
            return [[RVSSdk sharedSdk] momentsForChannel:self.parameter];
            break;
            
        case RVSAppMomentListFactoryTypeProgram:
            return [[RVSSdk sharedSdk] momentsForContent:self.parameter];
            break;
            
        default:
            break;
    }
    
    return nil;
}

@end
