//
//  RVSAppComposeReplyContentCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/27/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostComposeReplyCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@implementation RVSAppPostComposeReplyCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppComposeReplyContentCell];
    }
    
    return self;
}

-(void)initRVSAppComposeReplyContentCell
{
    [self loadContentsFromNib];
    self.postView.shouldParseHotwords = NO;    
    self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1];
    
    self.postView.timeLabel.textColor = [UIColor rvsReplyTimestampColor];
    self.postView.textLabel.textColor = [UIColor rvsReplyTextColor];
    self.postView.userView.userHandleLabel.textColor = [UIColor rvsReplyUserHandleColor];
    self.postView.userView.userNameLabel.textColor = [UIColor rvsReplyUserNameColor];
}

@end
