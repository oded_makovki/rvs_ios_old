//
//  RVSAppChannelHeaderCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelDataCell.h"

@interface RVSAppChannelHeaderCell : RVSAppChannelDataCell

@end
