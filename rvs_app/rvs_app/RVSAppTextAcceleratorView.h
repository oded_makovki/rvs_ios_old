//
//  RVSAppHashTagsAcceleratorView.h
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppAcceleratorView.h"

@interface RVSAppTextAcceleratorView : RVSAppAcceleratorView

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) NSString *loadingText;
@property (nonatomic) NSString *noDataText;
-(void)scrollToTop;
@end
