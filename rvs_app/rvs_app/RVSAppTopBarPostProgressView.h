//
//  RVSAppTopBarPostProgressView.h
//  rvs_app
//
//  Created by Barak Harel on 11/27/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@interface RVSAppTopBarPostProgressView : UIView

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (nonatomic) NSObject <RVSAsyncImage> *thumbnail;
@property (weak, nonatomic) IBOutlet UIImageView *resultImageView;

@end
