//
//  MoviePlayer.h
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import MediaPlayer;

@interface MoviePlayer : MPMoviePlayerController
@property (nonatomic,readonly) UIActivityIndicatorView *activityIndicator;

@end
