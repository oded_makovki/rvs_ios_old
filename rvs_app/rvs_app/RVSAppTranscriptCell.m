//
//  RVSAppTranscriptCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTranscriptCell.h"
#import "UIView+NibLoading.h"
#import "RVSAppSelectionLayoutAttributes.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppTranscriptCell()

@end

@implementation RVSAppTranscriptCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        [self layout];
    }
    return self;
}

-(void)layout
{
    self.backgroundColor = [UIColor darkGrayColor];
    self.textLabel.numberOfLines = 0;
}

-(void)applyLayoutAttributes:(RVSAppSelectionLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    
    if (layoutAttributes.isSelected)
    {
        self.textLabel.textColor = [UIColor rvsTranscriptSelectedColor];
    }
    else
    {
        self.textLabel.textColor = [UIColor rvsTranscriptUnselectedColor];
    }
}

-(void)setHighlighted:(BOOL)highlighted
{    
    [super setHighlighted:highlighted];
    
    if (highlighted)
    {
        self.textLabel.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    }
    else
    {
        self.textLabel.backgroundColor = [UIColor clearColor];
    }
}

@end
