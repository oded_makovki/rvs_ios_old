//
//  RVSAppCommonDataViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"

@interface RVSAppCommonDataViewController ()

@end

@implementation RVSAppCommonDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.dataListView)
    {
        RVSAppDataListView *dataListView = [[RVSAppDataListView alloc] initWithFrame:self.view.frame];
        self.dataListView = dataListView;
        [self.dataListView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.contentView insertSubview:self.dataListView atIndex:0];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.dataListView}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.dataListView}]];
        
        //update
        [self.dataListView layoutIfNeeded];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
