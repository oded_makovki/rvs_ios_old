//
//  RVSAppFooterLinkCell.m
//  rvs_app
//
//  Created by Barak Harel on 1/6/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppFooterLinkCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppFooterLinkCell()
@property (nonatomic) NSObject <RVSAsyncImage> *asyncImage;
@property (nonatomic) NSArray *asyncMiniImages;

@property (nonatomic) NSMutableArray *miniImageViews;
@end

@implementation RVSAppFooterLinkCell


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppFooterLinkCell];
    }
    
    return self;
}

-(void)initRVSAppFooterLinkCell
{
    [self loadContentsFromNib];
    
    [self.fullImageView.layer setMinificationFilter:kCAFilterTrilinear];
    [self.smallImageView.layer setMinificationFilter:kCAFilterTrilinear];
    
    [self reset];
}

-(void)reset
{
    self.fullImageView.image = nil;
    self.smallImageView.image = nil;
    self.msgLabel.text = @"";
}

-(void)setData:(RVSAppFooterLinkDataObject *)data
{
    [super setData:data];
    
    switch (data.type)
    {
        case RVSAppFooterLinkTypeProgram:
            self.asyncImage = [data.program.content.image asyncImage];
            self.topView.backgroundColor = [UIColor blackColor];
            self.msgLabel.text = [NSString stringWithFormat:@"More moments from %@", [data.program.content.name uppercaseString]];
            break;
            
        case RVSAppFooterLinkTypeChannel:
            self.asyncImage = [data.channel.logo asyncImage];
            self.topView.backgroundColor = [UIColor blackColor];
            self.msgLabel.text = [NSString stringWithFormat:@"More moments from %@", [data.channel.name uppercaseString]];
            break;
            
        case RVSAppFooterLinkTypeMomentUsers:
            self.asyncMiniImages = [self asyncImagesFromMoment:data.moment];
            self.topView.backgroundColor = [UIColor whiteColor];
            self.msgLabel.text = @"More posts talking about this";
            break;
            
        default:
            break;
    }
}


-(void)setAsyncMiniImages:(NSArray *)asyncMiniImages
{
    if (asyncMiniImages == _asyncMiniImages)
        return;
    
    _asyncMiniImages = asyncMiniImages;
   
    for (int i = 0; i < [_asyncMiniImages count] && i < [self.miniImageViews count]; i++)
    {
        __weak NSObject <RVSAsyncImage> *asyncImage = _asyncMiniImages[i];
        __weak UIImageView *imageView = self.miniImageViews[i];
        
        [asyncImage imageWithSize:imageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
            imageView.image = image;
        }];
    }
}

-(NSArray *)asyncImagesFromMoment:(NSObject <RVSMoment> *)moment
{
    if (!moment || [moment.topPosts count] == 0)
        return nil;
    
    NSSet *users = [NSSet setWithArray:[moment.topPosts valueForKey:@"author"]];
    NSLog(@"users: %@", users);
    
    NSMutableArray *asyncImages = [NSMutableArray arrayWithCapacity:users.count];
    for (NSObject <RVSUser> *user in users)
    {
        [asyncImages addObject:[user.profileImage asyncImage]];
    }
    
    return asyncImages;
}


-(void)setAsyncImage:(NSObject<RVSAsyncImage> *)asyncImage
{
   if (asyncImage == _asyncImage)
        return;
    
    _asyncImage = asyncImage;
    
    __weak RVSAppFooterLinkCell *weakSelf = self;
    [self.asyncImage imageWithSize:weakSelf.fullImageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
        
        if (image.size.height == 0)
            return;
        
        CGFloat imageAspect = image.size.width / image.size.height;
        CGFloat smallAspect = self.smallImageView.frame.size.width / self.smallImageView.frame.size.height;
        
        if (imageAspect <= smallAspect)
        {
            weakSelf.smallImageView.image = image;
        }
        else
        {
            weakSelf.fullImageView.image = image;
        }
    }];
    
}

-(NSMutableArray *)miniImageViews
{
    if (!_miniImageViews)
    {
        NSInteger num = 7;
         _miniImageViews = [NSMutableArray arrayWithCapacity:num];
        
        //add subviews
        CGFloat size = self.miniImageViewsContainer.frame.size.height;
        
        //calculate spacing:
        //total space = width - [size x number of images]
        //spacing = [total space] / [number of images + 1]
        CGFloat spacing = (self.miniImageViewsContainer.frame.size.width - (size * num)) / (num - 1);
        
        for (int i = 0; i < num; i++)
        {
            UIImageView *miniImageView = [[UIImageView alloc] initWithFrame:CGRectMake(i * (size + spacing), 0, size, size)];
            
            miniImageView.layer.cornerRadius = 5;
            miniImageView.layer.masksToBounds = YES;
            miniImageView.contentMode = UIViewContentModeScaleAspectFill;
            
            [miniImageView.layer setMinificationFilter:kCAFilterTrilinear];
            
            [self.miniImageViews addObject:miniImageView];
            [self.miniImageViewsContainer addSubview:miniImageView];
        }
    }
    
    return _miniImageViews;
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 110);
}

@end
