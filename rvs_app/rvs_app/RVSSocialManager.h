//
//  RVSSocialManager.h
//  rvs_app
//
//  Created by Barak Harel on 2/24/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>

typedef void(^RVSSocialManagerRequestCompletionHandler)(BOOL success, NSError *error);

@interface RVSSocialManager : NSObject

@property (nonatomic, readonly) ACAccount *facebookAccount;
@property (nonatomic, readonly) ACAccount *twitterAccount;

/**
 *  Get shared instanse of the manager
 *
 *  @return shared manager
 */
+(RVSSocialManager *)sharedManager;

-(void)requestTwitterAccountWithCompletion:(RVSSocialManagerRequestCompletionHandler)completion;
-(void)requestFacebookAccountWithCompletion:(RVSSocialManagerRequestCompletionHandler)completion;

-(void)shareToFacebookWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
-(void)shareToTwitterWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;

-(void)showShareDialogToFacebookWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
-(void)showShareDialogToTwitterWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;

-(UIActivityViewController *)activityControllerForMessage:(NSString *)message link:(NSString *)link;

@end
