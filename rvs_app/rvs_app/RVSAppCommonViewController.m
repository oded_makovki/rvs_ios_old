//
//  RVSAppCommonViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import "RVSApplication.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

#import "UIColor+RVSApplication.h"
#import "RVSAppPagesViewController.h"

//headers
#import "RVSAppUserHeaderCell.h"
#import "RVSAppChannelHeaderCell.h"
#import "RVSAppProgramHeaderCell.h"
#import "RVSAppMomentHeaderCell.h"
//posts
#import "RVSAppPostCommentCell.h"

//pushables
#import "RVSAppPushableUserDetailViewController.h"
#import "RVSAppPushablePostListViewController.h"
#import "RVSAppPushablePostDetailViewController.h"
#import "RVSAppPushableUserListViewController.h"
#import "RVSAppPushableChannelDetailViewController.h"
#import "RVSAppPushableProgramDetailViewController.h"
#import "RVSAppPushableMomentListViewController.h"
#import "RVSAppPushableMomentDetailViewController.h"

//webview
#import <SVModalWebViewController.h>
#import <SVWebViewController.h>

NSString *RVSAppCommonViewControllerReadyNotification = @"RVSAppCommonViewControllerReadyNotification";

@interface RVSAppCommonViewController ()

@property (nonatomic) RVSNotificationObserver *composeDidCompleteObserver;
@property (nonatomic) RVSNotificationObserver *topBarDidChangeObserver;
@property (nonatomic) RVSNotificationObserver *networkStatusDidChangeObserver;
@property (nonatomic) RVSNotificationObserver *shouldDetectChannelDidChangeObserver;

@property (nonatomic) UIBarButtonItem *spacer;
@end

@implementation RVSAppCommonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.hidesBottomBarWhenPushed = YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if (!self.contentView)
    {
        UIView *contentView = [[UIView alloc] init];
        self.contentView = contentView;
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self.view insertSubview:self.contentView atIndex:0];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[content]|" options:0 metrics:nil views:@{@"content":self.contentView}]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[content]|" options:0 metrics:nil views:@{@"content":self.contentView}]];
        
        self.contentTopConstraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        [self.view addConstraint:self.contentTopConstraint];

        //update
        [self.contentView layoutIfNeeded];
    }
    
    self.contentView.backgroundColor = [UIColor clearColor];
    
    __weak RVSAppCommonViewController *weakSelf = self;
    
    self.topBarDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationTopBarDidChangeNotification object:nil usingBlock:^(NSNotification *notification)
    {
        [weakSelf topBarUpdatedAnimated:YES];
    }];
    
    self.composeDidCompleteObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationComposeDidCompleteNotification object:nil usingBlock:^(NSNotification *notification)
    {
        NSObject <RVSPost> *post = notification.userInfo[RVSApplicationComposeDidCompleteNewPostUserInfoKey];
        NSObject <RVSPost> *replyToPost = notification.userInfo[RVSApplicationComposeDidCompleteReplyToPostUserInfoKey];
        
        [weakSelf composeDidCompleteWithPost:post inReplyToPost:replyToPost];
    }];
    
    self.networkStatusDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationNetworkStatusDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf networkStatusDidChange];
    }];
    
    //auto note to self
    if (!self.navigationItem.title || [self.navigationItem.title length] == 0)
    {
        self.navigationItem.title = @"NAME ME!";
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self addDefaultRightButtons];
    self.contentTopConstraint.constant = [RVSApplication application].topBar.barHeight;
    
    if (![self isKindOfClass:[RVSAppPagesViewController class]] &&
        ![self conformsToProtocol:@protocol(RVSAppContainedPageViewController)])
    {
            [RVSApplication application].activeViewController = self;
    }
    
    [self topBarUpdatedAnimated:NO];
}

-(void)topBarUpdatedAnimated:(BOOL)animated
{
    [self setContentTopConstraintconstant:[RVSApplication application].topBar.barHeight animated:animated];
}

-(void)setContentTopConstraintconstant:(CGFloat)constant animated:(BOOL)animated
{
    self.contentTopConstraint.constant = constant;
    
    if (animated)
    {
        [UIView animateWithDuration:0.2f animations:^{
            [self.contentView layoutIfNeeded];
        }];
    }
}

#pragma mark - Methods to override

-(void)networkStatusDidChange
{
    //to be overriden
}

-(void)composeDidCompleteWithPost:(NSObject <RVSPost> *)post inReplyToPost:(NSObject <RVSPost> *)replyToPost
{
    //to be overriden
}

#pragma mark - Right Buttons

- (void)addDefaultRightButtons
{
    [self.navigationItem setRightBarButtonItems:[self navigationRightButtons] animated:NO];
}


-(NSArray *)navigationRightButtons
{
    return @[[RVSApplication application].topSearchButton];
}

- (void)searchPressed:(id)sender
{
    //TBD
}

#pragma mark - Pushables:
#pragma mark - Post

-(void)pushPostDetailViewControlWithPost:(NSObject <RVSPost> *)post title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostDetailViewController *vc = [[RVSAppPushablePostDetailViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.post = post;
    
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)pushPostListViewControlWithPostsByUser:(NSObject <RVSUser> *)user title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithUser:user.userId];
    
    vc.headerClass = [RVSAppUserHeaderCell class];
    vc.headerData = [RVSAppUserDataObject dataObjectsFromUser:user userViewDelegate:self];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushPostListViewControlWithFavoritesOfUser:(NSObject<RVSUser> *)user title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithMyFavoritesOfUser:user.userId];
    
    vc.headerClass = [RVSAppUserHeaderCell class];
    vc.headerData = [RVSAppUserDataObject dataObjectsFromUser:user userViewDelegate:self];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushPostListViewControlWithPostsOfMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithMoment:moment.momentId];
    
    vc.headerClass = [RVSAppMomentHeaderCell class];
    vc.headerData = [RVSAppMomentDataObject dataObjectFromMoment:moment momentViewDelegate:self];
    
    vc.postsClass = [RVSAppPostCommentCell class];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - User

-(void)pushUserDetailViewControlWithCachedUser:(NSObject <RVSUser> *)user userId:(NSString *)userId title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserDetailViewController *vc = [[RVSAppPushableUserDetailViewController alloc] init];    
    
    vc.user = user;
    vc.userId = userId;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithFollowersOfUserId:(NSString *)userId title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithFollowersOfUser:userId];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithFollowedByUserId:(NSString *)userId title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithFollowedByUser:userId];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithFavoritesOfMoment:(NSObject<RVSMoment> *)moment title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithFavoritedMoment:moment.momentId];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithRepostsOfMoment:(NSObject<RVSMoment> *)moment title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithRepostedMoment:moment.momentId];
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Channel

-(void)pushChannelDetailViewControllerWithChannel:(NSObject<RVSChannel> *)channel title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableChannelDetailViewController *vc = [[RVSAppPushableChannelDetailViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.channel = channel;
    vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithChannel:channel];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Program

-(void)pushProgramDetailViewControllerWithProgram:(NSObject <RVSProgram> *)program title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableProgramDetailViewController *vc = [[RVSAppPushableProgramDetailViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.program = program;
    vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithProgram:program];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Moment

-(void)pushMomentDetailViewControlWithMoment:(NSObject <RVSMoment> *)moment title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableMomentDetailViewController *vc = [[RVSAppPushableMomentDetailViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.moment = moment;
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushMomentDetailViewControlRelatedToPost:(NSObject <RVSPost> *)post title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableMomentDetailViewController *vc = [[RVSAppPushableMomentDetailViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.relatedToPostId = post.postId;
    
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)pushMomentListViewControlWithTopic:(NSString *)topic title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableMomentListViewController *vc = [[RVSAppPushableMomentListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithTopic:topic];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushMomentListViewControlWithChannel:(NSObject <RVSChannel> *)channel title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableMomentListViewController *vc = [[RVSAppPushableMomentListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithChannel:channel];
    
    vc.headerClass = [RVSAppChannelHeaderCell class];
    vc.headerData = [RVSAppChannelDataObject dataObjectsFromChannel:channel channelViewDelegate:self];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushMomentListViewControlWithProgram:(NSObject <RVSProgram> *)program title:(NSString *)title
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableMomentListViewController *vc = [[RVSAppPushableMomentListViewController alloc] init];
    
    vc.navigationItem.title = title;
    vc.momentListFactory = [[RVSAppMomentListFactory alloc] initWithProgram:program];
    
    vc.headerClass = [RVSAppProgramHeaderCell class];
    vc.headerData = [RVSAppProgramDataObject dataObjectsFromProgram:program programViewDelegate:self];
    
    [self.navigationController pushViewController:vc animated:YES];
}



#pragma mark - UserView Delegate

-(void)userView:(RVSAppUserView *)userView didSelectPostsCountForUser:(NSObject<RVSUser> *)user
{
    [self pushPostListViewControlWithPostsByUser:user title:NSLocalizedString(@"posts title", nil)];
}

-(void)userView:(RVSAppUserView *)userView didSelectFollowingCountForUser:(NSObject<RVSUser> *)user
{
    [self pushUserListViewControlWithFollowedByUserId:user.userId title: NSLocalizedString(@"following title", nil)];
}

-(void)userView:(RVSAppUserView *)userView didSelectFollowersCountForUser:(NSObject<RVSUser> *)user
{
    [self pushUserListViewControlWithFollowersOfUserId:user.userId title: NSLocalizedString(@"followers title", nil)];
}

#pragma mark - MomentView Delegate

-(void)momentView:(RVSAppMomentView *)momentView didSelectUser:(NSObject<RVSUser> *)user
{
    [self pushUserDetailViewControlWithCachedUser:user userId:nil title:[NSString stringWithFormat:@"@%@", user.name]];
}

-(void)momentView:(RVSAppMomentView *)momentView didSelectPostsCountForMoment:(NSObject<RVSMoment> *)moment
{
    [self pushPostListViewControlWithPostsOfMoment:moment title:NSLocalizedString(@"posts title", nil)];
}

-(void)momentView:(RVSAppMomentView *)momentView didSelectFavoritesCountForMoment:(NSObject<RVSMoment> *)moment
{
    [self pushUserListViewControlWithFavoritesOfMoment:moment title:NSLocalizedString(@"favorites title", nil)];
}

-(void)momentView:(RVSAppMomentView *)momentView didSelectRepostsCountForMoment:(NSObject<RVSMoment> *)moment
{
    [self pushUserListViewControlWithRepostsOfMoment:moment title:NSLocalizedString(@"reposts title", nil)];
}

#pragma mark - PostView Delegate

-(void)postView:(RVSAppPostView *)postView didSelectChannel:(NSObject<RVSChannel> *)channel
{
    [self pushChannelDetailViewControllerWithChannel:channel title:channel.name];
}

-(void)postView:(RVSAppPostView *)postView didSelectProgram:(NSObject<RVSProgram> *)program
{
    [self pushProgramDetailViewControllerWithProgram:program title:program.content.name];
}

-(void)postView:(RVSAppPostView *)postView didSelectUser:(NSObject<RVSUser> *)user
{
    [self pushUserDetailViewControlWithCachedUser:user userId:nil title:[NSString stringWithFormat:@"@%@", user.name]];
}

-(void)postView:(RVSAppPostView *)postView didSelectMomentRelatedToPost:(NSObject<RVSPost> *)post
{
    [self pushMomentDetailViewControlRelatedToPost:post title:NSLocalizedString(@"moment title", nil)];
}

-(void)postView:(RVSAppPostView *)postView didSelectLinkWithURL:(NSURL *)url
{
    if ([url.scheme isEqualToString:@"user"])
    {
		NSLog(@"user: %@", [url host]);
        [self pushUserDetailViewControlWithCachedUser:nil userId:[url host] title:[url host]];
	}
    else if ([url.scheme isEqualToString:@"hash"])
    {
		NSLog(@"hash tag: %@", [url host]);
        [self pushMomentListViewControlWithTopic:[url host] title:[NSString stringWithFormat:@"#%@",[url host]]];
	}
    else if ([url.scheme isEqualToString:@"related"])
    {
		NSLog(@"related tag: %@", [url host]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Show moment for post:" message:[url host] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        
        [alert show];
	}
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            // Execute the default behavior, which is opening the URL in Safari for URLs, starting a call for phone numbers, ...
            NSLog(@"Can canOpenURL: %@", url);
            [[RVSApplication application] showWebViewWithURL:url];
        }
        else
        {
            NSLog(@"Cannot canOpenURL: %@", url);
        }
	}
    
    //[[UIApplication sharedApplication] openURL:...]
    
}


#pragma mark - Other

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
