//
//  RVSAppProgramDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppProgramDataCell.h"

@implementation RVSAppProgramDataCell

-(void)setData:(RVSAppProgramDataObject *)data
{
    [super setData:data];
    self.programView.program = data.program;
    self.programView.delegate = data.programViewDelegate;
}

-(void)activate
{
    //TBD
}

-(void)reset
{
    [self.programView reset];
}

@end
