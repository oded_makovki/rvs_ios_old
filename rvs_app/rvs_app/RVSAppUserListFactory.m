//
//  RVSAppUserListFactory.m
//  rvs_app
//
//  Created by Barak Harel on 12/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserListFactory.h"

@interface RVSAppUserListFactory()
@property (nonatomic) id parameter;
@end

@implementation RVSAppUserListFactory

-(id)initWithFollowersOfUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeUserFollowers;
        _parameter = userId;
    }
    return self;
}

-(id)initWithFollowedByUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeUserFollowing;
        _parameter = userId;
    }
    return self;
}

-(id)initWithFavoritedMoment:(NSString *)momentId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeFavoritedMoment;
        _parameter = momentId;
    }
    return self;
}

-(id)initWithRepostedMoment:(NSString *)momentId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeRepostedMoment;
        _parameter = momentId;
    }
    return self;
}

-(NSObject<RVSAsyncUserList> *)userList
{
    switch (self.type)
    {
        case RVSAppUserListFactoryTypeUserFollowers:
            return [[RVSSdk sharedSdk] followersOfUser:self.parameter];
            break;
            
        case RVSAppUserListFactoryTypeUserFollowing:
            return [[RVSSdk sharedSdk] followedByUser:self.parameter];
            break;
            
        case RVSAppUserListFactoryTypeFavoritedMoment:
            return nil;
            break;
            
        case RVSAppUserListFactoryTypeRepostedMoment:
            return nil;
            break;
            
        default:
            return nil;
            break;
    }
}

@end
