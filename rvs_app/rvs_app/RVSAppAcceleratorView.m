//
//  RVSAppAcceleratorView.m
//  rvs_app
//
//  Created by Barak Harel on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppAcceleratorView.h"

@implementation RVSAppAcceleratorView
@synthesize widthConstraint = _widthConstraint;
@synthesize horizontalConstraints = _horizontalConstrains;


-(void)didMoveToSuperview
{
    [super didMoveToSuperview];
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
}

-(NSLayoutConstraint *)widthConstraint
{
    if (!_widthConstraint && self.superview)
    {
        _widthConstraint = [NSLayoutConstraint constraintWithItem:self
                                                       attribute:NSLayoutAttributeWidth
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:nil
                                                       attribute:NSLayoutAttributeNotAnAttribute
                                                      multiplier:1
                                                        constant:self.frame.size.width];

    }
    
    return _widthConstraint;
}

-(NSArray *)horizontalConstraints
{
    if (!_horizontalConstrains)
    {
        _horizontalConstrains = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[view(==30)]-(0)-|"
                                                                        options:0 metrics:nil
                                                                          views:@{@"view":self}];
    }
    
    return _horizontalConstrains;
}

-(void)loadData:(NSArray *)data
{
    _data = data;
    [self reloadData];
}

-(void)reloadData
{
    //override this
}

-(void)invalidateLayout
{
    //override this
}

-(NSString *)textSuffix
{
    return @"";
}

-(NSString *)textPrefix
{
    return @"";
}

@end
