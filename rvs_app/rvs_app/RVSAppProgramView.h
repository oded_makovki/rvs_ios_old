//
//  RVSAppProgramView.h
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@class RVSAppProgramView;
@protocol RVSAppProgramViewDelegate <NSObject>
//TBD
@optional

-(void)programView:(RVSAppProgramView *)programView didSelectPostsCountForProgram:(NSObject <RVSProgram> *)program;
-(void)programView:(RVSAppProgramView *)programView didSelectFollowersCountForProgram:(NSObject <RVSProgram> *)program;
-(void)programView:(RVSAppProgramView *)programView didSelectFollowingCountForProgram:(NSObject <RVSProgram> *)program;

@end


@interface RVSAppProgramView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *programImageView;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;

@property (weak, nonatomic) IBOutlet UILabel *momentsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *momentsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *momentsCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followersCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followingCountTapView;

@property (weak, nonatomic) id <RVSAppProgramViewDelegate> delegate;
@property (nonatomic) NSObject <RVSProgram> *program;

-(void)reset;
@end
