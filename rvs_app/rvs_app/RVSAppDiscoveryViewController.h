//
//  RVSAppDiscoveryViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPagesViewController.h"

@interface RVSAppDiscoveryViewController : RVSAppPagesViewController

@end
