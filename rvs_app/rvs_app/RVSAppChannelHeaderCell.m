//
//  RVSAppChannelHeaderCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelHeaderCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppChannelHeaderCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self loadContentsFromNib];
    }
    return self;
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 60);
}

@end
