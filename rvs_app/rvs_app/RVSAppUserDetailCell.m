//
//  RVSAppUserHeaderCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDetailCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppUserDetailCell()
@end

@implementation RVSAppUserDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppUserDetailCell];
    }
    return self;
}

-(void)initRVSAppUserDetailCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    self.userView.userThumbnail.layer.cornerRadius = 10;
    [self.userView.userThumbnail.layer setBorderColor: [[UIColor darkGrayColor] CGColor]];
    [self.userView.userThumbnail.layer setBorderWidth: 0.5f];
    self.userView.userThumbnail.layer.masksToBounds = YES;
    
    self.userView.userNameLabel.textColor    = [UIColor rvsPostUserNameColor];
    self.userView.userHandleLabel.textColor  = [UIColor rvsPostUserHandleColor];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 118);
}

@end
