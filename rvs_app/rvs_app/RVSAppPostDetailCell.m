//
//  RVSAppDetailPostContentCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostDetailCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppPostDetailCell() <RVSAppClipPlayerViewDelegate>
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mdTopConstraint;

@end

@implementation RVSAppPostDetailCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppDetailPostContentCell];
    }
    
    return self;
}

-(void)initRVSAppDetailPostContentCell
{
    [self loadContentsFromNib];
    
//    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){12.0, 12.0}].CGPath;
//    
//    self.postView.layer.mask = maskLayer;
    
    self.postView.shouldParseHotwords = YES;
    self.postView.isUsingLongTimeFormat = YES;
    self.postView.clipPlayerView.delegate = self;
    
    self.postView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.postView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.postView.programNameLabel.textColor = [UIColor rvsProgramTextColor];
    
    self.postView.timeLabel.preferredMaxLayoutWidth = 300;
    self.postView.textLabel.preferredMaxLayoutWidth = 300;
}


-(void)reset
{
    [super reset];
    self.mdTopConstraint.constant = 0;
}

@end
