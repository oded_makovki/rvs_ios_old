//
//  Channel.m
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "Channel.h"

@implementation Channel

- (UIImage*)logo
{
    return [UIImage imageNamed:self.channelInfo[@"logo"]];
}

@end
