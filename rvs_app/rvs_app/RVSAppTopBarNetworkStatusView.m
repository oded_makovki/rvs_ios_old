//
//  RVSAppTopBarNetworkStatusView.m
//  rvs_app
//
//  Created by Barak Harel on 12/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTopBarNetworkStatusView.h"
#import "UIColor+HexString.h"

@interface RVSAppTopBarNetworkStatusView()

@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end

@implementation RVSAppTopBarNetworkStatusView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppNetworkStatusBar];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppNetworkStatusBar];
}

-(void)initRVSAppNetworkStatusBar
{
    self.textLabel.text = NSLocalizedString(@"network status no network", nil);
}

@end
