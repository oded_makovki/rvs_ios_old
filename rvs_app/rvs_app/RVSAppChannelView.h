//
//  RVSAppChannelView.h
//  rvs_app
//
//  Created by Barak Harel on 11/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@class RVSAppChannelView;
@protocol RVSAppChannelViewDelegate <NSObject>
//TBD
@optional

-(void)channelView:(RVSAppChannelView *)channelView didSelectPostsCountForChannel:(NSObject <RVSChannel> *)channel;
-(void)channelView:(RVSAppChannelView *)channelView didSelectFollowersCountForChannel:(NSObject <RVSChannel> *)channel;
-(void)channelView:(RVSAppChannelView *)channelView didSelectFollowingCountForChannel:(NSObject <RVSChannel> *)channel;
@end

@interface RVSAppChannelView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *channelImageView;
@property (weak, nonatomic) IBOutlet UILabel *channelLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;

@property (weak, nonatomic) IBOutlet UILabel *momentsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *momentsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *momentsCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followersCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followingCountTapView;

@property (weak, nonatomic) id <RVSAppChannelViewDelegate> delegate;
@property (nonatomic) NSObject <RVSChannel> *channel;

-(void)reset;
@end
