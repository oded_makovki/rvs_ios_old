//
//  RVSAppTranscriptView.m
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTranscriptAcceleratorView.h"
#import "RVSAppTranscriptCell.h"
#import "UIView+NibLoading.h"
#import "RVSAppSelectionFlowLayout.h"


@interface RVSAppTranscriptAcceleratorView() <UICollectionViewDataSource, RVSAppSelectionDelegateFlowLayout, UICollectionViewDelegate>

@property (strong, nonatomic) RVSAppTranscriptCell *sizingCell;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) NSDate *selectionStart;
@property (strong, nonatomic) NSDate *selectionEnd;

@end

@implementation RVSAppTranscriptAcceleratorView

-(void)awakeFromNib
{
    [super awakeFromNib];    
    [self initRVSAppTranscriptAcceleratorView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppTranscriptAcceleratorView];
    }
    return self;
}

-(void)initRVSAppTranscriptAcceleratorView
{
    //load from nib
    [self loadContentsFromNib];
    
    RVSAppSelectionFlowLayout *layout = [[RVSAppSelectionFlowLayout alloc] init];
    layout.delegate = self;
    layout.minimumLineSpacing = 0;
    
    self.sizingCell = [[RVSAppTranscriptCell alloc] initWithFrame:CGRectMake(0, 0, self.collectionView.frame.size.width, 5)];
    [self.collectionView registerClass:[RVSAppTranscriptCell class] forCellWithReuseIdentifier:@"Cell"];
    
    
    self.collectionView.collectionViewLayout = layout;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    
    self.errorLabel.text = NSLocalizedString(@"acc loading transcripts" , nil);
    self.errorView.hidden = NO;
    
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionVertical;
}

-(void)invalidateLayout
{
    [super invalidateLayout];
    [self.collectionView.collectionViewLayout invalidateLayout];    
}

-(void)reloadData
{
    if (!self.data)
    {
        self.errorLabel.text = NSLocalizedString(@"acc loading transcripts" , nil);
        self.errorView.hidden = NO;
    }
    else if ([self.data count] == 0)
    {
        self.errorLabel.text = NSLocalizedString(@"acc no transcripts" , nil);
        self.errorView.hidden = NO;
    }
    else
    {
        self.errorView.hidden = YES;
    }
    
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded];
}


#pragma mark - Selection / Scroll

-(void)setSelectionStart:(NSDate *)selectionStart selectionEnd:(NSDate *)selectionEnd animated:(BOOL)animated
{
    if (!self.data)
        return;
    
    self.selectionStart = [selectionStart dateByAddingTimeInterval:self.timeSyncOffset];
    self.selectionEnd = [selectionEnd dateByAddingTimeInterval:self.timeSyncOffset];

    [self.collectionView.collectionViewLayout invalidateLayout];
    [self scrollToSelection];
}

-(void)scrollToSelection
{
    [self scrollTo:self.selectionStart animated:YES];
}

-(BOOL)scrollTo:(NSDate *)date animated:(BOOL)animated
{
//    NSLog(@"Scroll to time: %@ (%.2f)", time, [time timeIntervalSinceDate:self.nowTime]);
    
    BOOL canScroll = NO;
    NSInteger row = [self rowForTime:date];
    
    if (row >= 0)
    {
        canScroll = YES;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
        
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:animated];
    }
    
    return canScroll;
}

-(NSInteger)rowForTime:(NSDate *)time
{
    if (!self.data || [self.data count] <= 0)
        return -1;
    
    NSInteger foundRow = [self.data count] - 1;
    
    for (int i=0; i < [self.data count]; i++)
    {
        foundRow = i;
        
        NSObject <RVSClosedCaption> * d = self.data[i];
        NSTimeInterval diff = [d.start timeIntervalSinceDate:time];

        if (diff >= 0)
        {
            break;
        }
    }
    
    return foundRow;
}

#pragma mark - CollectionView

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.data count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppTranscriptCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSObject <RVSClosedCaption> * data = self.data[indexPath.row];
    [self updateCell:cell withData:data];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject <RVSClosedCaption> * data = self.data[indexPath.row];
    [self updateCell:self.sizingCell withData:data];
    
    CGSize size = [self.sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
//    NSLog(@"sizeForItemAtIndexPath: %d text: %@ size: %@", indexPath.row, data.text, NSStringFromCGSize(size));
    return CGSizeMake(self.collectionView.frame.size.width, size.height);
}

-(void)updateCell:(RVSAppTranscriptCell *)cell withData:(NSObject <RVSClosedCaption> *)data
{
    cell.textLabel.text = data.text;
    cell.textLabel.preferredMaxLayoutWidth = self.collectionView.frame.size.width;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject <RVSClosedCaption> * data = self.data[indexPath.row];
    [self.delegate acceleratorView:self didSelectText:data.text];
}

-(BOOL)collectionView:(UICollectionView *)collectionView layout:(RVSAppSelectionFlowLayout *)collectionViewLayout shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSObject <RVSClosedCaption> * data = self.data[indexPath.row];
    BOOL isSelected = ([data.start timeIntervalSinceDate:self.selectionStart] >= 0 && [data.start timeIntervalSinceDate:self.selectionEnd] <= 0);
    
    return isSelected;
}

#pragma mark -

-(NSString *)textPrefix
{
    return @"\"";
}

-(NSString *)textSuffix
{
    return @"\"";
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end

