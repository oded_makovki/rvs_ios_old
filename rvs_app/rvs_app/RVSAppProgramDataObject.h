//
//  RVSAppProgramDataObject.h
//  rvs_app
//
//  Created by Barak Harel on 12/19/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>
#import "RVSAppProgramView.h"

@interface RVSAppProgramDataObject : NSObject
@property (nonatomic) NSObject <RVSProgram> *program;
@property (weak, nonatomic) id <RVSAppProgramViewDelegate> programViewDelegate;

-(id)initWithProgram:(NSObject <RVSProgram> *)program programViewDelegate:(id <RVSAppProgramViewDelegate>)programViewDelegate;

+(NSArray *)dataObjectsFromPrograms:(NSArray *)programs
                programViewDelegate:(id<RVSAppProgramViewDelegate>)programViewDelegate;

+(RVSAppProgramDataObject *)dataObjectsFromProgram:(NSObject <RVSProgram> *)program programViewDelegate:(id<RVSAppProgramViewDelegate>)programViewDelegate;

@end
