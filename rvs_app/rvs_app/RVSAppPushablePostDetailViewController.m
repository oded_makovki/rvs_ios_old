//
//  RVSAppPushablePostDetailViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushablePostDetailViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import "RVSAppDataListView.h"
#import "RVSAppPostDetailCell.h"
#import "RVSAppPostDetailReplyCell.h"
#import "RVSAppPostDetailFooterCell.h"
#import "RVSApplication.h"
#import "RVSAppPostDataObject.h"

@interface RVSAppPushablePostDetailViewController () <RVSAppDataListViewDelegate, RVSAppPostViewDelegate>

@property (nonatomic) NSObject <RVSAsyncPostList> *repliesList;
@property (nonatomic) NSObject <RVSAsyncPost> *asyncInReplyToPost;
@property (nonatomic) RVSAppPostDataObject *inReplyToPostData;

@property (nonatomic) BOOL shouldClearRepliesData;
@end

@implementation RVSAppPushablePostDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = NSLocalizedString(@"post title", nil);
    
    self.dataListView.numberOfSections = 3; //replyTo, post, replies
    
    [self.dataListView setSectionsCellClass:[RVSAppPostDetailReplyCell class] forSection:0];
    [self.dataListView setSectionsCellClass:[RVSAppPostDetailCell class] forSection:1];
    [self.dataListView setSectionsCellClass:[RVSAppPostDetailReplyCell class] forSection:2];
    
    self.dataListView.footerCellClass = [RVSAppPostDetailFooterCell class];
    self.dataListView.delegate = self;

    [self loadPost];
}

-(void)viewDidAppear:(BOOL)animated
{
    [self loadInReplyToPostOnce];
    [self reloadReplies];
}

- (void)loadPost
{
    //post
    RVSAppPostDataObject *data = [RVSAppPostDataObject dataObjectFromPost:self.post
                                                                postViewDelegate:self];
    [self.dataListView addData:@[data] toSection:1 animated:NO];
}

-(void)loadInReplyToPostOnce
{
    __weak RVSAppPushablePostDetailViewController *weakSelf = self;
    
    if (!self.asyncInReplyToPost && self.post.inReplyToPostId)
    {
        self.asyncInReplyToPost = [[RVSSdk sharedSdk] postForPostId:self.post.inReplyToPostId];
        [self.asyncInReplyToPost postUsingBlock:^(NSObject<RVSPost> *post, NSError *error)
        {
            if (post)
            {
                RVSAppPostDataObject *postData = [RVSAppPostDataObject dataObjectFromPost:post
                                                                         postViewDelegate:weakSelf];
                
                [weakSelf.dataListView addData:@[postData] toSection:0 animated:YES];
//                [weakSelf.dataListView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:NO];
            }
        }];
    }
}


- (void)loadReplies
{
    __weak RVSAppPushablePostDetailViewController *weakSelf = self;

    self.repliesList = [[RVSSdk sharedSdk] repliesForPost:self.post.postId];
    [self.repliesList postsUsingBlock:^(NSArray *posts, NSError *error) {
        
        if (weakSelf.shouldClearRepliesData)
        {
            weakSelf.shouldClearRepliesData = NO;
            [weakSelf.dataListView clearDataInSection:2];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
        }
        else
        {
            if (!posts || [posts count] == 0)
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            }
            else
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
            }
                
            [weakSelf.dataListView addData:[RVSAppPostDataObject dataObjectsFromPosts:posts
                                                                               postViewDelegate:weakSelf]
                                 toSection:2 animated:YES]; //replies
        }
        
    } count:10];
}

-(void)reloadReplies
{
    self.shouldClearRepliesData = YES;
    [self loadReplies];
}

#pragma mark - Reload after post

-(void)composeDidCompleteWithPost:(NSObject <RVSPost> *)post inReplyToPost:(NSObject <RVSPost> *)replyToPost
{
    if (replyToPost.postId == self.post.postId)
    {
        [self reloadReplies];
    }
}

#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    if (indexPath.section == 1) //don't autoplay replies
        return YES;
    
    return NO;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    [self.repliesList nextPosts];
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadReplies];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if (indexPath.section != 1) //do allow click on detailed post
    {
        if ([data isKindOfClass:[RVSAppPostDataObject class]])
        {
            [self pushPostDetailViewControlWithPost:[(RVSAppPostDataObject *)data post] title:NSLocalizedString(@"post title", nil)];
        }
    }
}

#pragma mark - Memory

-(void)dealloc
{
    
}

@end
