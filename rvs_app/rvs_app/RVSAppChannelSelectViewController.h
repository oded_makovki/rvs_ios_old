//
//  RVSAppChannelSelectViewController2.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"

@class RVSAppChannelSelectViewController;
@protocol RVSAppChannelSelectViewControllerDelegate <NSObject>
- (void)channelSelectViewController:(RVSAppChannelSelectViewController *)viewController didSelectChannel:(NSObject <RVSChannel> *)channel;
@end

@interface RVSAppChannelSelectViewController : UIViewController
@property (weak, nonatomic) id <RVSAppChannelSelectViewControllerDelegate> delegate;
@end
