//
//  NSDictionary+NotNull.m
//  rvs_app
//
//  Created by Barak Harel on 12/9/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "NSDictionary+NotNull.h"

@implementation NSDictionary (NotNull)

// in case of [NSNull null] values a nil is returned ...
- (id)objectForKeyNotNull:(id)key
{
    id object = [self objectForKey:key];
    if (object == [NSNull null])
        return nil;
    
    return object;
}

@end
