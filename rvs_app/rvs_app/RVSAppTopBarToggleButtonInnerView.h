//
//  RVSAppTopBarToggleButtonInnerView.h
//  rvs_app
//
//  Created by Barak Harel on 1/14/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppTopBarToggleButtonInnerView : UIView
@property (nonatomic, readonly) UIButton *button;
@property (nonatomic, readonly) UIImageView *backgroundImageView;
@property (nonatomic) BOOL selected;

- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;
- (void)showMatchAnimation;
- (void)showDetectingAnimation;
@end
