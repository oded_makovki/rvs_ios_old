//
//  RVSAppRefreshCollectionView.m
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppRefreshCollectionView.h"

@implementation RVSAppRefreshCollectionView


-(id)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if (self)
    {
        [self initRVSAppRefreshCollectionView];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initRVSAppRefreshCollectionView];
    }
    return self;
}

-(void)initRVSAppRefreshCollectionView
{
    self.alwaysBounceVertical = YES;
}

-(void)willMoveToSuperview:(UIView *)newSuperview
{
    if (self.refreshControl)
    {
        [self.refreshControl removeFromSuperview];
        self.refreshControl = nil;
    }
    
    if (newSuperview)
    {
        PullToRefreshView *refreshControl = [[PullToRefreshView alloc] initWithScrollView: self];
        
        self.refreshControl = refreshControl;
        [self addSubview:self.refreshControl];
    }
}

@end
