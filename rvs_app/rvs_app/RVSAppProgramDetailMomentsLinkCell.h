//
//  RVSAppProgramDetailPostsLinkCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppUserDataObject.h"

@interface RVSAppProgramDetailMomentsLinkCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet UILabel *msgLabel; //needs to be init or set
@end
