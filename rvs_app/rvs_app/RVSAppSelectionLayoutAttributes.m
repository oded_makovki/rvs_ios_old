//
//  RVSAppTimeSelectionLayoutAttributes.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSelectionLayoutAttributes.h"

@implementation RVSAppSelectionLayoutAttributes

- (instancetype)copyWithZone:(NSZone *)zone {
    RVSAppSelectionLayoutAttributes *copy = [super copyWithZone:zone];
    copy.isSelected = self.isSelected;
    return copy;
}

- (BOOL)isEqual:(id)other
{
    if (![super isEqual:other])
        return NO;
    
    //same by super, check same self:
    if (![other isKindOfClass:[RVSAppSelectionLayoutAttributes class]])
        return NO;
    
    if (((RVSAppSelectionLayoutAttributes *)other).isSelected != self.isSelected)
        return NO;
    
    return YES;
}

@end
