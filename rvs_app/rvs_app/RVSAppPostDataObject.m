//
//  RVSAppPostCellData.m
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostDataObject.h"

@implementation RVSAppPostDataObject

-(id)initWithPost:(NSObject<RVSPost> *)post postViewDelegate:(id<RVSAppPostViewDelegate>)postViewDelegate
{
    self = [self init];
    if (self)
    {
        self.postViewDelegate = postViewDelegate;
        self.post = post;
    }
    
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        self.post = nil;
        self.postViewDelegate = nil;
    }
    return self;
}

+(RVSAppPostDataObject *)dataObjectFromPost:(NSObject<RVSPost> *)post postViewDelegate:(id<RVSAppPostViewDelegate>)postViewDelegate
{
    RVSAppPostDataObject *data = [[RVSAppPostDataObject alloc] initWithPost:post postViewDelegate:postViewDelegate];
    
    return data;
}

+(NSArray *)dataObjectsFromPosts:(NSArray *)posts postViewDelegate:(id<RVSAppPostViewDelegate>)postViewDelegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:posts.count];
    for (NSObject <RVSPost> *post in posts)
    {
        [dataArray addObject:[self dataObjectFromPost:post postViewDelegate:postViewDelegate]];
    }
    
    return dataArray;
}

@end
