//
//  SearchController.h
//  InnovaTV
//
//  Created by Barak Harel on 1/26/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultsController : UICollectionViewController
@property (nonatomic) NSString *query;
@end
