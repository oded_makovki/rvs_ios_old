//
//  RVSAppDataListView.m
//  rvs_app
//
//  Created by Barak Harel on 11/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataListView.h"

#import "RVSAppDataCell.h"
#import "RVSAppDataListFooterCell.h"
#import "RVSAppRefreshCollectionView.h"
#import "PullToRefreshView.h"
#import "RVSApplication.h"
#import "RVSAppSnapFlowLayout.h"

#import <rvs_sdk_api helpers.h>

#define POST_VIEW_CELL_ID

@interface RVSAppDataListView() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PullToRefreshViewDelegate>

@property (weak, nonatomic) IBOutlet RVSAppRefreshCollectionView *collectionView;
@property (nonatomic) RVSAppDataCell *sizingCell;

@property (nonatomic) NSDate *lastUpdateDate;
@property (nonatomic) NSMutableArray *sectionsData; //of arrays of RVSAppCellData
@property (nonatomic) NSMutableArray *sectionsClasses; //of classes
@property (nonatomic) NSMutableArray *sectionsIdentifiers; //of nsstrings
//@property (nonatomic) BOOL shouldClearData;

@property (nonatomic) NSInteger footerSection;
@property (nonatomic) RVSAppDataListFooterData *footerData;


@property (nonatomic) BOOL requestedNextPageByTrigger;
@end

@implementation RVSAppDataListView

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {        
        [self initRVSAppDataListView];
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppDataListView];
}

-(void)initRVSAppDataListView
{
    //default
    self.loadPhase = RVSAppDataListLoadPhaseDefault;
    self.refreshEnabled = YES;
    
    if (!self.collectionView) //add be code!
    {
        NSLog(@"Adding new collection view...");
        
        RVSAppSnapFlowLayout *layout = [[RVSAppSnapFlowLayout alloc] init];
//        layout.snapToCell = YES;
        layout.minimumLineSpacing = 0;
        
        RVSAppRefreshCollectionView *collectionView = [[RVSAppRefreshCollectionView alloc] initWithFrame:self.frame collectionViewLayout:layout];
        
        [self addSubview:collectionView];
        
        self.collectionView = collectionView;
        self.collectionView.backgroundColor = [UIColor clearColor];
        
        [self.collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.collectionView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.collectionView}]];
    }
    
    self.nextPageTriggerSection = -1;
    self.nextPageTriggerDistance = 0;
        
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    
    self.footerCellClass = [RVSAppDataListFooterCell class];
    self.footerData = [[RVSAppDataListFooterData alloc] init];
    self.footerData.type = RVSAppDataListFooterTypeDefault;
    
    [self setNumberOfSections:0];
    
    self.collectionView.refreshControl.delegate = self;
    self.collectionView.refreshControl.backgroundColor = [UIColor clearColor];
    self.collectionView.refreshControl.textColor = [UIColor darkGrayColor];
    self.collectionView.refreshControl.shadowColor = [UIColor clearColor];
}

#pragma mark - Data/Sections

-(void)setNumberOfSections:(NSInteger)numberOfSections
{
    _numberOfSections = numberOfSections;
    self.footerSection = _numberOfSections; //if 1 sections, 0 = real section, 1 = footer
    
    self.sectionsClasses = [NSMutableArray array];
    self.sectionsIdentifiers = [NSMutableArray array];
    
    for (int i=0; i < numberOfSections + 1; i++) //sections + footer
    {
        [self.sectionsClasses addObject:[RVSAppDataCell class]]; //for section
        [self.sectionsIdentifiers addObject:@""];
    }
    
    [self clear];
}

-(void)clear:(BOOL)clear andAddData:(NSArray *)newData toSection:(NSInteger)section
{
    if (clear)
    {
        [self clearDataInSection:section];
    }
    [self addData:newData toSection:section];
}

-(void)addData:(NSArray *)newData toSection:(NSInteger)section
{
    [self addData:newData toSection:section animated:YES];
}

-(void)addData:(NSArray *)newData toSection:(NSInteger)section animated:(BOOL)animated
{
    NSAssert(section < self.numberOfSections, @"section out of bounds, set numberOfSections first.");
    
    NSMutableArray *dataArray = self.sectionsData[section];
    int previousCount = [dataArray count];
    
    //calc new index paths
    NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
    for (int i = previousCount; i < previousCount + newData.count; i++)
    {
        [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }

    //add
    [dataArray addObjectsFromArray:newData];
    self.sectionsData[section] = dataArray;
    
//    if (section == 1)
//        NSLog(@"Data: %@",self.sectionsData[1]);
    
    if (animated)
    {
        [self.collectionView performBatchUpdates:^{
            
            if (previousCount == 0) //stupid apple bug, if was empty - can't insert...
            {
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:section]];
            }
            else
            {
                [self.collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
            }
            
        } completion:^(BOOL finished) {
            if (finished)
            {
                //reload footer
                [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:self.footerSection]];
            }
        }];
    }
    else
    {
        [UIView setAnimationsEnabled:NO];
        
        if (previousCount == 0) //stupid apple bug, if was empty - can't insert...
        {
            
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:section]];
        }
        else
        {
            [self.collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
        }
        
        //reload footer
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:self.footerSection]];
        
        [UIView setAnimationsEnabled:YES];
    }
    
    //turn off flag
    if (self.requestedNextPageByTrigger && section == self.nextPageTriggerSection)
        self.requestedNextPageByTrigger = NO;
    
    //finishedLoading must be called only AFTER the new data is set!
    self.lastUpdateDate = [NSDate date];
    [self.collectionView.refreshControl finishedLoading];
    
    [self activateIfNeeded];
}

-(void)clear
{
    [self clearData];
    [self.collectionView reloadData];
}

-(void)clearDataInSection:(NSInteger)section
{
    NSMutableArray *dataArray = self.sectionsData[section];
    
//    if (section == 1)
//        NSLog(@"deleting: %@", dataArray);
    
    [dataArray removeAllObjects];
}

-(void)clearData
{
//    self.shouldClearData = NO;
    self.sectionsData = [NSMutableArray arrayWithCapacity:self.numberOfSections];
    
    for (int i=0; i<self.numberOfSections; i++)
    {
        [self.sectionsData addObject:[NSMutableArray array]]; //for section
    }
}

#pragma mark - CollectionView Data

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = self.sectionsIdentifiers[indexPath.section];
    NSObject *data;
    RVSAppDataCell *dataCell;
    
    if (indexPath.section == self.footerSection)
    {
        dataCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        data = self.footerData;
        
        if (!self.requestedNextPageByTrigger && self.loadPhase == RVSAppDataListLoadPhaseShouldRequestNextData)
            [self.delegate nextDataForDataListView:self];
    }
    else
    {
        dataCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        
        NSMutableArray *dataArray = self.sectionsData[indexPath.section];
        data = dataArray[indexPath.row];
    
        if (indexPath.section == self.nextPageTriggerSection)
        {
            RVSVerbose(@"View item: %i, data length:%i buffer: %d", indexPath.row, dataArray.count, dataArray.count - indexPath.row);
            
            if (!self.requestedNextPageByTrigger &&
                self.loadPhase == RVSAppDataListLoadPhaseShouldRequestNextData &&
                
                self.nextPageTriggerDistance > 0 &&
                indexPath.row >= (int)dataArray.count - self.nextPageTriggerDistance)
            {
                RVSVerbose(@"nextDataForDataListView %@", self.description);
                self.requestedNextPageByTrigger = YES;
                [self.delegate nextDataForDataListView:self];
            }
        }
    }
    
    dataCell.data = data;
    return dataCell;
}

-(NSString *)description
{
    NSString *desc = [super description];
    if ([self.delegate respondsToSelector:@selector(descriptionForDataListView:)])
    {
        desc = [self.delegate descriptionForDataListView:self];
    }
    
    return desc;
}

-(void)setLoadPhase:(RVSAppDataListLoadPhase)loadPhase
{
    _loadPhase = loadPhase;
    switch (_loadPhase) {
            
        case RVSAppDataListLoadPhaseShouldRequestNextData:
            self.footerData.type = RVSAppDataListFooterTypeLoading;
            break;
            
        case RVSAppDataListLoadPhaseEndOfList:
            self.footerData.type = RVSAppDataListFooterTypeEndOfList;
            break;
            
        default:
            self.footerData.type = RVSAppDataListFooterTypeDefault;
            break;
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == self.footerSection)
    {
        return 1;
    }
    else
    {
        NSMutableArray *posts = self.sectionsData[section];
        return [posts count];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.numberOfSections +1;
}

#pragma mark - CollectionView Delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == self.footerSection) //footer
        return;
    
    if (![self.delegate respondsToSelector:@selector(dataListView:didSelectItemAtIndexPath:withData:)])
        return;
    
    NSMutableArray *dataArray = self.sectionsData[indexPath.section];
    NSObject *data = dataArray[indexPath.row];
    
    [self.delegate dataListView:self didSelectItemAtIndexPath:indexPath withData:data];
}


#pragma mark - CollectionView Sizing

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //return value
    CGSize cellSize;
    
    //check section class
    Class cellClass = self.sectionsClasses[indexPath.section];
    
    //check class prefered size:
    if ([cellClass respondsToSelector:@selector(preferredCellSize)])
    {
        cellSize = [cellClass preferredCellSize];
        if (! CGSizeEqualToSize(cellSize, CGSizeZero)) //ignore zero size
            return cellSize;
    }
    
    //use sizingcell and autolayout:
    return [self sizeForItemAtIndexPath:indexPath withCellClass:cellClass];
}

-(CGSize)sizeForItemAtIndexPath:(NSIndexPath *)indexPath withCellClass:(Class)cellClass
{
    //update sizing cell if needed
    if (!self.sizingCell || ![self.sizingCell isKindOfClass:[cellClass class]])
    {
        //update sizing cell
        self.sizingCell = [[cellClass alloc] initSizingCellWithFrame:CGRectMake(0, 0, 320, 40)];
    }
    
    //calculate using sizing cell...
    NSObject *data;
    if (indexPath.section == self.footerSection)
    {
        data = self.footerData;
    }
    else if (indexPath.section < [self.sectionsData count])
    {
        NSMutableArray *dataArray = self.sectionsData[indexPath.section];
        data = dataArray[indexPath.row];
    }
    
    self.sizingCell.data = data;
    CGSize cellSize = [self.sizingCell systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return CGSizeMake(self.collectionView.frame.size.width, cellSize.height);
}

#pragma mark - CollectionView Cells

-(void)setSectionsCellClass:(Class)cellClass forSection:(NSInteger)section;
{
    NSAssert(section < self.numberOfSections, @"section out of bounds, set numberOfSections first.");
    
    [self registerCellClass:cellClass forSection:section];
}

-(void)setFooterCellClass:(Class)footerCellClass
{
    [self registerCellClass:footerCellClass forSection:self.footerSection];
}

-(void)registerCellClass:(Class)cellClass forSection:(NSInteger)section
{
    NSAssert(section < self.numberOfSections || section == self.footerSection, @"section out of bounds, set numberOfSections first.");
    
    NSString *identifier = [NSString stringWithFormat:@"ReuseIdentifier_section_%d_className_%@", section, NSStringFromClass(cellClass)];
    
    //save identifier
    self.sectionsIdentifiers[section] = identifier;
    //save class
    self.sectionsClasses[section] = cellClass;
    //register
    [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

#pragma mark - PullToRefreshView Delegate

-(void)setRefreshEnabled:(BOOL)refreshEnabled
{
    _refreshEnabled = refreshEnabled;
    self.collectionView.refreshControl.enabled = _refreshEnabled;
}

-(void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view
{
    self.loadPhase = RVSAppDataListLoadPhaseRequestingData;
    [self.delegate refreshDataListView:self];
}

-(NSDate *)pullToRefreshViewLastUpdated:(PullToRefreshView *)view
{
    return self.lastUpdateDate;
}

#pragma mark - Scroll

- (void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated

{
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:animated];
}

-(void)scrollToTop
{
    [self.collectionView setContentOffset:CGPointZero];
}

-(void)setScrollsToTop:(BOOL)scrollsToTop
{
    self.collectionView.scrollsToTop = scrollsToTop;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self performSelector:@selector(scrollViewDidEndScrollingAnimation:) withObject:scrollView afterDelay:0.3 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
    [self activateIfNeeded];
}

-(void)activateIfNeeded
{
//    NSLog(@"activateIfNeeded");
    if (![self.delegate respondsToSelector:@selector(dataListView:shouldActivateItemAtIndexPath:)])
        return;
    
    //find indexPath of item
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:CGPointMake(self.collectionView.frame.size.width / 2, self.collectionView.contentOffset.y + (self.collectionView.frame.size.height / 2))];
    
    //item found and not in footer
    if (indexPath && indexPath.section != self.footerSection)
    {
        //should I activate?
        if([self.delegate dataListView:self shouldActivateItemAtIndexPath:indexPath])
        {
            //find cell
            RVSAppDataCell *cell = (RVSAppDataCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            
            //find visible frame
            CGFloat y = MAX(0, self.collectionView.contentOffset.y - cell.frame.origin.y);
            CGFloat h = MIN(cell.frame.size.height - y,
                            self.collectionView.contentOffset.y + self.collectionView.frame.size.height - cell.frame.origin.y);
            
            CGRect visibleFrame = CGRectMake(0, y, self.collectionView.frame.size.width, h);
            
//        NSLog(@"cell: %@ visible: %@",
//              NSStringFromCGRect(cell.frame),
//              NSStringFromCGRect(visibleFrame));
            
            //call
            [cell activateWithVisibleFrame:visibleFrame];
        }
    }
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end
