//
//  RVSAppStyledNavigationController.m
//  rvs_app
//
//  Created by Barak Harel on 11/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppStyledNavigationController.h"

@interface RVSAppStyledNavigationController ()
@property (nonatomic) UIImageView *line;
@end

@implementation RVSAppStyledNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationBar.translucent = NO;
    self.navigationBar.barTintColor = [UIColor whiteColor];
    self.navigationBar.shadowImage = nil;
    
//    self.line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greyLine"]];
//    [self.line setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
//    [self.view addSubview:self.line];
//    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[line]-0-|" options:0 metrics:nil views:@{@"line":self.line}]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
//    
//    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.line attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.navigationBar attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    
    [self.navigationBar setBackIndicatorImage: [UIImage imageNamed:@"icon_back"]];
    [self.navigationBar setBackIndicatorTransitionMaskImage: [UIImage imageNamed:@"icon_back"]];
    
//    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];    
//    [self.navigationBar setShadowImage:[[UIImage alloc] init]];
    
}


@end
