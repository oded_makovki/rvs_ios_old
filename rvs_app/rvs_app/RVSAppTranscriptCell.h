//
//  RVSAppTranscriptCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppTranscriptCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end
