//
//  RVSAppPushableUserViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableMomentDetailViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import "RVSAppMomentDetailCell.h"
#import "RVSAppPostCommentCell.h"
#import "RVSAppFooterLinkCell.h"
#import "RVSAppUserDetailFooterCell.h"
#import "RVSAppMomentDataObject.h"
#import "RVSApplication.h"

#define HEADER_SECTION 0
#define POSTS_LIST_SECTION 1
#define POSTS_LINK_SECTION 2
#define PROGRAM_LINK_SECTION 3
#define CHANNEL_LINK_SECTION 4

@interface RVSAppPushableMomentDetailViewController () <RVSAppDataListViewDelegate, RVSAppPostViewDelegate>
@property (nonatomic) NSObject <RVSAsyncPostList> *postList;

@property (nonatomic) NSObject <RVSAsyncMoment> *asyncMoment;
@property (nonatomic) NSObject <RVSAsyncError> *followAsyncError;
@end

@implementation RVSAppPushableMomentDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.dataListView.refreshEnabled = NO;
    
    self.dataListView.numberOfSections = 5; //moment, posts, 3 links
    
    [self.dataListView setSectionsCellClass:[RVSAppMomentDetailCell class] forSection:HEADER_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppPostCommentCell class] forSection:POSTS_LIST_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppFooterLinkCell class] forSection:POSTS_LINK_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppFooterLinkCell class] forSection:PROGRAM_LINK_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppFooterLinkCell class] forSection:CHANNEL_LINK_SECTION];
    
    self.dataListView.footerCellClass = [RVSAppUserDetailFooterCell class];
    self.dataListView.delegate = self;

    [self refresh];
}

-(void)refresh
{
    if (self.moment)
    {
        [self loadMomentAndRelated:YES];
        [self reloadMoment];
    }
    else if (self.relatedToPostId)
    {
        [self loadAsyncMomentAndRelated:YES];
    }
}

-(void)loadMomentAndRelated:(BOOL)andRelated
{
    if (!self.moment)
        return;
    
    //clear old
    [self.dataListView clearDataInSection:HEADER_SECTION];
    
    //new data from current/empty user
    RVSAppMomentDataObject *data = [RVSAppMomentDataObject dataObjectFromMoment:self.moment momentViewDelegate:self];
    
    //add data
    [self.dataListView addData:@[data] toSection:HEADER_SECTION];
    
    //related
    if (andRelated)
    {
        [self loadLinks];
        [self loadPosts];
    }
}

-(void)reloadMoment
{
    [self loadAsyncMomentAndRelated:NO];
}

-(void)loadAsyncMomentAndRelated:(BOOL)andRelated
{
    __weak RVSAppPushableMomentDetailViewController *weakSelf = self;
    
    //remove old
    self.asyncMoment = nil;
    
    if (self.moment)
    {
        self.asyncMoment = [[RVSSdk sharedSdk] momentForMomentId:self.moment.momentId];
    }
    else if (self.relatedToPostId)
    {
        self.asyncMoment = [[RVSSdk sharedSdk] momentForPost:self.relatedToPostId];
    }
    
    [self.asyncMoment momentUsingBlock:^(NSObject<RVSMoment> *moment, NSError *error) {
        
        if (error || !moment) //keep cached if available
         return;

        weakSelf.moment = moment;
        [weakSelf loadMomentAndRelated:andRelated];
     }];
}

-(void)loadLinks
{
    if ([self.moment.topPosts count] > 0)
    {
        NSObject <RVSPost> *topPost = self.moment.topPosts[0];
     
        [self.dataListView clearDataInSection:POSTS_LINK_SECTION];
        RVSAppFooterLinkDataObject *postsLinkData = [[RVSAppFooterLinkDataObject alloc] initWithUsersOfMoment:self.moment];        
        [self.dataListView addData:@[postsLinkData] toSection:POSTS_LINK_SECTION];
        
        
        [self.dataListView clearDataInSection:PROGRAM_LINK_SECTION];
        RVSAppFooterLinkDataObject *programLinkData = [[RVSAppFooterLinkDataObject alloc] initWithProgram:topPost.program];
        [self.dataListView addData:@[programLinkData] toSection:PROGRAM_LINK_SECTION];
        
        
        [self.dataListView clearDataInSection:CHANNEL_LINK_SECTION];
        RVSAppFooterLinkDataObject *channelLinkData = [[RVSAppFooterLinkDataObject alloc] initWithChannel:topPost.channel];
        [self.dataListView addData:@[channelLinkData] toSection:CHANNEL_LINK_SECTION];
    }
}

- (void)loadPosts
{
    NSArray *topPosts = self.moment.topPosts;
    
    if (topPosts.count > 3)
    {
       topPosts = [topPosts subarrayWithRange:NSMakeRange(0, 3)];
    }
    
    [self.dataListView clearDataInSection:POSTS_LIST_SECTION];
    self.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
    
    //have posts?
    if (!topPosts || topPosts.count == 0)
    {
        [self.dataListView addData:nil toSection:POSTS_LIST_SECTION];
    }
    else
    {
        [self.dataListView addData:[RVSAppPostDataObject dataObjectsFromPosts:topPosts
                                                             postViewDelegate:self]
                         toSection:POSTS_LIST_SECTION];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:self];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    //disabled
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self refresh];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if (indexPath.section == POSTS_LIST_SECTION)
    {
        [self pushPostDetailViewControlWithPost:[(RVSAppPostDataObject *)data post] title:NSLocalizedString(@"post title", nil)];
    }
    else if (indexPath.section == POSTS_LINK_SECTION)
    {
        if (self.moment)
        {
            [self pushPostListViewControlWithPostsOfMoment:self.moment title:NSLocalizedString(@"posts title", nil)];
        }
    }
    else if (indexPath.section == PROGRAM_LINK_SECTION)
    {
        NSObject <RVSProgram> *program = ((RVSAppFooterLinkDataObject *)data).program;
        if (program)
        {            
            [self pushProgramDetailViewControllerWithProgram:program title:program.content.name];
        }
    }
    else if (indexPath.section == CHANNEL_LINK_SECTION)
    {
        NSObject <RVSChannel> *channel = ((RVSAppFooterLinkDataObject *)data).channel;
        if (channel)
        {
            [self pushChannelDetailViewControllerWithChannel:channel title:channel.name];
        }
    }
}

@end
