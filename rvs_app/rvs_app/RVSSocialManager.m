//
//  RVSSocialManager.m
//  rvs_app
//
//  Created by Barak Harel on 2/24/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSSocialManager.h"

#import <Social/Social.h>
#import <rvs_sdk_api helpers.h>

#import "RVSApplication.h"

#define DEF_FB_APP_ID @"460388174061649"

@interface RVSSocialManager()
@property (nonatomic) NSString *facebookAppId;

@property (nonatomic) ACAccountStore *accountStore;
@property (nonatomic) ACAccount *facebookAccount;
@property (nonatomic) ACAccount *twitterAccount;

@property (nonatomic) ACAccountType *facebookAccountType;
@property (nonatomic) ACAccountType *twitterAccountType;
@end

@implementation RVSSocialManager

+(RVSSocialManager *)sharedManager
{
    static RVSSocialManager* _sharedManager;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        _sharedManager = [[RVSSocialManager alloc] init];
    });
    
    return  _sharedManager;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        self.facebookAppId = DEF_FB_APP_ID;
        self.accountStore = [[ACAccountStore alloc] init];
        
        self.twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        self.facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    }
    return self;
}

-(UIActivityViewController *)activityControllerForMessage:(NSString *)message link:(NSString *)link
{
    NSArray *itemsToShare = @[message, [NSURL URLWithString:link]];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll, UIActivityTypeAddToReadingList]; //or whichever you don't need
    
    return activityVC;
}

-(void)requestTwitterAccountWithCompletion:(RVSSocialManagerRequestCompletionHandler)completion
{
    __weak RVSSocialManager *weakSelf = self;
    
    [self.accountStore requestAccessToAccountsWithType:self.twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
        
        [RVSSocialManager logError:error];
        
        BOOL success = NO;
        if(granted)
        {
            weakSelf.twitterAccount = [[weakSelf.accountStore accountsWithAccountType:weakSelf.twitterAccountType] lastObject];
            
            if (weakSelf.twitterAccount)
                success = YES;
            
            NSLog(@"Twitter username: %@", weakSelf.twitterAccount.username);
        }
        
        if (completion)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(success, error);
            });
        }
    }];
}

-(void)requestFacebookAccountWithCompletion:(RVSSocialManagerRequestCompletionHandler)completion
{
    __weak RVSSocialManager *weakSelf = self;
    
    NSDictionary *options = @{ACFacebookAppIdKey: self.facebookAppId,
                              ACFacebookPermissionsKey: @[@"email"]
                              };
    
    [self.accountStore requestAccessToAccountsWithType:self.facebookAccountType options:options completion:^(BOOL granted, NSError *error) {
        
        [RVSSocialManager logError:error];
        
        BOOL success = NO;
        if(granted)
        {
            weakSelf.facebookAccount = [[weakSelf.accountStore accountsWithAccountType:weakSelf.facebookAccountType] lastObject];
            
            if (weakSelf.facebookAccount)
                success = YES;
            
            NSLog(@"Facebook username: %@", weakSelf.facebookAccount.username);
        }
        
        if (completion)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(success, error);
            });
        }
    }];
}

-(void)showShareDialogToFacebookWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    SLComposeViewController *composeVC = [SLComposeViewController
                                          composeViewControllerForServiceType:SLServiceTypeFacebook];
    [composeVC addURL:[NSURL URLWithString:link]];
    [composeVC setInitialText:message];
    
    if (completion)
    {
        [composeVC setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             BOOL success = NO;
             if (result == SLComposeViewControllerResultDone)
                 success = YES;
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(success, nil);
             });
         }];
    }
    
    [[RVSApplication application].mainViewController presentViewController:composeVC animated:YES completion:nil];
}

-(void)showShareDialogToTwitterWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    SLComposeViewController *composeVC = [SLComposeViewController
                                          composeViewControllerForServiceType:SLServiceTypeTwitter];
    [composeVC addURL:[NSURL URLWithString:link]];
    [composeVC setInitialText:message];
    
    if (completion)
    {
        [composeVC setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             BOOL success = NO;
             if (result == SLComposeViewControllerResultDone)
                 success = YES;
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 completion(success, nil);
             });
         }];
    }
    
    [[RVSApplication application].mainViewController presentViewController:composeVC animated:YES completion:nil];
}

-(void)shareToTwitterWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    RVSVerifyMainThread();
    
    //    NSAssert(clip, @"Clip must not be nil");
    
    __weak RVSSocialManager *weakSelf = self;
    
    if (self.twitterAccount)
    {
        [self postToTwitterWithMessage:message link:link completion:completion];
    }
    else
    {
        [self requestTwitterAccountWithCompletion:^(BOOL success, NSError *error)
         {
             if (success)
             {
                 [weakSelf postToTwitterWithMessage:message link:link completion:completion];
             }
             else if (completion)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion(NO, error);
                 });
             }
         }];
    }
}

-(void)shareToFacebookWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    RVSVerifyMainThread();
    
    //    NSAssert(clip, @"Clip must not be nil");
    
    __weak RVSSocialManager *weakSelf = self;
    
    if (self.facebookAccount)
    {
        [self postToFacebookWithMessage:message link:link completion:completion];
    }
    else
    {
        [self requestFacebookAccountWithCompletion:^(BOOL success, NSError *error)
         {
             if (success)
             {
                 NSDictionary *options = @{ACFacebookAppIdKey: weakSelf.facebookAppId,
                                           ACFacebookPermissionsKey: @[@"publish_stream", @"publish_actions"],
                                           ACFacebookAudienceKey: ACFacebookAudienceOnlyMe
                                           };
                 
                 [weakSelf.accountStore requestAccessToAccountsWithType:weakSelf.facebookAccountType options:options completion:^(BOOL granted, NSError *error)
                  {
                      [RVSSocialManager logError:error];
                      
                      if (granted && !error)
                      {
                          [weakSelf postToFacebookWithMessage:message link:link completion:completion];
                      }
                      else
                      {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              completion(NO, error);
                          });
                      }
                  }];
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completion(NO, error);
                 });
             }
         }];
    }
}

-(void)postToFacebookWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    NSDictionary *parameters = @{@"message": message,
                                 @"link": link};
    
    NSURL *feedURL = [NSURL URLWithString:@"https://graph.facebook.com/me/feed"];
    SLRequest *feedRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                requestMethod:SLRequestMethodPOST
                                                          URL:feedURL
                                                   parameters:parameters];
    
    feedRequest.account = self.facebookAccount;
    [feedRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSLog(@"Facebook HTTP response: %i", [urlResponse statusCode]);
         [RVSSocialManager logError:error];
         
         BOOL success = NO;
         
         if (!error && urlResponse.statusCode == 200)
             success = YES;
         
         dispatch_async(dispatch_get_main_queue(), ^{
             completion(success, error);
         });
     }];
}

-(void)postToTwitterWithMessage:(NSString *)message link:(NSString *)link completion:(RVSSocialManagerRequestCompletionHandler)completion;
{
    NSDictionary *parameters = @{@"status": [NSString stringWithFormat:@"%@ %@",message, link]};
    
    NSURL *requestURL = [NSURL
                         URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
    
    SLRequest *postRequest = [SLRequest
                              requestForServiceType:SLServiceTypeTwitter
                              requestMethod:SLRequestMethodPOST
                              URL:requestURL parameters:parameters];
    
    postRequest.account = self.twitterAccount;
    
    [postRequest performRequestWithHandler:^(NSData *responseData,
                                             NSHTTPURLResponse *urlResponse, NSError *error)
     {
         NSLog(@"Twitter HTTP response: %i", [urlResponse statusCode]);
         [RVSSocialManager logError:error];
         
         BOOL success = NO;
         
         if (!error && urlResponse.statusCode == 200)
             success = YES;
         
         dispatch_async(dispatch_get_main_queue(), ^{
             completion(success, error);
         });
     }];
}

+(void)logError:(NSError *)error
{
    if (!error)
        return;
    
    if ([[error domain] isEqualToString:ACErrorDomain])
    {
        // The following error codes and descriptions are found in ACError.h
        switch ([error code])
        {
            case ACErrorAccountMissingRequiredProperty:
                NSLog(@"Account wasn't saved because it is missing a required property.");
                break;
            case ACErrorAccountAuthenticationFailed:
                NSLog(@"Account wasn't saved because authentication of the supplied credential failed.");
                break;
            case ACErrorAccountTypeInvalid:
                NSLog(@"Account wasn't saved because the account type is invalid.");
                break;
            case ACErrorAccountAlreadyExists:
                NSLog(@"Account wasn't added because it already exists.");
                break;
            case ACErrorAccountNotFound:
                NSLog(@"Account wasn't deleted because it could not be found.");
                break;
            case ACErrorPermissionDenied:
                NSLog(@"The operation didn't complete because the user denied permission.");
                break;
            case ACErrorUnknown:
            default: // fall through for any unknown errors...
                NSLog(@"An unknown error occurred.");
                break;
        }
    }
    else
    {
        // handle other error domains and their associated response codes...
        NSLog(@"%@", [error localizedDescription]);
    }
}

@end
