//
//  UIColor+HexString.h
//  ios_player_sample
//
//  Created by Barak Harel on 8/29/13.
//
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)
+ (UIColor *) colorWithHexString: (NSString *) hexString;
+ (UIColor *) colorWithRGBValue:(uint32_t)rgb;
+ (UIColor *) colorWithRGBAValue:(uint32_t)rgba;
@end
