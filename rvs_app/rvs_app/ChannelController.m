//
//  ChannelController.m
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "ChannelController.h"
#import "Channel.h"
#import "MoviePlayer.h"

@interface ChannelController ()

@property (nonatomic) MoviePlayer *moviePlayer;
@property (nonatomic) UILabel *noVideoLabel;
@end

@implementation ChannelController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = self.channel.channelInfo[@"title"];

    NSString *videoUrlString = self.channel.channelInfo[@"video"];
    
    if (videoUrlString.length)
    {
        self.moviePlayer = [[MoviePlayer alloc] init];
        [self.view addSubview:self.moviePlayer.view];
        self.moviePlayer.contentURL = [NSURL URLWithString:videoUrlString];
        [self.moviePlayer play];
    }
    else
    {
        self.noVideoLabel = [[UILabel alloc] init];
        self.noVideoLabel.backgroundColor = [UIColor blackColor];
        self.noVideoLabel.textColor = [UIColor whiteColor];
        self.noVideoLabel.text = @"This channel is currently not available";
        [self.noVideoLabel sizeToFit];
        [self.view addSubview:self.noVideoLabel];
    }
    
    if (self.searchResultData)
    {
        UIBarButtonItem *actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(shareSearchResult)];
        
        self.navigationItem.rightBarButtonItem = actionButton;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    self.moviePlayer.view.frame = self.view.bounds;
    self.moviePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    self.noVideoLabel.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration
{
    CGPoint newCenter = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
    self.noVideoLabel.center = newCenter;
    self.moviePlayer.activityIndicator.center = newCenter;
}

-(void)shareSearchResult
{
    [self.searchResultData.delegate shareToTwitterWithData:self.searchResultData andImage:self.searchResultImage];
}

-(void)dealloc
{
    NSLog(@"ChannelController dealloc");
}

@end
