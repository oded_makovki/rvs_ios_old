//
//  RVSAppPushableUserViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableUserDetailViewController.h"
#import "RVSAppCommonViewController_Private.h"
#import "RVSAppUserDetailCell.h"
#import "RVSAppPostListCell.h"
#import "RVSAppUserDetailPostsLinkCell.h"
#import "RVSAppUserDetailFooterCell.h"
#import "RVSAppUserView.h"
#import "RVSAppPostListFactory.h"
#import "RVSApplication.h"
#import <UIActionSheet+BlocksKit.h>

//mentions stuff
#import "RVSAppPushablePostListViewController.h"

#define HEADER_SECTION 0
#define POSTS_LIST_SECTION 1
#define POSTS_LINK_SECTION 2

@interface RVSAppPushableUserDetailViewController () <RVSAppDataListViewDelegate, RVSAppPostViewDelegate,RVSAppUserViewDelegate>
@property (nonatomic) NSObject <RVSAsyncUser> *asyncUser;
@property (nonatomic) NSObject <RVSAsyncPostList> *postList;

@property (nonatomic) BOOL shouldClearPostsData;

@property (nonatomic) NSObject <RVSAsyncError> *followAsyncError;
@end

@implementation RVSAppPushableUserDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.dataListView.refreshEnabled = NO;
    
    self.dataListView.numberOfSections = 3; //user, posts, link
    
    self.navigationItem.title = NSLocalizedString(@"profile title", nil);
    
    [self.dataListView setSectionsCellClass:[RVSAppUserDetailCell class] forSection:HEADER_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppPostListCell class] forSection:POSTS_LIST_SECTION];
    [self.dataListView setSectionsCellClass:[RVSAppUserDetailPostsLinkCell class] forSection:POSTS_LINK_SECTION];
    
    self.dataListView.footerCellClass = [RVSAppUserDetailFooterCell class];
    self.dataListView.delegate = self;
    
    //load default user (cached or empty)
    [self loadDefaultUser];
    
    //always refresh user
    if (self.userId) //preset or from cachedUser
    {
        [self loadPosts];
        [self reloadUser];
    }
}

-(void)loadDefaultUser
{
    //new data from current/empty user
    RVSAppUserDataObject *data = [[RVSAppUserDataObject alloc] init];
    
    if (self.user)
    {
        data.user = self.user;
        data.userViewDelegate = self;
        self.userId = self.user.userId; //might overwrite
    }
    //add data
    [self.dataListView addData:@[data] toSection:HEADER_SECTION];
}

-(void)reloadUser
{
    __weak RVSAppPushableUserDetailViewController *weakSelf = self;
    
    self.asyncUser = nil;
    self.asyncUser = [[RVSSdk sharedSdk] userForUserId:self.userId];
    
    [self.asyncUser userUsingBlock:^(NSObject<RVSUser> *user, NSError *error)
    {
        if (error || !user) //keep cached if available
            return;
        
        weakSelf.user = user;
        
        RVSAppUserDataObject *data = [[RVSAppUserDataObject alloc] init];
        data.user = weakSelf.user;
        data.userViewDelegate = weakSelf;
        
        [weakSelf.dataListView clearDataInSection:HEADER_SECTION];
        [weakSelf.dataListView addData:@[data] toSection:HEADER_SECTION animated:NO];
    }];
}

-(void)reloadPosts
{
    self.shouldClearPostsData = YES;
    [self loadPosts];
    [self reloadUser];
}

- (void)loadPosts
{
    self.postList = nil;
    self.postList = [[RVSSdk sharedSdk] postsForUser:self.userId];
    
    __weak RVSAppPushableUserDetailViewController *weakSelf = self;
    
    [self.postList postsUsingBlock:^(NSArray *posts, NSError *error) {
        
        if (weakSelf.shouldClearPostsData)
        {
            weakSelf.shouldClearPostsData = NO;
            [weakSelf.dataListView clearDataInSection:POSTS_LIST_SECTION];
        }
        
        if (error)
        {
            weakSelf.dataListView.error = error;
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;

            [weakSelf.dataListView addData:nil toSection:POSTS_LIST_SECTION];
        }
        else
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppCommonViewControllerReadyNotification object:weakSelf];
            
            [weakSelf.dataListView addData:[RVSAppPostDataObject dataObjectsFromPosts:posts
                                                                               postViewDelegate:weakSelf]
                                 toSection:POSTS_LIST_SECTION];
            
            if ([posts count] > 0)
            {
                [weakSelf.dataListView clearDataInSection:POSTS_LINK_SECTION];
                [weakSelf.dataListView addData:@[[NSObject new]] toSection:POSTS_LINK_SECTION];
            }
        }
        
    } count:3];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self != [RVSApplication application].activeViewController ||
        [RVSApplication application].topBar.isShown || [RVSApplication application].menu.isOpen)
        return NO;
    
    return YES;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    //disabled
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadPosts];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data
{
    if (indexPath.section == POSTS_LIST_SECTION)
    {
        [self pushPostDetailViewControlWithPost:[(RVSAppPostDataObject *)data post] title:NSLocalizedString(@"post title", nil)];
    }
    else if (indexPath.section == POSTS_LINK_SECTION)
    {
        if (self.user)
        {
            [self pushPostListViewControlWithPostsByUser:self.user title:NSLocalizedString(@"posts title", nil)];
        }
    }
}

-(void)postView:(RVSAppPostView *)postView didSelectUser:(NSObject<RVSUser> *)user
{
    if ([[self.userId lowercaseString] isEqualToString:[user.userId lowercaseString]])
    {
        //same user!
        
        CAKeyframeAnimation *animation;
        animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
        animation.values = [NSArray arrayWithObjects:@0,@(5),@(0), nil];
        animation.keyTimes = [NSArray arrayWithObjects:@0,@0.5,@1.0, nil];
        
        animation.autoreverses = NO;
        animation.repeatCount = 1; // Play it just once, and then reverse it
        animation.duration = 0.3;
        [postView.layer addAnimation:animation forKey:nil];
        
        return;
    }
    
    [super postView:postView didSelectUser:user];
}
    
#pragma mark - User Settings


-(void)userView:(RVSAppUserView *)userView didSelectSettingsForUser:(NSObject<RVSUser> *)user
{
    __weak RVSAppPushableUserDetailViewController *weakSelf = self;
    
    if ([[RVSApplication application] isMyUserId:user.userId])
    {
        //settings sheet
        UIActionSheet *settingsSheet = [UIActionSheet bk_actionSheetWithTitle:nil];
        
        [settingsSheet bk_addButtonWithTitle:NSLocalizedString(@"favorites action", nil) handler:^{
            [weakSelf pushPostListViewControlWithFavoritesOfUser:user title:NSLocalizedString(@"favorites title", nil)];
        }];
        
        [settingsSheet bk_addButtonWithTitle:NSLocalizedString(@"more title", nil) handler:^{
//            [RVSApplication application].mainViewController.selectedIndex = 3;
            UIViewController *about = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppMoreViewController"];
            [weakSelf.navigationController pushViewController:about animated:YES];
        }];
        
        [settingsSheet bk_setDestructiveButtonWithTitle:NSLocalizedString(@"logout action", nil) handler:^{
            [[RVSApplication application] logout];
        }];
        
        [settingsSheet bk_setCancelButtonWithTitle:nil handler:nil];
        
        [settingsSheet showInView:[RVSApplication application].mainViewController.view];
    }
    
//    else
//    {
//        [self showSettingsForUser:user];
//    }
}

-(void)userView:(RVSAppUserView *)userView didSelectMentionsForUser:(NSObject<RVSUser> *)user
{
    if ([[RVSApplication application] isMyUserId:user.userId])
    {
        [self showMyMentions];
    }
}

-(void)showMyMentions
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.navigationItem.title = NSLocalizedString(@"mentions title", nil);
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithMyMentions];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)composeDidCompleteWithPost:(NSObject<RVSPost> *)post inReplyToPost:(NSObject<RVSPost> *)replyToPost
{
    if ([[RVSApplication application] isMyUserId:self.userId])
    {
        [self reloadPosts];
        [self reloadUser];
    }
}

//
//-(void)showSettingsForUser:(NSObject <RVSUser> *)user
//{
//    __weak RVSAppPushableProfileViewController *weakSelf = self;
//    
//    UIActionSheet *settingsSheet = [UIActionSheet actionSheetWithTitle:user.name];
//    
//    if (![user.isFollowedByMe boolValue])
//    {
//        [settingsSheet addButtonWithTitle:@"Follow" handler:^{
//            [weakSelf followUser:user];
//        }];
//    }
//    else
//    {
//        [settingsSheet addButtonWithTitle:@"Unfollow" handler:^{
//            [weakSelf unfollowUser:user];
//        }];
//    }
//    
//    [settingsSheet addButtonWithTitle:@"Help" handler:^{
//        NSLog(@"HELP!!!");
//        [[RVSApplication application] showWebViewWithURL:[NSURL URLWithString:@"http://rayv-inc.com"]];
//    }];
//    
//    [settingsSheet setDestructiveButtonWithTitle:@"Report User" handler:nil];
//    [settingsSheet setCancelButtonWithTitle:nil handler:nil];
//    [settingsSheet showInView:[RVSApplication application].mainViewController.view];
//}
//
//-(void)unfollowUser:(NSObject <RVSUser> *)user
//{
//    self.followAsyncError = [[RVSSdk sharedSdk] unfollowUser:user.userId];
//    [self.followAsyncError errorUsingBlock:^(NSError *error) {
//        if (error)
//        {
//            NSLog(@"error: %@", error);
//        }
//    }];
//}
//
//-(void)followUser:(NSObject <RVSUser> *)user
//{
//    self.followAsyncError = [[RVSSdk sharedSdk] followUser:user.userId];
//    [self.followAsyncError errorUsingBlock:^(NSError *error) {
//        if (error)
//        {
//            NSLog(@"error: %@", error);
//        }
//    }];
//}

@end
