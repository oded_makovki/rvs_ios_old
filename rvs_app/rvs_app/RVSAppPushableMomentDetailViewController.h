//
//  RVSAppPushableMomentDetailViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import <rvs_sdk_api.h>

@interface RVSAppPushableMomentDetailViewController : RVSAppCommonDataViewController
@property (nonatomic) NSObject <RVSMoment> *moment;
@property (nonatomic) NSString *relatedToPostId;

- (void)refresh;
@end
