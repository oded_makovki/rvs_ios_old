//
//  RVSAppFooterLinkDataObject.h
//  rvs_app
//
//  Created by Barak Harel on 1/6/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppFooterLinkType)
{
    RVSAppFooterLinkTypeMomentUsers,
    RVSAppFooterLinkTypeProgram,
    RVSAppFooterLinkTypeChannel
};

@interface RVSAppFooterLinkDataObject : NSObject
@property (nonatomic, readonly) RVSAppFooterLinkType type;
@property (nonatomic, readonly) NSObject <RVSMoment> *moment;
@property (nonatomic, readonly) NSObject <RVSProgram> *program;
@property (nonatomic, readonly) NSObject <RVSChannel> *channel;


-initWithUsersOfMoment:(NSObject <RVSMoment> *)moment;
-initWithProgram:(NSObject <RVSProgram> *)program;
-initWithChannel:(NSObject <RVSChannel> *)channel;

@end
