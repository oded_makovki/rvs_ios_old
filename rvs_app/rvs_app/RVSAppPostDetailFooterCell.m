//
//  RVSAppDetailPostFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostDetailFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppPostDetailFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppDetailPostFooterCell];
    }
    return self;
}

-(void)initRVSAppDetailPostFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];

    [self setText:NSLocalizedString(@"post detail footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"post detail footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"post detail footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 1);
}


@end
