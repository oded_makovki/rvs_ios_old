//
//  RVSAppTopBarPreComposeView.m
//  rvs_app
//
//  Created by Barak Harel on 1/19/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppTopBarPreComposeView.h"
#import "RVSApplication.h"
#import <rvs_sdk_api helpers.h>

@interface RVSAppTopBarPreComposeView()
@property (nonatomic) RVSNotificationObserver *currentChannelObserver;
@property (nonatomic) NSObject <RVSAsyncImage> *currentChannelAsyncImage;
@property (nonatomic) NSTimer *currentChannerImageTimer;
@end

@implementation RVSAppTopBarPreComposeView

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.channelView.channel = nil;
        self.clipsToBounds = YES;
        self.loadingView.hidden = NO;
        self.channelView.hidden = YES;
        self.showComposeButton.enabled = NO;
        self.changeChannelButton.enabled = NO;
        
        [self.currentChannelImageSpinner startAnimating];
        
        self.loadingChannelsLabel.text = NSLocalizedString(@"no channels"  , nil);
        
        __weak RVSAppTopBarPreComposeView *weakSelf = self;
        
        self.currentChannelObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationCurrentChannelDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf currentChannelDidChange:notification];
        }];
        
        [self refresh];
    }
    
    return self;
}

-(void)stopCurrentChannelTimer
{
    [self.currentChannerImageTimer invalidate];
    self.currentChannerImageTimer = nil;
}

-(void)startCurrentChannelTimer
{
    //cancel old
    [self stopCurrentChannelTimer];
    
    self.currentChannerImageTimer = [NSTimer scheduledTimerWithTimeInterval:4.0 target:self selector:@selector(updateChannelThumbnail) userInfo:nil repeats:YES];
    
    //update now
    [self updateChannelThumbnail];
}

-(void)updateChannelThumbnail
{
    if (![RVSApplication application].currentChannel)
        return;
    
    NSDate *almostNow = [NSDate dateWithTimeIntervalSinceNow:[RVSApplication application].nowTimeOffset];
    NSLog(@"Top Bar - Updating channel thumbnail...");
    
    NSObject <RVSAsyncImageFactory> *imageFactory = [[RVSSdk sharedSdk] thumbnailForChannel:[RVSApplication application].currentChannel.channelId date:almostNow];
    
    self.currentChannelAsyncImage = [imageFactory asyncImage];
}

- (void)currentChannelDidChange:(NSNotification *)notification
{
    NSLog(@"Top Bar - Current channel changed...");
    
    self.currentChannelImageView.image = nil;
    [self.currentChannelImageSpinner startAnimating];
    [self refresh];
}

- (void)refresh
{
    if ([[RVSApplication application].channelsList count] <= 0)
    {
        NSLog(@"Top Bar - No channels, skipping...");
        return;
    }
    
    NSLog(@"Top Bar - Refreshing...");
    
    self.loadingView.hidden = YES;
    self.channelView.hidden = NO;
    self.showComposeButton.enabled = YES;
    self.changeChannelButton.enabled = YES;

    //refresh
    self.channelView.channel = [RVSApplication application].currentChannel;
    //update
    [self updateChannelThumbnail];
}

-(void)setCurrentChannelAsyncImage:(NSObject<RVSAsyncImage> *)currentChannelAsyncImage
{
    _currentChannelAsyncImage = currentChannelAsyncImage;
    if (_currentChannelAsyncImage)
    {
        __weak RVSAppTopBarPreComposeView *weakSelf = self;
        [weakSelf.currentChannelAsyncImage imageWithSize:weakSelf.currentChannelImageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
            
            if (error)
            {
                [weakSelf.currentChannelImageSpinner startAnimating];
            }
            else
            {
                [UIView transitionWithView:weakSelf.currentChannelImageView
                                  duration:0.2f
                                   options:UIViewAnimationOptionTransitionCrossDissolve
                                animations:^{
                                    weakSelf.currentChannelImageView.image = image;
                                } completion:^(BOOL finished) {
                                    [weakSelf.currentChannelImageSpinner stopAnimating];
                                }];
                
            }
            
            
        }];
    }
}

#pragma mark - Methods

- (IBAction)showChannelSelect:(id)sender
{
    [[RVSApplication application] showChannelSelect];
}


- (IBAction)showCompose:(id)sender
{
    [[RVSApplication application] showCompose];
}

@end
