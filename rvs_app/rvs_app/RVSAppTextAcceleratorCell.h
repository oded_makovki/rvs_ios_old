//
//  RVSAppHashTagsCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppTextAcceleratorCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@end
