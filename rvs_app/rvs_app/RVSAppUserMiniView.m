//
//  RVSAppUserMiniView.m
//  rvs_app
//
//  Created by Barak Harel on 1/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppUserMiniView.h"

@interface RVSAppUserMiniView()
@property (nonatomic) NSObject <RVSAsyncImage> *asyncImage;
@end

@implementation RVSAppUserMiniView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserMiniView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUserMiniView];
}

-(void)initRVSAppUserMiniView
{
    //round corners (if will have subclasses, do it there).
    self.userThumbnail.layer.cornerRadius = 5;
    self.userThumbnail.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap)];
    [self addGestureRecognizer:tap];
}

-(void)reset
{
    _user = nil;    
    self.userThumbnail.image = nil;
}

-(void)setUser:(NSObject<RVSUser> *)user
{
    if (user  && _user == user) //reset even if nil
        return;
    
    [self reset];
    _user = user;
    
    if (!_user)
        return;
    
    self.asyncImage = [user.profileImage asyncImage];
}

-(void)setAsyncImage:(NSObject<RVSAsyncImage> *)asyncImage
{
    __weak RVSAppUserMiniView *weakSelf = self;
    
    _asyncImage = asyncImage;
    [_asyncImage imageWithSize:weakSelf.userThumbnail.frame.size usingBlock:^(UIImage *image, NSError *error) {
        if (error || !image)
        {
            weakSelf.userThumbnail.image = [UIImage imageNamed:@"default_profile"];
        }
        else
        {
            weakSelf.userThumbnail.image = image;
        }
    }];
}

-(void)handleTap
{
    if (!self.user)
        return;
    
    if ([self.delegate respondsToSelector:@selector(didSelectUserMiniView:)])
    {
        [self.delegate didSelectUserMiniView:self];
    }
}

@end
