//
//  RVSAppComposeViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppComposeViewController.h"
#import "RVSApplication.h"
#import "RVSAppChannelSelectViewController.h"
#import "RVSAppChannelSelectCollectionView.h"
#import "RVSAppTimelineView.h"
#import "RVSAppClipPlayerView.h"
#import "RVSAppAcceleratorsToolbar.h"
#import "RVSAppTranscriptAcceleratorView.h"

#import "RVSAppPostComposeCell.h"
#import "RVSAppPostComposeReplyCell.h"
#import "RVSAppPostDetailFooterCell.h"

#import "RVSAppDataListView.h"
#import <BlocksKit.h>
#import <UIAlertView+BlocksKit.h>
#import <rvs_sdk_api helpers.h>

#import "RVSAppPostDataObject.h"
#import "RVSAppComposeAccessoryView.h"

NSInteger const kMaxCharacters = 140;

@interface RVSAppComposeViewController () <RVSAppTimelineViewDelegate, RVSAppTimelineViewDataSource, UITextViewDelegate, RVSAppAcceleratorViewDelegate, RVSAppDataListViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *postButton;

@property (strong, nonatomic) UIBarButtonItem *cancelButton;
    
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIView *topContainer;

@property (weak, nonatomic) IBOutlet UIView *clipControls;
//conatins:
@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (strong, nonatomic) RVSAppClipPlayerView *clipPlayerView;
@property (weak, nonatomic) IBOutlet RVSAppTimelineView *timeline;

@property (weak, nonatomic) IBOutlet UIView *replyControls;
//conatins:
@property (weak, nonatomic) IBOutlet RVSAppDataListView *dataListView;
@property (nonatomic) NSObject <RVSAsyncPostList> *repliesList;
@property (nonatomic) BOOL shouldClearRepliesData;

//accelerators
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *acceleratorsToolbarTopConstraint;

@property (strong, nonatomic) NSObject <RVSAsyncClosedCaptionList> *closedCaptionsList;
@property (strong, nonatomic) NSObject <RVSAsyncTopicList> *topicsList;

@property (nonatomic) NSMutableArray *hashTags;

//text
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *textViewPlaceholder;
@property (strong, nonatomic) UITapGestureRecognizer *textViewEndTextEditTapGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer *textViewEndTextEditSwipeGesture;
@property (strong, nonatomic) UISwipeGestureRecognizer *leftButtonEndTextEditSwipeGesture;

//time
@property (strong, nonatomic) NSDate *contextNow;
@property (strong, nonatomic) NSDate *contextStart;
@property (strong, nonatomic) NSDate *contextEnd;

@property (strong, nonatomic) NSDate *selectionStart;
@property (strong, nonatomic) NSDate *selectionEnd;

@property (nonatomic) BOOL inEditVideo;

@property (nonatomic) NSString *channelId;
@property (nonatomic) NSObject <RVSProgram> *currentProgram;
@property (nonatomic) NSObject <RVSAsyncProgram> *asyncCurrentProgram;
@property (nonatomic) NSObject <RVSAsyncUserList> *asyncUserList;

@property (nonatomic) NSObject <RVSClip> *clip;
@property (nonatomic) NSObject <RVSAsyncImageFactory> *imageFactory;

@property (nonatomic) RVSNotificationObserver *keyboardWillShowObserver;
@property (nonatomic) RVSNotificationObserver *keyboardWillHideObserver;

@property (nonatomic) RVSAppComposeAccessoryView *textAccView;

@property (nonatomic) RVSNotificationObserver *currentChannelObserver;
@property (weak, nonatomic) IBOutlet RVSAppChannelView *channelView;
@property (weak, nonatomic) IBOutlet UILabel *nowWatchingLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeChannelLabel;

@property (weak, nonatomic) IBOutlet UIButton *changeChannelButton;
@end

@implementation RVSAppComposeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.textView.keyboardAppearance = UIKeyboardAppearanceDark;
    
//    self.navigationItem.leftBarButtonItem = self.cancelButton;

    //set time frame
    
    self.textAccView = [[RVSAppComposeAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 320, 65) textView:self.textView];
    
    if (self.replyToPost)
    {
        self.navigationItem.title = NSLocalizedString(@"reply title", nil);

        self.replyControls.hidden = NO;
        self.clipControls.hidden = YES;
        
        self.textView.text = [NSString stringWithFormat:@"@%@ ",self.replyToPost.author.userId] ;
        self.textViewPlaceholder.hidden = YES;
        
        self.channelId = self.replyToPost.channel.channelId;
        
        self.contextNow = self.contextStart = [self.replyToPost.clip.start dateByAddingTimeInterval:-120.0];
        self.contextEnd = [self.replyToPost.clip.start dateByAddingTimeInterval:15.0]; //window size
        
        self.selectionStart = self.replyToPost.clip.start;
        self.selectionEnd = self.replyToPost.clip.end;
        
        self.dataListView.numberOfSections = 2; //post, replies
        
        [self.dataListView setSectionsCellClass:[RVSAppPostComposeCell class] forSection:0];
        [self.dataListView setSectionsCellClass:[RVSAppPostComposeReplyCell class] forSection:1];
        
        self.dataListView.footerCellClass = [RVSAppPostDetailFooterCell class];
        self.dataListView.delegate = self;
        
        [self loadPost];
        [self loadReplies];        
    }
    else
    {
        self.navigationItem.title = NSLocalizedString(@"compose title", nil);

        self.replyControls.hidden = YES;
        self.clipControls.hidden = NO;
        
        self.channelId = [RVSApplication application].currentChannel.channelId;
        
        NSDate *almostNow = [[NSDate date] dateByAddingTimeInterval:[RVSApplication application].nowTimeOffset]; //offset
        
        self.contextNow = almostNow;
        self.contextStart = [almostNow dateByAddingTimeInterval:-120.0];
        self.contextEnd = [almostNow dateByAddingTimeInterval:15.0]; //window size
        //TO DO: maybe add window size as a parameter/const of the timeline
        
        self.clipPlayerView = [[RVSAppClipPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.videoContainer.frame.size.width, self.videoContainer.frame.size.height)];
        
        self.clipPlayerView.backgroundColor = [UIColor blackColor];
        [self.videoContainer addSubview:self.clipPlayerView];
        
        self.timeline.delegate = self;
        self.timeline.dataSource = self;
        self.timeline.delegateTimeintervalSensitivity = 1.0f;
        self.timeline.isCentered = NO;
        //self.timeline.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"dotted-pattern"]];
        
        [self.timeline updateWithNow:self.contextNow start:self.contextStart end:self.contextEnd];
        [self.timeline scrollTo:[self.contextNow dateByAddingTimeInterval:-10.5]  animated:NO];
        
        __weak RVSAppComposeViewController *weakSelf = self;
        
        self.currentChannelObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationCurrentChannelDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf currentChannelDidChange:notification];
        }];
    }
    
    self.textAccView.acceleratorsToolbar.transcriptView.timeSyncOffset = -10;// [RVSApplication application].nowTimeOffset;
    self.textAccView.acceleratorsToolbar.transcriptView.delegate = self;
    self.textAccView.acceleratorsToolbar.hashTagsView.delegate = self;
    self.textAccView.acceleratorsToolbar.usersView.delegate = self;
    
    self.textView.delegate = self;
        
    self.textViewEndTextEditTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endTextEdit)];
    [self.textView addGestureRecognizer:self.textViewEndTextEditTapGesture];
    self.textViewEndTextEditTapGesture.enabled = NO;
    
    self.textViewEndTextEditSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(endTextEdit)];
    self.textViewEndTextEditSwipeGesture.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
    [self.textView addGestureRecognizer:self.textViewEndTextEditSwipeGesture];
    self.textViewEndTextEditSwipeGesture.enabled = NO;
    
    [self loadData];
    
    [self.textAccView.acceleratorsToolbar setLayoutInEditVideo:NO animateWithDuration:0];
    [self setLayoutInEditVideo:YES keyboardHeight:0 animated:NO];
}

- (void)cancel:(id)sender
{
    __weak RVSAppComposeViewController *weakSelf = self;
    
    [UIAlertView bk_showAlertViewWithTitle:NSLocalizedString(@"cancel title", nil)
                                message:NSLocalizedString(@"cancel message", nil)
                      cancelButtonTitle:NSLocalizedString(@"alert cancel", nil)
                      otherButtonTitles:[NSArray arrayWithObject:NSLocalizedString(@"alert ok", nil)]
                                handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) //ok
        {
            [weakSelf.clipPlayerView stop];
            [weakSelf.delegate composeViewController:self
                   didCompleteWithComposePostFactory:nil
                                           thumbnail:nil
                                       inReplyToPost:nil];
        }
    }];
}

-(void)currentChannelDidChange:(NSNotification *)notification
{
    self.channelId = [RVSApplication application].currentChannel.channelId;
    [self.timeline updateWithNow:self.contextNow start:self.contextStart end:self.contextEnd];
    [self.timeline scrollTo:[self.contextNow dateByAddingTimeInterval:-10.5] animated:NO];
    [self setClipPlayerThumbnailFromDate:[self.contextNow dateByAddingTimeInterval:-10.5]];
    [self loadData];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    __weak RVSAppComposeViewController *weakSelf = self;
    
    self.keyboardWillShowObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillShowNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillShow:notification];
                                     }];
    
    
    self.keyboardWillHideObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillHideNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillHide:notification];
                                     }];
    
    if (self.replyToPost)
    {
        [self.textView becomeFirstResponder];
    }
}

-(void)willMoveToParentViewController:(UIViewController *)parent
{
    if (!parent)
    {
        self.keyboardWillShowObserver = nil;
        self.keyboardWillHideObserver = nil;
    }
    
    [super willMoveToParentViewController:parent];
}

#pragma mark - Methods

- (void)addText:(NSString *)text
{
    if (!text || text.length == 0)
        return;
    
    self.textViewPlaceholder.hidden = YES;
    
    if (!self.textView.text || [self.textView.text length] == 0) //no text, just set
    {
        self.textView.text = text;
    }
    else if ([self.textView.text hasSuffix:@" "] || [self.textView.text hasSuffix:@"\n"])
    {
        self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, text];
    }
    else
    {
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        NSArray *words = [self.textView.text componentsSeparatedByCharactersInSet:whitespace];
        NSString *lastWord = [words lastObject];
        
        if (lastWord && [[text lowercaseString] hasPrefix:[lastWord lowercaseString]]) //found prefix! add leftovers
        {
            self.textView.text = [NSString stringWithFormat:@"%@%@", [self.textView.text substringToIndex:self.textView.text.length - lastWord.length], text];
        }
        else //not prefix - add space and add
        {
            self.textView.text = [NSString stringWithFormat:@"%@ %@", self.textView.text, text];
        }
    }
    
    [self updateCharCount];
}

#pragma mark - Data

- (void)loadData
{
    self.hashTags = [NSMutableArray array];
    
    if (self.replyToPost) //reply
    {
        self.currentProgram = self.replyToPost.program;
    }
    else //compose
    {
        [self getCurrentProgram];
        self.channelView.channel = [[RVSApplication application] getChannelById:self.channelId];
    }
    
    self.closedCaptionsList = [[RVSSdk sharedSdk] closedCaptionsForChannel:self.channelId start:self.contextStart end:self.contextEnd];

    self.topicsList = [[RVSSdk sharedSdk] topicsForChannel:self.channelId start:self.contextStart end:self.contextEnd];
    
    self.asyncUserList = [[RVSSdk sharedSdk] followedByUser:[RVSSdk sharedSdk].myUserId];
    
    //loading data
    __weak RVSAppComposeViewController *weakSelf = self;
    
    [self.closedCaptionsList closedCaptionsUsingBlock:^(NSArray *closedCaptions, NSError *error)
    {
        [weakSelf didReceiveClosedCaptions:closedCaptions];
    }];
    
    [self.topicsList topicsUsingBlock:^(NSArray *topics, NSError *error) {
        [weakSelf didReceiveTopics:topics];
    }];    
    
    [self.asyncUserList usersUsingBlock:^(NSArray *users, NSError *error) {
        [weakSelf didReceiveUsers:users];
    } count:30];
}

- (void)getCurrentProgram
{
    self.asyncCurrentProgram = [[RVSSdk sharedSdk] updatingCurrentProgramForChannel:self.channelId];
    
    __weak RVSAppComposeViewController *weakSelf = self;
    [weakSelf.asyncCurrentProgram programUsingBlock:^(NSObject<RVSProgram> *program, NSError *error) {
        if (error)
        {
            //            NSLog(@"Error in programsMetadataUsingBlock: %@", error);
            return;
        }
        
        weakSelf.currentProgram = program;
    }];
}

-(void)setCurrentProgram:(NSObject <RVSProgram> *)currentProgram
{
    if (_currentProgram == currentProgram)
        return;
    
    _currentProgram = currentProgram;
    //add topics
    [self didReceiveTopics:_currentProgram.content.topics];
}


-(void)didReceiveUsers:(NSArray *)users
{
    NSMutableArray *cleanHandles = [NSMutableArray arrayWithCapacity:users.count];
    for (NSObject <RVSUser> *user in users)
    {
        NSString *handle = user.userId;
        if (handle && handle.length > 0)
        {
            NSString *cleanHandle = [NSString stringWithFormat:@"@%@",handle];
            [cleanHandles addObject:cleanHandle];
        }
    }
    
    [self.textAccView.acceleratorsToolbar.usersView loadData:cleanHandles];
}

-(void)didReceiveClosedCaptions:(NSArray *)closedCaptions
{
    [self.textAccView.acceleratorsToolbar.transcriptView loadData:closedCaptions];
    
    //init with current selection
    [self.textAccView.acceleratorsToolbar.transcriptView setSelectionStart:self.selectionStart selectionEnd:self.selectionEnd animated:NO];
}

-(void)didReceiveTopics:(NSArray *)topics
{
    if (!topics || topics.count == 0)
        return;
    
    NSMutableArray *hashTags = [NSMutableArray arrayWithCapacity:topics.count];
    for (NSString *topic in topics)
    {
        if (topic && topic.length > 0)
        {
            NSString *hashTag = topic;
            
            //add # if needed
            if (![hashTag hasPrefix:@"#"])
                hashTag = [NSString stringWithFormat:@"#%@",hashTag];
            
            //upper camel case!
            hashTag = [hashTag replaceSpacesWithCapitals]; //thanks Ofer!
            [hashTags addObject:hashTag];
        }
    }
    
    //add at end
    [self.hashTags addObjectsFromArray:hashTags];
    [self.textAccView.acceleratorsToolbar.hashTagsView loadData:self.hashTags];
}

#pragma mark - Layout

-(void)endTextEdit
{
    [self.view endEditing:NO];
}

-(void)setLayoutInEditVideo:(BOOL)inEditVideo keyboardHeight:(CGFloat)height animated:(BOOL)animated
{
    if (self.inEditVideo == inEditVideo)
        return;
    
    self.inEditVideo = inEditVideo;
    
    if (inEditVideo)
    {
        self.textViewEndTextEditTapGesture.enabled = NO;
        self.textViewEndTextEditSwipeGesture.enabled = NO;
        self.leftButtonEndTextEditSwipeGesture.enabled = NO;
        
        //move view down
        self.commentViewBottomConstraint.constant = 10;
        self.topConstraint.constant = 0;
    }
    else
    {
        self.textViewEndTextEditTapGesture.enabled = YES;
        self.textViewEndTextEditSwipeGesture.enabled = YES;
        self.leftButtonEndTextEditSwipeGesture.enabled = YES;
        
        self.topConstraint.constant = -self.topContainer.frame.size.height; //move view up
        self.commentViewBottomConstraint.constant = height + 10;
    }
    
    if (animated)
    {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            if (inEditVideo) //delayed action
            {
                //TBD
            }
        }];
    }    
}

#pragma mark - TextView Delegate

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary *info = [notification userInfo];
    
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    
    [self setLayoutInEditVideo:NO keyboardHeight:height animated:YES];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [self setLayoutInEditVideo:YES keyboardHeight:0 animated:YES];
}


-(void)textViewDidChange:(UITextView *)textView
{
    if (self.textView.text.length == 0)
    {
        self.textViewPlaceholder.hidden = NO;
    }
    else
    {
        self.textViewPlaceholder.hidden = YES;
    }
    
    [self updateCharCount];
    
    if([textView.text hasSuffix:@"\""])
    {
        [self.textAccView.acceleratorsToolbar showTranscriptAccelerator];
    }
    else if ([textView.text hasSuffix:@"#"])
    {
        [self.textAccView.acceleratorsToolbar showHashTagsAccelerator];
    }
    else if ([textView.text hasSuffix:@"@"])
    {
        [self.textAccView.acceleratorsToolbar showUsersAccelerator];
    }
}

-(void)updateCharCount
{
    NSInteger charsLeft = kMaxCharacters - self.textView.text.length;
    self.textAccView.charsCountLabel.text = [NSString stringWithFormat:@"%d", charsLeft];
    
    if (self.textView.text.length == 0)
    {
        self.textAccView.charsCountLabel.textColor = [UIColor lightGrayColor];
    }
    else if (self.textView.text.length <= kMaxCharacters)
    {
        self.textAccView.charsCountLabel.textColor = [UIColor darkGrayColor];
    }
    else
    {
        self.textAccView.charsCountLabel.textColor = [UIColor redColor];
    }
}

#pragma mark - RVSAppAcceleratorView Delegate/Data Source

-(void)acceleratorView:(RVSAppAcceleratorView *)acceleratorView didSelectText:(NSString *)text
{
    if (self.inEditVideo)
        [self.textView becomeFirstResponder];
    
    NSString *decoratedText = [NSString stringWithFormat:@"%@%@%@", [acceleratorView textPrefix], text, [acceleratorView textSuffix]];
    [self addText:decoratedText];
}

#pragma mark - RVSAppTimelineView Delegate/Data Source

-(NSObject <RVSAsyncImage> *)timeline:(RVSAppTimelineView *)timeline thumbnailForDate:(NSDate *)date
{
    NSObject <RVSAsyncImageFactory> *imageFactory = [[RVSSdk sharedSdk] thumbnailForChannel:self.channelId date:date];
    return [imageFactory asyncImage];
}

-(void)timeline:(RVSAppTimelineView *)timeline didChangeWithStart:(NSDate *)start end:(NSDate *)end
{
   [self.clipPlayerView stop];
    
    self.selectionStart = start;
    self.selectionEnd = end;
    
    self.clip = [[RVSSdk sharedSdk] clipWithChannelId:self.channelId start:self.selectionStart end:self.selectionEnd];
    self.clipPlayerView.clip = self.clip;
    
    self.imageFactory = [[RVSSdk sharedSdk] thumbnailForChannel:self.channelId date:self.selectionStart];
    
    [self.textAccView.acceleratorsToolbar.transcriptView setSelectionStart:self.selectionStart selectionEnd:self.selectionEnd animated:YES];
    
    [self setClipPlayerThumbnailFromDate:start];
}

-(void)timeline:(RVSAppTimelineView *)timeline didSelectFrameWithDate:(NSDate *)date
{
    [self setClipPlayerThumbnailFromDate:date];
}

-(void)setClipPlayerThumbnailFromDate:(NSDate *)date
{
    NSObject <RVSAsyncImageFactory> *tmpImageFactory = [[RVSSdk sharedSdk] thumbnailForChannel:self.channelId date:date];
    self.clipPlayerView.thumbnail = [tmpImageFactory asyncImage];
}

#pragma mark - Reply

- (void)loadPost
{
    //add replay to
    [self.dataListView addData:@[[RVSAppPostDataObject dataObjectFromPost:self.replyToPost
                                                               postViewDelegate:nil]] toSection:0];
}

- (void)reloadReplies
{
    self.shouldClearRepliesData = YES;
    [self loadReplies];
}

-(void)loadReplies
{
    __weak RVSAppComposeViewController *weakSelf = self;
    //reply list
    self.repliesList = [[RVSSdk sharedSdk] repliesForPost:self.replyToPost.postId];
    
    [self.repliesList postsUsingBlock:^(NSArray *posts, NSError *error) {
        
        if (weakSelf.shouldClearRepliesData)
        {
            weakSelf.shouldClearRepliesData = NO;
            [weakSelf.dataListView clearDataInSection:1];
        }
        
        
        if (error)
        {
            weakSelf.dataListView.error = error;
        }
        else
        {
            if (!posts || [posts count] == 0)
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            }
            else
            {
                weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
            }
            
            [weakSelf.dataListView addData:[RVSAppPostDataObject dataObjectsFromPosts:posts
                                                                               postViewDelegate:nil]
                                 toSection:1]; //replies
        }
        
    } count:10];
}


#pragma mark - DataListView Delegate

-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    [self.repliesList nextPosts];
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadReplies];
}

#pragma mark - Accessors

- (UIBarButtonItem *)cancelButton
{
    if (!_cancelButton)
    {
        _cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
    }
    
    return _cancelButton;
}

- (IBAction)post:(UIBarButtonItem *)sender
{
    if (self.textView.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"post problem title" , nil) message:NSLocalizedString(@"post problem no text message", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];

        [alert show];
    }
    else if (self.textView.text.length > kMaxCharacters)
    {
        NSString *message = NSLocalizedString(@"post problem long text message", nil);
        
        if ([message rangeOfString:@"%d"].location != NSNotFound)           //contains %d ?
            message = [NSString stringWithFormat:message, kMaxCharacters];  //add max chars
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"post problem title" , nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];
        
        [alert show];
    }
    else
    {
        RVSAppComposePostFactory *postFactory;
        
        if (self.replyToPost)
        {
            postFactory = [[RVSAppComposePostFactory alloc] initWithReplyTo:self.replyToPost text:self.textView.text];
        }
        else
        {
            postFactory = [[RVSAppComposePostFactory alloc] initWithPostWithText:self.textView.text clip:self.clip program:self.currentProgram];
        }
        
        [self.delegate composeViewController:self didCompleteWithComposePostFactory:postFactory
                                   thumbnail:[self.imageFactory asyncImage] inReplyToPost:self.replyToPost];
    }
}

- (IBAction)showChannelSelect:(id)sender
{
    [[RVSApplication application] showChannelSelect];
}

#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

@end
