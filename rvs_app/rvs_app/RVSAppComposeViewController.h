//
//  RVSAppComposeViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppStyledViewController.h"
#import "RVSAppComposePostFactory.h"
#import <rvs_sdk_api.h>

@class RVSAppComposeViewController;
@protocol RVSAppComposeViewControllerDelegate <NSObject>

- (void)composeViewController:(RVSAppComposeViewController *)sender
didCompleteWithComposePostFactory:(RVSAppComposePostFactory *)postFactory
                    thumbnail:(NSObject <RVSAsyncImage> *)thumbnail
                inReplyToPost:(NSObject <RVSPost> *)replyToPost;
@end

@interface RVSAppComposeViewController : RVSAppStyledViewController
@property (nonatomic) NSObject <RVSPost> *replyToPost;
@property (weak, nonatomic) id <RVSAppComposeViewControllerDelegate> delegate;
@end
