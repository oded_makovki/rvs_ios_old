//
//  RVSAppUserListFooterCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserListFooterCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppUserListFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserListFooterCell];
    }
    return self;
}

-(void)initRVSAppUserListFooterCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor clearColor];
    
    [self setText:NSLocalizedString(@"user list footer default", nil)
          forType:RVSAppDataListFooterTypeDefault];
    
    [self setText:NSLocalizedString(@"user list footer loading more", nil)
          forType:RVSAppDataListFooterTypeLoading];
    
    [self setText:NSLocalizedString(@"user list footer end of list", nil)
          forType:RVSAppDataListFooterTypeEndOfList];
}

+(CGSize)preferredCellSize;
{
    return CGSizeMake(320, 40);
}

@end
