//
//  ChannelController.h
//  InnovaTV
//
//  Created by Ofer Shem Tov on 12/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import UIKit;
@class Channel;

#import "SearchResult.h"

@interface ChannelController : UIViewController

@property (nonatomic) Channel *channel;
@property (nonatomic) SearchResult *searchResultData;
@property (nonatomic) UIImage *searchResultImage;
@end
