//
//  RVSAppMomentHeaderCell.m
//  rvs_app
//
//  Created by Barak Harel on 1/5/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppMomentHeaderCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppMomentHeaderCell() <RVSAppClipPlayerViewDelegate>
@end

@implementation RVSAppMomentHeaderCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppMomentHeaderCell];
    }
    
    return self;
}

-(void)initRVSAppMomentHeaderCell
{
    [self loadContentsFromNib];
    
//    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){12.0, 12.0}].CGPath;
//    
//    self.momentView.layer.mask = maskLayer;
    
    self.momentView.shouldParseHotwords = NO;
    self.momentView.isUsingLongTimeFormat = NO;
    self.momentView.topPostView.clipPlayerView.delegate = self;
    
    self.momentView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.momentView.topPostView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.momentView.topPostView.programNameLabel.textColor = [UIColor rvsProgramTextColor];    
}

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSAppClipPlayerViewState)newState
{
    //    NSLog(@"RVSAppClipPlayerViewState: %d", (int)newState);
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 239);
}


@end
