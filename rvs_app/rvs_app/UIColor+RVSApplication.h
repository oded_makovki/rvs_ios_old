//
//  UIColor+RVSApplication.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RVSApplication)
+(UIColor *)rvsTintColor;
+(UIColor *)rvsBackgroundColor;
+(UIColor *)rvsFooterTextColor;

+ (UIColor *)rvsTranscriptUnselectedColor;
+ (UIColor *)rvsTranscriptSelectedColor;

+(UIColor *)rvsPostLinkColor;
+(UIColor *)rvsPostTextColor;
+(UIColor *)rvsPostUserNameColor;
+(UIColor *)rvsPostUserHandleColor;
+(UIColor *)rvsPostTimestampColor;

+(UIColor *)rvsReplyLinkColor;
+(UIColor *)rvsReplyTextColor;
+(UIColor *)rvsReplyUserNameColor;
+(UIColor *)rvsReplyUserHandleColor;
+(UIColor *)rvsReplyTimestampColor;

+(UIColor *)rvsMomentLinkColor;
+(UIColor *)rvsMomentTextColor;
+(UIColor *)rvsMomentUserNameColor;
+(UIColor *)rvsMomentUserHandleColor;
+(UIColor *)rvsMomentTimestampColor;
+(UIColor *)rvsMomentTopPostsColor;

+(UIColor *)rvsUserNameColor;
+(UIColor *)rvsUserHandleColor;

+(UIColor *)rvsProgramTimestampColor;
+(UIColor *)rvsProgramTextColor;

@end
