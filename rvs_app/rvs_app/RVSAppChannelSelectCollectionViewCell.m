//
//  RVSAppChannelSelectCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelSelectCollectionViewCell.h"

@interface RVSAppChannelSelectCollectionViewCell()
@property (weak, nonatomic) IBOutlet RVSAppChannelView *channelView;
@end

@implementation RVSAppChannelSelectCollectionViewCell

-(void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    self.channelView.programLabel.highlighted = self.channelView.channelLabel.highlighted = highlighted;
}

-(void)setChannel:(NSObject <RVSChannel> *)channel
{
    _channel = channel;
    self.channelView.channel = channel;
}


@end
