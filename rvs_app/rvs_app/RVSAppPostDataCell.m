//
//  RVSAppPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostDataCell.h"
#import "RVSApplication.h"

@implementation RVSAppPostDataCell

-(void)reset
{
    [self.postView reset];
}

-(void)setData:(RVSAppPostDataObject *)data
{
    [super setData:data];
    if (self.isSizingCell)
    {
        [self.postView setPost:data.post isSizingView:YES];
    }
    else
    {
        self.postView.delegate = data.postViewDelegate;
        [self.postView setPost:data.post isSizingView:NO];
    }
}

-(void)activateWithVisibleFrame:(CGRect)visibleFrame
{
    CGRect playerFrame = [self.postView.clipPlayerView convertRect:self.postView.clipPlayerView.frame toView:self];
    
    if (CGRectContainsRect(visibleFrame, playerFrame)) //player fully visible
    {
        [self.postView.clipPlayerView activateIfNeeded];
        NSLog(@"will play post: %@", self.data.post.postId);
    }
    else
    {
        [self.postView.clipPlayerView stop];
    }
}

@end
