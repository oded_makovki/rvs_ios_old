//
//  SearchResultCell.m
//  InnovaTV
//
//  Created by Barak Harel on 1/26/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "SearchResultCell.h"
#import <UIImageView+AFNetworking.h>
#import "MoviePlayer.h"

@interface SearchResultCell()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelLabel;
@property (weak, nonatomic) IBOutlet UILabel *programLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIImageView *channelImageView;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;

@property (nonatomic) MoviePlayer *moviePlayer;

@end

@implementation SearchResultCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        //meh
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self.channelImageView.layer setMinificationFilter:kCAFilterTrilinear];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playClip)];
    [self addGestureRecognizer:tap];
}

-(void)setData:(SearchResult *)data
{
    _data = data;
    
    self.shareButton.enabled = NO;
    self.textView.attributedText = data.attText;
    
    __weak SearchResultCell *weakSelf = self;
    
    self.thumbnailImageView.image = nil;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:data.imageURL]];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    
    [self.thumbnailImageView setImageWithURLRequest:request placeholderImage:nil success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
        weakSelf.thumbnailImageView.image = image;
        weakSelf.shareButton.enabled = YES;
    } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
        weakSelf.shareButton.enabled = NO;
    }];
    
    if (data.channelImageName && data.channelImageName.length > 0)
    {
        self.channelImageView.hidden = NO;
        self.channelLabel.hidden = YES;
        self.channelImageView.image = [UIImage imageNamed:data.channelImageName];
    }
    else
    {
        self.channelImageView.hidden = YES;
        self.channelLabel.hidden = NO;
        self.channelLabel.text = data.channelName;
    }
    
    self.programLabel.text = data.programTitle;
    self.timeLabel.text = [self timeBetweenNowAndDate:data.time];
}

- (NSString *)timeBetweenNowAndDate:(NSDate*)dateTime
{
    NSTimeInterval seconds = [dateTime timeIntervalSinceNow];
    BOOL isPastTime = NO;
    if (seconds < 0)
    {
        isPastTime = YES;
        seconds = fabs(seconds);
    }
    
    NSInteger weeks = seconds / (3600 * 24 * 7);
    NSInteger days = seconds / (3600 * 24);
    NSInteger hours = seconds / (3600);
    NSInteger minutes = seconds / 60;
    
    long value = 0;
    NSString *c = @"";
    
    if (weeks > 0)
    {
        value = weeks;
        c = @"w";
    }
    else if (days > 0)
    {
        value = days;
        c = @"d";
    }
    else if (hours > 0)
    {
        value = hours;
        c = @"h";
    }
    else if (minutes > 0)
    {
        value = minutes;
        c = @"m";
    }
    else
    {
        value = seconds;
        c = @"s";
    }
    
    return [NSString stringWithFormat:@"%li%@%@", value, c, isPastTime?@" ago":@""];
}

-(void)playClip
{
    [self.data.delegate playClipWithData:self.data andImage:self.thumbnailImageView.image];
}

- (IBAction)share:(id)sender
{
    [self.data.delegate shareToTwitterWithData:self.data andImage:self.thumbnailImageView.image];
}


@end
