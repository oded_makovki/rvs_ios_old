//
//  RVSAppChannelDataObject.h
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVSAppChannelView.h"
#import <rvs_sdk_api.h>

@interface RVSAppChannelDataObject : NSObject
@property (nonatomic) NSObject <RVSChannel> *channel;
@property (weak, nonatomic) id <RVSAppChannelViewDelegate> channelViewDelegate;

-(id)initWithChannel:(NSObject <RVSChannel> *)channel channelViewDelegate:(id <RVSAppChannelViewDelegate>)channelViewDelegate;

+(NSArray *)dataObjectsFromChannels:(NSArray *)channels
                channelViewDelegate:(id<RVSAppChannelViewDelegate>)channelViewDelegate;

+(RVSAppChannelDataObject *)dataObjectsFromChannel:(NSObject <RVSChannel> *)channel channelViewDelegate:(id<RVSAppChannelViewDelegate>)channelViewDelegate;
@end
