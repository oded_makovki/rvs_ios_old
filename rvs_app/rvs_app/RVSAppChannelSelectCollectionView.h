//
//  RVSAppChannelsCollectionView.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppChannelSelectCollectionView : UICollectionView
@property (copy, nonatomic) NSArray *channels;

-(void)scrollToChannelId:(NSString *)channelId animated:(BOOL)animated;
-(CGPoint)centerWithContentOffset:(CGPoint)offset;
@end
