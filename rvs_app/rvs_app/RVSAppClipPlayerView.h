//
//  RVSAppClipPlayerView.h
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

/*!
 Clip view playback states
 */
typedef NS_ENUM(NSUInteger, RVSAppClipPlayerViewState) {
    
    RVSAppClipPlayerViewStateReset = -1,
    RVSAppClipPlayerViewStateStopped = 0,
    RVSAppClipPlayerViewStateLoading = 1,
    RVSAppClipPlayerViewStatePlaying = 2
};

@class RVSAppClipPlayerView;
@protocol RVSAppClipPlayerViewDelegate <NSObject>
@optional
-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSAppClipPlayerViewState)newState;
@end

@interface RVSAppClipPlayerView : UIView

@property (weak, nonatomic) id <RVSAppClipPlayerViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *thumbnailContainer;
@property (weak, nonatomic) IBOutlet UIView *videoContainer;

@property (weak, nonatomic) IBOutlet UIView *overlayContainer;
@property (nonatomic) NSObject <RVSAsyncImage> *thumbnail;
@property (nonatomic, readonly) BOOL isPlaying;
@property (nonatomic, readonly) BOOL isLoading;
@property (nonatomic, readonly) RVSAppClipPlayerViewState playerState;

@property (nonatomic) NSObject <RVSClip> *clip;

/*!
 Toggle fullscreen mode
 Has an effect only when clip is playing
 */
@property (nonatomic) BOOL fullscreen;

-(BOOL)activateIfNeeded;
-(void)reset;
-(void)play;
-(void)stop;

@end
