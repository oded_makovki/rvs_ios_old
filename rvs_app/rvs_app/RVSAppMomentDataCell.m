//
//  RVSAppMomentDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentDataCell.h"
#import "RVSApplication.h"

@implementation RVSAppMomentDataCell

-(void)reset
{
    [self.momentView reset];
}

-(void)setData:(RVSAppMomentDataObject *)data
{
    [super setData:data];
    if (self.isSizingCell)
    {
        [self.momentView setMoment:data.moment isSizingView:YES];
    }
    else
    {
        self.momentView.delegate = data.momentViewDelegate;
        [self.momentView setMoment:data.moment isSizingView:NO];
    }
}

-(void)activateWithVisibleFrame:(CGRect)visibleFrame
{
    CGRect playerFrame = [self.momentView.topPostView.clipPlayerView convertRect:self.momentView.topPostView.clipPlayerView.frame toView:self];
    
    if (CGRectContainsRect(visibleFrame, playerFrame)) //player fully visible
    {
        [self.momentView.topPostView.clipPlayerView activateIfNeeded];
        NSLog(@"will play: %@", self.data.moment.momentId);
    }
    else
    {
        [self.momentView.topPostView.clipPlayerView stop];
    }
}

@end
