//
//  RVSAppDataListView.h
//  rvs_app
//
//  Created by Barak Harel on 11/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppPostView.h"

/**
 *  Data list load phase
 */
typedef NS_ENUM(NSInteger, RVSAppDataListLoadPhase)
{
    /**
     *  Default
     */
    RVSAppDataListLoadPhaseDefault,
    /**
     *  Should request next data when reaching bottom of list
     */
    RVSAppDataListLoadPhaseShouldRequestNextData,
    /**
     *  (Read-only) data list is waiting for data
     */
    RVSAppDataListLoadPhaseRequestingData,
    /**
     *  List reached its end
     */
    RVSAppDataListLoadPhaseEndOfList
};

@class RVSAppDataListView;
@protocol RVSAppDataListViewDelegate <NSObject>
-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView;
-(void)refreshDataListView:(RVSAppDataListView *)dataListView;
@optional
-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath withData:(NSObject *)data;
-(BOOL)dataListView:(RVSAppDataListView *)dataListView shouldActivateItemAtIndexPath:(NSIndexPath *)indexPath;
-(NSString *)descriptionForDataListView:(RVSAppDataListView *)dataListView;
@end

@interface RVSAppDataListView : UIView

@property (weak, nonatomic) id <RVSAppDataListViewDelegate> delegate;

@property (nonatomic) NSError *error;

@property (nonatomic) BOOL refreshEnabled;

/**
 *  set/get loadPhase, see RVSAppDataListLoadPhase
 */
@property (nonatomic) RVSAppDataListLoadPhase loadPhase;

/**
 *  Distance from bottom of list that will trigger next page request
 */
@property (nonatomic) NSInteger nextPageTriggerDistance;

/**
 *  Section number that can trigger next page
 */
@property (nonatomic) NSInteger nextPageTriggerSection;

//must be set!
@property (nonatomic) NSInteger numberOfSections;

@property (nonatomic) BOOL scrollsToTop;


-(void)activateIfNeeded;
-(void)clear;
//-(void)clearDataIfNeeded;
//-(void)needsClearData;

-(void)clearDataInSection:(NSInteger)section;

-(void)clear:(BOOL)clear andAddData:(NSArray *)newData toSection:(NSInteger)section;

-(void)addData:(NSArray *)newData toSection:(NSInteger)section;
-(void)addData:(NSArray *)newData toSection:(NSInteger)section animated:(BOOL)animated;
-(void)setSectionsCellClass:(Class)cellClass forSection:(NSInteger)section;
-(void)setFooterCellClass:(Class)footerCellClass;

-(void)scrollToTop;
-(void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;

@end
