//
//  NSDate+RVSApplication.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, NSDateComponentType)
{
    NSDateComponentTypeDays,
    NSDateComponentTypeHours,
    NSDateComponentTypeMinutes,
    NSDateComponentTypeSeconds
};


@interface NSDate (RVSApplication)
-(NSString *)timeShortStringSinceNow;
-(NSString *)timeShortStringSinceDate:(NSDate *)anotherDate;
-(NSString *)timeLongStringSinceNow;
-(NSString *)timeLongStringSinceDate:(NSDate *)anotherDate;

-(NSString *)rvsTimeLongStringSinceNow;
@end
