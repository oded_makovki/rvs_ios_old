//
//  RVSAppChannelView.m
//  rvs_app
//
//  Created by Barak Harel on 11/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppChannelView.h"
#import <rvs_sdk_api.h>

@interface RVSAppChannelView()
@property (strong,nonatomic) NSObject <RVSAsyncProgram> *asyncCurrentProgram;
@property (strong,nonatomic) NSObject <RVSProgram> * currentProgram;

@property (strong, nonatomic) NSObject <RVSAsyncImage> *channelAsyncImage;
@end

@implementation RVSAppChannelView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initRVSAppChannelView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppChannelView];
}

-(void)initRVSAppChannelView
{
     self.programLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:self.programLabel.font.pointSize];
    
    [self.channelImageView.layer setMinificationFilter:kCAFilterTrilinear];
}

-(void)reset
{
    //clear
    self.channelImageView.image = nil;
    self.channelAsyncImage = nil;
    
    self.channelLabel.text = NSLocalizedString(@"channel placeholder", nil);
    self.programLabel.text = NSLocalizedString(@"program placeholder", nil);
}

-(void)setChannel:(NSObject <RVSChannel> *)channel
{
    if (_channel == channel)
        return;
    
    [self reset];
    _channel = channel;
    
    if (!_channel)
        return;
    
    //update metadata
    NSString *channelName = self.channel.name;
    NSObject <RVSAsyncImageFactory> *imageFactory = self.channel.logo;
    self.channelAsyncImage = [imageFactory asyncImage];
    
    //update name
    if (self.channelLabel.text && channelName && channelName.length > 0)
         self.channelLabel.text = channelName;
    
    //update program
    if (self.programLabel)
    {
        [self getCurrentProgram];
    }
}

-(void)setChannelAsyncImage:(NSObject <RVSAsyncImage> *)channelAsyncImage
{
    if (_channelAsyncImage == channelAsyncImage)
        return;
    
    _channelAsyncImage = channelAsyncImage;
    self.channelImageView.image = nil;
    
    __weak RVSAppChannelView *weakSelf = self;
    [weakSelf.channelAsyncImage imageWithSize:weakSelf.channelImageView.frame.size usingBlock:^(UIImage *image, NSError *error) {
        weakSelf.channelImageView.image = image;
    }];
}

- (void)getCurrentProgram
{
    self.asyncCurrentProgram = [[RVSSdk sharedSdk] updatingCurrentProgramForChannel:self.channel.channelId];
    
    __weak RVSAppChannelView *weakSelf = self;
    [weakSelf.asyncCurrentProgram programUsingBlock:^(NSObject<RVSProgram> *program, NSError *error) {
        if (error)
        {
            //            NSLog(@"Error in programsMetadataUsingBlock: %@", error);
            return;
        }
        
        weakSelf.currentProgram = program;
    }];
}

-(void)setCurrentProgram:(NSObject <RVSProgram> *)currentProgram
{
    if (_currentProgram == currentProgram)
        return;
    
    _currentProgram = currentProgram;
    self.programLabel.text = [currentProgram.content.name uppercaseString];
}

@end
