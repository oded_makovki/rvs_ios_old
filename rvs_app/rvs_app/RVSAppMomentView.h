//
//  RVSAppMomentCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppPostView.h"

@class RVSAppMomentView;
@protocol RVSAppMomentViewDelegate <RVSAppPostViewDelegate>

@optional
-(void) momentView:(RVSAppMomentView *)momentView didSelectUser:(NSObject <RVSUser> *)user;
-(void) momentView:(RVSAppMomentView *)momentView didSelectPostsCountForMoment:(NSObject <RVSMoment> *)moment;
-(void) momentView:(RVSAppMomentView *)momentView didSelectFavoritesCountForMoment:(NSObject <RVSMoment> *)moment;
-(void) momentView:(RVSAppMomentView *)momentView didSelectRepostsCountForMoment:(NSObject <RVSMoment> *)moment;
@end

@interface RVSAppMomentView : UIView

@property (weak, nonatomic) IBOutlet RVSAppPostView *topPostView;
@property (weak, nonatomic) IBOutlet UILabel *topPostsLabel;

@property (weak, nonatomic) IBOutlet UILabel *postsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoritesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *repostsCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *postsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *favoritesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *repostsTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *postsCountTapView;
@property (weak, nonatomic) IBOutlet UIView *favoritesCountTapView;
@property (weak, nonatomic) IBOutlet UIView *repostsCountTapView;

@property (weak, nonatomic) IBOutlet UILabel *likeInfoLabel;

@property (weak, nonatomic) IBOutlet UILabel *commentInfoLabel;

@property (weak, nonatomic) id <RVSAppMomentViewDelegate> delegate;
@property (nonatomic) NSObject <RVSMoment> *moment;

@property (weak, nonatomic) IBOutlet UIView *usersImagesContainer;
@property (nonatomic) CGFloat minimumUsersImagesSpacing;

@property (nonatomic) BOOL isUsingLongTimeFormat;
@property (nonatomic) BOOL shouldParseHotwords;

-(void)reset;

/**
 *  set moment for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param moment moment object
 *  @param isSizingView set to YES for optimiziation
 */
-(void)setMoment:(NSObject <RVSMoment> *)moment isSizingView:(BOOL)isSizingView;
/**
 *  set moment for view. same as setMoment:moment isSizingView:NO
 *
 *  @param moment           moment object
 */
-(void)setMoment:(NSObject<RVSMoment> *)moment;
@end
