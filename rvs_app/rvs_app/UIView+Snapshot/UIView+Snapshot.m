//
//  UIView+Snapshot.m
//  rvs_app
//
//  Created by Barak Harel on 11/13/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "UIView+Snapshot.h"
#import "UIImage+ImageEffects.h"

@implementation UIView (Snapshot)

-(UIImage *)snapshot
{
    return [self snapshotInRect:self.bounds];
}

-(UIImage *)snapshotInRect:(CGRect)rect
{
    // Create the image context
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, self.window.screen.scale);
    
    // There he is! The new API method
    [self drawViewHierarchyInRect:rect afterScreenUpdates:NO];
    
    // Get the snapshot
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Be nice and clean your mess up
    UIGraphicsEndImageContext();
    
    return snapshotImage;
}

-(UIImage *)blurredSnapshot
{
    return [self blurredSnapshotWithParameters:nil inRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

-(UIImage *)blurredSnapshotInRect:(CGRect)rect
{
    return [self blurredSnapshotWithParameters:nil inRect:rect];
}

-(UIImage *)blurredSnapshotWithParameters:(BlurParameters *)parameters
{
    return [self blurredSnapshotWithParameters:parameters inRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

-(UIImage *)blurredSnapshotWithParameters:(BlurParameters *)parameters inRect:(CGRect)rect
{
    UIImage *snapshotImage = [self snapshotInRect:rect];
    
    if (!parameters)
    {
        //set default
        parameters = [BlurParameters lightEffect];
    }
    
    UIImage *blurredSnapshotImage = [snapshotImage applyBlurWithRadius:parameters.radius tintColor:parameters.tintColor saturationDeltaFactor:parameters.saturationDeltaFactor maskImage:parameters.maskImage];
    
    return blurredSnapshotImage;
}

@end


@interface BlurParameters()
@end

@implementation BlurParameters

+ (BlurParameters *) lightEffect {
    BlurParameters *parameters = [[BlurParameters alloc] init];
    
    parameters.radius = 6;
    parameters.tintColor = [UIColor colorWithWhite:.8f alpha:.2f];
    parameters.saturationDeltaFactor = 1.8f;
    parameters.maskImage = nil;
    
    return parameters;
}

+ (BlurParameters *) darkEffect {
    BlurParameters *parameters = [[BlurParameters alloc] init];
    
    parameters.radius = 8;
    parameters.tintColor = [UIColor colorWithRed:0.0f green:0.0 blue:0.0f alpha:.5f];
    parameters.saturationDeltaFactor = 3.0f;
    parameters.maskImage = nil;
    
    return parameters;
}

+ (BlurParameters *) coralEffect {
    BlurParameters *parameters = [[BlurParameters alloc] init];
    
    parameters.radius = 8;
    parameters.tintColor = [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:.1f];
    parameters.saturationDeltaFactor = 3.0f;
    parameters.maskImage = nil;
    
    return parameters;
}

+ (BlurParameters *) neonEffect {
    BlurParameters *parameters = [[BlurParameters alloc] init];
    
    parameters.radius = 8;
    parameters.tintColor = [UIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:.1f];
    parameters.saturationDeltaFactor = 3.0f;
    parameters.maskImage = nil;
    
    return parameters;
}

+ (BlurParameters *) skyEffect {
    BlurParameters *parameters = [[BlurParameters alloc] init];
    
    parameters.radius = 8;
    parameters.tintColor = [UIColor colorWithRed:0.0f green:0.0f blue:1.0f alpha:.1f];
    parameters.saturationDeltaFactor = 3.0f;
    parameters.maskImage = nil;
    
    return parameters;
}

@end