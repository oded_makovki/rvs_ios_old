//
//  UIView+Snapshot.h
//  rvs_app
//
//  Created by Barak Harel on 11/13/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BlurParameters;
@interface UIView (Snapshot)
-(UIImage *)snapshot;
-(UIImage *)snapshotInRect:(CGRect)rect;

-(UIImage *)blurredSnapshot;
-(UIImage *)blurredSnapshotInRect:(CGRect)rect;

-(UIImage *)blurredSnapshotWithParameters:(BlurParameters *)parameters;
-(UIImage *)blurredSnapshotWithParameters:(BlurParameters *)parameters inRect:(CGRect)rect;
@end


/// Blur color components.
///
@interface BlurParameters : NSObject

@property(nonatomic, assign) CGFloat radius;
@property(nonatomic, strong) UIColor *tintColor;
@property(nonatomic, assign) CGFloat saturationDeltaFactor;
@property(nonatomic, strong) UIImage *maskImage;

///Light color effect.
///
+ (BlurParameters *) lightEffect;

///Dark color effect.
///
+ (BlurParameters *) darkEffect;

///Coral color effect.
///
+ (BlurParameters *) coralEffect;

///Neon color effect.
///
+ (BlurParameters *) neonEffect;

///Sky color effect.
///
+ (BlurParameters *) skyEffect;

@end