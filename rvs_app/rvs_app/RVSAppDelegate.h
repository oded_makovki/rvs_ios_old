//
//  RVSAAppDelegate.h
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RVSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
