//
//  RVSAppMomentDetailCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMomentDetailCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppMomentDetailCell() <RVSAppClipPlayerViewDelegate>
@end

@implementation RVSAppMomentDetailCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppMomentDetailCell];
    }
    
    return self;
}

-(void)initRVSAppMomentDetailCell
{
    [self loadContentsFromNib];
    
//    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: UIRectCornerTopLeft | UIRectCornerTopRight cornerRadii: (CGSize){12.0, 12.0}].CGPath;
//    
//    self.momentView.layer.mask = maskLayer;
    
    self.momentView.shouldParseHotwords = NO;
    self.momentView.isUsingLongTimeFormat = NO;
    self.momentView.topPostView.clipPlayerView.delegate = self;
    
    self.momentView.clipsToBounds = YES;
    
    self.backgroundColor = [UIColor clearColor];
    self.momentView.topPostView.programTimeLabel.textColor = [UIColor rvsProgramTimestampColor];
    self.momentView.topPostView.programNameLabel.textColor = [UIColor rvsProgramTextColor];    
    self.momentView.topPostView.textLabel.preferredMaxLayoutWidth = 215;
    
    self.momentView.topPostsLabel.text = [NSString stringWithFormat:@"%@:", NSLocalizedString(@"top post title", nil)];
    
    self.momentView.minimumUsersImagesSpacing = 2.0f;
}

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSAppClipPlayerViewState)newState
{
//    NSLog(@"RVSAppClipPlayerViewState: %d", (int)newState);
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 275);
}


@end
