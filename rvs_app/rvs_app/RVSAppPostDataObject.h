//
//  RVSAppPostCellData.h
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>
#import "RVSAppPostView.h"

@interface RVSAppPostDataObject : NSObject

@property (weak, nonatomic) id <RVSAppPostViewDelegate> postViewDelegate;
@property (strong, nonatomic) NSObject <RVSPost> *post;

-(id)initWithPost:(NSObject <RVSPost> *)post postViewDelegate:(id <RVSAppPostViewDelegate>)postViewDelegate;

#pragma mark - Helpers

+(NSArray *)dataObjectsFromPosts:(NSArray *)posts
                     postViewDelegate:(id<RVSAppPostViewDelegate>)postViewDelegate;

+(RVSAppPostDataObject *)dataObjectFromPost:(NSObject <RVSPost> *)post
                                 postViewDelegate:(id<RVSAppPostViewDelegate>)postViewDelegate;


@end
