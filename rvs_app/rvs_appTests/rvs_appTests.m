//
//  rvs_appTests.m
//  rvs_appTests
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface rvs_appTests : XCTestCase

@end

@implementation rvs_appTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
