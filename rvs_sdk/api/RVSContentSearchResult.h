//
//  RVSContentSearchResult.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSSearchResult.h"

@protocol RVSProgramExcerpt;
@protocol RVSPost;

/**
 *  Search content result, can either be a program excerpt or a post
 */
@protocol RVSContentSearchResult <RVSSearchResult>

/**
 *  Program excerpt found or nil if result is not a program excerpt
 */
@property (nonatomic, readonly) NSObject<RVSProgramExcerpt> *programExcerpt;

/**
 *  Post found or nil if result is not a post
 */
@property (nonatomic, readonly) NSObject<RVSPost> *post;


@end
