//
//  RVSDataObject.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

@protocol RVSDataObject <NSObject>

- (NSArray*)rvsSdkProperties;

@end
