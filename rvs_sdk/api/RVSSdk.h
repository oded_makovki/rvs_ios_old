//
//  RVSSdk.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/20/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

/*!
 RVS SDK singleton
 */
@interface RVSSdk : NSObject

/*!
 Returns the RVS SDK singleton.
 SDK will created if did not exist yet.
 @returns SDK singleton
*/
+ (RVSSdk*)sharedSdk;

@end
