//
//  RVSProgramExcerpt.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSChannel;
@protocol RVSProgram;
@protocol RVSMediaContext;
@protocol RVSAsyncImage;

/**
 *  Program excerpt object generated by content search
 */
@protocol RVSProgramExcerpt <RVSDataObject>

/**
 *  Channel
 */
@property (nonatomic, readonly) NSObject<RVSChannel> *channel;

/**
 *  Program
 */
@property (nonatomic, readonly) NSObject<RVSProgram> *program;

/**
 *  Media context
 */
@property (nonatomic, readonly) NSObject<RVSMediaContext> *mediaContext;

/**
 *  Excerpt cover image
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *coverImage;

/*!
 Program transcript
 */
@property (nonatomic, readonly) NSString *transcript;

@end
