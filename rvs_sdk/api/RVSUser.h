//
//  RVSUser.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/12/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSAsyncImage;

/*!
 User object, some properties can be nil if no value
 */
@protocol RVSUser <RVSDataObject>

/*!
 User ID
 */
@property (nonatomic, readonly) NSString *userId;

/*!
 User name
 */
@property (nonatomic, readonly) NSString *name;

/*!
 YES if user is a verfied user
 */
@property (nonatomic, readonly) NSNumber *isVerified;

/*!
 Profile image
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *profileImage;

/*!
 YES is user if followed by me
 */
@property (nonatomic, readonly) NSNumber *isFollowedByMe;

/*!
 Number of users following this user
 */
@property (nonatomic, readonly) NSNumber *followerCount;

/*!
 Number of users followed by this user
 */
@property (nonatomic, readonly) NSNumber *followingCount;


/*!
 Number of posts created by this user
 */
@property (nonatomic, readonly) NSNumber *postCount;

/**
 *  Number of likes created by this user
 */
@property (nonatomic, readonly) NSNumber *likeCount;

/**
 *  Number of likes reposts by this user
 */
@property (nonatomic, readonly) NSNumber *repostCount;

@end
