//
//  RVSErrors.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

/*!
 RVS SDK error domain
 */
extern NSString *RVSErrorDomain;

/*!
 RVS SDK error codes
 */
typedef NS_ENUM(NSUInteger, RVSErrorCode) {
    
    /*!
     Operation failed because client is not signed in or provided bad credentials.
     */
    RVSErrorCodeSignInRequired,
};