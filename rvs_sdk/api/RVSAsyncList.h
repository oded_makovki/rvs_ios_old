//
//  RVSAsyncList.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

@import Foundation;
@class RVSPromise;

/*!
 Asynchronous object list
 */
@protocol RVSAsyncList <NSObject>

/*!
 Get more object
 The returned promise is retained by the async list. To cancel the request just release the async list
 @param count desired for number of posts to receive, actual received number can be different
 @return promise for NSArray
 */
- (RVSPromise*)next:(NSUInteger)count;

/**
 *  Flag if list reached its end
 */
@property (nonatomic, readonly) BOOL didReachEnd;

@end
