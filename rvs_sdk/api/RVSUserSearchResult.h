//
//  RVSUserSearchResult
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSSearchResult.h"

@protocol RVSUser;

@protocol RVSUserSearchResult <RVSSearchResult>

/**
 *  User found or nil
 */
@property (nonatomic, readonly) NSObject<RVSUser> *user;

@end
