//
//  RVSProgram.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSAsyncImage;

/**
 *  A cast member participating in the show
 */
@protocol RVSCastMember <RVSDataObject>

/**
 *  character
 */
@property(nonatomic, readonly) NSString *character;

/**
 *  actor playing the character
 */
@property(nonatomic, readonly) NSString *actor;

@end

/**
 *  Show object
 */
@protocol RVSShow <RVSDataObject>

/**
 *  show ID
 */
@property(nonatomic, readonly) NSString *showId;

/**
 *  show display name
 */
@property(nonatomic, readonly) NSString *name;

/**
 *  show synopsis
 */
@property(nonatomic, readonly) NSString *synopsis;

/**
 *  array of RVSCastMember participating in the show
 */
@property (nonatomic, readonly) NSArray * castMembers;

/**
 *  image for show
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *image;

/**
 *  official show site URL
 */
@property (nonatomic, readonly) NSURL *officialUrl;

@end

/**
 *  Episode object
 */
@protocol RVSEpisode <RVSShow>

/**
 *  season number
 */
@property (nonatomic, readonly) NSNumber *seasonNumber;

/**
 *  episode number
 */
@property (nonatomic, readonly) NSNumber *episodeNumber;

@end

/*!
 Program information object about contnet in a specific time
 */
@protocol RVSProgram <RVSDataObject>

/**
 *  program ID
 */
@property (nonatomic, readonly) NSString *programId;

/*!
 program start time in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *start;

/*!
 program end time in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *end;

/**
 *  program show, might have additional, more specific, contnet if program is an episode
 */
@property (nonatomic, readonly) NSObject<RVSShow> *show;

/**
 *  episode object is program is an episode or nil if not an episode
 */
@property (nonatomic, readonly) NSObject <RVSEpisode> *episode;

@end