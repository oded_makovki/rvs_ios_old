//
//  RVSSdk+Posts.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"
@protocol RVSAsyncUserList;
@protocol RVSAsyncPostList;
@protocol RVSAsyncError;
@protocol RVSAsyncMoment;
@protocol RVSPost;
@protocol RVSProgram;
@protocol RVSAsyncList;
@class RVSPromise;

@interface RVSSdk (Posts)

/*!
 Create post with text and a channel clip.
 @param text post text
 @param mediaContext media context used for composing the post
 @startOffset post clip start offset in seconds relative ot the media context
 @duration post clip duration in seconds
 @coverImageId the thumbnail ID to use as the post cover image
 @return post promise object allowing to submit the post and receive the submitted post or an error if failed
 */
- (RVSPromise*)createPostWithText:(NSString*)text mediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString*)coverImageId;

/*!
 Create comment with text and a channel clip.
 @param text post text
 @param postId post id of the related post
 
 @return comment promise object allowing to submit the comment and receive the submitted comment or an error if failed
 */
- (RVSPromise*)createCommentWithMessage:(NSString*)message forPostId:(NSString*)postId;

/*!
 Get post objet for post ID
 @param postId post to get post object for
 @return async post object
 */
- (RVSPromise*)postForPostId:(NSString*)postId;

/**
 *  Get tending post list
 *
 *  @return async list with RVSPost objects
 */
- (NSObject<RVSAsyncList>*)trendingPosts;

/**
 *  Get sharing URL for a post
 *
 *  @param postId ID of post to share
 *
 *  @return promise for NSURL
 */
- (RVSPromise*)sharingUrlForPost:(NSString*)postId;

/**
 *  Get comments list for post ID
 *
 *  @param postId postId post to get comments for
 *
 *  @return async list with RVSComment objects
 */
- (NSObject<RVSAsyncList>*)commentsForPost:(NSString*)postId;

/*!
 Get users that liked a post
 @param postId id of post to get favoriting users for
 @return async list object
 */
- (NSObject<RVSAsyncList>*)likingUsersForPost:(NSString*)postId;

/*!
 Get users that reposted a post
 @param postId id of post to get reposting users for
 @return async list object
 */
- (NSObject<RVSAsyncList>*)repostingUsersForPost:(NSString*)postId;

/*!
 Like a post
 @param postId post fo like
 @return promise with some object if success, object should be ignored
 */
- (RVSPromise*)createLikeForPost:(NSString*)postId;

/*!
 Unlike a post
 @param postId post fo unlike
 @return promise with some object if success, object should be ignored
 */
- (RVSPromise*)deleteLikeForPost:(NSString*)postId;

/*!
 Repost a post
 @param postId post fo repost
 @return promise with some object if success, object should be ignored
 */
- (RVSPromise*)createRepostForPost:(NSString*)postId;

/*!
 Delete a repost to a post
 @param postId post fo unrepost
 @return promise with some object if success, object should be ignored
 */
- (RVSPromise*)deleteRepostForPost:(NSString*)postId;

/*!
 Delete a post
 @param postId post to delete
 @return promise object allowing to delete the post or an error if failed
 */
- (RVSPromise*)deletePost:(NSString*)postId;

/**
 *  Delete a comment
 *
 *  @param commentId comment to delete
 *  @param postId    parent post of comment
 *
 *  @return promise object allowing to delete the comment or an error if failed
 */
- (RVSPromise*)deleteComment:(NSString *)commentId fromPost:(NSString*)postId;

@end
