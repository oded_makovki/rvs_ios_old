//
//  RVSFragmentRange.h
//  rvs_sdk
//
//  Created by Barak Harel on 5/11/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

/**
 *  A Fragment range
 */
@protocol RVSFragmentRange <RVSDataObject>

/**
 *  The offset from the fragment start
 */
@property (nonatomic, readonly) NSNumber *matchCharacterOffset;

/**
 *  How Many characters are matched in the fragment
 */
@property (nonatomic, readonly) NSNumber *matchCharacterCount;

@end
