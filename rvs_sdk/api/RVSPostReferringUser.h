//
//  RVSPostReferringUser.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"
@protocol RVSUser;

@protocol RVSPostReferringUser <RVSDataObject>

/*!
 Post referring user
 */
@property (nonatomic, readonly) NSObject <RVSUser> *user;

/*!
 Time re-posted in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *timestamp;

@end
