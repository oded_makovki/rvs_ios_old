//
//  RVSSdk+Channels.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"

@protocol RVSChannel;
@protocol RVSAsyncImage;
@protocol RVSMediaContext;
@class RVSPromise;

/*!
 Notification when channel detector identifies a channel/program
*/
extern NSString *RVSSdkChannelDetectorMatchNotfication;

/*!
 Channel object key in channel detector notification user info
 */
extern NSString *RVSSdkChannelDetectorChannelUserInfoKey;

/**
 *  Time object key in channel detector notification user info
 */
extern NSString *RVSSdkChannelDetectorTimeUserInfoKey;

/*!
 Notification when channel detector encounters an error
 */
extern NSString *RVSSdkChannelDetectorErrorNotfication;

/*!
 Error object key in channel detector notification user info
 */
extern NSString *RVSSdkChannelDetectorErrorUserInfoKey;

/*!
 Notification when channel detector status changes
 */
extern NSString *RVSSdkChannelDetectorStatusNotfication;

/*!
 Status object key in channel detector notification user info. 
 Value is RVSSdkChannelDetectorStatus wrapped in NSNumber.
 */
extern NSString *RVSSdkChannelDetectorStatusUserInfoKey;

/**
 * Channel detector status
 */
typedef NS_ENUM(NSInteger, RVSSdkChannelDetectorStatus)
{
    RVSSdkChannelDetectorStatusError = -1,
    RVSSdkChannelDetectorStatusIdle,
    RVSSdkChannelDetectorStatusDetecting,
    RVSSdkChannelDetectorStatusMatching
};

/*!
 RVS SDK channels category
 */
@interface RVSSdk (Channels)

/*!
 Enable/Disable channel detector
 Does nothing if already enabled/disabled
 @param enable YES to enable, NO to disable
 */
- (void)enableChannelDetector:(BOOL)enable;

/*!
 Create media context for live channel
 Should be used when composing posts only since it uses the DVR for the channel
 @param channelId channel ID to use for creating the media context
 @return a promise RVSMediaContext object
 */
- (RVSPromise*)mediaContextForChannel:(NSString*)channelId;

/**
 *  Get channel object from channel call sign
 *
 *  @param callSign call sign to search
 *
 *  @return promise for RVSChannel object
 */
- (RVSPromise*)channelForCallSign:(NSString*)callSign;

/**
 *  Get current program for a channel
 *
 *  @param channelId channel ID
 *
 *  @return promise for RVSProgram object
 */
- (RVSPromise*)currentProgramForChannelId:(NSString*)channelId;

@end
