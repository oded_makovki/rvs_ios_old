//
//  RVSPost.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/13/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSUser;
@protocol RVSChannel;
@protocol RVSProgram;
@protocol RVSMediaContext;
@protocol RVSAsyncImage;
@protocol RVSComment;
@protocol RVSPostReferringUser;

/*!
 Post object
 */
@protocol RVSPost <RVSDataObject>

/*!
 Post ID
 */
@property (nonatomic, readonly) NSString *postId;

/*!
 Post text
 */
@property (nonatomic, readonly) NSString *text;

/*!
 Post authoer user
 */
@property (nonatomic, readonly) NSObject<RVSUser> *author;

/*!
 Time posted in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *creationTime;

/*!
 Channel the post is about
 */
@property (nonatomic, readonly) NSObject<RVSChannel> *channel;

/*!
 Program the post is about
 */
@property (nonatomic, readonly) NSObject<RVSProgram> *program;

/*!
 Post clip
 */
@property (nonatomic, readonly) NSObject<RVSMediaContext> *mediaContext;

/*!
 Post cover image
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *coverImage;

/*!
 YES if post is favorited by me
 */
@property (nonatomic, readonly) NSNumber *likedByCaller;

/*!
 YES if post is reposted by me
 */
@property (nonatomic, readonly) NSNumber *repostedByCaller;

/*!
 Favorite count
 */
@property (nonatomic, readonly) NSNumber *likeCount;

/*!
 Repost count
 */
@property (nonatomic, readonly) NSNumber *repostCount;

/*!
 Reposting referr if a repost
 */
@property (nonatomic, readonly) NSObject<RVSPostReferringUser> *repostingUser;

/**
 *  the number of comments made on this post
 */
@property (nonatomic, readonly) NSNumber *commentCount;

/**
 *  the latest comments on this post
 */
@property (nonatomic, readonly) NSArray <RVSComment> *latestComments;

@end
