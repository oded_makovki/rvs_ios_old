//
//  RVSSearchMatch.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSFragmentRange;
@protocol RVSMediaContext;
@protocol RVSAsyncImage;

/**
 *  A search match
 */
@protocol RVSSearchMatch <RVSDataObject>

/**
 *  The field name that the highlight resides
 */
@property (nonatomic, readonly) NSString *fieldName;

/**
 *  An excerpt for the highlight
 */
@property (nonatomic, readonly) NSString *valueFragment;

/**
 *  An array of RVSFragmentRange
 */
@property (nonatomic, readonly) NSArray *fragmentRanges;

/**
 *  Applies only when the fieldName is captions, it holds the start time for where the match starts
 */
@property (nonatomic, readonly) NSNumber *referenceTime;

/**
 *  Match media context
 */
@property (nonatomic, readonly) NSObject<RVSMediaContext> *mediaContext;

/*!
 Post cover image
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *coverImage;

@end
