//
//  RVSSdk+Users.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"

@protocol RVSAsyncPostList;
@protocol RVSAsyncUserList;
@protocol RVSAsyncError;
@protocol RVSAsyncUser;
@protocol RVSAsyncList;

@interface RVSSdk (Users)

/*!
 Get user objet for user ID
 @param userId user id to get user object for
 @return promise for user object
 */
- (RVSPromise*)userForUserId:(NSString*)userId;

/*!
 Get user posts
 @param userId id of user to get posts for
 @return async post list
 */
- (NSObject<RVSAsyncList>*)userFeedForUser:(NSString*)userId;

/*!
 Get user news feed
 @param userId id of user to get news feed for
 @return async post list
 */
- (NSObject<RVSAsyncList>*)newsFeedForUser:(NSString*)userId;

/*!
 Get user followers
 @param userId id of user to get followers for
 @return async post list object
 */
- (NSObject<RVSAsyncList>*)followersOfUser:(NSString*)userId;

/*!
 Get users following a user
 @param userId id of user to get followed users for
 @return async post list object
 */
- (NSObject<RVSAsyncList>*)followedByUser:(NSString*)userId;

/*!
 Get user liked posts
 @param userId id of user to get liked posts for
 @return async post list object
 */
- (NSObject<RVSAsyncList>*)likedPostsByUser:(NSString*)userId;

/*!
 Follow user
 @param userId user to follow
 @return promise for NSNull
 */
- (RVSPromise*)followUser:(NSString*)userId;

/*!
 Unfollow user
 @param userId user to unfollow
 @return promise for NSNull
 */
- (RVSPromise*)unfollowUser:(NSString*)userId;

@end
