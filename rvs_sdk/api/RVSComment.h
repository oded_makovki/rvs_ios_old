//
//  RVSComment.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSUser;

@protocol RVSComment <RVSDataObject>

/*!
 Comment ID
 */
@property (nonatomic, readonly) NSString *commentId;

/*!
 Comment text
 */
@property (nonatomic, readonly) NSString *message;

/*!
 Comment authoer user
 */
@property (nonatomic, readonly) NSObject <RVSUser> *author;

/*!
 Time posted in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *creationTime;

@end
