//
//  RVSPromise.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 3/27/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <RXPromise/RXPromise.h>

@class RVSPromiseHolder;

typedef void (^promise_noreturnvalue_completionHandler_t)(id result);
typedef void (^promise_noreturnvalue_errorHandler_t)(NSError* error);

@interface RVSPromise : RXPromise

@property (nonatomic, readonly) RVSPromiseHolder *newHolder;

-(RXPromise*)thenOnMain:(promise_noreturnvalue_completionHandler_t)onSuccess error:(promise_noreturnvalue_errorHandler_t)onFailure;

@end

typedef void (^promise_holder_completionHandler_t)(id result);
typedef void (^promise_holder_errorHandler_t)(NSError* error);
typedef RXPromise* (^promise_holder_then_block_t)(promise_noreturnvalue_completionHandler_t, promise_noreturnvalue_errorHandler_t);

@interface RVSPromiseHolder : NSObject

@property (nonatomic, readonly) RVSPromise *promise;

-(RXPromise*)thenOnMain:(promise_noreturnvalue_completionHandler_t)onSuccess error:(promise_noreturnvalue_errorHandler_t)onFailure;


@end
