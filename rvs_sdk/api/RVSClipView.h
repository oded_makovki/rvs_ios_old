//
//  RVSClipView.h
//
//
//  Created by Ofer Shem Tov on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import UIKit;
@import AVFoundation;
@protocol RVSMediaContext;
@class RVSClipView;

/*!
 Clip view delegate protocol
 */
@protocol RVSClipViewDelegate <NSObject>

/*!
 Called when clip playback state changed
 @param clipView clip view for which the state was changed
 */
- (void)clipViewStateDidChange:(RVSClipView*)clipView;
@optional
/**
 *  Called when player head changes position
 *
 *  @param clipView    clip view object
 *  @param currentTime current time
 *  @param duration    duration of playing item
 */
- (void)clipView:(RVSClipView*)clipView didProgressWithCurrent:(NSTimeInterval)current duration:(NSTimeInterval)duration;
/**
 *  Called when buffering state changes
 *
 *  @param clipView    clip view object
 *  @param isBuffering is buffering flag
 */
- (void)clipView:(RVSClipView*)clipView isBuffering:(BOOL)isBuffering;
@end

/*!
 Clip view playback states
 */
typedef NS_ENUM(NSUInteger, RVSClipViewState) {
    
    /*!
     Clip is stopped
     */
    RVSClipViewStateUnknown = 0,
    
    /*!
     Clip is loading
     */
    RVSClipViewStateLoading,
    
    /*!
     Clip is playing
     */
    RVSClipViewStatePlaying,
    
    /*!
     Clip is paused
     */
    RVSClipViewStatePaused,
    
    /*!
     Clip has ended
     */
    RVSClipViewStatePlaybackEnded,
    
    /*!
     Clip has failed
     */
    RVSClipViewStateFailed
};

/*!
 View for playing clips
 */
@interface RVSClipView : UIView

/*!
 Play clip
 */
- (void)play;

/*!
 Stop playback
 */
- (void)stop;

/*!
 Pause playback
 */
- (void)pause;

/**
 *  Rewind and play
 */
- (void)rewind;

/*!
 @method		accessLog
 @abstract		Returns an object that represents a snapshot of the network access log. Can be nil.
 @discussion	An AVPlayerItemAccessLog provides methods to retrieve the network access log in a format suitable for serialization.
 If nil is returned then there is no logging information currently available for this AVPlayerItem.
 An AVPlayerItemNewAccessLogEntryNotification will be posted when new logging information becomes available. However, accessLog might already return a non-nil value even before the first notification is posted.
 @result		An autoreleased AVPlayerItemAccessLog instance.
 */
- (AVPlayerItemAccessLog *)playerAccessLog;

/*!
 @method		errorLog
 @abstract		Returns an object that represents a snapshot of the error log. Can be nil.
 @discussion	An AVPlayerItemErrorLog provides methods to retrieve the error log in a format suitable for serialization.
 If nil is returned then there is no logging information currently available for this AVPlayerItem.
 @result		An autoreleased AVPlayerItemErrorLog instance.
 */
- (AVPlayerItemErrorLog *)playerErrorLog;

/**
 *  Set clip to play
 *
 *  @param mediaContext media context to take clip from
 *  @param startOffset  clip start offset within the media context
 *  @param duration     clip duration
 */
- (void)setClipWithMediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration;


/**
 *  Hint player that scrubbing will begin
 */
-(void)beginScrubbing;

/**
 *  Hint player that scrubbing has ended
 */
-(void)endScrubbing;

/**
 *  Preform scrubbing
 *  Must called after beginScrubbing and before endScrubbing
 *
 *  @param value    value to scrub to
 *  @param minValue min allowed value
 *  @param maxValue max allowed value
 */
-(void)scrubToValue:(double)value minValue:(double)minValue maxValue:(double)maxValue;

/*!
Delegate
 */
@property(nonatomic, weak) id<RVSClipViewDelegate> delegate;

/*!
 Playback state
 */
@property (nonatomic, readonly) RVSClipViewState state;

/*!
 Buffering state
 */
@property (nonatomic, readonly) BOOL isBuffering;


/**
 *  Playing state
 */
@property (nonatomic, readonly) BOOL isPlaying;

/*!
 Toggle fullscreen mode, affects background view, video view and overlay view
 */
@property (nonatomic) BOOL fullscreen;

/**
 *  A customizable view that is displayed behind the movie content
 */
@property (nonatomic, readonly) UIView *backgroundView;

/**
 *  A customizable transparent view that is displayed above the movie content
 */
@property (nonatomic, readonly) UIView *overlayView;

@end
