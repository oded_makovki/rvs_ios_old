//
//  RVSSdk+Service.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"

@class RVSPromise;

/*!
 Notification that will be posted when user was signed in succesfully to the service.
 */
extern NSString *RVSServiceSignedInNotification;

/*!
 Notification that will be posted when user failed to sign in to the service.
 the notification user info will contain the error information
 Does not occur as a result of signout
 */
extern NSString *RVSServiceSignInErrorNotification;

/*!
 Notification that will be posted when the user was signed out from the service.
 This can happen when calling signOut, authentication failure or service decision to sign use out
 Unless the signOut was called, the notification user info will contain the error information
 Can not occur as a result of calling signIn
 */
extern NSString *RVSServiceSignedOutNotification;

/*!
 NSError key in signed out notification user info
 */
extern NSString *RVSServiceSignedOutErrorUserInfoKey;

/*!
 RVS service network status. Network status can change even when signed in
 */
typedef NS_ENUM(NSUInteger, RVSServiceNetworkStatus) {
    
    /*!
     iOS has not network so service is disconnected and cannot perform any network operation
     */
    RVSServiceNetworkStatusNoNetwork,
    
    /*!
     Disconnected from service because user is signed or because of a network problem
     */
    RVSServiceNetworkStatusDisconnected,
    
    /*!
     Trying to connect to the service
     */
    RVSServiceNetworkStatusConnecting,
    
    /*!
     Connected to the service
     */
    RVSServiceNetworkStatusConnected,
};

/*!
 Notification that will be posted when the service network status changes.
 */
extern NSString *RVSServiceNetworkStatusChangedNotification;

/*!
RVS SDK Service category
 */
@interface RVSSdk (Service)

/*!
 YES if signIn must be called before accessing the service
 */
@property (nonatomic, readonly) BOOL isSignedOut;

/*!
 Current service network status
 */
@property (nonatomic, readonly) RVSServiceNetworkStatus networkStatus;

/*!
 My user ID, nil if not singed in
 */
@property (nonatomic, readonly) NSString *myUserId;

/*!
 Sign in to the service using credentials.
 Once signed in the service object itself will handle network disconnections.
 RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
 User credentials will be stored by the service object for automatic sign in when started.
 @param userName user name to use for signing in to the service
 @param password password to use for signing in to the service
 */
- (void)signInWithUserName:(NSString*)userName password:(NSString*)password;

/*!
 Sign in to the service using facebook token.
 Once signed in the service object itself will handle network disconnections.
 RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
 Facebook token will be stored by the service object for automatic sign in when started.
 @param token facebook token to use for signing in to the service
 */
- (void)signInWithFacebookToken:(NSString*)token;

/*!
 Sign in to the service usong facebook token.
 Once signed in the service object itself will handle network disconnections.
 RVSAccountSignedInNotification or RVSAccountSignedOutNotification will be posted once done.
 Twitter token will be stored by the service object for automatic sign in when started.
 @param token twitter token to use for signing in to the service
 @param secret twitter secret to use for signing in to the service
 */
- (void)signInWithTwitterToken:(NSString*)token secret:(NSString *)secret;

/*!
 Sign out from the service and delete credentials
 */
- (void)signOut;

/**
 *  Get a configuration document for the client
 *
 *  @return promise fulfilled with configuration (NSDictionary *)
 */
-(RVSPromise *)configuration;

@end
