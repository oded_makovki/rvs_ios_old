//
//  RVSThumbnail.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSAsyncImage;

/**
 *  Thumbnail from a media context
 */
@protocol RVSThumbnail <RVSDataObject>

/**
 *  Thumbnail ID
 */
@property (nonatomic, readonly) NSString *thumbnailId;

/**
 *  Start offset in seconds relative to the media context
 */
@property (nonatomic, readonly) NSTimeInterval startOffset;

/**
 *  Async image for getting actual images with different resolutions
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *asyncImage;

@end
