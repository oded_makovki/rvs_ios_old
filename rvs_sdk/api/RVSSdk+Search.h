//
//  RVSSdk+Discover.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"

@protocol RVSAsyncList;

extern NSString *RVSSearchTypeProgram;
extern NSString *RVSSearchTypePost;
extern NSString *RVSSearchTypePostOrProgram;

@interface RVSSdk (Discover)

/**
 *  Perform a content search
 *
 *  @param searchString text to search
 *  @param type type of results. use: RVSSearchType
 *  @param minStartTime return only search results with mininimu start time
 *
 *  @return async list with search content result NSArray with RVSContentSearchResult
 */
- (NSObject<RVSAsyncList>*)searchContent:(NSString*)searchString type:(NSString *)type minStartTime:(NSNumber *)minStartTime;

/**
 *  Perform a channel search
 *
 *  @param searchString text to search
 *
 *  @return async list with search content result NSArray with RVSChannelSearchResult
 */
- (NSObject<RVSAsyncList>*)searchChannel:(NSString*)searchString;

/**
 *  Perform a user search
 *
 *  @param searchString text to search
 *
 *  @return async list with search content result NSArray with RVSUserSearchResult
 */
- (NSObject<RVSAsyncList>*)searchUser:(NSString*)searchString;

@end
