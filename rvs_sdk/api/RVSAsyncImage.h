//
//  RVSAsyncImage.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import UIKit;
@class RVSPromise;

/*!
 Asynchronous image
 */
@protocol RVSAsyncImage <NSObject>

/*!
 Get image for given size
 @param size required image size in UI points
 @return promise for UIImage
 */
- (RVSPromise*) imageWithSize:(CGSize)size;

@end
