//
//  RVSSearchResult.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@protocol RVSSearchResult <RVSDataObject>

/**
 *  array of RVSSearchMatch objects
 */
@property (nonatomic, readonly) NSArray *searchMatches;

/**
 *  Score of the result
 */
@property (nonatomic, readonly) NSNumber *score;

@end
