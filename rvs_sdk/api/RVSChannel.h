//
//  RVSChannel.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObject.h"
@protocol RVSAsyncImage;
@protocol RVSProgram;

/*!
 Channel object
 */
@protocol RVSChannel <RVSDataObject>
    
/*!
 Channel ID
 */
@property (nonatomic, readonly) NSString *channelId;

/*!
 Channel display name
 */
@property (nonatomic, readonly) NSString *name;

/*!
 Channel logo
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *logo;

/*!
 Channel watermark logo to display on channel video
 */
@property (nonatomic, readonly) NSObject<RVSAsyncImage> *watermarkLogo;

/*!
 Channel official site URL
 */
@property (nonatomic, readonly) NSURL *officialUrl;

/**
 *  Channel call sign
 */
@property (nonatomic, readonly) NSString *callSign;

/**
 *  Current program if available
 */
@property (nonatomic, readonly) NSObject <RVSProgram> *currentProgram;

@end
