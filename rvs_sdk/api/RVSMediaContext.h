//
//  RVSMediaContext.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObject.h"

@class RVSPromise;
@protocol RVSAsyncImage;

/**
 *  Clip media types
 */
extern NSString *RVSClipMediaTypeHLS;

/**
 *  Asynchronous media context
 */
@protocol RVSMediaContext <RVSDataObject>

/**
 *  Get partial clip URL for playback using native player
 *
    @param mediaType mediaType for the clip, nil for default media type
 *  @param startOffset start offset in seconds relative to media context start
 *  @param duration clip duration
 *
 *  @return clip URL
 */
- (NSURL*)clipUrlWithMediaType:(NSString *)mediaType startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration;

/**
 *  Get thumbnails for part of the media context
 *
 *  @param startOffset start offset in seconds relative to media context start
 *  @param duration duration of the media context part
 *
 *  @return promise for NSArray with RVSThumbnail objects
 */
- (RVSPromise*)thumbnailsWithStartOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration;

/**
 *  Start time in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *start;

/**
 *  End time in milliseconds since 1970
 */
@property (nonatomic, readonly) NSNumber *end;

/**
 *  Media context duration
 */
@property (nonatomic, readonly) NSTimeInterval duration;

@end
