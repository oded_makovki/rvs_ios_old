//
//  RVSUserImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSUser.h"
#import "RVSSyncedDataObject.h"

@interface RVSUser : RVSSyncedDataObject <RVSUser>

@property (nonatomic) NSNumber<Optional> *isFollowedByMe;
@property (nonatomic) NSNumber<Optional> *followerCount;
@property (nonatomic) NSNumber<Optional> *followingCount;
@property (nonatomic) NSNumber<Optional> *postCount;
@property (nonatomic) NSNumber<Optional> *likeCount;
@property (nonatomic) NSNumber<Optional> *repostCount;

@end
    