//
//  RVSGetUserList.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSGetUserList.h"
#import "RVSReferringUserList.h"
#import "RVSLogger.h"
#import "RVSReferringUser.h"

@interface RVSGetUserList ()
@property (nonatomic) NSMutableArray *responseBuffer;
@end

@implementation RVSGetUserList

- (id)initWithMethod:(NSString*)method
{
    self = [super initWithMethod:method params:nil responseClass:([RVSReferringUserList class]) arrayPropertyName:@"referringUsers"];
     return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    NSMutableArray *users = [[NSMutableArray alloc] initWithCapacity:responseArray.count];
    for (RVSReferringUser *item in responseArray)
    {
        RVSUser *user = item.user;
        [users addObject:user];
    }
    
    [self.responseBuffer addObjectsFromArray:users];
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
}

@end

