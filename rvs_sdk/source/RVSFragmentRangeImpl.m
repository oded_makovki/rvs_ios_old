//
//  RVSFragmentRangeImpl.m
//  rvs_sdk
//
//  Created by Barak Harel on 5/11/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSFragmentRangeImpl.h"

@interface RVSFragmentRange ()

@property (nonatomic) NSNumber<Optional> *matchCharacterOffset;
@property (nonatomic) NSNumber<Optional> *matchCharacterCount;

@end

@implementation RVSFragmentRange

JSON_MODEL_KEY_MAPPER(RVSFragmentRange)

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"matchCharacterOffset"   :@"matchCharacterOffset",
             @"matchCharacterCount"    :@"matchCharacterCount"
             };
}

@end

