//
//  RVSGetCommentList.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetPagingListBase.h"

@interface RVSGetCommentList : RVSGetPagingListBase

- (id)initWithMethod:(NSString*)method;

@end
