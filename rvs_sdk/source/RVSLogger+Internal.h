//
//  RVSLogger+Internal.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/12/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSLogger.h"

@interface RVSLogger : NSObject

+ (NSArray*)logLevelNames;
+ (void)saveConfig;
+ (NSString*)logLevelNameFroClassName:(NSString*)className;
+ (int)logLevelFromString:(NSString*)logLevel;
+ (void)setAllLogLevels:(NSString*)logLevel;

@end
