//
//  RVSClipView.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSClipView.h"
#import "RVSMediaContext.h"
#import "RVSLogger.h"
#import "RVSNotificationObserver.h"
#import "RVSKeyPathObserver.h"
#import "RVSSdk+Internal.h"
#import "RVSPromise.h"

@interface RVSClipViewVideoView : UIView

@end

@interface RVSClipViewFullscreenController : UIViewController

- (id)initWithClipView:(RVSClipView*)clipView;

@property (nonatomic) RVSClipView *clipView;

@end

@interface RVSClipView ()

@property (nonatomic) AVPlayer *avPlayer;
@property (nonatomic) RVSClipViewState state;
@property (nonatomic) BOOL isBuffering;
@property (nonatomic) NSURL *contentUrl;
@property (nonatomic) BOOL notifiedPlaybackStart;
@property (nonatomic) RVSKeyPathObserver *playerStatusObserver;
@property (nonatomic) id playerFineTimeObserver;
@property (nonatomic) id playerProgressTimeObserver;
@property (nonatomic) RVSNotificationObserver *playbackEndObserver;
@property (nonatomic) RVSKeyPathObserver *playbackKeepUpObserver;
@property (nonatomic) RVSKeyPathObserver *playbackBufferEmptyObserver;
@property (nonatomic) RVSKeyPathObserver *playbackRateObserver;
@property (nonatomic) UIView *backgroundView;
@property (nonatomic) RVSClipViewVideoView *videoView;
@property (nonatomic) UIView *overlayView;
@property (nonatomic) RVSClipViewFullscreenController *fullscreenController;
@property (nonatomic) NSObject<RVSMediaContext>* mediaContext;
@property (nonatomic) NSTimeInterval startOffset;
@property (nonatomic) NSTimeInterval duration;

@property (nonatomic) NSArray *backgroundViewSuperViewConstraints;
@property (nonatomic) NSArray *videoViewSuperViewConstraints;
@property (nonatomic) NSArray *overlayViewSuperViewConstraints;

@end

@implementation RVSClipView

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithFrame:(CGRect)frame
{
    RVSVerifyMainThread();
    
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSClipView];
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initRVSClipView];
    }
    
    return self;
}

-(void)initRVSClipView
{
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _videoView = [[RVSClipViewVideoView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    
    [_backgroundView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_videoView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_overlayView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [self addSubview:_backgroundView];
    [self addSubview:_videoView];
    [self addSubview:_overlayView];
    
    [self subViewsDidMoveToNewParent];
    
    _state = RVSClipViewStateUnknown;
}

- (void)dealloc
{
    [self stop];
    
    if (_playerProgressTimeObserver)
        [_avPlayer removeTimeObserver:_playerProgressTimeObserver];
    
    if (_playerFineTimeObserver)
        [_avPlayer removeTimeObserver:_playerFineTimeObserver];
}

- (void)setClipWithMediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration
{
    RVSVerifyMainThread();
    RVSParameterAssert(mediaContext);
    
    [self stop];
    _mediaContext = mediaContext;
    _startOffset = startOffset;
    _duration = duration;
    
    self.contentUrl = nil;
    self.notifiedPlaybackStart = NO;
}


#pragma mark - Playback State

- (BOOL)isPlaying
{
    return (self.state == RVSClipViewStatePlaying || self.state == RVSClipViewStateLoading);
}

- (void)startMoviePlayback
{
    self.notifiedPlaybackStart = YES;
    [[RVSSdk sharedSdk] informClipPlayback:YES];

    __weak RVSClipView *weakSelf = self;
    
    if (!self.avPlayer)
    {
        // create player first time
        RVSInfo(@"Create AVPlayer with url: %@", self.contentUrl);
        
        self.avPlayer = [[AVPlayer alloc] initWithURL:self.contentUrl];
        self.avPlayer.closedCaptionDisplayEnabled = NO;
        
        AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self.videoView layer];
        [playerLayer setPlayer:self.avPlayer];
        playerLayer.videoGravity = self.fullscreen ? AVLayerVideoGravityResizeAspect : AVLayerVideoGravityResizeAspect;

        self.playerStatusObserver = [self.avPlayer newObserverForKeyPath:@"status" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
            
            if (weakSelf.avPlayer.status == AVPlayerStatusFailed)
                weakSelf.state = RVSClipViewStateUnknown;
        }];
        
        self.playbackRateObserver = [self.avPlayer newObserverForKeyPath:@"rate" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
            if (weakSelf.avPlayer.rate > 0 && weakSelf.avPlayer.currentItem.playbackLikelyToKeepUp)
            {
                if (weakSelf.state == RVSClipViewStateLoading || weakSelf.state == RVSClipViewStatePaused)
                {
                    weakSelf.state = RVSClipViewStatePlaying;
                }
            }
        }];
        
        self.playerFineTimeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(0.05, NSEC_PER_SEC) queue:NULL usingBlock:^(CMTime time) {
            
            NSTimeInterval timeSeconds = CMTimeGetSeconds(time);
            
            if (timeSeconds <= 1.0)
            {
                weakSelf.avPlayer.volume = (exp(timeSeconds * 2) - 1.0) / (exp(2) - 1);
                //printf("%f\n", weakSelf.avPlayer.volume);
            }
            else
            {
                NSTimeInterval duration = CMTimeGetSeconds(weakSelf.avPlayer.currentItem.duration);
                NSTimeInterval diff = duration - timeSeconds;
                
                if (diff <0)
                    diff = 0;
                
                if (diff <= 0.5)
                    weakSelf.avPlayer.volume = diff * 2;
                else
                    weakSelf.avPlayer.volume = 1.0;
            }
        }];
    }

    // re-set player item observers every time there is a new url
    self.playbackKeepUpObserver = [self.avPlayer.currentItem newObserverForKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
        
        if (weakSelf.avPlayer.rate > 0 && weakSelf.avPlayer.currentItem.playbackLikelyToKeepUp)
        {
            if (weakSelf.state == RVSClipViewStateLoading || weakSelf.state == RVSClipViewStatePaused)
                weakSelf.state = RVSClipViewStatePlaying;
        }
        
        if (weakSelf.avPlayer.currentItem.playbackLikelyToKeepUp)
        {
            weakSelf.isBuffering = NO;
        }
    }];
    
    self.playbackBufferEmptyObserver = [self.avPlayer.currentItem newObserverForKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
        
        if (weakSelf.avPlayer.currentItem.playbackBufferEmpty)
        {
            weakSelf.isBuffering = YES;
        }
    }];
    
    self.playbackEndObserver = [RVSNotificationObserver newObserverForNotificationWithName:AVPlayerItemDidPlayToEndTimeNotification object:self.avPlayer.currentItem usingBlock:^(NSNotification *notification) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.state = RVSClipViewStatePlaybackEnded;
        });
    }];
    
    // so we can fade in volume
    self.avPlayer.volume = 0.0;
    
    AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self.videoView layer];
    [playerLayer setPlayer:self.avPlayer];
    
    self.avPlayer.closedCaptionDisplayEnabled = NO;
    [self.avPlayer play];
}

-(void)rewind
{
    if (self.avPlayer.currentItem)
    {
        [self.avPlayer.currentItem seekToTime:kCMTimeZero];
    }
    
    [self reportProgress];
}

- (void)play
{
    RVSVerifyMainThread();
    RVSAssert(self.mediaContext);
    
    if (self.state == RVSClipViewStatePlaybackEnded)
        [self rewind];
    
    self.state = RVSClipViewStateLoading;
    self.contentUrl = [self.mediaContext clipUrlWithMediaType:nil startOffset:self.startOffset duration:self.duration];
    [self startMoviePlayback];
}

- (void)pause
{
    RVSVerifyMainThread();
   
    if (_avPlayer)
        [self.avPlayer pause];
    
    self.state = RVSClipViewStatePaused;
}

- (void)stop
{
    RVSVerifyMainThread();
    
    if (_avPlayer)
    {
        [self.avPlayer pause];
        
        AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self.videoView layer];
        [playerLayer setPlayer:nil];
        self.avPlayer = nil;
    }
    
    self.state = RVSClipViewStateUnknown;
    [self reportProgress];
}

-(void)setIsBuffering:(BOOL)isBuffering
{
    _isBuffering = isBuffering;
    
    if ([self.delegate respondsToSelector:@selector(clipView:isBuffering:)])
    {
        [self.delegate clipView:self isBuffering:isBuffering];
    }
}

- (void)setState:(RVSClipViewState)state
{
    RVSVerifyMainThread();
    
    if (state == RVSClipViewStateUnknown || state == RVSClipViewStatePlaybackEnded)
    {
        if (self.notifiedPlaybackStart)
        {
            self.notifiedPlaybackStart = NO;
            [[RVSSdk sharedSdk] informClipPlayback:NO];
        }
    }
    
    if (state == RVSClipViewStatePlaying)
    {
        [self addProgressTimeObserver];
    }
    else
    {
        [self removeProgressTimeObserver];
    }
        
    if (state == _state)
        return;
    
    _state = state;
    
    [self.delegate clipViewStateDidChange:self];
}

#pragma mark - Time Progress

- (CMTime)playerItemDuration
{
    AVPlayerItem *playerItem = [self.avPlayer currentItem];
    if (playerItem.status == AVPlayerItemStatusReadyToPlay)
    {
        return ([playerItem duration]);
    }
    return kCMTimeInvalid;
}

- (void)addProgressTimeObserver
{
    __weak typeof(self) weakSelf = self;
    
    self.playerProgressTimeObserver = [self.avPlayer addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(0.5, NSEC_PER_SEC) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        [weakSelf reportProgress];
    }];
}

- (void)removeProgressTimeObserver
{
    if (_playerProgressTimeObserver)
    {
        [self.avPlayer removeTimeObserver:_playerProgressTimeObserver];
        _playerProgressTimeObserver = nil;
    }
}

-(void)reportProgress
{
    NSTimeInterval duration = self.duration;
    NSTimeInterval current = 0;
    
    if (self.avPlayer && self.state != RVSClipViewStatePlaybackEnded)
    {
        CMTime durationTime = [self playerItemDuration];
        
        if (!CMTIME_IS_INVALID(durationTime)) //use real values
        {
            duration = CMTimeGetSeconds(durationTime);
            
            CMTime currentTime = [self.avPlayer currentTime];
            current = CMTimeGetSeconds(currentTime);
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(clipView:didProgressWithCurrent:duration:)])
    {
        [self.delegate clipView:self didProgressWithCurrent:current duration:duration];
    }
}



#pragma mark - Scrub

-(void)beginScrubbing
{
    if (self.state == RVSClipViewStateUnknown)
    {
        [self play];
    }
    
    [self pause];
}

-(void)endScrubbing
{
    [self play];
}

-(void)scrubToValue:(double)value minValue:(double)minValue maxValue:(double)maxValue
{
    double time = self.duration * (value - minValue) / (maxValue - minValue);
    [self.avPlayer seekToTime:CMTimeMakeWithSeconds(time, NSEC_PER_SEC)];
}

#pragma mark - Display State

- (BOOL)fullscreen
{
    return self.fullscreenController != nil;
}

- (void)setFullscreen:(BOOL)fullscreen
{
    if (fullscreen == self.fullscreen)
        return;
    
    //prevent change while in transition
    if (self.fullscreenController.isBeingPresented || self.fullscreenController.isBeingDismissed)
        return;
    
    if (fullscreen)
    {
        self.fullscreenController = [[RVSClipViewFullscreenController alloc] initWithClipView:self];
        self.fullscreenController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        [self.window.rootViewController presentViewController:self.fullscreenController animated:YES completion:nil];
    }
    else
    {
        [self.fullscreenController dismissViewControllerAnimated:NO completion:nil];
        self.fullscreenController = nil;
    }
}

-(void)subViewsWillMoveToNewParent
{
    if (self.backgroundViewSuperViewConstraints)
        [self.backgroundView.superview removeConstraints:self.backgroundViewSuperViewConstraints];
    
    if (self.videoViewSuperViewConstraints)
        [self.videoView.superview removeConstraints:self.videoViewSuperViewConstraints];
    
    if (self.overlayViewSuperViewConstraints)
        [self.overlayView.superview removeConstraints:self.overlayViewSuperViewConstraints];
}

-(void)subViewsDidMoveToNewParent
{
    if (self.backgroundView)
    {
        NSMutableArray *backgroundViewSuperViewConstraints = [NSMutableArray array];
        [backgroundViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": self.backgroundView}]];
        [backgroundViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": self.backgroundView}]];
        
        self.backgroundViewSuperViewConstraints = backgroundViewSuperViewConstraints;
        [self.backgroundView.superview addConstraints:self.backgroundViewSuperViewConstraints];
    }
    
    if (self.videoView)
    {
        NSMutableArray *videoViewSuperViewConstraints = [NSMutableArray array];
        [videoViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": self.videoView}]];
        [videoViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": self.videoView}]];
        
        self.videoViewSuperViewConstraints = videoViewSuperViewConstraints;
        [self.videoView.superview addConstraints:self.videoViewSuperViewConstraints];
    }
    
    if (self.overlayView)
    {
        NSMutableArray *overlayViewSuperViewConstraints = [NSMutableArray array];
        [overlayViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": self.overlayView}]];
        [overlayViewSuperViewConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": self.overlayView}]];
        
        self.overlayViewSuperViewConstraints = overlayViewSuperViewConstraints;
        [self.overlayView.superview addConstraints:self.overlayViewSuperViewConstraints];
    }
}

-(void)setHidden:(BOOL)hidden
{
    [super setHidden:hidden];
    
    self.videoView.hidden = hidden;
}

#pragma mark - Logs

-(AVPlayerItemAccessLog *)playerAccessLog
{
    return self.avPlayer.currentItem.accessLog;
}

-(AVPlayerItemErrorLog *)playerErrorLog
{
    return self.avPlayer.currentItem.errorLog;
}

@end

#pragma mark - Video View

@implementation RVSClipViewVideoView

+ (Class)layerClass
{
	return [AVPlayerLayer class];
}

@end

@implementation RVSClipViewFullscreenController

- (id)initWithClipView:(RVSClipView*)clipView;
{
    self = [super init];
    self.clipView = clipView;
    
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    
    [self.clipView subViewsWillMoveToNewParent];
    [self.view addSubview:self.clipView.backgroundView];
    [self.view addSubview:self.clipView.videoView];
    [self.view addSubview:self.clipView.overlayView];
    [self.clipView subViewsDidMoveToNewParent];
    
    [self.clipView.backgroundView layoutIfNeeded];
    
    AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self.clipView.videoView layer];
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    [self.clipView subViewsWillMoveToNewParent];
    [self.clipView addSubview:_clipView.backgroundView];
    [self.clipView addSubview:_clipView.videoView];
    [self.clipView addSubview:_clipView.overlayView];
    [self.clipView subViewsDidMoveToNewParent];
    
    AVPlayerLayer *playerLayer = (AVPlayerLayer*)[self.clipView.videoView layer];
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

-(BOOL)prefersStatusBarHidden
{
    return YES;
}

@end
