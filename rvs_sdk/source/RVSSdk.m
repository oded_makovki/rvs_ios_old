//
//  RVSSdk.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/20/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk+Internal.h"
#import "RVSServiceManager.h"
#import "RVSRequestManager.h"
#import "RVSCreatePost.h"
#import "RVSCreateComment.h"
#import "RVSGetPostList.h"
#import "RVSGetUserList.h"
#import "RVSGnChannelDetector.h"
#import "RVSLogger+Internal.h"
#import "RVSUserImpl.h"
#import "RVSPromise.h"
#import "RVSProgramImpl.h"
#import "RVSGetPostList.h"
#import "RVSGetCommentList.h"
#import "RVSGetSearchContent.h"
#import "RVSGetSearchChannel.h"
#import "RVSGetSearchUser.h"
#import "RVSGetMediaContext.h"
#import "RVSChannelImpl.h"
#import "RVSSharingUrl.h"
#import "RVSPostImpl.h"
#import "RVSCommentImpl.h"
#import "RVSChannelSearchResult.h"

NSString *RVSErrorDomain = @"com.rayv.rvssdk";
NSString *RVSSdkChannelListChangedNotfication = @"RVSSdkChannelListChangedNotfication";
NSString *RVSSdkClipPlaybackStartingNotification = @"RVSSdkClipPlaybackStartingNotification";
NSString *RVSSdkClipPlaybackEndedNotification = @"RVSSdkClipPlaybackEndedNotification";

NSString *RVSSearchTypeProgram = @"PROGRAM";
NSString *RVSSearchTypePost = @"POST";
NSString *RVSSearchTypePostOrProgram = @"POST_OR_PROGRAM";

@interface RVSSdk ()

@property (nonatomic) RVSLogger *logger;
@property (nonatomic) NSOperationQueue *backgroundOperationQueue;
@property (nonatomic) RVSGnChannelDetector *channelDetector;
@property (nonatomic) NSUInteger playingClipCount;
@property (nonatomic) RVSServiceManager *serviceManager;
@property (nonatomic) RVSRequestManager *requestManager;

@end

@implementation RVSSdk

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)init
{
    RVSVerifyMainThread();
    
    self = [super init];
    
    _logger = [[RVSLogger alloc] init];
    
    DDLogInfo(@"************* RVS SDK started ****************");
    
    _requestManager = [[RVSRequestManager alloc] init];
    _serviceManager = [[RVSServiceManager alloc] initWithDomain:_requestManager.baseURL.host];
    
    return self;
}

+ (RVSSdk*)sharedSdk
{
    static RVSSdk* _rvsSdk;
    static dispatch_once_t playkitOnce;
    
    dispatch_once(&playkitOnce, ^{
        _rvsSdk = [[RVSSdk alloc] init];
    });
    
    return  _rvsSdk;
}

- (void)addBackgroundOperation:(NSOperation *)operation
{
    RVSVerifyMainThread();
    RVSParameterAssert(operation);
    
    static dispatch_once_t rvsSdkOperationQueueOnce;
    dispatch_once(&rvsSdkOperationQueueOnce, ^{
        
        self.backgroundOperationQueue = [[NSOperationQueue alloc] init];
        self.backgroundOperationQueue.name = @"com.rayv.RVSSdk";
    });
    
    return [self.backgroundOperationQueue addOperation:operation];
}

- (void)informClipPlayback:(BOOL)playing
{
    if (playing)
        self.playingClipCount++;
    else
        self.playingClipCount--;
    
    if (playing && self.playingClipCount == 1)
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSSdkClipPlaybackStartingNotification object:self];
    else if (!playing && self.playingClipCount == 0)
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSSdkClipPlaybackEndedNotification object:self];
}

- (RVSPromise*)channelForCallSign:(NSString*)callSign
{
    RVSVerifyMainThread();
    RVSParameterAssert(callSign);
    
    NSObject<RVSAsyncList> *list = [[RVSGetSearchChannel alloc] initWithSearchCallSign:callSign];
    
    RVSPromise *promise = [[RVSPromise alloc] init];
    __weak RVSPromise *weakPromise = promise;
    
    [[list next:1] thenOnMain:^(id result) {
        NSArray *searchResults = result;
        if (!searchResults.count)
            [weakPromise rejectWithReason:nil];
        else
        {
            NSObject<RVSChannelSearchResult> *searchResult = searchResults[0];
            [weakPromise fulfillWithValue:searchResult.channel];
        }
        
    } error:^(NSError *error) {
        [weakPromise rejectWithReason:error];
    }];
    
    return promise;
}

- (RVSPromise*)currentProgramForChannelId:(NSString*)channelId
{
    RVSVerifyMainThread();
    
    NSString *method = [NSString stringWithFormat:@"channels/%@/currentProgram", channelId];
    return [self.requestManager getJsonObject:method parameters:nil objectClass:[RVSProgram class]];
}

#pragma mark Channels

- (void)enableChannelDetector:(BOOL)enable
{
    if (enable)
    {
        if (self.channelDetector)
            return;
        
        self.channelDetector = [[RVSGnChannelDetector alloc] init];
    }
    else
    {
        if (!self.channelDetector)
            return;
        
        self.channelDetector = nil;
    }
}

- (RVSPromise*)mediaContextForChannel:(NSString*)channelId
{
    RVSVerifyMainThread();
    RVSParameterAssert(channelId);
    
    return [[RVSGetMediaContext alloc] initWithChannel:channelId].getMediaContext;
}

#pragma mark Service

- (BOOL)isSignedOut
{
    RVSVerifyMainThread();
    
    return self.serviceManager.isSignedOut;
}

- (RVSServiceNetworkStatus)networkStatus
{
    RVSVerifyMainThread();
    
    return self.serviceManager.networkStatus;
}

- (NSString*)myUserId
{
    RVSVerifyMainThread();
    
    return self.serviceManager.userId;
}

- (void)signInWithUserName:(NSString*)userName password:(NSString*)password
{
    RVSVerifyMainThread();
    RVSParameterAssert(userName);
    RVSParameterAssert(password);
    
    [self.serviceManager signInWithUserName:userName password:password];
}

- (void)signInWithFacebookToken:(NSString*)token
{
    RVSVerifyMainThread();
    RVSParameterAssert(token);
    
    [self.serviceManager signInWithFacebookToken:token];
}

- (void)signInWithTwitterToken:(NSString*)token secret:(NSString *)secret
{
    RVSVerifyMainThread();
    RVSParameterAssert(token);
    RVSParameterAssert(secret);
    
    [self.serviceManager signInWithTwitterToken:token secret:secret];
}

- (void)signOut
{
    RVSVerifyMainThread();
    RVSAssert(!self.isSignedOut);
    
    [self.serviceManager signOut:nil];
}

- (RVSPromise *)configuration
{
    RVSVerifyMainThread();
    
    return [self.serviceManager configuration];
}

#pragma mark Users

- (RVSPromise*)userForUserId:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@", userId];
    return [self.requestManager getJsonObject:method parameters:nil objectClass:[RVSUser class]];
}

- (NSObject<RVSAsyncList>*)userFeedForUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/userFeed", userId];
    return [[RVSGetPostList alloc] initWithMethod:method filterResponse:YES];
}

- (NSObject<RVSAsyncList>*)newsFeedForUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/newsFeed", userId];
    return [[RVSGetPostList alloc] initWithMethod:method filterResponse:YES];
}

- (NSObject<RVSAsyncList>*)followersOfUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/followers", userId];
    return [[RVSGetUserList alloc] initWithMethod:method];
}

- (NSObject<RVSAsyncList>*)followedByUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/followings", userId];
    return [[RVSGetUserList alloc] initWithMethod:method];
}

- (NSObject<RVSAsyncList>*)likedPostsByUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/liked", userId];
    return [[RVSGetPostList alloc] initWithMethod:method filterResponse:YES];
}

- (RVSPromise*)followUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    RVSAssert(self.myUserId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/followings/%@", self.myUserId, userId];
    RVSPromise *promise = [self.requestManager PUT:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSUser notifyChangedDataObjectWithId:userId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.isFollowedByMe = [NSNumber numberWithBool:YES];
            user.followerCount = [NSNumber numberWithUnsignedInteger:user.followerCount.unsignedIntegerValue + 1];
        }];
        [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.followingCount = [NSNumber numberWithUnsignedInteger:user.followingCount.unsignedIntegerValue + 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)unfollowUser:(NSString*)userId
{
    RVSVerifyMainThread();
    RVSParameterAssert(userId);
    
    NSString *method = [NSString stringWithFormat:@"users/%@/followings/%@", self.myUserId, userId];
    RVSPromise *promise = [self.requestManager DELETE:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSUser notifyChangedDataObjectWithId:userId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.isFollowedByMe = [NSNumber numberWithBool:NO];
            user.followerCount = [NSNumber numberWithUnsignedInteger:user.followerCount.unsignedIntegerValue - 1];
        }];
        [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.followingCount = [NSNumber numberWithUnsignedInteger:user.followingCount.unsignedIntegerValue - 1];
        }];
    } error:nil];
    return promise;
}

#pragma mark Posts

- (RVSPromise*)postForPostId:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@", postId];
    return [self.requestManager getJsonObject:method parameters:nil objectClass:[RVSPost class]];
}

- (NSObject<RVSAsyncList>*)trendingPosts
{
    return [[RVSGetPostList alloc] initWithMethod:@"posts/trending" filterResponse:YES];
}

- (RVSPromise*)sharingUrlForPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/sharingUrl", postId];
    return [self.requestManager getJsonObjectProperty:method parameters:nil objectClass:[RVSSharingUrl class] propertyName:@"url"];
}

- (NSObject<RVSAsyncList>*)commentsForPost:(NSString*)postId
{
    NSString *method = [NSString stringWithFormat:@"posts/%@/comments", postId];
    return [[RVSGetCommentList alloc] initWithMethod:method];
}

- (NSObject<RVSAsyncList>*)repostingUsersForPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/repostUsers", postId];
    return [[RVSGetUserList alloc] initWithMethod:method];
}

- (NSObject<RVSAsyncList>*)likingUsersForPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/likeUsers", postId];
    return [[RVSGetUserList alloc] initWithMethod:method];
}


- (RVSPromise*)createLikeForPost:(NSString*)postId;
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    RVSAssert(self.myUserId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/likeUsers/%@", postId, self.myUserId];
    RVSPromise *promise = [_requestManager PUT:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSPost *post = (RVSPost*)obj;
            post.likedByCaller = [NSNumber numberWithBool:YES];
            post.likeCount = [NSNumber numberWithUnsignedInteger:post.likeCount.unsignedIntegerValue + 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)deleteLikeForPost:(NSString*)postId;
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    RVSAssert(self.myUserId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/likeUsers/%@", postId, self.myUserId];
    RVSPromise *promise = [_requestManager DELETE:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSPost *post = (RVSPost*)obj;
            post.likedByCaller = [NSNumber numberWithBool:NO];
            post.likeCount = [NSNumber numberWithUnsignedInteger:post.likeCount.unsignedIntegerValue - 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)createRepostForPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    RVSAssert(self.myUserId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/repostUsers/%@", postId, self.myUserId];
    RVSPromise *promise =  [_requestManager PUT:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSPost *post = (RVSPost*)obj;
            post.repostedByCaller = [NSNumber numberWithBool:YES];
            post.repostCount = [NSNumber numberWithUnsignedInteger:post.repostCount.unsignedIntegerValue + 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)deleteRepostForPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    RVSAssert(self.myUserId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/repostUsers/%@", postId, self.myUserId];
    RVSPromise *promise =  [_requestManager DELETE:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSPost *post = (RVSPost*)obj;
            post.repostedByCaller = [NSNumber numberWithBool:NO];
            post.repostCount = [NSNumber numberWithUnsignedInteger:post.repostCount.unsignedIntegerValue - 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)createPostWithText:(NSString*)text mediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString*)coverImageId
{
    RVSVerifyMainThread();
    RVSParameterAssert(text);
    RVSParameterAssert(mediaContext);
    RVSParameterAssert(coverImageId);
    RVSAssert(self.myUserId);
    
    RVSCreatePost *createPost = [[RVSCreatePost alloc] initWithText:text mediaContext:mediaContext startOffset:startOffset duration:duration coverImageId:coverImageId];
    RVSPromise *promise = [createPost createPost];
    [promise thenOnMain:^(id result) {
        [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.postCount = [NSNumber numberWithUnsignedInteger:user.postCount.unsignedIntegerValue + 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)deletePost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@", postId];
    RVSPromise *promise = [self.requestManager DELETE:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSUser notifyChangedDataObjectWithId:self.myUserId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSUser *user = (RVSUser*)obj;
            user.postCount = [NSNumber numberWithUnsignedInteger:user.postCount.unsignedIntegerValue - 1];
        }];
    } error:nil];
    return promise;
}

- (RVSPromise*)createCommentWithMessage:(NSString*)message forPostId:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(message);
    RVSParameterAssert(postId);
    
    RVSCreateComment *createComment = [[RVSCreateComment alloc] initWithMessage:message postId:postId];
    RVSPromise *promise = [createComment createComment];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            RVSPost *post = (RVSPost*)obj;
            post.commentCount = [NSNumber numberWithUnsignedInteger:post.commentCount.unsignedIntegerValue + 1];

            if (post.latestComments) {
                [post.latestComments insertObject:result atIndex:0];
            } else {
                post.latestComments = (NSMutableArray <RVSComment, Optional> *)[NSMutableArray arrayWithObject:result];
            }
        }];
    } error:nil];

    return promise;
}

- (RVSPromise*)deleteComment:(NSString *)commentId fromPost:(NSString*)postId
{
    RVSVerifyMainThread();
    RVSParameterAssert(commentId);
    RVSParameterAssert(postId);
    
    NSString *method = [NSString stringWithFormat:@"posts/%@/comments/%@", postId, commentId];
    RVSPromise *promise = [self.requestManager DELETE:method parameters:nil];
    [promise thenOnMain:^(id result) {
        [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
            [RVSPost notifyChangedDataObjectWithId:postId syncBlock:^(RVSSyncedDataObject *obj) {
                RVSPost *post = (RVSPost*)obj;
                post.commentCount = [NSNumber numberWithUnsignedInteger:post.commentCount.unsignedIntegerValue - 1];
                [post.latestComments enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                    if ([commentId isEqualToString:((RVSComment*)obj).commentId])
                    {
                        [post.latestComments removeObjectAtIndex:idx];
                        *stop = YES;
                    }
                }];
            }];
        }];
    } error:nil];
    return promise;
}

#pragma mark search

- (NSObject<RVSAsyncList>*)searchContent:(NSString*)searchString type:(NSString *)type minStartTime:(NSNumber *)minStartTime
{
    RVSVerifyMainThread();
    RVSParameterAssert(searchString);
    
    return [[RVSGetSearchContent alloc] initWithSearchString:searchString type:type minStartTime:minStartTime];
}

- (NSObject<RVSAsyncList>*)searchChannel:(NSString*)searchString
{
    RVSVerifyMainThread();
    RVSParameterAssert(searchString);
    
    return [[RVSGetSearchChannel alloc] initWithSearchString:searchString];
}

- (NSObject<RVSAsyncList>*)searchUser:(NSString*)searchString
{
    RVSVerifyMainThread();
    RVSParameterAssert(searchString);
    
    return [[RVSGetSearchUser alloc] initWithSearchString:searchString];
}

@end
