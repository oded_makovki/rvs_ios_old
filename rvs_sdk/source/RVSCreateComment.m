//
//  RVSCreateComment.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSCreateComment.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSLogger.h"
#import "RVSCommentImpl.h"


@interface RVSCreateComment()

@property (nonatomic) NSString *postId;
@property (nonatomic) NSString *message;

@property (nonatomic) NSURLSessionDataTask *createCommentTask;

@end

@implementation RVSCreateComment

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithMessage:(NSString *)message postId:(NSString *)postId
{
    RVSParameterAssert(postId);
    RVSParameterAssert(message);
    
    self = [super init];
    if (self)
    {
        self.message = message;
        self.postId = postId;
    }
    return self;
}

- (void)dealloc
{
    [_createCommentTask cancel];
}

- (RVSPromise*)createComment
{
    RVSVerifyMainThread();
    
    NSMutableDictionary *postInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys: self.message, @"message", nil];

    RVSInfo(@"posting a post with info: %@", postInfo);
    
    RVSPromise *promise = [[RVSPromise alloc] init];
    __weak RVSPromise *weakPromise = promise;
    
    //POST /posts/{postId}/comments
    NSString *method = [NSString stringWithFormat:@"posts/%@/comments", self.postId];
    
    self.createCommentTask = [[RVSSdk sharedSdk].requestManager POST:method parameters:postInfo
                                                          success:^(NSURLSessionDataTask *task, id responseObject)
                           {
                               NSError *error;
                               id json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                         options:kNilOptions
                                                                           error:&error];
                               if (error)
                               {
                                   RVSError(@"failed to deserialize json error %@", error);
                                   [weakPromise rejectWithReason:error];
                               }
                               
                               RVSComment *createdComment = [[RVSComment alloc] initWithDictionary:json error:&error];
                               
                               if (error)
                               {
                                   RVSError(@"failed to parse created comment with error %@", error);
                                   [weakPromise rejectWithReason:error];
                               }
                               else
                               {
                                   RVSInfo(@"created comment %@", createdComment);
                                   [weakPromise fulfillWithValue:createdComment];
                               }
                           }
                                                          failure:^(NSURLSessionDataTask *task, NSError *error)
                           {
                               RVSError(@"failed to create comment with error %@", error);
                               [weakPromise rejectWithReason:error];
                           }];
    
    // keep a reference to self so it won't be destyoed before the promise
    promise.then(nil, ^NSError*(NSError* error) {
        if (weakPromise.isCancelled)
            [self.createCommentTask cancel];
        return nil;
    });
    
    return promise;
}


@end
