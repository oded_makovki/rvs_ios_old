//
//  RVSCoverImage.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSCoverImage.h"
#import "RVSImage.h"

@interface RVSCoverImage ()

@property (nonatomic) NSArray<RVSImage, Optional> *images;

@end

@implementation RVSCoverImage

JSON_MODEL_KEY_MAPPER(RVSImage)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"representations": @"images"};
}

@end
