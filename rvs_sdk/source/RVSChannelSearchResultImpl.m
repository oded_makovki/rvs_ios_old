//
//  RVSChannelSearchResult.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSChannelSearchResultImpl.h"
#import "RVSChannelImpl.h"
#import "RVSSearchMatchImpl.h"

@interface RVSChannelSearchResult ()

@property (nonatomic) RVSChannel <RVSChannel, Optional> *channel;
@property (nonatomic) NSArray <RVSSearchMatch, Optional> *searchMatches;
@property (nonatomic) NSNumber <Optional> *score;

@end

@implementation RVSChannelSearchResult

JSON_MODEL_KEY_MAPPER(RVSChannelSearchResult)

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"channel"         :@"channel",
             @"searchMatches"   :@"searchMatches",
             @"score"           :@"score"
             };
}


@end
