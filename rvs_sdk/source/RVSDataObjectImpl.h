//
//  RVSDataObjectImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObject.h"
#import <JSONModel/JSONModel.h>

@protocol RVSJsonDataObject <RVSDataObject>

// return dictionary with keys as json names, values as property names, compatible with JsonModel mapping dictionary. Can be nil
+ (NSDictionary*)jsonMappedProperties;

// return not mapped properties (for use by rvsSdkProperties method). Can be nill
+ (NSArray*)notMappedProperties;

// return a JsonModel key mapper initialized with the jsonMapping dictionary
// implemented with the JSON_MODEL_KEY_MAPPER macro
+(JSONKeyMapper*)keyMapper;

@end

@interface RVSDataObject : JSONModel <RVSJsonDataObject>

@end

// should be placed in the @implementation of a RVSJsonDataObject derived classes
#define JSON_MODEL_KEY_MAPPER(className) \
+ (JSONKeyMapper*)keyMapper \
{ \
static JSONKeyMapper* className##KeyMapper; \
static dispatch_once_t className##KeyMapperOnceToken; \
dispatch_once(&className##KeyMapperOnceToken, ^{ \
className##KeyMapper = [[JSONKeyMapper alloc] initWithDictionary:[self jsonMappedProperties]]; \
}); \
return className##KeyMapper; \
}
