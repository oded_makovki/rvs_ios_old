//
//  RVSPostImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSyncedDataObject.h"
#import "RVSPost.h"
#import "RVSUserImpl.h"

@interface RVSPost : RVSSyncedDataObject <RVSPost>

@property (nonatomic) NSNumber<Optional> *likedByCaller;
@property (nonatomic) NSNumber<Optional> *repostedByCaller;
@property (nonatomic) NSNumber<Optional> *likeCount;
@property (nonatomic) NSNumber<Optional> *repostCount;
@property (nonatomic) NSNumber<Optional> *commentCount;
@property (nonatomic) NSMutableArray<RVSComment, Optional> *latestComments;

@end
