//
//  RVSPagingContext.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSPagingContext.h"

@interface RVSPagingContext ()

@property (nonatomic) NSString<Optional> *forwardCookie;

@end

@interface RVSPagingListBase ()

@property (nonatomic) RVSPagingContext<Optional> *pagingContext;

@end

@implementation RVSPagingContext

- (NSString*)description
{
    return self.forwardCookie;
}

@end

@implementation RVSPagingListBase

@end
