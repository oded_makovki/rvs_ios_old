//
//  RVSProgramExcerptImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSProgramExcerpt.h"
@protocol RVSAsyncImage;

@interface RVSProgramExcerpt : RVSDataObject <RVSProgramExcerpt>

@end
