//
//  RVSThumbnailImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSThumbnail.h"

@interface RVSThumbnail : RVSDataObject <RVSThumbnail>

@end
