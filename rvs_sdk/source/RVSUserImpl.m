//
//  RVSUserImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/12/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSUserImpl.h"
#import "RVSImage.h"
#import "RVSImageFromUrlOrArray.h"

@interface RVSUser () {
    
    NSObject<RVSAsyncImage> *_profileImage;
}

@property (nonatomic) NSString<Optional> *name;
@property (nonatomic) NSArray<RVSImage, Optional> *profileImages;
@property (nonatomic) NSString<Index> *userId;
@property (nonatomic) NSNumber<Optional> *isVerified;

@end

@implementation RVSUser

JSON_MODEL_KEY_MAPPER(RVSUser)

- (NSObject<RVSAsyncImage>*)profileImage
{
    if (_profileImage)
        return _profileImage;
    
    if (self.profileImages)
    {
        _profileImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.profileImages];
        return _profileImage;
    }
    else
        return nil;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"userProfile.id"                      :@"userId",
             @"userProfile.name"                    :@"name",
             @"userProfile.images"                  :@"profileImages",
             @"userProfile.verified"                :@"isVerified",
             @"followedByCaller"                    :@"isFollowedByMe",
             @"followerCount"                       :@"followerCount",
             @"followingCount"                      :@"followingCount",
             @"postCount"                           :@"postCount",
             @"likeCount"                           :@"likeCount",
             @"repostCount"                         :@"repostCount"
             };
}

+ (NSArray*)notMappedProperties
{
    return @[@"profileImage"];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.name, self.userId];
}

@end
