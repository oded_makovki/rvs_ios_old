//
//  RVSServiceManager.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/14/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk+Service.h"

@class STTwitterAPI;
@class RVSPromise;

@interface RVSServiceManager : NSObject

- (id)initWithDomain:(NSString*)domain;
- (void)signInWithUserName:(NSString*)userName password:(NSString*)password;
- (void)signInWithFacebookToken:(NSString*)token;
- (void)signInWithTwitterToken:(NSString*)token secret:(NSString *)secret;
- (void)signOut:(NSError*)error;
- (RVSPromise*)authenticate;
- (RVSPromise*)configuration;

@property (nonatomic, readonly) BOOL isSignedOut;
@property (nonatomic, readonly) RVSServiceNetworkStatus networkStatus;
@property(nonatomic, readonly) NSString *sessionToken;
@property (nonatomic, readonly) NSString *userId;

@end
