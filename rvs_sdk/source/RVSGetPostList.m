//
//  RVSGetPostList.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSGetPostList.h"
#import "RVSLogger.h"
#import "RVSPostList.h"
#import "RVSPostImpl.h"

@interface RVSGetPostList ()

@property (nonatomic) BOOL shouldFilterResponse;
@property (nonatomic) NSMutableSet *postIds;
@property (nonatomic) NSMutableArray *responseBuffer;
@end

@implementation RVSGetPostList

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithMethod:(NSString*)method filterResponse:(BOOL)filterResponse
{
    self = [super initWithMethod:method params:nil responseClass:([RVSPostList class]) arrayPropertyName:@"posts"];
    
    if (filterResponse)
    {
        _shouldFilterResponse = YES;
        _postIds = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    if (self.shouldFilterResponse)
    {
        // if no posts than nothing to filter
        if (responseArray.count)
        {
            // filter out all posts with post Id that we already got beofre
            for (RVSPost *post in responseArray)
            {
                if ([self.postIds containsObject:post.postId])
                {
                    RVSInfo(@"ignoring redundent post %@", post);
                    continue;
                }
                
                [self.responseBuffer addObject:post];
                [self.postIds addObject:post.postId];
            }
        }
    }
    else
    {
        [self.responseBuffer addObjectsFromArray:responseArray];
    }
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
}

@end

