//
//  RVSUserSearchResultImpl.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSUserSearchResultImpl.h"
#import "RVSUserImpl.h"
#import "RVSSearchMatchImpl.h"

@interface RVSUserSearchResult ()

@property (nonatomic) RVSUser <RVSUser, Optional> *user;
@property (nonatomic) NSArray <RVSSearchMatch, Optional> *searchMatches;
@property (nonatomic) NSNumber <Optional> *score;

@end

@implementation RVSUserSearchResult

JSON_MODEL_KEY_MAPPER(RVSUserSearchResult)

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"user"            :@"user",
             @"searchMatches"   :@"searchMatches",
             @"score"           :@"score"
             };
}

@end
