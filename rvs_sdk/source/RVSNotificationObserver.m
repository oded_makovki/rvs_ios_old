//
//  RVSNotificationObserver.m
//  rvs_sdk
//
//  Created by Barak Harel on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSNotificationObserver.h"
#import "RVSLogger.h"

@interface RVSNotificationObserver()
@property id observer;
@end

@implementation RVSNotificationObserver


-(id)initWithName:(NSString *)name object:(id)object usingBlock:(void (^)(NSNotification *notification))block
{
    self = [super init];
    if (self)
    {
        self.observer = [[NSNotificationCenter defaultCenter] addObserverForName:name object:object queue:[NSOperationQueue mainQueue] usingBlock:block];
    }
    
    return self;
}

+(instancetype)newObserverForNotificationWithName:(NSString *)name object:(id)object usingBlock:(void (^)(NSNotification *notification))block
{
    return [[self alloc] initWithName:name object:object usingBlock:block];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:_observer];
}

@end
