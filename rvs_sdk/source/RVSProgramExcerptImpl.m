//
//  RVSProgramExcerptImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSProgramExcerptImpl.h"
#import "RVSProgramImpl.h"
#import "RVSChannelImpl.h"
#import "RVSMediaContextImpl.h"
#import "RVSCoverImage.h"
#import "RVSImageFromUrlOrArray.h"

@interface RVSProgramExcerpt () {

    NSObject<RVSAsyncImage> *_coverImage;
}

@property (nonatomic) RVSChannel<RVSChannel, Optional> *channel;
@property (nonatomic) RVSProgram<RVSProgram, Optional> *program;
@property (nonatomic) RVSMediaContext<RVSMediaContext, Optional> *mediaContext;
@property (nonatomic) RVSCoverImage <Optional> *excerptCoverImage;
@property (nonatomic) NSString <Optional> *transcript;

@end

@implementation RVSProgramExcerpt

JSON_MODEL_KEY_MAPPER(RVSProgram)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"program"                 :@"program",
             @"channel"                 :@"channel",
             @"mediaContext"            :@"mediaContext",
             @"coverImage"              :@"excerptCoverImage",
             @"transcript"              :@"transcript"
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@", self.program.show.name];
}

- (NSObject<RVSAsyncImage>*)coverImage
{
    if (_coverImage)
        return _coverImage;
    
    if (self.excerptCoverImage)
    {
        _coverImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.excerptCoverImage.images];
        return _coverImage;
    }
    else
        return nil;
}

@end
