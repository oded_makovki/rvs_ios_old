//
//  RVSLoggerConfigController.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/12/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSLoggerConfigController.h"
#import <CocoaLumberjack/DDLog.h>
#import "RVSLogger+Internal.h"
#import "RVSLogger.h"
#import "NSString+rvssdk.h"

@interface RVSLoggerConfigControllerCell : UITableViewCell
@end

@interface RVSLoggerConfigController () <UISearchBarDelegate>


@property (nonatomic) NSArray *classes;
@property (nonatomic) NSArray *logLevelNames;
@property (nonatomic) NSString *toggleAllLogLevelName;
@property (nonatomic) UISearchBar *searchBar;

@end

static NSString *CellIdentifier = @"RVSLoggerConfigControllerCell";

@implementation RVSLoggerConfigControllerCell

RVS_LOGGING_IMPLEMENTATION

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    return self;
}

@end

@implementation RVSLoggerConfigController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Logger";
    
    self.searchBar = [[UISearchBar alloc] init];
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.delegate = self;
    
    self.navigationItem.rightBarButtonItems =@[
                                               [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(toggleAllLevels:)],
                                               [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemStop target:self action:@selector(stopSearch:)]
                                               ];

    self.classes = [DDLog registeredClassNames];
    self.logLevelNames = [RVSLogger logLevelNames];
    self.toggleAllLogLevelName = self.logLevelNames[0];
    [self.tableView registerClass:[RVSLoggerConfigControllerCell class] forCellReuseIdentifier:CellIdentifier];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.classes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    RVSLoggerConfigControllerCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSString *className = self.classes[indexPath.row];;
    cell.textLabel.text = className;
    cell.detailTextLabel.text = [RVSLogger logLevelNameFroClassName:className];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString *className = self.classes[indexPath.row];;
    NSString *levelName = [RVSLogger logLevelNameFroClassName:className];
    
    RVSLoggerConfigControllerCell *cell = (RVSLoggerConfigControllerCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSString* newLevelName = [self nextLogLevelName:levelName];
    
    cell.detailTextLabel.text = newLevelName;
    
    int newLogLevel = [RVSLogger logLevelFromString:newLevelName];
    
    Class theClass = NSClassFromString(className);
    [theClass ddSetLogLevel:newLogLevel];
    
    [RVSLogger saveConfig];
}

- (void)toggleAllLevels:(id)sender
{
    self.toggleAllLogLevelName = [self nextLogLevelName:self.toggleAllLogLevelName];
    int newLogLevel = [RVSLogger logLevelFromString:self.toggleAllLogLevelName];
    for (NSString *className in self.classes)
    {
        Class theClass = NSClassFromString(className);
        [theClass ddSetLogLevel:newLogLevel];
    }
    
    [self.tableView reloadData];
    [RVSLogger saveConfig];
}

- (NSString*)nextLogLevelName:(NSString*)logLevelName
{
    NSInteger index = [self.logLevelNames indexOfObject:logLevelName];
    
    if (index == NSNotFound)
    {
        RVSError(@"did not find level %@ in log levels", logLevelName);
        return @"Unknown";
    }
    
    index++;
    
    if (index == self.logLevelNames.count)
        index = 0;
    
    return self.logLevelNames[index];
}

- (void)stopSearch:(id)sender
{
    [self.searchBar resignFirstResponder];
    self.classes = [DDLog registeredClassNames];
    [self.tableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar textDidChange:self.searchBar.text];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSArray *classes = [DDLog registeredClassNames];
    if ([searchText isEmpty])
        self.classes = classes;
    else
        self.classes = [classes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF contains[c]  %@", searchText]];
    [self.tableView reloadData];
}

@end
