//
//  RVSPromise+Internal.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 3/27/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSPromise.h"

typedef void (^RVSPromiseDeallocBlock)();

@interface RVSPromise (Internal)

@property (nonatomic) RVSPromiseDeallocBlock deallocBlock;

@end
