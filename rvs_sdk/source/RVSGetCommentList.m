//
//  RVSGetCommentList.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetCommentList.h"
#import "RVSLogger.h"
#import "RVSCommentList.h"
#import "RVSCommentImpl.h"
#import "RVSComment.h"

@interface RVSGetCommentList ()
@property (nonatomic) NSMutableArray *responseBuffer;
@end

@implementation RVSGetCommentList

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithMethod:(NSString*)method
{
    self = [super initWithMethod:method params:nil responseClass:([RVSCommentList class]) arrayPropertyName:@"comments"];

    return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    NSMutableArray *comments = [[NSMutableArray alloc] initWithCapacity:responseArray.count];
    
    //TO DO: TBD? (otherwise just add responseArray to responseBuffer)
    for (RVSComment *comment in responseArray)
    {
        [comments addObject:comment];
    }
    
    [self.responseBuffer addObjectsFromArray:comments];
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
}

@end
