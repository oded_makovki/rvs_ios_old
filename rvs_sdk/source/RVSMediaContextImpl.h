//
//  RVSMediaContext.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSMediaContext.h"
#import "RVSDataObjectImpl.h"

@interface RVSMediaContext : RVSDataObject <RVSMediaContext>

@property (nonatomic, readonly) NSString *context;

@end
