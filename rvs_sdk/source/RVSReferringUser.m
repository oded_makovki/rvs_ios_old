//
//  RVSReferringUser.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSReferringUser.h"
#import "RVSUserImpl.h"

@interface RVSReferringUser ()

@property (nonatomic) RVSUser *user;
@property (nonatomic) NSNumber *timestamp;

@end

@implementation RVSReferringUser

@end
