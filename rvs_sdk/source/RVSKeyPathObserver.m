//
//  RVSKeyPathObserver.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 8/23/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSKeyPathObserver.h"
#import "RVSLogger.h"

@interface RVSKeyPathObserver ()

@property (nonatomic) NSObject *observed;

- (id)initWithObserved:(NSObject*)observed;

@end

@interface RVSSingleKeyPathObserver : RVSKeyPathObserver

@property (nonatomic )NSString *keyPath;
@property (nonatomic, strong) RVSKeyPathObserverKeyPathBlock block;

- (id)initWithObserved:(NSObject*)observed keyPath:(NSString*)keyPath options:(NSKeyValueObservingOptions)options block:(RVSKeyPathObserverKeyPathBlock)block;

@end

@interface RVSMultipleKeyPathsObserver : RVSKeyPathObserver

@property (nonatomic) NSArray *keyPaths;
@property (nonatomic, strong) RVSKeyPathObserverKeyPathsBlock block;

- (id)initWithObserved:(NSObject*)observed keyPaths:(NSArray*)keyPaths options:(NSKeyValueObservingOptions)options block:(RVSKeyPathObserverKeyPathsBlock)block;

@end

@implementation RVSKeyPathObserver

- (id)initWithObserved:(NSObject*)observed
{
    self = [super init];
    _observed = observed;
    return self;
}

@end

@implementation NSObject (RVSKeyPathObserver)

- (RVSKeyPathObserver*)newObserverForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options usingBlock:(RVSKeyPathObserverKeyPathBlock)block
{
    RVSVerifyMainThread();
    RVSParameterAssert(keyPath);
    RVSParameterAssert(block);
    
    return [[RVSSingleKeyPathObserver alloc] initWithObserved:self keyPath:keyPath options:options block:block];
}

- (RVSKeyPathObserver*)newObserverForKeyPaths:(NSArray *)keyPaths options:(NSKeyValueObservingOptions)options usingBlock:(RVSKeyPathObserverKeyPathsBlock)block
{
    RVSVerifyMainThread();
    RVSParameterAssert(keyPaths);
    RVSParameterAssert(block);
    
    return [[RVSMultipleKeyPathsObserver alloc] initWithObserved:self keyPaths:keyPaths options:options block:block];
}

@end

@implementation RVSSingleKeyPathObserver

- (id)initWithObserved:(NSObject*)observed keyPath:(NSString*)keyPath options:(NSKeyValueObservingOptions)options block:(RVSKeyPathObserverKeyPathBlock)block
{
    self = [super initWithObserved:observed];
    _keyPath = keyPath;
    _block = block;
    [self.observed addObserver:self forKeyPath:keyPath options:options context:nil];
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.block(self.observed, change);
}

- (void)dealloc
{
    [self.observed removeObserver:self forKeyPath:_keyPath context:nil];
}

@end

@implementation RVSMultipleKeyPathsObserver

- (id)initWithObserved:(NSObject*)observed keyPaths:(NSArray *)keyPaths options:(NSKeyValueObservingOptions)options block:(RVSKeyPathObserverKeyPathsBlock)block
{
    self = [super initWithObserved:observed];
    _keyPaths = keyPaths;
    _block = block;
    for (NSString *keyPath in _keyPaths)
        [self.observed addObserver:self forKeyPath:keyPath options:options context:nil];
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    self.block(self.observed, keyPath, change);
}

- (void)dealloc
{
    for (NSString *keyPath in _keyPaths)
        [self.observed removeObserver:self forKeyPath:keyPath context:nil];
}

@end