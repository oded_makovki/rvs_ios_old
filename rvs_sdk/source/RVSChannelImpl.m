//
//  RVSChannelImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSChannelImpl.h"
#import "RVSImageFromUrlOrArray.h"
#import "RVSImage.h"
#import "RVSProgram.h"
#import "RVSProgramImpl.h"

@interface RVSChannel () {
    
    NSObject<RVSAsyncImage> *_logo;
    NSObject<RVSAsyncImage> *_watermarkLogo;
}

@property (nonatomic) NSString<Index> *channelId;
@property (nonatomic) NSString<Optional> *name;
@property (nonatomic) NSArray<RVSImage, Optional> *logoImages;
@property (nonatomic) NSArray<RVSImage, Optional> *watermarkLogoImages;
@property (nonatomic) NSURL<Optional> *officialUrl;
@property (nonatomic) NSString<Optional> *callSign;
@property (nonatomic) RVSProgram <RVSProgram, Optional> *currentProgram;

@end

@implementation RVSChannel

JSON_MODEL_KEY_MAPPER(RVSChannel)

- (NSObject<RVSAsyncImage>*)logo
{
    if (_logo)
        return _logo;
    
    if (self.logoImages)
    {
        _logo = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.logoImages];
        return _logo;
    }
    else
        return nil;
}

- (NSObject<RVSAsyncImage>*)watermarkLogo
{
    if (_watermarkLogo)
        return _watermarkLogo;
    
    if (self.watermarkLogoImages)
    {
        _watermarkLogo = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.watermarkLogoImages];
        return _watermarkLogo;
    }
    else
        return nil;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"id"                                  :@"channelId",
             @"name"                                :@"name",
             @"officialUrl"                         :@"officialUrl",
             @"callSign"                            :@"callSign",
             @"images"                              :@"logoImages",
             @"watermarkImages"                     :@"watermarkLogoImages",
             @"currentProgram"                      :@"currentProgram"
             };
}

+ (NSArray*)notMappedProperties
{
    return @[@"logo"];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.name, self.channelId];
}

@end
