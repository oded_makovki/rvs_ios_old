    //
//  RVSGnChannelDetector.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/9/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

// RVS TODO: adjustedPosition
// RVS TODO: handle audio session issues, restart SDK when return from background? after phone call...
// RVS TODO: optimize battery
// RVS TODO: what happens if user does not want to allow access mic, how do we ask again?
// RVS TODO: disable while playing clips

@import AVFoundation;
#import "RVSGnChannelDetector.h"
#import <GracenoteACR/GnSdkManager.h>
#import <GracenoteACR/GnUser.h>
#import <GracenoteACR/GnACR.h>
#import <GracenoteACR/GnAcrAudioConfig.h>
#import <GracenoteACR/GnAudioSourceiOSMic.h>
#import <GracenoteACR/GnAcrMatch.h>
#import <SSKeychain/SSKeychain.h>
#import "RVSChannelImpl.h"
#import "RVSLogger.h"
#import "RVSSdk+Channels.h"
#import "RVSNotificationObserver.h"
#import "RVSSdk+Internal.h"
#import "RVSPromise.h"
#import "RVSProgram.h"

NSString *RVSSdkChannelDetectorMatchNotfication = @"RVSSdkChannelDetectorMatchNotfication";
NSString *RVSSdkChannelDetectorChannelUserInfoKey = @"RVSSdkChannelDetectorChannelUserInfoKey";
NSString *RVSSdkChannelDetectorTimeUserInfoKey = @"RVSSdkChannelDetectorTimeUserInfoKey";
NSString *RVSSdkChannelDetectorProgramUserInfoKey = @"RVSSdkChannelDetectorProgramUserInfoKey";


NSString *RVSSdkChannelDetectorErrorNotfication = @"RVSSdkChannelDetectorErrorNotfication";
NSString *RVSSdkChannelDetectorErrorUserInfoKey = @"RVSSdkChannelDetectorErrorUserInfoKey";

NSString *RVSSdkChannelDetectorStatusNotfication = @"RVSSdkChannelDetectorStatusNotfication";
NSString *RVSSdkChannelDetectorStatusUserInfoKey = @"RVSSdkChannelDetectorStatusUserInfoKey";

NSString *RVSSdkGracenoteLicenseString = @"RVSSdkGracenoteLicenseString";
NSString *RVSSdkGracenoteClientId      = @"RVSSdkGracenoteClientId";
NSString *RVSSdkGracenoteClientTag     = @"RVSSdkGracenoteClientTag";

static NSDateFormatter *rvsGnTvAiringDateFormatter;


@interface RVSGnChannelDetector () <IGnAcrResultDelegate, IGnAcrStatusDelegate, GnAudioSourceDelegate>

@property (nonatomic) GnSdkManager *gnSdkManager;
@property (nonatomic) GnUser *gnUser;
@property (nonatomic) GnACR *gnACR;
@property (nonatomic) GnAudioSourceiOSMic *gnAudioSourceiOSMic;
@property (nonatomic) RVSNotificationObserver *audioInterruptObserver;
@property (nonatomic) RVSNotificationObserver *mediaLostObserver;
@property (nonatomic) RVSNotificationObserver *mediaResetObserver;
@property (nonatomic) RVSNotificationObserver *clipPlayObserver;
@property (nonatomic) RVSNotificationObserver *clipStopObserver;
@property (nonatomic) RVSSdkChannelDetectorStatus status;
@property (nonatomic) dispatch_queue_t serialDispatchQueue;
@end

@implementation RVSGnChannelDetector

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)init
{
    self = [super init];
    _serialDispatchQueue = dispatch_queue_create("com.rayv.RVSGnChannelDetector", NULL);

    dispatch_async(_serialDispatchQueue, ^{
        [self initSdkManager];
        [self initUser];
        [self initRecording];
    });
    
    self.status = RVSSdkChannelDetectorStatusIdle;
    
    __weak RVSGnChannelDetector *weakSelf = self;
    
    _audioInterruptObserver = [RVSNotificationObserver newObserverForNotificationWithName:AVAudioSessionInterruptionNotification object:nil usingBlock:^(NSNotification *notification) {
        
        NSNumber *interruptType = notification.userInfo[AVAudioSessionInterruptionTypeKey];
        if (interruptType.integerValue == AVAudioSessionInterruptionTypeBegan)
        {
            RVSInfo(@"recording interrupted");
            dispatch_async(weakSelf.serialDispatchQueue, ^{
                [weakSelf stopACR];
            });
        }
        else if (interruptType.integerValue == AVAudioSessionInterruptionTypeEnded)
        {
            RVSInfo(@"resuming recording");
            dispatch_async(weakSelf.serialDispatchQueue, ^{
                [weakSelf initRecording];
            });
        }
    }];
    
    _mediaLostObserver = [RVSNotificationObserver newObserverForNotificationWithName:AVAudioSessionMediaServicesWereLostNotification object:nil usingBlock:^(NSNotification *notification) {
        RVSInfo(@"media services lost");
        dispatch_async(weakSelf.serialDispatchQueue, ^{
            [weakSelf stopACR];
        });
    }];
    
    _mediaResetObserver = [RVSNotificationObserver newObserverForNotificationWithName:AVAudioSessionMediaServicesWereResetNotification object:nil usingBlock:^(NSNotification *notification) {
        RVSInfo(@"media services reset");
        dispatch_async(weakSelf.serialDispatchQueue, ^{
            [weakSelf initRecording];
        });
    }];
    
    _clipPlayObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkClipPlaybackStartingNotification object:nil usingBlock:^(NSNotification *notification) {
        RVSInfo(@"clip is starting to play, stopping recording");
        dispatch_async(weakSelf.serialDispatchQueue, ^{
            [weakSelf stopACR];
        });
    }];
    
    _clipStopObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkClipPlaybackEndedNotification object:nil usingBlock:^(NSNotification *notification) {
        RVSInfo(@"clip playback ended, starting recording");
        dispatch_async(weakSelf.serialDispatchQueue, ^{
            [weakSelf initRecording];
        });
    }];
    
    return self;
}

- (void)dealloc
{
    [self stopACR];
}

- (void)initSdkManager
{
    NSError *error;
    NSString* gnLicenseString = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSSdkGracenoteLicenseString];
    if (!gnLicenseString || gnLicenseString.length == 0)
    {
        RVSError(@"Gracenote sdk manager license string not found in app-info.plist");
        return;
    }
    
    _gnSdkManager = [[GnSdkManager alloc] initWithLicense:gnLicenseString error:&error];
    
    if (error)
    {
        RVSError(@"failed to create Gracenote sdk manager %@", error);
        return;
    }
}

- (void)initUser
{
    if (!_gnSdkManager)
        return;
    
    if (!_gnUser)
    {
        NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
        NSString* gnClientId = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSSdkGracenoteClientId];
        NSString* gnClientTag = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSSdkGracenoteClientTag];
        
        if (!gnClientId || gnClientId.length == 0 || !gnClientTag || gnClientTag.length == 0)
        {
            RVSError(@"Failed to create Gracenote user. Client Tag or ID not found in app-info.plist");
            return;
        }
        
        if (!version)
        {
            version = @"0.0.0"; //fake
        }
        
        NSError *error;
        _gnUser = [[GnUser alloc] initWithClientId:gnClientId
                                      clientIdTag:gnClientTag
                                       appVersion:version
                                        registrationType:GnUserRegistrationType_NewUser error:&error];
        
        if (error)
        {
            RVSError(@"Failed to create Gracenote user %@", error);
            return;
        }
    }
}

- (void)initAcr
{
    if (!_gnUser)
        return;
    
    NSError *error;
    _gnACR = [[GnACR alloc] initWithUser:_gnUser
                                 error:&error];
    
    if (error)
    {
        RVSError(@"failed to create Gracenote ACR %@", error);
        return;
    }
    
    _gnACR.statusDelegate = self;
    _gnACR.resultDelegate = self;
    
    // set the Audio config on the ACR object
    GnAcrAudioConfig *audioConfig = [[GnAcrAudioConfig alloc] initWithAudioSourceType:GnAcrAudioSourceMic
                                            sampleRate:GnAcrAudioSampleRate44100
                                            format:GnAcrAudioSampleFormatPCM16
                                            numChannels:1];
    
    error = [_gnACR audioInitWithAudioConfig:audioConfig];
    
    if (error)
    {
        RVSError(@"failed to set audio config %@", error);
        return;
    }
    
    [_gnACR setOptimizationMode:GnAcrOptimizationDefault];
    
    _gnAudioSourceiOSMic = [[GnAudioSourceiOSMic alloc] initWithAudioConfig:audioConfig];
    _gnAudioSourceiOSMic.audioDelegate = self;
    
    error = [_gnAudioSourceiOSMic startMicrophone];
    
    if (error)
    {
        RVSError(@"iOS mic audio source failed to start %@", error);
    }
}

- (void)stopACR
{
    NSError *error;
    error = [_gnAudioSourceiOSMic stopMicrophone];
    
    if (error)
    {
        RVSError(@"iOS mic audio source failed to stop %@", error);
    }
    
    @synchronized (self)
    {
        _gnAudioSourceiOSMic.audioDelegate = nil;
        _gnAudioSourceiOSMic = nil;
        
        _gnACR.statusDelegate = nil;
        _gnACR.resultDelegate = nil;
        _gnACR  = nil;
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];

    // if we don't do that then MPMoviePlayerController fails to play video
    error = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error)
    {
        RVSError(@"failed setting audio session playback category %@", error);
    }
    
    error = nil;
    [audioSession setActive:YES error:&error];
    
    if (error)
    {
        RVSError(@"failed activating playback audio session %@", error);
    }
}

- (void)initRecording
{
    if (!_gnUser)
        return;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError *error;
    [audioSession setCategory:AVAudioSessionCategoryPlayAndRecord error:&error];
    
    if (error)
    {
        RVSError(@"failed setting audio session category %@", error);
        return;
    }
    
    error = nil;
    [audioSession setActive:YES error:&error];
    
    if (error)
    {
        RVSError(@"failed activating play and record audio session %@", error);
        return;
    }
    
    [self initAcr];
}

#pragma mark IGnAcrResultDelegate

-(void)acrResultReady:(GnResult*)result
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [self handlAcrResult:result];
        }
        @catch (NSException *exception) {
            RVSError(@"Exception in handlAcrResult: %@", exception);
        }
    });
}

- (void)handlAcrResult:(GnResult*)result
{
    if (result.resultType == GnResultTypeAcrMatch)
    {
        GnTvChannel *gnChannel;
        for (GnAcrMatch *match in result.acrMatches)
        {
            GnTvAiring *airing = match.tvAiring;
            gnChannel = airing.channel;
            NSString *channelCallsign = gnChannel.callsign;
            RVSInfo(@"detected channel %@", channelCallsign);
            if (channelCallsign.length > 0)
            {
                // cache formatter for efficiency
                // 2013-11-18T15:42:16Z
                static dispatch_once_t rvsGnTvAiringDateFormatterOnce;
                dispatch_once(&rvsGnTvAiringDateFormatterOnce, ^{
                    rvsGnTvAiringDateFormatter = [[NSDateFormatter alloc] init];
                    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
                    [rvsGnTvAiringDateFormatter setTimeZone:timeZone];
                    [rvsGnTvAiringDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm"];
                });
                
                __block NSDate *start = [rvsGnTvAiringDateFormatter dateFromString:airing.dateStart];
                __block NSTimeInterval position = [match.actualPosition doubleValue] / 1000;
                
                __weak typeof(self) weakSelf = self;
                [[RVSSdk sharedSdk] channelForCallSign:channelCallsign].thenOn(dispatch_get_main_queue(), ^id(id channel) {
                    [weakSelf postDetectedChannel:channel time:[start dateByAddingTimeInterval:position]];
                    return nil;
                }, nil);
            }
        }
    }
    else
    {
        RVSWarn(@"unknown gracenote result %d", result.resultType);
    }
}

#pragma mark IGnAcrStatusDelegate

-(void)acrStatusReady:(GnAcrStatus*)status
{
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            [self handleAcrStatus:status];
        }
        @catch (NSException *exception) {
            RVSError(@"Exception in handleAcrStatus: %@", exception);
        }
    });
}

-(void)handleAcrStatus:(GnAcrStatus*)status
{
    RVSVerbose(@"status %d %f %@ %@", status.statusType, status.value, status.error, status.message);
    
    switch (status.statusType)
    {
        case GnAcrStatusTypeFingerprintStarted:
            self.status = RVSSdkChannelDetectorStatusDetecting;
            break;
            
        case GnAcrStatusTypeOnlineLookupComplete:
            self.status = RVSSdkChannelDetectorStatusMatching;
            break;
            
        case GnAcrStatusTypeSilentRatio:
            self.status = RVSSdkChannelDetectorStatusIdle;
            break;
            
        default:
            break;
    }
    
    if (status.error)
    {
        RVSError(@"Error in gracenote status: %@", status.error);
        [self postError:status.error];
        self.status = RVSSdkChannelDetectorStatusError;
    }
}

-(void)setStatus:(RVSSdkChannelDetectorStatus)status
{
    if (_status != status)
    {
        _status = status;
        [self postStatus:status];
    }
}

#pragma mark GnAudioSourceDelegate

-(void)audioBytesReady:(void const * const)bytes length:(int)length
{
    NSError *error = nil;
    
    // make sure gnAcr is not delloacated in the middle of writing bytes
    @synchronized (self)
    {
        error = [self.gnACR writeBytes:bytes length:length];
    }

    if (error)
    {
        RVSError(@"audioBytesReady error: %@", error);
    }
}

#pragma mark - Notifications

- (void)postDetectedChannel:(NSObject<RVSChannel>*)channel time:(NSDate *)time
{
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObject:channel forKey:RVSSdkChannelDetectorChannelUserInfoKey];
    
    if (time)
        userInfo[RVSSdkChannelDetectorTimeUserInfoKey] = time;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] userInfo:userInfo];
}

- (void)postError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSSdkChannelDetectorErrorNotfication object:[RVSSdk sharedSdk] userInfo:@{ RVSSdkChannelDetectorErrorUserInfoKey : error}];
}

- (void)postStatus:(RVSSdkChannelDetectorStatus)status
{

    [[NSNotificationCenter defaultCenter] postNotificationName:RVSSdkChannelDetectorStatusNotfication object:[RVSSdk sharedSdk] userInfo:@{RVSSdkChannelDetectorStatusUserInfoKey : @(status)}];
}

@end
