//
//  NSString+rvssdk.m
//  Playful
//
//  Created by ost on 6/5/13.
//  Copyright (c) 2013 ost12666. All rights reserved.
//

#import "NSString+rvssdk.h"

@interface RVSNSStringEntitiesConverter : NSObject <NSXMLParserDelegate>

@property (nonatomic) NSMutableString* resultString;

- (NSString*)convertEntiesInString:(NSString*)s;

@end

@implementation RVSNSStringEntitiesConverter

- (id)init
{
    self = [super init];
    _resultString = [[NSMutableString alloc] init];
    return self;
}
- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)s
{
    [self.resultString appendString:s];
}

- (NSString*)convertEntiesInString:(NSString*)s
{
    NSString* xmlStr = [NSString stringWithFormat:@"<d>%@</d>", s];
    NSData *data = [xmlStr dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    NSXMLParser* xmlParse = [[NSXMLParser alloc] initWithData:data];
    [xmlParse setDelegate:self];
    [xmlParse parse];
    NSString* returnStr = [[NSString alloc] initWithFormat:@"%@",self.resultString];
    return returnStr;
}

@end

@implementation NSString (rvssdk)

- (BOOL)isEmpty
{
    return [[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""];
}

- (NSString*)replaceSpacesWithCapitals
{
    NSArray *components = [self componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (components.count == 1)
        return self;

    NSMutableString *newString = [[NSMutableString alloc] init];
    for (NSString *component in components)
        [newString appendString:[component capitalizedString]];
    
    return newString;
}

- (NSString*)decodeHtmlEntities
{
    RVSNSStringEntitiesConverter *converter = [[RVSNSStringEntitiesConverter alloc] init];
    return [converter convertEntiesInString:self];
}

@end
