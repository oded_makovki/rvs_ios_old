//
//  RVSGetPagingListBase.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSGetPagingListBase.h"
#import "RVSLogger.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSPagingContext.h"
#import "RVSPromise.h"

@interface RVSGetPagingListBase ()

@property (nonatomic) NSUInteger count;
@property (nonatomic) BOOL didReachEnd;
@property (nonatomic) RVSPagingContext *pagingContext;
@property (nonatomic) RVSPromise *promise;
@property (nonatomic) RVSPromise *requestPromise;
@property (nonatomic) NSString *method;
@property (nonatomic) NSDictionary *params;
@property (nonatomic) Class responseClass;
@property (nonatomic) NSString *arrayPropertyName;

@end

@implementation RVSGetPagingListBase

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithMethod:(NSString*)method params:(NSDictionary*)params responseClass:(Class)responseClass arrayPropertyName:(NSString*)arrayPropertyName
{
    RVSAssert([responseClass isSubclassOfClass:[RVSPagingListBase class]]);
    self = [super init];
    _method = method;
    _params = params;
    _responseClass = responseClass;
    _arrayPropertyName = arrayPropertyName;
    return self;
}

- (void)dealloc
{
    if (_promise.isPending)
        [_promise cancel];
}

// subclasses will tell this class to fulfill the promise by returning an array from processList
- (RVSPromise*)next:(NSUInteger)count
{
    RVSVerifyMainThread();
    
    RVSAssert(!self.promise || !self.promise.isPending);
    
    self.promise = [[RVSPromise alloc] init];
    
    self.count = count;
    [self internalNext:count];
    
    __weak RVSPromise *weakPromise = self.promise;
    __weak typeof(self) weakSelf = self;
    self.promise.then(nil, ^NSError*(NSError *error) {
        if (weakPromise.isCancelled)
            [weakSelf.requestPromise cancel];
        return nil;
    });
    
    return self.promise;
}

- (void)internalNext:(NSUInteger)count
{
    RVSAssert(!self.requestPromise || !self.requestPromise.isPending);
    RVSAssert(!self.didReachEnd);
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:[NSNumber numberWithUnsignedInteger:count] forKey:@"pageSize"];
    if (self.pagingContext)
        params[@"pagingCookie"] = self.pagingContext.forwardCookie;
    if (self.params)
        [params addEntriesFromDictionary:self.params];
    
    __weak RVSGetPagingListBase *weakSelf = self;
    self.requestPromise = [[RVSSdk sharedSdk].requestManager getJsonObject:self.method parameters:params objectClass:self.responseClass];
    self.requestPromise.then(^id(RVSPagingListBase *pagingList) {
        typeof(self) strongSelf = weakSelf;
        if (strongSelf)
        {
            NSArray *responseArray = [pagingList valueForKey:strongSelf.arrayPropertyName];
            
            if (responseArray)
            {
                RVSInfo(@"got %@ %@", NSStringFromClass(strongSelf.responseClass), pagingList);
                
                strongSelf.pagingContext = pagingList.pagingContext;
                
                if (!strongSelf.pagingContext || !strongSelf.pagingContext.forwardCookie || [strongSelf.pagingContext.forwardCookie length] == 0)
                {
                    self.didReachEnd = YES;
                }
                
                [strongSelf processList:responseArray];
            }
            else
            {
                RVSError(@"got nil response array");
                [strongSelf.promise rejectWithReason:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadServerResponse userInfo:nil]];
            }
        }
        return nil;
    }, ^NSError*(NSError *error) {
        [weakSelf.promise rejectWithReason:error];
        return nil;
    });
}


- (void)processList:(NSArray *)responseArray
{
    RVSAssert(false);
}

- (void)gotResultList:(NSArray*)resultList
{
    [self.promise fulfillWithValue:resultList];
}

@end

