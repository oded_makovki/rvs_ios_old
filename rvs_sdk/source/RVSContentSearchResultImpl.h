//
//  RVSContentSearchResultImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSContentSearchResult.h"

@interface RVSContentSearchResult : RVSDataObject <RVSContentSearchResult>

@end
