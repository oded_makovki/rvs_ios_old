//
//  RVSSearchMatchImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSSearchMatchImpl.h"
#import "RVSMediaContextImpl.h"
#import "RVSCoverImage.h"
#import "RVSImageFromUrlOrArray.h"

@interface RVSSearchMatch ()
{    
    NSObject<RVSAsyncImage> *_coverImage;
}

@property (nonatomic) NSString<Optional> *fieldName;
@property (nonatomic) NSString<Optional> *valueFragment;
@property (nonatomic) NSArray <Optional, RVSFragmentRange> *fragmentRanges;
@property (nonatomic) NSNumber<Optional> *referenceTime;
@property (nonatomic) RVSMediaContext <RVSMediaContext, Optional> *mediaContext;
@property (nonatomic) RVSCoverImage <Optional> *matchCoverImage;
@end

@implementation RVSSearchMatch

JSON_MODEL_KEY_MAPPER(RVSSearchMatch)

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"fieldName"              :@"fieldName",
             @"valueFragment"          :@"valueFragment",
             @"fragmentRanges"         :@"fragmentRanges",
             @"referenceTime"          :@"referenceTime",
             @"mediaContext"           :@"mediaContext",
             @"coverImage"             :@"matchCoverImage"
             };
}

- (NSObject<RVSAsyncImage>*)coverImage
{
    if (_coverImage)
        return _coverImage;
    
    if (self.matchCoverImage)
    {
        _coverImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.matchCoverImage.images];
        return _coverImage;
    }
    else
        return nil;
}

@end
