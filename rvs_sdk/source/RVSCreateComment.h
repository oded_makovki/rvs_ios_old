//
//  RVSCreateComment.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
@class RVSPromise;

@interface RVSCreateComment : NSObject

- (id)initWithMessage:(NSString*)message postId:(NSString *)postId;
- (RVSPromise*)createComment;

@end
