//
//  RVSGetMediaContext.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

@import Foundation;
@class RVSPromise;

@interface RVSGetMediaContext : NSObject

- (id)initWithChannel:(NSString*)channelId;
- (RVSPromise*)getMediaContext;

@end