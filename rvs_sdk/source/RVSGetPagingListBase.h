//
//  RVSGetPagingListBase.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAsyncList.h"

@interface RVSGetPagingListBase : NSObject <RVSAsyncList>

@property (nonatomic, readonly) NSUInteger count;

- (id)initWithMethod:(NSString*)method params:(NSDictionary*)params responseClass:(Class)responseClass arrayPropertyName:(NSString*)arrayPropertyName;

// perform a request to get more objects from the list, count property is not changed by given count parameter
- (void)internalNext:(NSUInteger)count;

// called on the derived class when the list was received to allow processing by derived classes
// derived classes should call gotResultList when got result
- (void)processList:(NSArray*)responseArray;

// called by the derived class when got the result list
- (void)gotResultList:(NSArray*)resultList;

@end
