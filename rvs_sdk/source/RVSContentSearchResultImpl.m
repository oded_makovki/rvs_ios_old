//
//  RVSContentSearchResultImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSContentSearchResultImpl.h"
#import "RVSSearchMatchImpl.h"
#import "RVSPostImpl.h"
#import "RVSProgramExcerptImpl.h"
#import "RVSChannelImpl.h"

@interface RVSContentSearchResult ()

@property (nonatomic) RVSProgramExcerpt<RVSProgramExcerpt, Optional> *programExcerpt;
@property (nonatomic) RVSPost<RVSPost, Optional> *post;
@property (nonatomic) RVSChannel<RVSChannel, Optional> *channel;
@property (nonatomic) NSArray <RVSSearchMatch, Optional> *searchMatches;
@property (nonatomic) NSNumber <Optional> *score;

@end

@implementation RVSContentSearchResult

JSON_MODEL_KEY_MAPPER(RVSContentSearchResult)

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"programExcerpt"  :@"programExcerpt",
             @"post"            :@"post",
             @"channel"         :@"channel",
             @"searchMatches"   :@"searchMatches",
             @"score"           :@"score"
             };
}

@end
