//
//  RVSSyncedDataObject.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSyncedDataObject.h"
#import "RVSNotificationObserver.h"
#import "RVSLogger.h"

@interface RVSSyncedDataObject ()

@property (nonatomic) RVSNotificationObserver<Ignore> *propChangeObserver;

@end

@implementation RVSSyncedDataObject

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    self = [super initWithDictionary:dict error:err];
    
    if (!self)
        return nil;
    
    NSAssert(self.indexPropertyName, @"No index property");
    
    // notiication name is class name + value of index property (e.g. user ID, post ID, etc.)
    __weak RVSSyncedDataObject *weakSelf = self;
    _propChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:[[self class] notificationName:[self valueForKey:self.indexPropertyName]] object:nil usingBlock:^(NSNotification *notification) {
        
        // update changed object, optionally firing up KVO notifications
        RVSSyncedDataObjectSync syncBlock = notification.userInfo[@"syncBlock"];
        syncBlock(weakSelf);
    }];
    
    return  self;
}

+ (void)notifyChangedDataObjectWithId:(NSString*)dataObjectId syncBlock:(RVSSyncedDataObjectSync)syncBlock
{
    if (dataObjectId)
    {
        NSDictionary *userInfo = @{@"syncBlock" : syncBlock};
        [[NSNotificationCenter defaultCenter] postNotificationName:[self notificationName:dataObjectId] object:self userInfo:userInfo];
    }
}

+ (NSString*)notificationName:(NSString*)dataObjectId
{
    NSString *notificationName = NSStringFromClass([self class]);
    notificationName = [notificationName stringByAppendingString:dataObjectId];
    return notificationName;
}

@end
