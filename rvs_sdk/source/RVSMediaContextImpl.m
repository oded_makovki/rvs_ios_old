//
//  RVSMediaContextImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSMediaContextImpl.h"
#import "RVSpromise.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSLogger.h"
#import "RVSThumbnailList.h"

NSString *RVSClipMediaTypeHLS = @"hls";

@interface RVSMediaContext ()

@property (nonatomic) NSString *context;
@property (nonatomic) NSNumber *start;
@property (nonatomic) NSNumber *end;
@end

@implementation RVSMediaContext

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

JSON_MODEL_KEY_MAPPER(RVSMediaContext)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"context"                             :@"context",
             @"startTime"                           :@"start",
             @"endTime"                             :@"end",
             };
}

- (NSTimeInterval)duration
{
    return (self.end.doubleValue - self.start.doubleValue) / 1000;
}

- (NSURL*)clipUrlWithMediaType:(NSString *)mediaType startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration
{
    RVSVerifyMainThread();
    
    NSMutableDictionary *parameters =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.context, @"mediaContext", [NSNumber numberWithLongLong:startOffset * 1000], @"startOffset", [NSNumber numberWithLongLong:(startOffset + duration) * 1000], @"endOffset", nil];
    
    NSString *mediaSource = [[NSUserDefaults standardUserDefaults] stringForKey:@"mediaSource"];
    if (mediaSource)
        parameters[@"mediaSource"] = mediaSource;
    
    return [[RVSSdk sharedSdk].requestManager completeURLForMethod:@"media/video/types/hls" parameters:parameters];
}

- (RVSPromise*)thumbnailsWithStartOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration
{
    RVSVerifyMainThread();
    
    NSMutableDictionary *parameters =  [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.context, @"mediaContext", [NSNumber numberWithLongLong:startOffset * 1000], @"startOffset", [NSNumber numberWithLongLong:(startOffset + duration) * 1000], @"endOffset", nil];
    
    NSString *imageSource = [[NSUserDefaults standardUserDefaults] stringForKey:@"imageSource"];
    if (imageSource)
        parameters[@"imageSource"] = imageSource;
    
    return [[RVSSdk sharedSdk].requestManager getJsonObjectProperty:@"media/thumbnails" parameters:parameters objectClass:[RVSThumbnailList class] propertyName:@"images"];
}

@end
