//
//  RVSLogger.h
//  rvs_sdk
//
//  Created by ost on 3/9/13.
//  Copyright (c) 2013 ost12666. All rights reserved.
//

#import "RVSLogger+Internal.h"
#import <CocoaLumberjack/DDTTYLogger.h>
#import <CocoaLumberjack/DDFileLogger.h>
#import <CocoaLumberjack/DDDispatchQueueLogFormatter.h>
#import <objc/runtime.h>

const char *__crashreporter_info__ = NULL;
asm(".desc _crashreporter_info, 0x10");

int ddLogLevel = LOG_LEVEL_ERROR;

@interface RVSLoggerFormatter : DDDispatchQueueLogFormatter

@end

@implementation RVSLoggerFormatter

- (NSString *)formatLogMessage:(DDLogMessage *)logMessage
{
    NSString *logLevel;
    switch (logMessage->logFlag)
    {
        case LOG_FLAG_ERROR : logLevel = @"E"; break;
        case LOG_FLAG_WARN  : logLevel = @"W"; break;
        case LOG_FLAG_INFO  : logLevel = @"I"; break;
        case LOG_FLAG_VERBOSE : logLevel = @"V"; break;
        default             : logLevel = @"?"; break;
    }
    
    NSString *timestamp = [self stringFromDate:(logMessage->timestamp)];
    
    NSString *queueThreadLabel = [self queueThreadLabelForLogMessage:logMessage];
    
    NSString *message = [NSString stringWithFormat:@"%@ [%@] %@ %@:%s(%d) %@", timestamp, queueThreadLabel, logLevel, [logMessage fileName], logMessage->function, logMessage->lineNumber, logMessage->logMsg];
    
    return message;
}

@end

@implementation RVSLogger

- (id)init
{
    RVSVerifyMainThread();
    
    self = [super init];
    
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    [[DDTTYLogger sharedInstance] setLogFormatter:[[RVSLoggerFormatter alloc] init]];
    [[DDTTYLogger sharedInstance] setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor blueColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];
    [[DDTTYLogger sharedInstance] setForegroundColor:[UIColor purpleColor] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];
    
    [self loadConfig];
         
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    
    NSString *logsDirectory = [documentsDirectory stringByAppendingPathComponent:@"logs"];
    DDLogFileManagerDefault *logFileManager = [[DDLogFileManagerDefault alloc] initWithLogsDirectory:logsDirectory];
    DDFileLogger *fileLogger = [[DDFileLogger alloc] initWithLogFileManager:logFileManager];
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;
    [fileLogger setLogFormatter:[[RVSLoggerFormatter alloc] init]];
    [DDLog addLogger:fileLogger];
    
    RVSInfo(@"Logging started...");
    
    return self;
}

+ (NSString*)configFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths[0];
    return [documentsDirectory stringByAppendingPathComponent:@"rvs_logger.plist"];
}

// dynamic logging implementation, to make sure RVSLogger is in the log classes UI but it actually changes the global level
+ (int)ddLogLevel
{
    return ddLogLevel;
}

+ (void)ddSetLogLevel:(int)logLevel
{
    ddLogLevel = logLevel;
}

- (void)loadConfig
{
    NSDictionary *rvsLoggerConfig = [NSMutableDictionary dictionaryWithContentsOfFile:[RVSLogger configFilePath]];
    
    NSArray *classes = rvsLoggerConfig[@"Classes"];
    
    if (classes)
        NSLog(@"Applying log levels for %d classes", classes.count);
    
    for (NSDictionary *classConfig in classes)
    {
        NSString *className = classConfig[@"Name"];
        NSString *logLevelName = classConfig[@"Level"];
        NSLog(@"%@ : %@", className, logLevelName);
        Class theClass = NSClassFromString(className);
        SEL setterSel = @selector(ddSetLogLevel:);
        Method setter = class_getClassMethod(theClass, setterSel);
        if (theClass && setter != NULL )
        {
            int logLevel = [RVSLogger logLevelFromString:logLevelName];
            
            if (logLevel < 0)
            {
                NSLog(@"unknown log level '%@', using OFF instead", logLevelName);
                logLevel = LOG_LEVEL_OFF;
            }
            
            [theClass ddSetLogLevel:logLevel];
        }
        else
        {
            NSLog(@"could not find class %@ or class does not support dynamic logging", className);
        }
    }
}

+ (void)saveConfig
{
    NSMutableDictionary *config = [[NSMutableDictionary alloc] init];
    NSArray * classNames = [DDLog registeredClassNames];
    NSMutableArray *classConfigs = [[NSMutableArray alloc] initWithCapacity:1];
   
    for (NSString *className in classNames)
    {
        int logLevel = [NSClassFromString(className) ddLogLevel];
        NSDictionary *logConfig = @{@"Name" : className, @"Level" : [RVSLogger stringFromLogLevel:logLevel]};
        [classConfigs addObject:logConfig];
    }
    
    config[@"Classes"] = classConfigs;
    
    [config writeToFile:[RVSLogger configFilePath] atomically:YES];
}

+ (void)setAllLogLevels:(NSString*)logLevel
{
    int logLevelInt = [RVSLogger logLevelFromString:logLevel];
    NSArray *classes = [DDLog registeredClasses];
    
    for (Class loggingClass in classes)
        [loggingClass ddSetLogLevel:logLevelInt];
}

+ (NSString*)logLevelNameFroClassName:(NSString*)className
{
    Class theClass = NSClassFromString(className);
    
    if (theClass)
    {
        int logLevel = [theClass ddLogLevel];
        return [RVSLogger stringFromLogLevel:logLevel];
    }
    else
    {
        NSLog(@"could not find class %@", className);
    }
    
    return @"OFF";
}

+ (int)logLevelFromString:(NSString*)logLevel
{
    if ([logLevel isEqualToString:@"INFO"])
        return LOG_LEVEL_INFO;
    else if ([logLevel isEqualToString:@"WARN"])
        return LOG_LEVEL_WARN;
    else if ([logLevel isEqualToString:@"ERROR"])
        return LOG_LEVEL_ERROR;
    else if ([logLevel isEqualToString:@"OFF"])
        return LOG_LEVEL_OFF;
    else if ([logLevel isEqualToString:@"VERBOSE"])
        return LOG_LEVEL_VERBOSE;

    return -1;
}

+ (NSString*)stringFromLogLevel:(int)logLevel
{
    switch (logLevel)
    {
        case LOG_LEVEL_VERBOSE:
            return @"VERBOSE";
        case LOG_LEVEL_INFO:
            return @"INFO";
        case LOG_LEVEL_WARN:
            return @"WARN";
        case LOG_LEVEL_OFF:
            return @"OFF";
        case LOG_LEVEL_ERROR:
            return @"ERROR";
        default:
            return @"UNKNOWN";
    }
}

+ (NSArray*)logLevelNames
{
    return @[@"OFF", @"VERBOSE", @"INFO", @"WARN", @"ERROR"];
}

@end
