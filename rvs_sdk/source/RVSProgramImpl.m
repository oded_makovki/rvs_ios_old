//
//  RVSProgramImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSProgramImpl.h"
#import "RVSImageFromUrlOrArray.h"
#import "RVSImage.h"

@interface RVSCastMember ()

@property(nonatomic) NSString<Optional> *character;
@property(nonatomic) NSString<Optional> *actor;

@end

@interface RVSShow () {

    NSObject<RVSAsyncImage> *_image;
}

@property(nonatomic) NSString<Index, Optional> *showId;
@property(nonatomic) NSString<Optional> *name;
@property(nonatomic) NSString<Optional> *synopsis;
@property (nonatomic) NSArray<RVSCastMember, Optional> * castMembers;
@property (nonatomic) NSArray<RVSImage, Optional> *images;
@property (nonatomic) NSURL<Optional> *officialUrl;

@end

@interface RVSEpisode () {
    
    NSObject<RVSAsyncImage> *_image;
}

@property(nonatomic) NSString<Index, Optional> *showId;
@property(nonatomic) NSString<Optional> *name;
@property(nonatomic) NSString<Optional> *synopsis;
@property (nonatomic) NSArray<RVSCastMember, Optional> * castMembers;
@property (nonatomic) NSArray<RVSImage, Optional> *images;
@property (nonatomic) NSURL<Optional> *officialUrl;
@property (nonatomic) NSNumber<Optional> *seasonNumber;
@property (nonatomic) NSNumber<Optional> *episodeNumber;
@property (nonatomic) NSString<Optional> *roviSeriesId;

@end

@interface RVSProgram ()

@property (nonatomic) NSString<Index, Optional> *programId;
@property (nonatomic) NSNumber<Optional> *start;
@property (nonatomic) NSNumber<Optional> *end;
@property (nonatomic) RVSShow<RVSShow, Optional> *show;
@property (nonatomic) RVSEpisode<RVSEpisode, Optional> *episode;

@end

@implementation RVSCastMember

JSON_MODEL_KEY_MAPPER(RVSCastMember)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"character"               :@"character",
             @"actor"                   :@"actor",
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.character, self.actor];
}

@end

@implementation RVSShow

JSON_MODEL_KEY_MAPPER(RVSShow)

- (NSObject<RVSAsyncImage>*)image
{
    if (_image)
        return _image;
    
    if ([self.images count] > 0)
    {
        _image = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.images];
        return _image;
    }
    else
        return nil;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"showId"                  :@"showId",
             @"name"                    :@"name",
             @"synopsis"                :@"synopsis",
             @"castMembers"             :@"castMembers",
             @"images"                  :@"images",
             @"officialUrl"             :@"officialUrl",
             };
}

+ (NSArray*)notMappedProperties
{
    return @[@"image"];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@", self.name];
}

@end

@implementation RVSEpisode

JSON_MODEL_KEY_MAPPER(RVSEpisode)

- (NSObject<RVSAsyncImage>*)image
{
    if (_image)
        return _image;
    
    if ([self.images count] > 0)
    {
        _image = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.images];
        return _image;
    }
    else
        return nil;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"showId"                  :@"showId",
             @"name"                    :@"name",
             @"synopsis"                :@"synopsis",
             @"castMembers"             :@"castMembers",
             @"images"                  :@"images",
             @"officialUrl"             :@"officialUrl",
             @"seasonNumber"             :@"seasonNumber",
             @"episodeNumber"            :@"episodeNumber",
             };
}

+ (NSArray*)notMappedProperties
{
    return @[@"image"];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (S%@E%@)", self.name, self.seasonNumber, self.episodeNumber];
}

@end

@implementation RVSProgram

JSON_MODEL_KEY_MAPPER(RVSProgram)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"id"                      :@"programId",
             @"showMetadata"            :@"show",
             @"startTime"               :@"start",
             @"endTime"                 :@"end",
             @"episodeMetadata"         :@"episode",
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.show.name, self.start];
}

@end
