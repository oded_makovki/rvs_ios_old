//
//  RVSCommentImpl.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSCommentImpl.h"
#import "RVSSdk+Internal.h"
#import "RVSUserImpl.h"

@interface RVSComment()

@property (nonatomic) NSString <Optional, Index> *commentId;
@property (nonatomic) NSString <Optional> *message;
@property (nonatomic) RVSUser <RVSUser, Optional> *author;
@property (nonatomic) NSNumber <Optional> *creationTime;

@end

@implementation RVSComment

JSON_MODEL_KEY_MAPPER(RVSComment)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"message"                             :@"message",
             @"id"                                  :@"commentId",
             @"author"                              :@"author",
             @"creationTime"                        :@"creationTime",
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.message, self.commentId];
}

@end
