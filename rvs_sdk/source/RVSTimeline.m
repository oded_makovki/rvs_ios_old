//
//  RVSTimeline.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSTimeline.h"
#import "RVSPostImpl.h"

@interface RVSTimelineItem ()

@property (nonatomic) RVSPost *post;
@property (nonatomic) RVSReferringUser<Optional> *repostingUser;

@end

@interface RVSTimeline ()

@property (nonatomic) NSArray<RVSTimelineItem, Optional> *timelineItems;

@end

@implementation RVSTimelineItem

@end

@implementation RVSTimeline

- (NSString*)description
{
    return [NSString stringWithFormat:@"%d items paging:%@", self.timelineItems.count, self.pagingContext];
}

@end
