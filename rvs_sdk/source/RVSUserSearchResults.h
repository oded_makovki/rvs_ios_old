//
//  RVSUserSearchResults.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSPagingContext.h"

@interface RVSUserSearchResults : RVSPagingListBase

@end
