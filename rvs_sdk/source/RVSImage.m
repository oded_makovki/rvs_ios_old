//
//  RVSImage.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSImage.h"

@interface RVSImage ()

@property (nonatomic) NSString *url;
@property (nonatomic) NSNumber<Optional> *width;
@property (nonatomic) NSNumber<Optional> *height;

@end

@implementation RVSImage

JSON_MODEL_KEY_MAPPER(RVSImage)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"url"                     :@"url",
             @"width"                   :@"width",
             @"height"                  :@"height",
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"(%@x%@ %@)", self.width, self.height, self.url];
}

@end
