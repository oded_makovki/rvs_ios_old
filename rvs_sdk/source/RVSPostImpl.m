//
//  RVSPostImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSPostImpl.h"
#import "RVSChannelImpl.h"
#import "RVSProgramImpl.h"
#import "RVSImage.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSCoverImage.h"
#import "RVSImageFromUrlOrArray.h"
#import "RVSMediaContextImpl.h"
#import "RVSPostReferringUserImpl.h"

@interface RVSPost () {
    
    NSObject<RVSAsyncImage> *_coverImage;
}

@property (nonatomic) NSString<Index> *postId;
@property (nonatomic) NSString<Optional> *text;
@property (nonatomic) RVSUser<RVSUser, Optional> *author;
@property (nonatomic) NSNumber<Optional> *creationTime;
@property (nonatomic) RVSChannel<RVSChannel, Optional> *channel;
@property (nonatomic) RVSProgram<RVSProgram, Optional> *program;
@property (nonatomic) RVSCoverImage<Optional> *postCoverImage;
@property (nonatomic) RVSMediaContext<RVSMediaContext, Optional> *mediaContext;
@property (nonatomic) RVSPostReferringUser<Optional,RVSPostReferringUser> *repostingUser;

@end

@implementation RVSPost

JSON_MODEL_KEY_MAPPER(RVSPost)

- (NSObject<RVSAsyncImage>*)coverImage
{
    if (_coverImage)
        return _coverImage;
    
    if (self.postCoverImage)
    {
        _coverImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.postCoverImage.images];
        return _coverImage;
    }
    else
        return nil;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"message"                             :@"text",
             @"likeCount"                           :@"likeCount",
             @"repostCount"                         :@"repostCount",
             @"id"                                  :@"postId",
             @"author"                              :@"author",
             @"creationTime"                        :@"creationTime",
             @"channel"                             :@"channel",
             @"program"                             :@"program",
             @"coverImage"                          :@"postCoverImage",
             @"mediaContext"                        :@"mediaContext",
             @"commentCount"                        :@"commentCount",
             @"latestComments"                      :@"latestComments",
             @"repostingUser"                       :@"repostingUser",
             @"likedByCaller"                       :@"likedByCaller",
             @"repostedByCaller"                    :@"repostedByCaller"
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.text, self.postId];
}

@end
