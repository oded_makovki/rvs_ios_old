//
//  RVSTimeline.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSPagingContext.h"

@class RVSPost;
@class RVSReferringUser;

@protocol RVSTimelineItem <NSObject>
@end

@interface RVSTimelineItem : JSONModel

@property (nonatomic, readonly) RVSPost *post;

@end

@interface RVSTimeline : RVSPagingListBase

@property (nonatomic, readonly) NSArray<RVSTimelineItem, Optional> *timelineItems;

@end
