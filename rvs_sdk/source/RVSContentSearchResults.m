//
//  RVSContentSearchResults.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSContentSearchResults.h"
#import "RVSContentSearchResultImpl.h"

@interface RVSContentSearchResults ()

@property (nonatomic) NSArray<RVSContentSearchResult, Optional> *results;

@end

@implementation RVSContentSearchResults

@end
