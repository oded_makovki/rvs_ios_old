//
//  RVSSyncedDataObject.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"

@class RVSSyncedDataObject;
typedef void (^RVSSyncedDataObjectSync)(RVSSyncedDataObject *obj);

@interface RVSSyncedDataObject : RVSDataObject

+ (void)notifyChangedDataObjectWithId:(NSString*)dataObjectId syncBlock:(RVSSyncedDataObjectSync)syncBlock;

@end

