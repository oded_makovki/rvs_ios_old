//
//  RVSReferringUserList.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSPagingContext.h"

@class RVSReferringUser;
@protocol RVSReferringUser;

@interface RVSReferringUserList : RVSPagingListBase

@property (nonatomic, readonly) NSArray<RVSReferringUser> *referringUsers;

@end
