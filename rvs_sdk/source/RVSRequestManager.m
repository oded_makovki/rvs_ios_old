//
//  RVSRequestManager.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSRequestManager.h"
#import "RVSServiceManager.h"
#import "RVSSdk+Internal.h"
#import "RVSErrors.h"
#import "NSString+rvssdk.h"
#import "RVSLogger.h"
#import "RVSErrors.h"
#import "RVSPromise+Internal.h"
#import "RVSDataObjectImpl.h"

typedef NS_ENUM(NSInteger, RVSRequestManagerHttpMethod)
{
    GET,
    PUT,
    POST,
    DELETE
};

NSString *RVSSdkBaseUrlUserDefaultsKey = @"RVSSdkBaseUrlUserDefaultsKey";
static NSString *RVSSdkBackendApiVersion = @"2.1";
static NSString *RVSSdkClientVersion = @"0.0.0";

@interface RVSRequestSerializer : AFJSONRequestSerializer

+ (NSURLRequest *)URLRequestByAppendingCommonQueryStringTo:(NSURLRequest *)request;

@end

@interface RVSResponseSerializer : AFHTTPResponseSerializer

@end

@implementation RVSRequestManager

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)init
{
    //update client version if available
    NSString* appVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (appVersion && appVersion.length > 0)
        RVSSdkClientVersion = appVersion;
    
    NSString *baseUrlString = [[NSUserDefaults standardUserDefaults] objectForKey:RVSSdkBaseUrlUserDefaultsKey];
    
    if (baseUrlString.length > 0)
    {
        //verify ends with '/'
        if (![baseUrlString hasSuffix:@"/"])
            baseUrlString = [baseUrlString stringByAppendingString:@"/"];
            
        self = [super initWithBaseURL:[NSURL URLWithString:baseUrlString]];
    }
    else //use default
    {
        NSString *defaultBaseUrl = @"https://api-alpha2-s.whipclip.com/v1/";
        self = [super initWithBaseURL:[NSURL URLWithString:defaultBaseUrl]];
    }
    
    self.requestSerializer = [RVSRequestSerializer serializer];
    self.responseSerializer = [RVSResponseSerializer serializer];
    self.responseSerializer.acceptableContentTypes = [self.responseSerializer.acceptableContentTypes setByAddingObject:@"application/x-mpegurl"];
    return self;
}

- (RVSPromise*)requestWithHttpMethod:(RVSRequestManagerHttpMethod)httpMethod method:(NSString*)method parameters:(NSDictionary *)parameters
{
    RVSPromise *promise = [[RVSPromise alloc] init];
    
    __weak typeof(self) weakSelf = self;
    __weak typeof(promise) weakPromise = promise;
    
    void (^successBlock)(NSURLSessionDataTask *task, id responseObject);
    void (^failureBlock)(NSURLSessionDataTask *task, NSError *error);
    
    successBlock = ^(NSURLSessionDataTask *task, id responseObject) {
        [weakPromise fulfillWithValue:responseObject];
    };
    
    failureBlock = ^(NSURLSessionDataTask *task, NSError *error) {
        
        RVSError(@"request failed with error: %@", error);
        
        // if not authenticate request and failed because not authenticated then try to authenticate
        if (![method hasPrefix:@"authenticate"] && [error.domain isEqualToString:RVSErrorDomain] && error.code == RVSErrorCodeSignInRequired && ![RVSSdk sharedSdk].serviceManager.isSignedOut)
        {
            RVSVerbose(@"authentication required, trying to re-authenticate");
            RVSPromise *authenticatePromise = [[RVSSdk sharedSdk].serviceManager authenticate];
            
            [authenticatePromise thenOnMain:^(id result) {
                
                // retry the request
                RVSVerbose(@"authenticated, retrying request %@", method);
                RVSPromise *retryPromise = [weakSelf requestWithHttpMethod:httpMethod method:method parameters:parameters];
                
                [retryPromise thenOnMain:^(id result) {
                    [weakPromise fulfillWithValue:result];
                } error:^(NSError *error) {
                    [weakPromise rejectWithReason:error];
                }];
                
            } error:^(NSError *error) {
                [weakPromise rejectWithReason:error];
            }];
        }
        else
            [weakPromise rejectWithReason:error];
    };
    
    NSURLSessionDataTask *dataTask;
    
    switch (httpMethod)
    {
        case GET:
            dataTask = [self GET:method parameters:parameters success:successBlock failure:failureBlock];
            break;
        case POST:
            dataTask = [self POST:method parameters:parameters success:successBlock failure:failureBlock];
            break;
        case PUT:
            dataTask = [self PUT:method parameters:parameters success:successBlock failure:failureBlock];
            break;
        case DELETE:
            dataTask = [self DELETE:method parameters:parameters success:successBlock failure:failureBlock];
            break;
    }
    
    // promise keeps reference to task so it won't be dealloced before promise
    promise.then(nil, ^id(NSError* error){
        if (promise.isCancelled) {
            [dataTask cancel];
        }
        return error;
    });
    
    return promise;
}

- (RVSPromise*)GET:(NSString *)method parameters:(NSDictionary *)parameters
{
    return [self requestWithHttpMethod:GET method:method parameters:parameters];
}

- (RVSPromise*)PUT:(NSString *)method parameters:(NSDictionary *)parameters
{
    return [self requestWithHttpMethod:PUT method:method parameters:parameters];
}

- (RVSPromise*)POST:(NSString *)method parameters:(NSDictionary *)parameters
{
    return [self requestWithHttpMethod:POST method:method parameters:parameters];
}

- (RVSPromise*)DELETE:(NSString *)method parameters:(NSDictionary *)parameters
{
    return [self requestWithHttpMethod:DELETE method:method parameters:parameters];
}

- (RVSPromise*)getJsonObject:(NSString *)method parameters:(NSDictionary *)parameters objectClass:(Class)objectClass
{
    return [self getJsonObjectProperty:method parameters:parameters objectClass:objectClass propertyName:nil];
}

- (RVSPromise*)getJsonObjectProperty:(NSString *)method parameters:(NSDictionary *)parameters objectClass:(Class)objectClass propertyName:(NSString*)propertyName
{
    RVSPromise *promise = [[RVSPromise alloc] init];
    
    __weak typeof(promise) weakPromise = promise;
    __block RVSPromise *getPromise = [self GET:method parameters:parameters];
    getPromise.then(^id(id result) {
        NSError *error;
        id json = [NSJSONSerialization JSONObjectWithData:result
                                                 options:kNilOptions
                                                   error:&error];
        if (error)
        {
            RVSError(@"failed to deserialize json with error %@", error);
            [weakPromise rejectWithReason:error];
        }
        else
        {
            RVSAssert([objectClass isSubclassOfClass:[JSONModel class]]);
            RVSDataObject *object = [[objectClass alloc] initWithDictionary:json error:&error];
            if (error)
                [weakPromise rejectWithReason:error];
            else
            {
                if (propertyName)
                {
                    id property = [object valueForKey:propertyName];
                    if (property)
                        [weakPromise fulfillWithValue:property];
                    else
                    {
                        RVSError(@"failed to find property %@ in object %@", propertyName, object);
                        [weakPromise rejectWithReason:[JSONModelError errorInvalidDataWithMissingKeys:[NSSet setWithObject:propertyName]]];
                    }
                }
                else
                    [weakPromise fulfillWithValue:object];
            }
        }
        return result;
    }, ^id(NSError* error) {
        [weakPromise rejectWithReason:error];
        return error;
    });
    
    // keep reference to getPromise so it won't be destroyed before our promise 
    promise.then(nil, ^id(NSError* error){
        if (promise.isCancelled) {
            [getPromise cancel];
        }
        return error;
    });
    
    return promise;
}

- (NSURL*)completeURLForMethod:(NSString*)method parameters:(NSDictionary *)parameters
{
    NSURL *requestUrl = [self.baseURL URLByAppendingPathComponent:method];
    NSURLRequest *request = [RVSRequestSerializer URLRequestByAppendingCommonQueryStringTo:[NSURLRequest requestWithURL:requestUrl]];
    
    if (parameters)
    {
        NSMutableString *urlString = [NSMutableString stringWithString:request.URL.absoluteString];
        for (NSString *param in parameters)
            [urlString appendFormat:@"&%@=%@", param, parameters[param]];
        return [NSURL URLWithString:urlString];
    }
    else
        return request.URL;
}

@end

@implementation RVSRequestSerializer

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request
                               withParameters:(NSDictionary *)parameters
                                        error:(NSError *__autoreleasing *)error
{
    // add credentials only if missing
    RVSServiceManager *serviceManager = [RVSSdk sharedSdk].serviceManager;
    NSString *sessionToken = serviceManager.sessionToken;
    if (sessionToken)
    {
        NSString *authField = [NSString stringWithFormat:@"Bearer %@", sessionToken];
        [self setValue:authField forHTTPHeaderField:@"Authorization"];
    }
    else
    {
        [self setValue:nil forHTTPHeaderField:@"Authorization"];
    }
    
    //update request with common query string
    NSURLRequest *updatedRequest = [RVSRequestSerializer URLRequestByAppendingCommonQueryStringTo:request];
    
    return [super requestBySerializingRequest:updatedRequest withParameters:parameters error:error];
}

+ (NSURLRequest *)URLRequestByAppendingCommonQueryStringTo:(NSURLRequest *)request
{
    //API Version Query String
    NSString *queryString = [NSString stringWithFormat:@"apiVersion=%@&clientVersion=%@",
                             RVSSdkBackendApiVersion,
                             RVSSdkClientVersion];
    
    //build urlString
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",
                           [request.URL absoluteString],
                           [request.URL query] ? @"&" : @"?",
                           [queryString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    //copy request
    NSMutableURLRequest *requestCopy = [request mutableCopy];
    
    //updated request copy with updated url
    [requestCopy setURL:[NSURL URLWithString:urlString]];
    
    return requestCopy;
}

@end

@implementation RVSResponseSerializer

- (id)responseObjectForResponse:(NSHTTPURLResponse *)response data:(NSData *)data error:(NSError *__autoreleasing *)error
{
    // super responseObjectForResponse does not return error in most cases but tries to parse response but we want to fail on bad status code so validate directly
    if (![self validateResponse:(NSHTTPURLResponse *)response data:data error:error])
    {
        // if failed to authenticate then we are signed out
        if (response.statusCode == 401 || response.statusCode == 403)
        {
            RVSError(@"failed because not signed in");
            NSError *authenticationError = [[NSError alloc] initWithDomain:RVSErrorDomain code:RVSErrorCodeSignInRequired userInfo:nil];
            if (error)
                *error = authenticationError;
        }

        return nil;
    }
    
    id responseJson = [super responseObjectForResponse:response data:data error:error];
    
    if (!responseJson)
        return nil;
    
    return responseJson;
}

@end
