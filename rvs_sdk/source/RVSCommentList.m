//
//  RVSCommentList.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSCommentList.h"
#import "RVSCommentImpl.h"

@interface RVSCommentList()

@property (nonatomic) NSArray <RVSComment> *comments;

@end

@implementation RVSCommentList

- (NSString*)description
{
    return [NSString stringWithFormat:@"%d comments paging:%@", self.comments.count, self.pagingContext];
}

@end
