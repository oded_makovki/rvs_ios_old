//
//  RVSGetMediaContext.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetMediaContext.h"
#import "RVSLogger.h"
#import "RVSPromise.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSMediaContextImpl.h"

@interface RVSGetMediaContext ()

@property (nonatomic) NSString *channelId;
@property (nonatomic) RVSPromiseHolder *requestPromise;

@end

@implementation RVSGetMediaContext

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithChannel:(NSString*)channelId
{
    self = [super init];
    _channelId = channelId;
    return self;
}

- (RVSPromise*)getMediaContext
{
    RVSVerifyMainThread();
    
    RVSPromise *promise = [[RVSPromise alloc] init];
    __weak RVSPromise *weakPromise = promise;
    
    NSString *method = [NSString stringWithFormat:@"media/contexts?channelId=%@", self.channelId];
    
    self.requestPromise = [[RVSSdk sharedSdk].requestManager POST:method parameters:nil].newHolder;
    self.requestPromise.promise.then(^id(id responseObject) {
        
        NSError *error;
        id json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                  options:kNilOptions
                                                    error:&error];
        if (error)
        {
            RVSError(@"failed to deserialize json error %@", error);
            [weakPromise rejectWithReason:error];
        }
        
        RVSMediaContext *mediaContext = [[RVSMediaContext alloc] initWithDictionary:json error:&error];
        
        if (error)
        {
            RVSError(@"failed to parse created media context with error %@", error);
            [weakPromise rejectWithReason:error];
        }
        else
        {
            RVSInfo(@"created media context %@", mediaContext);
            [weakPromise fulfillWithValue:mediaContext];
        }
        return nil;
    }, ^id(NSError *error) {
        RVSError(@"failed to create media context with error %@", error);
        [weakPromise rejectWithReason:error];
        return nil;
    });
    
    // keep a reference to self so it won't be destyoed before the promise
    promise.then(nil, ^NSError*(NSError* error) {
        if (weakPromise.isCancelled)
            self.requestPromise = nil;
        return nil;
    });
    
    return promise;
}

@end
