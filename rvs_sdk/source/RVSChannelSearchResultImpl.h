//
//  RVSChannelSearchResult.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSChannelSearchResult.h"

@interface RVSChannelSearchResult : RVSDataObject <RVSChannelSearchResult>

@end
