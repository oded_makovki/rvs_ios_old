//
//  RVSChannelImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/22/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSChannel.h"
#import "RVSDataObjectImpl.h"

@interface RVSChannel : RVSDataObject <RVSChannel>
@end
