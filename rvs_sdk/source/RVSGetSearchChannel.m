//
//  RVSGetSearchChannel.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetSearchChannel.h"
#import "RVSChannelSearchResults.h"
#import "RVSLogger.h"
#import "RVSChannelSearchResult.h"
#import "RVSChannel.h"

@interface RVSGetSearchChannel ()
@property (nonatomic) NSMutableArray *responseBuffer;
@property (nonatomic) NSMutableSet *channelIds;
@end

@implementation RVSGetSearchChannel

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithSearchString:(NSString *)searchString
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:searchString forKey:@"freeText"];
    
    self = [super initWithMethod:@"search/channels" params:params responseClass:[RVSChannelSearchResults class] arrayPropertyName:@"results"];
    _channelIds = [NSMutableSet set];
    
    return self;
}

-(id)initWithSearchCallSign:(NSString *)callsign
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:callsign forKey:@"callsign"];
    
    self = [super initWithMethod:@"search/channels" params:params responseClass:[RVSChannelSearchResults class] arrayPropertyName:@"results"];
    _channelIds = [NSMutableSet set];
    
    return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    // if no posts than nothing to filter
    if (responseArray.count)
    {
        // filter out all posts/programs with Ids that we already got before
        for (NSObject <RVSChannelSearchResult> *result in responseArray)
        {
            if ([self.channelIds containsObject:result.channel.channelId])
            {
                RVSInfo(@"ignoring redundent channel %@", result.channel.channelId);
                continue;
            }
            
            [self.channelIds addObject:result.channel.channelId];            
            //add result
            [self.responseBuffer addObject:result];
        }
    }
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
    
}

@end
