//
//  RVSReferringUserList.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSReferringUserList.h"

@interface RVSReferringUserList ()

@property (nonatomic) NSArray<RVSReferringUser> *referringUsers;

@end

@implementation RVSReferringUserList

@end
