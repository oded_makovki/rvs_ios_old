//
//  RVSCreatePost.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

@protocol RVSMediaContext;
@protocol RVSProgram;
@class RVSPromise;

@interface RVSCreatePost : NSObject

- (id)initWithText:(NSString*)text mediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString*)coverImageId;
- (RVSPromise*)createPost;

@end
