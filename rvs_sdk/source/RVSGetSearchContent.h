//
//  RVSGetSearchContent.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetPagingListBase.h"

@interface RVSGetSearchContent : RVSGetPagingListBase

- (id)initWithSearchString:(NSString*)searchString type:(NSString *)type minStartTime:(NSNumber *)minStartTime;

@end
