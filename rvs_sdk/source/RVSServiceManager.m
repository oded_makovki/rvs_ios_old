//
//  RVSServiceManager.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSServiceManager.h"
#import "RVSLogger.h"
#import <AFNetworking/AFNetworkReachabilityManager.h>
#import "NSString+rvssdk.h"
#import <SSKeychain/SSKeychain.h>
#import "RVSErrors.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSSdk+Users.h"

NSString *RVSServiceSignedInNotification = @"RVSServiceSignedInNotification";
NSString *RVSServiceSignInErrorNotification = @"RVSServiceSignInErrorNotification";
NSString *RVSServiceSignedOutNotification = @"RVSServiceSignedOutNotification";
NSString *RVSServiceSignedOutErrorUserInfoKey = @"RVSServiceSignedOutErrorUserInfoKey";
NSString *RVSServiceNetworkStatusChangedNotification = @"RVSServiceNetworkStatusChangedNotification";

@interface RVSServiceManager ()

@property (nonatomic) RVSServiceNetworkStatus networkStatus;
@property (nonatomic) AFNetworkReachabilityManager *reachabilityManager;
@property (nonatomic) RVSPromiseHolder *authenticateRequestPromise;
@property (nonatomic) RVSPromiseHolder *authenticatePromise;
@property (nonatomic) RVSPromiseHolder *configurationRequestPromise;
@property (nonatomic) RVSPromiseHolder *configurationPromise;
@property (nonatomic) BOOL isSignedOut;
@property(nonatomic) NSString *sessionToken;
@property (nonatomic) NSString *userId;

@end

@implementation RVSServiceManager

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithDomain:(NSString *)domain
{
    RVSInfo(@"starting service manager");
    
    self = [super init];
    
    [self setReachability:domain];
    
    if (!self.isSignedOut)
    {
        __weak typeof(self) weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
           
            [weakSelf authenticateAndNotify];
        });
    }
    
    return self;
}

- (void)setReachability:(NSString*)domain
{    
    // this can block the main thread so start on a background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        RVSInfo(@"starting reachability for domain %@", domain);
        
        _reachabilityManager = [AFNetworkReachabilityManager managerForDomain:domain];
        [_reachabilityManager startMonitoring];
        
        __weak RVSServiceManager *weakSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            [weakSelf updateNetworkStatus:YES];
        });

        [_reachabilityManager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            
            RVSVerifyMainThread();
            [weakSelf updateNetworkStatus:NO];
        }];
    });
}

- (void)updateNetworkStatus:(BOOL)firstTime
{
    RVSServiceNetworkStatus networkStatus;
    
    if (!_reachabilityManager.isReachable)
        networkStatus = RVSServiceNetworkStatusNoNetwork;
    else if (self.isSignedOut)
        networkStatus = RVSServiceNetworkStatusDisconnected;
    else
        networkStatus = RVSServiceNetworkStatusConnected;
    
    if (firstTime || networkStatus != _networkStatus)
    {
        _networkStatus = networkStatus;
        RVSInfo(@"network status is %d", networkStatus);
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSServiceNetworkStatusChangedNotification object:[RVSSdk sharedSdk]];
    }
}

- (NSString*)userName
{
    NSString *username = [[NSUserDefaults standardUserDefaults] stringForKey:@"RVSServiceUserName"];
    return username;
}

- (NSString*)password
{
    NSString *user = self.userName;
    
    if (user)
        return [SSKeychain passwordForService:@"RVSServicePassword" account:user];
    else
        return nil;
}

- (NSString*)facebookToken
{
    return [SSKeychain passwordForService:@"RVSServiceFacebookToken" account:@"facebook"];
}

- (NSString*)twitterToken
{
    return [SSKeychain passwordForService:@"RVSServiceTwitterToken" account:@"twitter"];
}

- (NSString*)twitterSecret
{
    return [SSKeychain passwordForService:@"RVSServiceTwitterSecret" account:@"twitter"];
}

- (void)setIsSignedOut:(BOOL)isSignedOut
{
    [[NSUserDefaults standardUserDefaults] setBool:!isSignedOut forKey:@"RVSServiceSignedIn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)isSignedOut
{
    return ![[NSUserDefaults standardUserDefaults] boolForKey:@"RVSServiceSignedIn"];
}

- (void)signInWithUserName:(NSString*)userName password:(NSString*)password
{
    RVSInfo(@"sigining in with user %@", userName);
    
    RVSParameterAssert(![userName isEmpty]);
    RVSParameterAssert(![password isEmpty]);
//    RVSAssert(self.isSignedOut);
    
    [self clearSignInInfo];
    
    // store user since request manager will need those to get my user
    [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"RVSServiceUserName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [SSKeychain setPassword:password forService:@"RVSServicePassword" account:userName];
    
    [self authenticateAndNotify];
}

- (void)signInWithFacebookToken:(NSString*)token
{
    RVSInfo(@"sigining in with facebook token %@", token);
    
    RVSParameterAssert(![token isEmpty]);
//    RVSAssert(self.isSignedOut);
    
    [self clearSignInInfo];
    
    // store token since request manager will need those to get my user
    [SSKeychain setPassword:token forService:@"RVSServiceFacebookToken" account:@"facebook"];
    
    [self authenticateAndNotify];

}

- (void)signInWithTwitterToken:(NSString*)token secret:(NSString *)secret
{
    RVSInfo(@"sigining in with twitter token %@ secret %@", token, secret);
    
    RVSParameterAssert(![token isEmpty]);
    RVSParameterAssert(![secret isEmpty]);
//    RVSAssert(self.isSignedOut);
    
    [self clearSignInInfo];
    
    // store token and secret since request manager will need those to get my user
    [SSKeychain setPassword:token forService:@"RVSServiceTwitterToken" account:@"twitter"];
    [SSKeychain setPassword:secret forService:@"RVSServiceTwitterSecret" account:@"twitter"];
    
    [self authenticateAndNotify];
}

- (void)authenticateAndNotify
{
    [self authenticate];
    __weak RVSServiceManager *weakSelf = self;
    [self.authenticatePromise thenOnMain:^(id result) {
        
        RVSInfo(@"signed in");
        weakSelf.isSignedOut = NO;
        [weakSelf updateNetworkStatus:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSServiceSignedInNotification object:[RVSSdk sharedSdk]];
        
    } error:^(NSError *error) {
        
        RVSError(@"failed to sign in with error: %@", error);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSServiceSignInErrorNotification object:[RVSSdk sharedSdk] userInfo:@{RVSServiceSignedOutErrorUserInfoKey : error}];
    }];
}

- (void)signOut:(NSError*)error
{
    RVSInfo(@"sigining out");
    [self clearSignInInfo];
    [self signOutInternal:nil];
}

- (void)signOutInternal:(NSError*)error
{
    self.isSignedOut = YES;
    
    [self updateNetworkStatus:NO];
    
    NSDictionary *userInfo;
    
    if (error)
        userInfo = @{RVSServiceSignedOutErrorUserInfoKey : error};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSServiceSignedOutNotification object:[RVSSdk sharedSdk] userInfo:userInfo];
}

- (void)clearSignInInfo
{
    // not valid anymore
    self.userId = nil;
    self.sessionToken = nil;
    
    // clear credentials
    NSString *userName = self.userName;
    [SSKeychain deletePasswordForService:@"RVSServiceFacebookToken" account:@"facebook"];
    [SSKeychain deletePasswordForService:@"RVSServiceTwitterToken" account:@"twitter"];
    [SSKeychain deletePasswordForService:@"RVSServiceTwitterSecret" account:@"twitter"];
    [SSKeychain deletePasswordForService:@"RVSServicePassword" account:userName];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"RVSServiceUserName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (RVSPromise*)authenticate
{
    // we synchoronize the singleton authenticate promise by allowing it only from main thread so no threading issues
    RVSVerifyMainThread();
    
    // if already in the middle of authenticating just return the current promise
    if (self.authenticatePromise && self.authenticatePromise.promise.isPending)
        return self.authenticatePromise.promise;

    self.sessionToken = nil;
    
    __weak RVSServiceManager *weakSelf = self;
    self.authenticatePromise = [[RVSPromise alloc] init].newHolder;
    __weak RVSPromise *weakAuthenticatePromise = self.authenticatePromise.promise;
    
    if ([self facebookToken])
    {
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:[self facebookToken], @"facebookToken", nil];
        self.authenticateRequestPromise = [[RVSSdk sharedSdk].requestManager POST:@"authenticate/facebook" parameters:parameters].newHolder;
    }
    else if ([self twitterToken] && [self twitterSecret])
    {
        NSDictionary *parameters = @{@"accessToken" : [self twitterToken],
                                     @"accessTokenSecret" : [self twitterSecret]
                                     };
        
        self.authenticateRequestPromise = [[RVSSdk sharedSdk].requestManager POST:@"authenticate/twitter" parameters:parameters].newHolder;
    }
    else
    {
        NSDictionary *parameters = [NSDictionary dictionaryWithObjectsAndKeys:self.userName, @"username", self.password, @"password", nil];
        self.authenticateRequestPromise = [[RVSSdk sharedSdk].requestManager POST:@"authenticate" parameters:parameters].newHolder;
    }

    [self.authenticateRequestPromise thenOnMain:^(id responseData) {
        
        NSError *error;
        NSDictionary *authenticationDetails = [NSJSONSerialization JSONObjectWithData:responseData
                                                  options:kNilOptions
                                                    error:&error];
        
        if (error)
        {
            [weakAuthenticatePromise rejectWithReason:error];
            return;
        }
        
        weakSelf.sessionToken = authenticationDetails[@"sessionToken"];
        weakSelf.userId = authenticationDetails[@"userId"];
        RVSVerbose(@"authenticated with user %@", weakSelf.userId);
        [weakAuthenticatePromise fulfillWithValue:authenticationDetails];
        
    } error:^(NSError *error) {
        
        RVSError(@"failed to authenticate in with error: %@", error);
        [weakAuthenticatePromise rejectWithReason:error];
        
        // if we were signed in and error is bad credentials then signout
        if ([error.domain isEqualToString:RVSErrorDomain] && error.code == RVSErrorCodeSignInRequired)
        {
            RVSInfo(@"was signed in so signing out");
            [self signOutInternal:error];
        }
    }];
    
    
    __weak RVSPromise *weakAuthenticateRequestPromise = self.authenticateRequestPromise.promise;
    [self.authenticatePromise thenOnMain:nil  error:^(NSError *error) {
        if (weakAuthenticatePromise.isCancelled)
            [weakAuthenticateRequestPromise cancel];
    }];
    
    return self.authenticatePromise.promise;
}

- (RVSPromise *)configuration
{
    // if already in the middle of getting the configuration just return the current promise
    if (self.configurationPromise && self.configurationPromise.promise.isPending)
        return self.configurationPromise.promise;
    
    self.configurationPromise = [[RVSPromise alloc] init].newHolder;
    __weak RVSPromise *weakConfigurationPromise = self.configurationPromise.promise;
    
    self.configurationRequestPromise = [[RVSSdk sharedSdk].requestManager GET:@"configuration" parameters:nil].newHolder;
    
    [self.configurationRequestPromise thenOnMain:^(id responseData) {
        
        NSError *error;
        NSDictionary *configuration = [NSJSONSerialization JSONObjectWithData:responseData
                                                                              options:kNilOptions
                                                                                error:&error];
        
        if (error)
        {
            [weakConfigurationPromise rejectWithReason:error];
            return;
        }
        
        RVSVerbose(@"got configuration: %@", configuration);
        [weakConfigurationPromise fulfillWithValue:configuration];
        
    } error:^(NSError *error) {
        
        RVSError(@"failed to get configuration in with error: %@", error);
        [weakConfigurationPromise rejectWithReason:error];
    }];
    
    
    __weak RVSPromise *weakConfigurationRequestPromise = self.configurationRequestPromise.promise;
    [self.configurationPromise thenOnMain:nil  error:^(NSError *error) {
        if (weakConfigurationRequestPromise.isCancelled)
            [weakConfigurationRequestPromise cancel];
    }];
    
    return self.configurationPromise.promise;
}

@end

