//
//  RVSCoverImage.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"

@protocol RVSCoverImage <NSObject>

@property (nonatomic, readonly) NSArray *images;

@end

@interface RVSCoverImage : RVSDataObject <RVSCoverImage>

@end
