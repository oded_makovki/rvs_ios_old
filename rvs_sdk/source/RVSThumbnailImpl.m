//
//  RVSThumbnailImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSThumbnailImpl.h"
#import "RVSImage.h"
#import "RVSImageFromUrlOrArray.h"

@interface RVSThumbnail () {
    
    NSObject<RVSAsyncImage> *_asyncImage;
}

@property (nonatomic) NSString<Optional> *thumbnailId;
@property (nonatomic) NSNumber<Optional> *startOffsetMs;
@property (nonatomic) NSArray<RVSImage, Optional>*images;

@end

@implementation RVSThumbnail

JSON_MODEL_KEY_MAPPER(RVSThumbnail)

- (NSObject<RVSAsyncImage>*)asyncImage
{
    if (_asyncImage)
        return _asyncImage;
    
    if (self.images)
    {
        _asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:self.images];
        return _asyncImage;
    }
    else
        return nil;
}

- (NSTimeInterval)startOffset
{
    if (!self.startOffsetMs)
        return 0;
    
    return self.startOffsetMs.doubleValue / 1000;
}

+ (NSDictionary*)jsonMappedProperties
{
    return @{
             @"id"                                  :@"thumbnailId",
             @"startOffset"                         :@"startOffsetMs",
             @"representations"                     :@"images"
             };
}

+ (NSArray*)notMappedProperties
{
    return @[@"asyncImage", @"startOffset"];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.thumbnailId, self.startOffsetMs];
}

@end
