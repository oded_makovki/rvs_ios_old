//
//  RVSCreatePost.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSCreatePost.h"
#import "RVSSdk+Internal.h"
#import "RVSRequestManager.h"
#import "RVSLogger.h"
#import "RVSPostImpl.h"
#import "RVSPromise.h"
#import "RVSMediaContextImpl.h"

@interface RVSCreatePost ()

@property (nonatomic) NSString *text;
@property (nonatomic) NSURLSessionDataTask *createPostTask;
@property (nonatomic) RVSMediaContext *mediaContext;
@property (nonatomic) NSTimeInterval startOffset;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSString *coverImageId;

@end

@implementation RVSCreatePost

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

static NSCharacterSet *RVSCreatePostTopicInvalidCharacterSet;
+ (void)initialize
{
    RVSCreatePostTopicInvalidCharacterSet = [[NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_"] invertedSet];
}

- (id)initWithText:(NSString*)text mediaContext:(NSObject<RVSMediaContext>*)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString*)coverImageId
{
    RVSParameterAssert([mediaContext isKindOfClass:[RVSMediaContext class]]);
    
    self = [super init];
    _text = text;
    _mediaContext = (RVSMediaContext*)mediaContext;
    _startOffset = startOffset;
    _duration = duration;
    _coverImageId = coverImageId;
    return self;
}

- (void)dealloc
{
    [_createPostTask cancel];
}

- (NSString*)convertToLegalTopic:(NSString*)str
{
    NSData *data = [str dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *asciiStr = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
    return [[asciiStr componentsSeparatedByCharactersInSet:RVSCreatePostTopicInvalidCharacterSet] componentsJoinedByString:@""];
}

- (RVSPromise*)createPost
{
    RVSVerifyMainThread();
    
    NSNumber *startOffset = [NSNumber numberWithLongLong:self.startOffset * 1000];
    NSNumber *endOffset = [NSNumber numberWithLongLong:(self.startOffset + self.duration) * 1000];
    
    NSMutableDictionary *postInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
    self.text, @"message",
    self.mediaContext.context, @"mediaContext",
    startOffset, @"startOffset",
    endOffset, @"endOffset",
    self.coverImageId, @"coverImageId",
    nil];
    
    NSArray *hashTags = [self hashTagsForPostText];
    
    NSMutableArray *topics = [[NSMutableArray alloc] init];
    
    for (NSString* topic in hashTags)
        [topics addObject:[self convertToLegalTopic:topic]];
    
    if (topics.count)
        postInfo[@"topics"] = topics;
    
    RVSInfo(@"posting a post with info: %@", postInfo);
    
    RVSPromise *promise = [[RVSPromise alloc] init];
    __weak RVSPromise *weakPromise = promise;
    self.createPostTask = [[RVSSdk sharedSdk].requestManager POST:@"posts" parameters:postInfo
                                                          success:^(NSURLSessionDataTask *task, id responseObject)
                           {
                               NSError *error;
                               id json = [NSJSONSerialization JSONObjectWithData:responseObject
                                                                         options:kNilOptions
                                                                           error:&error];
                               if (error)
                               {
                                   RVSError(@"failed to deserialize json error %@", error);
                                   [weakPromise rejectWithReason:error];
                               }

                               RVSPost *createdPost = [[RVSPost alloc] initWithDictionary:json error:&error];
                               
                               if (error)
                               {
                                   RVSError(@"failed to parse created post with error %@", error);
                                   [weakPromise rejectWithReason:error];
                               }
                               else
                               {
                                   RVSInfo(@"created post %@", createdPost);
                                   [weakPromise fulfillWithValue:createdPost];
                               }
                           }
                                                          failure:^(NSURLSessionDataTask *task, NSError *error)
                           {
                               RVSError(@"failed to create post with error %@", error);
                               [weakPromise rejectWithReason:error];
                           }];
    
    // keep a reference to self so it won't be destyoed before the promise
    promise.then(nil, ^NSError*(NSError* error) {
        if (weakPromise.isCancelled)
            [self.createPostTask cancel];
        return nil;
    });
    
    return promise;
}

- (NSArray*)hashTagsForPostText
{
    __block NSMutableArray *hashTags;
    
    // Detect all "#xxx" mention-like strings using the "@\w+" regular expression
    NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
    
    [hashRegex enumerateMatchesInString:self.text options:0 range:NSMakeRange(0, self.text.length)
                             usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         // For each "#xxx" hash found, add a custom link:
         NSString* topic = [[self.text substringWithRange:match.range] substringFromIndex:1]; // get the matched topic, removing the “#"
         if (!hashTags)
             hashTags = [[NSMutableArray alloc] initWithCapacity:1];
         
         [hashTags addObject:topic];
     }];
    
    return hashTags;
}

@end