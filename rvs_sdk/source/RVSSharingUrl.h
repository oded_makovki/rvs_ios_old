//
//  RVSSharingUrl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 5/11/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "JSONModel.h"

@interface RVSSharingUrl : JSONModel

@property (nonatomic) NSURL *url;

@end
