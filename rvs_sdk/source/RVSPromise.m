//
//  RVSPromise.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 3/27/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSPromise+Internal.h"

@interface RVSPromiseHolder ()

@property (nonatomic) RVSPromise *promise;

@end

@interface RVSPromise (theOnMain)

- (instancetype) registerWithExecutionContext:(dispatch_queue_t)target_queue
                         onSuccess:(promise_completionHandler_t)onSuccess
                         onFailure:(promise_errorHandler_t)onFailure
                     returnPromise:(BOOL)returnPromise NS_RETURNS_RETAINED;

@end

@interface RVSPromise ()

@property (nonatomic, strong) RVSPromiseDeallocBlock deallocBlock;

@end

@implementation RVSPromiseHolder

- (id)initWithPromise:(RVSPromise*)promise
{
    self = [super init];
    _promise = promise;
    return self;
}

- (void)dealloc
{
    if (_promise.isPending)
        [_promise cancel];
}

-(RXPromise *)thenOnMain:(promise_noreturnvalue_completionHandler_t)onSuccess error:(promise_noreturnvalue_errorHandler_t)onFailure
{
    promise_completionHandler_t successBlock = ^id(id result) {
        
        if (onSuccess)
            onSuccess(result);
        return nil;
    };
    
    promise_errorHandler_t failureBlock = ^id(NSError *error) {
        
        if (onFailure)
            onFailure(error);
        return nil;
    };
    
    return [self.promise registerWithExecutionContext:dispatch_get_main_queue() onSuccess:successBlock onFailure:failureBlock returnPromise:YES];
}

@end

@implementation RVSPromise

- (id)init
{
    self = [super init];
    return self;
}

- (void)dealloc
{
    if (_deallocBlock)
        _deallocBlock();
}

- (RVSPromiseHolder*)newHolder
{
    return [[RVSPromiseHolder alloc] initWithPromise:self];
}

-(RXPromise *)thenOnMain:(promise_noreturnvalue_completionHandler_t)onSuccess error:(promise_noreturnvalue_errorHandler_t)onFailure
{
    promise_completionHandler_t successBlock = ^id(id result) {
        
        if (onSuccess)
            onSuccess(result);
        return nil;
    };
    
    promise_errorHandler_t failureBlock = ^id(NSError *error) {
        
        if (onFailure)
            onFailure(error);
        return nil;
    };
    
    return [self registerWithExecutionContext:dispatch_get_main_queue() onSuccess:successBlock onFailure:failureBlock returnPromise:YES];
}

@end
