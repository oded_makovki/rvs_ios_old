//
//  RVSPagingContext.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@interface RVSPagingContext : JSONModel

@property (nonatomic, readonly) NSString<Optional> *forwardCookie;

@end

@interface RVSPagingListBase : JSONModel

@property (nonatomic, readonly) RVSPagingContext<Optional> *pagingContext;

@end

