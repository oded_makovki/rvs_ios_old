//
//  RVSReferringUser.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@class RVSUser;
@class RVSPagingContext;

@protocol RVSReferringUser <NSObject>
@end

@interface RVSReferringUser : JSONModel

@property (nonatomic, readonly) RVSUser *user;
@property (nonatomic, readonly) NSNumber *timestamp;

@end