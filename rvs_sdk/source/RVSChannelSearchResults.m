//
//  RVSChannelSearchResults.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSChannelSearchResults.h"
#import "RVSChannelSearchResultImpl.h"

@interface RVSChannelSearchResults ()

@property (nonatomic) NSArray<RVSChannelSearchResult, Optional> *results;

@end

@implementation RVSChannelSearchResults

@end
