//
//  RVSPostReferringUserImpl.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSPostReferringUserImpl.h"
#import "RVSUserImpl.h"

@interface RVSPostReferringUser()
@property (nonatomic) RVSUser <Optional, RVSUser> *user;
@property (nonatomic) NSNumber <Optional> *timestamp;
@end

@implementation RVSPostReferringUser

JSON_MODEL_KEY_MAPPER(RVSPostReferringUser)

+ (NSDictionary*)jsonMappedProperties
{
    return @{@"user"        :@"user",
             @"timestamp"   :@"timestamp"
             };
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"%@ (%@)", self.user, self.timestamp];
}

@end
