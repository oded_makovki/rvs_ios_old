//
//  RVSPostList.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSPostList.h"
#import "RVSPostImpl.h"

@interface RVSPostList ()

@property (nonatomic) NSArray<RVSPost> *posts;

@end

@implementation RVSPostList

- (NSString*)description
{
    return [NSString stringWithFormat:@"%d posts paging:%@", self.posts.count, self.pagingContext];
}

@end
