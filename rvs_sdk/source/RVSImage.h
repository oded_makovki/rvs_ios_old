//
//  RVSImage.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"

@protocol RVSImage <NSObject>

// size is in pixels
@property (nonatomic, readonly) NSString *url;
@property (nonatomic, readonly) NSNumber *width;
@property (nonatomic, readonly) NSNumber *height;

@end

// JSON model forces us to have same name for protocol and class when using NSArray<RVSImage>
@interface RVSImage : RVSDataObject <RVSImage>

@end
