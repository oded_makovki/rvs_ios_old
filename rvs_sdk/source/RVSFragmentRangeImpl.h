//
//  RVSFragmentRangeImpl.h
//  rvs_sdk
//
//  Created by Barak Harel on 5/11/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//
#import "RVSFragmentRange.h"
#import "RVSDataObjectImpl.h"

@interface RVSFragmentRange : RVSDataObject <RVSFragmentRange>
@end
