//
//  RVSGetSearchContent.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetSearchContent.h"
#import "RVSContentSearchResults.h"
#import "RVSLogger.h"
#import "RVSContentSearchResult.h"
#import "RVSPost.h"
#import "RVSProgramExcerpt.h"
#import "RVSProgram.h"

@interface RVSGetSearchContent ()
@property (nonatomic) NSMutableArray *responseBuffer;
@property (nonatomic) NSMutableSet *postIds;
@property (nonatomic) NSMutableSet *programIds;
@end

@implementation RVSGetSearchContent

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithSearchString:(NSString *)searchString type:(NSString *)type minStartTime:(NSNumber *)minStartTime
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:searchString forKey:@"freeText"];
    if (type)
        params[@"type"] = type;
    
    if (minStartTime)
        params[@"minStartTime"] = @([minStartTime longLongValue]); //force long long
    
    self = [super initWithMethod:@"search/content" params:params responseClass:[RVSContentSearchResults class] arrayPropertyName:@"results"];
    
    _postIds = [NSMutableSet set];
    _programIds = [NSMutableSet set];
    
    return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    // if no posts than nothing to filter
    if (responseArray.count)
    {
        // filter out all posts/programs with Ids that we already got before
        for (NSObject <RVSContentSearchResult> *result in responseArray)
        {
            if (result.post)
            {
                if ([self.postIds containsObject:result.post.postId])
                {
                    RVSInfo(@"ignoring redundent post %@", result.post);
                    continue;
                }
                
                [self.postIds addObject:result.post.postId];
            }
            else if (result.programExcerpt)
            {
                if ([self.programIds containsObject:result.programExcerpt.program.programId])
                {
                    RVSInfo(@"ignoring redundent program %@", result.programExcerpt.program);
                    continue;
                }
                
                [self.programIds addObject:result.programExcerpt.program.programId];
            }
            
            //add result
            [self.responseBuffer addObject:result];
        }
    }    
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
    
}

@end
