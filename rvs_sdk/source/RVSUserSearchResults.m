//
//  RVSUserSearchResults.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSUserSearchResults.h"
#import "RVSUserSearchResultImpl.h"

@interface RVSUserSearchResults ()

@property (nonatomic) NSArray<RVSUserSearchResult, Optional> *results;

@end


@implementation RVSUserSearchResults

@end
