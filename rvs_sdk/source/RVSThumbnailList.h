//
//  RVSThumbnailList.h
//  rvs_sdk
//
//  Created by Barak Harel on 5/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@protocol RVSThumbnail;

@protocol RVSThumbnailList

@property (nonatomic, readonly) NSArray <RVSThumbnail> *images;

@end

@interface RVSThumbnailList : JSONModel

@property (nonatomic) NSArray <RVSThumbnail> *images;

@end
