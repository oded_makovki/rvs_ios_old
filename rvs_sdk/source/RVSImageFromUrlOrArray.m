//
//  RVSImageFromUrlOrArray.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSImageFromUrlOrArray.h"
#import <AFNetworking/AFNetworking.h>
#import "RVSLogger.h"
#import "RVSImage.h"
#import "RVSPromise.h"
#import "RVSSdk+Internal.h"

@interface RVSImageFromUrlOrArray () <RVSAsyncImage>

@property (nonatomic) NSArray *images;

@end

@implementation RVSImageFromUrlOrArray

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithImageUrl:(NSString*)imageUrlString
{
    RVSInfo(@"created with image url %@", imageUrlString);
    
    RVSVerifyMainThread();
    RVSParameterAssert(imageUrlString);
    
    self = [super init];
    
    // make it look like a single image in the array
    RVSImage *image = [[RVSImage alloc] initWithDictionary:@{@"url": imageUrlString, @"width" : @0, @"height" : @0} error:nil];
    _images = @[image];
    
    return self;
}

- (id)initWithImageUrlOrArray:(id)imageUrlOrArray
{
    RVSVerifyMainThread();
    RVSParameterAssert(imageUrlOrArray);
    
    if ([imageUrlOrArray isKindOfClass:[NSString class]])
        return [self initWithImageUrl:imageUrlOrArray];
    
     RVSInfo(@"created with image array %@", imageUrlOrArray);
    
    if (![imageUrlOrArray isKindOfClass:[NSArray class]])
    {
        RVSError(@"not an array object: %@", imageUrlOrArray);
        return nil;
    }
    
    NSArray *imageInfos = imageUrlOrArray;
    
    for (id checkDict in imageInfos)
    {
        if (![checkDict isKindOfClass:[RVSImage class]])
        {
            RVSError(@"array must contain RVSImage but contains %@", [checkDict class]);
            return nil;
        }
    }
    
    self = [super init];
    
    if (imageInfos.count > 1)
    {
        NSMutableArray *sortedImageInfos = [NSMutableArray arrayWithArray:imageInfos];
        [sortedImageInfos sortUsingComparator:^NSComparisonResult(RVSImage *a, RVSImage *b) {
            
            NSUInteger aNumber = [a.height unsignedIntegerValue];
            NSUInteger bNumber = [b.height unsignedIntegerValue];
            return aNumber > bNumber;
        }];
        
        _images = sortedImageInfos;
    }
    else
        _images = imageUrlOrArray;
    
    return self;
}

- (NSString*)imageUrlForSize:(CGSize)size
{
    CGFloat pixelHeight = size.height * [[UIScreen mainScreen] scale];
    int count = self.images.count;
    
    if (!count)
    {
        RVSWarn(@"no images found in image array");
        return nil;
    }
    NSString *urlString;
    
    if (self.images.count == 1)
    {
        RVSImage *image = self.images[0];
        urlString = image.url;
    }
    else
    {
        for (RVSImage *image in self.images)
        {
            NSUInteger height = [image.height unsignedIntValue];
            
            if (pixelHeight <= height)
            {
                urlString = image.url;
                break;
            }
        }
        
        if (!urlString)
        {
            RVSImage *image = [self.images lastObject];
            urlString = image.url;
        }
    }
    
    if (!urlString)
    {
        RVSError(@"did not found image URL key");
    }
    
    return urlString;
}

- (RVSPromise*) imageWithSize:(CGSize)size
{
    RVSVerifyMainThread();
    
    RVSPromise *promise = [[RVSPromise alloc] init];
    
    NSString *urlString = [self imageUrlForSize:size];
    
    if (!urlString)
    {
        [promise rejectWithReason:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadURL userInfo:nil]];
        return promise;
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    __weak RVSPromise *weakPromise = promise;
    AFHTTPRequestOperation* imageOperation = [[AFHTTPRequestOperation alloc] initWithRequest:[NSURLRequest requestWithURL:url]];
    imageOperation.responseSerializer = [AFImageResponseSerializer serializer];
    [imageOperation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [weakPromise fulfillWithValue:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
        RVSError(@"failed to load image. url: %@, error: %@", url, error);
        [weakPromise rejectWithReason:error];
    }];
    
    // promise should reference operation to keep it alive as long as promise is alive
    promise.then(nil, ^NSError*(NSError* error) {
        
        if (weakPromise.isCancelled)
            [imageOperation cancel];
        return error;
    });
    
    [[RVSSdk sharedSdk] addBackgroundOperation:imageOperation];
    
    return promise;
}

@end
