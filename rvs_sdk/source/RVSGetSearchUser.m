//
//  RVSGetSearchUser.m
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetSearchUser.h"
#import "RVSUserSearchResults.h"
#import "RVSLogger.h"
#import "RVSUserSearchResult.h"
#import "RVSUser.h"

@interface RVSGetSearchUser ()
@property (nonatomic) NSMutableArray *responseBuffer;
@property (nonatomic) NSMutableSet *userIds;
@end

@implementation RVSGetSearchUser

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithSearchString:(NSString *)searchString
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObject:searchString forKey:@"username"];
    
    self = [super initWithMethod:@"search/users" params:params responseClass:[RVSUserSearchResults class] arrayPropertyName:@"results"];
    _userIds = [NSMutableSet set];
    
    return self;
}

- (void)processList:(NSArray *)responseArray
{
    if (!self.responseBuffer)
        self.responseBuffer = [[NSMutableArray alloc] init];
    
    // if no posts than nothing to filter
    if (responseArray.count)
    {
        // filter out all posts/programs with Ids that we already got before
        for (NSObject <RVSUserSearchResult> *result in responseArray)
        {
            if ([self.userIds containsObject:result.user.userId])
            {
                RVSInfo(@"ignoring redundent user %@", result.user.userId);
                continue;
            }
            
            [self.userIds addObject:result.user.userId];
            //add result
            [self.responseBuffer addObject:result];
        }
    }
    
    //if response buffer don't contain enough items, perform another request so caller won't think its end of list
    if (self.responseBuffer.count < self.count && !self.didReachEnd)
    {
        [self internalNext:self.count - self.responseBuffer.count];
        return;
    }
    
    [self gotResultList:self.responseBuffer];
    self.responseBuffer = nil;
    
}

@end