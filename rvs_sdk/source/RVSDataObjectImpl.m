//
//  RVSDataObjectImpl.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSLogger.h"
#import "RVSSdk+Internal.h"
#import "NSString+rvssdk.h"

@implementation RVSDataObject

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

+ (NSDictionary*)jsonMappedProperties
{
    RVSAssert(false);
}

+(JSONKeyMapper*)keyMapper
{
    RVSAssert(false);
}

+ (NSArray*)notMappedProperties
{
    return nil;
}

- (id)initWithDictionary:(NSDictionary *)dict error:(NSError *__autoreleasing *)err
{
    NSError *error;
    self = [super initWithDictionary:dict error:&error];
    
    if (!self)
    {
        RVSError(@"failed to create json object with error %@", error);
        if (err)
            *err = error;
        return nil;
    }
    
    NSString *indexProperty = [self indexPropertyName];
    
    if (indexProperty)
    {
        if ([[self valueForKey:indexProperty] isEmpty])
        {
            RVSError(@"failed to create json object with empty index property");
            if (err)
                *err = [NSError errorWithDomain:JSONModelErrorDomain code:kJSONModelErrorModelIsInvalid userInfo:@{NSLocalizedDescriptionKey:@"Index property cannot be empty string."}];
            return nil;
        }
    }
    
    return self;
}

- (NSArray*)rvsSdkProperties
{
    NSArray *nonMappedProps = [self.class notMappedProperties];
    NSArray *mappedPrps = [[self.class jsonMappedProperties] allValues];
    
    RVSAssert(mappedPrps || nonMappedProps);
    
    if (!nonMappedProps)
        return mappedPrps;
    
    if (!mappedPrps)
        return nonMappedProps;
    
    NSMutableArray *allProps = [NSMutableArray arrayWithArray:mappedPrps];
    [allProps addObjectsFromArray:nonMappedProps];
    return allProps;
}

@end


