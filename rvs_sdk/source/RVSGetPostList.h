//
//  RVSGetPostList.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSGetPagingListBase.h"

@interface RVSGetPostList : RVSGetPagingListBase

- (id)initWithMethod:(NSString*)method filterResponse:(BOOL)filterResponse;

@end

