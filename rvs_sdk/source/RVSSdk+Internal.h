//
//  RVSSdk_Internal.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/20/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSSdk.h"
#import "RVSLogger.h"

@class RVSPromise;

// notification to allow chaennl detector recroding to stop while playing back
extern NSString *RVSSdkClipPlaybackStartingNotification;
extern NSString *RVSSdkClipPlaybackEndedNotification;

@class RVSServiceManager;
@class RVSRequestManager;

@interface RVSSdk (Internal)

- (void)addBackgroundOperation:(NSOperation*)operation;
- (void)informClipPlayback:(BOOL)playing;
- (RVSPromise*)channelForCallSign:(NSString*)callSign;
- (RVSPromise*)currentProgramForChannelId:(NSString*)channelId;

@property (nonatomic, readonly) RVSServiceManager *serviceManager;
@property (nonatomic, readonly) RVSRequestManager *requestManager;

@end
