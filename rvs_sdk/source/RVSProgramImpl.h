//
//  RVSProgramImpl.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/29/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSProgram.h"

@interface RVSCastMember : RVSDataObject <RVSCastMember>

@end

@interface RVSShow : RVSDataObject <RVSShow>

@end

@interface RVSEpisode : RVSDataObject <RVSEpisode>

@end

@interface RVSProgram : RVSDataObject <RVSProgram>

@end
