//
//  RVSCommentImpl.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSComment.h"

@interface RVSComment : RVSDataObject <RVSComment>

@end
