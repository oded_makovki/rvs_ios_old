//
//  RVSGetSearchUser.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSGetPagingListBase.h"

@interface RVSGetSearchUser : RVSGetPagingListBase

- (id)initWithSearchString:(NSString*)searchString;

@end
