//
//  RVSRequestManager.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>
#import "RVSPromise.h"

@interface RVSRequestManager : AFHTTPSessionManager

- (RVSPromise*)GET:(NSString *)method parameters:(NSDictionary *)parameters;
- (RVSPromise*)PUT:(NSString *)method parameters:(NSDictionary *)parameters;
- (RVSPromise*)POST:(NSString *)method parameters:(NSDictionary *)parameters;
- (RVSPromise*)DELETE:(NSString *)method parameters:(NSDictionary *)parameters;
- (RVSPromise*)getJsonObject:(NSString *)method parameters:(NSDictionary *)parameters objectClass:(Class)objectClass;
- (RVSPromise*)getJsonObjectProperty:(NSString *)method parameters:(NSDictionary *)parameters objectClass:(Class)objectClass propertyName:(NSString*)propertyName;
- (NSURL*)completeURLForMethod:(NSString*)method parameters:(NSDictionary *)parameters;

@end
