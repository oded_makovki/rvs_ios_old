//
//  RVSPostReferringUserImpl.h
//  rvs_sdk
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSDataObjectImpl.h"
#import "RVSPostReferringUser.h"

@interface RVSPostReferringUser : RVSDataObject <RVSPostReferringUser>

@end
