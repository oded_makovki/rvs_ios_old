//
//  RVSImageFromUrlOrArray.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 10/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAsyncImage.h"

@interface RVSImageFromUrlOrArray : NSObject <RVSAsyncImage>

- (id)initWithImageUrl:(NSString*)imageUrlString;

// array should NSArray with RVSImage objects
- (id)initWithImageUrlOrArray:(id)imageUrlOrArray;

@end
