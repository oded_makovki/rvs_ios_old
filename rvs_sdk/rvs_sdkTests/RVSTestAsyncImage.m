//
//  RVSTestAsyncImage.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 12/23/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import <OHHTTPStubs/OHHTTPStubs.h>
#import "RVSImageFromUrlOrArray.h"
#import "RVSTest.h"
#import "RVSImage.h"

static NSData* imageDataWithSize(CGSize size)
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [[UIColor blueColor] CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return UIImageJPEGRepresentation(image, 0.0);
}

SPEC_BEGIN(RVSTestAsyncImage)
describe(@"RVSTestAsyncImage", ^
{
    __block RVSSdk *rvsSdk;
    __block NSMutableDictionary *imagesData;
    __block NSMutableDictionary *imagesInfo;
    __block CGFloat scale;
    
    beforeAll(^
              {
                  scale = [[UIScreen mainScreen] scale];
                  imagesInfo = [NSMutableDictionary dictionaryWithCapacity:3];
                  imagesData = [NSMutableDictionary dictionaryWithCapacity:3];
                  for (NSNumber *height in @[@128, @320, @640])
                      imagesData[height] = imageDataWithSize(CGSizeMake(height.unsignedIntValue / 2, height.unsignedIntValue));
                  
                  [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest *request) {
                      return [request.URL.host isEqualToString:@"test.com"];
                  } withStubResponse:^OHHTTPStubsResponse *(NSURLRequest *request) {
                      NSNumber *height = imagesInfo[request.URL.path];
                      if (height)
                          return  [OHHTTPStubsResponse responseWithData:imagesData[height] statusCode:200 headers:@{@"Content-type" : @"image/jpeg"}];
                      
                      else
                          return [OHHTTPStubsResponse responseWithError:[NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorFileDoesNotExist userInfo:nil]];
                  }];
                  
                  rvsSdk = [RVSTest prepareSdk];
              });
    
    afterAll(^{
        [OHHTTPStubs removeAllStubs];
    });
    
    __block NSError *testError;
    __block UIImage *testImage;
    __block NSMutableArray *imagesArray;
    
    beforeEach(^{
        testError = nil;
        testImage = nil;

        imagesArray = [NSMutableArray arrayWithCapacity:3];
        [imagesInfo removeAllObjects];
        for (NSNumber *height in @[@128, @320, @640])
        {
            // create random URLs to avoid caching
            NSString *imageName = [NSString stringWithFormat:@"/%@.jpg", [[NSUUID UUID] UUIDString]];
            NSString *imageUrl = [NSString stringWithFormat:@"http://test.com%@", imageName];
            NSMutableDictionary * imageInfo = [NSMutableDictionary dictionaryWithDictionary:@{@"url" : imageUrl, @"height" : height, @"width" : [NSNumber numberWithUnsignedInt:height.unsignedIntValue / 2]}];
            [imagesArray addObject:[[RVSImage alloc] initWithDictionary:imageInfo error:nil]];
            imagesInfo[imageName] = height;
        }
    });
    
    it(@"should fail to download with bad URL string", ^
       {
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrl:@"http://test.com/bad.jpg"];
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(0, 0)].newHolder;
           [holder thenOnMain:nil error:^(NSError *error) {
               testError = error;
           }];
           
           [[expectFutureValue(testError) shouldEventually] beNonNil];
       });
    
    it(@"should download with good URL string", ^
       {
           RVSImage *image = imagesArray[0];
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrl:image.url];
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(0, 0)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
           [[theValue(testImage.size.height * scale) should] equal:image.height];
       });
    
    it(@"should fail to download with url key missing", ^
       {
           for (RVSImage *image in imagesArray)
               [image setValue:nil forKey:@"url"];
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:imagesArray];
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(0, 0)].newHolder;
           [holder thenOnMain:nil error:^(NSError *error) {
               testError = error;
           }];
           
           [[expectFutureValue(testError) shouldEventually] beNonNil];
       });
    
    it(@"should ask for height less than smallest and get smallest", ^
       {
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:imagesArray];
           RVSImage *image = imagesArray[0];
           int height = [image.height integerValue] / scale / 2;
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(40, height)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
           [[theValue(testImage.size.height * scale) should] equal:image.height];
       });
    
    it(@"should ask for height same as middle and get middle", ^
       {
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:imagesArray];
           RVSImage *image = imagesArray[1];
           int height = [image. height integerValue] / scale;
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(40, height)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
           [[theValue(testImage.size.height * scale) should] equal:image.height];
       });
    
    it(@"should ask for height more than max and get max", ^
       {
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:imagesArray];
           RVSImage *image = imagesArray[2];
           int height = [image.height integerValue] / scale + 50;
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(40, height)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
           [[theValue(testImage.size.height * scale) should] equal:image.height];
       });
    
    it(@"should use array with one image and get it no matter the size", ^
       {
           RVSImage *image = imagesArray[0];
           NSArray *array = [NSArray arrayWithObject:image];
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:array];
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(40, 9999)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
           [[theValue(testImage.size.height * scale) should] equal:image.height];
       });
    
    it(@"should use array without heights and get some image", ^
       {
           for (RVSImage *image in imagesArray)
               [image setValue:nil forKey:@"height"];
           NSObject<RVSAsyncImage> *asyncImage = [[RVSImageFromUrlOrArray alloc] initWithImageUrlOrArray:imagesArray];
           RVSPromiseHolder *holder = [asyncImage imageWithSize:CGSizeMake(40, 256)].newHolder;
           [holder thenOnMain:^(UIImage *image) {
               testImage = image;
           } error:nil];
           
           [[expectFutureValue(testImage) shouldEventually] beNonNil];
       });
});
SPEC_END
