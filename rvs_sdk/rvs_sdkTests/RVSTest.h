//
//  RVSTest.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 3/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "rvs_sdk_api.h"
#import "rvs_sdk_api helpers.h"

#define RVSTestUserId @"_test"
#define RVSTestUserPassword @"kalisher30"

@interface RVSTest : NSObject

+ (void)printObject:(id)object title:(NSString*)title;
+ (RVSSdk*)prepareSdk;

@end

