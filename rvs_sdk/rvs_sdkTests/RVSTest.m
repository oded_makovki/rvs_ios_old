//
//  RVSTest.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 3/30/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSTest.h"
#import "RVSLogger+Internal.h"
#import "RVSDataObject.h"

@implementation RVSTest

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

+ (void)printObject:(id)object title:(NSString*)title
{
    if ([object conformsToProtocol:@protocol(RVSDataObject)])
    {
        NSLog(@"rvs object:%@", title);
        for (NSString *prop in ((NSObject<RVSDataObject>*)object).rvsSdkProperties)
            [RVSTest printObject:[object valueForKey:prop] title:prop];
        
        return;
    }
    
    if ([object isKindOfClass:[NSArray class]])
    {
        NSLog(@"array:%@", title);
        NSArray *array = object;
        for (int i = 0; i < array.count; i++)
            [RVSTest printObject:array[i] title:[NSString stringWithFormat:@"%d", i]];
    
        return;
    }
    
    NSLog(@"%@:%@", title, object);
}

+ (RVSSdk*)prepareSdk
{
    RVSSdk *sdk = [RVSSdk sharedSdk];
    [RVSLogger setAllLogLevels:@"VERBOSE"];
    return sdk;
}

@end