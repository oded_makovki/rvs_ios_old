//
//  RVSTestObservers.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 1/12/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RVSTest.h"

@interface RVSTestObserversObject : NSObject

@property (nonatomic) NSString *property1;
@property (nonatomic) int property2;

@end

@implementation RVSTestObserversObject

@end

SPEC_BEGIN(RVSTestObservers)
describe(@"RVSTestObservers", ^
{
    __block RVSTestObserversObject *testObject;
    __block __weak RVSTestObserversObject *weakTestObject;
    
    beforeEach(^
               {
                   testObject = [[RVSTestObserversObject alloc] init];
                   weakTestObject = testObject;
               });
    
    context(@"test KVO notifications using observer helper", ^
            {
                it(@"get get initial KVO for a single key path", ^
                   {
                       RVSKeyPathObserver *observer = [testObject newObserverForKeyPath:@"property1" options:NSKeyValueObservingOptionInitial usingBlock:^(id obj, NSDictionary *change) {
                           [weakTestObject.property1 shouldBeNil];
                       }];
                       
                       // should get only initial KVO but not the next one since we release the observer
                       observer = nil;
                       testObject.property1 = @"test";
                   });
                
                it(@"should get new KVO for multiple key paths", ^
                   {
                       RVSKeyPathObserver *observer = [testObject newObserverForKeyPaths:@[@"property1", @"property2"] options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSString *keyPath, NSDictionary *change) {
                           
                           if ([keyPath isEqualToString:@"property1"])
                           {
                               [[weakTestObject.property1 should] equal:@"testvalue"];
                               [[theValue(weakTestObject.property2) should] equal:@0];
                           }
                           
                           if ([keyPath isEqualToString:@"property2"])
                           {
                               [[weakTestObject.property1 should] equal:@"testvalue"];
                               [[theValue(weakTestObject.property2) should] equal:@1];
                           }
                       }];
                       
                       testObject.property1 = @"testvalue";
                       testObject.property2 = 1;
                       
                       // should not get the next KVO since we release the observer
                       observer = nil;
                       testObject.property1 = @"anothervalue";
                   });
            });
    
    context(@"test notifications using notifications observer helper", ^
            {
                it(@"should get only one notification", ^
                   {
                       __block BOOL gotNotification = NO;
                       
                       RVSNotificationObserver *observer = [RVSNotificationObserver newObserverForNotificationWithName:@"RVSTestObservers" object:nil usingBlock:^(NSNotification *notification) {
                           [[theValue(gotNotification) should] beNo];
                           gotNotification = YES;
                       }];
                       
                       
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"RVSTestObservers" object:self];
                       observer = nil;
                       [[NSNotificationCenter defaultCenter] postNotificationName:@"RVSTestObservers" object:self];
                   });
            });
});
SPEC_END

