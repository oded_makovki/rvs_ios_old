//
//  RVSTestChannels.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RVSTest.h"
#import "RVSSdk+Internal.h"

SPEC_BEGIN(RVSTestChannels)
describe(@"RVSTestChannels", ^
{
    __block RVSSdk *rvsSdk;
    
    beforeAll(^
              {
                  rvsSdk = [RVSTest prepareSdk];
              });
    
    context(@"test getting channel and current program from call sign", ^{
        
        __block NSObject<RVSChannel> *testChannel;
        __block NSObject<RVSProgram> *testProgram;
        
        it(@"should receive channel and current program succesfully", ^
           {
               [rvsSdk channelForCallSign:@"CNNHD"].thenOnMain(^id(id channel) {
                   testChannel = channel;
                   return [rvsSdk currentProgramForChannelId:testChannel.channelId];
               }, nil).thenOnMain(^id(id program) {
                   testProgram = program;
                   return nil;
               }, nil);
               
               [[expectFutureValue(testProgram) shouldEventuallyBeforeTimingOutAfter(3)] beNonNil];
               [[testChannel.channelId should] equal:@"us_cnnhd"];
               [RVSTest printObject:testChannel title:@"channel"];
               [RVSTest printObject:testProgram title:@"program"];
           });
    });
});
SPEC_END

