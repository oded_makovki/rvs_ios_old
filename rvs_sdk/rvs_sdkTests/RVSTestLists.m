//
//  RVSTestLists.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/3/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RVSTest.h"
#import "RVSSdk+Posts.h"
#import "RVSPromise.h"
#import "RVSAsyncList.h"

SPEC_BEGIN(RVSTestLists)
describe(@"RVSTestLists", ^
{
    __block RVSSdk *rvsSdk;
    
    beforeAll(^
              {
                  rvsSdk = [RVSTest prepareSdk];
              });
    
    context(@"test getting trending posts", ^{
        
        it(@"should receive channel and current program succesfully", ^
           {
               __block NSMutableArray *trendingPosts;
               __block BOOL endOfList = NO;
               NSObject<RVSAsyncList> *trendingPostsList = [rvsSdk trendingPosts];
               [trendingPostsList next:1].thenOn(dispatch_get_main_queue(), ^id(NSArray *array) {
                   trendingPosts = [[NSMutableArray alloc] init];
                   [trendingPosts addObjectsFromArray:array];
                   return [trendingPostsList next:100];
               }, nil).thenOn(dispatch_get_main_queue(), ^id(NSArray *array) {
                   [trendingPosts addObjectsFromArray:array];
                   endOfList = YES;
                   return nil;
               }, nil);
               
               [[expectFutureValue(theValue(endOfList)) shouldEventuallyBeforeTimingOutAfter(3)] beYes];
               [[theValue(trendingPosts.count) should] beGreaterThan:theValue(0)];
           });
    });
    
    context(@"test search content", ^{
        
        it(@"should receive content search results", ^
           {
               __block NSMutableArray *searchResults;
               __block BOOL endOfList = NO;
               NSNumber * startTime = [NSNumber numberWithLongLong:[[NSDate date] timeIntervalSince1970] * 1000 - 24*3600*1000];
               NSObject<RVSAsyncList> *searchResultsList = [rvsSdk searchContent:@"bob burger" type:RVSSearchTypePost minStartTime:startTime];
               [searchResultsList next:5].thenOn(dispatch_get_main_queue(), ^id(NSArray *array) {
                   searchResults = [[NSMutableArray alloc] init];
                   [searchResults addObjectsFromArray:array];
                   return [searchResultsList next:5];
               }, nil).thenOn(dispatch_get_main_queue(), ^id(NSArray *array) {
                   [searchResults addObjectsFromArray:array];
                   endOfList = YES;
                   return nil;
               }, nil);
               
               [[expectFutureValue(theValue(endOfList)) shouldEventuallyBeforeTimingOutAfter(10)] beYes];
               [[theValue(searchResults.count) should] beGreaterThan:theValue(0)];
           });
    });

});
SPEC_END