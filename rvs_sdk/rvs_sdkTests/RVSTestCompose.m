//
//  RVSTestCompose.m
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 4/1/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

@import MediaPlayer;
#import <Kiwi/Kiwi.h>
#import "RVSTest.h"
#import "RVSSdk+Posts.h"
#import "RVSSdk+Internal.h"
#import "RVSSdk+Channels.h"
#import "RVSChannel.h"
#import "RVSProgram.h"
#import "RVSPromise.h"
#import "RVSAsyncImage.h"
#import "RVSPost.h"
#import "RVSMediaContext.h"

SPEC_BEGIN(RVSTestCompose)
describe(@"RVSTestCompose", ^
{
    __block RVSSdk *rvsSdk;
    
    beforeAll(^{
        rvsSdk = [RVSTest prepareSdk];
        if (rvsSdk.isSignedOut)
            [rvsSdk signInWithUserName:RVSTestUserId password:RVSTestUserPassword];
    });

    context(@"live channel thumbnails and DVR",
            ^{
                it(@"should get dvr clip and thumbnail succesfully", ^
                   {
                       __block NSObject<RVSChannel> *testChannel;
                       __block NSObject<RVSMediaContext> *testMediaContext;
                       __block NSObject<RVSAsyncImage> *testAsyncImage;
                       __block UIImage *testImage;
                       __block MPMoviePlayerController *player;
                       [rvsSdk channelForCallSign:@"CNNHD"].thenOnMain(^id(NSObject<RVSChannel> *channel) {
                           testChannel = channel;
                           return [rvsSdk mediaContextForChannel:testChannel.channelId];
                       }, nil).thenOnMain(^id(NSObject<RVSMediaContext> *mediaContext) {
                           testMediaContext = mediaContext;
                           return [testMediaContext thumbnailsWithStartOffset:0 duration:mediaContext.duration];
                       }, nil).thenOnMain(^id(NSArray *thumbnails) {
                           NSObject<RVSThumbnail>* thumbnail = thumbnails[0];
                           testAsyncImage = thumbnail.asyncImage;
                           return [testAsyncImage imageWithSize:CGSizeMake(0, 0)];
                       }, nil).thenOnMain(^id(UIImage *image) {
                           testImage = image;
                           return [testMediaContext clipUrlWithMediaType:nil startOffset:0 duration:testMediaContext.duration];
                       }, nil).thenOnMain(^id(NSURL *contentUrl) {
                           NSLog(@"content url:%@", contentUrl);
                           player = [[MPMoviePlayerController alloc] initWithContentURL:contentUrl];
                           return nil;
                       }, nil);
                       
                       NSDate *startPlay = [NSDate date];
                       [[expectFutureValue(theValue(player.loadState)) shouldEventuallyBeforeTimingOutAfter(15)] beGreaterThan:theValue(MPMovieLoadStateUnknown)];
                       NSLog(@"time for starting playback %f", [[NSDate date] timeIntervalSinceDate:startPlay]);
                       [[testImage should] beNonNil];
                   });
            });
    
    context(@"posting with CNN with current program",
            ^{
                it(@"should create post and receive the new post succesfully", ^
                   {
                       __block NSObject<RVSChannel> *testChannel;
                       __block NSObject<RVSPost> *testPost;
                        __block NSObject<RVSMediaContext> *testMediaContext;
                       __block MPMoviePlayerController *player;
                       NSString *testText = [NSString stringWithFormat:@"test %@", [NSDate date]];
                       [rvsSdk channelForCallSign:@"CNNHD"].thenOnMain(^id(NSObject<RVSChannel> *channel) {
                           testChannel = channel;
                           return [rvsSdk mediaContextForChannel:testChannel.channelId];
                       }, nil).thenOnMain(^id( NSObject<RVSMediaContext> *mediaContext) {
                           testMediaContext = mediaContext;
                           return [testMediaContext thumbnailsWithStartOffset:0 duration:mediaContext.duration];
                       }, nil).thenOnMain(^id(NSArray *thumbnails) {
                           NSObject<RVSThumbnail>* thumbnail = thumbnails[0];
                           return [rvsSdk createPostWithText:testText mediaContext:testMediaContext startOffset:0 duration:testMediaContext.duration coverImageId:thumbnail.thumbnailId];
                       }, nil).thenOnMain(^id(NSObject<RVSPost>* post) {
                           testPost = post;
                           return [post.mediaContext clipUrlWithMediaType:nil startOffset:0 duration:post.mediaContext.duration];
                       }, nil).thenOnMain(^id(NSURL *contentUrl) {
                           NSLog(@"content url:%@", contentUrl);
                           player = [[MPMoviePlayerController alloc] initWithContentURL:contentUrl];
                           return nil;
                       }, nil);
                       
                       NSDate *startPlay = [NSDate date];
                       [[expectFutureValue(theValue(player.loadState)) shouldEventuallyBeforeTimingOutAfter(15)] beGreaterThan:theValue(MPMovieLoadStateUnknown)];
                       NSLog(@"time for starting playback %f", [[NSDate date] timeIntervalSinceDate:startPlay]);
                       [[testPost.text should] equal:testText];
                       [RVSTest printObject:testPost title:@"post"];
                   });
            });
});
SPEC_END

