//
//  RVSTestUser.m
//  rvs_sdkTests
//
//  Created by Ofer Shem Tov on 10/20/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Kiwi/Kiwi.h>
#import "RVSTest.h"

SPEC_BEGIN(RVSTestUser)
describe(@"RVSTestUser", ^
{
    __block RVSSdk *rvsSdk;
    
    beforeAll(^
    {
        rvsSdk = [RVSTest prepareSdk];
    });
    
    context(@"test getting user from server", ^{
        
        __block NSObject<RVSUser> *testUser;
        __block RVSPromiseHolder *userPromise;
        __block RVSNotificationObserver *signinObserver;
        
        it(@"should receive my user succesfully", ^
        {
            if (rvsSdk.isSignedOut)
                [rvsSdk signInWithUserName:RVSTestUserId password:RVSTestUserPassword];
       
            signinObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedInNotification object:rvsSdk usingBlock:^(NSNotification *notification) {
                userPromise = [rvsSdk userForUserId:[rvsSdk myUserId]].newHolder;
                [userPromise thenOnMain:^(NSObject<RVSUser>* user) {
                    testUser = user;
                } error:nil];
            }];
            
            [[expectFutureValue(testUser) shouldEventuallyBeforeTimingOutAfter(3)] beNonNil];
            [[testUser.name should] equal:@"_test"];
            [RVSTest printObject:testUser title:@"my user"];
        });
    });
});
SPEC_END

