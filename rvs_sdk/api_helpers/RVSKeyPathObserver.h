//
//  RVSKeyPathObserver.h
//  rvs_sdk
//
//  Created by Ofer Shem Tov on 8/23/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

typedef void (^RVSKeyPathObserverKeyPathBlock)(id obj, NSDictionary *change);
typedef void (^RVSKeyPathObserverKeyPathsBlock)(id obj, NSString *keyPath, NSDictionary *change);

@interface RVSKeyPathObserver : NSObject

@end

@interface NSObject (RVSKeyPathObserver)

// using newXXX causes the returned observer to be retained and not auto released
- (RVSKeyPathObserver*)newObserverForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options usingBlock:(RVSKeyPathObserverKeyPathBlock)block;
- (RVSKeyPathObserver*)newObserverForKeyPaths:(NSArray *)keyPaths options:(NSKeyValueObservingOptions)options usingBlock:(RVSKeyPathObserverKeyPathsBlock)block;

@end
