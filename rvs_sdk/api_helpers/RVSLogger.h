//
//  RVSLogger.h
//  rvs_sdk
//
//  Created by ost on 3/9/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <CocoaLumberjack/DDLog.h>

// http://www.mikeash.com/pyblog/friday-qa-2013-05-03-proper-use-of-asserts.html
// assert that will show failure message in crash reporter
// http://www.mikeash.com/pyblog/friday-qa-2013-05-03-proper-use-of-asserts.html
extern const char *__crashreporter_info__;
#define RVSAssert(expression, ...) \
do { \
if(!(expression)) { \
NSString *__MAAssert_temp_string = [NSString stringWithFormat: @"Assertion failure: %s in %s on line %s:%d. %@", #expression, __func__, __FILE__, __LINE__, [NSString stringWithFormat: @"" __VA_ARGS__]]; \
NSLog(@"%@", __MAAssert_temp_string); \
__crashreporter_info__ = [__MAAssert_temp_string UTF8String]; \
abort(); \
} \
} while(0)

#define RVSParameterAssert(condition) RVSAssert((condition), @"Invalid parameter not satisfying: %s", #condition)
#define RVSVerifyMainThread() RVSAssert([NSThread isMainThread], @"Method called using a thread other than main!")

#define RVSVerbose(frmt, ...) DDLogVerbose(frmt, ##__VA_ARGS__)
#define RVSInfo(frmt, ...) DDLogInfo(frmt, ##__VA_ARGS__)
#define RVSWarn(frmt, ...) DDLogWarn(frmt, ##__VA_ARGS__)
#define RVSError(frmt, ...) DDLogError(frmt, ##__VA_ARGS__)

#define RVS_LOGGING_IMPLEMENTATION \
extern int ddLogLevel;

#define RVS_DYANAMIC_LOGGING_IMPLEMENTATION \
static int ddLogLevel = LOG_LEVEL_ERROR; \
+ (int)ddLogLevel \
{ \
    return ddLogLevel; \
} \
+ (void)ddSetLogLevel:(int)logLevel \
{ \
    ddLogLevel = logLevel; \
} \
