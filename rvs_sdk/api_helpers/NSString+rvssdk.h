//
//  NSString+rvssdk.h
//  Playful
//
//  Created by ost on 6/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

@interface NSString (rvssdk)

// return YES if trimmed string is empty
- (BOOL)isEmpty;

// replace spaces with capitals (e.g. "Multi Word String" to "MultiWordString")
- (NSString*)replaceSpacesWithCapitals;

// decode HTML entities
- (NSString*)decodeHtmlEntities;

@end
