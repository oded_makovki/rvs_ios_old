//
//  RVSNotificationObserver.h
//  rvs_sdk
//
//  Created by Barak Harel on 12/1/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import Foundation;

@interface RVSNotificationObserver : NSObject

- (id)initWithName:(NSString *)name object:(id)object usingBlock:(void (^)(NSNotification *))block;

// using newXXX causes the returned observer to be retained and not auto released
+ (instancetype)newObserverForNotificationWithName:(NSString *)name object:(id)object usingBlock:(void (^)(NSNotification *notification))block;

@end
