#!/usr/bin/perl
use Getopt::Long;

my ($CUSTOMIZATION,$IDENTIFIER,$IPAFILE,$HTTPSERVER,$RESOURCEDIR)=('','','','','');
GetOptions('file|f=s' => \$IPAFILE, 'custom|c=s' => \$CUSTOMIZATION, 'resdir|d=s' => \$RESOURCEDIR, 'identifier|i=s' => \$IDENTIFIER, 'server|s=s' => \$HTTPSERVER);
my ($ARTIFACTSDIR,$FILENAME)=$IPAFILE=~/(.*)\/(.*)\.ipa/;

exit 0 if ($FILENAME=~/appstore/);

$PLIST= <<'PLIST';
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>items</key>
	<array>
		<dict>
			<key>assets</key>
			<array>
				<dict>
					<key>kind</key>
					<string>software-package</string>
					<key>url</key>
					<string>[SERVER]/[FILENAME]/[FILENAME].ipa</string>
				</dict>
				<!-- display-image: the icon to display during download .-->
               <dict>
                   <key>kind</key>
                   <string>display-image</string>
                   <!-- optional.  indicates if icon needs shine effect applied. -->
                   <key>needs-shine</key>
                   <true/>
                   <key>url</key>
                   <string>[SERVER]/[FILENAME]/Icon.png</string>
               </dict>
               <!-- full-size-image: the large 512x512 icon used by iTunes. -->
               <dict>
                   <key>kind</key>
                   <string>full-size-image</string>
                   <key>needs-shine</key>
                   <true/>
                   <key>url</key><string>[SERVER]/[FILENAME]/iTunesArtwork.png</string>
               </dict>
			</array>
			<key>metadata</key>
			<dict>
				<key>bundle-identifier</key>
				<string>[IDENTIFIER]</string>
				<key>bundle-version</key>
				<string></string>
				<key>kind</key>
				<string>software</string>
				<key>title</key>
				<string>[CUSTOMIZATION]</string>
			</dict>
		</dict>
	</array>
</dict>
</plist>
PLIST

$PLIST=~s/\[SERVER\]/$HTTPSERVER/gs;
$PLIST=~s/\[IDENTIFIER\]/$IDENTIFIER/s;
$PLIST=~s/\[FILENAME\]/$FILENAME/gs;
$PLIST=~s/\[CUSTOMIZATION\]/$CUSTOMIZATION/s;

open PLIST,">/tmp/$FILENAME.plist";
print PLIST $PLIST;
close PLIST;

`curl -u admin:123456 --digest -X MKCOL $HTTPSERVER/$FILENAME/`;
`curl -u admin:123456 --digest -X PUT -T $RESOURCEDIR/Icon.png $HTTPSERVER/$FILENAME/Icon.png`;
`curl -u admin:123456 --digest -X PUT -T $RESOURCEDIR/iTunesArtwork $HTTPSERVER/$FILENAME/iTunesArtwork.png`;
`curl -u admin:123456 --digest -X PUT -T /tmp/$FILENAME.plist $HTTPSERVER/$FILENAME/$FILENAME.plist`;
`curl -u admin:123456 --digest -X PUT -T "$IPAFILE" $HTTPSERVER/$FILENAME/$FILENAME.ipa`;
`mkdir $ARTIFACTSDIR/Links`;
my $WGET_CMD="curl \'http://tinyurl.com/create.php?alias=$FILENAME&url=itms-services://?action=download-manifest%26url=".$HTTPSERVER.'/'.$FILENAME.'/'.$FILENAME.".plist\'";
my $WGET=`$WGET_CMD`;
my ($LINK)=$WGET=~/data-clipboard-text=\"(.*?)\"/s;
open LINKFILE, ">$ARTIFACTSDIR/Links/".$CUSTOMIZATION."_downloadlink.txt";
print LINKFILE $LINK;
close LINKFILE;
unlink("/tmp/$FILENAME.plst");