//
//  RVSAppCommentListLoadMoreDatObject.m
//  rvs_app2
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListLoadMoreDataObject.h"

@implementation RVSAppCommentListLoadMoreDataObject

-(instancetype)init
{
    self = [super init];
    if (self)
    {
        self.isHidden = YES;
        self.isLoading = NO;
    }
    return self;
}

+(instancetype)data
{
    return [[self alloc] init];
}
@end
