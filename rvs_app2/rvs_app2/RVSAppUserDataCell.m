//
//  RVSAppUserDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDataCell.h"

@implementation RVSAppUserDataCell


-(void)setData:(RVSAppUserDataObject *)data
{
    [super setData:data];
    self.userView.user = data.user;
    self.userView.delegate = data.userViewDelegate;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.userView clear];
}

@end
