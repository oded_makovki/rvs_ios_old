//
//  RVSAppTransitionControllerDelegate.h
//  rvs_app2
//
//  Created by Barak Harel on 3/23/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppTransitionControllerDelegate : NSObject  <UINavigationControllerDelegate>

-(id)initWithNavigationController:(UINavigationController *)navigationController;

@end
