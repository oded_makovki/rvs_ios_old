//
//  RVSAppSearchSuggestionCell.m
//  rvs_app2
//
//  Created by Barak Harel on 3/30/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchSuggestionCell.h"
#import "UIView+NibLoading.h"
#import "UIFont+RVSApplication.h"

@implementation RVSAppSearchSuggestionCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppSearchSuggestionCell];
    }
    
    return self;
}

-(void)initRVSAppSearchSuggestionCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor whiteColor];
    
    self.textLabel.textColor = [UIColor rvsSearchTextColor];
    self.textLabel.font = [UIFont rvsRegularFontWithSize:self.textLabel.font.pointSize];
    
    self.rightLabel.textColor = [UIColor rvsSearchTextColor2];
    self.rightLabel.font = [UIFont rvsRegularFontWithSize:self.rightLabel.font.pointSize];
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.textLabel setText:@""];
    [self.rightLabel setText:@""];
}

-(void)setData:(NSString *)data
{
    [self.textLabel setText:data];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 44);
}


@end
