//
//  RVSAppPostViewDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataObject.h"
#import <rvs_sdk_api.h>
#import "RVSAppViewDelegate.h"

@interface RVSAppPostViewDataObject : RVSAppPostDataObject

@property (nonatomic) NSObject <RVSPost> *post;
@property (nonatomic) NSArray *searchMatches; //of <RVSSearchMatch>

-(id)initWithPost:(NSObject <RVSPost> *)post delegate:(id <RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches;

#pragma mark - Helpers

+(NSArray *)dataObjectsFromPosts:(NSArray *)posts
                delegate:(id<RVSAppViewDelegate>)delegate;

+(instancetype)dataObjectFromPost:(NSObject <RVSPost> *)post
                           delegate:(id<RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches;


@end
