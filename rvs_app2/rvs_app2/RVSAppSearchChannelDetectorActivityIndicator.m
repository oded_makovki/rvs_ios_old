//
//  RVSAppSearchChannelDetectorActivityIndicator.m
//  rvs_app2
//
//  Created by Barak Harel on 4/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchChannelDetectorActivityIndicator.h"
#import "UIColor+HexString.h"

#define BAR_ANIMATION_UPDOWN_DURATION 0.75f
#define BAR_ANIMATION_STOP_DURATION 0.5f
#define BAR_MAX_Y 16

@interface RVSAppSearchChannelDetectorActivityIndicator()
@property (nonatomic) UIView *bar1;
@property (nonatomic) UIView *bar2;
@property (nonatomic) UIView *bar3;

@property (nonatomic) BOOL bar1ShouldGoUp;
@property (nonatomic) BOOL bar2ShouldGoUp;
@property (nonatomic) BOOL bar3ShouldGoUp;

@property (nonatomic) NSTimer *timer;
@end

@implementation RVSAppSearchChannelDetectorActivityIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppSearchChannelDetectorActivityIndicator];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppSearchChannelDetectorActivityIndicator];
}

-(void)initRVSAppSearchChannelDetectorActivityIndicator
{
    self.clipsToBounds = YES;
    self.isDetected = NO;
    
    UIView *back1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, self.frame.size.height)];
    UIView *back2 = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 4, self.frame.size.height)];
    UIView *back3 = [[UIView alloc] initWithFrame:CGRectMake(12, 0, 4, self.frame.size.height)];
    
    back1.backgroundColor = back2.backgroundColor = back3.backgroundColor = [UIColor colorWithRGBValue:0xE6E7EA];
    
    [self addSubview:back1];
    [self addSubview:back2];
    [self addSubview:back3];
    
    self.bar1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 4, 0)];
    self.bar2 = [[UIView alloc] initWithFrame:CGRectMake(6, 0, 4, 0)];
    self.bar3 = [[UIView alloc] initWithFrame:CGRectMake(12, 0, 4, 0)];
    
    [self addSubview:self.bar1];
    [self addSubview:self.bar2];
    [self addSubview:self.bar3];
    
    self.barsColor = [UIColor colorWithRGBValue:0x46A500];
    
    _isAnimating = NO;
}

-(void)setIsDetected:(BOOL)isDetected
{
    _isDetected = isDetected;
    if (_isDetected)
    {
        self.barsColor = [UIColor colorWithRGBValue:0x46A500];
    }
    else
    {
        self.barsColor = [UIColor colorWithRGBValue:0x868C99];
    }
}

-(void)setBarsColor:(UIColor *)barsColor
{
    _barsColor = barsColor;
    [self resetBarColors];
}

-(void)startAnimation
{
    if (_isAnimating)
        return;
    
    _isAnimating = YES;
    
    self.bar1.frame = [self barFrameForBar:self.bar1 y:BAR_MAX_Y];
    self.bar2.frame = [self barFrameForBar:self.bar2 y:BAR_MAX_Y+4];
    self.bar3.frame = [self barFrameForBar:self.bar3 y:BAR_MAX_Y+8];
    
    self.bar1ShouldGoUp = self.bar2ShouldGoUp = self.bar3ShouldGoUp = YES;
    
    if (!self.timer)
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.02 target:self selector:@selector(update) userInfo:nil repeats:YES];
}

-(void)stopAnimation
{
    _isAnimating = NO;
    [self.timer invalidate];
    self.timer = nil;
    
    [UIView animateWithDuration:BAR_ANIMATION_STOP_DURATION animations:^{
        self.bar1.frame = [self barFrameForBar:self.bar1 y:BAR_MAX_Y];
        self.bar2.frame = [self barFrameForBar:self.bar2 y:BAR_MAX_Y];
        self.bar3.frame = [self barFrameForBar:self.bar3 y:BAR_MAX_Y];
    }];
}

-(void)update
{
    self.bar1ShouldGoUp = [self updateBar:self.bar1 upFlag:self.bar1ShouldGoUp];
    self.bar2ShouldGoUp = [self updateBar:self.bar2 upFlag:self.bar2ShouldGoUp];
    self.bar3ShouldGoUp = [self updateBar:self.bar3 upFlag:self.bar3ShouldGoUp];
}

-(BOOL)updateBar:(UIView *)bar upFlag:(BOOL)upFlag
{
    if (upFlag)
    {
        if (bar.frame.origin.y <= 0)
        {
            upFlag = NO;
        }
        else
        {
            bar.frame = [self barFrameForBar:bar y:bar.frame.origin.y - 0.5f];
        }
    }
    else
    {
        if (bar.frame.origin.y >= self.frame.size.height)
        {
            upFlag = YES;
        }
        else
        {
            bar.frame = [self barFrameForBar:bar y:bar.frame.origin.y + 0.5f];
        }
    }
    
    return upFlag;
}

-(CGRect)barFrameForBar:(UIView *)bar y:(CGFloat)y
{
   return CGRectMake(bar.frame.origin.x, y, bar.frame.size.width, self.frame.size.height);
}

-(void)setTempBarsColor:(UIColor *)tempColor
{
    [UIView animateWithDuration:0.3 animations:^{
        self.bar1.backgroundColor = self.bar2.backgroundColor = self.bar3.backgroundColor = tempColor;
    }];
    
    [self performSelector:@selector(resetBarColors) withObject:nil afterDelay:1];
}

-(void)resetBarColors
{
    [UIView animateWithDuration:0.3 animations:^{
        self.bar1.backgroundColor = self.bar2.backgroundColor = self.bar3.backgroundColor = self.barsColor;
    }];
}

@end
