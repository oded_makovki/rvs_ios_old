//
//  RVSAppMainContentType.h
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, RVSAppMainContentType) {
    
    RVSAppMainContentTypeNone = -2,
    RVSAppMainContentTypeProfile = -1,
    RVSAppMainContentTypeTrending = 0,
    RVSAppMainContentTypeRecent = 1,
    RVSAppMainContentTypeNewsFeed = 2,
    RVSAppMainContentTypeLiked = 3,
    RVSAppMainContentTypeTags = 4,
    RVSAppMainContentTypeGenres = 5,
    RVSAppMainContentTypeNotifications = 6,
    RVSAppMainContentTypeAbout = 7,
    
    RVSAppMainContentTypeFeedback = 99,
    RVSAppMainContentTypeSettings = 100,
    //misc
    RVSAppMainContentTypeDevSettings = 666,

};
