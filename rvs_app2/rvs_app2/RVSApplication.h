//
//  RVSAppDelegate.h
//  rvs_app2
//
//  Created by Barak Harel on 3/3/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppClipPlayerView.h"
#import "RVSAppMainViewController.h"
#import "RVSAppCreateCommentFactory.h"

extern NSString *RVSApplicationNetworkStatusDidChangeNotification;
extern NSString *RVSApplicationUserFollowDidChangeNotification;

extern NSString *RVSApplicationWillReturnFromBackgroundNotification;
extern NSString *RVSApplicationDidEnterBackgroundDateUserInfoKey;
extern NSString *RVSApplicationWillEnterForegroundDateUserInfoKey;

extern NSString *RVSApplicationDidCreatePostNotification;
extern NSString *RVSApplicationDidCreatePostNewPostUserInfoKey;
extern NSString *RVSApplicationWillDeletePostNotification;
extern NSString *RVSApplicationWillDeletePostIdUserInfoKey;

extern NSString *RVSApplicationDidCreateCommentNotification;
extern NSString *RVSApplicationDidCreateCommentNewCommentUserInfoKey;
extern NSString *RVSApplicationDidCreateCommentPostIdUserInfoKey;
extern NSString *RVSApplicationDidDeleteCommentNotification;
extern NSString *RVSApplicationDidDeleteCommentDeletedCommentIdUserInfoKey;
extern NSString *RVSApplicationDidDeleteCommentPostIdUserInfoKey;

extern NSString *RVSApplicationDidUpdatePostNotification;
extern NSString *RVSApplicationDidUpdatePostIdUserInfoKey;

extern NSString *RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey;
extern NSString *RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey;
extern NSString *RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey;

/*!
 Recieve post
 @param post post object. Can be nil if error
 @param error error information or nil if no error
 */
typedef void (^RVSAppPostResultBlock)(BOOL completed, NSObject <RVSPost> *post);

@interface RVSApplication : UIResponder <UIApplicationDelegate>

@property (nonatomic) UIWindow *window;
@property (nonatomic, readonly) UIStoryboard *mainStoryboard;

@property (nonatomic, readonly) RVSAppMainViewController *mainViewController;

@property (nonatomic, readonly) BOOL hasNetworkConnection; //to the internet

@property (weak, nonatomic) RVSAppClipPlayerView *activeClipPlayerView;
@property (weak, nonatomic) UIViewController *activeViewController;

@property (nonatomic) BOOL isMirroring;

@property (nonatomic) BOOL shouldDetectChannel;

@property (nonatomic, readonly) BOOL deviceInLandscape;

@property (nonatomic, readonly) NSDateFormatter *utcDateFormatter;
@property (nonatomic, readonly) NSDateFormatter *localDateFormatter;

@property (nonatomic) BOOL inDebugMode;

+ (RVSApplication*)application;

-(void)refreshNetworkStatus;
-(void)refreshNetworkStatusAfterDelay:(NSTimeInterval)delay;

-(BOOL)isMyUserId:(NSString *)userId;

-(void)showComposeWithMediaContext:(NSObject<RVSMediaContext>*)mediaContext
                       defaultText:(NSString *)defaultText;

-(void)showComposeWithChannel:(NSString*)channelId
                              defaultText:(NSString *)defaultText;


-(void)deletePostId:(NSString *)postId;
-(void)deleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId;

-(void)createCommentWithFactory:(RVSAppCreateCommentFactory *)commentFactory;


-(void)showWebViewWithURL:(NSURL *)url;

-(RVSPromise *)showSignInIfNeeded;
-(void)signOut;
-(void)skipSignIn;

-(void)showFeedback;

@end
