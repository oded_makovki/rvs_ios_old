//
//  RVSAppChannelCell.h
//  rvs_app2
//
//  Created by Barak Harel on 4/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppChannelDataObject.h"
#import "RVSAppChannelView.h"

@interface RVSAppChannelCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppChannelView *channelView;
@property (nonatomic) RVSAppChannelDataObject *data;
@end
