//
//  RVSAppClipPlayerViewInlineUI.h
//  rvs_app2
//
//  Created by Barak Harel on 5/18/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppClipPlayerControls.h"

@interface RVSAppClipPlayerInlineControls : RVSAppClipPlayerControls

@end
