//
//  RVSAppComposeViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppComposeViewController.h"
#import "RVSApplication.h"
#import "RVSAppTimelineView.h"
#import "RVSAppClipPlayerView.h"
#import <rvs_sdk_api helpers.h>

#import "RVSAppComposeAccessoryView.h"
#import "UIFont+RVSApplication.h"

NSInteger const kMaxCharacters = 140;

@interface RVSAppComposeViewController () <RVSAppTimelineViewDelegate, RVSAppTimelineViewDataSource, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *postButton;

@property (strong, nonatomic) UIBarButtonItem *cancelButton;
    
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topConstraint;
@property (weak, nonatomic) IBOutlet UIView *topContainer;

@property (weak, nonatomic) IBOutlet UIView *clipControls;
//conatins:
@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (strong, nonatomic) RVSAppClipPlayerView *clipPlayerView;
@property (weak, nonatomic) IBOutlet RVSAppTimelineView *timeline;

//text
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentViewBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *textViewPlaceholder;
@property (weak, nonatomic) IBOutlet UILabel *commentHeaderLabel;

@property (nonatomic) UITapGestureRecognizer *textViewEndTextEditTapGesture;
@property (nonatomic) UISwipeGestureRecognizer *textViewEndTextEditSwipeGesture;
@property (nonatomic) UISwipeGestureRecognizer *leftButtonEndTextEditSwipeGesture;

@property (nonatomic) RVSPromiseHolder *mediaContextPromiseHolder;
@property (nonatomic) NSObject <RVSMediaContext> *mediaContext;
@property (nonatomic) RVSPromiseHolder *thumbnailsPromiseHolder;
@property (nonatomic) NSArray *thumbnails;

//time
@property (nonatomic) BOOL isLive;
@property (nonatomic) NSString *defaultText;

@property (nonatomic) NSTimeInterval selectionStartOffset;
@property (nonatomic) NSTimeInterval selectionDuration;
@property (nonatomic) NSString *selectionThumbnailId;

@property (nonatomic) RVSNotificationObserver *keyboardWillShowObserver;
@property (nonatomic) RVSNotificationObserver *keyboardWillHideObserver;

@property (nonatomic) RVSAppComposeAccessoryView *textAccView;
@property (nonatomic) BOOL inEditVideo;
@end

@implementation RVSAppComposeViewController

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithMediaContextPromise:(RVSPromise *)mediaContextPromise defaultText:(NSString *)defaultText isLive:(BOOL)isLive
{
    NSAssert(mediaContextPromise, @"no mediaContextPromise");
    //default text may be nil
    
    self = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppComposeViewController"];
    if (self)
    {
        self.mediaContextPromiseHolder = [mediaContextPromise newHolder];
        self.defaultText = defaultText;
        self.isLive = isLive;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    self.postButton.enabled = NO; //wait for thumbs
    
    self.textView.keyboardAppearance = UIKeyboardAppearanceDefault;
    self.navigationItem.leftBarButtonItem = self.cancelButton;

    //set time frame
    
    self.textAccView = [[RVSAppComposeAccessoryView alloc] initWithFrame:CGRectMake(0, 0, 320, 34) textView:self.textView];
    
    self.commentHeaderLabel.font = [UIFont rvsLightFontWithSize:12];
    self.textView.font = [UIFont rvsRegularFontWithSize:14];
    self.textViewPlaceholder.font = [UIFont rvsRegularFontWithSize:14];
    
    self.navigationItem.title = NSLocalizedString(@"compose title", nil);
    self.textViewPlaceholder.text = NSLocalizedString(@"compose comment placeholder", nil);
    self.commentHeaderLabel.text = NSLocalizedString(@"compose comment header", nil);
    self.postButton.title = NSLocalizedString(@"compose post button", nil);
    self.cancelButton.title = NSLocalizedString(@"compose cancel button", nil);
    
    self.clipControls.hidden = NO;
    
    self.clipPlayerView = [[RVSAppClipPlayerView alloc] initWithFrame:CGRectMake(0, 0, self.videoContainer.frame.size.width, self.videoContainer.frame.size.height)];
    
    self.clipPlayerView.shouldRewindOnPlay = YES;
    [self.videoContainer addSubview:self.clipPlayerView];
    
    self.timeline.delegate = self;
    self.timeline.dataSource = self;
    self.timeline.delegateTimeintervalSensitivity = 1.0f;
    self.timeline.isCentered = NO;
    
    [self.mediaContextPromiseHolder thenOnMain:^(NSObject <RVSMediaContext> *mediaContext)
    {
        weakSelf.mediaContext = mediaContext;
        weakSelf.thumbnailsPromiseHolder = [mediaContext thumbnailsWithStartOffset:0 duration:mediaContext.duration].newHolder;
        [weakSelf.thumbnailsPromiseHolder thenOnMain:^(NSArray *thumbnailsList) {
            weakSelf.thumbnails = thumbnailsList;
            NSObject <RVSThumbnail> *thumbnail = [weakSelf.thumbnails firstObject];
            if (thumbnail)
            {
                weakSelf.postButton.enabled = YES;
                weakSelf.selectionThumbnailId = thumbnail.thumbnailId;
            }
            
            NSDate *startTime = [NSDate dateWithTimeIntervalSince1970:([mediaContext.start doubleValue] / 1000)];
            [weakSelf.timeline setupWithDuration:mediaContext.duration referenceStartTime:startTime isLive:weakSelf.isLive];
            
        } error:^(NSError *error) {
            //oh no
        }];
        
    } error:^(NSError *error) {
        //oh no
    }];
    
    self.textView.delegate = self;
    self.textView.text = self.defaultText;
    [self textViewDidChange:self.textView];
        
    self.textViewEndTextEditTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endTextEdit)];
    [self.textView addGestureRecognizer:self.textViewEndTextEditTapGesture];
    self.textViewEndTextEditTapGesture.enabled = NO;
    
    self.textViewEndTextEditSwipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(endTextEdit)];
    self.textViewEndTextEditSwipeGesture.direction = UISwipeGestureRecognizerDirectionDown | UISwipeGestureRecognizerDirectionUp;
    [self.textView addGestureRecognizer:self.textViewEndTextEditSwipeGesture];
    self.textViewEndTextEditSwipeGesture.enabled = NO;
    
    [self setLayoutInEditVideo:YES keyboardHeight:0 animated:NO];
}

- (void)cancel:(id)sender
{
//    __weak RVSAppComposeViewController *weakSelf = self;
//    
//    [UIAlertView bk_showAlertViewWithTitle:NSLocalizedString(@"compose cancel title", nil)
//                                message:NSLocalizedString(@"compose cancel message", nil)
//                      cancelButtonTitle:NSLocalizedString(@"alert cancel", nil)
//                      otherButtonTitles:[NSArray arrayWithObject:NSLocalizedString(@"alert ok", nil)]
//                                handler:^(UIAlertView *alertView, NSInteger buttonIndex) {
//        if (buttonIndex == 1) //ok
//        {
//            
//        }
//    }];
    
    [self.clipPlayerView stop];
    [self.delegate composeViewController:self
           didCompleteWithComposePostFactory:nil];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [RVSApplication application].activeViewController = self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    __weak RVSAppComposeViewController *weakSelf = self;
    
    self.keyboardWillShowObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillShowNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillShow:notification];
                                     }];
    
    
    self.keyboardWillHideObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillHideNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillHide:notification];
                                     }];
}

-(void)willMoveToParentViewController:(UIViewController *)parent
{
    if (!parent)
    {
        self.keyboardWillShowObserver = nil;
        self.keyboardWillHideObserver = nil;
    }
    
    [super willMoveToParentViewController:parent];
}

#pragma mark - Methods

- (void)addText:(NSString *)text
{
    if (!text || text.length == 0)
        return;
    
    self.textViewPlaceholder.hidden = YES;
    if (!self.textView.text || [self.textView.text length] == 0) //no text, just set
    {
        self.textView.text = text;
    }
    else if ([self.textView.text hasSuffix:@" "] || [self.textView.text hasSuffix:@"\n"])
    {
        self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, text];
    }
    else
    {
        NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
        
        NSArray *words = [self.textView.text componentsSeparatedByCharactersInSet:whitespace];
        NSString *lastWord = [words lastObject];
        
        if (lastWord && [[text lowercaseString] hasPrefix:[lastWord lowercaseString]]) //found prefix! add leftovers
        {
            self.textView.text = [NSString stringWithFormat:@"%@%@", [self.textView.text substringToIndex:self.textView.text.length - lastWord.length], text];
        }
        else //not prefix - add space and add
        {
            self.textView.text = [NSString stringWithFormat:@"%@ %@", self.textView.text, text];
        }
    }
    
    [self updateCharCount];
}


#pragma mark - Layout

-(void)endTextEdit
{
    [self.view endEditing:NO];
}

-(void)setLayoutInEditVideo:(BOOL)inEditVideo keyboardHeight:(CGFloat)height animated:(BOOL)animated
{
    if (self.inEditVideo == inEditVideo)
        return;
    
    self.inEditVideo = inEditVideo;
    
    if (inEditVideo)
    {
        self.textViewEndTextEditTapGesture.enabled = NO;
        self.textViewEndTextEditSwipeGesture.enabled = NO;
        self.leftButtonEndTextEditSwipeGesture.enabled = NO;
        
        //move view down
        self.commentViewBottomConstraint.constant = 10;
        self.topConstraint.constant = 0;
    }
    else
    {
        self.textViewEndTextEditTapGesture.enabled = YES;
        self.textViewEndTextEditSwipeGesture.enabled = YES;
        self.leftButtonEndTextEditSwipeGesture.enabled = YES;
        
        self.topConstraint.constant = -self.topContainer.frame.size.height; //move view up
        self.commentViewBottomConstraint.constant = height + 10;
    }
    
    if (animated)
    {
        [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionAllowUserInteraction animations:^{
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            if (inEditVideo) //delayed action
            {
                //TBD
            }
        }];
    }    
}

#pragma mark - TextView Delegate

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary *info = [notification userInfo];
    
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    CGFloat height = keyboardFrame.size.height;
    
    [self setLayoutInEditVideo:NO keyboardHeight:height animated:YES];
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    [self setLayoutInEditVideo:YES keyboardHeight:0 animated:YES];
}


-(void)textViewDidChange:(UITextView *)textView
{
    if (self.textView.text.length == 0)
    {
        self.textViewPlaceholder.hidden = NO;
    }
    else
    {
        self.textViewPlaceholder.hidden = YES;
    }
    [self updateCharCount];
}

-(void)updateCharCount
{
    NSString *cleanText = [self cleanText];
    NSInteger charsLeft = kMaxCharacters - cleanText.length;
    self.textAccView.charsCountLabel.text = [NSString stringWithFormat:@"%d", charsLeft];
    
    if (cleanText.length == 0)
    {
        self.textAccView.charsCountLabel.textColor = [UIColor lightGrayColor];
    }
    else if (cleanText.length <= kMaxCharacters)
    {
        self.textAccView.charsCountLabel.textColor = [UIColor darkGrayColor];
    }
    else
    {
        self.textAccView.charsCountLabel.textColor = [UIColor redColor];
    }
}

#pragma mark - RVSAppTimelineView Delegate/Data Source

-(NSObject <RVSAsyncImage> *)timeline:(RVSAppTimelineView *)timeline thumbnailForTimeOffset:(NSTimeInterval)timeOffset
{
    NSObject <RVSThumbnail> *thumbnail = [self thumbnailForOffset:timeOffset];
    return thumbnail.asyncImage;
}

-(NSObject <RVSThumbnail> *)thumbnailForOffset:(NSTimeInterval)timeOffset
{
    if ([self.thumbnails count] == 0)
        return nil;
    
    //index of thumb in list
    NSInteger index = 0;
 
    NSObject <RVSThumbnail> *thumbnail;
    for (int i = 0; i < [self.thumbnails count]; i++)
    {
        thumbnail = self.thumbnails[i];
        if (thumbnail.startOffset <= timeOffset) //valid, save try next
        {
            index = i;
        }
        else // > time offset, break return last found
        {
            break;
        }
    }
    
    return self.thumbnails[index];
}

-(void)timeline:(RVSAppTimelineView *)timeline didChangeWithStartOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration
{
   [self.clipPlayerView stop];
    RVSVerbose(@"Timeline didChangeWithStartOffset start: %.0f, duration: %.0f", startOffset, duration);
    
    self.selectionStartOffset = startOffset;
    self.selectionDuration = duration;
    
    [self.clipPlayerView setClipWithMediaContext:self.mediaContext startOffset:self.selectionStartOffset duration:self.selectionDuration];
    
    [self setSelectionThumbnailFromTimeOffset:startOffset];
}

-(void)timeline:(RVSAppTimelineView *)timeline didSelectFrameWithOffset:(NSTimeInterval)timeOffset
{
    [self setSelectionThumbnailFromTimeOffset:timeOffset];
}

-(void)setSelectionThumbnailFromTimeOffset:(NSTimeInterval)timeOffset
{
    NSObject <RVSThumbnail> *selectionStartThumbnail = [self thumbnailForOffset:timeOffset];
    self.selectionThumbnailId = selectionStartThumbnail.thumbnailId;
    [self.clipPlayerView setCoverImage:selectionStartThumbnail.asyncImage];
}

#pragma mark - Accessors

- (UIBarButtonItem *)cancelButton
{
    if (!_cancelButton)
    {
        _cancelButton = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancel:)];
    }
    
    return _cancelButton;
}

- (NSString *)cleanText
{
    return [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (IBAction)post:(UIBarButtonItem *)sender
{
    NSString *cleanText = [self cleanText];
    
    if (cleanText.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"post problem title" , nil) message:NSLocalizedString(@"post problem no text message", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];

        [alert show];
    }
    else if (cleanText.length > kMaxCharacters)
    {
        NSString *message = NSLocalizedString(@"post problem long text message", nil);
        
        if ([message rangeOfString:@"%d"].location != NSNotFound)           //contains %d ?
            message = [NSString stringWithFormat:message, kMaxCharacters];  //add max chars
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"post problem title" , nil) message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];
        
        [alert show];
    }
    else
    {
        RVSAppCreatePostFactory *postFactory;
        

        postFactory = [[RVSAppCreatePostFactory alloc] initWithPostWithText:cleanText mediaContext:self.mediaContext startOffset:self.selectionStartOffset duration:self.selectionDuration coverImageId:self.selectionThumbnailId];
        
        [self.delegate composeViewController:self didCompleteWithComposePostFactory:postFactory];
    }
}

#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

@end
