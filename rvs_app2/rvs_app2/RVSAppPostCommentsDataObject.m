//
//  RVSAppPostCommentsLinkDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostCommentsDataObject.h"

@implementation RVSAppPostCommentsDataObject

+(instancetype)dataObjectFromPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate
{
    return [[self alloc] initWithPost:post delegate:delegate];
}

-(NSArray *)latestComments
{
    return self.post.latestComments;
}

-(NSInteger)moreCommentsCount
{
    return MAX(0, [self.post.commentCount integerValue] - [self.post.latestComments count]);
}

@end
