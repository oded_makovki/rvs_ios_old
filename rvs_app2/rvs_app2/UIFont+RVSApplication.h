//
//  UIFont+RVSApplication.h
//  rvs_app2
//
//  Created by Barak Harel on 5/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (RVSApplication)

+(UIFont *)rvsExtraLightFontWithSize:(CGFloat)size;
+(UIFont *)rvsLightFontWithSize:(CGFloat)size;
+(UIFont *)rvsRegularFontWithSize:(CGFloat)size;
+(UIFont *)rvsMediumFontWithSize:(CGFloat)size;
+(UIFont *)rvsBoldFontWithSize:(CGFloat)size;

@end
