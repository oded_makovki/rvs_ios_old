//
//  RVSAppCommentListCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListCell.h"
#import "UIView+NibLoading.h"

@interface RVSAppCommentListCell()
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;
@end

@implementation RVSAppCommentListCell


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppCommentListCell];
    }
    
    return self;
}

-(void)initRVSAppCommentListCell
{
    [self loadContentsFromNib];
    [self.contentView addSubview:self.commentView];
    
    self.backgroundColor = [UIColor rvsCommentBackgroundColor];
    self.commentView.minimumLineHeight = 16.0f;
    self.commentView.maximumLineHeight = 30.0f;
    self.commentView.fontPointSize = 14.0f;
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.commentView clear];
}

-(void)setData:(RVSAppCommentListDataObject *)data
{
    [super setData:data];
    self.delegate = data.delegate;
    [self.commentView setComment:data.comment delegate:data.delegate];
    
    __weak typeof(self) weakSelf = self;
    if ([[RVSApplication application] isMyUserId:data.comment.author.userId])
    {
        self.editActions = @[[RVSAppAction actionWithTitle:@"" image:[UIImage imageNamed:@"comment_action_delete"] backgroundColor:[UIColor rvsDarkRedColor] block:^{
            NSLog(@"Delete");
            
            if ([weakSelf.delegate respondsToSelector:@selector(view:didSelectDeleteComment:)])
            {
                [weakSelf.delegate view:weakSelf didSelectDeleteComment:data.comment];
            }
            
        }]];
    }
    else
    {
        self.editActions = @[[RVSAppAction actionWithTitle:@"" image:[UIImage imageNamed:@"comment_action_mention"] backgroundColor:[UIColor rvsCommentActionBackgroundColor] block:^{
            NSLog(@"Mention");
            
            if ([weakSelf.delegate respondsToSelector:@selector(view:didSelectMentionCommentUser:)])
            {
                [weakSelf.delegate view:weakSelf didSelectMentionCommentUser:data.comment.author];
            }
            
        }]];
    }
}

-(CGSize)sizeForData:(RVSAppCommentListDataObject *)data
{
    [self setData:data];
    
    CGSize size = [self.commentView.messageLabel sizeThatFits:CGSizeMake(self.commentView.messageLabel.frame.size.width, CGFLOAT_MAX)];
    
    return CGSizeMake(320, MAX(size.height + 44, 60)); //44 = text:34+text+10, 60 = image:10+40+10;
}

@end
