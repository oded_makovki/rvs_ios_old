//
//  RVSAppDelegate.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSApplication.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

#import "UIColor+RVSApplication.h"

#import "RVSAppSignInViewController.h"
#import "RVSAppComposeViewController.h"
#import "RVSAppPortraitNavigationController.h"
#import "RVSAppPushablePostListViewController.h"

#import "RVSAppConfiguration.h"

#import <SVModalWebViewController.h>
#import <CCAlertView.h>
#import <Crashlytics/Crashlytics.h>
#import <CTFeedbackViewController.h>
#import "TestFlight.h"

extern void UninstallCrashHandlers(BOOL restore);

NSString *RVSAppTestFlightToken = @"RVSAppTestFlightToken";
NSString *RVSAppCrashlyticsApiKey = @"RVSAppCrashlyticsApiKey";

NSString *RVSApplicationNetworkStatusDidChangeNotification = @"RVSApplicationNetworkStatusDidChangeNotification";

NSString *RVSApplicationDidCreatePostNotification = @"RVSApplicationDidCreatePostNotification";
NSString *RVSApplicationDidCreatePostNewPostUserInfoKey = @"RVSApplicationDidCreatePostNewPostUserInfoKey";

NSString *RVSApplicationWillDeletePostNotification = @"RVSApplicationWillDeletePostNotification";
NSString *RVSApplicationWillDeletePostIdUserInfoKey = @"RVSApplicationWillDeletePostIdUserInfoKey";

NSString *RVSApplicationDidCreateCommentNotification = @"RVSApplicationDidCreateCommentNotification";
NSString *RVSApplicationDidCreateCommentNewCommentUserInfoKey = @"RVSApplicationDidCreateCommentNewCommentUserInfoKey";
NSString *RVSApplicationDidCreateCommentPostIdUserInfoKey = @"RVSApplicationDidCreateCommentPostIdUserInfoKey";

NSString *RVSApplicationDidDeleteCommentNotification = @"RVSApplicationDidDeleteCommentNotification";
NSString *RVSApplicationDidDeleteCommentDeletedCommentIdUserInfoKey = @"RVSApplicationDidDeleteCommentDeletedCommentIdUserInfoKey";
NSString *RVSApplicationDidDeleteCommentPostIdUserInfoKey = @"RVSApplicationDidDeleteCommentPostIdUserInfoKey";

NSString *RVSApplicationDidUpdatePostNotification = @"RVSApplicationDidUpdatePostNotification";
NSString *RVSApplicationDidUpdatePostIdUserInfoKey = @"RVSApplicationDidUpdatePostIdUserInfoKey";

NSString *RVSApplicationWillReturnFromBackgroundNotification = @"RVSApplicationWillReturnFromBackgroundNotification";
NSString *RVSApplicationDidEnterBackgroundDateUserInfoKey = @"RVSApplicationDidEnterBackgroundDateUserInfoKey";
NSString *RVSApplicationWillEnterForegroundDateUserInfoKey = @"RVSApplicationWillEnterForegroundDateUserInfoKey";

NSString *RVSApplicationUserFollowDidChangeNotification = @"RVSApplicationUserFollowDidChangeNotification";

NSString *RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey = @"RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey";
NSString *RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey = @"RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey";
NSString *RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey = @"RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey";

typedef void (^RVSAppSignInCompletion)();

@interface RVSApplication() <RVSAppComposeViewControllerDelegate>

@property (nonatomic) RVSAppMainViewController *mainViewController;
@property (nonatomic) RVSAppSignInViewController *signInViewController;

@property (nonatomic) RVSNotificationObserver *signInObserver;
@property (nonatomic) RVSNotificationObserver *signOutObserver;

@property (nonatomic) RVSNotificationObserver *deviceOrientationObserver;
@property (nonatomic) RVSNotificationObserver *connectionObserver;

@property (nonatomic) RVSNotificationObserver *screenDidConnectObserver;
@property (nonatomic) RVSNotificationObserver *screenDidDisconnectbserver;

@property (nonatomic) RVSPromiseHolder *createPostPromise;
@property (nonatomic) RVSPromiseHolder *deletePostPromise;
@property (nonatomic) RVSPromiseHolder *createCommentPromise;
@property (nonatomic) RVSPromiseHolder *deleteCommentPromise;

@property (nonatomic) BOOL hasNetworkConnection;

@property (nonatomic) NSDate *didEnterBackgroundDate;

@property (nonatomic) NSDateFormatter *utcDateFormatter;
@property (nonatomic) NSDateFormatter *localDateFormatter;

@property (nonatomic) RVSPromiseHolder *signInPromise;
@property (nonatomic) NSString *shouldShowPostId;
@end


@implementation RVSApplication

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    NSLog(@"Here, url host: %@, id: %@", [url host], [[url absoluteURL] pathComponents]);

    if ([[[url host] lowercaseString] isEqualToString:@"posts"])
    {
        NSArray *pathComponents = [url pathComponents];
        if ([pathComponents count] > 1)
        {
            NSString *postId = pathComponents[1];
            if ([postId length] > 0)
            {
                if ([self.window.rootViewController isKindOfClass:[RVSAppMainViewController class]])
                {
                    [self showSinglePostWithId:postId];
                }
                else
                {
                    self.shouldShowPostId = postId;
                }
                
                return YES;
            }
        }
    }
    
    return NO;
}

-(void)showSinglePostWithId:(NSString *)postId
{
    UIViewController *vc = [RVSAppCenterNavigationController singlePostViewControllerWithPostId:postId];
    
    [self.mainViewController.centerViewController pushViewController:vc animated:YES];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self initApplication];
    
    //Test Flight
    NSString* testFlightAppToken = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSAppTestFlightToken];
    if (testFlightAppToken && testFlightAppToken.length > 0)
    {
        [TestFlight setOptions:@{TFOptionReportCrashes: @NO}];
        [TestFlight takeOff:testFlightAppToken];
    }
    
    //Crashlytics!
    NSString* crashlyticsApiKey = [[NSBundle mainBundle] objectForInfoDictionaryKey:RVSAppCrashlyticsApiKey];
    if (crashlyticsApiKey && crashlyticsApiKey.length > 0)
    {
        [Crashlytics startWithAPIKey:crashlyticsApiKey];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    __weak RVSApplication *weakSelf = self;
    
    self.signInObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedInNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleServiceSignedIn];
    }];
    
    self.signOutObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedOutNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {

        NSError *error = notification.userInfo[RVSServiceSignedOutErrorUserInfoKey];
        [weakSelf handleServiceSignedOutWithError:error];
    }];
    
    if ([RVSSdk sharedSdk].isSignedOut)
    {
        [self showMainViewController];
        
        //delay the call to prevent unbalanced transition (also nicer ux, hinting that there's already data there).
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [weakSelf showSignInIfNeeded];
        });
    }
    else
    {
        //wait for RVSServiceSignedInNotification, show something untill then
        RVSAppSignInViewController *signInViewController = [self.mainStoryboard instantiateViewControllerWithIdentifier:@"signInViewController"];
        
        self.window.rootViewController = signInViewController;
        [self.window makeKeyAndVisible];
        
        //do after display
        [signInViewController setLastSignInType];
    }
        
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    self.didEnterBackgroundDate = [NSDate date];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    NSDictionary *userInfo = @{RVSApplicationWillEnterForegroundDateUserInfoKey: [NSDate date],
                               RVSApplicationDidEnterBackgroundDateUserInfoKey: self.didEnterBackgroundDate
                               };
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationWillReturnFromBackgroundNotification object:self userInfo:userInfo];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Main View Controller

-(RVSAppMainViewController *)mainViewController
{
    if (!_mainViewController)
    {
        _mainViewController = [self newMainViewController];
    }
    
    return _mainViewController;
}

-(RVSAppMainViewController *)newMainViewController
{
    RVSAppLeftMenuViewController *leftMenuViewController = [self.mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppLeftMenuViewController"];
    return [[RVSAppMainViewController alloc] initWithLeftMenuViewController:leftMenuViewController contentType:RVSAppMainContentTypeTrending];
}

#pragma mark - Application

@synthesize mainStoryboard = _mainStoryboard;

+(RVSApplication *)application
{
    return (RVSApplication *)[UIApplication sharedApplication].delegate;
}

-(void)initApplication
{
    __weak RVSApplication *weakSelf = self;
    
    [[RVSAppConfiguration sharedConfiguration] loadConfiguration];
    
    self.hasNetworkConnection = YES; //lets be positive
    self.shouldDetectChannel = NO;
    self.inDebugMode = NO;
    
    self.deviceOrientationObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIDeviceOrientationDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf handleOrientationChangeNotification:notification];
    }];
    
    self.connectionObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceNetworkStatusChangedNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf handleConnectionStatusChangeNotification:notification];
    }];
    
    
    self.screenDidConnectObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIScreenDidConnectNotification object:Nil usingBlock:^(NSNotification *notification) {
        weakSelf.isMirroring = YES;
    }];
    
    self.screenDidDisconnectbserver = [RVSNotificationObserver newObserverForNotificationWithName:UIScreenDidDisconnectNotification object:Nil usingBlock:^(NSNotification *notification) {
        weakSelf.isMirroring = NO;
    }];
    
    if ([UIScreen screens].count > 1)
    {
        RVSVerbose(@"Mirroring ON");
        self.isMirroring = YES;
    }
    else
    {
        RVSVerbose(@"Mirroring OFF");
        self.isMirroring = NO;
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    // if we don't do that then MPMoviePlayerController fails to play video
    NSError *error = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&error];
    
    if (error)
    {
        RVSError(@"Failed setting audio session playback category %@", error);
    }
}



#pragma mark - Connection Status

-(void)refreshNetworkStatusAfterDelay:(NSTimeInterval)delay
{
    [self performSelector:@selector(refreshNetworkStatus) withObject:nil afterDelay:delay];
}

-(void)refreshNetworkStatus
{
    //cancel previous
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(refreshNetworkStatus) object:nil];
    
    RVSVerbose(@"Refreshed network status: %u",[RVSSdk sharedSdk].networkStatus);
    
    BOOL wasConnected = self.hasNetworkConnection;
    
    if ([RVSSdk sharedSdk].networkStatus == RVSServiceNetworkStatusNoNetwork)
    {
        self.hasNetworkConnection = NO;
    }
    else
    {
        self.hasNetworkConnection = YES;
    }
    
    //notify if needded
    if (self.hasNetworkConnection != wasConnected)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationNetworkStatusDidChangeNotification object:self];
    }
}

-(void)handleConnectionStatusChangeNotification:(NSNotification *)notification
{
    [self refreshNetworkStatus];
}


#pragma mark - Player

-(void)handleOrientationChangeNotification:(NSNotification *)notification
{
    UIDeviceOrientation toInterfaceOrientation = [[UIDevice currentDevice] orientation];
    
    if (toInterfaceOrientation == UIInterfaceOrientationPortrait)
    {
        self.deviceInLandscape = NO;
    }
    else if (toInterfaceOrientation == UIDeviceOrientationLandscapeLeft ||
             toInterfaceOrientation == UIDeviceOrientationLandscapeRight)
    {
        self.deviceInLandscape = YES;
    }
}

-(void)setDeviceInLandscape:(BOOL)deviceInLandscape
{
    _deviceInLandscape = deviceInLandscape;
    
    if (self.activeClipPlayerView)
    {
        if (deviceInLandscape && [self.activeViewController supportedInterfaceOrientations] & UIInterfaceOrientationMaskLandscape)
        {
            [self.activeClipPlayerView setFullscreen:_deviceInLandscape];
        }
        else
        {
            [self.activeClipPlayerView setFullscreen:NO];
        }
    }
    
    //    NSLog(@"deviceInLandscape: %d", deviceInLandscape);
}

#pragma mark - Channel Detection

-(void)updateDetectChannelState
{
    [[RVSSdk sharedSdk] enableChannelDetector:(self.shouldDetectChannel && !self.isMirroring)];
}

-(void)setShouldDetectChannel:(BOOL)shouldDetectChannel
{
    RVSVerbose(@"setShouldDetectChannel: %@", shouldDetectChannel?@"YES":@"NO");
    
    _shouldDetectChannel = shouldDetectChannel;
    [self updateDetectChannelState];
}

#pragma mark - Other

- (UIStoryboard *)mainStoryboard
{
    if (!_mainStoryboard)
    {
        _mainStoryboard = [UIStoryboard storyboardWithName:@"iPhoneMain" bundle:nil];
    }
    return _mainStoryboard;
}

-(void)setIsMirroring:(BOOL)shouldDetectChannel
{
    _isMirroring = shouldDetectChannel;
    [self updateDetectChannelState];
}

-(BOOL)isMyUserId:(NSString *)userId
{
    return [[[RVSSdk sharedSdk].myUserId lowercaseString] isEqualToString:[userId lowercaseString]];
}

-(NSDateFormatter *)utcDateFormatter
{
    if (!_utcDateFormatter)
    {
        _utcDateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
        [_utcDateFormatter setTimeZone:timeZone];
        [_utcDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    }
    
    return _utcDateFormatter;
}

-(NSDateFormatter *)localDateFormatter
{
    if (!_localDateFormatter)
    {
        _localDateFormatter = [[NSDateFormatter alloc] init];
        NSTimeZone *timeZone = [NSTimeZone localTimeZone];
        [_localDateFormatter setTimeZone:timeZone];
        [_localDateFormatter setDateFormat:NSLocalizedString(@"local date formatter format", nil)];
    }
    
    return _localDateFormatter;
}


-(void)setActiveViewController:(RVSAppCommonViewController *)activeViewController
{
    if (_activeViewController == activeViewController)
        return;
    
    [self.activeClipPlayerView stop];
    
    _activeViewController = activeViewController;
    RVSInfo(@"ActiveViewController: %@", activeViewController.navigationItem.title);
}

#pragma mark - Sign In/Out

-(void)signOut
{
    RVSVerbose(@"Do sign out");
    [[RVSSdk sharedSdk] signOut];
}

- (void)handleServiceSignedOutWithError:(NSError *)error
{
    RVSVerbose(@"SignedOut, error: %@", error);
    
    if (self.signInViewController)
        return;
    
    if (!error || (error.domain == RVSErrorDomain && error.code == RVSErrorCodeSignInRequired))
    {
        __weak typeof(self) weakSelf = self;
        self.mainViewController = [self newMainViewController]; //create new instance

        [UIView transitionFromView:self.window.rootViewController.view
                            toView:self.mainViewController.view
                          duration:0.5
                           options:UIViewAnimationOptionTransitionFlipFromLeft | UIViewAnimationOptionAllowAnimatedContent
                        completion:^(BOOL finished)
         {
             weakSelf.window.rootViewController = weakSelf.mainViewController; //new instance
             
             //delay the call to prevent unbalanced transition (also nicer ux, hinting that there's already data there).
             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                 [weakSelf showSignInIfNeeded];
             });
         }];
    }
}

-(RVSPromise *)showSignInIfNeeded
{
    if (![RVSSdk sharedSdk].isSignedOut)
    {
        RVSPromise *signInSuccessPromise = [[RVSPromise alloc] init];
        [signInSuccessPromise fulfillWithValue:nil];
        
        return signInSuccessPromise;
    }
    
    //signed out:
    if (self.signInViewController)
    {
        return self.signInViewController.signInViewControllerSuccessPromise;
    }
    else
    {
        //show sign in, return its promise
        self.signInViewController = [self.mainStoryboard instantiateViewControllerWithIdentifier:@"signInViewController"];
        [self.mainViewController presentViewController:self.signInViewController animated:YES completion:nil];
        
        return self.signInViewController.signInViewControllerSuccessPromise;
    }
}

-(void)skipSignIn
{
    [self showMainViewController];
}

- (void)handleServiceSignedIn
{
    RVSVerbose(@"SignedIn");
    [self showMainViewController];
}

-(void)showMainViewController
{
    if (self.signInViewController) //modal sign in, dismiss.
    {
        RVSAppSignInViewController *signInViewController = self.signInViewController;
        self.signInViewController = nil;
        
        [signInViewController.presentingViewController dismissViewControllerAnimated:YES completion:^{
            [signInViewController wasDismissed];
        }];
    }
    else
    {
        //sign in vc as root or no root, switch to main vc.
        self.window.rootViewController = self.mainViewController;
        [self.window makeKeyAndVisible];
    }
    
    if (self.shouldShowPostId)
    {
        [self showSinglePostWithId:self.shouldShowPostId];
        self.shouldShowPostId = nil;
    }
}

#pragma mark - Main Delegate

-(void)deletePostId:(NSString *)postId
{
    self.deletePostPromise = [[RVSSdk sharedSdk] deletePost:postId].newHolder;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationWillDeletePostNotification object:self userInfo:@{RVSApplicationWillDeletePostIdUserInfoKey: postId}];
    
    [self.deletePostPromise thenOnMain:^(id result) {
        RVSInfo(@"Post id %@ deleted successfully", postId);
    } error:^(NSError *error) {
        RVSInfo(@"Error while deleteing post %@: %@", postId, [error localizedDescription]);
    }];
}

-(void)deleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId
{
    self.deleteCommentPromise = [[RVSSdk sharedSdk] deleteComment:commentId fromPost:postId].newHolder;

    [self.deleteCommentPromise thenOnMain:^(id result) {
        RVSInfo(@"Comment id %@ of post id %@ deleted successfully", commentId, postId);
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationDidDeleteCommentNotification object:nil userInfo:@{RVSApplicationDidDeleteCommentDeletedCommentIdUserInfoKey: commentId, RVSApplicationDidDeleteCommentPostIdUserInfoKey: postId }];
        
    } error:^(NSError *error) {
        RVSInfo(@"Error while deleteing post %@: %@", postId, [error localizedDescription]);
    }];
}

-(void)showComposeWithMediaContext:(NSObject<RVSMediaContext> *)mediaContext defaultText:(NSString *)defaultText
{
    [self.activeClipPlayerView stop];
    
    __weak typeof(self) weakSelf = self;
    self.signInPromise = [self showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        
        RVSPromise *mediaContextPromise = [[RVSPromise alloc] init];
        [mediaContextPromise fulfillWithValue:mediaContext];
        [weakSelf internalShowComposeWithMediaContextPromise:mediaContextPromise defaultText:defaultText isLive:NO];
        
    } error:^(NSError *error) {
        //
    }];
}

-(void)showComposeWithChannel:(NSString *)channelId defaultText:(NSString *)defaultText
{
    [self.activeClipPlayerView stop];
    
    __weak typeof(self) weakSelf = self;    
    self.signInPromise = [self showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        
        RVSPromise *mediaContextPromise = [[RVSSdk sharedSdk] mediaContextForChannel:channelId];
        [weakSelf internalShowComposeWithMediaContextPromise:mediaContextPromise defaultText:defaultText isLive:YES];
        
    } error:^(NSError *error) {
        //
    }];
}

-(void)internalShowComposeWithMediaContextPromise:(RVSPromise *)mediaContextPromise defaultText:(NSString *)defaultText isLive:(BOOL)isLive
{
    RVSAppComposeViewController *compose = [[RVSAppComposeViewController alloc] initWithMediaContextPromise:mediaContextPromise defaultText:defaultText isLive:isLive];
    compose.delegate = self;
    RVSAppPortraitNavigationController *nc = [[RVSAppPortraitNavigationController alloc] initWithRootViewController:compose];
    nc.navigationBar.translucent = NO;
    [self.mainViewController presentViewController:nc animated:YES completion:nil];
}


-(void)showWebViewWithURL:(NSURL *)url
{
    [self.activeClipPlayerView stop];
    
    SVModalWebViewController *webViewController = [[SVModalWebViewController alloc] initWithURL:url];
    webViewController.navigationBar.translucent = NO;
    webViewController.navigationBar.barTintColor = [UIColor blackColor];
    
    //add line to nav bar
    UIImageView *line = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"greyLine"]];
    [line setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    [webViewController.view addSubview:line];
    [webViewController.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[line]-0-|" options:0 metrics:nil views:@{@"line":line}]];
    
    [webViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:1]];
    
    [webViewController.view addConstraint:[NSLayoutConstraint constraintWithItem:line attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:webViewController.navigationBar attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
    
    
    webViewController.modalPresentationStyle = UIModalPresentationFullScreen;
    webViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    //set text style
    [webViewController.topViewController.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName,nil]];
    
    [self.mainViewController presentViewController:webViewController animated:YES completion:nil];
    
    //override color (after presenting)
    [webViewController.navigationBar setTintColor:[UIColor rvsTintColor]];
}

#pragma mark - RVSAppComposeViewController Delegate

-(void)composeViewController:(RVSAppComposeViewController *)sender didCompleteWithComposePostFactory:(RVSAppCreatePostFactory *)postFactory
{
    __weak RVSApplication *weakSelf = self;
    
    //dismiss
    [sender.presentingViewController dismissViewControllerAnimated:YES completion:^{
        if (postFactory)
        {
            //post
            [weakSelf.mainViewController.centerViewController showPostingProgress];
            
            //wait with post
            double delayInSeconds = 0.3;
            dispatch_time_t delay = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
            
            dispatch_after(delay, dispatch_get_main_queue(), ^(void){
                [weakSelf tryPostingWithFactory:postFactory];
            });
        }
    }];
}

-(void)tryPostingWithFactory:(RVSAppCreatePostFactory *)postFactory
{
    //keep pointer
    self.createPostPromise = [postFactory createPost].newHolder;
    
    __weak RVSApplication *weakSelf = self;
    
    [self.createPostPromise thenOnMain:^(NSObject <RVSPost> *post){
        
        [weakSelf.mainViewController.centerViewController showPostResultSuccess];
        
        //notify
        NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:post,RVSApplicationDidCreatePostNewPostUserInfoKey, nil];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationDidCreatePostNotification object:weakSelf userInfo:userInfo];
        
    } error:^(NSError *error) {
        
        //show progress error
        [weakSelf.mainViewController.centerViewController showPostResultFailed];
        
        CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:NSLocalizedString(@"create post error title",nil) message:NSLocalizedString(@"create post error message",nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"create post error retry",nil) block:^{
            [weakSelf tryPostingWithFactory:postFactory];
        }];
        [alertView addButtonWithTitle:NSLocalizedString(@"create post error cancel",nil) block:^{
            [weakSelf.mainViewController.centerViewController hidePostingProgress];
        }];
        
        [alertView show];
    }];
}

-(void)createCommentWithFactory:(RVSAppCreateCommentFactory *)commentFactory
{
    __weak typeof(self) weakSelf = self;
    self.signInPromise = [self showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        [weakSelf tryCommentingWithFactory:commentFactory];
    } error:nil];
}

-(void)tryCommentingWithFactory:(RVSAppCreateCommentFactory *)commentFactory
{
    //keep pointer
    self.createCommentPromise = [commentFactory createComment].newHolder;
    
    __weak RVSApplication *weakSelf = self;
    
    [self.createCommentPromise thenOnMain:^(NSObject <RVSComment> *comment){
        
//        [weakSelf.mainViewController.centerViewController showPostResultSuccess];
        
        //notify
        NSDictionary *userInfo = @{
                                   RVSApplicationDidCreateCommentNewCommentUserInfoKey: comment,
                                   RVSApplicationDidCreateCommentPostIdUserInfoKey: commentFactory.postId
                                   };
        
        [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationDidCreateCommentNotification object:weakSelf userInfo:userInfo];
        
    } error:^(NSError *error) {
        
        //show progress error
//        [weakSelf.mainViewController.centerViewController showPostResultFailed];
        
        CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:NSLocalizedString(@"create comment error title",nil) message:NSLocalizedString(@"create comment error message",nil)];
        [alertView addButtonWithTitle:NSLocalizedString(@"create comment error retry",nil) block:^{
            [weakSelf tryCommentingWithFactory:commentFactory];
        }];
        [alertView addButtonWithTitle:NSLocalizedString(@"create comment error cancel",nil) block:^{
//            [weakSelf.mainViewController.centerViewController hidePostingProgress];
        }];
        
        [alertView show];
    }];
}

-(void)showFeedback
{
    CTFeedbackViewController *feedbackViewController = [CTFeedbackViewController controllerWithTopics:CTFeedbackViewController.defaultTopics localizedTopics:CTFeedbackViewController.defaultLocalizedTopics];
    feedbackViewController.toRecipients = @[NSLocalizedString(@"feedback email address", nil)];
    feedbackViewController.useHTML = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:feedbackViewController];
    [self.mainViewController presentViewController:navigationController animated:YES completion:nil];
}


@end
