//
//  RVSAppPortraitNavigationController.m
//  rvs_app
//
//  Created by Barak Harel on 11/26/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPortraitNavigationController.h"
#import "RVSAppCardTransitionController.h"

@interface RVSAppPortraitNavigationController () <UINavigationControllerDelegate>

@end

@implementation RVSAppPortraitNavigationController


-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.delegate = self;
    }
    return self;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
