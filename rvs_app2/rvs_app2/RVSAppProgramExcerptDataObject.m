//
//  RVSAppProgramExcerptDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppProgramExcerptDataObject.h"

@implementation RVSAppProgramExcerptDataObject

-(id)init
{
    self = [super init];
    if (self)
    {
        self.programExcerpt = nil;
        self.programExcerptViewDelegate = nil;
    }
    return self;
}

-(id)initWithProgramExcerpt:(NSObject<RVSProgramExcerpt> *)programExcerpt programExcerptViewDelegate:(id<RVSAppProgramExcerptViewDelegate>)programExcerptViewDelegate searchMatches:(NSArray *)searchMatches
{
    self = [self init];
    if (self)
    {
        self.programExcerpt = programExcerpt;
        self.programExcerptViewDelegate = programExcerptViewDelegate;
        self.searchMatches = searchMatches;
    }
    
    return self;
}

+(RVSAppProgramExcerptDataObject *)dataObjectFromProgramExcerpt:(NSObject<RVSProgramExcerpt> *)programExcerpt programExcerptViewDelegate:(id<RVSAppProgramExcerptViewDelegate>)programExcerptViewDelegate searchMatches:(NSArray *)searchMatches
{
    RVSAppProgramExcerptDataObject *data = [[RVSAppProgramExcerptDataObject alloc] initWithProgramExcerpt:programExcerpt programExcerptViewDelegate:programExcerptViewDelegate searchMatches:searchMatches];
    
    return data;
}

@end
