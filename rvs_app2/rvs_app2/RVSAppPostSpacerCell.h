//
//  RVSAppSpacerCell.h
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppPostSpacerDataObject.h"

@interface RVSAppPostSpacerCell : RVSAppDataCell
@property (nonatomic) RVSAppPostSpacerDataObject *data;
@end
