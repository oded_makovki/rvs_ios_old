//
//  RVSAppMainViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCenterNavigationController.h"
#import <rvs_sdk_api helpers.h>

#import "RVSAppPushablePostListViewController.h"
#import "RVSAppAboutViewController.h"

#import "UIColor+RVSApplication.h"
#import "RVSApplication.h"

#import "RVSAppCenterNavigationControllerDelegate.h"
#import "RVSAppHeaderView.h"

@interface RVSAppCenterNavigationController()
@property (nonatomic) RVSAppCenterNavigationControllerDelegate *navManager;
@property (nonatomic) RVSNotificationObserver *networkStatusDidChangeObserver;

@property (nonatomic) UIImageView *postProgressViewImageView;
@property (nonatomic) RVSAppHeaderView *postProgressView;
@property (nonatomic) RVSAppHeaderView *networkStatusBar;
@end

@implementation RVSAppCenterNavigationController


-(id)initWithRootContentType:(RVSAppMainContentType)contentType leftBarButtonItem:(UIBarButtonItem *)item
{
    UIViewController *vc = [RVSAppCenterNavigationController viewControllerForContentType:contentType];
    vc.navigationItem.leftBarButtonItem = item;
    
    self = [super initWithRootViewController:vc];
    if (self)
    {
        self.navManager = [[RVSAppCenterNavigationControllerDelegate alloc] initWithNavigationController:self];
        self.delegate = self.navManager;
        
        __weak typeof(self) weakSelf = self;
        self.networkStatusDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationNetworkStatusDidChangeNotification object:[RVSApplication application] usingBlock:^(NSNotification *notification) {
            [weakSelf newtworkStatusDidChange];
        }];
    }
    
    return self;
}

-(void)addProgressViewIfNeeded
{
    if (!self.postProgressView)
    {
        //show
        self.postProgressView = [[RVSAppHeaderView alloc] initWithFrame:CGRectMake(0, self.navigationBar.frame.origin.y + self.navigationBar.frame.size.height, CGRectGetWidth(self.view.frame), 30)];
        self.postProgressView.alpha = 0;
        self.postProgressViewImageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 14, 14)];
        [self.postProgressView addSubview:self.postProgressViewImageView];
        
        [self.view addSubview:self.postProgressView];
    }
}

-(void)showPostingProgress
{
    __weak typeof(self) weakSelf = self;
    
    [self addProgressViewIfNeeded];
    
    self.postProgressViewImageView.image = nil;
    self.postProgressView.titleLabel.text = NSLocalizedString(@"create post progress", nil);
    self.postProgressView.titleLabel.textColor = [UIColor whiteColor];
    self.postProgressView.contentView.backgroundColor = [[UIColor rvsTintColor] colorWithAlphaComponent:0.8];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         weakSelf.postProgressView.alpha = 1;
                     }
                     completion:nil];
}

-(void)showPostResultSuccess
{
    __weak typeof(self) weakSelf = self;
    
    [self addProgressViewIfNeeded];
    
    self.postProgressViewImageView.image = [UIImage imageNamed:@"post_status_success"];
    self.postProgressView.titleLabel.text = NSLocalizedString(@"create post success", nil);
    self.postProgressView.titleLabel.textColor = [UIColor whiteColor];
    self.postProgressView.contentView.backgroundColor = [[UIColor rvsTintColor] colorWithAlphaComponent:0.8];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         weakSelf.postProgressView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         [weakSelf hidePostingProgress];
                     }];
}

-(void)showPostResultFailed
{
    __weak typeof(self) weakSelf = self;
    
    [self addProgressViewIfNeeded];
    
    self.postProgressViewImageView.image = nil;
    self.postProgressView.titleLabel.text = NSLocalizedString(@"create post fail", nil);
    self.postProgressView.titleLabel.textColor = [UIColor whiteColor];
    self.postProgressView.contentView.backgroundColor = [[UIColor rvsTintColor] colorWithAlphaComponent:0.8];
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         weakSelf.postProgressView.alpha = 1;
                     }
                     completion:nil];
}

-(void)hidePostingProgress
{
    if (!self.postProgressView)
        return;
    
    //hide
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5 delay:2 options:0 animations:^{
        weakSelf.postProgressView.alpha = 0;
    } completion:^(BOOL finished) {
        [weakSelf.postProgressView removeFromSuperview];
        weakSelf.postProgressView = nil;
    }];
}

-(void)newtworkStatusDidChange
{
    __weak typeof(self) weakSelf = self;
    
    if (!self.networkStatusBar && ![RVSApplication application].hasNetworkConnection)
    {
        //show
        self.networkStatusBar = [[RVSAppHeaderView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 28, CGRectGetWidth(self.view.frame), 28)];
        self.networkStatusBar.titleLabel.text = NSLocalizedString(@"network status no network", nil);
        self.networkStatusBar.alpha = 0;
        self.networkStatusBar.backgroundColor = [[UIColor rvsHeaderBackgroundColor] colorWithAlphaComponent:0.7];
        
        [self.view addSubview:self.networkStatusBar];
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.networkStatusBar.alpha = 1;
        }];
        
    }
    else if (self.networkStatusBar && [RVSApplication application].hasNetworkConnection)
    {
        //hide
        [UIView animateWithDuration:0.5 animations:^{
            weakSelf.networkStatusBar.alpha = 0;
        } completion:^(BOOL finished) {
            [weakSelf.networkStatusBar removeFromSuperview];
            weakSelf.networkStatusBar = nil;
        }];
    }
}

+(UIViewController *)singlePostViewControllerWithPostId:(NSString *)postId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    vc.singlePostId = postId;
    vc.navigationItem.title = NSLocalizedString(@"post title", nil);
    
    return vc;
}

+(UIViewController *)viewControllerForContentType:(RVSAppMainContentType)contentType
{
    switch (contentType) {
        case RVSAppMainContentTypeTrending:
            return [RVSAppCenterNavigationController trendingPostsRootViewController];
            break;
            
        case RVSAppMainContentTypeRecent:
            return [RVSAppCenterNavigationController recentPostsRootViewController];
            break;
            
        case RVSAppMainContentTypeNewsFeed:
            return [RVSAppCenterNavigationController newsFeedRootViewController];
            break;
            
        case RVSAppMainContentTypeLiked:
            return [RVSAppCenterNavigationController likedPostsRootViewController];
            break;
            
        case RVSAppMainContentTypeProfile:
            return [RVSAppCenterNavigationController profileRootViewController];
            break;        
        
        case RVSAppMainContentTypeDevSettings:
            return [RVSAppCenterNavigationController devSettingsRootViewController];
            break;
            
        case RVSAppMainContentTypeAbout:
        default:
            return [RVSAppCenterNavigationController aboutRootViewController];
            break;
    }
}

+(UIViewController *)trendingPostsRootViewController
{
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithTrendingPosts];
    vc.navigationItem.title = NSLocalizedString(@"trending title", nil);
    
    return vc;
}

+(UIViewController *)recentPostsRootViewController
{
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithRecentPosts];
    vc.navigationItem.title = NSLocalizedString(@"recent title", nil);
    
    return vc;
}

+(UIViewController *)newsFeedRootViewController
{
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithNewsFeedForUser:[RVSSdk sharedSdk].myUserId];
    vc.navigationItem.title = NSLocalizedString(@"news feed title", nil);
    
    return vc;
}

+(UIViewController *)likedPostsRootViewController
{
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithLikedPostsByUser:[RVSSdk sharedSdk].myUserId];
    vc.navigationItem.title = NSLocalizedString(@"likes title", nil);
    
    return vc;
}

-(id<UIViewControllerTransitioningDelegate>)transitioningDelegate
{
    return nil;
}

+(UIViewController *)profileRootViewController
{
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    vc.userId = [RVSSdk sharedSdk].myUserId;
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithUserFeedForUser:vc.userId];
    vc.navigationItem.title = NSLocalizedString(@"my profile title", nil);
    
    return vc;
}

+(UIViewController *)aboutRootViewController
{
    RVSAppAboutViewController *vc = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppAboutViewController"];
    
    return vc;
}

+(UIViewController *)devSettingsRootViewController
{
    UIViewController *vc = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppDevSettingsViewController"];
    
    return vc;
}

@end
