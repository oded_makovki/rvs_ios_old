//
//  RVSAppSearchChannelDetectorCell.m
//  rvs_app2
//
//  Created by Barak Harel on 4/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchChannelDetectorCell.h"
#import "UIView+NibLoading.h"
#import "RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import "UIFont+RVSApplication.h"

@interface RVSAppSearchChannelDetectorCell()
@property (nonatomic) RVSKeyPathObserver *appPropertiesObserver;
@property (nonatomic) BOOL isActive;
@property (nonatomic) RVSPromiseHolder *resultImagePromise;
@property (nonatomic) RVSNotificationObserver *channelDetectorStatusObserver;
@end

@implementation RVSAppSearchChannelDetectorCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppSearchChannelDetectorCell];
    }
    
    return self;
}

-(void)initRVSAppSearchChannelDetectorCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor whiteColor];
    self.textLabel.font = [UIFont rvsRegularFontWithSize:self.textLabel.font.pointSize];
    
    __weak typeof(self) weakSelf = self;
    self.channelDetectorStatusObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorStatusNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorStatusNotification:notification];
    }];
    
    [self setKvo];
}

- (void)setKvo
{
    __weak typeof(self) weakSelf = self;
    
    NSArray *props = @[@"shouldDetectChannel"];
    
    self.appPropertiesObserver = [[RVSApplication application] newObserverForKeyPaths:props
                                                                              options:NSKeyValueObservingOptionInitial
                                                                           usingBlock:^(id obj, NSString *keyPath, NSDictionary *change) {
                                                                               
                                                                               if ([RVSApplication application].shouldDetectChannel)
                                                                               {
                                                                                   [weakSelf activate];
                                                                               }
                                                                               else
                                                                               {
                                                                                   [weakSelf deactivate];
                                                                               }
                                                                           }];
}

-(void)deactivate
{
    self.isActive = NO;
    [self.detectorActivityIndicator stopAnimation];
    self.detectorActivityIndicator.isDetected = NO;
    
    [UIView transitionWithView:self.topContainer duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.micImageView.image = [[UIImage imageNamed:@"search_icon_mic"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        self.acttionImageView.image = [UIImage imageNamed:@"search_button_start"];
        self.textLabel.textColor = [UIColor rvsSearchTextColor];
        self.textLabel.text = NSLocalizedString(@"search detector inactive", nil);
    } completion:nil];
}

-(void)activate
{
    self.isActive = YES;
    
    [self.detectorActivityIndicator startAnimation];
    
    [UIView transitionWithView:self.topContainer duration:0.2 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.micImageView.image = [[UIImage imageNamed:@"search_icon_mic"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.acttionImageView.image = [UIImage imageNamed:@"search_button_stop"];
        self.micImageView.tintColor = [UIColor rvsTintColor2];
        self.textLabel.textColor = [UIColor rvsTintColor2];
        self.textLabel.text = NSLocalizedString(@"search detector active", nil);
    } completion:nil];
}

-(void)setData:(RVSAppChannelDataObject *)data
{
    [super setData:data];
    
    self.detectorActivityIndicator.isDetected = NO;
    if (data.channel)
    {
        self.detectorActivityIndicator.isDetected = YES;
    }
}

-(void)handleChannelDetectorStatusNotification:(NSNotification *)notification
{
    RVSSdkChannelDetectorStatus status = [notification.userInfo[RVSSdkChannelDetectorStatusUserInfoKey] integerValue];
    
    switch (status)
    {
        case RVSSdkChannelDetectorStatusDetecting:
            [self.detectorActivityIndicator setTempBarsColor:[UIColor rvsTintColor]];
            break;
            
        default:
            break;
    }
}


-(CGSize)sizeForData:(RVSAppChannelDataObject *)data
{
    //if channel or program found
    if (data.channel)
    {
        return CGSizeMake(320, 135);
    }
    else
    {
        return CGSizeMake(320, 44);
    }
}

@end
