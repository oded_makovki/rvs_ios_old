//
//  RVSAppSearchResultMatchData.m
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchMatchData.h"
#import "RVSAppStringUtil.h"
#import "NSArray+RVSApplication.h"

@implementation RVSAppSearchMatchData

+(RVSAppSearchMatchData *)dataWithSearchMatch:(NSObject<RVSSearchMatch> *)searchMatch
{
    RVSAppSearchMatchData *data = [[self alloc] init];

    NSString *title = @"";
    
    if (![searchMatch.fieldName isEqualToString:@"transcript"])
    {
        title = [[RVSAppStringUtil sharedUtil] stringForSearchFieldName:searchMatch.fieldName];
        title = [NSString stringWithFormat:@"%@: ", title];
    }
    
    data.text = [NSString stringWithFormat:@"%@%@", title, searchMatch.valueFragment];
    data.highlightRanges = [searchMatch.fragmentRanges rangesFromFragmentRangesWithOffset:title.length];
    data.referenceTime = searchMatch.referenceTime;
    
    return data;
}

@end
