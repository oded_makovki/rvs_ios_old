//
//  TVXTimeLine.h
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+NibLoading.h"
#import <rvs_sdk_api.h>

typedef enum
{
    AutoScrollTypeNone,
    AutoScrollTypeSmart,
    AutoScrollTypeAlways
}
AutoScrollType;

@class RVSAppTimelineView;

@protocol RVSAppTimelineViewDataSource <NSObject>
-(NSObject <RVSAsyncImage> *)timeline:(RVSAppTimelineView *)timeline thumbnailForTimeOffset:(NSTimeInterval)timeOffset;
@end

@protocol RVSAppTimelineViewDelegate <NSObject>
@optional
-(void)timeline:(RVSAppTimelineView *)timeline didChangeWithStartOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration;
-(void)timeline:(RVSAppTimelineView *)timeline didSelectFrameWithOffset:(NSTimeInterval)timeOffset;
-(void)timelineDidBeginDragging;
-(void)timelineDidEndDragging;
@end

@interface RVSAppTimelineView : NibLoadedView

@property (nonatomic) NSTimeInterval frameDuration;
@property (nonatomic) CGFloat frameWidth;
@property (nonatomic, readonly) NSTimeInterval windowDuration;

@property (nonatomic) NSInteger delegateTimeintervalSensitivity;

@property (nonatomic) BOOL isCentered;
@property (nonatomic, readwrite) BOOL isDragging;

@property (weak, nonatomic) id <RVSAppTimelineViewDelegate> delegate;
@property (weak, nonatomic) id <RVSAppTimelineViewDataSource> dataSource;

-(void)setupWithDuration:(NSTimeInterval)duration referenceStartTime:(NSDate *)startTime isLive:(BOOL)isLive;
-(BOOL)scrollTo:(NSTimeInterval)timeOffset animated:(BOOL)animated;
@end
