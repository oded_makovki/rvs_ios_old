//
// RVSAppTrimControl.h
// RVSAppTrimControl
//

#import <UIKit/UIKit.h>
#import "UIViewContainer.h"
#import "RVSAppTrimPopover.h"

@protocol RVSAppTrimControlDelegate;

@interface RVSAppTrimControl:UIViewContainer

@property (weak, nonatomic) id<RVSAppTrimControlDelegate> delegate;

@property (nonatomic) NSInteger length;
@property (nonatomic) NSInteger minLength;
@property (nonatomic) NSDate *startTime;
@property (nonatomic) NSTimeInterval startTimeOffset;

@property (readonly, nonatomic) CGFloat leftValue;
@property (readonly, nonatomic) CGFloat rightValue;

@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIColor *textBackgroundColor;
@property (nonatomic) NSInteger textVerticalOffset;

@property (nonatomic) BOOL isLive;
@property (nonatomic) BOOL enabled;
@end

@protocol RVSAppTrimControlDelegate <NSObject>

- (void)trimControl:(RVSAppTrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue;

@end