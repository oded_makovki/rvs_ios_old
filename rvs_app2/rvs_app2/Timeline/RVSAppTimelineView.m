//
//  TVXTimeLine.m
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTimelineView.h"
#import "RVSAppTimelineCell.h"
#import "RVSAppTrimControl.h"
#import "UIViewContainer.h"
#import "RVSAppSpinnerView.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

#define MAX_CLIP_DURATION 60.0f

#define DEF_FRAME_DURATION  3.0f
#define DEF_FRAME_WIDTH  64.0f

#define DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY 1

#define PNT_PER_SEC self.frameWidth / self.frameDuration
#define SEC_PER_PNT self.frameDuration / self.frameWidth


@interface RVSAppTimelineView () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIGestureRecognizerDelegate, RVSAppTrimControlDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet RVSAppTrimControl *trimControl;
@property (weak, nonatomic) IBOutlet RVSAppSpinnerView *spinner;

@property (nonatomic) NSTimeInterval notifiedSelectionStartOffset;
@property (nonatomic) NSTimeInterval notifiedSelectionDuration;

@property (nonatomic) NSDate *startTime;
@property (nonatomic) NSTimeInterval duration;

@property (nonatomic) NSTimeInterval currentStartOffset; //displayed window start

@property (nonatomic) NSTimeInterval selectionStartOffset; //cropped window start
@property (nonatomic) NSTimeInterval selectionDuration;

@property (nonatomic) NSTimeInterval windowDuration;
@end

@implementation RVSAppTimelineView

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppTimelineView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppTimelineView];
}

-(void)initRVSAppTimelineView
{
    _frameDuration = DEF_FRAME_DURATION;
    _frameWidth = DEF_FRAME_WIDTH;
    
    self.spinner.indicatorImage = [UIImage imageNamed:@"small_spinner_white"];
    self.spinner.hidesWhenStopped = YES;
    [self.spinner startAnimating];
    
    [self updateWindowDuration];
    
    self.delegateTimeintervalSensitivity = DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY;
    
    [self.collectionView registerClass:[RVSAppTimelineCell class] forCellWithReuseIdentifier:@"timelineCell"];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
//    self.collectionView.bounces = NO;
    
    self.trimControl.delegate = self;
    self.trimControl.enabled = NO;
    
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;     
}

-(void)setupWithDuration:(NSTimeInterval)duration referenceStartTime:(NSDate *)startTime isLive:(BOOL)isLive
{
    [self.spinner stopAnimating];
    self.trimControl.isLive = isLive;
    self.trimControl.enabled = YES;
    self.duration = duration;
    self.startTime = startTime;
    self.trimControl.startTime = startTime;
    
    if (self.duration >= MAX_CLIP_DURATION)
    {
        self.frameDuration = MAX_CLIP_DURATION / (self.frame.size.width / self.frameWidth);
    }
    else
    {
        self.frameDuration = self.duration / (self.frame.size.width / self.frameWidth);
    }
    
    [self.collectionView reloadData];
    [self.collectionView layoutIfNeeded]; //important - force update of content size otherwise getCurrentStartOffset will return wrong value.
    
    if (isLive)
    {
        [self scrollTo:self.duration animated:YES];
    }
    else
    {
        [self scrollTo:0 animated:YES];
    }
}

#pragma mark - Time Window / Frames


-(void)updateWindowDuration
{
    self.windowDuration = self.frameDuration * self.frame.size.width / self.frameWidth;
    self.trimControl.length = self.windowDuration;
    
    if (self.trimControl.length >= 5.0f)
    {
        self.trimControl.minLength = 5.0f;
    }
    else
    {
        self.trimControl.minLength = self.trimControl.length;
    }
    
    RVSInfo(@"Updated timeline window duration: %.2fs frame duration: %.2fs width: %.2fpx", self.windowDuration, self.frameDuration, self.frameWidth);
}

-(void)setFrameDuration:(NSTimeInterval)frameDuration
{
    NSAssert(frameDuration > 0, @"frame duration must be positive");
    _frameDuration = frameDuration;
    [self updateWindowDuration];
}

-(void)setFrameWidth:(CGFloat)frameWidth
{
    NSAssert(frameWidth > 0, @"frame width must be positive");
    _frameWidth = frameWidth;
    [self updateWindowDuration];
}

#pragma mark - Scroll To Time

-(BOOL)scrollTo:(NSTimeInterval)timeOffset animated:(BOOL)animated
{
    BOOL canScroll = YES;
    
    NSTimeInterval screenEndOffset = self.duration - self.windowDuration;

    if (timeOffset > screenEndOffset)
    {
        RVSVerbose(@"Timeline scroll outside screen, updated time: %.0f", screenEndOffset);
        canScroll = NO;
        timeOffset = screenEndOffset;
    }
    
    float offsetX = PNT_PER_SEC * timeOffset;
    
    if (canScroll && self.isCentered)
        offsetX -= self.frame.size.width /2;
    
    BOOL forceUpdate = NO;
    if (self.collectionView.contentOffset.x == offsetX)
        forceUpdate = YES;
    
    [self.collectionView setContentOffset:CGPointMake(offsetX, 0) animated:animated];
    
    if (forceUpdate)
        [self scrollViewDidScroll:self.collectionView];
    
    return canScroll;
}

#pragma mark - Bounds/Selection

-(NSTimeInterval)getCurrentStartOffset
{
    float startPt = self.collectionView.contentOffset.x;

    //verify upper bound
    if (startPt > self.collectionView.contentSize.width - self.collectionView.frame.size.width)
        startPt = self.collectionView.contentSize.width - self.collectionView.frame.size.width;
    
    //verify lower bound
    if (startPt < 0)
        startPt = 0;
    
    float startFrames = startPt / self.frameWidth;
    float startTimeSec = startFrames * self.frameDuration;
    
    return startTimeSec;
}

-(void)updateCurrentBounds
{
    if (self.duration == 0)
        return;
    
    self.currentStartOffset = [self getCurrentStartOffset];
    
    _trimControl.startTimeOffset = self.currentStartOffset;
}

-(void)updateSelectionBounds
{
    if (self.duration == 0)
        return;
    
    self.selectionStartOffset = round(self.currentStartOffset + self.trimControl.leftValue);
    NSTimeInterval selectionEndOffset = round(self.currentStartOffset + self.trimControl.rightValue);
    
    self.selectionDuration = selectionEndOffset - self.selectionStartOffset;
    
    RVSVerbose(@"Timeline updateSelectionBounds, selection start offset: %.2f end offset: %f duration: %.2f ", self.selectionStartOffset, selectionEndOffset, self.selectionDuration);
    
    if (self.selectionStartOffset < 0)
    {
        RVSWarn(@"Timeline selection start offset is nagative, updating selection start offset.");
        self.selectionStartOffset = 0;
    }
    
    if (self.selectionStartOffset + self.selectionDuration > self.duration)
    {
        RVSWarn(@"Timeline selection duration longer then duration, updating selection duration.");
        self.selectionDuration = self.duration - self.selectionStartOffset;
    }
    
    //if bound change bigger then sensitivity
    if (fabsf(self.notifiedSelectionStartOffset - self.selectionStartOffset) > self.delegateTimeintervalSensitivity ||
        fabsf(self.notifiedSelectionDuration - self.selectionDuration) > self.delegateTimeintervalSensitivity)
    {
        self.notifiedSelectionStartOffset = self.selectionStartOffset;
        self.notifiedSelectionDuration = self.selectionDuration;
        
        if ([self.delegate respondsToSelector:@selector(timeline:didChangeWithStartOffset:duration:)])
        {
            [self.delegate timeline:self didChangeWithStartOffset:self.selectionStartOffset duration:self.selectionDuration];
        }
    }
}

#pragma mark - RVSAppTrimControl Delegate

-(void)trimControl:(RVSAppTrimControl *)trimControl didChangeLeftValue:(CGFloat)leftValue rightValue:(CGFloat)rightValue
{
    [self updateSelectionBounds];
}

#pragma mark - UICollectionView Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self totalFrames];
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.frameWidth, self.frame.size.height);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppTimelineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"timelineCell" forIndexPath:indexPath];
    
    NSTimeInterval timeOffset = indexPath.row * self.frameDuration;
    cell.thumbnail = [self.dataSource timeline:self thumbnailForTimeOffset:timeOffset];
    
    return cell;
}

- (NSString *)timeFormatted:(int)totalSeconds
{
    BOOL isNagative = NO;
    if (totalSeconds < 0)
    {
        isNagative = YES;
        totalSeconds = fabs(totalSeconds);
    }
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    
    return [NSString stringWithFormat:@"%@%02d:%02d",(isNagative ? @"-" : @"+"), minutes, seconds];
}

#pragma mark - UIScrollView Delegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if (!_isDragging)
    {
        _isDragging = YES;
        if ([self.delegate respondsToSelector:@selector(timelineDidBeginDragging)])
            [self.delegate timelineDidBeginDragging];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateCurrentBounds];
    [self updateSelectionBounds];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self onScrollEnded:scrollView];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self onScrollEnded:scrollView];
    
    if (_isDragging)
    {
        _isDragging = NO;
        if ([self.delegate respondsToSelector:@selector(timelineDidEndDragging)])
            [self.delegate timelineDidEndDragging];
    }
}

-(void)onScrollEnded:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x >= scrollView.contentSize.width - scrollView.frame.size.width)
    {
        //TBD
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(timeline:didSelectFrameWithOffset:)])
    {
        NSTimeInterval timeOffset = indexPath.row * self.frameDuration;
        
        [self.delegate timeline:self didSelectFrameWithOffset:timeOffset];
    }
}

-(float)totalFrames
{
    return self.duration / self.frameDuration;
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}

@end
