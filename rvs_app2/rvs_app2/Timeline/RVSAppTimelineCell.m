//
//  TVXClipFrameCell.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppTimelineCell.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+NibLoading.h"

#define DEF_BACK_IMAGE @"frame_back_lines"

@interface RVSAppTimelineCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) RVSPromiseHolder *thumbnailPromise;
@end

@implementation RVSAppTimelineCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        [self.imageView setContentMode:UIViewContentModeScaleAspectFill];
            }
    return self;
}

-(void)setThumbnail:(NSObject <RVSAsyncImage> *)thumbnail
{
    //clear old to cancel
    self.imageView.image = nil;
    
    __weak RVSAppTimelineCell *weakSelf = self;
    
    self.thumbnailPromise = [thumbnail imageWithSize:self.imageView.frame.size].newHolder;    
    [self.thumbnailPromise thenOnMain:^(UIImage *image){
        weakSelf.imageView.image = image;
    } error:nil];
}

@end
