//
//  TVXClipFrameCell.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@interface RVSAppTimelineCell : UICollectionViewCell

-(void)setThumbnail:(NSObject <RVSAsyncImage> *)thumbnail;

@end
