//
//  RVSAppPushableUserListViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushableUserListViewController.h"
#import <rvs_sdk_api.h>
#import "RVSAppUserListCell.h"
#import "RVSAppDataListFooterCell.h"
#import "RVSApplication.h"

#import "RVSAppUserDataObject.h"

@interface RVSAppPushableUserListViewController () <RVSAppDataListViewDelegate, RVSAppDataListViewDataSource, RVSAppCommonViewControllerDelegate>
@property (nonatomic) NSObject <RVSAsyncList> *userList;
@property (nonatomic) RVSPromise *loadNextPromise;
@property (nonatomic) BOOL shouldClearUsersData;

@property (nonatomic) NSMutableArray *usersData;
@end

@implementation RVSAppPushableUserListViewController

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppPushableUserListViewController];
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPushableUserListViewController];
}

-(void)initRVSAppPushableUserListViewController
{
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataListView.dataSource = self;
    self.dataListView.delegate = self;
    
    [self.dataListView setNumberOfSections:1]; //just posts

    //already set - load data
    if (self.userListFactory)
        [self loadUsers];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)reloadUsers
{
    [self.loadNextPromise cancel]; //cancel current
    self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault; //prevent request next
    self.shouldClearUsersData = YES; //clear on load
    
    [self loadUsers];
}

- (void)loadUsers
{
    self.userList = nil;
    self.userList = [self.userListFactory userList];
    
    [self loadNextUsers];
}

-(void)clearDataIfNeeded
{
    if (self.shouldClearUsersData)
    {
        self.shouldClearUsersData = NO;
        [self.usersData removeAllObjects];
        [self.dataListView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    }
}

-(void)loadNextUsers
{
    __weak RVSAppPushableUserListViewController *weakSelf = self;
    self.loadNextPromise = [self.userList next:10];
    [self.loadNextPromise thenOnMain:^(NSArray *users) {
        
        [weakSelf clearDataIfNeeded];
        
        NSInteger startRow = [weakSelf.usersData count];
        [weakSelf.usersData addObjectsFromArray:[RVSAppUserDataObject dataObjectsFromUsers:users userViewDelegate:nil]];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:0 startRow:startRow count:[users count]]];
        
        if (weakSelf.userList.didReachEnd)
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        }
        else
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
        }
        
        
    } error:^(NSError *error) {
        
        if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
            return; //canceled
        
        [weakSelf clearDataIfNeeded];
        [weakSelf.dataListView reloadSection:0];
        
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;        
    }];
}

#pragma mark - DataListView Delegate

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf loadNextUsers];
    });
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadUsers];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppUserDataObject *userData = self.usersData[indexPath.row];
    [self pushUserDetailViewControlWithUserId:userData.user.userId];
}

#pragma mark - DataListView DataSource

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [RVSAppUserListCell class];
}

-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return self.usersData[indexPath.row];
}

-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section
{
    return [self.usersData count];
}

-(NSMutableArray *)usersData
{
    if (!_usersData)
        _usersData = [NSMutableArray array];
    
    return _usersData;
}

#pragma mark - Other

-(void)networkStatusDidChange
{
    if (self.dataListView.loadPhase == RVSAppDataListLoadPhaseEndOfList && [self.usersData count] == 0 && [RVSApplication application].hasNetworkConnection)
    {
        [self reloadUsers];
    }
}

-(void)userDidSignIn
{
    [self reloadUsers];
}

-(void)applicationDidCreatePost:(NSObject <RVSPost> *)post
{
    //TBD
}

@end
