//
//  RVSAppPostCellData.m
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostDataObject.h"

@implementation RVSAppPostDataObject

-(id)initWithPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate
{
    self = [self init];
    if (self)
    {
        self.post = post;
        self.delegate = delegate;
    }
    
    return self;
}


+(instancetype)dataObjectFromPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate
{
    id data = [[self alloc] initWithPost:post delegate:delegate];
    return data;
}


@end
