//
//  RVSAppSpoilersUtilConfigController.m
//  rvs_app2
//
//  Created by Barak Harel on 5/20/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppListUtilConfigController.h"
#import "RVSAppSpoilersUtil.h"
#import "RVSApplication.h"

@interface RVSAppListUtilConfigController () <UIAlertViewDelegate>
@property (nonatomic) UIAlertView *editValueAlertView;
@property (nonatomic) NSMutableArray *values;
@property (nonatomic) NSString *dataFilename;
@end

static NSString *CellIdentifier = @"Cell";

@implementation RVSAppListUtilConfigController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.dataFilename)
    {
        self.values = [[NSArray arrayWithContentsOfFile:[self dataFilePath]] mutableCopy];
    }
    
    if (!self.values) //first!
        self.values = [NSMutableArray array];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:CellIdentifier];
    
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEditing])
        return [self.values count] + 1;
    else
        return [self.values count];
}

-(void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.tableView reloadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (indexPath.row == ([self.values count]) && self.editing)
    {
        cell.textLabel.text = @"Add new value";
    }
    else
    {
        cell.textLabel.text = self.values[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
    }
    
    cell.showsReorderControl = YES;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
    NSString *value = self.values[indexPath.row];
    [self editValueAtRow:indexPath.row withValue:value];
}

-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.values count])
        return NO;
    
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    NSString *valueToMove = [self.values objectAtIndex:sourceIndexPath.row];
    [self.values removeObjectAtIndex:sourceIndexPath.row];
    [self.values insertObject:valueToMove atIndex:MIN(destinationIndexPath.row, [self.values count])];

    [self saveData];
}

-(void)editValueAtRow:(NSInteger)row withValue:(NSString *)value
{
    self.editValueAlertView = [[UIAlertView alloc] initWithTitle:@"Value:" message:nil delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
    
    self.editValueAlertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    [self.editValueAlertView textFieldAtIndex:0].text = value;
    self.editValueAlertView.tag = row;
    
    [self.editValueAlertView show];
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    UITextField *tf = [alertView textFieldAtIndex:0];
    return ([tf.text length] > 0);
}

-(void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        NSInteger row = self.editValueAlertView.tag;
        
        NSString *value = [self.editValueAlertView textFieldAtIndex:0].text;
        if ([value length] > 0)
        {
            if (row == -1)
            {
                [self.values addObject:value];
            }
            else
            {
                self.values[row] = value;
            }
            
            [self saveData];
            [self.tableView reloadData];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == NO || !indexPath)
        return UITableViewCellEditingStyleNone;
    
    if (self.editing && indexPath.row == [self.values count])
        return UITableViewCellEditingStyleInsert;
    else
        return UITableViewCellEditingStyleDelete;
    
    return UITableViewCellEditingStyleNone;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {

        [self.values removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        [self saveData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        [self editValueAtRow:-1 withValue:@""];
    }   
}

+(RVSAppListUtilConfigController *)showWithFilename:(NSString *)filename
{
    NSAssert(filename.length > 0, @"Invalid filename");
    
    RVSAppListUtilConfigController *vc = [[RVSAppListUtilConfigController alloc] init];
    vc.dataFilename = filename;
    [[RVSApplication application].mainViewController.centerViewController pushViewController:vc animated:YES];
    
    return vc;
}

-(void)saveData
{
    if (!self.values)
        return;
    
    // write the plist back to the documents directory
    [self.values writeToFile:[self dataFilePath] atomically:YES];
    
    if ([self.delegate respondsToSelector:@selector(listUtilController:didSaveWithValues:)])
    {
        [self.delegate listUtilController:self didSaveWithValues:[self.values copy]];
    }
}

-(NSString *)dataFilePath
{
    // get the path to the plist file
    NSArray *paths =  NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:self.dataFilename];
    
    return filePath;
}


@end
