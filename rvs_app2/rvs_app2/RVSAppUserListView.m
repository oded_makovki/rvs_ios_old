//
//  RVSAppUserListView.m
//  rvs_app2
//
//  Created by Barak Harel on 5/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppUserListView.h"
#import "UIFont+RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import "RVSAppStringUtil.h"

#define SEPERATOR @" • "

@implementation RVSAppUserListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserListView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUserListView];
}

-(void)initRVSAppUserListView
{
    self.userNameLabel.font = [UIFont rvsBoldFontWithSize:14];
    self.infoLabel.font = [UIFont rvsRegularFontWithSize:12];
    self.infoLabel.textColor = [UIColor rvsPostInfoTextColor];
}

-(void)clear
{
    [super clear];
    self.infoLabel.text = @"";
}

-(void)userDidChange
{
    [super userDidChange];
    NSString *info = @"";
    
    if ([self.user.followerCount integerValue] > 0)
    {
        NSString *followers = [NSString stringWithFormat:NSLocalizedString(@"user info followers format", nil), [RVSAppStringUtil prettyNumber:self.user.followerCount]];

        info = followers;
    }
    
    if ([self.user.followingCount integerValue] > 0)
    {
        if (info.length > 0)
            info = [NSString stringWithFormat:@"%@%@", info, SEPERATOR];
        
        NSString *following = [NSString stringWithFormat:NSLocalizedString(@"user info following format", nil), [RVSAppStringUtil prettyNumber:self.user.followingCount]];
        
        info = [NSString stringWithFormat:@"%@%@", info, following];
    }
    
    if ([self.user.postCount integerValue] > 0 || info.length == 0) //empty info must not be empty
    {
        if (info.length > 0)
            info = [NSString stringWithFormat:@"%@%@", info, SEPERATOR];
        
        NSString *posts = [NSString stringWithFormat:NSLocalizedString(@"user info posts format", nil), [RVSAppStringUtil prettyNumber:self.user.postCount]];
        
        info = [NSString stringWithFormat:@"%@%@", info, posts];
    }
    
    self.infoLabel.text = info;
}

@end
