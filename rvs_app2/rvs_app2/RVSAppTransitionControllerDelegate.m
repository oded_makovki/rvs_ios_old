//
//  RVSAppTransitionControllerDelegate.m
//  rvs_app2
//
//  Created by Barak Harel on 3/23/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppTransitionControllerDelegate.h"
#import "UIViewController+RVSAppTransitionController.h"

@interface RVSAppTransitionControllerDelegate () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) UINavigationController *navigationController;

@property (strong, nonatomic) UIPercentDrivenInteractiveTransition *interactiveTransition;
@property (strong, nonatomic) UIScreenEdgePanGestureRecognizer *interactiveGestureRecognizer;
@end

@implementation RVSAppTransitionControllerDelegate

-(id)initWithNavigationController:(UINavigationController *)navigationController
{
    self = [super init];
    if (self)
    {
        self.navigationController = navigationController;
        
        self.interactiveGestureRecognizer = [[UIScreenEdgePanGestureRecognizer alloc] initWithTarget:self action:@selector(handleTransitionGesture:)];
        self.interactiveGestureRecognizer.edges = UIRectEdgeLeft;
        self.interactiveGestureRecognizer.delegate = self;
        [self.navigationController.view addGestureRecognizer:self.interactiveGestureRecognizer];
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    
    return self;
}

#pragma mark - UINavigationControllerDelegate

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    switch (operation)
    {
        case UINavigationControllerOperationPush:
        {
            if ([toVC usesPushTransitionController])
                return [toVC popTransitionController];
            break;
        }
            
        case UINavigationControllerOperationPop:
        {
            if ([fromVC usesPopTransitionController])
            {
                return [fromVC popTransitionController];
            }
            break;
        }
            
        default:
            break;
    }
    
    return nil;
}

- (id<UIViewControllerInteractiveTransitioning>)navigationController:(UINavigationController *)navigationController interactionControllerForAnimationController:(id<UIViewControllerAnimatedTransitioning>)animationController
{
    return self.interactiveTransition;
}

#pragma mark - UIGestureRecognizer

- (void)handleTransitionGesture:(UIPanGestureRecognizer *)recognizer
{
    CGFloat progress = [recognizer translationInView:recognizer.view].x / recognizer.view.frame.size.width;
    
    switch (recognizer.state)
    {
        case UIGestureRecognizerStateBegan:
        {
            self.interactiveTransition = [[UIPercentDrivenInteractiveTransition alloc] init];
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
            
        case UIGestureRecognizerStateChanged:
        {
            [self.interactiveTransition updateInteractiveTransition:progress];
            break;
        }
            
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        {
            if (progress > 0.3) //tweak?
                [self.interactiveTransition finishInteractiveTransition];
            else
                [self.interactiveTransition cancelInteractiveTransition];
            self.interactiveTransition = nil;
            break;
        }
            
        default:
            break;
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([self.navigationController.transitionCoordinator isAnimated])
        return NO;
    
    if (self.navigationController.viewControllers.count < 2)
        return NO;
    
    if ([self.navigationController.topViewController usesPopTransitionController])
    {
        if (gestureRecognizer == self.interactiveGestureRecognizer)
            return YES;
    }
    else if (gestureRecognizer == self.navigationController.interactivePopGestureRecognizer)
    {
        return YES;
    }
    
    return NO;
}

@end
