//
//  RVSAppPostCommentsCell.h
//  rvs_app2
//
//  Created by Barak Harel on 6/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppPostCommentsView.h"
#import "RVSAppPostCommentsDataObject.h"

@interface RVSAppPostCommentsCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppPostCommentsView *commentsView; //needs to be init or set
@property (nonatomic) RVSAppPostCommentsDataObject *data;
@end
