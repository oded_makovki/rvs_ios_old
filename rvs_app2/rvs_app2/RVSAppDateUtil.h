//
//  RVSAppDateUtil.h
//  rvs_app2
//
//  Created by Barak Harel on 3/4/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppDateUtil : NSObject
+(NSString *)timeShortStringSinceTime:(NSTimeInterval)time;
+(NSString *)timeLongStringSinceTime:(NSTimeInterval)time;
@end
