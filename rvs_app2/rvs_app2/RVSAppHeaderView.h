//
//  RVSAppHeaderView.h
//  rvs_app2
//
//  Created by Barak Harel on 3/30/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppHeaderDataObject.h"

@interface RVSAppHeaderView : UICollectionReusableView
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) UIView *contentView;
@property (nonatomic) RVSAppHeaderDataObject *data;
+(CGSize)preferredHeaderSize;
-(void)prepareForReuse;
@end
