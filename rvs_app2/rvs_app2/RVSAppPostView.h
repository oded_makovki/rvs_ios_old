//
//  RVSAppPostCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import <TTTAttributedLabel.h>
#import "RVSAppClipPlayerView.h"
#import "RVSAppUserDetailView.h"
#import "UIViewContainer.h"
#import "RVSAppViewDelegate.h"

@interface RVSAppPostView : UIView

@property (weak, nonatomic) IBOutlet RVSAppClipPlayerView *clipPlayerView;

@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UILabel *searchResultMatchLabel;
@property (weak, nonatomic) IBOutlet UILabel *searchResultTitleLabel;

@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;
@property (nonatomic, readonly) NSObject <RVSPost> *post;
@property (nonatomic, readonly) NSArray *searchMatches;

@property (nonatomic) BOOL shouldParseHotwords;
@property (nonatomic) BOOL isUsingLongTimeFormat;

-(void)clear;

/**
 *  set post for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param post             post object
 *  @param searchMatches    optional search matches
 *  @param isSizingView     set to YES for optimiziation
 */
-(void)setPost:(NSObject <RVSPost> *)post searchMatches:(NSArray *)searchMatches isSizingView:(BOOL)isSizingView;

/**
 *  set post for view. same as setPost:post isSizingView:NO
 *
 *  @param post             post object
 *  @param searchMatches    optional search matches
 */
-(void)setPost:(NSObject<RVSPost> *)post searchMatches:(NSArray *)searchMatches;

-(CGFloat)heightForSearchMatches;

@end
