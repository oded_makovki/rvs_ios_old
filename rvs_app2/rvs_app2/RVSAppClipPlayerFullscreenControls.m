//
//  RVSAppClipPlayerViewLandscapeUI.m
//  rvs_app2
//
//  Created by Barak Harel on 5/18/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppClipPlayerFullscreenControls.h"
#import <rvs_sdk_api helpers.h>
#import "UIFont+RVSApplication.h"

@interface RVSAppClipPlayerFullscreenControls()

@property (weak, nonatomic) IBOutlet UIButton *playToggleButton;
@property (weak, nonatomic) IBOutlet UIView *uiContainer;
@property (nonatomic) NSTimer *uiHideTimer;
@end

@implementation RVSAppClipPlayerFullscreenControls

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppClipPlayerViewFullscreenUI];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppClipPlayerViewFullscreenUI];
}

-(void)initRVSAppClipPlayerViewFullscreenUI
{
    [self.scrubber setMinimumTrackImage:[UIImage imageNamed:@"player_scrubber_minimum_fullscreen"] forState:UIControlStateNormal];
    [self.scrubber setMaximumTrackImage:[UIImage imageNamed:@"player_scrubber_maximum_fullscreen"] forState:UIControlStateNormal];
    [self.scrubber setThumbImage:[UIImage imageNamed:@"player_scrubber_thumb_fullscreen"] forState:UIControlStateNormal];
    
    self.endCardLabel.font = [UIFont rvsBoldFontWithSize:20];
}


- (IBAction)playTogglePressed:(UIButton *)sender
{
    [self.delegate togglePlayback];
}

-(void)setPlayerState:(RVSClipViewState)state
{
    BOOL animated = YES;
    if (self.isReloadingData)
        animated = NO;
    
    if (state == RVSClipViewStateLoading)
    {
        [self.spinner startAnimating];
    }
    else
    {
        [self.spinner stopAnimating];
    }
    
    switch (state)
    {
        case RVSClipViewStateUnknown:    //show metadata (non-animated)
            [self showUIAnimated:NO];
            [self hideEndCardAnimated:NO];
            break;
            
        case RVSClipViewStateLoading:
            [self showUIAnimated:NO];
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePlaying: //hide metadata animated
            [self hideUIAnimated:animated];
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePaused: //show metadata animated
            if (!self.isScrubbing)
                [self showUIAnimated:animated];
            
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePlaybackEnded:  //show end card
            [self hideUIAnimated:animated]; //for now
            [self showEndCardAnimated:animated];
            break;
            
        default:
            break;
    }
    
    if (state == RVSClipViewStateLoading || state == RVSClipViewStatePlaying)
    {
        [self.playToggleButton setImage:[UIImage imageNamed:@"player_small_pause"] forState:UIControlStateNormal];
    }
    else
    {
        [self.playToggleButton setImage:[UIImage imageNamed:@"player_small_play"] forState:UIControlStateNormal];
    }
}

#pragma mark - End Card

-(void)showEndCardAnimated:(BOOL)animated
{
    self.togglePlayGestureRecognizer.enabled = NO;
    
    self.endCardView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.endCardView.alpha = 1.0f;
    }];
}

-(void)hideEndCardAnimated:(BOOL)animated
{
    self.togglePlayGestureRecognizer.enabled = YES;
    
    [self.endCardView setHidden:YES];
    self.endCardView.alpha = 0.0f;
}

#pragma mark - Scrub

-(void)scrubberValueDidChange
{
    [super scrubberValueDidChange];
    
    if (self.uiHideTimer)
        [self.uiHideTimer invalidate];
}

#pragma mark - Animations

-(void)hideUIAnimated:(BOOL)animated
{
    if (self.uiHideTimer)
        [self.uiHideTimer invalidate];
    
    if (animated && self.superview)
    {
        self.uiHideTimer = [NSTimer scheduledTimerWithTimeInterval:3 block:^(NSTimer *timer) {
            [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:
             ^{
                 self.uiContainer.alpha = 0;
             } completion:nil];
        } repeats:NO];
    }
    else
    {
        self.uiContainer.alpha = 0;
    }
}

-(void)showUIAnimated:(BOOL)animated
{
    if (self.uiHideTimer)
        [self.uiHideTimer invalidate];
    
    if (animated)
    {
        [UIView animateWithDuration:1 delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:
         ^{
            self.uiContainer.alpha = 1;
        } completion:nil];
    }
    else
    {
        self.uiContainer.alpha = 1;
    }
}

@end
