//
//  RVSAppPostListFactory.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

typedef NS_ENUM(NSInteger, RVSAppPostListFactoryType)
{
    RVSAppPostListFactoryTypeTrendingPosts,
    RVSAppPostListFactoryTypeRecentPosts,
    RVSAppPostListFactoryTypeNewsFeed,
    RVSAppPostListFactoryTypeUserFeed,
    RVSAppPostListFactoryTypeUserLikedPosts
};

@interface RVSAppPostListFactory : NSObject
@property (nonatomic, readonly) RVSAppPostListFactoryType type;

-(id)initWithTrendingPosts;
-(id)initWithRecentPosts;
-(id)initWithNewsFeedForUser:(NSString *)userId;
-(id)initWithUserFeedForUser:(NSString *)userId;
-(id)initWithLikedPostsByUser:(NSString *)userId;

-(NSObject <RVSAsyncList>*)postList;
@end
