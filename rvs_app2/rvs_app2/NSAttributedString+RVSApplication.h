//
//  NSAttributedString+RVSApplication.h
//  rvs_app2
//
//  Created by Barak Harel on 5/26/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSAttributedString (RVSApplication)

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingFont:(UIFont *)font color:(UIColor *)color ranges:(NSArray *)ranges;

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingBackgroundColor:(UIColor *)backgroundColor ranges:(NSArray *)ranges;

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingFont:(UIFont *)font foregroundColor:(UIColor *)foregroundColor backgroundColor:(UIColor *)backgroundColor ranges:(NSArray *)ranges;

@end
