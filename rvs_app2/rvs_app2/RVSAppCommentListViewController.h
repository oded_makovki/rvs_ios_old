//
//  RVSAppCommentListViewController.h
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"

@interface RVSAppCommentListViewController : RVSAppCommonDataViewController
@property (nonatomic) NSString *postId;
@property (nonatomic) BOOL shouldStartEdit;
@end
