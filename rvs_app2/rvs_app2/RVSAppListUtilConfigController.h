//
//  RVSAppSpoilersUtilConfigController.h
//  rvs_app2
//
//  Created by Barak Harel on 5/20/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RVSAppListUtilConfigController;

@protocol RVSAppListUtilConfigControllerDelegate <NSObject>
@optional
-(void)listUtilController:(RVSAppListUtilConfigController *)sender didSaveWithValues:(NSArray *)values;
@end

@interface RVSAppListUtilConfigController : UITableViewController
@property (nonatomic, readonly) NSMutableArray *values;
@property (nonatomic) id <RVSAppListUtilConfigControllerDelegate> delegate;

+(RVSAppListUtilConfigController *)showWithFilename:(NSString *)filename;

@end
