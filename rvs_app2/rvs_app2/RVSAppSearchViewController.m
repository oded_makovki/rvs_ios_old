//
//  RVSAppSearchViewController.m
//  rvs_app2
//
//  Created by Barak Harel on 3/17/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchViewController.h"
#import <rvs_sdk_api helpers.h>
#import "RVSApplication.h"
#import "RVSAppMainViewController.h"

#import "RVSAppHeaderView.h"
#import "RVSAppHeaderDataObject.h"

#import "RVSAppSearchSuggestionCell.h"
#import "RVSAppSearchChannelDetectorCell.h"
#import "RVSAppChannelDataObject.h"

#import "UIViewController+RVSAppTransitionController.h"
#import "RVSAppCrossFadeTransitionController.h"

#import "UIColor+RVSApplication.h"

#define DETECTOR_LIST_SECTION 0
#define SUGGEST_LIST_SECTION 1

#define DATA_FILE_NAME @"searchSuggestions.plist"

@interface RVSAppSearchViewController () <UISearchBarDelegate, RVSAppCommonViewControllerDelegate, RVSAppDataListViewDelegate, RVSAppDataListViewDataSource>

@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarLeadingConstraint;

@property (nonatomic) RVSNotificationObserver *channelDetectorMatchObserver;

@property (nonatomic) BOOL canSearch; //used to verify completly appear before you can search again

@property (nonatomic) NSMutableArray *suggestions;
@property (nonatomic) RVSAppChannelDataObject *channelData;

@property (nonatomic) BOOL channelDetectorEnabled;
@end

@implementation RVSAppSearchViewController

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppSearchViewController];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppSearchViewController];
}

-(void)initRVSAppSearchViewController
{
    self.delegate = self; //common;
    self.navigationItem.title = @"SEARCH";
    [self loadSuggestionsFromDisk];
}

-(NSMutableArray *)suggestions
{
    if (!_suggestions)
    {
        _suggestions = [NSMutableArray array];
    }
    
    return _suggestions;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor rvsBackgroundColor];
    
    self.searchBarLeadingConstraint.constant = 100;    
    
    __weak RVSAppSearchViewController *weakSelf = self;
    
    //inner background / color
    [self.searchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_inner_background"] forState:UIControlStateNormal];
    self.searchBar.placeholder = NSLocalizedString(@"search placeholder", nil);
    
    self.searchBar.delegate = self;
    self.doneButton.tintColor = [UIColor rvsTintColor];    
    
    self.dataListView.refreshEnabled = NO;
    
    [self.dataListView setNumberOfSections:2]; //detector, suggestions
    self.dataListView.delegate = self;
    self.dataListView.dataSource = self;
    
    [self loadTrendingSearches];
    
    
    self.channelDetectorEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey];
    
    if (self.channelDetectorEnabled)
    {
        
        self.channelDetectorMatchObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
            [weakSelf handleChannelDetectorMatchNotification:notification];
        }];
        
        [self addDetectorCell];
    }
}

-(void)addDetectorCell
{
    self.channelData = [[RVSAppChannelDataObject alloc] init];
    [self.dataListView reloadSection:DETECTOR_LIST_SECTION];
}

-(void)handleChannelDetectorMatchNotification:(NSNotification *)notification
{
    RVSAppChannelDataObject *data = [[RVSAppChannelDataObject alloc] init];
    data.channel = notification.userInfo[RVSSdkChannelDetectorChannelUserInfoKey];
    
    self.channelData = data;
    [self.dataListView reloadSection:DETECTOR_LIST_SECTION];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.searchBar layoutIfNeeded];
    
    self.searchBarLeadingConstraint.constant = -7;
    
    __weak RVSAppSearchViewController *weakSelf = self;
    
    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:0 animations:^{
        [weakSelf.searchBar layoutIfNeeded];
    } completion:^(BOOL finished) {
        //
    }];
    
    if (self.channelDetectorEnabled)
        [self tryActivatingChannelDetect:NO];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.canSearch = YES; //see property declaration
    
    NSLog(@"view did appear");
    [self.searchBar becomeFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.searchBar resignFirstResponder];
    self.canSearch = NO; //see property declaration

    //disable on exit
    [RVSApplication application].shouldDetectChannel = NO;
}

#pragma mark - Search bar Delegate

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
//    if (searchText.length > 0)
//    {
//        [self loadSuggestionsFor:searchText];
//    }
//    else
//    {
//        [self loadTrendingSearches];
//    }
}

//-(void)loadSuggestionsFor:(NSString *)searchText
//{
//    
//}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"searchBarSearchButtonClicked");
    if (self.searchBar.text.length > 0 && self.canSearch)
        [self pushSearchResultsViewControllerWithSearchText:self.searchBar.text withOwner:self.owner];
}

#pragma mark - Dismiss

-(IBAction)done:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Common Delegate

-(BOOL)shouldAddDefaultRightNavigationItems
{
    return NO;
}

-(void)menuWillAppear
{
    [self.searchBar resignFirstResponder];
}

#pragma mark - Data List

-(void)loadTrendingSearches
{
    //cached?
    if (self.suggestions)
    {
        [self.dataListView reloadSection:SUGGEST_LIST_SECTION];
        self.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
    }
    else
    {
        //load async
    }
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    //no refresh
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    //nope
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SUGGEST_LIST_SECTION)
    {
        //data is string
        NSString *searchText = self.suggestions[indexPath.row]; //already strings, not data object
        [self.searchBar resignFirstResponder];
        self.searchBar.text = searchText;
        [self pushSearchResultsViewControllerWithSearchText:searchText withOwner:self.owner];
    }
    else if (indexPath.section == DETECTOR_LIST_SECTION)
    {
        if ([RVSApplication application].shouldDetectChannel == NO)
        {
            [self tryActivatingChannelDetect:YES];
        }
        else
        {
            //toggle settings off
            [RVSApplication application].shouldDetectChannel = NO;
        }
    }
}

-(void)tryActivatingChannelDetect:(BOOL)showFailMessage
{
    [[AVAudioSession sharedInstance] requestRecordPermission:^(BOOL granted) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (granted)
            {
                
                [RVSApplication application].shouldDetectChannel = YES;
            }
            else
            {
                [RVSApplication application].shouldDetectChannel = NO;
                
                if (showFailMessage)
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"microphone permission error title", nil) message:NSLocalizedString(@"microphone permission error message", nil) delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];
                    [alert show];
                }
            }
        });
            
    }];
}

#pragma mark - DataListView DataSource

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath
{ 
    if (indexPath.section == SUGGEST_LIST_SECTION)
        return [RVSAppSearchSuggestionCell class];
    
    if (indexPath.section == DETECTOR_LIST_SECTION)
        return [RVSAppSearchChannelDetectorCell class];
    
    return nil;
}

-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == SUGGEST_LIST_SECTION)
        return self.suggestions[indexPath.row];
    
    if (indexPath.section == DETECTOR_LIST_SECTION)
        return self.channelData;
    
    return nil;
}

-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section
{
    if (section == SUGGEST_LIST_SECTION)
        return [self.suggestions count];
    
    if (section == DETECTOR_LIST_SECTION)
    {
        if (self.channelDetectorEnabled)
            return 1;
        else
            return 0;
    }
    
    return 0;
}

-(Class)dataListView:(RVSAppDataListView *)dataListView headerClassForItemAtSection:(NSInteger)section
{
    if (section == SUGGEST_LIST_SECTION)
        return [RVSAppHeaderView class];
    
    return nil;
}

-(RVSAppHeaderDataObject *)dataListView:(RVSAppDataListView *)dataListView headerDataForItemAtSection:(NSInteger)section
{
    if (section == SUGGEST_LIST_SECTION)
        return [RVSAppHeaderDataObject dataWithTitle:NSLocalizedString(@"search header trending", nil)];
    
    return nil;
}

-(CGFloat)dataListView:(RVSAppDataListView *)dataListView minimumLineSpacingForSection:(NSInteger)section
{
    return 0.5f;
}

#pragma mark - Keyboard


#pragma mark - Transition

-(BOOL)usesPushTransitionController
{
    return YES;
}

-(RVSAppTransitionController *)pushTransitionController
{
    return [RVSAppCrossFadeTransitionController new];
}

-(BOOL)usesPopTransitionController
{
    return NO;
}

-(RVSAppTransitionController *)popTransitionController
{
    RVSAppTransitionController *animator = [RVSAppCrossFadeTransitionController new];
    animator.reverse = YES;
    
    return animator;
}

#pragma mark - Suggestion Data

-(void)loadSuggestionsFromDisk
{
    self.suggestions = [[NSArray arrayWithContentsOfFile:[self dataFilePath]] mutableCopy];
    
    if ([self.suggestions count] == 0) //nothing on disk - use def.
    {
        [self saveSuggestions:@[@"CNN", @"ESPN", @"Game Of Thrones", @"Justified", @"River Monsters", @"Veep", @"World", @"Super Cars", @"Archer"]];
    }
}

-(void)saveSuggestions:(NSArray *)values
{
    if (!values)
        return;
    
    //updated self
    self.suggestions = [values mutableCopy];
    
    // write the plist back to the documents directory
    [self.suggestions writeToFile:[self dataFilePath] atomically:YES];
}

-(NSString *)dataFilePath
{
    // get the path to the plist file
    NSArray *paths =  NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:DATA_FILE_NAME];
    
    return filePath;
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
