//
//  RVSAppPostCommentsView.h
//  rvs_app2
//
//  Created by Barak Harel on 6/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>
#import <rvs_sdk_api.h>
#import "RVSAppViewDelegate.h"
#import "RVSAppUserView.h"

@interface RVSAppCommentListView : UIView
@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *messageLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic) CGFloat fontPointSize;
@property (nonatomic) CGFloat minimumLineHeight;
@property (nonatomic) CGFloat maximumLineHeight;

@property (nonatomic) BOOL shouldParseHotwords;

/**
 *  Set data for view
 */
-(void)setComment:(NSObject <RVSComment> *)comment delegate:(id <RVSAppViewDelegate>)delegate;

-(void)clear;
@end
