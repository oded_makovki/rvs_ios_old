//
//  RVSAppPostCommentsView.m
//  rvs_app2
//
//  Created by Barak Harel on 6/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"
#import "TTTAttributedLabel+RVSApplication.h"
#import "RVSAppDateUtil.h"

@interface RVSAppCommentListView() <TTTAttributedLabelDelegate>
@property (nonatomic) NSObject <RVSComment> *comment;
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;
@end

@implementation RVSAppCommentListView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostCommentsView];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostCommentsView];
}

-(void)initRVSAppPostCommentsView
{
    self.shouldParseHotwords = YES;
    self.backgroundColor = [UIColor clearColor];
    self.messageLabel.delegate = self;
    
    self.minimumLineHeight = 16.0f;
    self.maximumLineHeight = 30.0f;
    self.fontPointSize = 12.0f;
    
    if  (self.timeLabel)
    {
        self.timeLabel.textColor = [UIColor rvsCommentTimestampColor];
        self.timeLabel.font = [UIFont rvsRegularFontWithSize:12];
    }
    
    if (self.userView.userNameLabel)
    {
        self.userView.userNameLabel.textColor = [UIColor rvsPostUserNameColor];
    }
    
    if (self.userView.userThumbnail)
    {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userViewTapped:)];
        [self.userView.userThumbnail addGestureRecognizer:tap];
    }
    
    [self clear];
}

-(void)clear
{
    self.timeLabel.text = @"";
    self.messageLabel.text = @"";
    [self.userView clear];
}

-(void)setComment:(NSObject <RVSComment> *)comment delegate:(id<RVSAppViewDelegate>)delegate
{
    self.delegate = delegate;
    self.comment = comment;
    
    if (!comment)
        return;
    
    if (self.userView)
    {
        [self.userView setUser:comment.author];
    }
    
    [self updateTimeLabel];
    [self updateMessageLabel];
    
    [self parseText];
}

-(void)updateMessageLabel
{
    [self.messageLabel setDefaultLinkStyleAttributesWithFont:[UIFont rvsBoldFontWithSize:self.fontPointSize] activeColor:nil inactiveColor:[UIColor rvsTintColor]];
    
    NSDictionary *titleAttributes = @{NSForegroundColorAttributeName: [UIColor rvsCommentUserNameColor],                                                                                                                              NSFontAttributeName: [UIFont rvsBoldFontWithSize:self.fontPointSize]};
    
    NSDictionary *textAttributes = @{NSForegroundColorAttributeName: [UIColor rvsCommentTextColor],                                                                                                                              NSFontAttributeName: [UIFont rvsRegularFontWithSize:self.fontPointSize]};
    
    NSMutableAttributedString *attributedString = [NSMutableAttributedString new];
    
    NSMutableAttributedString *userName;
    if (!self.userView)
    {
        userName = [[NSMutableAttributedString alloc] initWithString:self.comment.author.name attributes:titleAttributes];
        
        if (userName.length > 0)
        {
            [attributedString appendAttributedString:userName];
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
        }
    }
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:self.comment.message attributes:textAttributes];
    
    if (text.length > 0)
        [attributedString appendAttributedString:text];
    
    //paragraph style
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = self.minimumLineHeight;
    paragraphStyle.maximumLineHeight = self.maximumLineHeight;
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    
    self.messageLabel.attributedText = attributedString;
    
    if (userName.length > 0)
    {
        NSString* linkURLString = [NSString stringWithFormat:@"user://%@", self.comment.author.userId]; // build the "hash:" link
        [self.messageLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:NSMakeRange(0, userName.length)]; // add it
    }
}

#pragma mark - Parse post

- (void)parseText
{
    if (!self.messageLabel.attributedText)
        return;
    
    NSInteger length = self.messageLabel.attributedText.length;
    NSString *text = self.messageLabel.attributedText.string;
    
    if (self.shouldParseHotwords)
    {
        // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
        
        [userRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "@xxx" user mention found, add a custom link:
             NSString* user = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"user://%@", user]; // build the "user:" link
             [self.messageLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];
        
        // Detect all "#xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
        
        [hashRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "#xxx" hash found, add a custom link:
             NSString* topic = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"hash://%@", topic]; // build the "hash:" link
             [self.messageLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
                          
         }];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if (url && [self.delegate respondsToSelector:@selector(view:didSelectLinkWithURL:)])
    {
        [self.delegate view:self didSelectLinkWithURL:url];
    }
}

-(void)updateTimeLabel
{
    if (self.timeLabel)
    {
        if (!self.comment.creationTime)
        {
            self.timeLabel.text = @"";
        }
        else
        {
            self.timeLabel.text = [RVSAppDateUtil timeShortStringSinceTime:[self.comment.creationTime doubleValue] / 1000];
        }
    }
}

#pragma mark - Actions

-(void)userViewTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(view:didSelectUser:)])
    {
        [self.delegate view:self didSelectUser:self.comment.author];
    }
}

@end
