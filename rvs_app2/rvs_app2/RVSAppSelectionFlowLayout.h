//
//  RVSAppTimeSelectionFlowLayout.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RVSAppSelectionFlowLayout;

@protocol RVSAppSelectionDelegateFlowLayout <UICollectionViewDelegateFlowLayout>
- (BOOL)collectionView:(UICollectionView *)collectionView
                layout:(RVSAppSelectionFlowLayout *)collectionViewLayout
shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface RVSAppSelectionFlowLayout : UICollectionViewFlowLayout
@property (weak, nonatomic) id<RVSAppSelectionDelegateFlowLayout> delegate;
@end
