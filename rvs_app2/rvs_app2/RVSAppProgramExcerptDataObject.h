//
//  RVSAppProgramExcerptDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVSAppProgramExcerptView.h"

@interface RVSAppProgramExcerptDataObject : NSObject
@property (nonatomic) NSObject <RVSProgramExcerpt> *programExcerpt;
@property (weak, nonatomic) id <RVSAppProgramExcerptViewDelegate> programExcerptViewDelegate;
@property (nonatomic) NSArray *searchMatches; //of <RVSSearchMatch>


-(id)initWithProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt programExcerptViewDelegate:(id <RVSAppProgramExcerptViewDelegate>)programExcerptViewDelegate searchMatches:(NSArray *)searchMatches;

#pragma mark - Helpers

+(RVSAppProgramExcerptDataObject *)dataObjectFromProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt programExcerptViewDelegate:(id<RVSAppProgramExcerptViewDelegate>)programExcerptViewDelegate searchMatches:(NSArray *)searchMatches;
@end
