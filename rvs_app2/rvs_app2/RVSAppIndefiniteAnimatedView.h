//
//  RVSAppIndefiniteAnimatedView.h
//  rvs_app2
//
//  Created by Barak Harel on 6/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppIndefiniteAnimatedView : UIView

@property (nonatomic, assign) CGFloat strokeThickness;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, strong) UIColor *strokeColor;

@end
