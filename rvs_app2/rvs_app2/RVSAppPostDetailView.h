//
//  RVSAppPostDetailView.h
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostView.h"
#import "RVSAppViewDelegate.h"

@protocol RVSAppPostDetailViewDelegate <RVSAppViewDelegate>

@optional
//TBD
@end

@interface RVSAppPostDetailView : RVSAppPostView

@property (weak, nonatomic) IBOutlet UIButton *likeToggleButton;
@property (weak, nonatomic) IBOutlet UIButton *commentButton;
@property (weak, nonatomic) IBOutlet UIButton *repostToggleButton;
@property (weak, nonatomic) IBOutlet UIButton *relatedButton;

@property (weak, nonatomic) id <RVSAppPostDetailViewDelegate> delegate;

@end
