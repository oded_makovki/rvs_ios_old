//
//  RVSAppProgramExcerptListCell.m
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppProgramExcerptListCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppProgramExcerptListCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppProgramExcerptListCell];
    }
    
    return self;
}

-(void)initRVSAppProgramExcerptListCell
{
    [self loadContentsFromNib];
    
    self.programExcerptView.clipsToBounds = YES;    
    
    [self prepareForReuse];
}

-(void)setData:(RVSAppProgramExcerptDataObject *)data
{
    [super setData:data];
    if (self.isSizingCell)
    {
        [self.programExcerptView setProgramExcerpt:data.programExcerpt searchMatches:nil isSizingView:YES];
    }
    else
    {
        self.programExcerptView.delegate = data.programExcerptViewDelegate;
        [self.programExcerptView setProgramExcerpt:data.programExcerpt searchMatches:data.searchMatches isSizingView:NO];
    }
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.programExcerptView clear];
}

-(CGSize)sizeForData:(RVSAppProgramExcerptDataObject *)data
{
    if ([data.searchMatches count] > 1)
    {
        return CGSizeMake(320, 263);
    }
    else
    {
        return CGSizeMake(320, 253);
    }
}


@end
