//
//  RVSAppPostListFactory.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostListFactory.h"

@interface RVSAppPostListFactory()
@property (nonatomic) id parameter;
@end

@implementation RVSAppPostListFactory

-(id)initWithTrendingPosts
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeTrendingPosts;
    }
    return self;
}

-(id)initWithRecentPosts
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeRecentPosts;
    }
    return self;
}

-(id)initWithUserFeedForUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeUserFeed;
        _parameter = userId;
    }
    return self;
}

-(id)initWithNewsFeedForUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeNewsFeed;
        _parameter = userId;
    }
    return self;
}

-(id)initWithLikedPostsByUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppPostListFactoryTypeUserLikedPosts;
        _parameter = userId;
    }
    return self;
}

-(NSObject<RVSAsyncList> *)postList
{
    switch (self.type)
    {            
        case RVSAppPostListFactoryTypeTrendingPosts:
            return [[RVSSdk sharedSdk] trendingPosts];
            break;
            
        case RVSAppPostListFactoryTypeRecentPosts:
            return [[RVSSdk sharedSdk] trendingPosts];
            break;
            
        case RVSAppPostListFactoryTypeUserFeed:
            return [[RVSSdk sharedSdk] userFeedForUser:self.parameter];
            break;
            
        case RVSAppPostListFactoryTypeNewsFeed:
            return [[RVSSdk sharedSdk] newsFeedForUser:self.parameter];
            break;
            
        case RVSAppPostListFactoryTypeUserLikedPosts:
            return [[RVSSdk sharedSdk] likedPostsByUser:self.parameter];
            break;
            
        default:
            return nil;
            break;
    }
}

@end
