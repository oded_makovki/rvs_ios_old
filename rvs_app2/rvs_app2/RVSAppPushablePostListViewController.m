//
//  RVSAppPushablePostListViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPushablePostListViewController.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

#import "RVSApplication.h"
#import "UIFont+RVSApplication.h"

#import "RVSAppPostDataHelper.h"
#import "RVSAppPostCommentsDataObject.h"

#import "RVSAppUserDataObject.h"
#import "RVSAppMyUserDetailCell.h"
#import "RVSAppOtherUserDetailCell.h"

#import "NSArray+RVSApplication.h"

#define HEADER_LIST_SECTION 0
#define POSTS_LIST_SECTION 1

@interface RVSAppPushablePostListViewController () <RVSAppDataListViewDelegate, RVSAppDataListViewDataSource, RVSAppViewDelegate, RVSAppCommonViewControllerDelegate, RVSAppUserDetailViewDelegate>
@property (nonatomic) NSObject <RVSAsyncList>*postList;
@property (nonatomic) RVSPromise *postsDataPromise;
@property (nonatomic) NSMutableArray *postsData;

@property (nonatomic) BOOL shouldClearData;

@property (nonatomic) RVSNotificationObserver *followUserDidChangeObserver; //RVSApplicationUserFollowDidChangeNotification

@property (nonatomic) RVSPromiseHolder *userPromise;
@property (nonatomic) RVSAppUserDataObject *userData;

@property (nonatomic) BOOL isSinglePostView;
@property (nonatomic) RVSPromiseHolder *singlePostDataPromise;
@end

@implementation RVSAppPushablePostListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppPushablePostListViewController];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPushablePostListViewController];
}

-(void)initRVSAppPushablePostListViewController
{
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataListView.delegate = self;
    self.dataListView.dataSource = self;
    self.dataListView.nextPageTriggerDistance = 0;
    
    [self.dataListView setNumberOfSections:2];
    
    self.dataListView.nextPageTriggerSection = POSTS_LIST_SECTION;
    
//    self.postId = @"211950cfc3514c7282d479f82f8b23f6"; //debug
    
    [self loadData];
}


-(void)loadData
{
    if (self.singlePostId)
    {
        [self loadSinglePost];
    }
    else if (self.postListFactory.type == RVSAppPostListFactoryTypeUserFeed)
    {
        [self loadUser];
    }
    else if (self.postListFactory)
    {
        [self loadPosts];
    }
}

-(void)loadSinglePost
{
    if (!self.singlePostId)
        return;
    
    [self prepareNoPostDataView];
    
    __weak typeof(self) weakSelf = self;
    
    self.singlePostDataPromise = [[RVSSdk sharedSdk] postForPostId:self.singlePostId].newHolder;
    
    [self.singlePostDataPromise thenOnMain:^(NSObject <RVSPost> *post) {
        
        [weakSelf clearDataIfNeeded];
        
        NSArray *newData = [RVSAppPostDataHelper dataObjectsForPost:post delegate:weakSelf];
        
        [weakSelf.postsData addObjectsFromArray:newData];
        [weakSelf.dataListView reloadSection:POSTS_LIST_SECTION];
        
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        
    } error:^(NSError *error) {
        if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
            return; //canceled
        
        [weakSelf clearDataIfNeeded];
        
        weakSelf.noDataView.hidden = NO;
        weakSelf.dataListView.hidden = YES;
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
    }];
}

-(void)loadUser
{
    if (!self.userId)
        return;
    
    [self prepareNoUserDataView];
    
    __weak typeof(self) weakSelf = self;
    
    self.userPromise = [[RVSSdk sharedSdk] userForUserId:self.userId].newHolder;
    
    [self.userPromise thenOnMain:^(NSObject <RVSUser> *user) {
        
        [weakSelf clearDataIfNeeded];
        
        weakSelf.userData = [[RVSAppUserDataObject alloc] init];
        weakSelf.userData.user = user;
        weakSelf.userData.userViewDelegate = weakSelf;
        
        [weakSelf.dataListView reloadSection:HEADER_LIST_SECTION];

        //cool continue
        [weakSelf loadPosts];
        
    } error:^(NSError *error) {
        if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
            return; //canceled
        
        [weakSelf clearDataIfNeeded];
        
        weakSelf.noDataView.hidden = NO;
        weakSelf.dataListView.hidden = YES;
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
    }];
}

-(void)prepareNoUserDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 120)];
    noDataView.backgroundColor = [UIColor clearColor];
    noDataView.center = self.view.center;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(55, 10, 90, 90)];
    imageView.image = [UIImage imageNamed:@"user_not_found"];
    [noDataView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 180, 20)];
    label.font = [UIFont rvsBoldFontWithSize:14];
    label.textColor = [UIColor rvsBackgroundTextColor];
    label.text = NSLocalizedString(@"user detail user not found", nil);
    label.textAlignment = NSTextAlignmentCenter;
    [noDataView addSubview:label];
    
    [self.view addSubview:noDataView];
    self.noDataView = noDataView;
    self.noDataView.hidden = YES;
}

-(void)prepareNoPostDataView
{
    UIView *noDataView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 120)];
    noDataView.backgroundColor = [UIColor clearColor];
    noDataView.center = self.view.center;
    
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(55, 10, 90, 90)];
//    imageView.image = [UIImage imageNamed:@"user_not_found"];
//    [noDataView addSubview:imageView];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, 180, 20)];
    label.font = [UIFont rvsBoldFontWithSize:14];
    label.textColor = [UIColor rvsBackgroundTextColor];
    label.text = NSLocalizedString(@"post detail post not found", nil);
    label.textAlignment = NSTextAlignmentCenter;
    [noDataView addSubview:label];
    
    [self.view addSubview:noDataView];
    self.noDataView = noDataView;
    self.noDataView.hidden = YES;
}

-(void)setPostListFactory:(RVSAppPostListFactory *)postListFactory
{
    _postListFactory = postListFactory;
    
    //feed should update if you start following someone
    if (_postListFactory.type == RVSAppPostListFactoryTypeNewsFeed ||
        _postListFactory.type == RVSAppPostListFactoryTypeTrendingPosts ||
        _postListFactory.type == RVSAppPostListFactoryTypeRecentPosts ||
        (_postListFactory.type == RVSAppPostListFactoryTypeUserFeed && [[RVSApplication application] isMyUserId:self.userId]))
    {
        __weak typeof(self) weakSelf = self;
        self.followUserDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationUserFollowDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
            [weakSelf reloadData];
        }];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}


- (void)reloadData
{
    //cancel current
    [self.userPromise.promise cancel];
    [self.postsDataPromise cancel];
    [self.singlePostDataPromise.promise cancel];
    
    self.shouldClearData = YES; //clear on load
    self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault; //prevent request next
    
    [self loadData];
}

- (void)loadPosts
{
    self.postList = [self.postListFactory postList];
    
    [self loadNextPosts];    
}

-(void)loadNextPosts;
{
    NSInteger postCount = 5;
    __weak typeof(self) weakSelf = self;
    
    NSLog(@"loadNextPosts");
    self.postsDataPromise = [self.postList next:postCount];
    [self.postsDataPromise thenOnMain:^(NSArray *posts){
        
        [weakSelf clearDataIfNeeded];
        
        NSInteger startRow = [weakSelf.postsData count];
        NSArray *newData = [RVSAppPostDataHelper dataObjectsForPosts:posts delegate:weakSelf];
        
        weakSelf.dataListView.nextPageTriggerDistance = [newData count] / 2;
        
        [weakSelf.postsData addObjectsFromArray:newData];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:POSTS_LIST_SECTION startRow:startRow count:[newData count]]];
        
        if (weakSelf.postList.didReachEnd)
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        }
        else
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
        }
        
        NSLog(@"data count: %d, load phase: %d promise: %@", [weakSelf.postsData count], weakSelf.dataListView.loadPhase, weakSelf.postsDataPromise);
        
    } error:^(NSError *error) {
        
        if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
            return; //canceled
        
        [weakSelf clearDataIfNeeded];
        [weakSelf.dataListView reloadSection:POSTS_LIST_SECTION];
        
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
    }];
}

-(void)clearDataIfNeeded
{
    NSLog(@"clearDataIfNeeded");
    if (self.shouldClearData)
    {
        NSLog(@"clearing...");
        self.shouldClearData = NO;
        self.userData = nil;
        [self.postsData removeAllObjects];
        
        NSMutableIndexSet *sections = [NSMutableIndexSet indexSet];
        [sections addIndex:HEADER_LIST_SECTION];
        [sections addIndex:POSTS_LIST_SECTION];
        [self.dataListView reloadSections:sections];
    }
}

#pragma mark - DataListView Delegate

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf loadNextPosts];
    });
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    [self reloadData];
}

#pragma mark - DataListView DataSource

-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section
{
    if (section == POSTS_LIST_SECTION)
        return [self.postsData count];
    
    if (section == HEADER_LIST_SECTION && self.userData)
        return 1;
    
    return 0;
}

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == POSTS_LIST_SECTION)
    {
        return [RVSAppPostDataHelper classForDataObject:self.postsData[indexPath.row]];
    }
    else if (indexPath.section == HEADER_LIST_SECTION)
    {
        return [self profileHeaderClass];
    }
    
    return nil;
}

-(Class)profileHeaderClass
{
    if ([[RVSApplication application] isMyUserId:self.userId])
    {
        return [RVSAppMyUserDetailCell class];
    }
    else
    {
        return [RVSAppOtherUserDetailCell class];
    }
}

-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == POSTS_LIST_SECTION)
        return self.postsData[indexPath.row];
    
    return self.userData;
}

-(UIEdgeInsets)dataListView:(RVSAppDataListView *)dataListView insetForSectionAtIndex:(NSInteger)section
{
    if (section == POSTS_LIST_SECTION)
        return UIEdgeInsetsMake(0, 0, 8, 0);
    
    else
    {
        return UIEdgeInsetsZero;
    }
}

-(NSMutableArray *)postsData
{
    if (!_postsData)
    {
        _postsData = [NSMutableArray array];
    }
    
    return _postsData;
}

#pragma mark - PostView Delegate

-(void)view:(UIView *)view didSelectUser:(NSObject<RVSUser> *)user
{
    if (![self view:view canShowUser:user.userId])
        return;
    
    [super view:view didSelectUser:user];
}

-(BOOL)view:(UIView *)view canShowUser:(NSString *)userId
{
    if ([self.userId isEqualToString:userId])
    {
        //same user! animate can't push.
        
        CAKeyframeAnimation *animation;
        animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
        animation.values = [NSArray arrayWithObjects:@0,@(5),@(0), nil];
        animation.keyTimes = [NSArray arrayWithObjects:@0,@0.5,@1.0, nil];
        
        animation.autoreverses = NO;
        animation.repeatCount = 1; // Play it just once, and then reverse it
        animation.duration = 0.3;
        [view.layer addAnimation:animation forKey:nil];
        
        return NO;
    }
    
    return YES;
}

-(void)view:(UIView *)view didSelectLinkWithURL:(NSURL *)url
{
    if ([url.scheme isEqualToString:@"user"] && ![self view:view canShowUser:[url host]])
        return;
    
    [super view:view didSelectLinkWithURL:url];
}

#pragma mark - User Detail View Delegate

-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectFollowingCountForUser:(NSObject<RVSUser> *)user
{
    [self pushUserListViewControlWithFollowedByUserId:user.userId];
}

-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectFollowersCountForUser:(NSObject<RVSUser> *)user
{
    [self pushUserListViewControlWithFollowersOfUserId:user.userId];
}

-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectLikesCountForUser:(NSObject<RVSUser> *)user
{
    [self pushPostListViewControlWithLikedPostesUserId:user.userId];
}

-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectMentionsForUser:(NSObject<RVSUser> *)user
{
    if ([[RVSApplication application] isMyUserId:user.userId])
    {
        [self showMyMentions];
    }
}

-(void)showMyMentions
{
    //    [[RVSApplication application].activeClipPlayerView stop];
    //
    //    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    //
    //    vc.navigationItem.title = NSLocalizedString(@"mentions title", nil);
    //    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithMyMentions];
    //
    //    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Common Delegate

-(void)networkStatusDidChange
{
    if (self.dataListView.loadPhase == RVSAppDataListLoadPhaseEndOfList && [self.postsData count] == 0 && [RVSApplication application].hasNetworkConnection)
    {        
        [self reloadData];
    }
}

-(void)applicationDidCreatePost:(NSObject <RVSPost> *)post
{
    [self reloadData];
}

-(void)userDidSignIn
{
    NSLog(@"userDidSignIn");
    [self reloadData];
}

-(void)applicationDidEnterBackgroundAt:(NSDate *)didEnterBackgroundDate andWillEnterForgroundAt:(NSDate *)willEnterForgroundDate
{
    if ([willEnterForgroundDate timeIntervalSinceDate:didEnterBackgroundDate] > 120) //was in background for more then 2 minutes
    {
        [self reloadData];
    }
}

-(void)applicationWillDeletePostId:(NSString *)postId
{
    NSIndexSet *deleteIndexes = [RVSAppPostDataHelper indexesInDataArray:self.postsData containingPostId:postId];
    
    if ([deleteIndexes count] > 0)
    {
        NSArray *deleteIndexPaths = [RVSAppDataListView indexPathsForIndexes:deleteIndexes inSection:POSTS_LIST_SECTION];
        
        //remove
        [self.postsData removeObjectsAtIndexes:deleteIndexes];
        //delete
        [self.dataListView deleteItemsAtIndexPaths:deleteIndexPaths];
    }
}

-(void)applicationDidCreateComment:(NSObject<RVSComment> *)comment forPostId:(NSString *)postId
{
    [self refreshPostCommentsWithPostId:postId];
}

-(void)applicationDidDeleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId
{
    [self refreshPostCommentsWithPostId:postId];
}

-(void)applicationDidUpdatePostId:(NSString *)postId
{
    [self refreshPostInfoWithPostId:postId];
}

-(void)refreshPostCommentsWithPostId:(NSString *)postId
{
    NSIndexSet *refreshIndexes = [RVSAppPostDataHelper indexesInDataArray:self.postsData containingCommentsWithPostId:postId];
    
    if ([refreshIndexes count] > 0)
    {
        NSArray *refreshIndexPaths = [RVSAppDataListView indexPathsForIndexes:refreshIndexes inSection:POSTS_LIST_SECTION];
        
        //refresh
        [self.dataListView reloadItemsAtIndexPaths:refreshIndexPaths];
    }
}

-(void)refreshPostInfoWithPostId:(NSString *)postId
{
    NSIndexSet *refreshIndexes = [RVSAppPostDataHelper indexesInDataArray:self.postsData containingInfoWithPostId:postId];
    
    if ([refreshIndexes count] > 0)
    {
        NSArray *refreshIndexPaths = [RVSAppDataListView indexPathsForIndexes:refreshIndexes inSection:POSTS_LIST_SECTION];
        
        //refresh
        [self.dataListView reloadItemsAtIndexPaths:refreshIndexPaths];
    }
}


@end
