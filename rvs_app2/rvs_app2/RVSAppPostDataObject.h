//
//  RVSAppPostCellData.h
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>
#import "RVSAppViewDelegate.h"

@interface RVSAppPostDataObject : NSObject

@property (nonatomic) NSObject <RVSPost> *post;
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;

-(id)initWithPost:(NSObject <RVSPost> *)post delegate:(id <RVSAppViewDelegate>)delegate;

#pragma mark - Helpers

+(instancetype)dataObjectFromPost:(NSObject <RVSPost> *)post delegate:(id <RVSAppViewDelegate>)delegate;

@end
