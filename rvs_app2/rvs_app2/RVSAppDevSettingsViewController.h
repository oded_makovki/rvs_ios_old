//
//  RVSAppSettingsViewController.h
//  rvs_app
//
//  Created by Barak Harel on 1/13/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"

@interface RVSAppDevSettingsViewController : RVSAppCommonViewController

@end
