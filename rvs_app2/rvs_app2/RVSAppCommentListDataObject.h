//
//  RVSAppCommentListDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVSAppViewDelegate.h"
#import <rvs_sdk_api.h>

@interface RVSAppCommentListDataObject : NSObject
@property (nonatomic) NSObject <RVSComment> *comment;
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;

-(id)initWithComment:(NSObject <RVSComment> *)comment delegate:(id <RVSAppViewDelegate>)delegate;

+(instancetype)dataObjectFromComment:(NSObject <RVSComment> *)comment delegate:(id<RVSAppViewDelegate>)delegate;

@end
