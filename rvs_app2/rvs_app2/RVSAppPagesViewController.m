//
//  RVSAppPagesViewController.m
//  PageTest
//
//  Created by Barak Harel on 12/15/13.
//  Copyright (c) 2013 Barak Harel. All rights reserved.
//

#import "RVSAppPagesViewController.h"

#define kPageMargin 20

@interface RVSAppPagesViewController () <UIScrollViewDelegate>

//main scroll view
@property (weak, nonatomic) UIScrollView *scrollView;

@property (nonatomic) UIView *titleView;
//contains
@property (weak, nonatomic) UIScrollView *tvScrollView;
@property (weak, nonatomic) RVSAppPageControl *tvPageControl;
@end

@implementation RVSAppPagesViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self initRVSAppPagesViewController];
}


-(void)initRVSAppPagesViewController
{
    //important!
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0,
                                                                              self.view.frame.size.width + kPageMargin,
                                                                              self.view.frame.size.height)];
    self.scrollView = scrollView;
    self.scrollView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.scrollView];
    
    self.scrollView.scrollsToTop = NO;
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.directionalLockEnabled = YES;
    self.scrollView.alwaysBounceVertical = NO;
    self.scrollView.backgroundColor = [UIColor clearColor];
    
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:[NSString stringWithFormat:@"H:|-0-[view]-(-%d)-|", kPageMargin] options:0 metrics:nil views:@{@"view":self.scrollView}]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.scrollView}]];
    
    [self initTitleView];
}

-(void)initTitleView
{
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 44)];
    
    UIScrollView *tvScrollView = [[UIScrollView alloc] initWithFrame:self.titleView.bounds];
    tvScrollView.delegate = self;
    
    self.tvScrollView = tvScrollView;
    [self.titleView addSubview:self.tvScrollView];
    self.tvScrollView.backgroundColor = [UIColor clearColor];
    self.tvScrollView.scrollsToTop = NO;
    self.tvScrollView.userInteractionEnabled = NO;
    
    [self.tvScrollView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.tvScrollView}]];
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-4-|" options:0 metrics:nil views:@{@"view":self.tvScrollView}]];
    
    
    RVSAppPageControl *pageControl = [[RVSAppPageControl alloc] initWithFrame:self.titleView.bounds];
    
    self.tvPageControl = pageControl;
    self.tvPageControl.numberOfPages = self.numberOfViews;
    self.tvPageControl.dotDiameter = 4.0f;
    self.tvPageControl.dotSpacing = 6.0f;
    self.tvPageControl.currentPage = 0;
    self.tvPageControl.userInteractionEnabled = NO;
    
    [self.titleView addSubview:self.tvPageControl];
    
    [self.tvPageControl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.tvPageControl}]];
    [self.titleView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(==16)]-0-|" options:0 metrics:nil views:@{@"view":self.tvPageControl}]];
    
    self.navigationItem.titleView = self.titleView;
}

-(void)loadPages
{
    //update layout
    [self.view layoutIfNeeded];
    
    _numberOfViews = [self.dataSource numberOfViewsFor:self];
    
    for (int index = 0; index < _numberOfViews; index++)
    {
        UIView *view = [self.dataSource pageViewController:self viewForIndex:index];
        
        if (view)
        {
            CGRect frame = self.scrollView.frame;
            frame.origin.x = index * frame.size.width;  //next page
            frame.size.width -= kPageMargin;            //remove margins
            view.frame = frame;
            
            //add view
            [self.scrollView addSubview:view];
            [self.scrollView setContentSize:CGSizeMake(frame.origin.x + frame.size.width + kPageMargin, frame.size.height)];
        }
        
        NSString *text = [self.dataSource pageViewController:self titleForIndex:index];
        
        if (text)
        {
            CGRect frame = self.tvScrollView.bounds;
            frame.origin.x = index * frame.size.width;
            
            UILabel *label = [[UILabel alloc] initWithFrame:frame];
            
            //def values
            UIFont *font = [UIFont boldSystemFontOfSize:18];
            UIColor *color = [UIColor whiteColor];
            
            if ([self.dataSource respondsToSelector:@selector(pageViewController:titleFontForIndex:)])
            {
                font = [self.dataSource pageViewController:self titleFontForIndex:index];
            }
            
            if ([self.dataSource respondsToSelector:@selector(pageViewController:titleColorForIndex:)])
            {
                color = [self.dataSource pageViewController:self titleColorForIndex:index];
            }

            label.textColor = color;
            label.font = font;
            label.textAlignment = NSTextAlignmentCenter;
            
            label.tag = 100 + index;
            label.text = text;
            [self.tvScrollView addSubview:label];
            [self.tvScrollView setContentSize:CGSizeMake(frame.size.width + frame.origin.x, frame.size.height)];
        }
    }
    
    //update pages
    self.tvPageControl.numberOfPages = _numberOfViews;    
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentSize.width == 0)
        return;
    
//    NSLog(@"frame: %@, contentSize: %@, offset: %@", NSStringFromCGSize(self.scrollView.frame.size), NSStringFromCGSize(self.scrollView.contentSize), NSStringFromCGPoint(self.scrollView.contentOffset));
    
    float pageProgress = scrollView.contentOffset.x / scrollView.frame.size.width;
    int page = round(pageProgress);
    
    float scrollRatio = scrollView.contentOffset.x / scrollView.contentSize.width;
    
    float x = scrollRatio * self.tvScrollView.contentSize.width;
    self.tvScrollView.contentOffset = CGPointMake(x, 0);
    
    //set page
    [self.tvPageControl setCurrentPage:page];
    //animate titles
    [self fadeLabelsWithCurrentPage:page pageProgress:pageProgress];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self notifyDidScroll];
}

-(void)notifyDidScroll
{
    if ([self.delegate respondsToSelector:@selector(pageViewController:didScrollToIndex:)])
    {
        [self.delegate pageViewController:self didScrollToIndex:self.tvPageControl.currentPage];
    }
}

-(void)fadeLabelsWithCurrentPage:(int)currentPage pageProgress:(float)pageProgress
{
    float pageDistance = fabs(pageProgress-currentPage);
    
    for (int i = 0; i < self.numberOfViews; i++)
    {
        UIView *label = [self.tvScrollView viewWithTag:100 + i];
        
        if (!label) //no label?
            continue;
        
        //inv if current
        if (i == currentPage)
        {
            label.alpha = pow((1-pageDistance), 3);
        }
        else
        {
            label.alpha = pow(pageDistance, 3); //power!
        }
    }
}

@end

