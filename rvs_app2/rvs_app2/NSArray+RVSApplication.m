//
//  NSArray+RVSApplication.m
//  rvs_app2
//
//  Created by Barak Harel on 5/26/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "NSArray+RVSApplication.h"
#import <rvs_sdk_api.h>

@implementation NSArray (RVSApplication)

-(NSMutableArray *)rangesFromFragmentRangesWithOffset:(NSInteger)offset
{
    NSMutableArray *ranges = [NSMutableArray arrayWithCapacity:[self count]];
    for (NSObject <RVSFragmentRange> *fragmentRange in self)
    {
        NSRange range = NSMakeRange(offset + [fragmentRange.matchCharacterOffset integerValue], [fragmentRange.matchCharacterCount integerValue]);
        [ranges addObject:[NSValue valueWithRange:range]];
    }
    
    return ranges;
}

-(NSIndexSet *)indexesInDataOfKindOfClass:(Class)class containingDataId:(NSString *)dataId inKeyPath:(NSString *)keyPath
{
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    
    [self enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        if ([obj isKindOfClass:class])
        {
            if ([[obj valueForKeyPath:keyPath] isEqualToString:dataId])
            {
                [indexes addIndex:idx];
            }
        }
    }];
    
    return indexes;
}

@end
