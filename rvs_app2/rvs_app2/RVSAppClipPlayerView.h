//
//  RVSAppClipPlayerView.h
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppSpinnerView.h"

@class RVSAppClipPlayerView;
@protocol RVSAppClipPlayerViewDelegate <NSObject>
@optional
-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSClipViewState)state;
@end

@interface RVSAppClipPlayerView : RVSClipView

@property (weak, nonatomic) id <RVSAppClipPlayerViewDelegate> playerDelegate;
@property (nonatomic, readonly) UIImageView *coverImageView;

@property (nonatomic) BOOL shouldRewindOnPlay;

-(void)reset;

-(void)setChannel:(NSObject <RVSChannel> *)channel program:(NSObject <RVSProgram> *)program;
-(void)setCoverImage:(NSObject <RVSAsyncImage> *)coverImage;
-(void)setCoverImage:(NSObject <RVSAsyncImage> *)coverImage withFallbackImage:(NSObject <RVSAsyncImage> *)fallbackImage;

@end
