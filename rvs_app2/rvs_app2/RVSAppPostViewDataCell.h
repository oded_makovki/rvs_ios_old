//
//  RVSAppPostCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppDataCell.h"
#import "RVSAppPostView.h"
#import "RVSAppPostViewDataObject.h"

@interface RVSAppPostViewDataCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppPostView *postView; //needs to be init or set
@property (nonatomic) RVSAppPostViewDataObject *data;
@end
