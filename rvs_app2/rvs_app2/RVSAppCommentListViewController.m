//
//  RVSAppCommentListViewController.m
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListViewController.h"
#import "RVSAppCommentListCell.h"
#import "RVSAppCommentListDataObject.h"

#import "RVSAppCommentListLoadMoreDataObject.h"
#import "RVSAppCommentListLoadMoreCell.h"

#import "RVSApplication.h"
#import <HPGrowingTextView.h>
#import "UIFont+RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>

#import "RVSAppPostDataHelper.h"

#import "RVSAppSwipeToEditStateMachine.h"
#import "NSArray+RVSApplication.h"


#define MORE_LIST_SECTION 0
#define COMMENT_LIST_SECTION 1

#define MAX_COMMENT_LENGTH 160

@interface RVSAppCommentListViewController () <RVSAppDataListViewDelegate, RVSAppDataListViewDataSource, RVSAppCommonViewControllerDelegate, HPGrowingTextViewDelegate>

@property (nonatomic) NSObject <RVSAsyncList> *commentList;
@property (nonatomic) RVSPromise *loadNextPromise;
@property (nonatomic) BOOL shouldClearData;
@property (nonatomic) NSMutableArray *commentsData;

@property (nonatomic) RVSPromiseHolder *signInPromise;

@property (nonatomic) RVSNotificationObserver *keyboardWillShowObserver;
@property (nonatomic) RVSNotificationObserver *keyboardWillHideObserver;

@property (nonatomic) UIView *composeContainer;
@property (nonatomic) NSLayoutConstraint *composeContainerBottomConstraint;
@property (nonatomic) NSLayoutConstraint *composeContainerContainerHeightConstraint;

@property (nonatomic) HPGrowingTextView *textView;
@property (nonatomic) UIButton *postButton;
@property (nonatomic) UILabel *charLeftLabel;

@property (nonatomic) RVSAppCommentListLoadMoreDataObject *loadMoreData;
@property (nonatomic, strong) RVSAppSwipeToEditStateMachine *swipeStateMachine;
@end

@implementation RVSAppCommentListViewController

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppCommentsViewController];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppCommentsViewController];
}

-(void)initRVSAppCommentsViewController
{
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.dataListView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.loadMoreData = [RVSAppCommentListLoadMoreDataObject data];
    
    __weak typeof(self) weakSelf = self;
    
    self.keyboardWillShowObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillShowNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillShow:notification];
                                     }];
    
    
    self.keyboardWillHideObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillHideNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillHide:notification];
                                     }];
    
    self.composeContainer = [[UIView alloc] init];
    self.composeContainer.backgroundColor = [UIColor rvsPostInfoBackgroundColor];
    
    self.composeContainer.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.composeContainerContainerHeightConstraint = [NSLayoutConstraint constraintWithItem:self.composeContainer attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationGreaterThanOrEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:44];
    
    [self.composeContainer addConstraint:self.composeContainerContainerHeightConstraint];
    
    [self.view addSubview:self.composeContainer];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[container]|" options:0 metrics:nil views:@{@"container": self.composeContainer}]];
    self.composeContainerBottomConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.composeContainer attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    [self.view addConstraint: self.composeContainerBottomConstraint];
    
    self.textView = [[HPGrowingTextView alloc] init];
    self.textView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.composeContainer addSubview:self.textView];
    
    self.textView.delegate = self;
    
    self.textView.layer.cornerRadius = 5.0f;
    self.textView.layer.borderWidth = 0.5f;
    self.textView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 50);
    
	self.textView.minNumberOfLines = 1;
	self.textView.maxNumberOfLines = 4;

	self.textView.returnKeyType = UIReturnKeyDefault;
	self.textView.font = [UIFont systemFontOfSize:15.0f];

    self.textView.internalTextView.showsHorizontalScrollIndicator = NO;
    self.textView.internalTextView.showsVerticalScrollIndicator = YES;
    self.textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    
    self.textView.backgroundColor = [UIColor whiteColor];
    self.textView.placeholder = NSLocalizedString(@"comments comment placeholder", nil);
    
    self.postButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.postButton.translatesAutoresizingMaskIntoConstraints = NO;
    [self.postButton setTitle:NSLocalizedString(@"comments post button" , nil) forState:UIControlStateNormal];
    [self.postButton setTitleColor:[UIColor rvsTintColor] forState:UIControlStateNormal];
    [self.postButton setTitleColor:[[UIColor rvsTintColor] colorWithAlphaComponent:0.5] forState:UIControlStateHighlighted];
    [self.postButton setTitleColor:[[UIColor rvsTintColor] colorWithAlphaComponent:0.3] forState:UIControlStateDisabled];
    self.postButton.titleLabel.font = [UIFont rvsBoldFontWithSize:16];
    [self.postButton addTarget:self action:@selector(postComment:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.composeContainer addSubview:self.postButton];
    
    self.charLeftLabel = [[UILabel alloc] init];
    self.charLeftLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.charLeftLabel.font = [UIFont rvsRegularFontWithSize:12];
    [self updatedCharCount];
    
    [self.composeContainer addSubview:self.charLeftLabel];
    
    [self.composeContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(5)-[textView(==258)]-(5)-[postButton]-5-|" options:0 metrics:nil views:@{@"textView":self.textView, @"postButton":self.postButton}]];
    
    [self.composeContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[charLeftLabel]-(15)-[postButton]-5-|" options:0 metrics:nil views:@{@"charLeftLabel":self.charLeftLabel, @"postButton":self.postButton}]];
    
    [self.composeContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(5)-[textView]-(5)-|" options:0 metrics:nil views:@{@"textView":self.textView}]];
    
    [self.composeContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[postButton]-(5)-|" options:0 metrics:nil views:@{@"postButton":self.postButton}]];
    
    [self.composeContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[charLeftLabel]-(10)-|" options:0 metrics:nil views:@{@"charLeftLabel":self.charLeftLabel}]];
    
    self.dataListView.refreshEnabled = NO;
    self.dataListView.dataSource = self;
    self.dataListView.delegate = self;
    self.navigationItem.title = NSLocalizedString(@"comments title", nil);
    
    [self.dataListView setNumberOfSections:2]; //more, comments
    [self.dataListView reloadSection:MORE_LIST_SECTION];
    
    [self updatedDataListBottom];
    
    self.swipeStateMachine = [[RVSAppSwipeToEditStateMachine alloc] initWithDataListView:self.dataListView];
    
    //already set - load data
    if (self.postId)
        [self loadComments];
}

-(void)updatedDataListBottom
{
    self.dataListViewBottomConstraint.constant = self.composeContainerContainerHeightConstraint.constant + self.composeContainerBottomConstraint.constant;
//    UIEdgeInsets insets = self.dataListView.contentInset;
//    insets.bottom = self.composeContainerContainerHeightConstraint.constant + self.composeContainerBottomConstraint.constant;
//    self.dataListView.contentInset = insets;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (self.shouldStartEdit)
    {
        self.shouldStartEdit = NO;
        [self.textView becomeFirstResponder];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.editing = NO;
    [self.swipeStateMachine viewDidDisappear:animated];
    [self.textView resignFirstResponder];
}

-(void)menuWillAppear
{
    self.editing = NO;
    [self.swipeStateMachine viewDidDisappear:YES];
    [self.textView resignFirstResponder];
}

#pragma mark - Data

- (void)loadComments
{
    self.commentList = [[RVSSdk sharedSdk] commentsForPost:self.postId];
    
    [self loadNextComments];
}

-(void)loadNextComments
{
    __weak typeof(self) weakSelf = self;
    self.loadNextPromise = [self.commentList next:5];
    [self.loadNextPromise thenOnMain:^(NSArray *comments) {
        
        [weakSelf clearDataIfNeeded];
        
        BOOL hadData = NO;
        if ([weakSelf.commentsData count] > 0)
            hadData = YES;
        
        //reverse iterate
        for (NSObject <RVSComment> *comment in comments)
        {
            [weakSelf.commentsData insertObject:[RVSAppCommentListDataObject dataObjectFromComment:comment delegate:weakSelf] atIndex:0];
        }
        
        //insert at start
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:COMMENT_LIST_SECTION startRow:0 count:[comments count]]];
        
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        
        if (!hadData)
        {
            [weakSelf.dataListView performSelector:@selector(scrollToBottom) withObject:nil afterDelay:0.5];
        }
        
        weakSelf.loadMoreData.isHidden = weakSelf.commentList.didReachEnd;
        weakSelf.loadMoreData.isLoading = NO;
        [weakSelf.dataListView reloadSection:MORE_LIST_SECTION];
        
    } error:^(NSError *error) {
        
        if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
            return; //canceled
        
        [weakSelf clearDataIfNeeded];
        
        [weakSelf.dataListView reloadSection:COMMENT_LIST_SECTION];
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        
    }];
}

- (void)reloadComments
{
    [self.loadNextPromise cancel]; //cancel current
    self.dataListView.loadPhase = RVSAppDataListLoadPhaseDefault; //prevent request next
    self.shouldClearData = YES; //clear on load
    
    [self loadComments];
}

-(void)clearDataIfNeeded
{
    if (self.shouldClearData)
    {
        self.shouldClearData = NO;
        [self.commentsData removeAllObjects];
        [self.dataListView reloadSections:[NSIndexSet indexSetWithIndex:COMMENT_LIST_SECTION]];
    }
}


#pragma mark - DataListView Delegate

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    //not supported, use load more button
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    //not supported
    //[self reloadComments];
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MORE_LIST_SECTION && !self.commentList.didReachEnd && !self.loadNextPromise.isPending)
    {
        self.loadMoreData.isLoading = YES;
        [self.dataListView reloadSection:MORE_LIST_SECTION];
        [self loadNextComments];
    }
}

#pragma mark - DataListView DataSource

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MORE_LIST_SECTION)
    {
        return [RVSAppCommentListLoadMoreCell class];
    }
    
    return [RVSAppCommentListCell class];
}

-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == MORE_LIST_SECTION)
    {
        return self.loadMoreData;
    }
    
    return self.commentsData[indexPath.row];
}

-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section
{
    if (section == MORE_LIST_SECTION)
    {
        return 1;
    }
    
    return [self.commentsData count];
}

-(CGFloat)dataListView:(RVSAppDataListView *)dataListView minimumLineSpacingForSection:(NSInteger)section
{
    return 0.5f;
}

-(UIEdgeInsets)dataListView:(RVSAppDataListView *)dataListView insetForSectionAtIndex:(NSInteger)section
{
    if (section == MORE_LIST_SECTION)
    {
        return UIEdgeInsetsMake(0, 0, 0.5f, 0);
    }
    
    return UIEdgeInsetsZero;
}

-(NSMutableArray *)commentsData
{
    if (!_commentsData)
        _commentsData = [NSMutableArray array];
    
    return _commentsData;
}

#pragma mark - TextView

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note
{
    NSLog(@"keyboardWillShow");
    // get keyboard size and loctaion
	CGRect keyboardBounds = [note.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [[note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [[note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    // set views with new info
    self.composeContainerBottomConstraint.constant = keyboardBounds.size.height;
    [self updatedDataListBottom];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:duration delay:0 options:curve << 16 animations:^{
        [weakSelf.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [weakSelf.dataListView scrollToBottom];
    }];
}

-(void) keyboardWillHide:(NSNotification *)note
{
    self.dataListViewBottomConstraint.constant = self.composeContainerContainerHeightConstraint.constant;
    [self.dataListView layoutIfNeeded];
    
    CGFloat duration = [[note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    UIViewAnimationCurve curve = [[note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
	
    // set views with new info
    self.composeContainerBottomConstraint.constant = 0;
    [self updatedDataListBottom];
    
    [UIView animateWithDuration:duration delay:0 options:curve << 16 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];	
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    self.composeContainerContainerHeightConstraint.constant = MAX(height + 10, 44);
    [self updatedDataListBottom];
    [self.composeContainer layoutIfNeeded];
}

-(void)growingTextViewDidChange:(HPGrowingTextView *)growingTextView
{
    [self updatedCharCount];
}

-(void)updatedCharCount
{
    NSInteger charLeft = MAX_COMMENT_LENGTH - [self cleanText].length;
    self.charLeftLabel.text = [NSString stringWithFormat:@"%ld", (long)charLeft];
    
    if (charLeft == MAX_COMMENT_LENGTH)
    {
        self.charLeftLabel.textColor = [UIColor rvsCommentTextColor];
        self.postButton.enabled = NO;
    }
    else if (charLeft < 0)
    {
        self.charLeftLabel.textColor = [UIColor rvsDarkRedColor];
        self.postButton.enabled = NO;
    }
    else
    {
        self.charLeftLabel.textColor = [UIColor rvsCommentTextColor];
        self.postButton.enabled = YES;
    }
}

- (NSString *)cleanText
{
    return [self.textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

#pragma mark - Post Comment

-(void)postComment:(UIButton *)sender
{
    __weak typeof(self) weakSelf = self;
    self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        
        [weakSelf createComment];
        
    } error:nil];
    return;
}

-(void)createComment
{
    RVSAppCreateCommentFactory *factory = [[RVSAppCreateCommentFactory alloc] initWithMessage:[self cleanText] postId:self.postId];
    [[RVSApplication application] createCommentWithFactory:factory];
    
    self.textView.text = @"";
    [self updatedCharCount];    
}

#pragma mark - Comment Delegate

-(void)view:(UIView *)view didSelectDeleteComment:(NSObject<RVSComment> *)comment
{
    [self.swipeStateMachine shutActionPaneForEditingCellAnimated:YES];
    [[RVSApplication application] deleteCommentId:comment.commentId fromPostId:self.postId];
}

-(void)view:(UIView *)view didSelectMentionCommentUser:(NSObject<RVSUser> *)user
{
    [self.swipeStateMachine shutActionPaneForEditingCellAnimated:YES];
    
    self.textView.text = [NSString stringWithFormat:@"%@%@%@ ", self.textView.text, (self.textView.text.length > 0 ? @" " : @""), user.name];
    
    [self.textView becomeFirstResponder];
}

#pragma mark - Other

-(void)networkStatusDidChange
{
    if (self.dataListView.loadPhase == RVSAppDataListLoadPhaseEndOfList && [self.commentsData count] == 0 && [RVSApplication application].hasNetworkConnection)
    {
        [self reloadComments];
    }
}

-(void)userDidSignIn
{
    [self reloadComments];
}

-(void)applicationDidCreatePost:(NSObject <RVSPost> *)post
{
    //not supported
}

-(void)applicationDidCreateComment:(NSObject<RVSComment> *)comment forPostId:(NSString *)postId
{
    if (self.postId != postId)
        return;
    
    [self.commentsData addObject:[RVSAppCommentListDataObject dataObjectFromComment:comment delegate:self]];
    
    //add
    [self.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:COMMENT_LIST_SECTION startRow:[self.commentsData count]-1 count:1]];
    
    [self.dataListView scrollToBottom];
}

-(void)applicationDidDeleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId
{
    NSIndexSet *deleteIndexes = [self.commentsData indexesInDataOfKindOfClass:[RVSAppCommentListDataObject class] containingDataId:commentId inKeyPath:@"comment.commentId"];
    
    if ([deleteIndexes count] > 0)
    {
        NSArray *deleteIndexPaths = [RVSAppDataListView indexPathsForIndexes:deleteIndexes inSection:COMMENT_LIST_SECTION];
        
        //remove
        [self.commentsData removeObjectsAtIndexes:deleteIndexes];
        //delete
        [self.dataListView deleteItemsAtIndexPaths:deleteIndexPaths];
    }
}

@end
