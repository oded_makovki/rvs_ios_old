//
//  RVSAppProgramExcerptView.m
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppProgramExcerptView.h"
#import "RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import <CCAlertView.h>
#import "NSArray+RVSApplication.h"
#import "NSAttributedString+RVSApplication.h"
#import "RVSAppSearchMatchData.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppProgramExcerptView() <RVSAppClipPlayerViewDelegate, RVSAppSearchResultMatchesViewDelegate>
@end

@implementation RVSAppProgramExcerptView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppProgramExcerptView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppProgramExcerptView];
    }
    
    return self;
}

-(void)initRVSAppProgramExcerptView
{
    if ([RVSApplication application].inDebugMode)
    {
        UIButton *debugButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [debugButton setImage:[UIImage imageNamed:@"icon_debug_mode"] forState:UIControlStateNormal];
        [debugButton setImageEdgeInsets:UIEdgeInsetsMake(-20, -20, 0, 0)];
        [debugButton addTarget:self action:@selector(handleDebugPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:debugButton];
    }
    
    if (self.searchResultMatchesView)
    {
        self.searchResultMatchesView.delegate = self;
    }
    
    if (self.composeButton)
    {
        [self.composeButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
        [self.composeButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.composeButton setTitle:NSLocalizedString(@"program action compose", nil) forState:UIControlStateNormal];
        [self.composeButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];
    }
    
    
    if (self.clipPlayerView)
    {
        self.clipPlayerView.playerDelegate = self;
    }
}

-(void)clear
{
    //clear post
    _programExcerpt = nil;
    
    //clear
    [self.clipPlayerView reset];
    
    [self.searchResultMatchesView clear];
}

-(void)setProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt searchMatches:(NSArray *)searchMatches
{
    [self setProgramExcerpt:programExcerpt searchMatches:searchMatches isSizingView:NO];
}

-(void)setProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt searchMatches:(NSArray *)searchMatches isSizingView:(BOOL)isSizingView
{
    _programExcerpt = programExcerpt;
    
    if (!_programExcerpt)
        return;
    
    _searchMatches = searchMatches;
    
    //skip this (for performance optimization)
    if (!isSizingView)
    {        
        if (self.clipPlayerView)
        {
            [self.clipPlayerView setChannel:_programExcerpt.channel program:_programExcerpt.program];
            [self.clipPlayerView setCoverImage:_programExcerpt.coverImage withFallbackImage:_programExcerpt.program.show.image];
            
            if (_programExcerpt.mediaContext)
            {
                [self.clipPlayerView setClipWithMediaContext:_programExcerpt.mediaContext startOffset:0 duration:_programExcerpt.mediaContext.duration];
            }
        }
        
        [self applyStyle];
    }
}

#pragma mark - Style

-(void)applyStyle
{
    [self addHighlightsFromSearchMatches];
}

-(void)addMatchAdditionalText
{
    if ([self.programExcerpt.transcript length] > 0)
        [self showSearchMatchWithText:self.programExcerpt.transcript];
    else if ([self.programExcerpt.program.show.synopsis length] > 0) //fallback
        [self showSearchMatchWithText:self.programExcerpt.program.show.synopsis];
    else //last resort
        [self showSearchMatchWithText:@"..."];
}

-(void)addHighlightsFromSearchMatches
{
    if (!self.searchMatches)
        return;
    
    NSObject <RVSSearchMatch> *searchMatch = [self.searchMatches firstObject];
    
    if ([searchMatch.fieldName isEqualToString:@"showName"] || [searchMatch.fieldName isEqualToString:@"channelName"])
    {
        //No highlight
        [self addMatchAdditionalText];
    }
    else
    {
        if (self.searchResultMatchesView)
        {
            NSMutableArray *matches = [NSMutableArray array];
            for (NSObject <RVSSearchMatch> *searchMatch in self.searchMatches)
            {
                RVSAppSearchMatchData *data = [RVSAppSearchMatchData dataWithSearchMatch:searchMatch];
                if (!data.referenceTime)
                    data.referenceTime = self.programExcerpt.program.start;
                
                [matches addObject:data];
            }
            
            [self.searchResultMatchesView setSearchMatchesData:matches];
        }
    }
}

-(void)showSearchMatchWithText:(NSString *)text
{
    RVSAppSearchMatchData *data = [[RVSAppSearchMatchData alloc] init];
    data.text = text;
    data.referenceTime = self.programExcerpt.program.start;
 
    [self.searchResultMatchesView setSearchMatchesData:@[data]];
}

#pragma mark - Share

-(void)share:(UIButton *)sender
{
    if (self.searchResultMatchesView.currentSearchMatchIndex >= [self.searchMatches count])
        return;
    
    if ([self.delegate respondsToSelector:@selector(programExcerptView:didSelectShareWithMediaContext:text:)])
    {
        NSObject <RVSSearchMatch> *searchMatch = self.searchMatches[self.searchResultMatchesView.currentSearchMatchIndex];
        
        NSString *defaultText = [searchMatch.valueFragment lowercaseString]; //until casing will be handled properly by the backend.
        
        NSObject <RVSMediaContext> *mediaContext = self.programExcerpt.mediaContext;
        if (searchMatch.mediaContext) //override
            mediaContext = searchMatch.mediaContext;
        
        [self.delegate programExcerptView:self didSelectShareWithMediaContext:mediaContext text:defaultText];
    }
}


#pragma mark - Clip Player Delegate

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSClipViewState)state
{
    
}

#pragma mark - Debug

-(void)handleDebugPressed:(UIButton *)sender
{
    NSString *message = self.programExcerpt.program.programId;
    if (!message)
        message = @"(Boxfish)";
    
    CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:@"Program Id" message:message];
    [alertView addButtonWithTitle:@"Ok" block:nil];
    [alertView show];
}

#pragma mark - Matches Collection View

-(void)searchResultMatchesView:(RVSAppSearchResultMatchesView *)searchResultMatchesView currentSearchMatchDidChangeWithIndex:(NSInteger)currentSearchMatchIndex
{
    if (self.searchResultMatchesView.currentSearchMatchIndex >= [self.searchMatches count])
        return;
    
    NSObject <RVSSearchMatch> *currentSearchMatch = self.searchMatches[self.searchResultMatchesView.currentSearchMatchIndex];
    
    NSLog(@"currentSearchMatchIndexDidChange: %@", currentSearchMatch);
    
    if (currentSearchMatch.mediaContext)
    {
        [self.clipPlayerView setClipWithMediaContext:[currentSearchMatch mediaContext] startOffset:0 duration:[currentSearchMatch mediaContext].duration];
    }
    
    if (currentSearchMatch.coverImage)
    {
        [self.clipPlayerView setCoverImage:currentSearchMatch.coverImage withFallbackImage:self.programExcerpt.program.show.image];
    }
    else //fallback
    {
        [self.clipPlayerView setCoverImage:self.programExcerpt.coverImage withFallbackImage:self.programExcerpt.program.show.image];
    }
}

@end
