//
//  RVSAppPostInfoDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostInfoDataObject.h"

@implementation RVSAppPostInfoDataObject

-(NSInteger)likeCount
{
    return MAX(0,[self.post.likeCount integerValue]);
}

-(NSInteger)repostCount
{
    return MAX(0,[self.post.repostCount integerValue]);
}

@end
