//
//  RVSAppCollectionViewActionsCell_Private.h
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppActionsDataCell.h"

@interface RVSAppActionsDataCell ()

@property (nonatomic) BOOL userInteractionEnabledForEditing;

@property (nonatomic, readonly, strong) UIView *actionsView;

@property (nonatomic) CGFloat swipeTrackingPosition;
@property (nonatomic, readonly) CGFloat minimumSwipeTrackingPosition;

/// If your collection view doesn't have separators between cells, you can set this to YES to display separators while editing. Default is NO.
@property (nonatomic) BOOL showsSeparatorsWhileEditing;

/// The color of the separators that will be shown while editing. Ignored if showsSeparatorsWhileEditing is NO.
@property (nonatomic, strong) UIColor *separatorColor;

// this is called during a UIView animation block for when the cell is removed
- (void)closeForDelete;

- (void)closeActionPaneAnimated:(BOOL)animate completionHandler:(void(^)(BOOL finished))handler;
- (void)openActionPaneAnimated:(BOOL)animated completionHandler:(void(^)(BOOL finished))handler;

// required calls
- (void)showEditActions;
- (void)hideEditActions;

// starts fading out the top and bottom hairline views -- they'll normally fade out at finishEditing
- (void)animateOutSwipeToEditAccessories;

@end
