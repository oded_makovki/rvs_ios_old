//
//  RVSAppDataCellManager.m
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCellManager.h"
#import "RVSAppDataCell.h"

@interface RVSAppDataCellManager()
@property (nonatomic) NSMutableDictionary *cellClasses;
@end

@implementation RVSAppDataCellManager

+ (RVSAppDataCellManager*)sharedManager
{
    static RVSAppDataCellManager* _sharedManager;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _sharedManager = [[RVSAppDataCellManager alloc] init];
    });
    
    return  _sharedManager;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        self.cellClasses = [NSMutableDictionary dictionary];
    }
    
    return self;
}

- (CGSize)sizeForClass:(Class)cellClass withData:(NSObject *)data
{
    NSAssert([cellClass isSubclassOfClass:[RVSAppDataCell class]], @"Cell class must subclass RVSAppDataCell");
    
    RVSAppDataCell *sizingCell = [self.cellClasses objectForKey:NSStringFromClass(cellClass)];
    if (!sizingCell)
    {
        NSLog(@"Adding class %@", NSStringFromClass(cellClass));
        
        sizingCell = [[cellClass alloc] initSizingCellWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
        [self.cellClasses setObject:sizingCell forKey:NSStringFromClass(cellClass)];
    }
    
    return [sizingCell sizeForData:data];
}

@end
