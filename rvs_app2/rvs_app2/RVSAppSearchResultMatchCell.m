//
//  RVSAppSearchResultMatchCell.m
//  rvs_app2
//
//  Created by Barak Harel on 5/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchResultMatchCell.h"
#import <TTTAttributedLabel.h>
#import "UIView+NibLoading.h"
#import "TTTAttributedLabel+RVSApplication.h"
#import "RVSAppDateUtil.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"
#import "NSAttributedString+RVSApplication.h"
#import "RVSAppStringUtil.h"

@interface RVSAppSearchResultMatchCell() <TTTAttributedLabelDelegate>
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (nonatomic)  RVSAppSearchMatchData *data;
@end

@implementation RVSAppSearchResultMatchCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppSearchResultMatchCell];
    }
    return self;
}

-(void)initRVSAppSearchResultMatchCell
{
    [self loadContentsFromNib];
//    self.textLabel.preferredMaxLayoutWidth = 232;
//    self.timeLabel.preferredMaxLayoutWidth = 232;
    
    if (self.timeLabel)
    {
        self.timeLabel.textColor = [UIColor rvsPostTimestampColor];
        self.timeLabel.font = [UIFont rvsRegularFontWithSize:self.timeLabel.font.pointSize];
    }
    
    if (self.textLabel)
    {
        self.textLabel.textColor        = [UIColor rvsPostTextColor];
        self.textLabel.preferredMaxLayoutWidth = self.textLabel.frame.size.width;
        self.textLabel.font = [UIFont rvsRegularFontWithSize:self.textLabel.font.pointSize];
    }
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    
    _data = nil;
    _textLabel.text = @"";
    _timeLabel.text = @"";
    _textLabel.lineBreakMode = NSLineBreakByTruncatingTail;
}


-(void)setSearchMatchData:(RVSAppSearchMatchData *)data
{
    _data = data;
    
    self.textLabel.preferredMaxLayoutWidth = self.textLabel.frame.size.width;
    self.timeLabel.preferredMaxLayoutWidth = self.timeLabel.frame.size.width;
    
    [self updateTimeLabelWithTime:[_data.referenceTime doubleValue] / 1000];
    [self updateTextLabel];
}

-(void)updateTextLabel
{
    NSString *text = [self.data.text lowercaseString]; //until casing will be handled properly by the backend.
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = 18;
    paragraphStyle.maximumLineHeight = 30;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:@{NSForegroundColorAttributeName: [UIColor rvsPostTextColor], NSFontAttributeName: [UIFont rvsRegularFontWithSize:14], NSParagraphStyleAttributeName : paragraphStyle}];
    
    self.textLabel.attributedText = [attributedText mutableCopyWithHightlightsUsingFont:[UIFont rvsBoldFontWithSize:self.textLabel.font.pointSize] color:[UIColor blackColor] ranges:self.data.highlightRanges];
    
    NSRange firstMatchRange = [[self.data.highlightRanges firstObject] rangeValue];
    if (firstMatchRange.location > 0)
    {
        [RVSAppStringUtil truncateTextInLabel:self.textLabel toDisplayRange:firstMatchRange];
    }
}

-(void)updateTimeLabelWithTime:(NSTimeInterval)time
{
    if (self.timeLabel)
    {
        if (time == 0)
        {
            self.timeLabel.text = @"";
        }
        else
        {
            self.timeLabel.text = [RVSAppDateUtil timeLongStringSinceTime:time];
        }
    }
}

@end
