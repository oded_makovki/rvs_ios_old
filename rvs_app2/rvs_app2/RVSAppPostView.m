//
//  RVSAppPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

@import AVFoundation;
#import "RVSAppPostView.h"
#import "RVSApplication.h"
#import "RVSAppDateUtil.h"
#import "UIColor+RVSApplication.h"
#import "TTTAttributedLabel+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import <CCAlertView.h>
#import "RVSAppStringUtil.h"
#import "NSArray+RVSApplication.h"
#import "NSAttributedString+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppPostView() <TTTAttributedLabelDelegate, RVSAppClipPlayerViewDelegate>
@property (weak, nonatomic) UIButton *debugButton;
@end

@implementation RVSAppPostView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostView];
    }
    
    return self;
}

-(void)initRVSAppPostView
{
    self.shouldParseHotwords = YES;
    self.isUsingLongTimeFormat = NO;
    
    if ([RVSApplication application].inDebugMode)
    {
        UIButton *debugButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [debugButton setImage:[UIImage imageNamed:@"icon_debug_mode"] forState:UIControlStateNormal];
        [debugButton setImageEdgeInsets:UIEdgeInsetsMake(-20, -20, 0, 0)];
        [debugButton addTarget:self action:@selector(handleDebugPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:debugButton];
    }
    
    if (self.textLabel)
    {
        self.textLabel.delegate = self;
        self.textLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
        [self.textLabel setDefaultLinkStyleAttributes];
        
        self.textLabel.font                    = [UIFont rvsRegularFontWithSize:14];
        self.textLabel.textColor               = [UIColor rvsPostTextColor];
        self.textLabel.preferredMaxLayoutWidth = self.textLabel.frame.size.width;
        self.textLabel.verticalAlignment       = TTTAttributedLabelVerticalAlignmentTop;
    }
    
    if  (self.timeLabel)
    {
        self.timeLabel.textColor = [UIColor rvsPostTimestampColor];
        self.timeLabel.font = [UIFont rvsRegularFontWithSize:12];
    }
    
    if (self.userView.userNameLabel)
    {
        self.userView.userNameLabel.textColor = [UIColor rvsPostUserNameColor];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userViewTapped:)];
        [self.userView.userNameLabel addGestureRecognizer:tap];
    }
    
    if (self.userView.userThumbnail)
    {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userViewTapped:)];
        [self.userView.userThumbnail addGestureRecognizer:tap];
    }
    
    if (self.clipPlayerView)
    {
        self.clipPlayerView.playerDelegate = self;
    }
    
    if (self.searchResultTitleLabel)
    {
        self.searchResultTitleLabel.textColor = [UIColor rvsSearchTextColor2];
        self.searchResultTitleLabel.font = [UIFont rvsRegularFontWithSize:12];
    }
    
    if (self.searchResultMatchLabel)
    {
        self.searchResultMatchLabel.textColor = [UIColor rvsSearchTextColor2];
        self.searchResultMatchLabel.font = [UIFont rvsRegularFontWithSize:12];
    }
}

-(void)clear
{
    //clear post
    _post = nil;
    _searchMatches = nil;
    
    self.textLabel.text = @"";
    self.timeLabel.text = @"";
    self.userView.user = nil;
    
    self.searchResultMatchLabel.text = @"";
    self.searchResultTitleLabel.text = @"";
    
    //clear
    [self.clipPlayerView reset];
}

-(void)setPost:(NSObject<RVSPost> *)post searchMatches:(NSArray *)searchMatches
{
    [self setPost:post searchMatches:nil isSizingView:NO];
}

-(void)setPost:(NSObject <RVSPost> *)post searchMatches:(NSArray *)searchMatches isSizingView:(BOOL)isSizingView
{
    _post = post;
    
    if (!_post)
        return;
    
    _searchMatches = searchMatches;
    
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:_post.text attributes:@{NSForegroundColorAttributeName: [UIColor rvsPostTextColor], NSFontAttributeName: [UIFont rvsRegularFontWithSize:14]}];
    
    //paragraph style
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = 18;
    paragraphStyle.maximumLineHeight = 30;
    
    [attributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedText.length)];
    
    self.textLabel.attributedText = attributedText;
    
    //updated labels
    [self updateTimeLabelsIsSizingView:isSizingView];
    
    //skip this (for performance optimization)
    if (!isSizingView)
    {
        if (self.userView)
        {
            [self.userView setUser:_post.author];
        }
        
        if (self.clipPlayerView)
        {
            [self.clipPlayerView setChannel:_post.channel program:_post.program];
            [self.clipPlayerView setCoverImage:_post.coverImage withFallbackImage:_post.program.show.image];
            
            if (_post.mediaContext)
            {
                [self.clipPlayerView setClipWithMediaContext:_post.mediaContext startOffset:0 duration:_post.mediaContext.duration];
            }
        }
        
        [self applyStyle];
    }
}

-(void)updateTimeLabelsIsSizingView:(BOOL)isSizingView
{
    if (isSizingView)
    {
        self.timeLabel.text = @"t";
        return;
    }
    
    if (self.timeLabel)
    {
        if (!self.post.creationTime)
        {
            self.timeLabel.text = @"";
        }
        else
        {
            if (self.isUsingLongTimeFormat)
            {
                self.timeLabel.text = [RVSAppDateUtil timeLongStringSinceTime:[self.post.creationTime doubleValue] / 1000];
            }
            else
            {
                self.timeLabel.text = [RVSAppDateUtil timeShortStringSinceTime:[self.post.creationTime doubleValue] / 1000];
            }
        }
    }    
}

#pragma mark - Style

-(void)applyStyle
{
    [self parseText];
    [self addHighlightsFromSearchMatches];
}

-(void)addHighlightsFromSearchMatches
{
    if (!self.searchMatches)
        return;
    
    NSObject <RVSSearchMatch> *searchMatch = [self.searchMatches firstObject];
    
    if ([searchMatch.fieldName isEqualToString:@"postMessage"])
    {
        [self highlightPostTextWithMatch:searchMatch];
    }
    else if ([searchMatch.fieldName isEqualToString:@"showName"])
    {
        //Do nothing
    }
    else if ([searchMatch.fieldName isEqualToString:@"channelName"])
    {
        //Do nothing
    }
    else
    {
        [self highlightMatches:self.searchMatches];
    }
}

-(void)highlightPostTextWithMatch:(NSObject <RVSSearchMatch> *)searchMatch
{
    if (self.post.text.length > 0)
    {
        [self highlightLabel:self.textLabel withMatch:searchMatch rangeOffset:0];
    }
}

-(void)highlightMatches:(NSArray *)searchMatches
{
    NSObject <RVSSearchMatch> *searchMatch = [searchMatches firstObject];
    
    if (searchMatch.valueFragment.length > 0)
    {
        if (![searchMatch.fieldName isEqualToString:@"transcript"])
        {
            NSString *title = [[RVSAppStringUtil sharedUtil] stringForSearchFieldName:searchMatch.fieldName];
            title = [NSString stringWithFormat:@"%@: ", title];
            
            self.searchResultTitleLabel.text = title;
        }
        
        self.searchResultMatchLabel.text = [searchMatch.valueFragment lowercaseString]; //until casing will be handled properly by the backend.
        [self highlightLabel:self.searchResultMatchLabel withMatch:searchMatch rangeOffset:0];
        
        NSObject <RVSFragmentRange> *fragmentRange = [searchMatch.fragmentRanges firstObject];
        NSRange range = NSMakeRange([fragmentRange.matchCharacterOffset integerValue], [fragmentRange.matchCharacterCount integerValue]);
        
        [RVSAppStringUtil truncateTextInLabel:self.searchResultMatchLabel toDisplayRange:range];
    }
    else
    {
        self.searchResultTitleLabel.text = @"";
        self.searchResultMatchLabel.text = @""; //NO Text
    }
}

-(void)highlightLabel:(UILabel *)label withMatch:(NSObject <RVSSearchMatch> *)searchMatch rangeOffset:(NSInteger)rangeOffset
{
    NSMutableArray *ranges = [searchMatch.fragmentRanges rangesFromFragmentRangesWithOffset:rangeOffset];
    
    label.attributedText = [label.attributedText mutableCopyWithHightlightsUsingFont:[UIFont rvsBoldFontWithSize:label.font.pointSize] color:[UIColor blackColor] ranges:ranges];
}

-(CGFloat)heightForSearchMatches
{
    NSObject <RVSSearchMatch> *searchMatch = [self.searchMatches firstObject];
    
    if ([searchMatch.fieldName isEqualToString:@"postMessage"] || [searchMatch.fieldName isEqualToString:@"showName"] || [searchMatch.fieldName isEqualToString:@"channelName"])
        return 0; //in line
    
    if ([self.searchMatches count] > 0)
        return 20.0f; //use label
    
    return 0;
}

#pragma mark - Parse post

- (void)parseText
{
    if (!self.textLabel.attributedText)
        return;
    
    NSInteger length = self.textLabel.attributedText.length;
    NSString *text = self.textLabel.attributedText.string;
    
    if (self.shouldParseHotwords)
    {
        // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
        
        [userRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "@xxx" user mention found, add a custom link:
             NSString* user = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"user://%@", user]; // build the "user:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];

        // Detect all "#xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
        
        [hashRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "#xxx" hash found, add a custom link:
             NSString* topic = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"hash://%@", topic]; // build the "hash:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];        
    }
}

#pragma mark - Actions

-(void)userViewTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(view:didSelectUser:)])
    {
        [self.delegate view:self didSelectUser:self.post.author];
    }
}

#pragma mark - TTTAttributedLabel Delegate

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    [self tryToOpenUrl:url];
}

-(void)tryToOpenUrl:(NSURL *)url
{
    if (url && [self.delegate respondsToSelector:@selector(view:didSelectLinkWithURL:)])
    {
        [self.delegate view:self didSelectLinkWithURL:url];
    }
}

#pragma mark - Clip Player Delegate

-(void)clipPlayerView:(RVSAppClipPlayerView *)playerView stateDidChange:(RVSClipViewState)state
{
    
}

#pragma mark - Debug

-(void)handleDebugPressed:(UIButton *)sender
{
    NSArray *accessEvents = [self.clipPlayerView playerAccessLog].events;
    NSLog(@"Collected %d access events", [accessEvents count]);
    
    for (AVPlayerItemAccessLogEvent *event in accessEvents)
    {
        NSLog(@"uri: %@ numberOfStalls: %d indicatedBitrate: %.0f observedBitrate: %.0f", event.URI, event.numberOfStalls, event.indicatedBitrate, event.observedBitrate);
    }
    
    NSArray *errorEvents = [self.clipPlayerView playerErrorLog].events;
    NSLog(@"Collected %d error events", [errorEvents count]);
    for (AVPlayerItemErrorLogEvent *event in errorEvents)
    {
        NSLog(@"error at: %@ uri: %@ comment: %@", event.date, event.URI, event.errorComment);
    }
    
    //copy to clipboard
    [[UIPasteboard generalPasteboard] setString:self.post.postId];
    
    CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:@"Post Id:" message:self.post.postId];
    [alertView addButtonWithTitle:@"Ok" block:nil];
    [alertView show];
}

@end
