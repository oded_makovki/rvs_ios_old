//
//  RVSAppUserView.m
//  rvs_app2
//
//  Created by Barak Harel on 3/4/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppUserView.h"
#import <rvs_sdk_api helpers.h>
#import "UIFont+RVSApplication.h"

@interface RVSAppUserView()
@property (nonatomic) RVSKeyPathObserver *userPropertiesObserver;
@property (nonatomic) RVSPromiseHolder *followPromise;
@property (nonatomic) RVSPromiseHolder *imagePromise;
@property (nonatomic) RVSPromiseHolder *signInPromise;

@end

@implementation RVSAppUserView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUserView];
}

-(void)initRVSAppUserView
{
    //round corners (if will have subclasses, do it there).
    self.userThumbnail.layer.cornerRadius = self.userThumbnail.frame.size.width / 2;
    self.userThumbnail.layer.masksToBounds = YES;
    self.userThumbnail.contentMode = UIViewContentModeScaleAspectFill;
    
    if (self.followToggleButton)
    {
        self.followToggleButton.hidden = YES;
        [self.followToggleButton addTarget:self action:@selector(toggleFollow:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    
    [self clear];
}

-(void)clear
{
    _user = nil;
    _userPropertiesObserver = nil;
    
    self.followToggleButton.hidden = YES;
    
    self.userNameLabel.text = @"";
    self.imagePromise = nil;
    self.userThumbnail.image = [UIImage imageNamed:@"default_profile"];
}

-(void)setUser:(NSObject<RVSUser> *)user
{
    [self setUser:user isSizingView:NO];
}

-(void)setUser:(NSObject<RVSUser> *)user isSizingView:(BOOL)isSizingView
{
    if (user  && _user == user) //reset even if nil
        return;
    
//    [self clear];
    _user = user;
    
    if (!_user)
        return;
    
    [self userDidChange];
}

-(void)userDidChange
{
    self.userNameLabel.text = self.user.name;
    self.imagePromise = [self.user.profileImage imageWithSize:self.userThumbnail.frame.size].newHolder;
    [self loadImage];
    
    [self updateActionButtons];
    [self setKvo];
    
    if ([self.delegate respondsToSelector:@selector(userView:didChangeWithUser:)])
    {
        [self.delegate userView:self didChangeWithUser:self.user];
    }
    
    //is my user?
    if ([[RVSApplication application] isMyUserId:self.user.userId])
    {
        //my user
        self.followToggleButton.hidden = YES;
    }
    else
    {
        //not
        self.followToggleButton.hidden = NO;
    }
}

- (void)setKvo
{
    __weak RVSAppUserView *weakSelf = self;
    self.userPropertiesObserver = [self.user newObserverForKeyPath:@"isFollowedByMe" options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSDictionary *change) {
        [weakSelf updateActionButtons];
    }];
}

-(void)updateActionButtons
{
    self.followToggleButton.selected = [self.user.isFollowedByMe boolValue];
}

-(void)toggleFollow:(UIButton *)sender
{
    //keep value of property in case of reuse of view before action is ended. DON'T use self.(property) !
    NSObject <RVSUser> *user = self.user;
    //must be signed in!
    __weak typeof(self) weakSelf = self;
    
    self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
    [self.signInPromise thenOnMain:^(id result) {
        
        //is my user?
        if ([[RVSApplication application] isMyUserId:user.userId])
            return; //if just signed in - make sure we this is not our user!
        
        self.followToggleButton.selected = !self.followToggleButton.selected;
        BOOL shouldNotify = NO;
        
        if (![weakSelf.user.isFollowedByMe boolValue])
        {
            shouldNotify = YES;
            weakSelf.followPromise = [[RVSSdk sharedSdk] followUser:user.userId].newHolder;
        }
        else
        {
            weakSelf.followPromise = [[RVSSdk sharedSdk] unfollowUser:user.userId].newHolder;
        }
        
        [weakSelf.followPromise thenOnMain:^(id result) {
            if (shouldNotify)
            {
                [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationUserFollowDidChangeNotification object:nil];
            }            
            
        } error:^(NSError *error) {            
            NSLog(@"error: %@", error);
        }];
        
    } error:nil];
    
}

-(void)loadImage
{    
    __weak typeof(self) weakSelf = self;
    [self.imagePromise thenOnMain:^(UIImage *image) {
        weakSelf.userThumbnail.image = image;
    } error:^(NSError *error) {
        weakSelf.userThumbnail.image = [UIImage imageNamed:@"default_profile"];
    }];
}


@end
