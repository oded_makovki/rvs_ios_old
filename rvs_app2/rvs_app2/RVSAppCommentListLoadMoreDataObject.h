//
//  RVSAppCommentListLoadMoreDatObject.h
//  rvs_app2
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppCommentListLoadMoreDataObject : NSObject
@property (nonatomic) BOOL isHidden;
@property (nonatomic) BOOL isLoading;

- (instancetype)init; // Designated initializer
+ (instancetype)data;
@end
