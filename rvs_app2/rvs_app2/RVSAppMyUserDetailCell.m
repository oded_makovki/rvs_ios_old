//
//  RVSAppMyUserDetailCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppMyUserDetailCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppMyUserDetailCell()
@end

@implementation RVSAppMyUserDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppMyUserDetailCell];
    }
    return self;
}

-(void)initRVSAppMyUserDetailCell
{
    [self loadContentsFromNib];
    
    self.userView.userNameLabel.textColor    = [UIColor blackColor];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 230);
}

@end
