//
//  RVSAppCommentListLoadMoreCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListLoadMoreCell.h"
#import "RVSAppSpinnerView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppCommentListLoadMoreCell()
@property (nonatomic) UILabel *titleLabel;
@property (nonatomic) RVSAppSpinnerView *spinner;
@end

@implementation RVSAppCommentListLoadMoreCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppCommentListLoadMoreCell];
    }
    
    return self;
}

-(void)initRVSAppCommentListLoadMoreCell
{
    UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self.contentView addSubview:label];
    
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:@{@"label":label}]];
    [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:@{@"label":label}]];
    
    label.font = [UIFont rvsLightFontWithSize:12];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.text = @"";
    label.textColor = [UIColor rvsCommentTextColor];
    label.backgroundColor = [UIColor clearColor];
    
    self.titleLabel = label;
    
    self.spinner = [[RVSAppSpinnerView alloc] initWithIndicatorImage:[UIImage imageNamed:@"tiny_spinner"] backgroundImage:nil];
    self.spinner.translatesAutoresizingMaskIntoConstraints = NO;
    self.spinner.hidesWhenStopped = YES;
    self.spinner.hidden = YES;
    [self.spinner stopAnimating];
    [self.contentView addSubview:self.spinner];
    
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.spinner attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.spinner attribute:NSLayoutAttributeCenterX multiplier:1 constant:20]];
    
    self.contentView.backgroundColor = [UIColor rvsCommentBackgroundColor];
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.titleLabel.text = @"";
}

-(CGSize)sizeForData:(RVSAppCommentListLoadMoreDataObject *)data
{
    if (data.isHidden)
    {
        return CGSizeMake(320, 0.1f);
    }
    else
    {
        return CGSizeMake(320, 30);
    }
}

-(void)setData:(RVSAppCommentListLoadMoreDataObject *)data
{
    [super setData:data];
    if (data.isLoading)
    {
        self.titleLabel.text = @"Loading...";
        [self.spinner startAnimating];
    }
    else
    {
        self.titleLabel.text = @"Load more comments...";
        [self.spinner stopAnimating];
    }
}

@end
