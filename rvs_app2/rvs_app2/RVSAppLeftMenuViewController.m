//
//  RVSAppLeftMenuViewController.m
//  rvs_app2
//
//  Created by Barak Harel on 3/13/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppLeftMenuViewController.h"
#import "RVSApplication.h"
#import "RVSAppUserView.h"
#import "UIColor+RVSApplication.h"
#import "UIColor+HexString.h"
#import "UIFont+RVSApplication.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>
#import <CCActionSheet.h>

#import "RVSAppSignInViewController.h"

NSString *RVSAppLeftMenuTitleStringKey = @"RVSAppLeftMenuTitleStringKey";
NSString *RVSAppLeftMenuIconImageKey = @"RVSAppLeftMenuIconImageKey";
NSString *RVSAppLeftMenuContentTypeKey = @"RVSAppLeftMenuContentTypeKey";
NSString *RVSAppLeftMenuIsHiddenKey = @"RVSAppLeftMenuIsHiddenKey";

@interface RVSAppLeftMenuViewController () <RVSAppUserViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;
@property (nonatomic) RVSPromiseHolder *userPromise;

@property (nonatomic) RVSNotificationObserver *contentTypeObserver;
@property (nonatomic) RVSNotificationObserver *keyboardWillShowObserver;
@property (nonatomic) RVSNotificationObserver *signInObserver;
@property (nonatomic) RVSNotificationObserver *signOutObserver;

@property (weak, nonatomic) IBOutlet UIView *contentButtonsContainer;
@property (nonatomic) NSMutableDictionary *contentButtons;
@property (weak, nonatomic) UIButton *activeButton;

@property (nonatomic) RVSPromiseHolder *signInPromise;

@property (nonatomic) BOOL isVisible;

@end

@implementation RVSAppLeftMenuViewController

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    
    self.signInObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedInNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleServiceSignedIn];
    }];
    
    self.scrollView.scrollsToTop = NO;
    self.scrollView.backgroundColor = [UIColor rvsMenuBackgroundColor];
    self.contentButtonsContainer.backgroundColor = [UIColor rvsMenuBackgroundColor2];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.userView.delegate = self;
    
    self.userView.userNameLabel.textColor = [UIColor rvsTintColor];
    
    self.userView.userThumbnail.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    self.userView.userThumbnail.layer.borderWidth = 0.5f;
    
    
    UITapGestureRecognizer *userTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showProfile:)];
    
    [self.userView addGestureRecognizer:userTapGesture];
    
    self.contentTypeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSAppMainContentTypeDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        [weakSelf handleContentTypeDidChangeNotification:notification];
    }];
    
    self.keyboardWillShowObserver = [RVSNotificationObserver newObserverForNotificationWithName:UIKeyboardWillShowNotification object:nil usingBlock:^(NSNotification *notification)
                                     {
                                         [weakSelf keyboardWillShow:notification];
                                     }];
    
    [self addMenuButtons];
    [self loadUser];
}

-(void)loadUser
{
    __weak typeof(self) weakSelf = self;
    
    if (![RVSSdk sharedSdk].isSignedOut)
    {
        self.userView.userNameLabel.text = NSLocalizedString(@"left menu loading user", nil);
        
        if ([RVSSdk sharedSdk].myUserId)
        {
            self.userPromise = [[RVSSdk sharedSdk] userForUserId:[RVSSdk sharedSdk].myUserId].newHolder;
            [self.userPromise thenOnMain:^(NSObject<RVSUser> *user){
                weakSelf.userView.user = user;
            } error:nil];
        }
    }
    else
    {
        //anonymous
        self.userView.user = nil;
        self.userView.userNameLabel.text = NSLocalizedString(@"action sign in", nil);
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    RVSVerbose(@"Menu viewWillAppear");
    self.isVisible = YES;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppMainMenuWillAppearNotification object:self];
    
    [[RVSApplication application].activeClipPlayerView pause];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    RVSVerbose(@"Menu viewDidDisappear");
    self.isVisible = NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppMainMenuDidDisappearNotification object:self];
}

-(void)handleServiceSignedIn
{
    [self loadUser];
}

-(void)addMenuButtons
{
    self.contentButtons = [NSMutableDictionary new];
    
    NSString *buttonsInfoPath = [[NSBundle mainBundle] pathForResource:@"leftMenuContentButtons" ofType:@"plist"];
    NSArray *buttonsInfo = [NSArray arrayWithContentsOfFile:buttonsInfoPath];
    
    UIView *lastView = nil;
    
    for (NSDictionary *buttonInfo in buttonsInfo)
    {
        BOOL isHidden = [buttonInfo[RVSAppLeftMenuIsHiddenKey] boolValue];
        if (isHidden)
            continue; //skip
        
        NSString *titleString = buttonInfo[RVSAppLeftMenuTitleStringKey];
        NSString *title = NSLocalizedString(titleString, nil);
        NSString *imageName = buttonInfo[RVSAppLeftMenuIconImageKey];
        UIImage *image = [UIImage imageNamed:imageName];
        RVSAppMainContentType contentType = [buttonInfo[RVSAppLeftMenuContentTypeKey] integerValue];
        
        lastView = [self addMenuButtonWithTitle:title image:image forContentType:contentType belowView:lastView];
    }
    
    [self.contentButtonsContainer addConstraint:[NSLayoutConstraint constraintWithItem:self.contentButtonsContainer attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:lastView attribute:NSLayoutAttributeBottom multiplier:1 constant:0.5]];
    self.activeButton = self.contentButtons[@(RVSAppMainContentTypeTrending)];
}

-(UIView *)addMenuButtonWithTitle:(NSString *)title image:(UIImage *)image forContentType:(RVSAppMainContentType)contentType belowView:(UIView *)belowView
{
    
    UIButton *contentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [contentButton setBackgroundImage:[UIImage imageNamed:@"menu_btn"] forState:UIControlStateNormal];
    [contentButton setBackgroundImage:[UIImage imageNamed:@"menu_btn_selected"] forState:UIControlStateSelected];
    
    [contentButton setTitle:title forState:UIControlStateNormal];
    [contentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [contentButton setImage:image forState:UIControlStateNormal];
    
    if (contentType == RVSAppMainContentTypeSettings)
    {
        [contentButton addTarget:self action:@selector(showSettings) forControlEvents:UIControlEventTouchUpInside];
    }
    else if (contentType == RVSAppMainContentTypeFeedback)
    {
        [contentButton addTarget:self action:@selector(showFeedback) forControlEvents:UIControlEventTouchUpInside];
    }
    else
    {
        [contentButton addTarget:self action:@selector(contentButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    contentButton.contentEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
    contentButton.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);

    contentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    contentButton.titleLabel.font = [UIFont rvsRegularFontWithSize:15];
    
    contentButton.tag = contentType;
    [contentButton addConstraint:[NSLayoutConstraint constraintWithItem:contentButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:45]];
    
    [self.contentButtons setObject:contentButton forKey:@(contentType)];
    [self.contentButtonsContainer addSubview:contentButton];
    [self.contentButtonsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[button]|" options:0 metrics:nil views:@{@"button":contentButton}]];
    
    
    contentButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    if (self.contentButtonsContainer)
    {
        [self.contentButtonsContainer addConstraint:[NSLayoutConstraint constraintWithItem:contentButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:belowView attribute:NSLayoutAttributeBottom multiplier:1 constant:0.5f]];
    }
    else
    {
        [self.contentButtonsContainer addConstraint:[NSLayoutConstraint constraintWithItem:contentButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentButtonsContainer attribute:NSLayoutAttributeTop multiplier:1 constant:0.5f]];
    }
    
    return contentButton;
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    if (!self.isVisible)
        return;
    
    if (![[RVSApplication application].mainViewController.centerViewController.presentedViewController isKindOfClass:[RVSAppSignInViewController class]]) //don't hide if displaying sign in vc
    {
        [[RVSApplication application].mainViewController closeDrawerAnimated:YES completion:nil];
    }
}

-(void)handleContentTypeDidChangeNotification:(NSNotification *)notification
{
    UIButton *contentButton = [self.contentButtons objectForKey:@([RVSApplication application].mainViewController.contentType)];
    self.activeButton = contentButton;
}


-(void)setActiveButton:(UIButton *)activeButton
{
    //deselect old
    _activeButton.selected = NO;
    _activeButton.titleLabel.font = [UIFont rvsRegularFontWithSize:15];
    
    //select new
    _activeButton = activeButton;
    _activeButton.titleLabel.font = [UIFont rvsRegularFontWithSize:15];
    
    [UIView transitionWithView:_activeButton
                      duration:0.2
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{ _activeButton.selected = YES; }
                    completion:nil];
}

-(void)contentButtonPressed:(UIButton *)sender
{
    RVSAppMainContentType contentType = sender.tag;
    [self showContentType:contentType];
}

-(void)showContentType:(RVSAppMainContentType)contentType
{
    if (contentType == RVSAppMainContentTypeLiked || contentType == RVSAppMainContentTypeNewsFeed || contentType == RVSAppMainContentTypeNotifications || contentType == RVSAppMainContentTypeProfile)
    {
        self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
        [self.signInPromise thenOnMain:^(id result) {
            
            [RVSApplication application].mainViewController.contentType = contentType;
            
        } error:^(NSError *error) {
            //ignore
        }];
    }
    else
    {
        [RVSApplication application].mainViewController.contentType = contentType;
    }
}


#pragma mark - Show

- (IBAction)showProfile:(id)sender
{
    [self showContentType:RVSAppMainContentTypeProfile];
}

-(void)showFeedback
{
    [[RVSApplication application] showFeedback];
}

- (void)showSettings
{
    __weak typeof(self) weakSelf = self;
    CCActionSheet *actionSheet = [[CCActionSheet alloc] initWithTitle:nil];
    [actionSheet addCancelButtonWithTitle:NSLocalizedString(@"action about", nil) block:^{
        [weakSelf showContentType: RVSAppMainContentTypeAbout];
    }];
    
    if ([RVSSdk sharedSdk].isSignedOut)
    {
        [actionSheet addDestructiveButtonWithTitle:NSLocalizedString(@"action sign in", nil) block:^{
            [[RVSApplication application] showSignInIfNeeded];
        }];
    }
    else
    {
        [actionSheet addDestructiveButtonWithTitle:NSLocalizedString(@"action sign out", nil) block:^{
            [[RVSApplication application] signOut];
        }];
    }
    [actionSheet addCancelButtonWithTitle:NSLocalizedString(@"action cancel", nil)];
    [actionSheet showInView:[RVSApplication application].mainViewController.view];
}

#pragma mark - Animation

+(MMDrawerControllerDrawerVisualStateBlock)parallaxVisualStateBlockWithParallaxFactor:(CGFloat)parallaxFactor{
    MMDrawerControllerDrawerVisualStateBlock visualStateBlock =
    ^(MMDrawerController * drawerController, MMDrawerSide drawerSide, CGFloat percentVisible){
        NSParameterAssert(parallaxFactor >= 1.0);
        CATransform3D transform = CATransform3DIdentity;
        UIViewController * sideDrawerViewController;
        if(drawerSide == MMDrawerSideLeft) {
            sideDrawerViewController = drawerController.leftDrawerViewController;
            CGFloat distance = MAX(drawerController.maximumLeftDrawerWidth,drawerController.visibleLeftDrawerWidth);
            transform = CATransform3DMakeTranslation((-distance)/parallaxFactor+(distance*percentVisible/parallaxFactor), 0.0, 0.0);
            
            ((RVSAppLeftMenuViewController *)sideDrawerViewController).shadowView.alpha = 1.00f - percentVisible;
        }
        else if(drawerSide == MMDrawerSideRight){
            //NA
        }
        
        [sideDrawerViewController.view.layer setTransform:transform];
    };
    return visualStateBlock;
}

+(MMDrawerControllerDrawerVisualStateBlock)cardVisualStateBlock{
    MMDrawerControllerDrawerVisualStateBlock visualStateBlock =
    ^(MMDrawerController * drawerController, MMDrawerSide drawerSide, CGFloat percentVisible){
        CATransform3D transform = CATransform3DIdentity;
        UIViewController * sideDrawerViewController;
        if(drawerSide == MMDrawerSideLeft) {
            sideDrawerViewController = drawerController.leftDrawerViewController;
            transform = CATransform3DScale(transform, percentVisible * 0.05f + 0.95f , percentVisible * 0.05f + 0.95f, 1.f);
            ((RVSAppLeftMenuViewController *)sideDrawerViewController).shadowView.alpha = 1.00f - percentVisible;
        }
        else if(drawerSide == MMDrawerSideRight){
            //NA
        }
        
        [sideDrawerViewController.view.layer setTransform:transform];
    };
    return visualStateBlock;
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




@end
