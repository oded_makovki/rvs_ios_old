//
//  RVSAppClipPlayerViewInlineUI.m
//  rvs_app2
//
//  Created by Barak Harel on 5/18/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppClipPlayerInlineControls.h"

@interface RVSAppClipPlayerInlineControls()

@property (weak, nonatomic) IBOutlet UIView *uiContainer;

@end

@implementation RVSAppClipPlayerInlineControls

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppClipPlayerViewInlineUI];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppClipPlayerViewInlineUI];
}

-(void)initRVSAppClipPlayerViewInlineUI
{
    [self.scrubber setMinimumTrackImage:[UIImage imageNamed:@"player_scrubber_minimum"] forState:UIControlStateNormal];
    [self.scrubber setMaximumTrackImage:[UIImage imageNamed:@"player_scrubber_maximum"] forState:UIControlStateNormal];
    [self.scrubber setThumbImage:[UIImage new] forState:UIControlStateNormal];
}

-(void)setPlayerState:(RVSClipViewState)state
{
    BOOL animated = YES;
    if (self.isReloadingData)
        animated = NO;
    
    if (state == RVSClipViewStateLoading)
    {
        [self.spinner startAnimating];
    }
    else
    {
        [self.spinner stopAnimating];
    }
    
    switch (state)
    {
        case RVSClipViewStateUnknown:    //show metadata (non-animated)
            [self showUIAnimated:NO];
            [self hideEndCardAnimated:NO];
            break;
            
        case RVSClipViewStateLoading:
            [self showUIAnimated:NO];
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePlaying: //hide metadata animated
            [self hideUIAnimated:animated];
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePaused: //show metadata animated
            if (!self.isScrubbing)
                [self showUIAnimated:animated];
            
            [self hideEndCardAnimated:animated];
            break;
            
        case RVSClipViewStatePlaybackEnded:  //show end card
            [self hideUIAnimated:animated]; //for now
            [self showEndCardAnimated:animated];
            break;
            
        default:
            break;
    }
}

#pragma mark - End Card

-(void)showEndCardAnimated:(BOOL)animated
{
    self.togglePlayGestureRecognizer.enabled = NO;
    
    self.endCardView.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.endCardView.alpha = 1.0f;
    }];
}

-(void)hideEndCardAnimated:(BOOL)animated
{
    self.togglePlayGestureRecognizer.enabled = YES;
    
    [self.endCardView setHidden:YES];
    self.endCardView.alpha = 0.0f;
}

#pragma mark - Animations

-(void)hideUIAnimated:(BOOL)animated
{
    if (animated)
    {
        [UIView animateWithDuration:1  delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.uiContainer.alpha = 0;
        } completion:nil];
    }
    else
    {
        self.uiContainer.alpha = 0;
    }
}

-(void)showUIAnimated:(BOOL)animated
{
    if (animated)
    {
        [UIView animateWithDuration:1  delay:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionBeginFromCurrentState animations:^{
            self.uiContainer.alpha = 1;
        } completion:nil];
    }
    else
    {
        self.uiContainer.alpha = 1;
    }
}

@end
