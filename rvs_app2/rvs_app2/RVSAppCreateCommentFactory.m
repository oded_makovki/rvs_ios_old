//
//  RVSAppCreateCommentFactory.m
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCreateCommentFactory.h"

@interface RVSAppCreateCommentFactory()
@property (nonatomic) NSString *message;
@property (nonatomic) NSString *postId;
@end

@implementation RVSAppCreateCommentFactory

-(id)initWithMessage:(NSString *)message postId:(NSString *)postId
{
    self = [super init];
    if (!self)
        return nil;
    
    NSAssert(message, @"No text");
    NSAssert(postId, @"No post id");

    
    self.message = message;
    self.postId = postId;
    
    return self;
}

-(RVSPromise *)createComment
{
    return [[RVSSdk sharedSdk] createCommentWithMessage:self.message forPostId:self.postId];
}

@end
