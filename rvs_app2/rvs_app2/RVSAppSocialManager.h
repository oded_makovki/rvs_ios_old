//
//  RVSAppSocialManager.h
//  rvs_app2
//
//  Created by Barak Harel on 4/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Accounts/Accounts.h>
#import <rvs_sdk_api.h>

extern NSString *RVSAppSocialManagerErrorDomain;
extern NSString *RVSAppSocialManagerTwitterOAuthTokenKey;
extern NSString *RVSAppSocialManagerTwitterOAuthTokenSecretKey;

/*!
 RVS SDK error codes
 */
typedef NS_ENUM(NSUInteger, RVSAppSocialManagerErrorCode) {
    
    RVSAppSocialManagerErrorUnknown,
    RVSAppSocialManagerErrorAccountNotFound,
    RVSAppSocialManagerErrorPermissionDenied,
};

@interface RVSAppSocialManager : NSObject

@property (nonatomic, readonly) ACAccount *facebookAccount;
@property (nonatomic, readonly) ACAccount *twitterAccount;

/**
 *  Get shared instanse of the manager
 *
 *  @return shared manager
 */
+(RVSAppSocialManager *)sharedManager;

+(BOOL)isLocalTwitterAccountAvailable;
+(BOOL)isLocalFacebookAccountAvailable;


/**
 *  Get facebook oAuth token
 *
 *  @return promise fullfiled with NSString *oauthToken
 */
-(RVSPromise *)facebookOAuthToken;

/**
 *  Get twitter oAuth token dictionary, use RVSAppSocialManagerTwitterOAuthTokenKey and RVSAppSocialManagerTwitterOAuthTokenSecretKey to get relevant values.
 *
 *  @return promise fullfiled with NSDictionary *oAuthDictionary
 */
-(RVSPromise *)twitterOAuthToken;

@end
