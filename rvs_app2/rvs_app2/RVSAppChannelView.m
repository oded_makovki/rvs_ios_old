//
//  RVSAppChannelView.m
//  rvs_app2
//
//  Created by Barak Harel on 4/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppChannelView.h"
#import "RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppChannelView()
@property (nonatomic) RVSPromiseHolder *imagePromise;
@end

@implementation RVSAppChannelView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppChannelView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppChannelView];
    }
    
    return self;
}

-(void)initRVSAppChannelView
{
    [self clear];
    if (self.composeButton)
    {
        [self.composeButton addTarget:self action:@selector(composeNow:) forControlEvents:UIControlEventTouchUpInside];
        [self.composeButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.composeButton setTitle:NSLocalizedString(@"channel action compose", nil) forState:UIControlStateNormal];
        [self.composeButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];
    }
}

-(void)clear
{
    self.titleLabel.text = @"";
    self.imageView.image = nil;
    self.imagePromise = nil;
}

-(void)setChannel:(NSObject<RVSChannel> *)channel
{
    _channel = channel;
    
    NSObject <RVSAsyncImage> *image;
    
    NSMutableAttributedString *attributedText = [NSMutableAttributedString new];
    if (channel)
    {
        image = channel.logo;
        
        NSMutableAttributedString *channelTitle = [[NSMutableAttributedString alloc] initWithString:channel.name attributes:@{NSForegroundColorAttributeName: [UIColor rvsPostTextColor], NSFontAttributeName: [UIFont rvsBoldFontWithSize:16]}];
        
        [attributedText appendAttributedString:channelTitle];
    }
    
    if (channel.currentProgram)
    {
        if (channel.currentProgram.show.image)
            image = channel.currentProgram.show.image;
       
        NSMutableAttributedString *programName = [[NSMutableAttributedString alloc] initWithString:channel.currentProgram.show.name attributes:@{NSForegroundColorAttributeName: [UIColor rvsPostTextColor], NSFontAttributeName: [UIFont rvsRegularFontWithSize:14]}];
        
        if (attributedText.length > 0) //add \n if needed
            [attributedText appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        
        [attributedText appendAttributedString:programName];
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = 18;
    paragraphStyle.maximumLineHeight = 30;
    [attributedText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedText.length)];
    
    self.titleLabel.attributedText = attributedText;
    
    if (image)
    {
        __weak typeof(self) weakSelf = self;
        
        self.imagePromise = [image imageWithSize:self.imageView.frame.size].newHolder;
        
        [self.imagePromise thenOnMain:^(UIImage *image) {
            weakSelf.imageView.image = image;
        } error:^(NSError *error) {
            weakSelf.imageView.image = nil;
        }];
    }
}


- (void)composeNow:(UIButton *)sender
{
    [[RVSApplication application] showComposeWithChannel:self.channel.channelId defaultText:nil];
}

@end
