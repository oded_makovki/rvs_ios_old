//
//  RVSAppDataListView.h
//  rvs_app
//
//  Created by Barak Harel on 11/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppDataCell.h"

/**
 *  Data list load phase
 */
typedef NS_ENUM(NSInteger, RVSAppDataListLoadPhase)
{
    /**
     *  Default
     */
    RVSAppDataListLoadPhaseDefault,
    /**
     *  Should request next data when reaching bottom of list
     */
    RVSAppDataListLoadPhaseShouldRequestNextData,
    /**
     *  (Read-only) data list is waiting for next data
     */
    RVSAppDataListLoadPhaseRequestingNextData,
    /**
     *  (Read-only) data list is waiting for data reload
     */
    RVSAppDataListLoadPhaseRequestingDataReload,
    /**
     *  List reached its end
     */
    RVSAppDataListLoadPhaseEndOfList
};

@class RVSAppDataListView;
@class RVSAppHeaderDataObject;

@protocol RVSAppDataListViewDataSource <NSObject>

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath;
-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath;
-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section;

@optional

-(Class)dataListView:(RVSAppDataListView *)dataListView headerClassForItemAtSection:(NSInteger)section;
-(RVSAppHeaderDataObject *)dataListView:(RVSAppDataListView *)dataListView headerDataForItemAtSection:(NSInteger)section;

-(CGFloat)dataListView:(RVSAppDataListView *)dataListView minimumLineSpacingForSection:(NSInteger)section;
-(UIEdgeInsets)dataListView:(RVSAppDataListView *)dataListView insetForSectionAtIndex:(NSInteger)section;
-(NSString *)descriptionForDataListView:(RVSAppDataListView *)dataListView;

@end

@protocol RVSAppDataListViewDelegate <NSObject>

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView;
-(void)refreshDataListView:(RVSAppDataListView *)dataListView;

@optional

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface RVSAppDataListView : UIView

@property (weak, nonatomic) id <RVSAppDataListViewDelegate> delegate;
@property (weak, nonatomic) id <RVSAppDataListViewDataSource> dataSource;

@property (nonatomic) BOOL refreshEnabled;

/**
 *  set/get loadPhase, see RVSAppDataListLoadPhase
 */
@property (nonatomic) RVSAppDataListLoadPhase loadPhase;

/**
 *  Distance from bottom of list that will trigger next page request
 */
@property (nonatomic) NSInteger nextPageTriggerDistance;

/**
 *  Section number that can trigger next page
 */
@property (nonatomic) NSInteger nextPageTriggerSection;

@property (nonatomic) BOOL scrollsToTop;

@property (nonatomic) BOOL showHeadersForEmptySections;

@property (nonatomic) UIScrollViewKeyboardDismissMode keyboardDismissMode;

@property (nonatomic) UIEdgeInsets contentInset;

@property (nonatomic) BOOL scrollEnabled;

//must be set!
-(void)setNumberOfSections:(NSInteger)numberOfSections;

//footer
-(void)setFooterCellClass:(Class)footerCellClass;

//scroll
-(void)scrollToTop;
-(void)scrollToBottom;
-(void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated;

- (void)insertItemsAtIndexPaths:(NSArray *)indexPaths;
- (void)deleteItemsAtIndexPaths:(NSArray *)indexPaths;
- (void)reloadItemsAtIndexPaths:(NSArray *)indexPaths;

- (void)reloadSections:(NSIndexSet *)sections;
- (void)reloadSection:(NSUInteger)section;

- (NSIndexPath *)indexPathForItemAtPoint:(CGPoint)point;
- (RVSAppDataCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath;

+(NSArray *)indexPathsForSection:(NSUInteger)section startRow:(NSUInteger)startRow count:(NSUInteger)count;
+(NSArray *)indexPathsForIndexes:(NSIndexSet *)indexes inSection:(NSUInteger)section;

@end
