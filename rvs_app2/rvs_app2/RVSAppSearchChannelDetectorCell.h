//
//  RVSAppSearchChannelDetectorCell.h
//  rvs_app2
//
//  Created by Barak Harel on 4/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppChannelCell.h"
#import "RVSAppSearchChannelDetectorActivityIndicator.h"
#import "RVSAppChannelView.h"

@interface RVSAppSearchChannelDetectorCell : RVSAppChannelCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;
@property (weak, nonatomic) IBOutlet UIImageView *micImageView;
@property (weak, nonatomic) IBOutlet UIImageView *acttionImageView;
@property (weak, nonatomic) IBOutlet RVSAppSearchChannelDetectorActivityIndicator *detectorActivityIndicator;

@property (weak, nonatomic) IBOutlet UIView *topContainer;

@end
