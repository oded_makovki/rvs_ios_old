//
//  RVSAppSpacerCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostSpacerCell.h"

@interface RVSAppPostSpacerCell()
@property (nonatomic) UIView *colorView;
@property (nonatomic) NSArray *vConstraints;
@property (nonatomic) NSArray *hConstraints;
@end

@implementation RVSAppPostSpacerCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)setData:(RVSAppPostSpacerDataObject *)data
{
    self.backgroundColor = [UIColor clearColor];
    [super setData:data];
    self.colorView.backgroundColor = data.color;
    
    [self removeColorBoxConstraints];
    
    self.vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(topInset)-[view]-(bottomInset)-|" options:0 metrics:@{@"topInset": @(self.data.edgeInsets.top), @"bottomInset": @(self.data.edgeInsets.bottom)} views:@{@"view": self.colorView}];
    
    self.hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(leftInset)-[view]-(rightInset)-|" options:0 metrics:@{@"leftInset": @(self.data.edgeInsets.left), @"rightInset": @(self.data.edgeInsets.right)} views:@{@"view": self.colorView}];
    
    [self addConstraints:self.hConstraints];
    [self addConstraints:self.vConstraints];
}

-(void)removeColorBoxConstraints
{
    if (self.vConstraints)
    {
        [self removeConstraints:self.vConstraints];
        self.vConstraints = nil;
    }
    
    if (self.hConstraints)
    {
        [self removeConstraints:self.hConstraints];
        self.hConstraints = nil;
    }
}


-(CGSize)sizeForData:(RVSAppPostSpacerDataObject *)data
{
    return CGSizeMake(320, data.height);
}

-(UIView *)colorView
{
    if (!_colorView)
    {
        _colorView = [[UIView alloc] init];
        _colorView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_colorView];
    }
    
    return _colorView;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    
    [self removeColorBoxConstraints];
    self.colorView.backgroundColor = [UIColor clearColor];
}

@end
