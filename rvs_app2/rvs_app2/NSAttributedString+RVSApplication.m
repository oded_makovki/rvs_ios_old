//
//  NSAttributedString+RVSApplication.m
//  rvs_app2
//
//  Created by Barak Harel on 5/26/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "NSAttributedString+RVSApplication.h"

@implementation NSAttributedString (RVSApplication)

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingFont:(UIFont *)font color:(UIColor *)color ranges:(NSArray *)ranges
{
    return [self mutableCopyWithHightlightsUsingFont:font foregroundColor:color backgroundColor:nil ranges:ranges];
}

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingBackgroundColor:(UIColor *)backgroundColor ranges:(NSArray *)ranges
{
    return [self mutableCopyWithHightlightsUsingFont:nil foregroundColor:nil backgroundColor:backgroundColor ranges:ranges];
}

-(NSMutableAttributedString *)mutableCopyWithHightlightsUsingFont:(UIFont *)font foregroundColor:(UIColor *)foregroundColor backgroundColor:(UIColor *)backgroundColor ranges:(NSArray *)ranges
{
    if ([ranges count] > 0)
    {
        NSMutableAttributedString *mutableCopy = [self mutableCopy];
        
        for (int i = 0; i < [ranges count]; i++)
        {
            NSValue *value = ranges[i];
            NSRange range = [value rangeValue];
            
            if (range.length > 0)
            {
                //mark range
                if (foregroundColor)
                    [mutableCopy addAttribute:NSForegroundColorAttributeName value:foregroundColor range:range];
                if (font)
                    [mutableCopy addAttribute:NSFontAttributeName value:font range:range];
                if (backgroundColor)
                    [mutableCopy addAttribute:NSBackgroundColorAttributeName value:backgroundColor range:range];
            }
        }
        
        return mutableCopy; //make immutable
    }
    else
    {
        return [self mutableCopy];
    }
}

@end
