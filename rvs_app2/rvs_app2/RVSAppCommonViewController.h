//
//  RVSAppCommonViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppStyledViewController.h"
#import <rvs_sdk_api.h>
#import "RVSAppViewDelegate.h"

@class RVSAppCommonViewController;
@protocol RVSAppCommonViewControllerDelegate <NSObject>
@optional
-(BOOL)shouldAddDefaultRightNavigationItems; //def = YES

-(void)menuWillAppear;
-(void)networkStatusDidChange;
-(void)applicationDidEnterBackgroundAt:(NSDate *)didEnterBackgroundDate andWillEnterForgroundAt:(NSDate *)willEnterForgroundDate;
-(void)applicationDidCreatePost:(NSObject <RVSPost> *)post;
-(void)applicationWillDeletePostId:(NSString *)postId; //RVSApplicationWillDeletePostNotification
-(void)applicationDidCreateComment:(NSObject<RVSComment> *)comment forPostId:(NSString *)postId;
-(void)applicationDidDeleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId; //RVSApplicationDidDeleteCommentNotification
-(void)applicationDidUpdatePostId:(NSString *)postId;
-(void)userDidSignIn;
@end

@interface RVSAppCommonViewController : RVSAppStyledViewController <RVSAppViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentBottomConstraint;
@property (nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) id <RVSAppCommonViewControllerDelegate> delegate;

-(void)pushUserDetailViewControlWithUserId:(NSString *)userId;
-(void)pushUserListViewControlWithFollowersOfUserId:(NSString *)userId;
-(void)pushUserListViewControlWithFollowedByUserId:(NSString *)userId;
-(void)pushUserListViewControlWithRepostingOfPostId:(NSString *)postId;
-(void)pushUserListViewControlWithLikingOfPostId:(NSString *)postId;


-(void)pushPostListViewControlWithSinglePostId:(NSString *)postId;
-(void)pushPostListViewControlWithLikedPostesUserId:(NSString *)userId;
-(void)pushCommentListViewControllerWithPostId:(NSString *)postId shouldStartEdit:(BOOL)shouldStartEdit;

-(void)pushSearchViewController;
-(void)pushSearchResultsViewControllerWithSearchText:(NSString *)searchText withOwner:(UIViewController *)owner;
@end
