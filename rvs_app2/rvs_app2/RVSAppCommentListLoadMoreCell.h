//
//  RVSAppCommentListLoadMoreCell.h
//  rvs_app2
//
//  Created by Barak Harel on 6/9/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppCommentListLoadMoreDataObject.h"

@interface RVSAppCommentListLoadMoreCell : RVSAppDataCell
@property (nonatomic) RVSAppCommentListLoadMoreDataObject *data;
@end
