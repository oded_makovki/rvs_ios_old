//
//  RVSAppCompostPostFactory.m
//  rvs_app
//
//  Created by Barak Harel on 1/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCreatePostFactory.h"

@interface RVSAppCreatePostFactory()
@property (nonatomic) NSString *text;
@property (nonatomic) NSObject<RVSMediaContext> *mediaContext;
@property (nonatomic) NSTimeInterval startOffset;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) NSString *coverImageId;
@end

@implementation RVSAppCreatePostFactory

-(id)initWithPostWithText:(NSString *)text mediaContext:(NSObject<RVSMediaContext> *)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString *)coverImageId
{
    self = [super init];
    if (self)
    {
        NSAssert(text, @"No text");
        NSAssert(mediaContext, @"No media context");
        NSAssert(coverImageId, @"No cover image");
        NSAssert(startOffset < mediaContext.duration, @"Invalid start offset");
        NSAssert(duration <= mediaContext.duration, @"Invalid duration");
        
        self.text = text;
        self.mediaContext = mediaContext;
        self.startOffset = startOffset;
        self.duration = duration;
        self.coverImageId = coverImageId;
    }
    
    return self;
}


-(RVSPromise *)createPost
{
    return [[RVSSdk sharedSdk] createPostWithText:self.text mediaContext:self.mediaContext startOffset:self.startOffset duration:self.duration coverImageId:self.coverImageId];
}

@end
