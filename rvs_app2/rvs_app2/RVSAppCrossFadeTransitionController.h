//
//  RVSAppCrossFadeTransitionController.h
//  rvs_app2
//
//  Created by Barak Harel on 3/23/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppTransitionController.h"

@interface RVSAppCrossFadeTransitionController : RVSAppTransitionController

@end
