//
//  RVSAppSpacerDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostSpacerDataObject.h"

@implementation RVSAppPostSpacerDataObject

- (instancetype)initWithPost:(NSObject <RVSPost> *)post color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets
{
    self = [super initWithPost:post delegate:nil];
    if (self)
    {
        self.color = color;
        self.height = height;
        self.edgeInsets = edgeInsets;
    }
    
    return self;
}

+ (instancetype)dataWithPost:(NSObject <RVSPost> *)post color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets
{
    return [[self alloc] initWithPost:post color:color height:height edgeInsets:edgeInsets];
}

@end
