//
//  RVSAppMainViewController.h
//  rvs_app2
//
//  Created by Barak Harel on 3/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <MMDrawerController.h>
#import "RVSAppMainContentType.h"
#import "RVSAppCenterNavigationController.h"
#import "RVSAppLeftMenuViewController.h"

extern NSString *RVSAppMainContentTypeDidChangeNotification;
extern NSString *RVSAppMainMenuWillAppearNotification;
extern NSString *RVSAppMainMenuDidDisappearNotification;

@interface RVSAppMainViewController : MMDrawerController <UIGestureRecognizerDelegate>
@property (nonatomic) RVSAppCenterNavigationController * centerViewController;
@property (nonatomic) RVSAppMainContentType contentType;
@property (nonatomic) BOOL enableMenuOpenEdgeSwipe;


-(id)initWithLeftMenuViewController:(RVSAppLeftMenuViewController *)leftMenuViewController contentType:(RVSAppMainContentType)contentType;

-(void)updateMenuGestures;

@end
