//
//  RVSAppPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostViewDataCell.h"
#import "RVSApplication.h"

@implementation RVSAppPostViewDataCell

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.postView clear];
}

-(void)setData:(RVSAppPostViewDataObject *)data
{
    [super setData:data];
    if (self.isSizingCell)
    {
        [self.postView setPost:data.post searchMatches:data.searchMatches isSizingView:YES];
    }
    else
    {
        self.postView.delegate = data.delegate;
        [self.postView setPost:data.post searchMatches:data.searchMatches isSizingView:NO];
    }
}

@end
