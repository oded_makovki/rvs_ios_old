//
//  RVSAppSnapFlowLayout.h
//  rvs_app
//
//  Created by Barak Harel on 11/27/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppSnapFlowLayout : UICollectionViewFlowLayout
@property (nonatomic) BOOL snapToCell;
@end
