//
//  RVSAppPostInfoCell.h
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppDataCell.h"
#import "RVSAppPostInfoView.h"
#import "RVSAppPostInfoDataObject.h"

@interface RVSAppPostInfoCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppPostInfoView *postInfoView; //needs to be init or set
@property (nonatomic) RVSAppPostInfoDataObject *data;
@end
