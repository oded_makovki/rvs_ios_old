//
//  RVSAppSwipeToEditStateMachine.h
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "AAPLStateMachine.h"
#import <UIKit/UIKit.h>
#import "RVSAppDataListView.h"

extern NSString * const AAPLSwipeStateNothing;
extern NSString * const AAPLSwipeStateEditing;
extern NSString * const AAPLSwipeStateTracking;
extern NSString * const AAPLSwipeStateAnimatingOpen;
extern NSString * const AAPLSwipeStateAnimatingShut;

@interface RVSAppSwipeToEditStateMachine : AAPLStateMachine <UIGestureRecognizerDelegate>

- (instancetype)initWithDataListView:(RVSAppDataListView *)collectionView;

- (void)viewDidDisappear:(BOOL)animated;
- (void)shutActionPaneForEditingCellAnimated:(BOOL)animate;


@end
