//
//  RVSAppCommentListCell.h
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppActionsDataCell.h"
#import "RVSAppCommentListView.h"
#import "RVSAppCommentListDataObject.h"

@interface RVSAppCommentListCell : RVSAppActionsDataCell
@property (weak, nonatomic) IBOutlet RVSAppCommentListView *commentView; //needs to be init or set
@property (nonatomic) RVSAppCommentListDataObject *data;
@end
