//
//  RVSAppPostInfoCell.m
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostInfoCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppPostInfoCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostListCell];
    }
    
    return self;
}

-(void)initRVSAppPostListCell
{
    [self loadContentsFromNib];    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.postInfoView clear];
}

-(void)setData:(RVSAppPostInfoDataObject *)data
{
    [super setData:data];
    if (self.isSizingCell)
    {
        //skip
    }
    else
    {
        [self.postInfoView setPostId:self.data.post.postId likeCount:self.data.likeCount repostCount:self.data.repostCount delegate:self.data.delegate];
    }
}

-(CGSize)sizeForData:(RVSAppPostInfoDataObject *)data
{
    if (data.likeCount > 0 || data.repostCount > 0) //has likes or reposts
    {
        return CGSizeMake(320, 44);
    }
    return CGSizeMake(320, 0); //hidden
}
@end
