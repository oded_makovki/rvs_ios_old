//
//  RVSAppUserView.h
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserView.h"

@class RVSAppUserDetailView;
@protocol RVSAppUserDetailViewDelegate <RVSAppUserViewDelegate>

@optional
-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectMentionsForUser:(NSObject <RVSUser> *)user;

-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectLikesCountForUser:(NSObject <RVSUser> *)user;
-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectFollowersCountForUser:(NSObject <RVSUser> *)user;
-(void)userDetailView:(RVSAppUserDetailView *)userView didSelectFollowingCountForUser:(NSObject <RVSUser> *)user;
@end

@interface RVSAppUserDetailView : RVSAppUserView

@property (weak, nonatomic) IBOutlet UILabel *likesCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *mentionsCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *postsCountLabel;

@property (weak, nonatomic) IBOutlet UILabel *likesTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mentionsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followersTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *followingTitleLabel;

@property (weak, nonatomic) IBOutlet UIView *likesCountTapView;
@property (weak, nonatomic) IBOutlet UIView *mentionsCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followersCountTapView;
@property (weak, nonatomic) IBOutlet UIView *followingCountTapView;

@property (weak, nonatomic) IBOutlet UIButton *optionsButton;

@property (weak, nonatomic) id <RVSAppUserDetailViewDelegate> delegate;


@end
