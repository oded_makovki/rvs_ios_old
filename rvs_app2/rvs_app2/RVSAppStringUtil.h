//
//  RVSAppStringUtil.h
//  rvs_app2
//
//  Created by Barak Harel on 5/12/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppStringUtil : NSObject

@property (nonatomic, readonly) NSArray *knownSearchFieldNames;

+ (RVSAppStringUtil*)sharedUtil;
- (NSString *)stringForSearchFieldName:(NSString *)fieldName;

+(NSString *)prettyNumber:(NSNumber *)number;
+ (void)truncateTextInLabel:(UILabel *)label toDisplayRange:(NSRange)range;
+ (NSString *)emailShareTextFromText:(NSString *)text andLink:(NSString *)link;
+ (NSString *)facebookShareTextFromText:(NSString *)text;
+ (NSString *)twitterShareTextFromText:(NSString *)text;
@end
