//
//  RVSAppViewDelegate.h
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RVSUser;
@protocol RVSComment;

@protocol RVSAppViewDelegate <NSObject>

@optional
-(void) view:(UIView *)view didSelectLinkWithURL:(NSURL *)url;
-(void) view:(UIView *)view didSelectUser:(NSObject <RVSUser> *)user;
-(void) view:(UIView *)view didSelectCommentsForPostId:(NSString *)postId shouldStartEdit:(BOOL)shouldStartEdit;
-(void) view:(UIView *)view didSelectMentionCommentUser:(NSObject <RVSUser> *)user;
-(void) view:(UIView *)view didSelectDeleteComment:(NSObject <RVSComment> *)comment;

@end
