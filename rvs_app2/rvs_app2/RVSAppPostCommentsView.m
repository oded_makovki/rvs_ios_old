//
//  RVSAppLatestCommentsView.m
//  rvs_app2
//
//  Created by Barak Harel on 6/11/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostCommentsView.h"
#import <rvs_sdk_api.h>
#import "TTTAttributedLabel+RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppPostCommentsView() <TTTAttributedLabelDelegate>
@property (nonatomic) NSUInteger moreCommentsCount;
@property (nonatomic) NSArray *latestComments;
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;
@property (nonatomic) NSString *postId;
@end

@implementation RVSAppPostCommentsView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostCommentsView];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostCommentsView];
}

-(void)initRVSAppPostCommentsView
{
    self.shouldParseHotwords = YES;
    self.backgroundColor = [UIColor clearColor];
    self.textLabel.delegate = self;
    
    self.minimumLineHeight = 16.0f;
    self.maximumLineHeight = 30.0f;
    self.fontPointSize = 12.0f;
    
    [self clear];
}

-(void)clear
{
    self.textLabel.text = @"";
}

-(void)setPostId:(NSString *)postId latestComments:(NSArray *)latestComments moreCommentsCount:(NSInteger)moreCommentsCount delegate:(id<RVSAppViewDelegate>)delegate
{
    self.postId = postId;
    self.delegate = delegate;
    self.latestComments = latestComments;
    self.moreCommentsCount = moreCommentsCount;
    
    if ([self.latestComments count] == 0)
        return;
    
    [self updateTextLabel];    
    [self parseText];
}

-(void)updateTextLabel
{
    [self.textLabel setDefaultLinkStyleAttributesWithFont:[UIFont rvsBoldFontWithSize:self.fontPointSize] activeColor:nil inactiveColor:[UIColor rvsTintColor]];
    
    NSDictionary *titleAttributes = @{NSForegroundColorAttributeName: [UIColor rvsCommentUserNameColor],                                                                                                                              NSFontAttributeName: [UIFont rvsBoldFontWithSize:self.fontPointSize]};
    
    NSDictionary *textAttributes = @{NSForegroundColorAttributeName: [UIColor rvsCommentTextColor],                                                                                                                              NSFontAttributeName: [UIFont rvsRegularFontWithSize:self.fontPointSize]};
    
    NSMutableAttributedString *attributedString = [NSMutableAttributedString new];
    
    NSMutableArray *links = [NSMutableArray array];
    
    for (NSObject <RVSComment> *comment in [self.latestComments reverseObjectEnumerator])
    {
        if (attributedString.length > 0)
        {
            //add line break
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        }
        
        NSMutableAttributedString *userName;
        userName = [[NSMutableAttributedString alloc] initWithString:comment.author.name attributes:titleAttributes];
        
        NSRange nameRange = NSMakeRange(attributedString.length, userName.length);
        [links addObject:@{@"range": [NSValue valueWithRange:nameRange], @"userId": comment.author.userId }];
        
        [attributedString appendAttributedString:userName];
        [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@" "]];
        
        NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:comment.message attributes:textAttributes];
        
        if (text.length > 0)
            [attributedString appendAttributedString:text];
    }
    
    NSRange moreLinkRange = NSMakeRange(0,0);
    
    if (self.moreCommentsCount > 0)
    {
        if (attributedString.length > 0)
        {
            //add line break
            [attributedString appendAttributedString:[[NSAttributedString alloc] initWithString:@"\n"]];
        }
        
        NSMutableAttributedString *moreLinkText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%d more comments", self.moreCommentsCount] attributes:titleAttributes];
        
        moreLinkRange = NSMakeRange(attributedString.length, moreLinkText.length);
        [attributedString appendAttributedString:moreLinkText];
    }
    
    //paragraph style
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.minimumLineHeight = self.minimumLineHeight;
    paragraphStyle.maximumLineHeight = self.maximumLineHeight;
    
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, attributedString.length)];
    
    self.textLabel.attributedText = attributedString;
    
    for (NSDictionary *link in links)
    {
        NSRange nameRange = [link[@"range"] rangeValue];
        NSString *userId = link[@"userId"];
    
        if (nameRange.length > 0)
        {
            NSString* linkURLString = [NSString stringWithFormat:@"user://%@", userId]; // build the "hash:" link
            [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:nameRange]; // add it
        }
    }
    
    if (moreLinkRange.length > 0)
    {
        NSString* linkURLString = [NSString stringWithFormat:@"comments://%@", self.postId]; // build the "hash:" link
        [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:moreLinkRange]; // add it
    }
}

#pragma mark - Parse post

- (void)parseText
{
    if (!self.textLabel.attributedText)
        return;
    
    NSInteger length = self.textLabel.attributedText.length;
    NSString *text = self.textLabel.attributedText.string;
    
    if (self.shouldParseHotwords)
    {
        // Detect all "@xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* userRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B@\\w+" options:0 error:nil];
        
        [userRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "@xxx" user mention found, add a custom link:
             NSString* user = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"user://%@", user]; // build the "user:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
         }];
        
        // Detect all "#xxx" mention-like strings using the "@\w+" regular expression
        NSRegularExpression* hashRegex = [NSRegularExpression regularExpressionWithPattern:@"\\B#\\w+" options:0 error:nil];
        
        [hashRegex enumerateMatchesInString:text options:0 range:NSMakeRange(0,length)
                                 usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
         {
             // For each "#xxx" hash found, add a custom link:
             NSString* topic = [[text substringWithRange:match.range] substringFromIndex:1]; // get the matched user name, removing the "@"
             NSString* linkURLString = [NSString stringWithFormat:@"hash://%@", topic]; // build the "hash:" link
             [self.textLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:match.range]; // add it
             
         }];
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if (url && [self.delegate respondsToSelector:@selector(view:didSelectLinkWithURL:)])
    {
        [self.delegate view:self didSelectLinkWithURL:url];
    }
}

@end
