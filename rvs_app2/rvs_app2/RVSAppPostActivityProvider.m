//
//  RVSAppPostActivityProvider.m
//  rvs_app2
//
//  Created by Barak Harel on 4/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostActivityProvider.h"

@interface RVSAppPostActivityProvider()

@property (nonatomic) NSString *signature;
@property (nonatomic) NSObject <RVSPost> *post;

@end

@implementation RVSAppPostActivityProvider

-(id)initWithPost:(NSObject<RVSPost> *)post
{
    self = [super init];
    if (self)
    {
        self.post = post;
        self.signature = NSLocalizedString(@"activity post signature", nil);
    }
    
    return self;
}

- (id) activityViewController:(UIActivityViewController *)activityViewController
          itemForActivityType:(NSString *)activityType
{
    if ( [activityType isEqualToString:UIActivityTypePostToTwitter] )
        return [self twitterMessage];
    
    return [self fullMessage];
}

-(id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return @"";
}

-(NSString *)fullMessage
{
    return [NSString stringWithFormat:@"%@%@", self.post.text, self.signature];
}

-(NSString *)twitterMessage
{
    NSString *text = self.post.text;
    NSInteger maxLength = 117 - [self.signature length];  //117 (max twitter text length with url) - length of signature
    
    if (text.length > maxLength)
    {
        text = [NSString stringWithFormat:@"%@...",[text substringToIndex:(maxLength - 3)]]; //max - 3 (length of "...")
    }
    
    return [NSString stringWithFormat:@"%@%@", text, self.signature];
}

@end
