//
//  RVSAppPostCommentsLinkDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataObject.h"
#import "RVSAppViewDelegate.h"

@interface RVSAppPostCommentsDataObject : RVSAppPostDataObject

- (NSInteger)moreCommentsCount;
- (NSArray *)latestComments;

@end
