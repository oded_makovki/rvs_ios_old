//
//  RVSAppPostInfoView.m
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostInfoView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"
#import "TTTAttributedLabel+RVSApplication.h"

#define SEPERATOR @" • "

@interface RVSAppPostInfoView() <TTTAttributedLabelDelegate>
@property (weak, nonatomic) id <RVSAppViewDelegate> delegate;
@property (nonatomic) NSString *postId;
@end

@implementation RVSAppPostInfoView


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostInfoView];
    }
    
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostInfoView];
}

-(void)initRVSAppPostInfoView
{
    self.backgroundColor = [UIColor rvsPostInfoBackgroundColor];
    self.infoLabel.delegate = self;
    [self.infoLabel setDefaultLinkStyleAttributesWithFont:nil activeColor:[UIColor rvsPostInfoTitleColor] inactiveColor:[UIColor rvsPostInfoTextColor]];
}

-(void)clear
{
    self.infoLabel.text = @"";
}

-(void)setPostId:(NSString *)postId likeCount:(NSInteger)likeCount repostCount:(NSInteger)repostCount delegate:(id<RVSAppViewDelegate>)delegate
{
    self.postId = postId;
    self.delegate = delegate;
    
    if (likeCount <= 0 && repostCount <= 0)
        return;
    
    NSDictionary *textAttributes = @{NSForegroundColorAttributeName: [UIColor rvsPostInfoTitleColor],                                                                                                                              NSFontAttributeName: [UIFont rvsBoldFontWithSize:12]};
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:@""];
    
    
    NSRange likesLinkRange = NSMakeRange(0, 0);
    if (likeCount > 0)
    {
        NSString *format;
        if (likeCount == 1)
            format = NSLocalizedString(@"post info like format", nil);
        else
            format = NSLocalizedString(@"post info likes format", nil);
        
        NSAttributedString *likesText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:format, likeCount] attributes:textAttributes];
        
        likesLinkRange = NSMakeRange(text.length, likesText.length);
        
        [text appendAttributedString:likesText];
    }
    
    NSRange repostsLinkRange = NSMakeRange(0, 0);
    if (repostCount > 0)
    {
        if (text.length > 0)
        {
            NSDictionary *seperatorAttributes = @{NSForegroundColorAttributeName: [UIColor rvsPostInfoTextColor],                                                                                                                              NSFontAttributeName: [UIFont rvsRegularFontWithSize:12]};
            NSAttributedString *seperatorText = [[NSMutableAttributedString alloc] initWithString:SEPERATOR attributes:seperatorAttributes];
            [text appendAttributedString:seperatorText];
        }
        
        NSString *format;
        if (repostCount == 1)
            format = NSLocalizedString(@"post info repost format", nil);
        else
            format = NSLocalizedString(@"post info reposts format", nil);
        
        NSAttributedString *repostsText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:format, repostCount] attributes:textAttributes];
        
        repostsLinkRange = NSMakeRange(text.length, repostsText.length);
        
        [text appendAttributedString:repostsText];
    }
    
    self.infoLabel.attributedText = text;
    if (repostsLinkRange.length > 0)
    {
        NSString* linkURLString = [NSString stringWithFormat:@"reposting://%@", self.postId]; // build the "hash:" link
        [self.infoLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:repostsLinkRange]; // add it
    }
    
    if (likesLinkRange.length > 0)
    {
        NSString* linkURLString = [NSString stringWithFormat:@"liking://%@", self.postId]; // build the "hash:" link
        [self.infoLabel addLinkToURL:[NSURL URLWithString:linkURLString] withRange:likesLinkRange]; // add it
    }
}

-(void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    if (url && [self.delegate respondsToSelector:@selector(view:didSelectLinkWithURL:)])
    {
        [self.delegate view:self didSelectLinkWithURL:url];
    }
}

@end
