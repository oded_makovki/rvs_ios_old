//
//  RVSAppSettingsViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/13/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppDevSettingsViewController.h"
#import "RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import <RMPickerViewController.h>
#import "RVSAppSpoilersUtil.h"
#import "RVSAppListUtilConfigController.h"

#define SEARCH_SUGGESTION_DATA_FILE_NAME @"searchSuggestions.plist"

extern NSString *RVSSdkBaseUrlUserDefaultsKey;
NSString *RVSMediaSourceTypeUserDefaultsKey = @"mediaSource";
NSString *RVSImageSourceTypeUserDefaultsKey = @"imageSource";

@interface RVSAppDevSettingsViewController () <RVSAppCommonViewControllerDelegate, RMPickerViewControllerDelegate, UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UILabel *channelDetectTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *channelDetectChannelNameLabel;

@property (nonatomic) RVSNotificationObserver *channelDetectorMatchObserver;

@property (nonatomic) NSDictionary *settings;

@property (weak, nonatomic) IBOutlet UIButton *backendServerButton;
@property (nonatomic) RMPickerViewController *backendPickerView;
@property (nonatomic) NSMutableArray *backendServers;
@property (nonatomic) NSInteger currentBackendServerIndex;

@property (weak, nonatomic) IBOutlet UIButton *mediaSourceButton;
@property (nonatomic) RMPickerViewController *mediaSourcePickerView;
@property (nonatomic) NSMutableArray *mediaSources;
@property (nonatomic) NSInteger currentMediaSourceIndex;

@property (weak, nonatomic) IBOutlet UIButton *imageSourceButton;
@property (nonatomic) RMPickerViewController *imageSourcePickerView;
@property (nonatomic) NSMutableArray *imageSources;
@property (nonatomic) NSInteger currentImageSourceIndex;

@property (weak, nonatomic) IBOutlet UISwitch *enableChannelDetectorSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *enableMenuOpenEdgeSwipeSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *enableMenuCardAnimationSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *debugModeSwitch;

@property (nonatomic) RVSKeyPathObserver *appPropertiesObserver;

@property (nonatomic) RVSAppListUtilConfigController *searchSuggestionsConfigViewController;
@end

@implementation RVSAppDevSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadSettings];
    [self loadServers];
    [self loadMediaSources];
    [self loadImageSources];
    
    self.navigationItem.title = @"Dev. Settings";
    
    self.backendServerButton.backgroundColor = [UIColor rvsTintColor];
    self.mediaSourceButton.backgroundColor = [UIColor rvsTintColor];
    self.imageSourceButton.backgroundColor = [UIColor rvsTintColor];
    
    BOOL enableChannelDetector = [[NSUserDefaults standardUserDefaults] boolForKey:RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey];
    self.enableChannelDetectorSwitch.on = enableChannelDetector;
    
    self.enableMenuOpenEdgeSwipeSwitch.on = [RVSApplication application].mainViewController.enableMenuOpenEdgeSwipe;
    
    BOOL enableMenuCardAnimation = [[NSUserDefaults standardUserDefaults] boolForKey:RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey];
    self.enableMenuCardAnimationSwitch.on = enableMenuCardAnimation;
    
    self.debugModeSwitch.on = [RVSApplication application].inDebugMode;
    
    [self setObservers];
}

-(BOOL)shouldAddDefaultRightNavigationItems
{
    return NO;
}

-(void)loadSettings
{
    NSString * settingsPath = [[NSBundle mainBundle] pathForResource:@"RVSAppDevSettings" ofType:@"plist"];
    self.settings = [NSDictionary dictionaryWithContentsOfFile:settingsPath];
}

-(void)loadServers
{
    //default
    NSDictionary *defaultServer = @{@"title" : @"Default"};
    
    self.backendServers = [NSMutableArray arrayWithObject:defaultServer];
    [self.backendServers addObjectsFromArray:self.settings[@"servers"]];
    
    [self getCurrentBackendServer];
}

-(void)loadMediaSources
{
    NSDictionary *defaultMediaType = @{@"title" : @"Default"};
    
    self.mediaSources = [NSMutableArray arrayWithObject:defaultMediaType];
    [self.mediaSources addObjectsFromArray:self.settings[@"mediaSources"]];
    
    [self getCurrentMediaType];
}

-(void)loadImageSources
{
    NSDictionary *defaultMediaType = @{@"title" : @"Default"};
    
    self.imageSources = [NSMutableArray arrayWithObject:defaultMediaType];
    [self.imageSources addObjectsFromArray:self.settings[@"imageSources"]];
    
    [self getCurrentImageType];
}

-(void)setObservers
{
    __weak RVSAppDevSettingsViewController *weakSelf = self;
    
    self.channelDetectorMatchObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSSdkChannelDetectorMatchNotfication object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
        [weakSelf handleChannelDetectorMatchNotification:notification];
    }];
}

#pragma mark - Notifications

-(void)handleChannelDetectorMatchNotification:(NSNotification *)notification
{
    NSObject <RVSChannel> *channel = notification.userInfo[RVSSdkChannelDetectorChannelUserInfoKey];
    
    if (channel)
    {
        self.channelDetectChannelNameLabel.text = channel.name;
    }
    else //no match
    {
        self.channelDetectChannelNameLabel.text = @"Channel: n/a";
    }
    
    NSDate *time = notification.userInfo[RVSSdkChannelDetectorTimeUserInfoKey];
    
    if (time)
    {
        self.channelDetectTimeLabel.text = [time description];
    }
    else
    {
        self.channelDetectTimeLabel.text = @"Time: n/a";
    }
}

#pragma mark - Debug Mode

- (IBAction)debugModeSwitchValueChanged:(UISwitch *)sender {
    [RVSApplication application].inDebugMode = sender.on;
}


#pragma mark - Left Menu

- (IBAction)enableMenuOpenEdgeSwipeSwitchValueChanged:(UISwitch *)sender {
    
    //update
    [RVSApplication application].mainViewController.enableMenuOpenEdgeSwipe = sender.on;
    [[RVSApplication application].mainViewController updateMenuGestures];
    
    //save
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)enableMenuCardAnimationSwitchValueChanged:(UISwitch *)sender {

    //update
    if (sender.on)
    {
        [[RVSApplication application].mainViewController setDrawerVisualStateBlock:[RVSAppLeftMenuViewController cardVisualStateBlock]];
    }
    else
    {
        [[RVSApplication application].mainViewController setDrawerVisualStateBlock:[RVSAppLeftMenuViewController parallaxVisualStateBlockWithParallaxFactor:2.0]];
    }
    
    //save
    [[NSUserDefaults standardUserDefaults] setBool:sender.on forKey:RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Channel Detector

- (IBAction)enableChannelDetectorValueChanged:(id)sender
{
    //save
    [[NSUserDefaults standardUserDefaults] setBool:self.enableChannelDetectorSwitch.on forKey:RVSApplicationSettingsEnableChannelDetectorUserDefaultsKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - Spoilers

- (IBAction)configSpoilers:(UIButton *)sender
{
    [[RVSAppSpoilersUtil sharedUtil] showConfig];
}

#pragma mark - Logger

- (IBAction)logger:(id)sender
{
    RVSLoggerConfigController *loggerController = [[RVSLoggerConfigController alloc] init];
    [self.navigationController pushViewController:loggerController animated:YES];
}

#pragma mark - Media Type



-(void)getCurrentMediaType
{
    self.currentMediaSourceIndex = -1;
    
    NSString *mediaSourceString = [[NSUserDefaults standardUserDefaults] objectForKey:RVSMediaSourceTypeUserDefaultsKey];
    
    if (!mediaSourceString) //not set, assume first type.
    {
        self.currentMediaSourceIndex = 0;
        return;
    }
    else
    {
        for (int i = 0; i < [self.mediaSources count]; i++)
        {
            if ([self.mediaSources[i][@"type"] isEqualToString:mediaSourceString])
            {
                self.currentMediaSourceIndex = i;
                break;
            }
        }
        
        //fallback
        if (self.currentMediaSourceIndex == -1)
        {
            NSDictionary *otherType = @{@"title" : @"Other", @"type" : mediaSourceString};
            [self.mediaSources addObject:otherType];
            self.currentMediaSourceIndex = [self.mediaSources count] - 1;
        }
    }
}

-(void)setCurrentMediaSourceIndex:(NSInteger)currentMediaSourceIndex
{
    [self setCurrentMediaSourceIndex:currentMediaSourceIndex save:NO];
}

-(void)setCurrentMediaSourceIndex:(NSInteger)currentMediaSourceIndex save:(BOOL)save
{
    if (_currentMediaSourceIndex == currentMediaSourceIndex)
        return;
    
    //update
    _currentMediaSourceIndex = currentMediaSourceIndex;
    
    //load data if needed
    if (_currentMediaSourceIndex >= 0 && _currentMediaSourceIndex < [self.mediaSources count])
    {
        NSDictionary *mediaSource = self.mediaSources[currentMediaSourceIndex];
        [self.mediaSourceButton setTitle:mediaSource[@"title"] forState:UIControlStateNormal];
        
        if (save && mediaSource) //don't check url as it can be nil (default)
        {
            //save new
            [[NSUserDefaults standardUserDefaults] setObject:mediaSource[@"type"] forKey:RVSMediaSourceTypeUserDefaultsKey]; //note - can be nil (will remove the entry)
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Saved media source: %@", mediaSource[@"title"]);
            
            //prompt user to kill the app
            [self promptForUserRestart:NO withTitle:[NSString stringWithFormat:@"Will use - %@", mediaSource[@"type"]]];
        }
    }
}

- (IBAction)selectMediaSource:(UIButton *)sender
{
    self.mediaSourcePickerView = [RMPickerViewController pickerController];
    self.mediaSourcePickerView.delegate = self;
    
    [self.mediaSourcePickerView show];
    [self.mediaSourcePickerView.picker selectRow:self.currentMediaSourceIndex inComponent:0 animated:NO];
}

#pragma mark - Image Source

-(void)getCurrentImageType
{
    self.currentImageSourceIndex = -1;
    
    NSString *imageSourceString = [[NSUserDefaults standardUserDefaults] objectForKey:RVSImageSourceTypeUserDefaultsKey];
    
    if (!imageSourceString) //not set, assume first type.
    {
        self.currentImageSourceIndex = 0;
        return;
    }
    else
    {
        for (int i = 0; i < [self.imageSources count]; i++)
        {
            if ([self.imageSources[i][@"type"] isEqualToString:imageSourceString])
            {
                self.currentImageSourceIndex = i;
                break;
            }
        }
        
        //fallback
        if (self.currentImageSourceIndex == -1)
        {
            NSDictionary *otherType = @{@"title" : @"Other", @"type" : imageSourceString};
            [self.imageSources addObject:otherType];
            self.currentImageSourceIndex = [self.imageSources count] - 1;
        }
    }
}

-(void)setCurrentImageSourceIndex:(NSInteger)currentImageSourceIndex
{
    [self setCurrentImageSourceIndex:currentImageSourceIndex save:NO];
}

-(void)setCurrentImageSourceIndex:(NSInteger)currentImageSourceIndex save:(BOOL)save
{
    if (_currentImageSourceIndex == currentImageSourceIndex)
        return;
    
    //update
    _currentImageSourceIndex = currentImageSourceIndex;
    
    //load data if needed
    if (_currentImageSourceIndex >= 0 && _currentImageSourceIndex < [self.imageSources count])
    {
        NSDictionary *imageSource = self.imageSources[currentImageSourceIndex];
        [self.imageSourceButton setTitle:imageSource[@"title"] forState:UIControlStateNormal];
        
        if (save && imageSource) //don't check url as it can be nil (default)
        {
            //save new
            [[NSUserDefaults standardUserDefaults] setObject:imageSource[@"type"] forKey:RVSImageSourceTypeUserDefaultsKey]; //note - can be nil (will remove the entry)
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Saved image source: %@", imageSource[@"title"]);
            
            //prompt user to kill the app
            [self promptForUserRestart:NO withTitle:[NSString stringWithFormat:@"Will use - %@", imageSource[@"type"]]];
        }
    }
}

- (IBAction)selectImageSource:(UIButton *)sender
{
    self.imageSourcePickerView = [RMPickerViewController pickerController];
    self.imageSourcePickerView.delegate = self;
    
    [self.imageSourcePickerView show];
    [self.imageSourcePickerView.picker selectRow:self.currentImageSourceIndex inComponent:0 animated:NO];
}


#pragma mark - Servers

- (void)getCurrentBackendServer
{
    self.currentBackendServerIndex = -1;
    
    NSString *baseUrlString = [[NSUserDefaults standardUserDefaults] objectForKey:RVSSdkBaseUrlUserDefaultsKey];
    
    if (!baseUrlString) //not set, assume first server.
    {
        self.currentBackendServerIndex = 0;
        return;
    }
    else
    {
        for (int i = 0; i < [self.backendServers count]; i++)
        {
            if ([self.backendServers[i][@"url"] isEqualToString:baseUrlString])
            {
                self.currentBackendServerIndex = i;
                break;
            }
        }
        
        //fallback
        if (self.currentBackendServerIndex == -1)
        {
            NSDictionary *otherServer = @{@"title" : @"Other", @"url" : baseUrlString};
            [self.backendServers addObject:otherServer];
            self.currentBackendServerIndex = [self.backendServers count] - 1;
        }
    }
}

-(void)setCurrentBackendServerIndex:(NSInteger)currentBackendServerIndex
{
    [self setCurrentBackendServerIndex:currentBackendServerIndex save:NO];
}

-(void)setCurrentBackendServerIndex:(NSInteger)currentBackendServerIndex save:(BOOL)save
{
    if (_currentBackendServerIndex == currentBackendServerIndex)
        return;
    
    //update
    _currentBackendServerIndex = currentBackendServerIndex;
    
    //load data if needed
    if (_currentBackendServerIndex >= 0 && _currentBackendServerIndex < [self.backendServers count])
    {
        NSDictionary *server = self.backendServers[currentBackendServerIndex];
        [self.backendServerButton setTitle:server[@"title"] forState:UIControlStateNormal];
        
        if (save && server) //don't check url as it can be nil (default)
        {
            //save new
            [[NSUserDefaults standardUserDefaults] setObject:server[@"url"] forKey:RVSSdkBaseUrlUserDefaultsKey]; //note - can be nil (will remove the entry)
            [[NSUserDefaults standardUserDefaults] synchronize];
            NSLog(@"Saved backend server url: %@", server[@"url"]);
            
            //prompt user to kill the app
            [self promptForUserRestart:YES withTitle:[NSString stringWithFormat:@"Will use - %@", server[@"title"]]];
        }
    }
}

- (IBAction)selectBackendServer:(UIButton *)sender
{
    self.backendPickerView = [RMPickerViewController pickerController];
    self.backendPickerView.delegate = self;
    
    //You can enable or disable bouncing and motion effects
    //self.backendPickerView.disableBouncingWhenShowing = YES;
    //self.backendPickerView.disableMotionEffects = YES;
    
    [self.backendPickerView show];
    [self.backendPickerView.picker selectRow:self.currentBackendServerIndex inComponent:0 animated:NO];
}

#pragma mark - Alert

-(void)promptForUserRestart:(BOOL)restart withTitle:(NSString *)title
{
    NSString *message = @"Please reload your data to apply your new settings";
    if (restart)
        message = @"Please restart the application to apply your new settings";
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alertView show];
}

#pragma mark - Picker View Delegate

- (void)pickerViewController:(RMPickerViewController *)vc didSelectRows:(NSArray *)selectedRows
{
    NSLog(@"Successfully selected rows: %@", selectedRows);
    if ([selectedRows count])
    {
        NSInteger row = [selectedRows[0] integerValue];
        if (vc == self.backendPickerView)
        {
            [self setCurrentBackendServerIndex:row save:YES];
        }
        else if (vc == self.mediaSourcePickerView)
        {
            [self setCurrentMediaSourceIndex:row save:YES];
        }
        else if (vc == self.imageSourcePickerView)
        {
            [self setCurrentImageSourceIndex:row save:YES];
        }
    }
}
- (void)pickerViewControllerDidCancel:(RMPickerViewController *)vc
{
    NSLog(@"Selection was canceled");
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == self.backendPickerView.picker)
    {
        return [self.backendServers count];
    }
    else if (pickerView == self.mediaSourcePickerView.picker)
    {
        return [self.mediaSources count];
    }
    else if (pickerView == self.imageSourcePickerView.picker)
    {
        return [self.imageSources count];
    }
    else
    {
        return 0;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (pickerView == self.backendPickerView.picker)
    {
        return self.backendServers[row][@"title"];
    }
    else if (pickerView == self.mediaSourcePickerView.picker)
    {
        return self.mediaSources[row][@"title"];
    }
    else if (pickerView == self.imageSourcePickerView.picker)
    {
        return self.imageSources[row][@"title"];
    }
    
    return @"";
}

#pragma mark - Search suggestions

-(IBAction)showSearchSuggestionConfig:(UIButton *)sender
{
    RVSAppListUtilConfigController *searchSuggestionsConfigViewController = [RVSAppListUtilConfigController showWithFilename:SEARCH_SUGGESTION_DATA_FILE_NAME];
    searchSuggestionsConfigViewController.navigationItem.title = @"Suggestions";
}

@end
