//
//  RVSAppMainViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppStyledNavigationController.h"
#import "RVSAppMainContentType.h"

@interface RVSAppCenterNavigationController : RVSAppStyledNavigationController

-(id)initWithRootContentType:(RVSAppMainContentType)contentType leftBarButtonItem:(UIBarButtonItem *)item;

-(void)showPostingProgress;
-(void)hidePostingProgress;

-(void)showPostResultSuccess;
-(void)showPostResultFailed;

+(UIViewController *)singlePostViewControllerWithPostId:(NSString *)postId;

@end
