//
//  RVSAppStyledNavigationController.m
//  rvs_app
//
//  Created by Barak Harel on 11/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppStyledNavigationController.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppStyledNavigationController ()
@property (nonatomic) UIImageView *line;
@end

@implementation RVSAppStyledNavigationController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationBar.translucent = YES;
//    self.navigationBar.barTintColor = [UIColor rvsNavigationBarColor];
    
    
//    [self.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];    
//    [self.navigationBar setShadowImage:[[UIImage alloc] init]];
}


@end
