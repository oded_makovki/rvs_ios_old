//
//  RVSAppAboutViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppAboutViewController.h"
#import "UIColor+RVSApplication.h"
#import "RVSApplication.h"

@interface RVSAppAboutViewController () <RVSAppCommonViewControllerDelegate>
@property (nonatomic) IBOutlet UIBarButtonItem *buttonSettings;

@end

@implementation RVSAppAboutViewController

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppAboutViewController];
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppAboutViewController];
}

-(void)initRVSAppAboutViewController
{
    self.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	self.navigationItem.title = NSLocalizedString(@"about title", nil);
    
    NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (!version)
        version = @"0.5.0";
    
    self.versionLabel.text = [NSString stringWithFormat:@"v%@", version];
    self.view.backgroundColor = [UIColor whiteColor];
}

-(BOOL)shouldAddDefaultRightNavigationItems
{
    return NO;
}


@end
