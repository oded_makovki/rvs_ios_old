//
//  RVSAppPushableUserListViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppUserListFactory.h"


@interface RVSAppPushableUserListViewController : RVSAppCommonDataViewController

@property (nonatomic) RVSAppUserListFactory *userListFactory;

- (void)reloadUsers;
@end
