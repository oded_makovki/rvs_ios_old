//
//  RVSAppComposeViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/3/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppStyledViewController.h"
#import "RVSAppCreatePostFactory.h"
#import <rvs_sdk_api.h>

@class RVSAppComposeViewController;
@protocol RVSAppComposeViewControllerDelegate <NSObject>

- (void)composeViewController:(RVSAppComposeViewController *)sender
didCompleteWithComposePostFactory:(RVSAppCreatePostFactory *)postFactory;
@end

@interface RVSAppComposeViewController : RVSAppStyledViewController
@property (weak, nonatomic) id <RVSAppComposeViewControllerDelegate> delegate;


-(id)initWithMediaContextPromise:(RVSPromise *)mediaContextPromise defaultText:(NSString *)defaultText isLive:(BOOL)isLive;

@end
