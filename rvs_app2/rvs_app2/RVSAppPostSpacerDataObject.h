//
//  RVSAppSpacerDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 6/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataObject.h"
@protocol RVSPost;

@interface RVSAppPostSpacerDataObject : RVSAppPostDataObject
@property (nonatomic) UIColor *color;
@property (nonatomic) CGFloat height;
@property (nonatomic) UIEdgeInsets edgeInsets;

- (instancetype)initWithPost:(NSObject <RVSPost> *)post color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets; // Designated initializer

+ (instancetype)dataWithPost:(NSObject <RVSPost> *)post color:(UIColor *)color height:(CGFloat)height edgeInsets:(UIEdgeInsets)edgeInsets;

@end
