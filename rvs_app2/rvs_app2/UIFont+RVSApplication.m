//
//  UIFont+RVSApplication.m
//  rvs_app2
//
//  Created by Barak Harel on 5/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "UIFont+RVSApplication.h"

@implementation UIFont (RVSApplication)

+(UIFont *)rvsExtraLightFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"BentonSans-ExtraLight" size:size];
}

+(UIFont *)rvsLightFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"BentonSans-Light" size:size];
}

+(UIFont *)rvsRegularFontWithSize:(CGFloat)size
{
    UIFont *regular = [UIFont fontWithName:@"BentonSans-Regular" size:size];
    if (!regular)
        regular = [UIFont fontWithName:@"BentonSans" size:size];
    
    return regular;
}

+(UIFont *)rvsMediumFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"BentonSans-Medium" size:size];
}

+(UIFont *)rvsBoldFontWithSize:(CGFloat)size
{
    return [UIFont fontWithName:@"BentonSans-Bold" size:size];
}

@end
