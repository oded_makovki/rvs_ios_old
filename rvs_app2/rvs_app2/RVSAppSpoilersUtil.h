//
//  RVSAppSpoilersUtil.h
//  rvs_app2
//
//  Created by Barak Harel on 5/20/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppSpoilersUtil : NSObject
+ (RVSAppSpoilersUtil*)sharedUtil;
- (BOOL)shouldProtectProgramNamed:(NSString *)programName;
+ (UIImage *)protectedImageFrom:(UIImage *)image;
- (void)saveUnspoiledProgramNames:(NSArray *)unspoiledProgramNames;

- (void)showConfig;

@property (nonatomic, readonly) NSArray *unspoiledProgramNames;
@end
