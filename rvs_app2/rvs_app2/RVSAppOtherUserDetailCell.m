//
//  RVSAppOtherUserDetailCell.m
//  rvs_app2
//
//  Created by Barak Harel on 4/9/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppOtherUserDetailCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@implementation RVSAppOtherUserDetailCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppOtherUserDetailCell];
    }
    return self;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
}

-(void)initRVSAppOtherUserDetailCell
{
    [self loadContentsFromNib];
    
    self.userView.followToggleButton.titleLabel.font = [UIFont rvsRegularFontWithSize:14];
    self.userView.optionsButton.titleLabel.font = [UIFont rvsRegularFontWithSize:14];
    
    self.userView.userNameLabel.textColor    = [UIColor blackColor];
    [self.userView.followToggleButton setTitle:NSLocalizedString(@"user detail button follow" , nil) forState:UIControlStateNormal];
    [self.userView.followToggleButton setTitle:NSLocalizedString(@"user detail button following" , nil) forState:UIControlStateSelected];
    [self.userView.followToggleButton setTitleColor:[UIColor rvsTintColor] forState:UIControlStateNormal];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 276);
}

@end
