//
//  TTTAttributedLabel+RVSApplication.m
//  rvs_app
//
//  Created by Barak Harel on 12/4/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TTTAttributedLabel+RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

#define LINK_FONT_NAME [UIFont rvsBoldFontWithSize:14]

@implementation TTTAttributedLabel (RVSApplication)

-(void)setDefaultLinkStyleAttributes
{
    [self setDefaultLinkStyleAttributesWithFont:LINK_FONT_NAME activeColor:[UIColor rvsPostLinkColor] inactiveColor:[UIColor rvsTintColor]];
}

-(void)setDefaultLinkStyleAttributesWithFont:(UIFont *)font activeColor:(UIColor *)actvieColor inactiveColor:(UIColor *)inactiveColor
{
    NSMutableDictionary *activeLinkAttributes = [NSMutableDictionary dictionary];
    [activeLinkAttributes setObject:[NSNumber numberWithInt: NSUnderlineStyleSingle] forKey:NSUnderlineStyleAttributeName];
    
    if (actvieColor)
        [activeLinkAttributes setObject:actvieColor forKey:NSForegroundColorAttributeName];
    
    if (font)
        [activeLinkAttributes setObject:font forKey:NSFontAttributeName];
    
    
    NSMutableDictionary *linkAttributes = [NSMutableDictionary dictionary];
    [linkAttributes setObject:[NSNumber numberWithInt: NSUnderlineStyleNone] forKey:NSUnderlineStyleAttributeName];
    
    if (actvieColor)
        [linkAttributes setObject:actvieColor forKey:NSForegroundColorAttributeName];
    
    if (font)
        [linkAttributes setObject:font forKey:NSFontAttributeName];
    
    NSMutableDictionary *inactiveLinkAttributes = [NSMutableDictionary dictionary];
    [inactiveLinkAttributes setObject:[NSNumber numberWithInt: NSUnderlineStyleNone] forKey:NSUnderlineStyleAttributeName];
    
    if (inactiveColor)
        [inactiveLinkAttributes setObject:inactiveColor forKey:NSForegroundColorAttributeName];
    
    if (font)
        [inactiveLinkAttributes setObject:font forKey:NSFontAttributeName];
    
    self.activeLinkAttributes = activeLinkAttributes;
    self.linkAttributes = linkAttributes;
    self.inactiveLinkAttributes = inactiveLinkAttributes;
}

@end


