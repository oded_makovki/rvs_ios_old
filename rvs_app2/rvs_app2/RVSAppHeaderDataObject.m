//
//  RVSAppHeaderDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 4/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppHeaderDataObject.h"

@implementation RVSAppHeaderDataObject

-(instancetype)initWithTitle:(NSString *)title
{
    self = [super init];
    if (self)
    {
        self.title = title;
    }
    return self;
}

+(instancetype)dataWithTitle:(NSString *)title
{
    return [[self alloc] initWithTitle:title];
}
@end
