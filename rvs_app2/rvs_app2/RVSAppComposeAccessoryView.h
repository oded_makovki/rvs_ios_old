//
//  RVSAppComposeAccessoryView.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+NibLoading.h"

@interface RVSAppComposeAccessoryView : NibLoadedView
@property (weak, nonatomic) IBOutlet UILabel *charsCountLabel;

-(id)initWithFrame:(CGRect)frame textView:(UITextView *)textView;
@end
