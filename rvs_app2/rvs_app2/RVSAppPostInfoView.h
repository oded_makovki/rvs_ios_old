//
//  RVSAppPostInfoView.h
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import <TTTAttributedLabel.h>
#import "RVSAppViewDelegate.h"

@interface RVSAppPostInfoView : UIView
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *infoLabel;

/**
 *  Set counters for view
 *
 *  @param likeCount   likes count
 *  @param repostCount reposts count
 */
-(void)setPostId:(NSString *)postId likeCount:(NSInteger)likeCount repostCount:(NSInteger)repostCount  delegate:(id <RVSAppViewDelegate>)delegate;

-(void)clear;
@end
