//
//  RVSAppClipPlayerView.m
//  rvs_app
//
//  Created by Barak Harel on 11/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppClipPlayerView.h"
#import "RVSApplication.h"
#import "UIColor+RVSApplication.h"
#import <rvs_sdk_api helpers.h>
#import "RVSAppClipPlayerControls.h"
#import "RVSAppClipPlayerInlineControls.h"
#import "RVSAppClipPlayerFullscreenControls.h"
#import "RVSAppSpoilersUtil.h"

@interface RVSAppClipPlayerView() <RVSClipViewDelegate, UIGestureRecognizerDelegate, RVSAppClipPlayerViewUIDelegate, RVSAppClipPlayerViewUIDataSource>

@property (nonatomic) UIImageView *coverImageView;

@property (nonatomic) UIImageView *playbackStateImageView;

@property (nonatomic) RVSPromiseHolder *coverImagePromise;
@property (nonatomic) RVSPromiseHolder *fallbackImagePromise;

@property (nonatomic) NSMutableArray *playerConstrains;
@property (nonatomic) RVSAppClipPlayerControls *playerControls;

#pragma mark - Data
@property (nonatomic) NSTimeInterval mediaCurrentPosition;
@property (nonatomic) NSTimeInterval mediaDuration;

@property (nonatomic) NSObject <RVSChannel> *channel;
@property (nonatomic) NSObject <RVSProgram> *program;

@end

@implementation RVSAppClipPlayerView

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppClipPlayer];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initRVSAppClipPlayer];
    }
    return self;
}

-(void)initRVSAppClipPlayer
{
    self.backgroundColor = [UIColor rvsClipPlayerBackgoundColor];
    self.clipsToBounds = YES;
    self.delegate = self;
    
    [self addCoverImageView];
    [self addPlaybackStateImageView];
    
    [self addOverlayGestures];
    
    self.fullscreen = NO;
}

#pragma mark - Subviews

- (void)addCoverImageView
{
    //image view
    self.coverImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self.coverImageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.coverImageView setClipsToBounds:YES];
    [self.backgroundView insertSubview:self.coverImageView atIndex:0];
    self.coverImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.coverImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": self.coverImageView}]];
    [self.coverImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": self.coverImageView}]];
}

- (void)setPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    if (_playerControls == playerControls)
        return;
    
    if (_playerControls)
    {
        [_playerControls removeFromSuperview];
        [self.overlayView removeConstraints:self.playerConstrains];
    }
    
    self.playerConstrains = [NSMutableArray array];
    
    _playerControls = playerControls;
    
    [self.overlayView addSubview:_playerControls];
    _playerControls.translatesAutoresizingMaskIntoConstraints = NO;
    
    [self.playerConstrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": _playerControls}]];
    
    [self.playerConstrains addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": _playerControls}]];
    
    [_playerControls.superview addConstraints:self.playerConstrains];
    
    //sync
    _playerControls.dataSource = self;
    _playerControls.delegate = self;
    [_playerControls reloadData];
}

-(void)addPlaybackStateImageView
{
    //play overlay (label/image)
    self.playbackStateImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self.playbackStateImageView setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.overlayView insertSubview:self.playbackStateImageView atIndex:0];
    self.playbackStateImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.playbackStateImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view": self.playbackStateImageView}]];
    [self.playbackStateImageView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view": self.playbackStateImageView}]];
}

#pragma mark - Gestures

-(void)addOverlayGestures
{
    //tap
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

#pragma mark - Playback

-(void)setFullscreen:(BOOL)fullscreen
{
    [super setFullscreen:fullscreen];
    if (self.fullscreen)
    {
        [self setPlayerControls:[RVSAppClipPlayerFullscreenControls new]];
    }
    else
    {
        [self setPlayerControls:[RVSAppClipPlayerInlineControls new]];
    }
}

-(void)play
{
    RVSVerbose(@"ClipPlayerView play");
    [super play];    
    
    //if needed
    if ([RVSApplication application].activeClipPlayerView != self)
    {
        //pause old
        [[RVSApplication application].activeClipPlayerView pause];
        //set
        [RVSApplication application].activeClipPlayerView = self;
    }
}

-(void)stop
{
    [super stop];
    
    //remove as active
    if ([RVSApplication application].activeClipPlayerView == self)
    {
        [RVSApplication application].activeClipPlayerView = nil;
    }
}

-(void)togglePlayback
{
    RVSVerbose(@"ClipPlayerView toggle playback");
    if ([self isPlaying])
    {
        [self animatePlaybackStateWithImage:[UIImage imageNamed:@"overlay_pause"]];
        [self pause];
    }
    else
    {
        [self animatePlaybackStateWithImage:[UIImage imageNamed:@"overlay_play"]];
        [self play];
        if (self.shouldRewindOnPlay)
            [self rewind];
    }
}

-(void)handleRewind
{
    [self animatePlaybackStateWithImage:[UIImage imageNamed:@"overlay_backward"]];
    [self rewind];
    [self play];
}

-(void)animatePlaybackStateWithImage:(UIImage *)image
{
    self.playbackStateImageView.image = image;
    self.playbackStateImageView.transform = CGAffineTransformMakeScale(0.5, 0.5);
    self.playbackStateImageView.alpha = 0.6;
 
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                              weakSelf.playbackStateImageView.transform = CGAffineTransformIdentity;
                              weakSelf.playbackStateImageView.alpha = 0;
                          } completion:^(BOOL finished) {
                              weakSelf.playbackStateImageView.image = nil;
                          }];
}

-(void)reset
{
    [self stop];

    [self.fallbackImagePromise.promise cancel];
    self.fallbackImagePromise = nil;
    
    [self.coverImagePromise.promise cancel];
    self.coverImagePromise = nil;
    
    self.coverImageView.image = [UIImage imageNamed:@"default_cover"];
    
    self.mediaCurrentPosition = 0;
    self.mediaDuration = 0;
    self.channel = nil;
    self.program = nil;
    
    [self.playerControls reloadData];
}

-(void)clipView:(RVSClipView *)clipView isBuffering:(BOOL)isBuffering
{
    [self.playerControls invalidateIsBufferingState];
}

-(void)clipViewStateDidChange:(RVSClipView *)clipView
{
    //notify gui
    [self.playerControls invalidatePlayerState];
    
    if ([self.playerDelegate respondsToSelector:@selector(clipPlayerView:stateDidChange:)])
    {
        [self.playerDelegate clipPlayerView:self stateDidChange:clipView.state];
    }
}


-(void)clipView:(RVSClipView *)clipView didProgressWithCurrent:(NSTimeInterval)current duration:(NSTimeInterval)duration
{
    RVSVerbose(@"ClipPlayerView progress: %.2f / %2.f", current, duration);
    self.mediaCurrentPosition = current;
    self.mediaDuration = duration;
    
    [self.playerControls invalidateScrubber];
}

#pragma mark - Set data

-(void)setChannel:(NSObject<RVSChannel> *)channel program:(NSObject<RVSProgram> *)program
{
    _channel = channel;
    _program = program;
    
    [self.playerControls invalidateChannelAndProgram];
}

-(void)setCoverImage:(NSObject<RVSAsyncImage> *)coverImage
{
    [self setCoverImage:coverImage withFallbackImage:nil];
}


-(void)setCoverImage:(NSObject<RVSAsyncImage> *)coverImage withFallbackImage:(NSObject<RVSAsyncImage> *)fallbackImage
{
    //clear old to cancel
    if (coverImage)
    {
        __weak typeof(self) weakSelf = self;
        
        self.coverImagePromise = [coverImage imageWithSize:self.coverImageView.frame.size].newHolder;
        [self.coverImagePromise thenOnMain:^(UIImage *image){

            [weakSelf setCoverImageViewImage:image];
            
        } error:^(NSError *error) {
            
            if ([error.domain isEqualToString:@"RXPromise"] && error.code == -1)
                return; //canceled, no fallback needed
            
            RVSWarn(@"Cover image loading error, using fallback image");
            
            if (fallbackImage)
            {
                weakSelf.fallbackImagePromise = [fallbackImage imageWithSize:weakSelf.coverImageView.frame.size].newHolder;
                
                [weakSelf.fallbackImagePromise thenOnMain:^(UIImage *image) {
                    
                    [weakSelf setCoverImageViewImage:image];
                    
                } error:nil];
                
            }
        }]; //will keep def cover
    }
}

-(void)setCoverImageViewImage:(UIImage *)image
{
    if (self.self.program && [[RVSAppSpoilersUtil sharedUtil] shouldProtectProgramNamed:self.program.show.name])
    {
        self.coverImageView.image = [RVSAppSpoilersUtil protectedImageFrom:image];
    }
    else
    {
        self.coverImageView.image = image;
    }
}


#pragma mark - UI Delegate

-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberTouchDown:(UISlider *)sender
{
    [self beginScrubbing];
}

-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberTouchUp:(UISlider *)sender
{
    [self endScrubbing];
}

-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberValueChanged:(UISlider *)sender
{
    [self scrubToValue:sender.value minValue:sender.minimumValue maxValue:sender.maximumValue];
}

#pragma mark - UI Data Source

-(NSObject<RVSChannel> *)channelForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return self.channel;
}

-(NSObject<RVSProgram> *)programForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return self.program;
}

-(RVSClipViewState)playerStateForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return self.state;
}

-(NSTimeInterval)mediaCurrentPositionForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return self.mediaCurrentPosition;
}

-(NSTimeInterval)mediaDurationForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return self.mediaDuration;
}

-(BOOL)isPlayingStateForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return [self isPlaying];
}

-(BOOL)isBufferingStateForPlayerControls:(RVSAppClipPlayerControls *)playerControls
{
    return [self isBuffering];
}

@end
