//
//  RVSAppPostListReferringCell.h
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppUserView.h"
#import "RVSAppPostReferringDataObject.h"

@interface RVSAppPostListReferringCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet RVSAppUserView *userView;

@property (nonatomic) RVSAppPostReferringDataObject *data;

@end
