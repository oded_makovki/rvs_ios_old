//
//  RVSAppUserSearchResultCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/12/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppUserSearchResultCell.h"

@implementation RVSAppUserSearchResultCell

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 68);
}

@end
