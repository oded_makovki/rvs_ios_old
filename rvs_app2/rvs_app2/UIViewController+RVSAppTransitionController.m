//
//  UIViewController+RVSAppTransitionController.m
//  rvs_app2
//
//  Created by Barak Harel on 3/23/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "UIViewController+RVSAppTransitionController.h"

@implementation UIViewController (RVSAppTransitionController)

-(BOOL)usesPopTransitionController
{
    return NO;
}

-(BOOL)usesPushTransitionController
{
    return NO;
}

-(RVSAppTransitionController *)popTransitionController
{
    return nil;
}

-(RVSAppTransitionController *)pushTransitionController
{
    return nil;
}

@end
