//
//  RVSAppTimeSelectionLayoutAttributes.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppSelectionLayoutAttributes : UICollectionViewLayoutAttributes
@property (nonatomic) BOOL isSelected;
@end
