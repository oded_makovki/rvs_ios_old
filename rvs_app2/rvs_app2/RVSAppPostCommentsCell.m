//
//  RVSAppPostCommentsCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostCommentsCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppPostCommentsCell


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostCommentsCell];
    }
    
    return self;
}

-(void)initRVSAppPostCommentsCell
{
    [self loadContentsFromNib];
    self.commentsView.backgroundColor = [UIColor rvsPostCommentBackgroundColor];
    self.commentsView.minimumLineHeight = 16.0f;
    self.commentsView.maximumLineHeight = 30.0f;
    self.commentsView.fontPointSize = 12.0f;
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.commentsView clear];
}

-(void)setData:(RVSAppPostCommentsDataObject *)data
{
    [super setData:data];
    [self.commentsView setPostId:data.post.postId latestComments:data.latestComments moreCommentsCount:data.moreCommentsCount delegate:data.delegate];
}

-(CGSize)sizeForData:(RVSAppPostCommentsDataObject *)data
{    
    if ([data.latestComments count] > 0) //has comments
    {
        [self setData:data];
        
        CGSize size = [self.commentsView.textLabel sizeThatFits:CGSizeMake(self.commentsView.textLabel.frame.size.width, CGFLOAT_MAX)];
        
        return CGSizeMake(320, MAX(44, size.height + 16)); //min - same height as post info cell
        
    }
    return CGSizeMake(320, 0); //hidden
}

@end
