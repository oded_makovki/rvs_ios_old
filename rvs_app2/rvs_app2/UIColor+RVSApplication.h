//
//  UIColor+RVSApplication.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RVSApplication)
+(UIColor *)rvsTintColor;
+(UIColor *)rvsTintColor2;
+(UIColor *)rvsBackgroundColor;
+(UIColor *)rvsBackgroundColor2;
+(UIColor *)rvsBackgroundTextColor;
+(UIColor *)rvsFooterTextColor;

+(UIColor *)rvsLoginBackgroundColor;

+(UIColor *)rvsMenuBackgroundColor;
+(UIColor *)rvsMenuBackgroundColor2;

+(UIColor *)rvsNavigationBarColor;
+(UIColor *)rvsNavigationBarColor2;

+(UIColor *)rvsNavigationTextColor;

+(UIColor *)rvsTranscriptUnselectedColor;
+(UIColor *)rvsTranscriptSelectedColor;

+(UIColor *)rvsPostLinkColor;
+(UIColor *)rvsPostTextColor;
+(UIColor *)rvsPostTextColor2;
+(UIColor *)rvsPostUserNameColor;
+(UIColor *)rvsPostTimestampColor;
+(UIColor *)rvsPostActionButtonTextColor;

+(UIColor *)rvsPostInfoBackgroundColor;
+(UIColor *)rvsPostInfoTextColor;
+(UIColor *)rvsPostInfoTitleColor;

+(UIColor *)rvsPostReferringTextColor;
+(UIColor *)rvsPostReferringBackgroundColor;

+(UIColor *)rvsPostCommentBackgroundColor;

+(UIColor *)rvsCommentBackgroundColor;
+(UIColor *)rvsCommentLinkColor;
+(UIColor *)rvsCommentTextColor;
+(UIColor *)rvsCommentUserNameColor;
+(UIColor *)rvsCommentTimestampColor;
+(UIColor *)rvsCommentActionBackgroundColor;
+(UIColor *)rvsCommentActionBackgroundColor2;

+(UIColor *)rvsUserNameColor;

+(UIColor *)rvsProgramTextColor;

+(UIColor *)rvsClipPlayerBackgoundColor;

+(UIColor *)rvsHeaderBackgroundColor;
+(UIColor *)rvsHeaderTextColor;

+(UIColor *)rvsSearchTextColor;
+(UIColor *)rvsSearchTextColor2;

+(UIColor *)rvsDarkRedColor;

@end
