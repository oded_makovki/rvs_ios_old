//
//  RVSAppCommonViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/31/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"
#import "RVSApplication.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

#import "UIColor+RVSApplication.h"
#import "RVSAppPagesViewController.h"

//pushables
#import "RVSAppPushablePostListViewController.h"
#import "RVSAppPushableUserListViewController.h"
#import "RVSAppCommentListViewController.h"

#import "RVSAppSearchViewController.h"
#import "RVSAppSearchResultsViewController.h"

//webview
#import <SVModalWebViewController.h>
#import <SVWebViewController.h>

@interface RVSAppCommonViewController ()

@property (nonatomic) RVSNotificationObserver *menuWillAppearObserver;
@property (nonatomic) RVSNotificationObserver *networkStatusDidChangeObserver;
@property (nonatomic) RVSNotificationObserver *applicationWillReturnFromBackgroundObserver;
@property (nonatomic) RVSNotificationObserver *signInObserver;
@property (nonatomic) RVSNotificationObserver *signOutObserver;
@property (nonatomic) RVSNotificationObserver *applicationDidCreatePostObserver;
@property (nonatomic) RVSNotificationObserver *applicationDidCreateCommentObserver;
@property (nonatomic) RVSNotificationObserver *applicationWillDeletePostObserver;
@property (nonatomic) RVSNotificationObserver *applicationDidDeleteCommentObserver;
@property (nonatomic) RVSNotificationObserver *applicationDidUpdatePostObserver;
@property (nonatomic) BOOL isSignedOut;
@end

@implementation RVSAppCommonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setDefaultBackButton];
    
    self.edgesForExtendedLayout = UIRectEdgeAll;
    
    if (!self.contentView)
    {
        UIView *contentView = [[UIView alloc] init];
        self.contentView = contentView;
        [self.contentView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
//        [self.view insertSubview:self.contentView atIndex:0];
        
        [self.view addSubview:self.contentView];        
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[content]|" options:0 metrics:nil views:@{@"content":self.contentView}]];
        
        self.contentTopConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        self.contentBottomConstraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        
        [self.view addConstraints:@[self.contentTopConstraint, self.contentBottomConstraint]];
        
        //update
//        [self.contentView layoutIfNeeded];
    }
    
    self.contentView.backgroundColor = [UIColor clearColor];
    
    //auto note to self
    if (!self.navigationItem.title || [self.navigationItem.title length] == 0)
    {
        self.navigationItem.title = @"NAME ME!";
    }
}

-(void)setDelegate:(id<RVSAppCommonViewControllerDelegate>)delegate
{
    _delegate = delegate;
    
    
    __weak typeof(self) weakSelf = self;
    
    if (_delegate)
    {
        if ([_delegate respondsToSelector:@selector(menuWillAppear)])
        {
            self.menuWillAppearObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSAppMainMenuWillAppearNotification object:nil usingBlock:^(NSNotification *notification) {
                [weakSelf.delegate menuWillAppear];
            }];
        }
        
        if ([_delegate respondsToSelector:@selector(networkStatusDidChange)])
        {
            self.networkStatusDidChangeObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationNetworkStatusDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
                [weakSelf.delegate networkStatusDidChange];
            }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationDidCreatePost:)])
        {
            self.applicationDidCreatePostObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationDidCreatePostNotification object:nil usingBlock:^(NSNotification *notification)
                                               {
                                                   NSObject <RVSPost> *post = notification.userInfo[RVSApplicationDidCreatePostNewPostUserInfoKey];
                                                   
                                                   [weakSelf.delegate applicationDidCreatePost:post];
                                               }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationWillDeletePostId:)])
        {
            self.applicationWillDeletePostObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationWillDeletePostNotification object:nil usingBlock:^(NSNotification *notification)
            {
                NSString *postId = notification.userInfo[RVSApplicationWillDeletePostIdUserInfoKey];
                [weakSelf.delegate applicationWillDeletePostId:postId];
            }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationDidCreateComment:forPostId:)])
        {
            self.applicationDidCreateCommentObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationDidCreateCommentNotification object:nil usingBlock:^(NSNotification *notification)
                                                     {
                                                         NSObject <RVSComment> *comment = notification.userInfo[RVSApplicationDidCreateCommentNewCommentUserInfoKey];
                                                         
                                                         NSString *postId = notification.userInfo[RVSApplicationDidCreateCommentPostIdUserInfoKey];
                                                         
                                                         [weakSelf.delegate applicationDidCreateComment:comment forPostId:postId];
                                                     }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationDidDeleteCommentId:fromPostId:)])
        {
            self.applicationDidDeleteCommentObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationDidDeleteCommentNotification object:nil usingBlock:^(NSNotification *notification)
                                                      {
                                                          NSString *commentId = notification.userInfo[RVSApplicationDidDeleteCommentDeletedCommentIdUserInfoKey];
                                                          NSString *postId = notification.userInfo[RVSApplicationDidDeleteCommentPostIdUserInfoKey];
                                                          [weakSelf.delegate applicationDidDeleteCommentId:commentId fromPostId:postId];
                                                      }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationDidEnterBackgroundAt:andWillEnterForgroundAt:)])
        {
            self.applicationWillReturnFromBackgroundObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationWillReturnFromBackgroundNotification object:nil usingBlock:^(NSNotification *notification) {
                
                NSDate *backgoundDate = notification.userInfo[RVSApplicationDidEnterBackgroundDateUserInfoKey];
                NSDate *forgroundDate = notification.userInfo[RVSApplicationWillEnterForegroundDateUserInfoKey];
                
                [weakSelf.delegate applicationDidEnterBackgroundAt:backgoundDate andWillEnterForgroundAt:forgroundDate];
            }];
        }
        
        if ([_delegate respondsToSelector:@selector(applicationDidUpdatePostId:)])
        {
            self.applicationDidUpdatePostObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationDidUpdatePostNotification object:nil usingBlock:^(NSNotification *notification) {
                
                NSString *postId = notification.userInfo[RVSApplicationDidUpdatePostIdUserInfoKey];
                [weakSelf.delegate applicationDidUpdatePostId:postId];
                
            }];
        }
        
        if ([_delegate respondsToSelector:@selector(userDidSignIn)])
        {
            self.isSignedOut = [RVSSdk sharedSdk].isSignedOut;
            self.signInObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignedInNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification) {
                if (weakSelf.isSignedOut)
                {
                    weakSelf.isSignedOut = NO;
                    [weakSelf.delegate userDidSignIn];
                }
            }];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    BOOL shouldAddItems = YES;
    if ([self.delegate respondsToSelector:@selector(shouldAddDefaultRightNavigationItems)])
        shouldAddItems = [self.delegate shouldAddDefaultRightNavigationItems];
    
    if (shouldAddItems)
        [self.navigationItem setRightBarButtonItems:[self defaultRightBarButtonItems]];    
    
    if (![self isKindOfClass:[RVSAppPagesViewController class]] &&
        ![self conformsToProtocol:@protocol(RVSAppContainedPageViewController)])
    {
        [RVSApplication application].activeViewController = self;
    }
}

-(NSArray *)defaultRightBarButtonItems
{
    UIBarButtonItem *searchButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_search"] style:UIBarButtonItemStylePlain target:self action:@selector(searchPressed:)];
    
    return @[searchButton];
}

-(void)setContentTopConstraintconstant:(CGFloat)constant animated:(BOOL)animated
{
    self.contentTopConstraint.constant = constant;
    
    if (animated)
    {
        [UIView animateWithDuration:0.2f animations:^{
            [self.contentView layoutIfNeeded];
        }];
    }
}

#pragma mark - Right Buttons

- (void)setDefaultBackButton
{
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@""
                                style:UIBarButtonItemStylePlain
                                target:nil
                                action:nil];
    
    self.navigationItem.backBarButtonItem = btnBack;
}

- (void)searchPressed:(id)sender
{
//    [[RVSApplication application] showSearch];
    [self pushSearchViewController];
}

#pragma mark - Pushables:

#pragma mark - Search

-(void)pushSearchViewController
{
    RVSAppSearchViewController *vc = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppSearchViewController"];
    vc.owner = self;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushSearchResultsViewControllerWithSearchText:(NSString *)searchText withOwner:(UIViewController *)owner;
{
    RVSAppSearchResultsViewController *vc = [[RVSApplication application].mainStoryboard instantiateViewControllerWithIdentifier:@"RVSAppSearchResultsViewController"];
    vc.searchText = searchText;
    vc.owner = owner;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Post

-(void)pushPostListViewControlWithSinglePostId:(NSString *)postId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    vc.singlePostId = postId;
    vc.navigationItem.title = NSLocalizedString(@"post title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushPostListViewControlWithLikedPostesUserId:(NSString *)userId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController *vc = [[RVSAppPushablePostListViewController alloc] init];
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithLikedPostsByUser:userId];
    vc.navigationItem.title = NSLocalizedString(@"likes title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Comments

-(void)pushCommentListViewControllerWithPostId:(NSString *)postId shouldStartEdit:(BOOL)shouldStartEdit
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppCommentListViewController *vc = [[RVSAppCommentListViewController alloc] init];
    vc.postId = postId;
    vc.shouldStartEdit = shouldStartEdit;
        
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - User

-(void)pushUserDetailViewControlWithUserId:(NSString *)userId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushablePostListViewController*vc = [[RVSAppPushablePostListViewController alloc] init];
    vc.userId = userId;
    vc.postListFactory = [[RVSAppPostListFactory alloc] initWithUserFeedForUser:userId];
    
    if ([[RVSApplication application] isMyUserId:userId])
    {
        vc.navigationItem.title = NSLocalizedString(@"my profile title", nil);
    }
    else
    {
        vc.navigationItem.title = NSLocalizedString(@"profile title", nil);
    }

    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithFollowersOfUserId:(NSString *)userId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithFollowersOfUser:userId];
    vc.navigationItem.title = NSLocalizedString(@"user followers title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithFollowedByUserId:(NSString *)userId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithFollowedByUser:userId];
    vc.navigationItem.title = NSLocalizedString(@"user following title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithRepostingOfPostId:(NSString *)postId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithRepostingOfPost:postId];
    vc.navigationItem.title = NSLocalizedString(@"post reposting title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)pushUserListViewControlWithLikingOfPostId:(NSString *)postId
{
    [[RVSApplication application].activeClipPlayerView stop];
    
    RVSAppPushableUserListViewController *vc = [[RVSAppPushableUserListViewController alloc] init];
    
    vc.userListFactory = [[RVSAppUserListFactory alloc] initWithLikingOfPost:postId];
    vc.navigationItem.title = NSLocalizedString(@"post liking title", nil);
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - App View Delegate

-(void)view:(UIView *)view didSelectCommentsForPostId:(NSString *)postId shouldStartEdit:(BOOL)shouldStartEdit
{
    [self pushCommentListViewControllerWithPostId:postId shouldStartEdit:shouldStartEdit];
}

-(void)view:(UIView *)view didSelectUser:(NSObject<RVSUser> *)user
{
    [self pushUserDetailViewControlWithUserId:user.userId];
}

-(void)view:(UIView *)view didSelectLinkWithURL:(NSURL *)url
{
    if ([url.scheme isEqualToString:@"user"])
    {
		NSLog(@"user: %@", [url host]);
        [self pushUserDetailViewControlWithUserId:[url host]];
	}
    else if ([url.scheme isEqualToString:@"hash"])
    {
		NSLog(@"hash tag: %@", [url host]);
        [self pushSearchResultsViewControllerWithSearchText:[NSString stringWithFormat:@"#%@",[url host]] withOwner:self];
	}
    else if ([url.scheme isEqualToString:@"comments"])
    {
		NSLog(@"hash tag: %@", [url host]);
        [self pushCommentListViewControllerWithPostId:[url host] shouldStartEdit:NO];
	}
    else if ([url.scheme isEqualToString:@"reposting"])
    {
		NSLog(@"reposting tag: %@", [url host]);
        [self pushUserListViewControlWithRepostingOfPostId:[url host]];
	}
    else if ([url.scheme isEqualToString:@"liking"])
    {
		NSLog(@"liking tag: %@", [url host]);
        [self pushUserListViewControlWithLikingOfPostId:[url host]];
	}
    else
    {
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            // Execute the default behavior, which is opening the URL in Safari for URLs, starting a call for phone numbers, ...
            NSLog(@"Can canOpenURL: %@", url);
            [[RVSApplication application] showWebViewWithURL:url];
        }
        else
        {
            NSLog(@"Cannot canOpenURL: %@", url);
        }
	}
    
    //[[UIApplication sharedApplication] openURL:...]
    
}

#pragma mark - Other

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
