//
//  RVSAppProgramExcerptView.h
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSAppClipPlayerView.h"
#import "UIViewContainer.h"
#import "RVSAppSearchResultMatchesView.h"

@class RVSAppProgramExcerptView;
@protocol RVSAppProgramExcerptViewDelegate <NSObject>

@optional
-(void) programExcerptView:(RVSAppProgramExcerptView *)programExcerptView didSelectShareWithMediaContext:(NSObject <RVSMediaContext> *)mediaContext text:(NSString *)text;
@end

@interface RVSAppProgramExcerptView : UIView

@property (weak, nonatomic) IBOutlet RVSAppClipPlayerView *clipPlayerView;

@property (weak, nonatomic) IBOutlet UIButton *composeButton;

@property (weak, nonatomic) IBOutlet RVSAppSearchResultMatchesView *searchResultMatchesView;

@property (nonatomic) NSUInteger maxTextLabelLength; //for trunc
@property (weak, nonatomic) id <RVSAppProgramExcerptViewDelegate> delegate;
@property (nonatomic, readonly) NSObject <RVSProgramExcerpt> *programExcerpt;
@property (nonatomic, readonly) NSArray *searchMatches;

-(void)clear;

/**
 *  set program excerpt for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param programExcerpt   programExcerpt object
 *  @param searchMatches    optional search matches
 *  @param isSizingView     set to YES for optimiziation
 */
-(void)setProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt searchMatches:(NSArray *)searchMatches isSizingView:(BOOL)isSizingView;

/**
 *  set program excerpt for view. same as setPost:post isSizingView:NO
 *
 *  @param programExcerpt   programExcerpt object
 *  @param searchMatches    optional search matches
 */
-(void)setProgramExcerpt:(NSObject <RVSProgramExcerpt> *)programExcerpt searchMatches:(NSArray *)searchMatches;

@end
