//
//  RVSAppDataListView.m
//  rvs_app
//
//  Created by Barak Harel on 11/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataListView.h"

#import "RVSAppHeaderView.h"
#import "RVSAppDataCell.h"
#import "RVSAppDataListFooterCell.h"
#import "RVSAppRefreshCollectionView.h"
#import "PullToRefreshView.h"
#import "RVSApplication.h"
#import "RVSAppSnapFlowLayout.h"

#import "RVSAppDataCellManager.h"

#import <rvs_sdk_api helpers.h>

#define POST_VIEW_CELL_ID

@interface RVSAppDataListView() <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, PullToRefreshViewDelegate>

@property (weak, nonatomic) IBOutlet RVSAppRefreshCollectionView *collectionView;
@property (nonatomic) NSInteger numberOfSections;

@property (nonatomic) NSDate *lastUpdateDate;

@property (nonatomic) NSMutableSet *cellClassIdentifiers; //of nsstrings
@property (nonatomic) NSMutableSet *headerClassIdentifiers; //of nsstrings

@property (nonatomic) NSInteger footerSection;
@property (nonatomic) Class footerCellClass;
@property (nonatomic) RVSAppDataListFooterCell *footerCell;
@end

@implementation RVSAppDataListView

RVS_DYANAMIC_LOGGING_IMPLEMENTATION

-(id)initWithFrame:(CGRect)frame
{
    self=[super initWithFrame:frame];
    if (self)
    {        
        [self initRVSAppDataListView];
    }
    return self;
}


-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppDataListView];
}

-(void)initRVSAppDataListView
{

    self.cellClassIdentifiers = [NSMutableSet set];
    self.headerClassIdentifiers = [NSMutableSet set];
    
    //default
    [self setInternalLoadPhase:RVSAppDataListLoadPhaseDefault];
    self.refreshEnabled = YES;
    
    if (!self.collectionView) //add be code!
    {
        NSLog(@"Adding new collection view...");
        
        RVSAppSnapFlowLayout *layout = [[RVSAppSnapFlowLayout alloc] init];
//        layout.snapToCell = YES;
        layout.minimumLineSpacing = 0;
        
        RVSAppRefreshCollectionView *collectionView = [[RVSAppRefreshCollectionView alloc] initWithFrame:self.frame collectionViewLayout:layout];
        
        [self addSubview:collectionView];
        
        self.collectionView = collectionView;
        self.collectionView.backgroundColor = [UIColor clearColor];
        
        [self.collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.collectionView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self.collectionView}]];
    }
    
    self.nextPageTriggerSection = -1;
    self.nextPageTriggerDistance = 0;
    
    __weak typeof(self) weakSelf = self;
        
    self.collectionView.dataSource = weakSelf;
    self.collectionView.delegate = weakSelf;
    
    self.footerCellClass = [RVSAppDataListFooterCell class];
    
    [self setNumberOfSections:0];
    
    self.collectionView.refreshControl.delegate = weakSelf;
    self.collectionView.refreshControl.backgroundColor = [UIColor clearColor];
    self.collectionView.refreshControl.textColor = [UIColor darkGrayColor];
    self.collectionView.refreshControl.shadowColor = [UIColor clearColor];
    
    self.collectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
}

-(void)setContentInset:(UIEdgeInsets)contentInset
{
    self.collectionView.contentInset = contentInset;
    self.collectionView.refreshControl.startingContentInset = contentInset;
}

-(UIEdgeInsets)contentInset
{
    return self.collectionView.contentInset;
}

-(void)setKeyboardDismissMode:(UIScrollViewKeyboardDismissMode)keyboardDismissMode
{
    _keyboardDismissMode = keyboardDismissMode;
    self.collectionView.keyboardDismissMode = _keyboardDismissMode;
}

#pragma mark - Data/Sections

-(void)setNumberOfSections:(NSInteger)numberOfSections
{
    _numberOfSections = numberOfSections;
    self.footerSection = _numberOfSections; //if 1 sections, 0 = real section, 1 = footer
    
    [self.collectionView reloadData];
}

#pragma mark - Internal

-(NSString *)description
{
    NSString *desc = [super description];
    if ([self.dataSource respondsToSelector:@selector(descriptionForDataListView:)])
    {
        desc = [self.dataSource descriptionForDataListView:self];
    }
    
    return desc;
}

-(void)setLoadPhase:(RVSAppDataListLoadPhase)loadPhase
{
    [self setInternalLoadPhase:loadPhase];
    
    if (loadPhase == RVSAppDataListLoadPhaseShouldRequestNextData || loadPhase == RVSAppDataListLoadPhaseEndOfList)
    {
        [self.collectionView.refreshControl finishedLoading];
        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:self.footerSection]];
    }
}

-(void)setInternalLoadPhase:(RVSAppDataListLoadPhase)loadPhase
{
    _loadPhase = loadPhase;
    [self setFooterTypeFromLoadPhase];
}

-(void)setFooterTypeFromLoadPhase
{
    switch (self.loadPhase) {
            
        case RVSAppDataListLoadPhaseShouldRequestNextData:
        case RVSAppDataListLoadPhaseRequestingNextData:
            self.footerCell.type = RVSAppDataListFooterTypeLoading;
            break;
            
        case RVSAppDataListLoadPhaseEndOfList:
            self.footerCell.type = RVSAppDataListFooterTypeEndOfList;
            break;
            
        default:
            self.footerCell.type = RVSAppDataListFooterTypeDefault;
            break;
    }
}

-(void)setFooterCellClass:(Class)footerCellClass
{
    NSAssert([footerCellClass isSubclassOfClass:[RVSAppDataListFooterCell class]], @"footerCellClass must subclass RVSAppDataListFooterCell");
    
    if (_footerCellClass == footerCellClass)
        return;
    
    _footerCellClass = footerCellClass;
    [self.collectionView registerClass:_footerCellClass forCellWithReuseIdentifier:NSStringFromClass(_footerCellClass)];
    
}

-(NSIndexPath *)indexPathForItemAtPoint:(CGPoint)point
{
    return [self.collectionView indexPathForItemAtPoint:[self convertPoint:point toView:self.collectionView]];
}

#pragma mark - Gestures

-(NSArray *)gestureRecognizers
{
    NSMutableArray *gestureRecognizers = [[super gestureRecognizers] mutableCopy];
    [gestureRecognizers addObjectsFromArray:self.collectionView.gestureRecognizers];
    return [gestureRecognizers copy];
}

#pragma mark - Dequeue

-(RVSAppDataCell *)dequeueReusableCellWithClass:(Class)cellClass forIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = NSStringFromClass(cellClass);
    if (![self.cellClassIdentifiers containsObject:identifier])
    {
        NSLog(@"Registering class: %@", identifier);
        [self.cellClassIdentifiers addObject:identifier];
        [self.collectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
    }
    
    return [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
}

-(RVSAppHeaderView *)dequeueReusableHeaderWithClass:(Class)headerClass forIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = NSStringFromClass(headerClass);
    if (![self.headerClassIdentifiers containsObject:identifier])
    {
        [self.headerClassIdentifiers addObject:identifier];
        [self.collectionView registerClass:headerClass forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:identifier];
    }
    
    return [self.collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:identifier forIndexPath:indexPath];
}

#pragma mark - Cells/Headers

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == self.footerSection)
    {
        self.footerCell = [self.collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass(self.footerCellClass) forIndexPath:indexPath];
        [self setFooterTypeFromLoadPhase];
        
        if (self.loadPhase == RVSAppDataListLoadPhaseShouldRequestNextData)
        {
            [self setInternalLoadPhase:RVSAppDataListLoadPhaseRequestingNextData];
            [self.delegate nextDataForDataListView:self];
        }
        
        return self.footerCell;
    }
    else
    {
        Class cellClass = [self.dataSource dataListView:self cellClassForItemAtIndexPath:indexPath];
        NSObject *data = [self.dataSource dataListView:self cellDataForItemAtIndexPath:indexPath];
        
        RVSAppDataCell *dataCell = [self dequeueReusableCellWithClass:cellClass forIndexPath:indexPath];
        dataCell.data = data;
        
        if (indexPath.section == self.nextPageTriggerSection)
        {
            NSInteger dataCount = [self.dataSource dataListView:self numberOfItemsInSection:indexPath.section];
            
            RVSVerbose(@"View item: %i, data length:%i buffer: %d", indexPath.row, dataCount, dataCount - indexPath.row);
            
            if (self.loadPhase == RVSAppDataListLoadPhaseShouldRequestNextData &&
                self.nextPageTriggerDistance > 0 &&
                indexPath.row >= dataCount - self.nextPageTriggerDistance)
            {
                RVSVerbose(@"nextDataForDataListView %@", self.description);
                [self setInternalLoadPhase:RVSAppDataListLoadPhaseRequestingNextData];
                [self.delegate nextDataForDataListView:self];
            }
        }
        
        return dataCell;
    }
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader && [self.dataSource respondsToSelector:@selector(dataListView:headerClassForItemAtSection:)])
    {
        Class headerClass = [self.dataSource dataListView:self headerClassForItemAtSection:indexPath.section];
        
        RVSAppHeaderView *header = [self dequeueReusableHeaderWithClass:headerClass forIndexPath:indexPath];
        
        if ([self.dataSource respondsToSelector:@selector(dataListView:headerDataForItemAtSection:)])
            header.data = [self.dataSource dataListView:self headerDataForItemAtSection:indexPath.section];
        
        return header;
    }
    
    return nil;
}

#pragma mark - Collection View Delegate

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (section == self.footerSection)
    {
        return UIEdgeInsetsZero;
    }
    else if ([self.dataSource respondsToSelector:@selector(dataListView:insetForSectionAtIndex:)])
    {
        return [self.dataSource dataListView:self insetForSectionAtIndex:section];
    }
    else
    {
        return UIEdgeInsetsZero;
    }
}

-(void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppDataCell *dataCell = (RVSAppDataCell *)cell;
    [dataCell prepareForReuse];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (section == self.footerSection)
    {
        return 1;
    }
    else
    {
        return [self.dataSource dataListView:self numberOfItemsInSection:section];
    }
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.numberOfSections +1;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == self.footerSection) //footer
        return;
    
    if (![self.delegate respondsToSelector:@selector(dataListView:didSelectItemAtIndexPath:)])
        return;
    
    [self.delegate dataListView:self didSelectItemAtIndexPath:indexPath];
}


#pragma mark - CollectionView Sizing

-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if ([self.dataSource respondsToSelector:@selector(dataListView:minimumLineSpacingForSection:)])
    {
        return [self.dataSource dataListView:self minimumLineSpacingForSection:section];
    }
    else
    {
        return 0;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == self.footerSection)
    {
        if (self.footerCell)
        {
            return [self.footerCell size];
        }
        else
        {
            return [self.footerCellClass defaultSize];
        }
    }
    
    //return value
    CGSize cellSize;
    
    //get relevant data
    NSObject *data = [self.dataSource dataListView:self cellDataForItemAtIndexPath:indexPath];
    
    //check section class
    Class cellClass = [self.dataSource dataListView:self cellClassForItemAtIndexPath:indexPath];
    
    //check class prefered size:
    if ([cellClass respondsToSelector:@selector(preferredCellSize)])
    {
        cellSize = [cellClass preferredCellSize];
        if (! CGSizeEqualToSize(cellSize, CGSizeZero)) //ignore zero size
        {
            return cellSize;
        }
    }
    
    //use sizingcell and autolayout:
    return [[RVSAppDataCellManager sharedManager] sizeForClass:cellClass withData:data];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    //return value
    CGSize headerSize = CGSizeZero;
    
    
    if ([self.dataSource respondsToSelector:@selector(dataListView:headerClassForItemAtSection:)])
    {
        if (self.showHeadersForEmptySections || [self.dataSource dataListView:self numberOfItemsInSection:section] > 0)
        {    
            Class headerClass = [self.dataSource dataListView:self headerClassForItemAtSection:section];
            //check class prefered size:
            if ([headerClass respondsToSelector:@selector(preferredHeaderSize)])
            {
                headerSize = [headerClass preferredHeaderSize];
            }
        }
    }
    
    return headerSize;
}

#pragma mark - PullToRefreshView Delegate

-(void)setRefreshEnabled:(BOOL)refreshEnabled
{
    _refreshEnabled = refreshEnabled;
    self.collectionView.refreshControl.enabled = _refreshEnabled;
}

-(void)pullToRefreshViewShouldRefresh:(PullToRefreshView *)view
{
    [self setInternalLoadPhase:RVSAppDataListLoadPhaseRequestingDataReload];
    [self.delegate refreshDataListView:self];
}

-(NSDate *)pullToRefreshViewLastUpdated:(PullToRefreshView *)view
{
    return self.lastUpdateDate;
}

#pragma mark - Scroll

- (void)scrollToItemAtIndexPath:(NSIndexPath *)indexPath animated:(BOOL)animated

{
    [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:animated];
}

-(void)scrollToTop
{
    [self.collectionView setContentOffset:CGPointZero];
}

-(void)scrollToBottom
{
    CGSize contentSize = self.collectionView.collectionViewLayout.collectionViewContentSize;
    CGSize viewSize = self.collectionView.frame.size;
    
    CGPoint contentOffset = CGPointMake(0, contentSize.height - viewSize.height + self.collectionView.contentInset.bottom);
    if (contentOffset.y < -self.collectionView.contentInset.top)
        contentOffset.y = -self.collectionView.contentInset.top;
        
    [self.collectionView setContentOffset:contentOffset animated:YES];
}

-(void)setScrollsToTop:(BOOL)scrollsToTop
{
    self.collectionView.scrollsToTop = scrollsToTop;
}

-(BOOL)scrollEnabled
{
    return self.collectionView.scrollEnabled;
}

-(void)setScrollEnabled:(BOOL)scrollEnabled
{
    [self.collectionView setScrollEnabled:scrollEnabled];
}

-(RVSAppDataCell *)cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return (RVSAppDataCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
}

#pragma mark - Insert/Delete/Reload

-(void)insertItemsAtIndexPaths:(NSArray *)indexPaths
{
    [self.collectionView insertItemsAtIndexPaths:indexPaths];
}

-(void)reloadItemsAtIndexPaths:(NSArray *)indexPaths
{
    [self.collectionView reloadItemsAtIndexPaths:indexPaths];
}

-(void)deleteItemsAtIndexPaths:(NSArray *)indexPaths
{
    [self.collectionView deleteItemsAtIndexPaths:indexPaths];
}

-(void)reloadSections:(NSIndexSet *)sections
{
    [self.collectionView reloadSections:sections];
}

-(void)reloadSection:(NSUInteger)section
{
    [self reloadSections:[NSIndexSet indexSetWithIndex:section]];
}

#pragma mark - dealloc

-(void)dealloc
{
    self.collectionView.delegate = nil; //stupid but nessecery
}


#pragma mark - Helpers

+(NSArray *)indexPathsForSection:(NSUInteger)section startRow:(NSUInteger)startRow count:(NSUInteger)count
{
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:count];
    for (int i = startRow; i < startRow + count; i++)
    {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    return indexPaths;
}

+(NSArray *)indexPathsForIndexes:(NSIndexSet *)indexes inSection:(NSUInteger)section
{
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:[indexes count]];
    [indexes enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:idx inSection:section]];
    }];

    return indexPaths;
}

@end
