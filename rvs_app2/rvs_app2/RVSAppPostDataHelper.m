//
//  RVSAppPostDataHelper.m
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataHelper.h"

#import <rvs_sdk_api.h>
#import "NSArray+RVSApplication.h"

#import "RVSAppPostReferringDataObject.h"
#import "RVSAppPostViewDataObject.h"
#import "RVSAppPostInfoDataObject.h"
#import "RVSAppPostCommentsDataObject.h"
#import "RVSAppPostSpacerDataObject.h"

#import "RVSAppPostListReferringCell.h"
#import "RVSAppPostListCell.h"
#import "RVSAppPostInfoCell.h"
#import "RVSAppPostCommentsCell.h"
#import "RVSAppPostSpacerCell.h"

@implementation RVSAppPostDataHelper

+(Class)classForDataObject:(id)dataObject
{
    if ([dataObject isKindOfClass:[RVSAppPostReferringDataObject class]])
    {
        return [RVSAppPostListReferringCell class];
    }
    else if ([dataObject isKindOfClass:[RVSAppPostViewDataObject class]])
    {
        return [RVSAppPostListCell class];
    }
    else if ([dataObject isKindOfClass:[RVSAppPostCommentsDataObject class]])
    {
        return [RVSAppPostCommentsCell class];
    }
    else if ([dataObject isKindOfClass:[RVSAppPostInfoDataObject class]])
    {
        return [RVSAppPostInfoCell class];
    }
    else if ([dataObject isKindOfClass:[RVSAppPostSpacerDataObject class]])
    {
        return [RVSAppPostSpacerCell class];
    }
    
    return nil;
}

+(NSArray *)dataObjectsForPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate
{
    return [self dataObjectsForPost:post delegate:delegate searchMatches:nil];
}

+(NSArray *)dataObjectsForPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches
{
    NSMutableArray *data = [NSMutableArray new];
    
    [data addObject:[RVSAppPostSpacerDataObject dataWithPost:post color:[UIColor clearColor] height:8 edgeInsets:UIEdgeInsetsZero]];
    
    if (post.repostingUser)
    {
        [data addObject:[RVSAppPostReferringDataObject dataObjectFromPost:post delegate:delegate]];
    }
    
    [data addObject:[RVSAppPostViewDataObject dataObjectFromPost:post delegate:delegate searchMatches:searchMatches]];
    [data addObject:[RVSAppPostInfoDataObject dataObjectFromPost:post delegate:delegate]];
    [data addObject:[RVSAppPostCommentsDataObject dataObjectFromPost:post delegate:delegate]];
        
    return data;
}

+(NSArray *)dataObjectsForPosts:(NSArray *)posts delegate:(id<RVSAppViewDelegate>)delegate
{
    NSMutableArray *data = [NSMutableArray array];
    for (NSObject <RVSPost> *post in posts)
    {
        [data addObjectsFromArray:[RVSAppPostDataHelper dataObjectsForPost:post delegate:delegate]];
    }
    
    return data;
}

+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingPostId:(NSString *)postId
{
    return [data indexesInDataOfKindOfClass:[RVSAppPostDataObject class] containingDataId:postId inKeyPath:@"post.postId"];
}

+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingCommentsWithPostId:(NSString *)postId
{
    return [data indexesInDataOfKindOfClass:[RVSAppPostCommentsDataObject class] containingDataId:postId inKeyPath:@"post.postId"];
}

+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingInfoWithPostId:(NSString *)postId
{
    return [data indexesInDataOfKindOfClass:[RVSAppPostInfoDataObject class] containingDataId:postId inKeyPath:@"post.postId"];
}

@end
