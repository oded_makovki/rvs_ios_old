//
//  RVSAppPostListCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostViewDataCell.h"

@interface RVSAppPostListCell : RVSAppPostViewDataCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *postContainerTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *matchContainer;
@end
