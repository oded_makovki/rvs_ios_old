//
//  RVSAppLatestCommentsView.h
//  rvs_app2
//
//  Created by Barak Harel on 6/11/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppViewDelegate.h"
#import <TTTAttributedLabel.h>

@interface RVSAppPostCommentsView : UIView

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *textLabel; //needs to be init or set

@property (nonatomic) CGFloat fontPointSize;
@property (nonatomic) CGFloat minimumLineHeight;
@property (nonatomic) CGFloat maximumLineHeight;

@property (nonatomic) BOOL shouldParseHotwords;

/**
 *  Set data for view
 */
-(void)setPostId:(NSString *)postId latestComments:(NSArray *)latestComments moreCommentsCount:(NSInteger)moreCommentsCount delegate:(id <RVSAppViewDelegate>)delegate;

-(void)clear;

@end
