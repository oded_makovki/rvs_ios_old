//
//  RVSAppHeaderView.m
//  rvs_app2
//
//  Created by Barak Harel on 3/30/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppHeaderView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@implementation RVSAppHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        self.contentView = [[UIView alloc] initWithFrame:self.bounds];
        self.contentView.backgroundColor = [UIColor rvsHeaderBackgroundColor];
        self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.contentView];
        
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0.5)-[view]-(0.5)-|" options:0 metrics:nil views:@{@"view":self.contentView}]];
        [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.contentView}]];
        
        UILabel *label = [[UILabel alloc] initWithFrame:self.bounds];
        label.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:label];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[label]|" options:0 metrics:nil views:@{@"label":label}]];
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[label]|" options:0 metrics:nil views:@{@"label":label}]];
        
        label.font = [UIFont rvsLightFontWithSize:12];
        label.textAlignment = NSTextAlignmentCenter;
        
        label.text = @"";
        label.textColor = [UIColor rvsHeaderTextColor];
        label.backgroundColor = [UIColor clearColor];
        
        self.titleLabel = label;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
}

-(void)prepareForReuse
{
    //override this
}

+(CGSize)preferredHeaderSize
{
    //override this
    return CGSizeMake(320, 29.5);
}

-(void)setData:(RVSAppHeaderDataObject *)data
{
    _data = data;
    self.titleLabel.text = _data.title;
}

@end
