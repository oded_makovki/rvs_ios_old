//
//  RVSAppConfiguration.m
//  rvs_app2
//
//  Created by Barak Harel on 5/22/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppConfiguration.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>

extern NSString *RVSApplicationNetworkStatusDidChangeNotification;
NSString *RVSAppConfigurationDidChangeNotification = @"RVSAppConfigurationDidChangeNotification";

#define DEFAULT_CONFIGURATION_FILE_NAME @"appDefaultConfiguration"
#define DEFAULT_CONFIGURATION_FILE_EXT  @"plist"
#define LOCAL_CONFIGURATION_FILE_NAME   @"appConfiguration.plist"

@interface RVSAppConfiguration()
@property (nonatomic) RVSNotificationObserver *networkStatusObserver;

@property (nonatomic) RVSPromiseHolder *remoteConfigurationPromise;
@property (nonatomic) BOOL remoteConfigurationLoaded;

@property (nonatomic) NSDictionary *configuration;
@end

@implementation RVSAppConfiguration

+ (RVSAppConfiguration*)sharedConfiguration
{
    static RVSAppConfiguration* _sharedConfiguration;
    static dispatch_once_t sharedConfigurationOnce;
    
    dispatch_once(&sharedConfigurationOnce, ^{
        _sharedConfiguration = [[RVSAppConfiguration alloc] init];
    });
    
    return  _sharedConfiguration;
}

-(void)loadConfiguration
{
    [self loadLocalConfiguation];
    [self loadRemoteConfiguration];
    
    __weak typeof(self) weakSelf = self;
    self.networkStatusObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSApplicationNetworkStatusDidChangeNotification object:nil usingBlock:^(NSNotification *notification) {
        if (!weakSelf.remoteConfigurationLoaded && !weakSelf.remoteConfigurationPromise.promise.isPending)
        {
            [weakSelf loadRemoteConfiguration];
        }
    }];
}

-(void)loadLocalConfiguation
{
    self.configuration = [NSDictionary dictionaryWithContentsOfFile:[self localFilePath]]; //load from documents folder with default config fallback
    
    if (!self.configuration)
    {
        NSString * defaultConfigurtionPath = [[NSBundle mainBundle] pathForResource:DEFAULT_CONFIGURATION_FILE_NAME ofType:DEFAULT_CONFIGURATION_FILE_EXT];
        [self saveConfiguration:[NSDictionary dictionaryWithContentsOfFile:defaultConfigurtionPath]];
    }
}

-(void)loadRemoteConfiguration
{
    __weak typeof(self) weakSelf = self;
    
    self.remoteConfigurationPromise = [[RVSSdk sharedSdk] configuration].newHolder;
    [self.remoteConfigurationPromise thenOnMain:^(NSDictionary *configuration) {
        weakSelf.remoteConfigurationLoaded = YES;
        [weakSelf saveConfiguration:configuration];
    } error:^(NSError *error) {
        //keep local
    }];
}

-(void)setConfiguration:(NSDictionary *)configuration
{
    _configuration = configuration;
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppConfigurationDidChangeNotification object:self userInfo:nil];
}

-(NSDictionary *)searchSettings
{
    return self.configuration[@"searchSettings"];
}


-(NSString *)localFilePath
{
    // get the path to the plist file
    NSArray *paths =  NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:LOCAL_CONFIGURATION_FILE_NAME];
    
    return filePath;
}

-(void)saveConfiguration:(NSDictionary *)configuration
{
    if (!configuration)
        return;
    
    if ([self versionOfConfiguration:configuration] < [self versionOfConfiguration:self.configuration])
    {
        //warn, don't save
        return;
    }
    
    //updated self
    self.configuration = configuration;
    
    // write the plist back to the documents directory
    [self.configuration writeToFile:[self localFilePath] atomically:YES];
}

-(NSInteger)versionOfConfiguration:(NSDictionary *)configuration
{
    return [configuration[@"version"] integerValue];
}

@end
