//
//  RVSAppUserHeaderData.h
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVSAppUserView.h"
#import <rvs_sdk_api.h>

@interface RVSAppUserDataObject : NSObject
@property (nonatomic) NSObject <RVSUser> *user;
@property (weak, nonatomic) id <RVSAppUserViewDelegate> userViewDelegate;

-(id)initWithUser:(NSObject <RVSUser> *)user userViewDelegate:(id <RVSAppUserViewDelegate>)userViewDelegate;

+(NSArray *)dataObjectsFromUsers:(NSArray *)users
                userViewDelegate:(id<RVSAppUserViewDelegate>)userViewDelegate;

+(RVSAppUserDataObject *)dataObjectsFromUser:(NSObject <RVSUser> *)user userViewDelegate:(id<RVSAppUserViewDelegate>)userViewDelegate;
@end
