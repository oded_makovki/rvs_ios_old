//
//  NSDictionary+NotNull.h
//  rvs_app
//
//  Created by Barak Harel on 12/9/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (NotNull)
- (id)objectForKeyNotNull:(id)key;
@end
