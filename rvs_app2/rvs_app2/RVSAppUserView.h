//
//  RVSAppUserView.h
//  rvs_app2
//
//  Created by Barak Harel on 3/4/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import "RVSApplication.h"

@class RVSAppUserView;
@protocol RVSAppUserViewDelegate <NSObject>
@optional
-(void)userView:(RVSAppUserView *)userView didChangeWithUser:(NSObject <RVSUser> *)user;
@end

@interface RVSAppUserView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *userThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *followToggleButton;

@property (nonatomic) NSObject <RVSUser> *user;
@property (weak, nonatomic) id <RVSAppUserViewDelegate> delegate;

-(void)clear;
-(void)userDidChange;

/**
 *  set user for view. if isSizingView is set to YES, some subviews values will not be set.
 *
 *  @param user         user object
 *  @param isSizingView set to YES for optimiziation
 */
-(void)setUser:(NSObject <RVSUser> *)user isSizingView:(BOOL)isSizingView;

/**
 *  set user for view. same as setUser:user isSizingView:NO
 *
 *  @param user user object
 */
-(void)setUser:(NSObject <RVSUser> *)user;
@end
