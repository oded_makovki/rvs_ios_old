//
//  RVSAppCenterNavigationControllerDelegate.m
//  rvs_app2
//
//  Created by Barak Harel on 3/17/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCenterNavigationControllerDelegate.h"
#import "RVSApplication.h"

@interface RVSAppCenterNavigationControllerDelegate ()

@end

@implementation RVSAppCenterNavigationControllerDelegate

-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [[RVSApplication application].mainViewController updateMenuGestures];
}

@end
