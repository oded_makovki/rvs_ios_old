//
//  RVSAppSearchResultMatchesView.m
//  rvs_app2
//
//  Created by Barak Harel on 5/12/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchResultMatchesView.h"
#import "RVSAppSearchResultMatchCell.h"
#import "UIColor+RVSApplication.h"
#import "RVSAppSearchMatchData.h"

@interface RVSAppSearchResultMatchesView() <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic) NSInteger currentSearchMatchIndex;
@property (nonatomic) NSArray *searchMatchesData; //of RVSAppSearchMatchData
@end


@implementation RVSAppSearchResultMatchesView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppSearchResultMatchesView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppSearchResultMatchesView];
}

-(void)initRVSAppSearchResultMatchesView
{
    if (self.matchesCollectionView)
    {
        [self.matchesCollectionView registerClass:[RVSAppSearchResultMatchCell class] forCellWithReuseIdentifier:@"MatchCell"];
        self.matchesCollectionView.scrollsToTop = NO;
        
        self.matchesCollectionView.dataSource = self;
        self.matchesCollectionView.delegate = self;
    }
    
    if (self.matchesPageControl)
    {
        self.matchesPageControl.currentPageIndicatorTintColor = [UIColor rvsTintColor];
        self.matchesPageControl.pageIndicatorTintColor = [UIColor rvsBackgroundColor];
    }
}

-(void)clear
{
    self.searchMatchesData = nil;
    self.matchesPageControl.currentPage = 0;
    self.matchesPageControl.numberOfPages = 1;
    self.currentSearchMatchIndex = -1;
    self.matchesCollectionView.contentOffset = CGPointZero;
    [self.matchesCollectionView reloadData];
}

-(void)setSearchMatchesData:(NSArray *)searchMatchesData
{
    _searchMatchesData = searchMatchesData;
    
    if ([_searchMatchesData count] > 0)
    {
        self.matchesPageControl.currentPage = 0;
        self.matchesPageControl.numberOfPages = [_searchMatchesData count];
        self.currentSearchMatchIndex = 0;
    }
    
    [self.matchesCollectionView reloadData];    
    [self currentSearchMatchDidChange];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RVSAppSearchResultMatchCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MatchCell" forIndexPath:indexPath];
    
    RVSAppSearchMatchData *searchMatchData = self.searchMatchesData[indexPath.row];
    [cell setSearchMatchData:searchMatchData];
    
    return cell;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.searchMatchesData count];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView != self.matchesCollectionView)
        return;
    
    [self matchesCollectionViewDidScroll];
}

-(void)matchesCollectionViewDidScroll
{
    self.matchesPageControl.currentPage = lround(self.matchesCollectionView.contentOffset.x /
                                                 (self.matchesCollectionView.contentSize.width / self.matchesPageControl.numberOfPages));
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (scrollView != self.matchesCollectionView)
        return;
    
    if (!decelerate)
        [self matchesCollectionViewDidEndScrolling];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (scrollView != self.matchesCollectionView)
        return;
    
    [self matchesCollectionViewDidEndScrolling];
}


-(void)matchesCollectionViewDidEndScrolling
{
    NSIndexPath *indexPath = [[self.matchesCollectionView indexPathsForVisibleItems] firstObject];
    if (!indexPath)
        return;
    
    self.currentSearchMatchIndex = indexPath.row;
    [self currentSearchMatchDidChange];
}

-(void)currentSearchMatchDidChange
{
    if (self.delegate)
    {
        [self.delegate searchResultMatchesView:self currentSearchMatchDidChangeWithIndex:self.currentSearchMatchIndex];
    }
}


-(void)dealloc
{
    self.matchesCollectionView.dataSource = nil;
    self.matchesCollectionView.delegate = nil;
}

@end
