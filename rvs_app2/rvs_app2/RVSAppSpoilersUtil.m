//
//  RVSAppSpoilersUtil.m
//  rvs_app2
//
//  Created by Barak Harel on 5/20/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSpoilersUtil.h"
#import "UIImage+ImageEffects.h"
#import "RVSAppListUtilConfigController.h"
#import "RVSApplication.h"

#define DATA_FILE_NAME @"unspoiledProgramNames.plist"

@interface RVSAppSpoilersUtil() <RVSAppListUtilConfigControllerDelegate>
@property (nonatomic) NSArray *unspoiledProgramNames;
@property (nonatomic) RVSAppListUtilConfigController *configViewController;
@end

@implementation RVSAppSpoilersUtil

+ (RVSAppSpoilersUtil*)sharedUtil
{
    static RVSAppSpoilersUtil* _sharedUtil;
    static dispatch_once_t sharedUtilOnce;
    
    dispatch_once(&sharedUtilOnce, ^{
        _sharedUtil = [[RVSAppSpoilersUtil alloc] init];
    });
    
    return  _sharedUtil;
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        [self reloadUnspoiledProgramNames];
    }
    
    return self;
}

-(NSString *)dataFilePath
{
    // get the path to the plist file
    NSArray *paths =  NSSearchPathForDirectoriesInDomains(NSLibraryDirectory,  NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:DATA_FILE_NAME];
    
    return filePath;
}

-(void)reloadUnspoiledProgramNames
{
    self.unspoiledProgramNames = [NSArray arrayWithContentsOfFile:[self dataFilePath]];
}

-(void)saveUnspoiledProgramNames:(NSArray *)unspoiledProgramNames
{
    if (!unspoiledProgramNames)
        return;
    
    //updated self
    self.unspoiledProgramNames = unspoiledProgramNames;
    
    // write the plist back to the documents directory
    [self.unspoiledProgramNames writeToFile:[self dataFilePath] atomically:YES];
}

-(BOOL)shouldProtectProgramNamed:(NSString *)programName
{
    if ([[self.unspoiledProgramNames filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"%@ LIKE[cd] SELF", programName]] count] > 0)
    {
        return YES;
    }
    return NO;
}

+(UIImage *)protectedImageFrom:(UIImage *)image
{
    UIColor *tintColor = nil; //[UIColor colorWithWhite:1.0 alpha:0.3];
    return [image applyBlurWithRadius:10 tintColor:tintColor saturationDeltaFactor:1 maskImage:nil];
}

-(void)showConfig
{
    self.configViewController = [RVSAppListUtilConfigController showWithFilename:DATA_FILE_NAME];
    self.configViewController.navigationItem.title = @"Unspoiled Programs";
    self.configViewController.delegate = self;
}

-(void)listUtilController:(RVSAppListUtilConfigController *)sender didSaveWithValues:(NSArray *)values
{
    [self reloadUnspoiledProgramNames];
}

@end
