//
//  RVSAppLeftMenuViewController.h
//  rvs_app2
//
//  Created by Barak Harel on 3/13/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MMDrawerController.h>

@interface RVSAppLeftMenuViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *shadowView;

+(MMDrawerControllerDrawerVisualStateBlock)cardVisualStateBlock;
+(MMDrawerControllerDrawerVisualStateBlock)parallaxVisualStateBlockWithParallaxFactor:(CGFloat)parallaxFactor;
@end
