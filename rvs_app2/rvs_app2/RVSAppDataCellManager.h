//
//  RVSAppDataCellManager.h
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppDataCellManager : NSObject

+ (RVSAppDataCellManager*)sharedManager;
- (CGSize)sizeForClass:(Class)cellClass withData:(NSObject *)data;

@end
