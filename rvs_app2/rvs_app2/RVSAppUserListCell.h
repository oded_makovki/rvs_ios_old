//
//  RVSAppUserListCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDataCell.h"
#import "RVSAppUserListView.h"

@interface RVSAppUserListCell : RVSAppUserDataCell
@property (weak, nonatomic) IBOutlet RVSAppUserListView *userView;
@end
