//
//  UIViewContainer.m
//  RayV_TV
//
//  Created by Barak Harel on 9/12/13.
//
//

#import "UIViewContainer.h"

@implementation UIViewContainer

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    //get hitting view
    UIView *hitView = [super hitTest:point withEvent:event];
    
    //if not self, return it
    if (hitView != self)
        return hitView;
    
    //else (hitting self), return nil
    return nil;
}

@end
