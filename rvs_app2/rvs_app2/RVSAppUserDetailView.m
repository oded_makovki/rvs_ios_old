//
//  RVSAppUserView.m
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDetailView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"
#import "RVSApplication.h"
#import "RVSAppStringUtil.h"

@interface RVSAppUserDetailView()
@end

@implementation RVSAppUserDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppUserDetailView];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppUserDetailView];
}

-(void)initRVSAppUserDetailView
{
    self.userThumbnail.contentMode = UIViewContentModeScaleAspectFill;
    
    self.userNameLabel.font = [UIFont rvsBoldFontWithSize:18];
    self.postsCountLabel.font = [UIFont rvsRegularFontWithSize:18];
    
    self.likesTitleLabel.text = NSLocalizedString(@"user detail likes count title", nil);
    self.mentionsTitleLabel.text = NSLocalizedString(@"user detail mentions count title", nil);
    self.followersTitleLabel.text = NSLocalizedString(@"user detail followers count title", nil);
    self.followingTitleLabel.text = NSLocalizedString(@"user detail following count title", nil);
    
    self.followersCountLabel.font =
    self.followingCountLabel.font =
    self.mentionsCountLabel.font =
    self.likesCountLabel.font = [UIFont rvsRegularFontWithSize:18];
    
    self.followersTitleLabel.font =
    self.followingTitleLabel.font =
    self.mentionsTitleLabel.font =
    self.likesTitleLabel.font = [UIFont rvsRegularFontWithSize:12];
    
    [self.optionsButton setTitleColor:[UIColor rvsTintColor] forState:UIControlStateNormal];
    
    if (self.likesCountTapView)
        [self addTapGestureToView:self.likesCountTapView target:self action:@selector(likesCountTapped:)];
    
    if (self.mentionsCountTapView)
        [self addTapGestureToView:self.mentionsCountTapView target:self action:@selector(mentionsCountTapped:)];
    
    if (self.followingCountTapView)
        [self addTapGestureToView:self.followingCountTapView target:self action:@selector(followingCountTapped:)];
    
    if (self.followersCountTapView)
        [self addTapGestureToView:self.followersCountTapView target:self action:@selector(followersCountTapped:)];
    
}


-(void)addTapGestureToView:(UIView *)view target:(id)target action:(SEL)action
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:action];
    [view addGestureRecognizer:tap];
}

-(void)clear
{
    [super clear];
    
    self.followersCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
    self.followingCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
    self.likesCountLabel.text =     NSLocalizedString(@"counter placeholder", nil);
    self.mentionsCountLabel.text =  NSLocalizedString(@"counter placeholder", nil);
    self.postsCountLabel.text = NSLocalizedString(@"counter placeholder", nil);
}

-(void)userDidChange
{
    [super userDidChange];
    
    self.followersCountLabel.text = [RVSAppStringUtil prettyNumber:self.user.followerCount];
    self.followingCountLabel.text = [RVSAppStringUtil prettyNumber:self.user.followingCount];
    self.likesCountLabel.text = [RVSAppStringUtil prettyNumber:self.user.likeCount];
    self.mentionsCountLabel.text = @"0";
    self.postsCountLabel.text = [NSString stringWithFormat:@"%@ Whips", [RVSAppStringUtil prettyNumber:self.user.postCount]];
}

-(void)mentionsCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userDetailView:didSelectMentionsForUser:)])
    {
        [self.delegate userDetailView:self didSelectMentionsForUser:self.user];
    }
}

-(void)likesCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userDetailView:didSelectLikesCountForUser:)])
    {
        [self.delegate userDetailView:self didSelectLikesCountForUser:self.user];
    }
}

-(void)followersCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userDetailView:didSelectFollowersCountForUser:)])
    {
        [self.delegate userDetailView:self didSelectFollowersCountForUser:self.user];
    }
}

-(void)followingCountTapped:(UITapGestureRecognizer *)tap
{
    if ([self.delegate respondsToSelector:@selector(userDetailView:didSelectFollowingCountForUser:)])
    {
        [self.delegate userDetailView:self didSelectFollowingCountForUser:self.user];
    }
}

@end
