//
//  RVSAppSearchSuggestionCell.h
//  rvs_app2
//
//  Created by Barak Harel on 3/30/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import <TTTAttributedLabel.h>

@interface RVSAppSearchSuggestionCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet UILabel *rightLabel;
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *textLabel;
@property (nonatomic) NSString *data;
@end
