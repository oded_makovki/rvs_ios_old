//
//  RVSAppChannelCell.m
//  rvs_app2
//
//  Created by Barak Harel on 4/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppChannelCell.h"
#import "UIView+NibLoading.h"

@implementation RVSAppChannelCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppChannelCell];
    }
    
    return self;
}

-(void)initRVSAppChannelCell
{
    [self loadContentsFromNib];
    self.backgroundColor = [UIColor rvsBackgroundColor];
}

-(void)setData:(RVSAppChannelDataObject *)data
{
    [super setData:data];
    [self.channelView setChannel:data.channel];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.channelView clear];
}

-(CGSize)sizeForData:(RVSAppChannelDataObject *)data
{
    return CGSizeMake(320, 80);
}

@end
