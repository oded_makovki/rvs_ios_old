//
//  UIColor+RVSApplication.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "UIColor+RVSApplication.h"
#import "UIColor+HexString.h"

@implementation UIColor (RVSApplication)

+(UIColor *)rvsTintColor
{
    return [UIColor colorWithRGBValue:0xff6600];
    //[UIColor colorWithRGBValue:0x7b2b75];
    //[UIColor colorWithRed:64.0/255 green:138.0/255 blue:190.0/255 alpha:1];
    //[UIColor colorWithRGBValue:0x0072EF];
}

+(UIColor *)rvsTintColor2
{
    return [UIColor colorWithRGBValue:0x46A500];
}

+(UIColor *)rvsNavigationBarColor
{
    return [UIColor colorWithRGBValue:0xf3f4f5];
}

+(UIColor *)rvsNavigationBarColor2
{
    return [UIColor colorWithRGBValue:0xe9eaed];
}

+(UIColor *)rvsBackgroundColor
{
    return [UIColor colorWithRGBValue:0xd0d3d8];
    //[UIColor colorWithRed:228.0/255.0 green:228.0/255.0 blue:233.0/255.0 alpha:1.0];
    //[UIColor colorWithRGBValue:0xd1d1d1];
}

+(UIColor *)rvsBackgroundColor2
{
    return [UIColor whiteColor];
}

+(UIColor *)rvsBackgroundTextColor
{
    return [UIColor colorWithRGBValue:0xa2a3a7];
}

+(UIColor *)rvsMenuBackgroundColor
{
    return [UIColor colorWithRGBValue:0x4e5665];
}

+(UIColor *)rvsMenuBackgroundColor2
{
    return [UIColor colorWithRGBValue:0x6a7180];
}

+(UIColor *)rvsNavigationTextColor
{
    return [UIColor colorWithRGBValue:0x6a7180];
}

+(UIColor *)rvsFooterTextColor
{
    return [UIColor darkGrayColor];
}

+(UIColor *)rvsLoginBackgroundColor
{
    return [UIColor colorWithRGBValue:0xF3F4F5];
}

//cc
+ (UIColor *)rvsTranscriptUnselectedColor
{
    return [UIColor colorWithWhite:1 alpha:0.3];
}

+ (UIColor *)rvsTranscriptSelectedColor
{
    return [UIColor whiteColor];
}

//posts q   
+(UIColor *)rvsPostLinkColor
{
    return [UIColor rvsPostTextColor]; //[UIColor colorWithRed:64.0/255 green:138.0/255 blue:190.0/255 alpha:1];
}

+(UIColor *)rvsPostTextColor
{
    return [UIColor colorWithRGBValue:0x4E5665]; //0xB5CCE6
}

+(UIColor *)rvsPostTextColor2
{
    return [UIColor colorWithRGBValue:0x687283]; //0xB5CCE6
}

+(UIColor *)rvsPostUserNameColor
{
    return [self rvsTintColor]; //[UIColor colorWithRGBValue:0x25292A]; //0xE4F0FE
}

+(UIColor *)rvsPostTimestampColor
{
    return [UIColor colorWithRGBValue:0xBEC1C5]; //0x8EA2B1
}

+(UIColor *)rvsPostActionButtonTextColor
{
    return [UIColor colorWithRGBValue:0x858b99];
}

+(UIColor *)rvsPostReferringBackgroundColor
{
    return [UIColor colorWithRGBValue:0x858b99];
}

+(UIColor *)rvsPostReferringTextColor
{
    return [UIColor colorWithRGBValue:0xC9CED6];
}

#pragma mark - Info

+(UIColor *)rvsPostInfoBackgroundColor
{
    return [UIColor colorWithRGBValue:0xf9f9f9];
}

+(UIColor *)rvsPostInfoTextColor
{
    return [UIColor colorWithRGBValue:0x868C99];
}

+(UIColor *)rvsPostInfoTitleColor
{
    return [UIColor rvsTintColor];
}

#pragma mark - Comment

+(UIColor *)rvsPostCommentBackgroundColor
{
    return [UIColor rvsPostInfoBackgroundColor];
}

+(UIColor *)rvsCommentBackgroundColor
{
    return [UIColor whiteColor];
}

+(UIColor *)rvsCommentLinkColor
{
    return [self rvsPostLinkColor];
}

+(UIColor *)rvsCommentTextColor
{
    return [UIColor colorWithRGBValue:0x868C99];
}

+(UIColor *)rvsCommentUserNameColor
{
    return [UIColor rvsTintColor];
}

+(UIColor *)rvsCommentTimestampColor
{
    return [UIColor rvsPostTimestampColor];
}

+(UIColor *)rvsCommentActionBackgroundColor
{
    return [UIColor colorWithRGBValue:0x6A7180];
}

+(UIColor *)rvsCommentActionBackgroundColor2
{
    return [UIColor colorWithRGBValue:0xAAAFB8];
}

#pragma mark - User


+(UIColor *)rvsUserNameColor
{
    return [UIColor colorWithRGBValue:0xE4F0FE]; //
}

#pragma mark - Program

+(UIColor *)rvsProgramTextColor
{
    return [UIColor whiteColor];
}

#pragma mark - Clip View

+(UIColor *)rvsClipPlayerBackgoundColor
{
    return [UIColor colorWithRGBValue:0x6A7180];
}

#pragma mark - Headers

+(UIColor *)rvsHeaderBackgroundColor
{
    return [UIColor colorWithRGBValue:0xFAFAFA];
}

+(UIColor *)rvsHeaderTextColor
{
    return [UIColor colorWithRGBValue:0x6A7180];
}

#pragma mark - Search

+(UIColor *)rvsSearchTextColor
{
    return [UIColor blackColor];
}

+(UIColor *)rvsSearchTextColor2
{
    return [UIColor colorWithRGBValue:0x868C99];
}

#pragma mark - Common

+(UIColor *)rvsDarkRedColor
{
    return [UIColor colorWithRGBValue:0xD72929];
}

@end
