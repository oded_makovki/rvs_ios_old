//
//  RVSAppPostListCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/25/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppPostListCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppPostListCell()
@end

@implementation RVSAppPostListCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostListCell];
    }
    
    return self;
}

-(void)initRVSAppPostListCell
{
    [self loadContentsFromNib];
    
    self.postView.shouldParseHotwords = YES;
    self.postView.isUsingLongTimeFormat = NO;
    
    self.postView.clipsToBounds = YES;    
    
    self.postView.textLabel.preferredMaxLayoutWidth = 240;
    self.postView.timeLabel.preferredMaxLayoutWidth = 240;
    
    self.postContainerTopConstraint = [NSLayoutConstraint constraintWithItem:self.matchContainer attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:0];
    
    [self addConstraint:self.postContainerTopConstraint];
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
}

-(void)setData:(RVSAppPostViewDataObject *)data
{
    [super setData:data];
    self.postContainerTopConstraint.constant = [self.postView heightForSearchMatches];
}

-(CGSize)sizeForData:(RVSAppPostViewDataObject *)data
{
    [self setData:data];
    
    CGSize size = [self.postView.textLabel sizeThatFits:CGSizeMake(self.postView.textLabel.frame.size.width, CGFLOAT_MAX)];
//    NSLog(@"text size: %@", NSStringFromCGSize(size));
    
    CGSize finalSize = CGSizeMake(320, 226.5 + MAX(size.height + 40, 56) + self.postContainerTopConstraint.constant); //40 = text:30+text+10, 56 = image:8+40+8; +top constraint.
    
//    NSLog(@"final size: %@", NSStringFromCGSize(finalSize));
    return finalSize;
}


@end
