//
//  RVSAppTimeSelectionFlowLayout.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSelectionFlowLayout.h"
#import "RVSAppSelectionLayoutAttributes.h"

@implementation RVSAppSelectionFlowLayout
+(Class)layoutAttributesClass
{
    return [RVSAppSelectionLayoutAttributes class];
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
    [self updateSelection:attributes];
    return attributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    RVSAppSelectionLayoutAttributes *attribute = (RVSAppSelectionLayoutAttributes *)[super layoutAttributesForItemAtIndexPath:indexPath];
    
    [self updateSelection:@[ attribute ]];
    return attribute;
}

-(void)updateSelection:(NSArray *)attributes
{
    for (RVSAppSelectionLayoutAttributes *attribute in attributes)
    {
        attribute.isSelected = [self.delegate collectionView:self.collectionView layout:self shouldSelectItemAtIndexPath:attribute.indexPath];
    }
}

@end
