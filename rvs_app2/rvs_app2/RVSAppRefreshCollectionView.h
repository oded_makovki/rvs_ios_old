//
//  RVSAppRefreshCollectionView.h
//  rvs_app
//
//  Created by Barak Harel on 11/24/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullToRefreshView.h"

@interface RVSAppRefreshCollectionView : UICollectionView
@property (weak, nonatomic) PullToRefreshView *refreshControl;

@end
