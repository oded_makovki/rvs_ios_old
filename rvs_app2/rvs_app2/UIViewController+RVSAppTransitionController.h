//
//  UIViewController+RVSAppTransitionController.h
//  rvs_app2
//
//  Created by Barak Harel on 3/23/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppTransitionController.h"

@interface UIViewController (RVSAppTransitionController)

-(RVSAppTransitionController *)pushTransitionController;
-(BOOL)usesPushTransitionController;

-(RVSAppTransitionController *)popTransitionController;
-(BOOL)usesPopTransitionController;

@end
