//
//  RVSAppChannelDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 4/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

@interface RVSAppChannelDataObject : NSObject
@property (nonatomic) NSObject <RVSChannel> *channel;
@end
