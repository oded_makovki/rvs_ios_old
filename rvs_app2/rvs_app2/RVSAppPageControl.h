//
//  RVSAppPagesControl.h
//  PageTest
//
//  Created by Barak Harel on 12/16/13.
//  Copyright (c) 2013 Barak Harel. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RVSAppPageControl;

@protocol RVSAppPageControlDelegate <NSObject>
@optional
- (void)pageControlPageDidChange:(RVSAppPageControl *)pageControl;
@end

@interface RVSAppPageControl : UIView

// Set these to control the PageControl.
@property (nonatomic) NSInteger currentPage;
@property (nonatomic) NSInteger numberOfPages;

// Customize these as well as the backgroundColor property.
@property (nonatomic) UIColor *dotColorCurrentPage;
@property (nonatomic) UIColor *dotColorOtherPage;
@property (nonatomic) CGFloat dotDiameter;
@property (nonatomic) CGFloat dotSpacing;

// Optional delegate for callbacks when user taps a page dot.
@property (weak, nonatomic) id <RVSAppPageControlDelegate> delegate;
@end
