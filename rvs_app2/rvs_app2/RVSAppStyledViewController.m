//
//  RVSAppStyledViewController.m
//  rvs_app
//
//  Created by Barak Harel on 11/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppStyledViewController.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppStyledViewController ()

@end

@implementation RVSAppStyledViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                       NSForegroundColorAttributeName: [UIColor rvsNavigationTextColor],
                                                                       NSFontAttributeName: [UIFont rvsRegularFontWithSize:18]
                                                                       }];
    
    self.navigationController.navigationBar.tintColor = [UIColor rvsTintColor];
    self.view.backgroundColor = [UIColor rvsBackgroundColor];
    
    UIBarButtonItem *btnBack = [[UIBarButtonItem alloc]
                                initWithTitle:@""
                                style:UIBarButtonItemStylePlain
                                target:nil action:nil];
    
    self.navigationItem.backBarButtonItem = btnBack;    
}

@end
