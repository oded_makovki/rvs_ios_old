//
//  RVSAppPostReffererDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataObject.h"
#import <rvs_sdk_api.h>

@interface RVSAppPostReferringDataObject : RVSAppPostDataObject

-(NSObject <RVSPostReferringUser> *)repostingUser;

@end
