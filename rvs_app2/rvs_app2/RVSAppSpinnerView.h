//
//  RVSAppSpinnerView.h
//  rvs_app2
//
//  Created by Barak Harel on 3/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppSpinnerView : UIView

@property (nonatomic) BOOL hidesWhenStopped;
@property (nonatomic) CFTimeInterval fullRotationDuration;
@property (nonatomic) CGFloat progress;
@property (nonatomic) CGFloat minProgressUnit;

@property (nonatomic) UIImage *backgroundImage;
@property (nonatomic) UIImage *indicatorImage;

- (id)initWithIndicatorImage:(UIImage *)indicatorImage backgroundImage:(UIImage *)backgroundImage;
- (void)startAnimating;
- (void)stopAnimating;
- (BOOL)isAnimating;

@end
