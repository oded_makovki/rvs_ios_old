//
//  RVSAppPostActivityProvider.h
//  rvs_app2
//
//  Created by Barak Harel on 4/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

@interface RVSAppPostActivityProvider : UIActivityItemProvider <UIActivityItemSource>

-(id)initWithPost:(NSObject <RVSPost> *)post;

@end