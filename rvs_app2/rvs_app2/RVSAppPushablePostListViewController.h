//
//  RVSAppPushablePostListViewController.h
//  rvs_app
//
//  Created by Barak Harel on 11/7/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"
#import "RVSAppPostListFactory.h"


@interface RVSAppPushablePostListViewController : RVSAppCommonDataViewController

@property (nonatomic) Class headerClass;

@property (nonatomic) RVSAppPostListFactory *postListFactory;
@property (nonatomic) NSString *userId;
@property (nonatomic) NSString *singlePostId;

- (void)reloadData;
@end
