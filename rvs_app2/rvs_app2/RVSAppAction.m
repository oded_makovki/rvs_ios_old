//
//  RVSAppAction.m
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppAction.h"

@interface RVSAppAction ()
@property (nonatomic, readwrite, strong) NSString *title;
@property (nonatomic, readwrite, copy) RVSAppActionBlock handler;
@property (nonatomic, readwrite, strong) UIImage *image;
@property (nonatomic, readwrite, strong) UIColor *backgroundColor;
@end

@implementation RVSAppAction


+(instancetype)actionWithTitle:(NSString *)title image:(UIImage *)image backgroundColor:(UIColor *)backgroundColor block:(RVSAppActionBlock)handler
{
    RVSAppAction *action = [[self alloc] init];
    action.title = title;
    action.image = image;
    action.backgroundColor = backgroundColor;
    action.handler = handler;
    return action;
}

@end
