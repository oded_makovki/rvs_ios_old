//
//  RVSAppPagesViewController.h
//  PageTest
//
//  Created by Barak Harel on 12/15/13.
//  Copyright (c) 2013 Barak Harel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppCommonViewController.h"
#import "RVSAppPageControl.h"

@class RVSAppPagesViewController;

@protocol RVSAppContainedPageViewController <NSObject>
@end

@protocol RVSAppPagesViewControllerDelegate <RVSAppCommonViewControllerDelegate>
@optional
-(void)pageViewController:(RVSAppPagesViewController *)pageViewController didScrollToIndex:(NSInteger)index;
@end

@protocol RVSAppPagesViewControllerDataSource <NSObject>
-(UIView *)pageViewController:(RVSAppPagesViewController *)pageViewController viewForIndex:(NSInteger)index;
-(NSString *)pageViewController:(RVSAppPagesViewController *)pageViewController titleForIndex:(NSInteger)index;
-(NSInteger)numberOfViewsFor:(RVSAppPagesViewController *)pageViewController;
@optional
-(UIColor *)pageViewController:(RVSAppPagesViewController *)pageViewController titleColorForIndex:(NSInteger)index;
-(UIFont *)pageViewController:(RVSAppPagesViewController *)pageViewController titleFontForIndex:(NSInteger)index;

@end


@interface RVSAppPagesViewController : RVSAppCommonViewController
@property (weak, nonatomic, readonly) RVSAppPageControl *tvPageControl;
@property (nonatomic, readonly) NSInteger numberOfViews;

@property (weak, nonatomic) id <RVSAppPagesViewControllerDelegate> delegate;
@property (weak, nonatomic) id <RVSAppPagesViewControllerDataSource> dataSource;

-(void)loadPages;
@end

