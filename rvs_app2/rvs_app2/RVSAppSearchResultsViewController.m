//
//  RVSAppSearchResultsViewController.m
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSearchResultsViewController.h"
#import <rvs_sdk_api.h>

#import "RVSAppConfiguration.h"
#import "UIColor+RVSApplication.h"
#import "NSArray+RVSApplication.h"

#import "RVSAppChannelDataObject.h"
#import "RVSAppChannelCell.h"

#import "RVSAppUserDataObject.h"
#import "RVSAppUserSearchResultCell.h"

#import "RVSAppPostDataHelper.h"
#import "RVSAppProgramExcerptDataObject.h"

#import "RVSAppPostListCell.h"
#import "RVSAppProgramExcerptListCell.h"

#import "RVSAppStringUtil.h"

#define RESULTS_LIST_SECTION 0

#define MAX_MATCHES_IN_RESULT 6

@interface RVSAppSearchResultsViewController () <RVSAppDataListViewDelegate, RVSAppDataListViewDataSource, RVSAppCommonViewControllerDelegate, RVSAppProgramExcerptViewDelegate>

@property (nonatomic) NSObject <RVSAsyncList> *resultList;
@property (nonatomic) BOOL resultsFound;

@property (nonatomic) NSMutableArray *resultsData;

@property (nonatomic) UIBarButtonItem *doneButton;
@end

@implementation RVSAppSearchResultsViewController

-(id)init
{
    self = [super init];
    if (self)
    {
        [self initRVSAppSearchResultsViewController];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppSearchResultsViewController];
}

-(void)initRVSAppSearchResultsViewController
{
    self.delegate = self;
}

-(BOOL)shouldAddDefaultRightNavigationItems
{
    return NO;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.searchText)
    {
        self.navigationItem.title = self.searchText;
    }
    else
    {
        self.navigationItem.title = @"";
    }
    
    if (self.owner)
    {
        self.doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(donePressed:)];
        self.navigationItem.rightBarButtonItem = self.doneButton;
    }
    
    self.view.backgroundColor = [UIColor rvsBackgroundColor];
    
    self.dataListView.refreshEnabled = NO;
    
    self.dataListView.dataSource = self;
    self.dataListView.delegate = self;
    self.dataListView.nextPageTriggerDistance = 3;
    self.dataListView.nextPageTriggerSection = RESULTS_LIST_SECTION;
    
    [self.dataListView setNumberOfSections:2];
    
    if (self.searchText)
    {
        [self searchChannels];
    }
}

#pragma mark - Search Channel

-(void)searchChannels
{
    self.resultList = [[RVSSdk sharedSdk] searchChannel:self.searchText];
    
    __weak typeof(self) weakSelf = self;
    
    RVSPromise *promise = [self.resultList next:1];
    [promise thenOnMain:^(NSArray *results) {
        
        NSArray *newData = [self dataFromResults:results];
        
        if ([newData count] > 0)
            weakSelf.resultsFound = YES;
        
        NSInteger startRow = [weakSelf.resultsData count];
        [weakSelf.resultsData addObjectsFromArray:newData];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:RESULTS_LIST_SECTION startRow:startRow count:[newData count]]];
        
        if ([newData count] > 0)
        {
            [weakSelf searchRecentPrograms];
        }
        else
        {
            [weakSelf searchUsers];
        }
        
    } error:^(NSError *error) {
        
        //ignore
        [weakSelf searchUsers];
    }];
}

#pragma mark - Search Channel

-(void)searchUsers
{
    self.resultList = [[RVSSdk sharedSdk] searchUser:self.searchText];
    
    __weak typeof(self) weakSelf = self;
    
    RVSPromise *promise = [self.resultList next:1];
    [promise thenOnMain:^(NSArray *results) {
        
        NSArray *newData = [self dataFromResults:results];
        
        if ([newData count] > 0)
            weakSelf.resultsFound = YES;
        
        NSInteger startRow = [weakSelf.resultsData count];
        [weakSelf.resultsData addObjectsFromArray:newData];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:RESULTS_LIST_SECTION startRow:startRow count:[newData count]]];
        
        
        [weakSelf searchRecentPrograms];
        
    } error:^(NSError *error) {
        
        //ignore
        [weakSelf searchRecentPrograms];
    }];
}

#pragma mark - Search Recent Programs

-(void)searchRecentPrograms
{
    NSInteger resultsCount = [[RVSAppConfiguration sharedConfiguration].searchSettings[@"recentProgramsCount"] integerValue];
    if (resultsCount == 0)
    {
        [self search];
        return;
    }
    
    double now = [[NSDate date] timeIntervalSince1970] * 1000;
    double lastProgramsMinStartOffset = [[RVSAppConfiguration sharedConfiguration].searchSettings[@"recentProgramsMinStartOffset"] doubleValue];
    
    NSNumber *minStartOffset = nil;
    if (lastProgramsMinStartOffset)
    {
        minStartOffset = @(now - lastProgramsMinStartOffset);
    }
    
    self.resultList = [[RVSSdk sharedSdk] searchContent:self.searchText type:RVSSearchTypeProgram minStartTime:minStartOffset];
    
    __weak typeof(self) weakSelf = self;
    
    RVSPromise *promise = [self.resultList next:resultsCount];
    [promise thenOnMain:^(NSArray *programExcerpts) {
        
        NSArray *sortedProgramExcerpts = [programExcerpts sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"programExcerpt.program.start" ascending:NO]]];
        NSArray *newData = [self dataFromResults:sortedProgramExcerpts];
        
        if ([newData count] > 0)
            weakSelf.resultsFound = YES;
        
        NSInteger startRow = [weakSelf.resultsData count];
        [weakSelf.resultsData addObjectsFromArray:newData];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:RESULTS_LIST_SECTION startRow:startRow count:[newData count]]];
        
        
        [weakSelf search];
        
    } error:^(NSError *error) {
        
        //ignore
        [weakSelf search];
    }];
}

#pragma mark - Search

- (void)search
{
    self.resultList = [[RVSSdk sharedSdk] searchContent:self.searchText type:nil minStartTime:nil];
    [self loadNextResults];
}

-(void)loadNextResults;
{
    NSInteger resultsCount = 5;
    __weak typeof(self) weakSelf = self;
    
    RVSPromise *promise = [self.resultList next:resultsCount];
    [promise thenOnMain:^(NSArray *results)
    {
        NSArray *newData = [self dataFromResults:results];
        
        if ([newData count] > 0)
            weakSelf.resultsFound = YES;
        
        NSInteger startRow = [weakSelf.resultsData count];
        [weakSelf.resultsData addObjectsFromArray:newData];
        [weakSelf.dataListView insertItemsAtIndexPaths:[RVSAppDataListView indexPathsForSection:RESULTS_LIST_SECTION startRow:startRow count:[newData count]]];
        
        
        if (weakSelf.resultList.didReachEnd)
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
            if (!weakSelf.resultsFound)
                [weakSelf showNoResults];
        }
        else
        {
            weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseShouldRequestNextData;
        }
        
    } error:^(NSError *error) {
        
        [weakSelf.dataListView reloadSection:RESULTS_LIST_SECTION];
        weakSelf.dataListView.loadPhase = RVSAppDataListLoadPhaseEndOfList;
        
        if (!weakSelf.resultsFound)
            [weakSelf showNoResults];
    }];
}

-(NSArray *)dataFromResults:(NSArray *)results
{
    NSMutableArray *data = [NSMutableArray arrayWithCapacity:[results count]];

    for (NSObject *result in results)
    {
        if ([result conformsToProtocol:@protocol(RVSContentSearchResult)])
        {
            NSObject <RVSContentSearchResult> *contentResult = (NSObject <RVSContentSearchResult> *)result;
            
            if (contentResult.post)
            {
                [data addObjectsFromArray:[RVSAppPostDataHelper dataObjectsForPost:contentResult.post delegate:self searchMatches:[self filteredSearchMatchesFrom:contentResult.searchMatches limit:MAX_MATCHES_IN_RESULT]]];
            }
            else if (contentResult.programExcerpt)
            {
                [data addObject:[RVSAppProgramExcerptDataObject dataObjectFromProgramExcerpt:(NSObject <RVSProgramExcerpt> *)contentResult.programExcerpt programExcerptViewDelegate:self searchMatches:[self filteredSearchMatchesFrom:contentResult.searchMatches limit:MAX_MATCHES_IN_RESULT]]];
            }
        }
        else if ([result conformsToProtocol:@protocol(RVSChannelSearchResult)])
        {
            NSObject <RVSChannelSearchResult> *channelResult = (NSObject <RVSChannelSearchResult> *)result;
            
            RVSAppChannelDataObject *channelData = [[RVSAppChannelDataObject alloc] init];
            channelData.channel = channelResult.channel;
            [data addObject:channelData];
        }
        else if ([result conformsToProtocol:@protocol(RVSUserSearchResult)])
        {
            NSObject <RVSUserSearchResult> *userResult = (NSObject <RVSUserSearchResult> *)result;
            
            RVSAppUserDataObject *userData = [RVSAppUserDataObject dataObjectsFromUser:userResult.user userViewDelegate:nil];
            [data addObject:userData];
        }
    }
    
    return data;
}

-(void)showNoResults
{
    self.noDataView.hidden = NO;
    self.dataListView.hidden = YES;
}

-(void)refreshDataListView:(RVSAppDataListView *)dataListView
{
    //no refresh?
}

-(void)nextDataForDataListView:(RVSAppDataListView *)dataListView
{
    __weak typeof(self) weakSelf = self;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [weakSelf loadNextResults];
    });
}

-(void)dataListView:(RVSAppDataListView *)dataListView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RESULTS_LIST_SECTION)
    {
        id data = self.resultsData[indexPath.row];
        if ([data isKindOfClass:[RVSAppUserDataObject class]])
        {
            [self pushUserDetailViewControlWithUserId:((RVSAppUserDataObject *)data).user.userId];
        }
    }
}

#pragma mark - DataListView DataSource

-(Class)dataListView:(RVSAppDataListView *)dataListView headerClassForItemAtSection:(NSInteger)section
{
    return nil;
}

-(RVSAppHeaderDataObject *)dataListView:(RVSAppDataListView *)dataListView headerDataForItemAtSection:(NSInteger)section
{
    return nil;
}

-(Class)dataListView:(RVSAppDataListView *)dataListView cellClassForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RESULTS_LIST_SECTION)
    {
        id data = self.resultsData[indexPath.row];
        if ([data isKindOfClass:[RVSAppProgramExcerptDataObject class]])
        {
            return [RVSAppProgramExcerptListCell class];
        }
        else if ([data isKindOfClass:[RVSAppChannelDataObject class]])
        {
            return [RVSAppChannelCell class];
        }
        else if ([data isKindOfClass:[RVSAppUserDataObject class]])
        {
            return [RVSAppUserSearchResultCell class];
        }
        else
        {
            return [RVSAppPostDataHelper classForDataObject:data];
        }
    }
    
    return nil;
}

-(NSObject *)dataListView:(RVSAppDataListView *)dataListView cellDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == RESULTS_LIST_SECTION)
    {
        return self.resultsData[indexPath.row];
    }
    
    return nil;
}

-(NSInteger)dataListView:(RVSAppDataListView *)dataListView numberOfItemsInSection:(NSInteger)section
{
    if (section == RESULTS_LIST_SECTION)
    {
        return [self.resultsData count];
    }
    
    return 0;
}

-(UIEdgeInsets)dataListView:(RVSAppDataListView *)dataListView insetForSectionAtIndex:(NSInteger)section
{
    if (section == RESULTS_LIST_SECTION)
        return UIEdgeInsetsMake(0, 0, 8, 0);
    
    else
    {
        return UIEdgeInsetsZero;
    }
}

-(void)programExcerptView:(RVSAppProgramExcerptView *)programExcerptView didSelectShareWithMediaContext:(NSObject<RVSMediaContext> *)mediaContext text:(NSString *)text
{
    [[RVSApplication application] showComposeWithMediaContext:mediaContext defaultText:text];
}

-(NSMutableArray *)resultsData
{
    if (!_resultsData)
        _resultsData = [NSMutableArray array];
    
    return _resultsData;
}
             
             

#pragma mark - Delete

-(void)applicationWillDeletePostId:(NSString *)postId
{
    NSIndexSet *deleteIndexes = [RVSAppPostDataHelper indexesInDataArray:self.resultsData containingPostId:postId];
    
    if ([deleteIndexes count] > 0)
    {
        NSArray *deleteIndexPaths = [RVSAppDataListView indexPathsForIndexes:deleteIndexes inSection:RESULTS_LIST_SECTION];
        
        //remove
        [self.resultsData removeObjectsAtIndexes:deleteIndexes];
        //delete
        [self.dataListView deleteItemsAtIndexPaths:deleteIndexPaths];
    }
}

-(void)applicationDidCreateComment:(NSObject<RVSComment> *)comment forPostId:(NSString *)postId
{
    [self refreshPostCommentsWithPostId:postId];
}

-(void)applicationDidDeleteCommentId:(NSString *)commentId fromPostId:(NSString *)postId
{
    [self refreshPostCommentsWithPostId:postId];
}

-(void)applicationDidUpdatePostId:(NSString *)postId
{
    [self refreshPostInfoWithPostId:postId];
}

-(void)refreshPostCommentsWithPostId:(NSString *)postId
{
    NSIndexSet *refreshIndexes = [RVSAppPostDataHelper indexesInDataArray:self.resultsData containingCommentsWithPostId:postId];
    
    if ([refreshIndexes count] > 0)
    {
        NSArray *refreshIndexPaths = [RVSAppDataListView indexPathsForIndexes:refreshIndexes inSection:RESULTS_LIST_SECTION];
        
        //refresh
        [self.dataListView reloadItemsAtIndexPaths:refreshIndexPaths];
    }
}

-(void)refreshPostInfoWithPostId:(NSString *)postId
{
    NSIndexSet *refreshIndexes = [RVSAppPostDataHelper indexesInDataArray:self.resultsData containingInfoWithPostId:postId];
    
    if ([refreshIndexes count] > 0)
    {
        NSArray *refreshIndexPaths = [RVSAppDataListView indexPathsForIndexes:refreshIndexes inSection:RESULTS_LIST_SECTION];
        
        //refresh
        [self.dataListView reloadItemsAtIndexPaths:refreshIndexPaths];
    }
}

#pragma mark - Filter Matches

/**
 *  if matches contains transcripts, filter all non-transcript matches.
 *  if not, sort matches by fieldName
 */
-(NSArray *)filteredSearchMatchesFrom:(NSArray *)searchMatches limit:(NSUInteger)limit
{
    if (!searchMatches || [searchMatches count] == 0)
        return searchMatches;
    
    NSString *fieldName;
    NSPredicate *predicate;
    NSArray *filteredSearchMatches;
    
    NSArray *knownSearchFieldNames = [RVSAppStringUtil sharedUtil].knownSearchFieldNames;
    
    for (int i = 0; i < [knownSearchFieldNames count]; i++)
    {
        fieldName = knownSearchFieldNames[i];
        predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"fieldName = '%@'", fieldName]];
        filteredSearchMatches = [searchMatches filteredArrayUsingPredicate:predicate];
        
        if ([filteredSearchMatches count] > 0)
            break;
    }
    
    if ([filteredSearchMatches count] > 0)
    {
        //cool, continue.
    }
    else //containing unknown field(s)
    {
        //fallback
        filteredSearchMatches = [searchMatches sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"fieldName" ascending:YES]]];
    }
    
    if ([filteredSearchMatches count] > limit)
        filteredSearchMatches = [filteredSearchMatches subarrayWithRange:NSMakeRange(0, limit)];
    
    return filteredSearchMatches;
}

-(void)donePressed:(UIBarButtonItem *)sender
{
    [self.navigationController popToViewController:self.owner animated:YES];
}

-(void)pushSearchResultsViewControllerWithSearchText:(NSString *)searchText withOwner:(UIViewController *)owner
{
    
    
    [super pushSearchResultsViewControllerWithSearchText:searchText withOwner:owner];
}

-(void)view:(UIView *)view didSelectLinkWithURL:(NSURL *)url
{
    if ([url.scheme isEqualToString:@"hash"])
    {
        NSString *searchText = [NSString stringWithFormat:@"#%@",[url host]];

        if ([searchText isEqualToString:self.searchText])
        {
            //same user! animate can't push.
            
            CAKeyframeAnimation *animation;
            animation = [CAKeyframeAnimation animationWithKeyPath:@"transform.translation.x"];
            animation.values = [NSArray arrayWithObjects:@0,@(5),@(0), nil];
            animation.keyTimes = [NSArray arrayWithObjects:@0,@0.5,@1.0, nil];
            
            animation.autoreverses = NO;
            animation.repeatCount = 1; // Play it just once, and then reverse it
            animation.duration = 0.3;
            [view.layer addAnimation:animation forKey:nil];
            
            return;
        }
    }
    
    [super view:view didSelectLinkWithURL:url];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
