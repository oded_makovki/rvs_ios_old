//
//  RVSAppProgramExcerptListCell.h
//  rvs_app2
//
//  Created by Barak Harel on 4/7/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDataCell.h"
#import "RVSAppProgramExcerptView.h"
#import "RVSAppProgramExcerptDataObject.h"

@interface RVSAppProgramExcerptListCell : RVSAppDataCell
@property (weak, nonatomic) IBOutlet RVSAppProgramExcerptView *programExcerptView; //needs to be init or set
@property (nonatomic) RVSAppProgramExcerptDataObject *data;
@end
