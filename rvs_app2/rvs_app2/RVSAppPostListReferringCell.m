//
//  RVSAppPostListReferringCell.m
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostListReferringCell.h"
#import "UIView+NibLoading.h"
#import "RVSAppDateUtil.h"
#import "UIFont+RVSApplication.h"

@implementation RVSAppPostListReferringCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostListReferringCell];
    }
    
    return self;
}

-(void)initRVSAppPostListReferringCell
{
    [self loadContentsFromNib];
    
    self.userView.backgroundColor = [UIColor rvsPostReferringBackgroundColor];
    self.userView.userNameLabel.textColor = [UIColor rvsPostReferringTextColor];
    self.userView.userNameLabel.font = [UIFont rvsRegularFontWithSize:12];
    
    if  (self.timeLabel)
    {
        self.timeLabel.textColor = [UIColor rvsPostReferringTextColor];
        self.timeLabel.font = [UIFont rvsRegularFontWithSize:11];
    }
    
    [self prepareForReuse];
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    [self.userView clear];
    self.timeLabel.text = @"";
}

-(void)updateTimeLabel
{
    if (self.timeLabel)
    {
        if (!self.data.repostingUser.timestamp)
        {
            self.timeLabel.text = @"";
        }
        else
        {
            self.timeLabel.text = [RVSAppDateUtil timeShortStringSinceTime:[self.data.repostingUser.timestamp doubleValue] / 1000];
        }
    }
}

-(void)setData:(RVSAppPostReferringDataObject *)data
{
    [super setData:data];
    [self.userView setUser:data.repostingUser.user isSizingView:NO];
    self.userView.userNameLabel.text = [NSString stringWithFormat:NSLocalizedString(@"post referring format", nil), data.repostingUser.user.name];
    [self updateTimeLabel];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 24);
}

@end
