//
//  RVSAppComposeAccessoryView.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//
#import "RVSAppComposeAccessoryView.h"
#import "UIColor+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppComposeAccessoryView()
@property (weak, nonatomic) UITextView *textView;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;
@end


@implementation RVSAppComposeAccessoryView

-(id)initWithFrame:(CGRect)frame textView:(UITextView *)textView
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.textView = textView;
        [self.textView setInputAccessoryView:self];
        self.tintColor = [UIColor rvsTintColor];
        self.backgroundColor = [UIColor rvsBackgroundColor];
        
        self.charsCountLabel.font = [UIFont rvsRegularFontWithSize:self.charsCountLabel.font.pointSize];
    }
    return self;
}

- (IBAction)done:(UIBarButtonItem *)sender
{
    [self.textView resignFirstResponder];
}

- (IBAction)addCC:(UIBarButtonItem *)sender
{
    [self addString:@"\""];
}


- (IBAction)addUser:(UIBarButtonItem *)sender
{
    [self addString:@"@"];
}


- (IBAction)addHash:(UIBarButtonItem *)sender
{
    [self addString:@"#"];
}

-(void)addString:(NSString *)string
{
    
    if (self.textView.text.length == 0)
    {
        self.textView.text = string;
    }
    else if ([self.textView.text hasSuffix:@" "])
    {
        self.textView.text = [NSString stringWithFormat:@"%@%@", self.textView.text, string];
    }
    else
    {
        self.textView.text = [NSString stringWithFormat:@"%@ %@", self.textView.text, string];
    }
    
    //notify
    if ([self.textView.delegate respondsToSelector:@selector(textViewDidChange:)])
    {
        [self.textView.delegate textViewDidChange:self.textView];
    }
}

@end
