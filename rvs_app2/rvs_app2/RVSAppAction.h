//
//  RVSAppAction.h
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^RVSAppActionBlock)();

@interface RVSAppAction : NSObject

+ (instancetype)actionWithTitle:(NSString *)title image:(UIImage *)image backgroundColor:(UIColor *)backgroundColor block:(RVSAppActionBlock)handler;

/// The title of the action.
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) UIImage *image;
@property (nonatomic, readonly) UIColor *backgroundColor;
@property (nonatomic, readonly, copy) RVSAppActionBlock handler;

@end
