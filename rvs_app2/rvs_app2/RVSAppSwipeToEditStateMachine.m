//
//  RVSAppSwipeToEditStateMachine.m
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSwipeToEditStateMachine.h"
#import "RVSAppActionsDataCell_Private.h"

NSString * const AAPLSwipeStateNothing = @"NothingState";
NSString * const AAPLSwipeStateEditing = @"EditingState";
NSString * const AAPLSwipeStateTracking = @"TrackingState";
NSString * const AAPLSwipeStateAnimatingOpen = @"AnimatingOpenState";
NSString * const AAPLSwipeStateAnimatingShut = @"AnimatingShutState";

@interface RVSAppSwipeToEditStateMachine ()

@property (nonatomic, strong) RVSAppDataListView *collectionView;

@property (nonatomic, strong) UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressGestureRecognizer;
@property (nonatomic, strong) RVSAppActionsDataCell *editingCell;
@property (nonatomic) CGFloat startTrackingX;

@end

@implementation RVSAppSwipeToEditStateMachine

- (instancetype)initWithDataListView:(RVSAppDataListView *)collectionView
{
    self = [super init];
    if (!self)
        return nil;

    self.collectionView = collectionView;

    self.currentState = AAPLSwipeStateNothing;
    self.validTransitions = @{
                              AAPLSwipeStateNothing : @[AAPLSwipeStateTracking, AAPLSwipeStateAnimatingOpen],
                              AAPLSwipeStateTracking : @[AAPLSwipeStateAnimatingOpen, AAPLSwipeStateAnimatingShut, AAPLSwipeStateTracking, AAPLSwipeStateNothing],
                              AAPLSwipeStateAnimatingOpen : @[AAPLSwipeStateEditing, AAPLSwipeStateAnimatingShut, AAPLSwipeStateTracking, AAPLSwipeStateNothing],
                              AAPLSwipeStateAnimatingShut : @[AAPLSwipeStateNothing],
                              AAPLSwipeStateEditing : @[AAPLSwipeStateTracking, AAPLSwipeStateAnimatingShut, AAPLSwipeStateNothing]
                              };

    _panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    _panGestureRecognizer.delegate = self;

    _longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    _longPressGestureRecognizer.minimumPressDuration = 0.05;
    _longPressGestureRecognizer.delegate = self;

    NSArray *gestureRecognizers = _collectionView.gestureRecognizers;
    for (UIGestureRecognizer *recognizer in gestureRecognizers) {
        if ([recognizer isKindOfClass:[UIPanGestureRecognizer class]])
            [recognizer requireGestureRecognizerToFail:_panGestureRecognizer];
        if ([recognizer isKindOfClass:[UILongPressGestureRecognizer class]])
            [recognizer requireGestureRecognizerToFail:_longPressGestureRecognizer];
    }

    [collectionView addGestureRecognizer:_panGestureRecognizer];
    [collectionView addGestureRecognizer:_longPressGestureRecognizer];

    return self;
}

- (void)setDelegate:(id<AAPLStateMachineDelegate>)delegate
{
    NSAssert(NO, @"you're not the boss of me");
}

- (void)setEditingCell:(RVSAppActionsDataCell *)editingCell
{
    if (_editingCell == editingCell)
        return;
    _editingCell = editingCell;
}

- (CGFloat)xPositionForTranslation:(CGPoint)translation
{
    return MIN(0, _startTrackingX + translation.x);
}

- (void)shutActionPaneForEditingCellAnimated:(BOOL)animate
{
    // our cancel gesture recognizer comes through this code path, and should normally cause the edit pane to slide shut except for when some control on screen, like a segmented control, triggers the pane to slide shut without animation. in that case, we need to preempt any animation with a debounce.
    if (animate) {
        // the segmented datasource might have changed in the same event loop, so delay this one loop cycle later
        dispatch_async(dispatch_get_main_queue(), ^{
            // somebody might have already shut us by the time we're called, so only shut if we're not already
            if (![self.currentState isEqualToString:AAPLSwipeStateNothing]) {
                self.currentState = AAPLSwipeStateAnimatingShut;
                [_editingCell closeActionPaneAnimated:YES completionHandler:^(BOOL finished) {
                    if (finished) {
                        self.currentState = AAPLSwipeStateNothing;
                    }
                }];
            }
        });
    }
    else {
        [_editingCell closeActionPaneAnimated:NO completionHandler:nil];
        self.currentState = AAPLSwipeStateNothing;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    if (![self.currentState isEqualToString:AAPLSwipeStateNothing]) {
        [self shutActionPaneForEditingCellAnimated:NO];
    }
}

#pragma mark - Gesture Recognizer action methods

- (void)handlePan:(UIPanGestureRecognizer *)recognizer
{
    [self handleNormalPan:recognizer];
}


- (void)handleNormalPan:(UIPanGestureRecognizer *)recognizer
{
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
        {
            break;
        }
        case UIGestureRecognizerStateChanged:
        {
            CGPoint translation = [recognizer translationInView:_editingCell];
            CGFloat xPosition = [self xPositionForTranslation:translation];
            _editingCell.swipeTrackingPosition = xPosition;
            self.currentState = AAPLSwipeStateTracking;

            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            CGFloat velocityX = [recognizer velocityInView:_editingCell].x;

            CGFloat xPosition = _editingCell.swipeTrackingPosition;
            CGFloat targetX = _editingCell.minimumSwipeTrackingPosition;

            double threshhold = 100.0;
            if (velocityX < 0 && (-velocityX > threshhold || xPosition <= targetX))
            {
                CGFloat velocityX = (0.2*[recognizer velocityInView:_editingCell].x);
                CGFloat animationDuration = (ABS(velocityX)*.0002)+.2;

                self.currentState = AAPLSwipeStateAnimatingOpen;
                [UIView animateWithDuration:animationDuration delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                    _editingCell.swipeTrackingPosition = targetX;
                } completion:^(BOOL finished) {
                    // Only set it to editing if we're still in the same state as we previously set. It can change if that devilish user keeps fiddling with things.
                    if ([AAPLSwipeStateAnimatingOpen isEqualToString:self.currentState])
                        self.currentState = AAPLSwipeStateEditing;
                }];
            }
            else {
                [self shutActionPaneForEditingCellAnimated:YES];
            }
            break;
        }
        case UIGestureRecognizerStateCancelled:
        {
            [self shutActionPaneForEditingCellAnimated:YES];
            break;
        }
        default:
            break;
    }
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)recognizer
{
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            break;

        case UIGestureRecognizerStateCancelled:
            
            self.currentState = AAPLSwipeStateNothing;
            break;
            
        case UIGestureRecognizerStateEnded:
            if ([self.currentState isEqualToString:AAPLSwipeStateEditing] || [self.currentState isEqualToString:AAPLSwipeStateAnimatingOpen])
                [self shutActionPaneForEditingCellAnimated:YES];
            else if ([self.currentState isEqualToString:AAPLSwipeStateNothing]) {
                // Tap in the remove control
                self.currentState = AAPLSwipeStateAnimatingOpen;
                [_editingCell openActionPaneAnimated:YES completionHandler:^(BOOL finished){
                    // Only set it to editing if we're still in the same state as we previously set. It can change if that devilish user keeps fiddling with things.
                    if ([AAPLSwipeStateAnimatingOpen isEqualToString:self.currentState])
                        self.currentState = AAPLSwipeStateEditing;
                }];
            }
            break;

        default:
            break;
    }
}

#pragma mark - State Transition methods

- (void)didEnterEditingState
{
    _editingCell.userInteractionEnabled = YES;
    _editingCell.userInteractionEnabledForEditing = YES;
}

- (void)didExitEditingState
{
    _editingCell.userInteractionEnabledForEditing = NO;
}

- (void)didExitNothingState
{
    _collectionView.scrollEnabled = NO;
    [_editingCell showEditActions];
}

- (void)didEnterNothingState
{
    _collectionView.scrollEnabled = YES;
    _startTrackingX = 0;
    _editingCell.userInteractionEnabled = YES;
    [_editingCell hideEditActions];
    self.editingCell = nil;
}

- (void)didEnterAnimatingShutState
{
    _editingCell.userInteractionEnabled = NO;
    [_editingCell animateOutSwipeToEditAccessories];
}

- (void)didEnterAnimatingOpenState
{
    _editingCell.userInteractionEnabled = NO;
}

- (void)didExitAnimatingOpenState
{
}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == _longPressGestureRecognizer) {

        // cancel taps only work once we're in full edit mode or animating open
        if (![self.currentState isEqualToString:AAPLSwipeStateEditing] && ![self.currentState isEqualToString:AAPLSwipeStateAnimatingOpen])
            return NO;

        // don't allow the cancel gesture to recognise if any of the touches are within the edit actions
        NSUInteger numberOfTouches = gestureRecognizer.numberOfTouches;
        UIView *editActionsView = _editingCell.actionsView;
        CGRect disabledRect = editActionsView.bounds;

        for (NSUInteger touchIndex = 0; touchIndex < numberOfTouches; ++touchIndex) {
            CGPoint touchLocation = [gestureRecognizer locationOfTouch:touchIndex inView:editActionsView];
            if (CGRectContainsPoint(disabledRect, touchLocation))
                return NO;
        }
        
        return YES;
    }

    if (gestureRecognizer == _panGestureRecognizer) {
        
        if ([self.currentState isEqualToString:AAPLSwipeStateNothing] || [self.currentState isEqualToString:AAPLSwipeStateEditing] || [self.currentState isEqualToString:AAPLSwipeStateAnimatingOpen]) {
            // only if it's a AAPLCollectionViewCell
            NSIndexPath *panCellPath = [_collectionView indexPathForItemAtPoint:[_panGestureRecognizer locationInView:_collectionView]];
            CGPoint velocity = [_panGestureRecognizer velocityInView:_collectionView];
            RVSAppActionsDataCell *cell = (RVSAppActionsDataCell *)[_collectionView cellForItemAtIndexPath:panCellPath];

            if (![cell isKindOfClass:[RVSAppActionsDataCell class]])
                return NO;

            if (![self.currentState isEqualToString:AAPLSwipeStateNothing] && cell != _editingCell)
                return NO;

            if ([cell.editActions count] == 0)
                return NO;

            // only if there's enough x velocity
            if (abs(velocity.y) >= abs(velocity.x))
                return NO;

            _startTrackingX = cell.swipeTrackingPosition;
            self.editingCell = cell;
            self.currentState = AAPLSwipeStateTracking;
            return YES;
        }
        else
            return NO;
    }

    // It's some other gesture recogniser?
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([_longPressGestureRecognizer isEqual:gestureRecognizer])
        return [_panGestureRecognizer isEqual:otherGestureRecognizer];

    if ([_panGestureRecognizer isEqual:gestureRecognizer])
        return [_longPressGestureRecognizer isEqual:otherGestureRecognizer];

    return NO;
}

@end