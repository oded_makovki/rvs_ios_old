//
//  main.m
//  rvs_app2
//
//  Created by Barak Harel on 3/3/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "RVSApplication.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([RVSApplication class]));
    }
}
