//
//  RVSAppUserListCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/10/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserListCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@implementation RVSAppUserListCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppUserListCell];
    }
    return self;
}

-(void)initRVSAppUserListCell
{
    [self loadContentsFromNib];
    self.userView.userNameLabel.textColor = [UIColor rvsPostUserNameColor];
}

+(CGSize)preferredCellSize
{
    return CGSizeMake(320, 60);
}

@end
