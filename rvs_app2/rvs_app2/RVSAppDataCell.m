//
//  RVSAppDataCell.m
//  rvs_app
//
//  Created by Barak Harel on 12/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataCell.h"

@implementation RVSAppDataCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor rvsBackgroundColor];
        self.opaque = YES;
        self.clipsToBounds = YES;
        
        self.layer.shouldRasterize = YES;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
    }
    return self;
}

-(id)initSizingCellWithFrame:(CGRect)frame
{
    self = [self initWithFrame:frame];
    if (self)
    {
        _isSizingCell = YES;
        self.layer.shouldRasterize = NO;
    }
    return self;
}

-(void)prepareForReuse
{
    //override this
}

-(CGSize)sizeForData:(NSObject *)data
{
    //fallback - very slow!
    [self setData:data];
    CGSize cellSize = [self systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    
    return CGSizeMake(self.frame.size.width, cellSize.height);
}

+(CGSize)preferredCellSize
{
    //override this, if zero: will be ignored.
    return CGSizeZero;
}

@end
