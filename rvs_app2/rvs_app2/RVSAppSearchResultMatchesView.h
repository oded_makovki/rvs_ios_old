//
//  RVSAppSearchResultMatchesView.h
//  rvs_app2
//
//  Created by Barak Harel on 5/12/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@class RVSAppSearchResultMatchesView;
@protocol RVSAppSearchResultMatchesViewDelegate <NSObject>
-(void)searchResultMatchesView:(RVSAppSearchResultMatchesView *)searchResultMatchesView currentSearchMatchDidChangeWithIndex:(NSInteger)currentSearchMatchIndex;
@end

@interface RVSAppSearchResultMatchesView : UIView

@property (weak, nonatomic) IBOutlet UICollectionView *matchesCollectionView;
@property (weak, nonatomic) IBOutlet UIPageControl *matchesPageControl;
@property (nonatomic, readonly) NSInteger currentSearchMatchIndex;
@property (weak, nonatomic) id <RVSAppSearchResultMatchesViewDelegate> delegate;

-(void)setSearchMatchesData:(NSArray *)searchMatchesData;
-(void)clear;

@end
