//
//  RVSAppLoginViewController.h
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>

@interface RVSAppSignInViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (nonatomic) RVSPromise *signInViewControllerSuccessPromise;
-(void)wasDismissed;

-(void)setLastSignInType;;

@end
