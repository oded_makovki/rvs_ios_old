//
//  RVSAppSearchChannelDetectorActivityIndicator.h
//  rvs_app2
//
//  Created by Barak Harel on 4/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RVSAppSearchChannelDetectorActivityIndicator : UIView
@property (nonatomic, readonly) BOOL isAnimating;
@property (nonatomic) BOOL isDetected;
@property (nonatomic) UIColor *barsColor;
-(void)startAnimation;
-(void)stopAnimation;
-(void)setTempBarsColor:(UIColor *)tempColor;
@end
