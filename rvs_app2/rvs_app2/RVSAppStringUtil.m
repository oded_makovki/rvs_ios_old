//
//  RVSAppStringUtil.m
//  rvs_app2
//
//  Created by Barak Harel on 5/12/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppStringUtil.h"

@interface RVSAppStringUtil()
@property (nonatomic) NSArray *knownSearchFieldNames;
@end

@implementation RVSAppStringUtil

+ (RVSAppStringUtil*)sharedUtil
{
    static RVSAppStringUtil* _sharedUtil;
    static dispatch_once_t sharedUtilOnce;
    
    dispatch_once(&sharedUtilOnce, ^{
        _sharedUtil = [[RVSAppStringUtil alloc] init];
    });
    
    return  _sharedUtil;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        [self loadKnownSearchFieldNames];
    }
    
    return self;
}

-(void)loadKnownSearchFieldNames
{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"knownSearchFieldNames" ofType:@"plist"];
    self.knownSearchFieldNames = [NSArray arrayWithContentsOfFile:filePath];
}

-(NSString *)stringForSearchFieldName:(NSString *)fieldName
{
    NSString *key;
    if ([self.knownSearchFieldNames containsObject:fieldName])
    {
        key = [NSString stringWithFormat:@"search field name %@", fieldName];
    }
    else
    {
        key = @"search field name other";
    }
    
    return NSLocalizedString(key, nil);
}

+(void)truncateTextInLabel:(UILabel *)label toDisplayRange:(NSRange)range
{
    NSAttributedString *text = label.attributedText;
    CGFloat maxWidth = label.frame.size.width * label.numberOfLines;
    
    
    NSRange leftRange;
    if (range.location + range.length + 1 <= text.length) //right edge + 1
    {
        leftRange = NSMakeRange(0, range.location+range.length + 1); //+1 = @"..."
    }
    else if (range.location + range.length <= text.length) //right edge
    {
        leftRange = NSMakeRange(0, range.location+range.length);
    }
    else //error? - right edge of text
    {
        leftRange = NSMakeRange(0, text.length);
    }
    
    NSAttributedString *leftText = [text attributedSubstringFromRange:leftRange];
    
    CGFloat leftWidth = [leftText size].width;
    
    if (leftWidth <= maxWidth)
    {
        label.lineBreakMode = NSLineBreakByTruncatingTail;
        return; //cool
    }
    
    NSRange rightRange;
    if (range.location >= 1 && range.location < text.length) //left egde - 1
    {
        rightRange = NSMakeRange(range.location - 1, text.length - range.location + 1); //+3 = @"..."
    }
    else if (range.location < text.length) //left edge
    {
        rightRange = NSMakeRange(range.location, text.length - range.location);
    }
    else //error?
    {
        rightRange = NSMakeRange(0, text.length);
    }
    
    NSAttributedString *rightText = [text attributedSubstringFromRange:rightRange];
    CGFloat rightWidth = [rightText size].width;
    
    if (rightWidth <= maxWidth)
    {
        label.lineBreakMode = NSLineBreakByTruncatingHead;
        return; //cool
    }

    label.attributedText = leftText;
    label.lineBreakMode = NSLineBreakByTruncatingHead;
}

+(NSString *)emailShareTextFromText:(NSString *)text andLink:(NSString *)link
{
    NSString *format = NSLocalizedString(@"share email format", nil);
    return [NSString stringWithFormat:format, text, link];
}

+(NSString *)facebookShareTextFromText:(NSString *)text
{
    NSString *signature = NSLocalizedString(@"share facebook signature", nil);
    return [NSString stringWithFormat:@"%@%@", text, signature];
}

+(NSString *)twitterShareTextFromText:(NSString *)text
{
    NSString *signature = NSLocalizedString(@"share twitter signature", nil);
    NSInteger maxLength = 117 - [signature length];  //117 (max twitter text length with url) - length of signature
    
    if (text.length > maxLength)
    {
        text = [NSString stringWithFormat:@"%@...",[text substringToIndex:(maxLength - 3)]]; //max - 3 (length of "...")
    }
    
    return [NSString stringWithFormat:@"%@%@", text, signature];
}


+(NSString *)prettyNumber:(NSNumber *)number
{
    if (!number)
        return @"0"; //NSLocalizedString(@"counter placeholder", nil);
    
    if ([number integerValue] < 1000)
        return [NSString stringWithFormat:@"%ld", [number longValue]];
    
    float num = [number integerValue] / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fK", num];
    
    num = num / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fM", num];
    
    num = num / 1000;
    if (num <= 1000)
        return [NSString stringWithFormat:@"%.1fG", num];
    
    num = num / 1000;
    //    if (num <= 1000)
    return [NSString stringWithFormat:@"%.1fT", num];
}

@end
