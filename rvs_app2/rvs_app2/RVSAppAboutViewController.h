//
//  RVSAppAboutViewController.h
//  rvs_app
//
//  Created by Barak Harel on 1/8/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCommonViewController.h"

@interface RVSAppAboutViewController : RVSAppCommonViewController
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end
