//
//  RVSAppCreateCommentFactory.h
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

@interface RVSAppCreateCommentFactory : NSObject
@property (nonatomic, readonly) NSString *message;
@property (nonatomic, readonly) NSString *postId;

-initWithMessage:(NSString *)message postId:(NSString *)postId;

-(RVSPromise*)createComment;

@end
