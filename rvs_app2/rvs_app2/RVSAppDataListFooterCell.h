//
//  RVSAppLoadingPostCell.h
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppSpinnerView.h"

typedef NS_ENUM(NSInteger, RVSAppDataListFooterType)
{
    RVSAppDataListFooterTypeDefault,
    RVSAppDataListFooterTypeLoading,
    RVSAppDataListFooterTypeEndOfList
};

@interface RVSAppDataListFooterCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet RVSAppSpinnerView *spinner;
@property (nonatomic) RVSAppDataListFooterType type;

-(CGSize)size;
+(CGSize)defaultSize;

@end
