//
//  RVSAppDateUtil.m
//  rvs_app2
//
//  Created by Barak Harel on 3/4/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppDateUtil.h"
#define MAX_SECONDS_CONSIDERED_AS_NOW 30

@implementation RVSAppDateUtil

+(NSString *)timeStringSinceTime:(NSTimeInterval)time isShortString:(BOOL)isShortString
{
    NSTimeInterval seconds = time - [[NSDate date] timeIntervalSince1970];
    BOOL isPastTime = NO;
    if (seconds < 0)
    {
        isPastTime = YES;
        seconds = fabs(seconds);
    }
    
    NSInteger weeks = seconds / (3600 * 24 * 7);
    NSInteger days = seconds / (3600 * 24);
    NSInteger hours = seconds / (3600);
    NSInteger minutes = seconds / 60;
    
    NSString *component;
    NSString *format;
    NSInteger value = 0;
    
    if (seconds <= MAX_SECONDS_CONSIDERED_AS_NOW)
    {
        return [self stringForNowUsingShortString:isShortString];
    }
    else if (weeks >= 1)
    {
        if (weeks == 1)
        {
            component = @"week";
        }
        else
        {
            component = @"weeks";
        }
        
        value = weeks;
    }
    else if (days >= 1)
    {
        if (days == 1)
        {
            component = @"day";
        }
        else
        {
            component = @"days";
        }
        
        value = days;
    }
    else if (hours >= 1)
    {
        if (hours == 1)
        {
            component = @"hour";
        }
        else
        {
            component = @"hours";
        }
        
        value = hours;
    }
    else if (minutes >= 1)
    {
        if (minutes == 1)
        {
            component = @"minute";
        }
        else
        {
            component = @"minutes";
        }
        
        value = minutes;
    }
    else if (seconds > 0)
    {
        if (seconds == 1)
        {
            component = @"second";
        }
        else
        {
            component = @"seconds";
        }
        
        value = seconds;
    }
    
    format = [self formatForComponent:component isPastTime:isPastTime isShortString:isShortString];
    return [NSString stringWithFormat:format, value];
}

+(NSString *)stringForNowUsingShortString:(BOOL)isShortString
{
    NSString *l = (isShortString ? @"short" : @"long");
    NSString *string = [NSString stringWithFormat:@"time format now %@", l];
    
    return NSLocalizedString(string ,nil);
}


+(NSString *)formatForComponent:(NSString *)component isPastTime:(BOOL)isPastTime isShortString:(BOOL)isShortString
{
    //    "days long time past format"
    NSString *t = (isPastTime ? @"past" : @"future");
    NSString *l = (isShortString ? @"short" : @"long");
    
    NSString *format = [NSString stringWithFormat:@"time format %@ %@ %@", component, t, l];
    return NSLocalizedString(format, nil);
}

+(NSString *)timeShortStringSinceTime:(NSTimeInterval)time
{
    return [self timeStringSinceTime:time isShortString:YES];
}

+(NSString *)timeLongStringSinceTime:(NSTimeInterval)time
{
    return [self timeStringSinceTime:time isShortString:NO];
}


@end
