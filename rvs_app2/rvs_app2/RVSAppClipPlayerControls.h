//
//  RVSAppClipPlayerControls.h
//  rvs_app2
//
//  Created by Barak Harel on 5/18/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppSpinnerView.h"
#import <rvs_sdk_api.h>

@class RVSAppClipPlayerControls;
@protocol RVSAppClipPlayerViewUIDelegate <NSObject>

@optional
-(void)togglePlayback;
-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberValueChanged:(UISlider *)sender;
-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberTouchDown:(UISlider *)sender;
-(void)playerControls:(RVSAppClipPlayerControls *)playerControls scrubberTouchUp:(UISlider *)sender;
@end


@protocol RVSAppClipPlayerViewUIDataSource <NSObject>

-(BOOL)isBufferingStateForPlayerControls:(RVSAppClipPlayerControls *)playerControls;

-(NSObject <RVSChannel> *)channelForPlayerControls:(RVSAppClipPlayerControls *)playerControls;
-(NSObject <RVSProgram> *)programForPlayerControls:(RVSAppClipPlayerControls *)playerControls;

-(RVSClipViewState)playerStateForPlayerControls:(RVSAppClipPlayerControls *)playerControls;
-(NSTimeInterval)mediaDurationForPlayerControls:(RVSAppClipPlayerControls *)playerControls;
-(NSTimeInterval)mediaCurrentPositionForPlayerControls:(RVSAppClipPlayerControls *)playerControls;

@end

@interface RVSAppClipPlayerControls : UIView

@property (nonatomic, weak) IBOutlet UIImageView *channelImageView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UISlider *scrubber;
@property (nonatomic) RVSAppSpinnerView *spinner;

@property (nonatomic, weak) IBOutlet UIView *endCardView; //TBD RVSAppClipPlayerEndCardView
@property (nonatomic, weak) IBOutlet UILabel *endCardLabel;
@property (weak, nonatomic) IBOutlet UIButton *endCardReplayButton;

@property (nonatomic, weak) id <RVSAppClipPlayerViewUIDelegate> delegate;
@property (nonatomic, weak) id <RVSAppClipPlayerViewUIDataSource> dataSource;

@property (nonatomic, readonly) UITapGestureRecognizer *togglePlayGestureRecognizer;

@property (nonatomic, readonly) BOOL isScrubbing;
@property (nonatomic, readonly) BOOL isReloadingData;

/**
 *  Called when scrubbing
 */
-(void)scrubberValueDidChange;

-(void)hideEndCardAnimated:(BOOL)animated;
-(void)showEndCardAnimated:(BOOL)animated;

/**
 *  Invalidate parts of the controls, will force update from data source
 */
-(void)reloadData;
-(void)invalidateChannelAndProgram;

-(void)invalidateScrubber;
-(void)invalidatePlayerState;

-(void)invalidateIsBufferingState;

@end
