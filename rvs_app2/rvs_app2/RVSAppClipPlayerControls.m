//
//  RVSAppClipPlayerControls.m
//  rvs_app2
//
//  Created by Barak Harel on 5/18/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppClipPlayerControls.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"
#import "NSAttributedString+RVSApplication.h"
#import "UIFont+RVSApplication.h"

@interface RVSAppClipPlayerControls()
@property (nonatomic) RVSPromiseHolder *channelImagePromise;
@property (nonatomic) BOOL isReloadingData;
@end

@implementation RVSAppClipPlayerControls

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initRVSAppClipPlayerControls];
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppClipPlayerControls];
}

-(void)initRVSAppClipPlayerControls
{
    [self loadContentsFromNib];
    
    self.scrubber.minimumValue = 0.0f;
    self.scrubber.maximumValue = 1.0f;
    self.scrubber.value = 0.0f;
    
    self.titleLabel.text = @"";
    self.titleLabel.font = [UIFont rvsBoldFontWithSize:14];
    self.channelImageView.image = nil;
    
    self.endCardLabel.font = [UIFont rvsBoldFontWithSize:16];
    
    [self.scrubber addTarget:self action:@selector(scrubberValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.scrubber addTarget:self action:@selector(scrubberTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self.scrubber addTarget:self action:@selector(scrubberTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    
    _togglePlayGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(togglePlayback)];
    [self addGestureRecognizer:_togglePlayGestureRecognizer];
    [self addSpinner];
}

-(void)addSpinner
{
    //spinner
    self.spinner = [[RVSAppSpinnerView alloc] initWithIndicatorImage:[UIImage imageNamed:@"spinner_front"] backgroundImage:nil]; //[UIImage imageNamed:@"spinner_background"]
    self.spinner.hidden = YES;
    [self addSubview:self.spinner];
    self.spinner.translatesAutoresizingMaskIntoConstraints = NO;
    [self.spinner.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.spinner.superview attribute:NSLayoutAttributeCenterX multiplier:1 constant:0]];
    [self.spinner.superview addConstraint:[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.spinner.superview attribute:NSLayoutAttributeCenterY multiplier:1 constant:0]];
    [self.spinner addConstraint:[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.spinner.frame.size.height]];
    [self.spinner addConstraint:[NSLayoutConstraint constraintWithItem:self.spinner attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.spinner.frame.size.width]];
}

#pragma mark - Playback

-(void)togglePlayback
{
    [self.delegate togglePlayback];
}

- (IBAction)replay:(id)sender
{
    [self.delegate togglePlayback];
}

#pragma mark - Scrubbing

-(void)scrubberValueChanged:(UISlider *)sender
{
    [self.delegate playerControls:self scrubberValueChanged:sender];
    [self scrubberValueDidChange];
}

-(void)scrubberValueDidChange
{
    //override this
}

-(void)scrubberTouchDown:(UISlider *)sender
{
    _isScrubbing = YES;
    [self.delegate playerControls:self scrubberTouchDown:sender];
}

-(void)scrubberTouchUp:(UISlider *)sender
{
    _isScrubbing = NO;
    [self.delegate playerControls:self scrubberTouchUp:sender];
}

-(void)setScrubberProgressWithCurrent:(NSTimeInterval)current duration:(NSTimeInterval)duration animated:(BOOL)animated
{
    double value = 0;
    if (duration > 0)
        value = current / duration;
    
    if (animated)
    {
        double animationDuration = 0;
        if (value > 0)
            animationDuration = 0.5f;
        
        [UIView animateWithDuration:animationDuration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
            [self.scrubber setValue:value animated:YES];
        } completion:nil];
    }
    else
    {
        [self.scrubber setValue:value];
    }
}

#pragma mark - Channel Image

-(void)setChannelImage:(NSObject<RVSAsyncImage> *)channelImage
{
    //clear old to cancel
    if (channelImage)
    {
        __weak typeof(self) weakSelf = self;
        
        self.channelImagePromise = [channelImage imageWithSize:self.channelImageView.frame.size].newHolder;
        [self.channelImagePromise thenOnMain:^(UIImage *image){
            weakSelf.channelImageView.image = image;
        } error:nil]; //will keep def cover
    }
    else
    {
        self.channelImagePromise = nil;
        self.channelImageView.image = nil;
    }
}


#pragma mark - State

-(void)setPlayerState:(RVSClipViewState)state
{
    switch (state)
    {
        case RVSClipViewStateLoading:
            [self.spinner startAnimating];
            break;
            
        default:
            [self.spinner stopAnimating];
            break;
    }
}

#pragma mark - Reload

-(void)reloadData
{
    if (!self.dataSource)
        return;
    
    self.isReloadingData = YES;
    
    [self invalidateChannelAndProgram];
    [self invalidatePlayerState];
    [self invalidateScrubber];
    
    self.isReloadingData = NO;
}

-(void)hideEndCardAnimated:(BOOL)animated
{
    self.endCardView.hidden = YES;
}

-(void)showEndCardAnimated:(BOOL)animated
{
    self.endCardView.hidden = NO;
}

-(void)invalidateChannelAndProgram
{
    NSObject <RVSChannel> *channel = [self.dataSource channelForPlayerControls:self];
    NSObject <RVSProgram> *program = [self.dataSource programForPlayerControls:self];

    [self setChannelImage:channel.watermarkLogo];

    self.titleLabel.text = program.show.name;
    
    if ([program.show.name length] > 0)
    {
        NSString *format = NSLocalizedString(@"end card text format", nil);
        self.endCardLabel.text = [NSString stringWithFormat:format, program.show.name, channel.name];
    }
    else
    {
        NSString *format = NSLocalizedString(@"end card no program text format", nil);
        self.endCardLabel.text = [NSString stringWithFormat:format, channel.name];
    }
}

-(void)invalidatePlayerState
{
    [self setPlayerState:[self.dataSource playerStateForPlayerControls:self]];
}

-(void)invalidateScrubber
{
    NSTimeInterval current = [self.dataSource mediaCurrentPositionForPlayerControls:self];
    NSTimeInterval duration = [self.dataSource mediaDurationForPlayerControls:self];
    
    BOOL animated = YES;
    
    if (self.isReloadingData) //suppress animation
        animated = NO;
    
    [self setScrubberProgressWithCurrent:current duration:duration animated:animated];
}

-(void)invalidateIsBufferingState
{
    BOOL isBuffering = [self.dataSource isBufferingStateForPlayerControls:self];
    
    if (isBuffering)
    {
        [self.spinner startAnimating];
    }
    else
    {
        [self.spinner stopAnimating];
    }

}

@end
