//
//  RVSAppChannelView.h
//  rvs_app2
//
//  Created by Barak Harel on 4/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <rvs_sdk_api.h>
#import <TTTAttributedLabel.h>

@interface RVSAppChannelView : UIView
@property (weak, nonatomic) IBOutlet TTTAttributedLabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet UIButton *composeButton;

@property (nonatomic) NSObject <RVSChannel> *channel;

-(void)setChannel:(NSObject<RVSChannel> *)channel;
-(void)clear;
@end
