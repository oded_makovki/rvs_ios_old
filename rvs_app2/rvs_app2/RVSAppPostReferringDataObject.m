//
//  RVSAppPostReffererDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 6/10/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostReferringDataObject.h"

@implementation RVSAppPostReferringDataObject

-(NSObject<RVSPostReferringUser> *)repostingUser
{
    return self.post.repostingUser;
}

@end
