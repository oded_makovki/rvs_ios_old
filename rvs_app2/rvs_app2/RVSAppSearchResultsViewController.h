//
//  RVSAppSearchResultsViewController.h
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"

@interface RVSAppSearchResultsViewController : RVSAppCommonDataViewController
@property (nonatomic) NSString *searchText;
@property (nonatomic, weak) UIViewController *owner;
@end
