//
//  RVSAppCommonDataViewController.m
//  rvs_app
//
//  Created by Barak Harel on 1/2/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCommonDataViewController.h"

@interface RVSAppCommonDataViewController ()

@end

@implementation RVSAppCommonDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!self.dataListView)
    {        
        self.automaticallyAdjustsScrollViewInsets = NO;
        
        RVSAppDataListView *dataListView = [[RVSAppDataListView alloc] initWithFrame:self.view.frame];
        self.dataListView = dataListView;
        [self.dataListView setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        CGRect navFrame = self.navigationController.navigationBar.frame;
        
        self.dataListView.contentInset = UIEdgeInsetsMake(navFrame.origin.y + navFrame.size.height, 0, 0, 0);
        [self.contentView addSubview:self.dataListView];
        
        self.dataListViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.dataListView attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        
        self.dataListViewBottomConstraint = [NSLayoutConstraint constraintWithItem:self.contentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.dataListView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
        
        [self.contentView addConstraints:@[self.dataListViewTopConstraint, self.dataListViewBottomConstraint]];
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.dataListView}]];        
        
        //update
        [self.dataListView layoutIfNeeded];
    }
    
    self.noDataView.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
