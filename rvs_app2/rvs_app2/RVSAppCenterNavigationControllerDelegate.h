//
//  RVSAppCenterNavigationControllerDelegate.h
//  rvs_app2
//
//  Created by Barak Harel on 3/17/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppTransitionControllerDelegate.h"

@interface RVSAppCenterNavigationControllerDelegate : RVSAppTransitionControllerDelegate
@end
