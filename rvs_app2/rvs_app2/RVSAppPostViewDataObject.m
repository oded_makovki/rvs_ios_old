//
//  RVSAppPostViewDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostViewDataObject.h"

@implementation RVSAppPostViewDataObject

-(id)initWithPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches
{
    self = [super initWithPost:post delegate:delegate];
    if (self)
    {
        self.post = post;
        self.searchMatches = searchMatches;
    }
    
    return self;
}

+(NSArray *)dataObjectsFromPosts:(NSArray *)posts delegate:(id<RVSAppViewDelegate>)delegate
{
    NSMutableArray *dataArray = [NSMutableArray arrayWithCapacity:posts.count];
    for (NSObject <RVSPost> *post in posts)
    {
        [dataArray addObject:[self dataObjectFromPost:post delegate:delegate searchMatches:nil]];
    }
    
    return dataArray;
}

+(instancetype)dataObjectFromPost:(NSObject<RVSPost> *)post delegate:(id<RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches
{
    id data = [[self alloc] initWithPost:post delegate:delegate searchMatches:searchMatches];
    return data;
}

@end
