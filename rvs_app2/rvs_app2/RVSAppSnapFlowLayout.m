//
//  RVSAppSnapFlowLayout.m
//  rvs_app
//
//  Created by Barak Harel on 11/27/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSnapFlowLayout.h"

@implementation RVSAppSnapFlowLayout

-(CGPoint) targetContentOffsetForProposedContentOffset:(CGPoint)proposedContentOffset
                                 withScrollingVelocity:(CGPoint)velocity
{
    if (!self.snapToCell)
        return proposedContentOffset;
        
        
    CGFloat offsetAdjustment = MAXFLOAT;
    CGFloat targetY = proposedContentOffset.y + self.minimumInteritemSpacing + self.sectionInset.top;
    
    CGRect targetRect = CGRectMake(0.0, proposedContentOffset.y, self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    
    NSArray *array = [super layoutAttributesForElementsInRect:targetRect];
    for(UICollectionViewLayoutAttributes *layoutAttributes in array) {
        
        if(layoutAttributes.representedElementCategory == UICollectionElementCategoryCell) {
            CGFloat itemY = layoutAttributes.frame.origin.y;
            
            if (ABS(itemY - targetY) < ABS(offsetAdjustment)) {
                offsetAdjustment = itemY - targetY;
            }
        }
    }
    
    return CGPointMake(proposedContentOffset.x, proposedContentOffset.y + offsetAdjustment);
}

@end
