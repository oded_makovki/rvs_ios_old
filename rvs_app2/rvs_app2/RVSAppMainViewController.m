//
//  RVSAppMainViewController.m
//  rvs_app2
//
//  Created by Barak Harel on 3/16/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppMainViewController.h"
#import "RVSAppCenterNavigationController.h"
#import "RVSAppLeftMenuViewController.h"

NSString *RVSAppMainContentTypeDidChangeNotification = @"RVSAppMainContentTypeDidChangeNotification";
NSString *RVSAppMainMenuWillAppearNotification = @"RVSAppMainMenuWillAppearNotification";
NSString *RVSAppMainMenuDidDisappearNotification = @"RVSAppMainMenuDidDisappearNotification";

extern NSString *RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey;
extern NSString *RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey;

@interface RVSAppMainViewController ()
@property (nonatomic) UIBarButtonItem *menuButton;
@end

@implementation RVSAppMainViewController

-(id)initWithLeftMenuViewController:(RVSAppLeftMenuViewController *)leftMenuViewController contentType:(RVSAppMainContentType)contentType
{
    RVSAppCenterNavigationController *centerNavigationController = [[RVSAppCenterNavigationController alloc] initWithRootContentType:contentType leftBarButtonItem:self.menuButton];    
    self = [super initWithCenterViewController:centerNavigationController
                               leftDrawerViewController:leftMenuViewController
                               rightDrawerViewController:nil];
    
    if (self)
    {
        _contentType = contentType;
        
        BOOL enableMenuOpenEdgeSwipe = [[NSUserDefaults standardUserDefaults] boolForKey:RVSApplicationSettingsEnableMenuOpenEdgeSwipeUserDefaultsKey];
        self.enableMenuOpenEdgeSwipe = enableMenuOpenEdgeSwipe;
        
        [self setMaximumLeftDrawerWidth:240.0];
        [self setShouldStretchDrawer:NO];
        [self setShowsShadow:NO];
        [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
        [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
        
        BOOL enableMenuCardAnimation = [[NSUserDefaults standardUserDefaults] boolForKey:RVSApplicationSettingsEnableMenuCardAnimationUserDefaultsKey];

        if (enableMenuCardAnimation)
        {
            [self setDrawerVisualStateBlock:[RVSAppLeftMenuViewController cardVisualStateBlock]];
        }
        else
        {
            [self setDrawerVisualStateBlock:[RVSAppLeftMenuViewController parallaxVisualStateBlockWithParallaxFactor:2.0]];
        }
        
        self.centerViewController.view.superview.layer.masksToBounds = NO;
        self.centerViewController.view.superview.layer.shadowRadius = 4.0f;
        self.centerViewController.view.superview.layer.shadowOpacity = 0.5f;
    }    
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(UIBarButtonItem *)menuButton
{
    if (!_menuButton)
    {
        _menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon_menu"] style:UIBarButtonItemStylePlain target:self action:@selector(menuButtonPressed:)];
    }
    
    return _menuButton;
}

-(void)menuButtonPressed:(UIBarButtonItem *)sender
{
    [self toggleDrawerSide:MMDrawerSideLeft animated:YES completion:nil];
}

#pragma mark - Menu

-(void)updateMenuGestures
{
    if ([self.centerViewController.viewControllers count] > 1 && !self.enableMenuOpenEdgeSwipe)
    {
        [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModePanningNavigationBar];
    }
    else
    {
        [self setOpenDrawerGestureModeMask: MMOpenDrawerGestureModeBezelPanningCenterView | MMOpenDrawerGestureModePanningNavigationBar];
    }
}

#pragma mark - Content

-(void)setContentType:(RVSAppMainContentType)contentType
{
    _contentType = contentType;
    
    RVSAppCenterNavigationController *centerNavigationController = [[RVSAppCenterNavigationController alloc] initWithRootContentType:contentType leftBarButtonItem:self.menuButton];
    
    [self setCenterViewController:centerNavigationController withFullCloseAnimation:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:RVSAppMainContentTypeDidChangeNotification object:self];
}

#pragma mark - Replace Center

-(void)replaceCenterViewController:(UIViewController *)viewControllerToPresent
{
    viewControllerToPresent.navigationItem.leftBarButtonItem = self.menuButton;
    [self.centerViewController setViewControllers:@[viewControllerToPresent] animated:YES];
}

#pragma mark - Memory

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Portrait Only

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

@end
