//
//  RVSAppDataCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/5/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIColor+RVSApplication.h"

@interface RVSAppDataCell : UICollectionViewCell
@property (nonatomic) NSObject *data;
@property (nonatomic, readonly) BOOL isSizingCell;

-(id)initSizingCellWithFrame:(CGRect)frame;

//override these:
-(void)prepareForReuse;

-(CGSize)sizeForData:(NSObject *)data;
+(CGSize)preferredCellSize;
@end
