//
//  RVSAppUserListFactory.m
//  rvs_app
//
//  Created by Barak Harel on 12/11/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserListFactory.h"

@interface RVSAppUserListFactory()
@property (nonatomic) id parameter;
@end

@implementation RVSAppUserListFactory

-(id)initWithFollowersOfUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeUserFollowers;
        _parameter = userId;
    }
    return self;
}

-(id)initWithFollowedByUser:(NSString *)userId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypeUserFollowing;
        _parameter = userId;
    }
    return self;
}

-(id)initWithLikingOfPost:(NSString *)postId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypePostLiking;
        _parameter = postId;
    }
    return self;
}

-(id)initWithRepostingOfPost:(NSString *)postId
{
    self = [super init];
    if (self)
    {
        _type = RVSAppUserListFactoryTypePostReposting;
        _parameter = postId;
    }
    return self;
}

-(NSObject<RVSAsyncList> *)userList
{
    switch (self.type)
    {
        case RVSAppUserListFactoryTypeUserFollowers:
            return [[RVSSdk sharedSdk] followersOfUser:self.parameter];
            break;
            
        case RVSAppUserListFactoryTypeUserFollowing:
            return [[RVSSdk sharedSdk] followedByUser:self.parameter];
            break;
            
        case RVSAppUserListFactoryTypePostReposting:
            return [[RVSSdk sharedSdk] repostingUsersForPost:self.parameter];
            break;
            
        case RVSAppUserListFactoryTypePostLiking:
            return [[RVSSdk sharedSdk] likingUsersForPost:self.parameter];
            break;
            
        default:
            return nil;
            break;
    }
}

@end
