//
//  RVSAppConfiguration.h
//  rvs_app2
//
//  Created by Barak Harel on 5/22/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *RVSAppConfigurationDidChangeNotification;

@interface RVSAppConfiguration : NSObject

+(RVSAppConfiguration*)sharedConfiguration;
-(void)loadConfiguration;

-(NSDictionary *)searchSettings;

@end
