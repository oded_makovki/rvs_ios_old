//
//  RVSAppSearchResultMatchData.h
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

@interface RVSAppSearchMatchData : NSObject
@property (nonatomic) NSString *text;
@property (nonatomic) NSArray *highlightRanges;
@property (nonatomic) NSNumber *referenceTime;


+(RVSAppSearchMatchData *)dataWithSearchMatch:(NSObject <RVSSearchMatch> *)searchMatch;

@end
