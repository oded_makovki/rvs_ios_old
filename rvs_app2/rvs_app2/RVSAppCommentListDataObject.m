//
//  RVSAppCommentListDataObject.m
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppCommentListDataObject.h"

@implementation RVSAppCommentListDataObject

-(id)initWithComment:(NSObject <RVSComment> *)comment delegate:(id<RVSAppViewDelegate>)delegate
{
    self = [super init];
    if (self)
    {
        self.delegate = delegate;
        self.comment = comment;
    }
    
    return self;
}

+(instancetype)dataObjectFromComment:(NSObject <RVSComment> *)comment delegate:(id<RVSAppViewDelegate>)delegate
{
    id dataObject = [[self alloc] initWithComment:comment delegate:delegate];
    
    return dataObject;
}

@end
