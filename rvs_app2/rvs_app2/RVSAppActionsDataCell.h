//
//  RVSAppCollectionViewActionsCell.h
//  rvs_app
//
//  Created by Barak Harel on 10/6/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppAction.h"
#import "RVSAppDataCell.h"

/// A subclass of UICollectionViewCell that enables editing and swipe to delete
@interface RVSAppActionsDataCell : RVSAppDataCell

@property (nonatomic, getter = isEditing) BOOL editing;

/// An array of RVSAppAction instances that should be displayed when this cell has been swiped for editing.
@property (nonatomic, strong) NSArray *editActions;

-(void)setMoreCanceledBlock:(RVSAppActionBlock)handler;

@end
