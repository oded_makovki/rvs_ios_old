//
//  RVSAppPostDataHelper.h
//  rvs_app2
//
//  Created by Barak Harel on 6/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RVSAppPostDataObject.h"
#import "RVSAppViewDelegate.h"

@interface RVSAppPostDataHelper : NSObject

+(Class)classForDataObject:(RVSAppPostDataObject *)dataObject;

+(NSArray *)dataObjectsForPost:(id)post delegate:(id<RVSAppViewDelegate>)delegate;
+(NSArray *)dataObjectsForPost:(id)post delegate:(id<RVSAppViewDelegate>)delegate searchMatches:(NSArray *)searchMatches;
+(NSArray *)dataObjectsForPosts:(NSArray *)posts delegate:(id<RVSAppViewDelegate>)delegate;

+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingPostId:(NSString *)postId;
+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingCommentsWithPostId:(NSString *)postId;
+(NSIndexSet *)indexesInDataArray:(NSArray *)data containingInfoWithPostId:(NSString *)postId;


@end
