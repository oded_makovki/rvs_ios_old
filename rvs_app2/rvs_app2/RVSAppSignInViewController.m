//
//  RVSAppSignInViewController.m
//  rvs_app
//
//  Created by Barak Harel on 10/30/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppSignInViewController.h"
#import "RVSApplication.h"
#import <rvs_sdk_api.h>
#import <rvs_sdk_api helpers.h>
#import <EAIntroView.h>
#import <CCAlertView.h>
#import "UIColor+RVSApplication.h"
#import "UIColor+HexString.h"
#import "UIFont+RVSApplication.h"

#import "RVSAppSocialManager.h"

typedef NS_ENUM(NSInteger, RVSAppSignInType)
{
    RVSAppSignInTypeUnknown,
    RVSAppSignInTypeUsername,
    RVSAppSignInTypeFacebook,
    RVSAppSignInTypeTwitter
};

@interface RVSAppSignInViewController () <UITextFieldDelegate, UIAlertViewDelegate, EAIntroDelegate>
@property (nonatomic) RVSNotificationObserver *signInErrorObserver;

@property (weak, nonatomic) IBOutlet UIView *backgroundView;
@property (weak, nonatomic) IBOutlet UIButton *usernameSignInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *usernameSignInActivityIndicator;

@property (weak, nonatomic) IBOutlet UIButton *facebookSignInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *facebookSignInActivityIndicator;

@property (weak, nonatomic) IBOutlet UIButton *twitterSignInButoon;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *twitterSignInActivityIndicator;

@property (weak, nonatomic) IBOutlet UIButton *skipSignInButton;

@property (nonatomic) EAIntroView *introView;
@property (nonatomic) NSTimer *introTimer;

@property (nonatomic) UIAlertView *signInWithUsernameAlertView;
@property (nonatomic) UITextField *usernameTextField;
@property (nonatomic) UITextField *passwordTextField;

@property (nonatomic) RVSPromiseHolder *facebookOAuthPromise;
@property (nonatomic) RVSPromiseHolder *twitterOAuthPromise;

@property (nonatomic) RVSAppSignInType signInType;
           
@end

@implementation RVSAppSignInViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.signInViewControllerSuccessPromise = [[RVSPromise alloc] init];
    
    __weak RVSAppSignInViewController *weakSelf = self;
    
    self.signInErrorObserver = [RVSNotificationObserver newObserverForNotificationWithName:RVSServiceSignInErrorNotification object:[RVSSdk sharedSdk] usingBlock:^(NSNotification *notification)
    {
        NSLog(@"RVSServiceSignInErrorNotification");
        NSError *error = notification.userInfo[RVSServiceSignedOutErrorUserInfoKey];
        [weakSelf signInFailedWithError:error];
    }];
    
    self.twitterSignInButoon.layer.cornerRadius = 5;
    self.facebookSignInButton.layer.cornerRadius = 5;
    
    [self.skipSignInButton setTitle:NSLocalizedString(@"sign in skip", nil) forState:UIControlStateNormal];
    self.skipSignInButton.enabled = YES;
    
    NSString* version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    if (!version)
        version = @"0.5.0";
    
    self.versionLabel.text = [NSString stringWithFormat:@"v%@", version];
    
    [self reset];
    [self prepareIntroView];    
}

-(void)wasDismissed
{
    if ([RVSSdk sharedSdk].isSignedOut)
    {
        [self.signInViewControllerSuccessPromise rejectWithReason:@"canceled"];
    }
    else
    {
        [self.signInViewControllerSuccessPromise fulfillWithValue:nil];
    }
}

#pragma mark - Intro View

-(void)prepareIntroView
{
    EAIntroPage *page1 = [self page];
    page1.title = NSLocalizedString(@"sign in intro page1 title", nil);
    page1.desc = NSLocalizedString(@"sign in intro page1 description", nil);
    page1.bgImage = [UIImage imageNamed:@"sign_in_back_01"];
    
    EAIntroPage *page2 = [self page];
    page2.title = NSLocalizedString(@"sign in intro page2 title", nil);;
    page2.desc = NSLocalizedString(@"sign in intro page2 description", nil);;
    page2.bgImage = [UIImage imageNamed:@"sign_in_back_02"];
    
    EAIntroPage *page3 = [self page];
    page3.title = NSLocalizedString(@"sign in intro page3 title", nil);;
    page3.desc = NSLocalizedString(@"sign in intro page3 description", nil);;
    page3.bgImage = [UIImage imageNamed:@"sign_in_back_03"];
    
    self.introView = [[EAIntroView alloc] initWithFrame:self.backgroundView.bounds andPages:@[page1,page2,page3]];
    self.introView.backgroundColor = [UIColor clearColor];
    
    self.introView.pageControl.currentPageIndicatorTintColor = [UIColor rvsTintColor];
    self.introView.pageControl.pageIndicatorTintColor = [UIColor darkGrayColor];
    self.introView.pageControlY = self.backgroundView.frame.size.height - (self.twitterSignInButoon.frame.origin.y - 30);
    self.introView.swipeToExit = NO;
    self.introView.skipButton = nil;
    [self.introView showInView:self.backgroundView animateDuration:0.0];
    [self.backgroundView sendSubviewToBack:self.introView];
    
    self.introView.delegate = self;
    
    [self startIntroTimer];
}

-(void)startIntroTimer
{
    NSLog(@"startIntroTimer");
    [self.introTimer invalidate];
    
    __weak typeof(self) weakSelf = self;
    self.introTimer = [NSTimer scheduledTimerWithTimeInterval:5 block:^(NSTimer *timer) {
        NSInteger pageIndex = weakSelf.introView.currentPageIndex;
        pageIndex++;

        if (pageIndex >= [weakSelf.introView.pages count])
            pageIndex = 0;

        [weakSelf.introView setCurrentPageIndex:pageIndex animated:YES];
    } repeats:YES];
}

-(void)intro:(EAIntroView *)introView pageStartScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex
{
    NSLog(@"invalidate");
    [self.introTimer invalidate];
}

-(void)intro:(EAIntroView *)introView pageEndScrolling:(EAIntroPage *)page withIndex:(NSInteger)pageIndex
{
//    [self startIntroTimer];
}

-(EAIntroPage *)page
{
    UIFont *titleFont = [UIFont rvsLightFontWithSize:24];
    UIFont *descFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];
    
    EAIntroPage *page = [EAIntroPage page];
    page.titleFont = titleFont;
    page.descFont = descFont;
    page.titleColor = [UIColor rvsTintColor];
    page.descColor = [UIColor colorWithRGBValue:0x6A717F];

    page.descriptionLabelSidePadding = 70; //35px x2
    page.titlePositionY = 380;
    page.descPositionY = 350;
    
    return page;
}

#pragma mark - Reset

-(void)reset
{
    [self.usernameSignInActivityIndicator stopAnimating];
    [self.facebookSignInActivityIndicator stopAnimating];
    [self.twitterSignInActivityIndicator stopAnimating];
    
    [self.usernameSignInButton setTitle:NSLocalizedString(@"sign in username button sign in", nil) forState:UIControlStateNormal];
    [self.facebookSignInButton setTitle:NSLocalizedString(@"sign in facebook button sign in", nil) forState:UIControlStateNormal];
    [self.twitterSignInButoon setTitle:NSLocalizedString(@"sign in twitter button sign in", nil) forState:UIControlStateNormal];
    
    self.usernameSignInButton.enabled = YES;
    self.facebookSignInButton.enabled = YES;
    self.twitterSignInButoon.enabled = YES;
    
    self.skipSignInButton.enabled = YES;
}


-(void)updateUIWithSignInType:(RVSAppSignInType)signInType
{
    self.usernameSignInButton.enabled = NO;
    self.facebookSignInButton.enabled = NO;
    self.twitterSignInButoon.enabled = NO;
    self.skipSignInButton.enabled = NO;
    
    switch (signInType)
    {
        case RVSAppSignInTypeFacebook:
            [self.facebookSignInButton setTitle:NSLocalizedString(@"sign in facebook button signing in", nil) forState:UIControlStateNormal];
            [self.facebookSignInActivityIndicator startAnimating];
            break;
            
        case RVSAppSignInTypeTwitter:
            [self.twitterSignInButoon setTitle:NSLocalizedString(@"sign in twitter button signing in", nil) forState:UIControlStateNormal];
            [self.twitterSignInActivityIndicator startAnimating];
            break;
            
        case RVSAppSignInTypeUsername:
            [self.usernameSignInButton setTitle:NSLocalizedString(@"sign in username button signing in", nil) forState:UIControlStateNormal];
            [self.usernameSignInActivityIndicator startAnimating];
            break;
            
        default:
            break;
    }
    
}

#pragma mark - Sign In Error

-(void)signInFailedWithError:(NSError *)error
{
    UIAlertView *alertView;
    if ([error.domain isEqual: RVSErrorDomain] && error.code == RVSErrorCodeSignInRequired)
    {
        switch (self.signInType) {
            case RVSAppSignInTypeUsername:
                [self requestCredentialsWithTitle:NSLocalizedString(@"sign in username bad credentials title" , nil)];
                break;
                
            case RVSAppSignInTypeFacebook:
                [self handleFacebookAccountError:error];
                break;
                
            case RVSAppSignInTypeTwitter:
                [self handleTwitterAccountError:error];
                break;
                
            default:
                break;
        }
    }
    else
    {
        alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"sign in error title", nil)
                                                            message:[error localizedDescription]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"alert ok", nil)
                                                  otherButtonTitles:nil];
        
        alertView.message = [error localizedDescription];
        [alertView show];
    }
    
    [self reset];
}

#pragma mark - Sign In With Facebook

- (IBAction)signInWithFacebookButtonPressed:(id)sender
{
    [self signInWithFacebook];
}

-(void)signInWithFacebook
{
    self.signInType = RVSAppSignInTypeFacebook;
    
    __weak typeof(self) weakSelf = self;
    
    self.facebookOAuthPromise = [[RVSAppSocialManager sharedManager] facebookOAuthToken].newHolder;
    [self.facebookOAuthPromise thenOnMain:^(NSString *oauthToken) {
        NSLog(@"token: %@", oauthToken);
        [[RVSSdk sharedSdk] signInWithFacebookToken:oauthToken];
    } error:^(NSError *error) {
        [weakSelf handleFacebookAccountError:error];
    }];
}

-(void)handleFacebookAccountError:(NSError *)error
{
    NSLog(@"handleFacebookAccountError: %@", error);
    
    NSString *title = NSLocalizedString(@"facebook permission unknown error title" , nil);
    NSString *message = NSLocalizedString(@"facebook permission unknown error message", nil);
    
    if ([error.domain isEqual:RVSAppSocialManagerErrorDomain])
    {
        switch (error.code) {
            case RVSAppSocialManagerErrorAccountNotFound:
                title = NSLocalizedString(@"facebook permission no account error title", nil);
                message = NSLocalizedString(@"facebook permission no account error message", nil);
                break;
                
            case RVSAppSocialManagerErrorPermissionDenied:
                title = NSLocalizedString(@"facebook permission no permission error title", nil);
                message = NSLocalizedString(@"facebook permission no permission error message", nil);
                if (error.userInfo && error.userInfo[NSLocalizedDescriptionKey])
                    message = error.userInfo[NSLocalizedDescriptionKey];
                break;
                
            default:
                break;
        }
        
    }
    else if ([error.domain isEqual:RVSErrorDomain] && error.code == RVSErrorCodeSignInRequired)
    {
        title = NSLocalizedString(@"facebook bad credentials title" , nil);
        message = NSLocalizedString(@"facebook bad credentials message", nil);
    }
    else
    {
        //TBD
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];
    [alertView show];
    [self reset];
}

#pragma mark - Sign In With Twitter

- (IBAction)signInWithTwitterButtonPressed:(id)sender
{
    [self signInWithTwitter];
}

-(void)signInWithTwitter
{
    self.signInType = RVSAppSignInTypeTwitter;
    
    __weak typeof(self) weakSelf = self;
    
    self.twitterOAuthPromise = [[RVSAppSocialManager sharedManager] twitterOAuthToken].newHolder;
    [self.twitterOAuthPromise thenOnMain:^(NSDictionary *oAuthDictionary) {

        NSString *token = oAuthDictionary[RVSAppSocialManagerTwitterOAuthTokenKey];
        NSString *secret = oAuthDictionary[RVSAppSocialManagerTwitterOAuthTokenSecretKey];
        
        [[RVSSdk sharedSdk] signInWithTwitterToken:token secret:secret];
        
    } error:^(NSError *error) {
        [weakSelf handleTwitterAccountError:error];
    }];
}

-(void)handleTwitterAccountError:(NSError *)error
{
    NSLog(@"handleTwitterAccountError: %@", error);
    
    NSString *title = NSLocalizedString(@"twitter permission unknown error title" , nil);
    NSString *message = NSLocalizedString(@"twitter permission unknown error message", nil);
    
    if ([error.domain isEqual:RVSAppSocialManagerErrorDomain])
    {
        switch (error.code) {
            case RVSAppSocialManagerErrorAccountNotFound:
                title = NSLocalizedString(@"twitter permission no account error title", nil);
                message = NSLocalizedString(@"twitter permission no account error message", nil);
                break;
                
            case RVSAppSocialManagerErrorPermissionDenied:
                title = NSLocalizedString(@"twitter permission no permission error title", nil);
                message = NSLocalizedString(@"twitter permission no permission error message", nil);
                if (error.userInfo && error.userInfo[NSLocalizedDescriptionKey])
                    message = error.userInfo[NSLocalizedDescriptionKey];
                break;
                
            default:
                break;
        }
        
    }
    else if ([error.domain isEqual:RVSErrorDomain] && error.code == RVSErrorCodeSignInRequired)
    {
        title = NSLocalizedString(@"twitter bad credentials title" , nil);
        message = NSLocalizedString(@"twitter bad credentials message", nil);
    }
    else
    {
        //TBD
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:NSLocalizedString(@"alert ok", nil) otherButtonTitles: nil];
    [alertView show];
    [self reset];
}

#pragma mark - Sign In With Username

- (IBAction)signInWithUsernameButtonPressed:(id)sender
{
    [self requestCredentialsWithTitle:NSLocalizedString(@"sign in username enter credentials title", nil)];
}

-(void)requestCredentialsWithTitle:(NSString *)title
{
    [self.introTimer invalidate];
    
    self.signInWithUsernameAlertView = [[UIAlertView alloc] initWithTitle:title message:NSLocalizedString(@"sign in username enter credentials message", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"alert cancel", nil) otherButtonTitles:NSLocalizedString(@"alert ok", nil), nil];
    self.signInWithUsernameAlertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    self.signInWithUsernameAlertView.delegate = self;
    
    self.usernameTextField = [self.signInWithUsernameAlertView textFieldAtIndex:0];
    self.passwordTextField = [self.signInWithUsernameAlertView textFieldAtIndex:1];
    
    self.usernameTextField.returnKeyType = UIReturnKeyNext;
    self.passwordTextField.returnKeyType = UIReturnKeyDone;
    self.passwordTextField.delegate = self;
    
    self.usernameTextField.placeholder = NSLocalizedString(@"sign in username credentials placeholder email", nil);
    self.passwordTextField.placeholder = NSLocalizedString(@"sign in username credentials placeholder password", nil);
    
    [self.signInWithUsernameAlertView show];
}

- (BOOL)canSignInWithUsername
{
    return ![self.usernameTextField.text isEmpty] && ![self.passwordTextField.text isEmpty];
}

-(void)signInWithUsername
{
    self.signInType = RVSAppSignInTypeUsername;
    
    [[RVSSdk sharedSdk] signInWithUserName:self.usernameTextField.text password:self.passwordTextField.text];
}

#pragma mark - SignInType

-(void)setSignInType:(RVSAppSignInType)signInType
{
    _signInType = signInType;
    [[NSUserDefaults standardUserDefaults] setObject:@(signInType) forKey:@"SignInType"];
    
    [self updateUIWithSignInType:signInType];
}

-(void)setLastSignInType
{
    self.signInType = [[[NSUserDefaults standardUserDefaults] objectForKey:@"SignInType"] integerValue];
}

#pragma mark - AlertView Delegate


-(void)willPresentAlertView:(UIAlertView *)alertView
{
    if (alertView == self.signInWithUsernameAlertView)
    {
        NSString *username = [RVSSdk sharedSdk].myUserId;
        if (username.length > 0)
        {
            self.usernameTextField.text = username;
        }
    }
}

-(void)didPresentAlertView:(UIAlertView *)alertView
{
    if (alertView == self.signInWithUsernameAlertView)
    {
//        if (self.usernameTextField.text.length > 0)
//        {
//            [self.passwordTextField becomeFirstResponder];
//        }
    }
}

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
    return [self canSignInWithUsername];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.signInWithUsernameAlertView)
    {
        if (buttonIndex == 1)
        {
            [self signInWithUsername];
        }
    }
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.passwordTextField && [self canSignInWithUsername])
    {
        [self.signInWithUsernameAlertView dismissWithClickedButtonIndex:1 animated:YES];
        [self signInWithUsername];
        
        return YES;
    }
    
    return NO;
}

#pragma mark - Sign In

- (IBAction)skipSignInPressed:(UIButton *)sender
{
    [[RVSApplication application] skipSignIn];
}


#pragma mark - Misc

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

#pragma mark - Rotation

-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


@end
