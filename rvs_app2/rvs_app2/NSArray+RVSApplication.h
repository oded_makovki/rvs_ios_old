//
//  NSArray+RVSApplication.h
//  rvs_app2
//
//  Created by Barak Harel on 5/26/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//



@interface NSArray (RVSApplication)

/**
 *  create an array on NSRange from <RVSFragmentRange> in array
 *
 *  @param offset optional offset of ranges
 *
 *  @return array of NSRange wrapped in NSValue
 */
-(NSMutableArray *)rangesFromFragmentRangesWithOffset:(NSInteger)offset;
-(NSIndexSet *)indexesInDataOfKindOfClass:(Class)class containingDataId:(NSString *)dataId inKeyPath:(NSString *)keyPath;
@end
