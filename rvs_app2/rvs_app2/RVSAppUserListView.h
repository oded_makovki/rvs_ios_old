//
//  RVSAppUserListView.h
//  rvs_app2
//
//  Created by Barak Harel on 5/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppUserView.h"

@interface RVSAppUserListView : RVSAppUserView
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@end
