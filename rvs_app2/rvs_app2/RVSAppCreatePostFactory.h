//
//  RVSAppCompostPostFactory.h
//  rvs_app
//
//  Created by Barak Harel on 1/7/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <rvs_sdk_api.h>

@interface RVSAppCreatePostFactory : NSObject

-initWithPostWithText:(NSString *)text mediaContext:(NSObject <RVSMediaContext> *)mediaContext startOffset:(NSTimeInterval)startOffset duration:(NSTimeInterval)duration coverImageId:(NSString *)coverImageId;

-(RVSPromise*)createPost;

@end
