//
//  RVSAppPostInfoDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 5/27/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDataObject.h"
#import <rvs_sdk_api.h>
#import "RVSAppPostInfoView.h"

@interface RVSAppPostInfoDataObject : RVSAppPostDataObject
-(NSInteger) likeCount;
-(NSInteger) repostCount;
@end
