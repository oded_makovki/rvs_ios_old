//
//  RVSAppUserDetailCell.h
//  rvs_app
//
//  Created by Barak Harel on 12/8/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppUserDataCell.h"
#import "RVSAppUserDetailView.h"

@interface RVSAppMyUserDetailCell : RVSAppUserDataCell
@property (weak, nonatomic) IBOutlet RVSAppUserDetailView *userView;
@end
