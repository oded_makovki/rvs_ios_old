//
//  RVSAppLoadingPostCell.m
//  rvs_app
//
//  Created by Barak Harel on 11/21/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAppDataListFooterCell.h"
#import "UIView+NibLoading.h"
#import "UIColor+RVSApplication.h"

@interface RVSAppDataListFooterCell()

@end

@implementation RVSAppDataListFooterCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
//        NSLog(@"new RVSAppDataListFooterCell");
        [self initRVSAppDataListFooterCell];
    }
    return self;
}

-(void)dealloc
{
//    NSLog(@"dealloc RVSAppDataListFooterCell");
}

-(void)initRVSAppDataListFooterCell
{
    self.backgroundColor = [UIColor clearColor];
    
    RVSAppSpinnerView *spinner = [[RVSAppSpinnerView alloc] initWithIndicatorImage:[UIImage imageNamed:@"small_spinner"] backgroundImage:nil];
    
    CGPoint center = self.center;
    center.y = 15; //offset up
    
    spinner.center = center;
    spinner.hidesWhenStopped = YES;
    
    [self addSubview:spinner];
    self.spinner = spinner;
    
    self.type = RVSAppDataListFooterTypeDefault;
}

-(void)setType:(RVSAppDataListFooterType)type
{
    _type = type;
    
    switch (_type)
    {
        case RVSAppDataListFooterTypeDefault:
            [self.spinner startAnimating];
            break;
            
        case RVSAppDataListFooterTypeLoading:
            [self.spinner startAnimating];
            break;
            
        case RVSAppDataListFooterTypeEndOfList:
            [self.spinner stopAnimating];
            break;
            
        default:
            [self.spinner stopAnimating];
            break;
    }
}

-(CGSize)size
{
    CGSize size;
    
    if (self.type == RVSAppDataListFooterTypeEndOfList)
    {
        size = CGSizeMake(320, 0.1);
    }
    else
    {
        size = [RVSAppDataListFooterCell defaultSize];
    }
    
//    NSLog(@"footer size: %@", NSStringFromCGSize(size));    
    return size;
}

+(CGSize)defaultSize
{
    return CGSizeMake(320, 100);
}

@end
