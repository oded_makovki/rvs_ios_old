//
//  RVSAppPostDetailView.m
//  rvs_app2
//
//  Created by Barak Harel on 4/2/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppPostDetailView.h"
#import <rvs_sdk_api helpers.h>
#import "RVSApplication.h"
#import "RVSAppCenterNavigationController.h"
#import "RVSAppPostActivityProvider.h"
#import "UIColor+RVSApplication.h"
#import <CCActionSheet.h>
#import <CCAlertView.h>
#import "UIFont+RVSApplication.h"
#import <Social/Social.h>
#import <MessageUI/MessageUI.h>
#import "RVSAppStringUtil.h"


@interface RVSAppPostDetailView() <MFMailComposeViewControllerDelegate>
@property (nonatomic) RVSKeyPathObserver *postPropertiesObserver;
@property (nonatomic) RVSPromiseHolder *likePromise;
@property (nonatomic) RVSPromiseHolder *repostPromise;

@property (nonatomic) RVSPromiseHolder *signInPromise;
@property (nonatomic) RVSPromiseHolder *sharingUrlPromise;
@end

@implementation RVSAppPostDetailView

-(void)awakeFromNib
{
    [super awakeFromNib];
    [self initRVSAppPostDetailView];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self initRVSAppPostDetailView];
    }
    
    return self;
}

-(void)initRVSAppPostDetailView
{
    if (self.commentButton)
    {
        [self.commentButton addTarget:self action:@selector(comment:) forControlEvents:UIControlEventTouchUpInside];
        [self.commentButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.commentButton setTitle:NSLocalizedString(@"post action comment", nil) forState:UIControlStateNormal];
        [self.commentButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];
    }
    
    if (self.repostToggleButton)
    {
        [self.repostToggleButton addTarget:self action:@selector(toggleRepost:) forControlEvents:UIControlEventTouchUpInside];
        [self.repostToggleButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.repostToggleButton setTitleColor:[[UIColor rvsPostActionButtonTextColor] colorWithAlphaComponent:0.3] forState:UIControlStateDisabled];
        [self.repostToggleButton setTitle:NSLocalizedString(@"post action repost", nil) forState:UIControlStateNormal];
        [self.repostToggleButton setTitle:NSLocalizedString(@"post action reposted", nil) forState:UIControlStateSelected];
        [self.repostToggleButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];

    }
    
    if (self.likeToggleButton)
    {
        [self.likeToggleButton addTarget:self action:@selector(toggleLike:) forControlEvents:UIControlEventTouchUpInside];
        [self.likeToggleButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.likeToggleButton setTitle:NSLocalizedString(@"post action like", nil) forState:UIControlStateNormal];
        [self.likeToggleButton setTitle:NSLocalizedString(@"post action liked", nil) forState:UIControlStateSelected];
        [self.likeToggleButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];
    }
    
    if (self.relatedButton)
    {
        [self.relatedButton addTarget:self action:@selector(relatedButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [self.relatedButton setTitleColor:[UIColor rvsPostActionButtonTextColor] forState:UIControlStateNormal];
        [self.relatedButton setTitle:NSLocalizedString(@"post action related", nil) forState:UIControlStateNormal];
        [self.relatedButton.titleLabel setFont:[UIFont rvsBoldFontWithSize:10]];
    }
}

-(void)clear
{
    [super clear];
    self.relatedButton.enabled = NO;
    self.commentButton.enabled = NO;
    self.repostToggleButton.enabled = NO;
    self.likeToggleButton.enabled = NO;
    self.repostToggleButton.selected = NO;
    self.likeToggleButton.selected = NO;
    
    _postPropertiesObserver = nil;
    _sharingUrlPromise = nil;
    _signInPromise = nil;
}

-(void)setPost:(NSObject <RVSPost> *)post searchMatches:(NSArray *)searchMatches isSizingView:(BOOL)isSizingView
{
    [super setPost:post searchMatches:searchMatches isSizingView:isSizingView];
    
    if (!post)
        return;    
    
    //skip this (for performance optimization)
    if (!isSizingView)
    {
        self.relatedButton.enabled = YES;
        self.commentButton.enabled = YES;
        self.likeToggleButton.enabled = YES;
        
        if ([self.post.likedByCaller boolValue])
            self.likeToggleButton.selected = YES;
        
        if ([self.post.repostedByCaller boolValue])
            self.repostToggleButton.selected = YES;

        //dont allow to repost self posts
        if (![[RVSApplication application] isMyUserId:self.post.author.userId])
            self.repostToggleButton.enabled = YES;

        
        [self setKvo];
    }
}

- (void)setKvo
{
    __weak typeof(self) weakSelf = self;
    
    NSArray *props = @[@"likedByCaller", @"repostedByCaller"];
    self.postPropertiesObserver = [self.post newObserverForKeyPaths:props options:NSKeyValueObservingOptionNew usingBlock:^(id obj, NSString *keyPath, NSDictionary *change) {
        [weakSelf updateActionButtons:keyPath];
    }];
    
}

-(void)updateActionButtons:(NSString *)keyPath
{
    if ([keyPath isEqualToString:@"repostedByCaller"])
    {
        self.repostToggleButton.selected = [self.post.repostedByCaller boolValue];
    }
    else if ([keyPath isEqualToString:@"likedByCaller"])
    {
        self.likeToggleButton.selected = [self.post.likedByCaller boolValue];
    }
}

#pragma mark - Actions

-(void)relatedButtonPressed:(UIButton *)sender
{
    if (!self.post)
        return;
    
    self.sharingUrlPromise = [[RVSSdk sharedSdk] sharingUrlForPost:self.post.postId].newHolder;
    
    __weak typeof(self) weakSelf = self;
    BOOL isMyPost = ![RVSSdk sharedSdk].isSignedOut && [[RVSApplication application] isMyUserId:self.post.author.userId];
    
    CCActionSheet *actionSheet = [[CCActionSheet alloc] initWithTitle:nil];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"action share facebook" , nil) block:^{
        [weakSelf.sharingUrlPromise thenOnMain:^(NSURL *shareUrl) {
            
            SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
            [composeController setInitialText:[RVSAppStringUtil facebookShareTextFromText:weakSelf.post.text]];
            [composeController addURL:shareUrl];
            
            [[RVSApplication application].mainViewController presentViewController:composeController animated:YES completion:nil];
            
        } error:^(NSError *error) {
            //TBD
            NSLog(@"error getting share url: %@", [error localizedDescription]);
        }];
    }];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"action share twitter" , nil) block:^{
        [weakSelf.sharingUrlPromise thenOnMain:^(NSURL *shareUrl) {
            
            SLComposeViewController *composeController = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
            [composeController setInitialText:[RVSAppStringUtil twitterShareTextFromText:weakSelf.post.text]];
            [composeController addURL:shareUrl];
            
            [[RVSApplication application].mainViewController presentViewController:composeController animated:YES completion:nil];
            
        } error:^(NSError *error) {
            //TBD
            NSLog(@"error getting share url: %@", [error localizedDescription]);
        }];
    }];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"action share email", nil) block:^{
        
        [weakSelf.sharingUrlPromise thenOnMain:^(NSURL *shareUrl) {
            
            MFMailComposeViewController *composeController = [[MFMailComposeViewController alloc] init];
            composeController.mailComposeDelegate = weakSelf;
            [composeController setSubject:NSLocalizedString(@"share email subject", nil)];
            [composeController setMessageBody:[RVSAppStringUtil emailShareTextFromText:weakSelf.post.text andLink:shareUrl.absoluteString] isHTML:NO];
            
            [[RVSApplication application].mainViewController presentViewController:composeController animated:YES completion:nil];
                
        } error:^(NSError *error) {
            //TBD
            NSLog(@"error getting share url: %@", [error localizedDescription]);
        }];
    }];
    
    [actionSheet addButtonWithTitle:NSLocalizedString(@"action copy post url", nil) block:^{
        
        [weakSelf.sharingUrlPromise thenOnMain:^(NSURL *shareUrl) {
            [[UIPasteboard generalPasteboard] setString:shareUrl.absoluteString];
        } error:^(NSError *error) {
            //TBD
            NSLog(@"error getting share url: %@", [error localizedDescription]);
        }];
    }];
    
    if (isMyPost)
    {
        [actionSheet addDestructiveButtonWithTitle:NSLocalizedString(@"action delete post", nil) block:^{
            CCAlertView *alertView = [[CCAlertView alloc] initWithTitle:NSLocalizedString(@"delete post title", nil) message:NSLocalizedString(@"delete post message", nil)];
            [alertView addButtonWithTitle:NSLocalizedString(@"alert cancel", nil) block:nil];
            [alertView addButtonWithTitle:NSLocalizedString(@"alert delete", nil) block:^{
                [[RVSApplication application] deletePostId:weakSelf.post.postId];
            }];
            [alertView show];
        }];
    }
    
    [actionSheet addCancelButtonWithTitle:NSLocalizedString(@"action cancel", nil)];
    [actionSheet showInView:[RVSApplication application].mainViewController.view];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [controller.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(void)comment:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(view:didSelectCommentsForPostId:shouldStartEdit:)])
    {
        //keep value of property in case of reuse of view before action is ended. DON'T use self.post.(property) !
        NSString *postId = self.post.postId;
        //must be signed in!
        __weak typeof(self) weakSelf = self;
        self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
        
        [self.signInPromise thenOnMain:^(id result) {
            [weakSelf.delegate view:weakSelf didSelectCommentsForPostId:postId shouldStartEdit:YES];
        } error:nil];
    }
}

-(void)toggleRepost:(UIButton *)sender
{
    //keep value of property in case of reuse of view before action is ended. DON'T use self.post.(property) !
    NSString *postId = self.post.postId;
    NSString *userId = self.post.author.userId; //don't allow self repost
    
    //must be signed in!
    __weak typeof(self) weakSelf = self;
    self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        
        if ([[RVSApplication application] isMyUserId:userId])
            return; //if just signed in - make sure we this is not our post!
        
        weakSelf.repostToggleButton.selected = !self.repostToggleButton.selected;
        
        if ([weakSelf.post.repostedByCaller boolValue])
            weakSelf.repostPromise = [[RVSSdk sharedSdk] deleteRepostForPost:postId].newHolder;
        else
            weakSelf.repostPromise = [[RVSSdk sharedSdk] createRepostForPost:postId].newHolder;
        
        [weakSelf.repostPromise thenOnMain:^(id result) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationDidUpdatePostNotification object:nil userInfo:@{RVSApplicationDidUpdatePostIdUserInfoKey: postId}];
            
        } error:^(NSError *error) {
            NSLog(@"error: %@", error);
        }];
        
    } error:nil];
}

-(void)toggleLike:(UIButton *)sender
{
    //keep value of property in case of reuse of view before action is ended. DON'T use self.post.(property) !
    NSString *postId = self.post.postId;
    
    //must be signed in!
    __weak typeof(self) weakSelf = self;
    self.signInPromise = [[RVSApplication application] showSignInIfNeeded].newHolder;
    
    [self.signInPromise thenOnMain:^(id result) {
        
        weakSelf.likeToggleButton.selected = !self.likeToggleButton.selected;
        
        if ([weakSelf.post.likedByCaller boolValue])
            weakSelf.likePromise = [[RVSSdk sharedSdk] deleteLikeForPost:postId].newHolder;
        else
            weakSelf.likePromise = [[RVSSdk sharedSdk] createLikeForPost:postId].newHolder;
        
        [weakSelf.likePromise thenOnMain:^(id result) {
            
            [[NSNotificationCenter defaultCenter] postNotificationName:RVSApplicationDidUpdatePostNotification object:nil userInfo:@{RVSApplicationDidUpdatePostIdUserInfoKey: postId}];
            
        } error:^(NSError *error) {
            NSLog(@"error: %@", error);
        }];
        
    } error:nil];
}

@end

