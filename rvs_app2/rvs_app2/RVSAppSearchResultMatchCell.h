//
//  RVSAppSearchResultMatchCell.h
//  rvs_app2
//
//  Created by Barak Harel on 5/8/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RVSAppSearchMatchData.h"

@interface RVSAppSearchResultMatchCell : UICollectionViewCell
-(void)setSearchMatchData:(RVSAppSearchMatchData *)data;
@end
