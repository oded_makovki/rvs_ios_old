//
//  RVSAppCardTransitionController.m
//  rvs_app
//
//  Created by Barak Harel on 2/24/14.
//  Copyright (c) 2014 RayV. All rights reserved.
//

#import "RVSAppCardTransitionController.h"

@implementation RVSAppCardTransitionController

- (id)init {
    if (self = [super init])
    {
        self.duration = 0.25f;
    }
    return self;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    if(self.reverse)
    {
        [self executeReverseAnimation:transitionContext fromVC:fromVC toVC:toVC fromView:fromView toView:toView];
    } else {
        [self executeForwardsAnimation:transitionContext fromVC:fromVC toVC:toVC fromView:fromView toView:toView];
    }
    
}

-(void)executeForwardsAnimation:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    UIView* containerView = [transitionContext containerView];
    
    // positions the to- view off the bottom of the sceen
    CGRect frame = [transitionContext initialFrameForViewController:fromVC];
    CGRect offScreenFrame = frame;
    
    offScreenFrame.origin.x = offScreenFrame.size.width;
    toView.frame = offScreenFrame;
    
    [containerView insertSubview:toView aboveSubview:fromView];
    
    CATransform3D t = CATransform3DIdentity;
    t = CATransform3DScale(t, 0.95, 0.95, 1);
    
    [UIView animateWithDuration:self.duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        
        fromView.layer.transform = t;
        fromView.alpha = 0.6;
        toView.frame = frame;
        
    } completion:^(BOOL finished) {
        
        fromView.alpha = 1;
        fromView.layer.transform = CATransform3DIdentity;
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        
    }];
}

-(void)executeReverseAnimation:(id<UIViewControllerContextTransitioning>)transitionContext fromVC:(UIViewController *)fromVC toVC:(UIViewController *)toVC fromView:(UIView *)fromView toView:(UIView *)toView {
    
    UIView* containerView = [transitionContext containerView];
    
    // positions the to- view behind the from- view
    CGRect frame = [transitionContext initialFrameForViewController:fromVC];
    toView.frame = frame;
    
    CATransform3D t = CATransform3DIdentity;
    toView.layer.transform = CATransform3DScale(t, 0.95, 0.95, 1);
    toView.alpha = 0.6;
    
    [containerView insertSubview:toView belowSubview:fromView];
    
    CGRect frameOffScreen = frame;
    frameOffScreen.origin.x = frame.size.width;
    
   [UIView animateWithDuration:self.duration delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
    
            fromView.frame = frameOffScreen;
            toView.layer.transform = CATransform3DIdentity;
            toView.alpha = 1.0;
        
    } completion:^(BOOL finished) {
        
        toView.layer.transform = CATransform3DIdentity;
        toView.alpha = 1.0;
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
        
    }];
}


@end
