//
//  RVSAppSocialManager.m
//  rvs_app2
//
//  Created by Barak Harel on 4/29/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import "RVSAppSocialManager.h"
#import <Social/Social.h>
#import "TWSignedRequest.h"

#define FB_APP_ID          @"708977959143377" //whipclip

#define TW_CONSUMER_KEY    @"c8CI0RqOyWTCRAGq5ROT2cpN2"
#define TW_CONSUMER_SECRET @"Dx72rm4MIR6Asa00DTGBhBUobrZVMfOFc0R64hs1EVAvtv6JaG"

#define TW_API_ROOT                  @"https://api.twitter.com"
#define TW_X_AUTH_MODE_KEY           @"x_auth_mode"
#define TW_X_AUTH_MODE_REVERSE_AUTH  @"reverse_auth"
#define TW_X_AUTH_MODE_CLIENT_AUTH   @"client_auth"
#define TW_X_AUTH_REVERSE_PARMS      @"x_reverse_auth_parameters"
#define TW_X_AUTH_REVERSE_TARGET     @"x_reverse_auth_target"
#define TW_OAUTH_URL_REQUEST_TOKEN   TW_API_ROOT "/oauth/request_token"
#define TW_OAUTH_URL_AUTH_TOKEN      TW_API_ROOT "/oauth/access_token"

#define TW_URL_STATUS_UPDATE         TW_API_ROOT "/1.1/statuses/update.json"

NSString *RVSAppSocialManagerErrorDomain = @"com.rayv.app.socialmanager";
NSString *RVSAppSocialManagerTwitterOAuthTokenKey = @"oauth_token";
NSString *RVSAppSocialManagerTwitterOAuthTokenSecretKey = @"oauth_token_secret";

typedef void(^RVSSocialManagerRequestDataHandler)(NSData *data, NSError *error);

@interface RVSAppSocialManager()

@property (nonatomic) ACAccountStore *accountStore;

@property (nonatomic) ACAccount *facebookAccount;
@property (nonatomic) ACAccountType *facebookAccountType;

@property (nonatomic) ACAccount *twitterAccount;
@property (nonatomic) ACAccountType *twitterAccountType;
@end

@implementation RVSAppSocialManager

+(RVSAppSocialManager *)sharedManager
{
    static RVSAppSocialManager* _sharedManager;
    static dispatch_once_t token;
    
    dispatch_once(&token, ^{
        _sharedManager = [[RVSAppSocialManager alloc] init];
    });
    
    return  _sharedManager;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        self.accountStore = [[ACAccountStore alloc] init];
        
        self.facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
        self.twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    }
    return self;
}

-(RVSPromise *)facebookOAuthToken
{
    NSLog(@"%@", [[NSBundle mainBundle] bundleIdentifier]);
    
    __weak typeof(self) weakSelf = self;
    RVSPromise *facebookOAuthPromise = [[RVSPromise alloc] init];
    
    NSDictionary *options = @{ACFacebookAppIdKey: FB_APP_ID,
                              ACFacebookPermissionsKey: @[@"email"]
                              };
    
    [self.accountStore requestAccessToAccountsWithType:self.facebookAccountType options:options completion:^(BOOL granted, NSError *error) {
        
        if (error)
        {
            [facebookOAuthPromise rejectWithReason:[weakSelf managerErrorForError:error]];
        }
        else if (granted)
        {
            weakSelf.facebookAccount = [[weakSelf.accountStore accountsWithAccountType:weakSelf.facebookAccountType] lastObject];
            
            if (weakSelf.facebookAccount)
            {
                [facebookOAuthPromise fulfillWithValue:[[weakSelf.facebookAccount credential] oauthToken]];
            }
            else
            {
                [facebookOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorAccountNotFound userInfo:nil]];
            }
        }
        else
        {
            [facebookOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorPermissionDenied userInfo:nil]];
        }
    }];
    
    return facebookOAuthPromise;
}

-(NSError *)managerErrorForError:(NSError *)error
{
    NSDictionary *userInfo = nil;
    NSInteger code = RVSAppSocialManagerErrorUnknown;
    
    if ([[error domain] isEqualToString:ACErrorDomain])
    {
        switch (error.code)
        {
            case ACErrorAccountNotFound:
                code = RVSAppSocialManagerErrorAccountNotFound;
                break;
            case ACErrorPermissionDenied:
                code = RVSAppSocialManagerErrorPermissionDenied;
                userInfo = error.userInfo;
                break;
                
            default:
                break;
        }
    }
    
    return [NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:code userInfo:userInfo];
}

+ (BOOL)isLocalTwitterAccountAvailable
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter];
}

+ (BOOL)isLocalFacebookAccountAvailable
{
    return [SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook];
}

-(RVSPromise *)twitterOAuthToken
{
    __weak typeof(self) weakSelf = self;
    RVSPromise *twitterOAuthPromise = [[RVSPromise alloc] init];
    
    [self.accountStore requestAccessToAccountsWithType:self.twitterAccountType options:nil completion:^(BOOL granted, NSError *error) {
        
        if (error)
        {
            [twitterOAuthPromise rejectWithReason:[weakSelf managerErrorForError:error]];
        }
        else if (granted)
        {
            weakSelf.twitterAccount = [[weakSelf.accountStore accountsWithAccountType:weakSelf.twitterAccountType] lastObject];
            
            if (weakSelf.twitterAccount)
            {
                RVSPromise *twitterInternalRequestTokenPromise = [self twitterInternalRequestToken];
                [twitterInternalRequestTokenPromise thenOnMain:^(NSString *signedReverseAuthSignature) {
                    if ([signedReverseAuthSignature length] <= 0)
                    {
                        [twitterOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorUnknown userInfo:nil]];
                    }
                    else
                    {
                        RVSPromise *twitterInternalOAuthTokenPromise = [self twitterInternalOAuthToken:weakSelf.twitterAccount signature:signedReverseAuthSignature];
                        
                        [twitterInternalOAuthTokenPromise thenOnMain:^(NSDictionary *oAuthDictionary) {
                            
                            [twitterOAuthPromise fulfillWithValue:oAuthDictionary];
                            
                        } error:^(NSError *error) {
                            
                            [twitterOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorUnknown userInfo:nil]];
                        }];
                        
                    }
                    
                } error:^(NSError *error) {
                    
                    [twitterOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorUnknown userInfo:nil]];
                }];
            }
            else
            {
                [twitterOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorAccountNotFound userInfo:nil]];
            }
        }
        else
        {
            [twitterOAuthPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorPermissionDenied userInfo:nil]];
        }
    }];
    
    return twitterOAuthPromise;
}

#pragma mark - twitter oauth

/**
 *  The first stage of Reverse Auth.
 *
 *  In this step, we sign and send a request to Twitter to obtain an
 *  Authorization: header which we will use in Step 2.
 *
 *  @return promise fullfiled with NSString *signedReverseAuthSignature
 */
- (RVSPromise *)twitterInternalRequestToken
{
    RVSPromise *requestPromise = [[RVSPromise alloc] init];
    
    NSURL *url = [NSURL URLWithString:TW_OAUTH_URL_REQUEST_TOKEN];
    NSDictionary *dict = @{ TW_X_AUTH_MODE_KEY: TW_X_AUTH_MODE_REVERSE_AUTH };
    
    TWSignedRequest *step1Request = [[TWSignedRequest alloc] initWithURL:url parameters:dict requestMethod:TWSignedRequestMethodPOST consumerKey:TW_CONSUMER_KEY consumerSecret:TW_CONSUMER_SECRET];
    
    NSLog(@"Step 1: Sending a request to %@ parameters %@", url, dict);
    
    [step1Request performRequestWithHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if (error)
        {
            [requestPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorUnknown userInfo:nil]];
        }
        else
        {
            NSString *signedReverseAuthSignature = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            [requestPromise fulfillWithValue:signedReverseAuthSignature];
        }
    }];
    
    return requestPromise;
}

/**
 *  The second stage of Reverse Auth.
 *
 *  In this step, we send our signed authorization header to Twitter in a
 *  request that is signed by iOS.
 *
 *  @param account                      The local account for which we wish to exchange tokens
 *  @param signedReverseAuthSignature   The Authorization: header returned from
 *                                      a successful step 1
 *
 *  @return promise fullfiled with NSDictionary *oAuthDictionary
 */
- (RVSPromise *)twitterInternalOAuthToken:(ACAccount *)account signature:(NSString *)signedReverseAuthSignature
{
    NSParameterAssert(account);
    NSParameterAssert(signedReverseAuthSignature);
    
    RVSPromise *requestPromise = [[RVSPromise alloc] init];
    
    NSDictionary *step2Params = @{
                                  TW_X_AUTH_REVERSE_TARGET: TW_CONSUMER_KEY,
                                  TW_X_AUTH_REVERSE_PARMS: signedReverseAuthSignature
                                  };
    
    NSURL *authTokenURL = [NSURL URLWithString:TW_OAUTH_URL_AUTH_TOKEN];
    SLRequest *step2Request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:SLRequestMethodPOST URL:authTokenURL parameters:step2Params];
    
    NSLog(@"Step 2: Sending a request to %@\nparameters %@\n", authTokenURL, step2Params);
    
    [step2Request setAccount:account];
    [step2Request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if (error)
        {
            [requestPromise rejectWithReason:[NSError errorWithDomain:RVSAppSocialManagerErrorDomain code:RVSAppSocialManagerErrorUnknown userInfo:nil]];
        }
        else
        {
            NSString *resposeString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
            NSDictionary *oAuthDictionary = [self parametersFromQueryString:resposeString];
            
            [requestPromise fulfillWithValue:oAuthDictionary];
        }
    }];
    
    return requestPromise;
}

- (NSDictionary *)parametersFromQueryString:(NSString *)queryString
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (queryString) {
        NSScanner *parameterScanner = [[NSScanner alloc] initWithString:queryString];
        NSString *name = nil;
        NSString *value = nil;
        
        while (![parameterScanner isAtEnd]) {
            name = nil;
            [parameterScanner scanUpToString:@"=" intoString:&name];
            [parameterScanner scanString:@"=" intoString:NULL];
            
            value = nil;
            [parameterScanner scanUpToString:@"&" intoString:&value];
            [parameterScanner scanString:@"&" intoString:NULL];
            
            if (name && value) {
                [parameters setValue:[value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] forKey:[name stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
            }
        }
    }
    
    return parameters;
}


@end
