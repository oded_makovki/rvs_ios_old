//
//  RVSAppHeaderDataObject.h
//  rvs_app2
//
//  Created by Barak Harel on 4/1/14.
//  Copyright (c) 2014 RayV Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RVSAppHeaderDataObject : NSObject
@property (nonatomic) NSString *title;

- (instancetype)initWithTitle:(NSString *)title; // Designated initializer
+ (instancetype)dataWithTitle:(NSString *)title;
@end
