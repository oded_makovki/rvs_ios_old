//
//  TVXClipEmbededViewController.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TVXClipEmbededViewController;
@protocol TVXClipEmbededViewControllerDelegate <NSObject>
-(void)onTVXClipEmbededViewControllerComplete:(TVXClipEmbededViewController *)sender;
@end

@interface TVXClipEmbededViewController : UIViewController
@property (weak, nonatomic) id <TVXClipEmbededViewControllerDelegate> delegate;
@end
