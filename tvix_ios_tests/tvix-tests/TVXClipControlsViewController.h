//
//  TVXClipControlsViewController.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVXClipControlsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *selectionStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectionEndLabel;

@end
