//
//  TVXClipFrameCell.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TVXTimeLineCell : UICollectionViewCell
@property (strong, nonatomic) NSDate *frameDate;
@property (nonatomic) NSTimeInterval retryInterval;

@end
