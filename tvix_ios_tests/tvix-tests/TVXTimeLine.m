//
//  TVXTimeLine.m
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXTimeLine.h"
#import "UIViewContainer.h"
#import "TVXTimeLineCell.h"

#define MAX_SELECTION_TIME 30.0     //30 sec
#define MIN_SELECTION_TIME 5.0      //5 sec

#define TIME_FRAME_SEC  6.0         //every 6 sec
#define SIZE_FRAME_PNT  64.0        //64 pnts

#define TIME_WINDOW_FRAMES self.frame.size.width / SIZE_FRAME_PNT
#define TIME_WINDOW_SEC TIME_WINDOW_FRAMES * TIME_FRAME_SEC

#define TIME_FUTURE_SEC 30.0        //30 sec
#define TIME_PAST_SEC   30.0 * 60.0   //30 min

#define DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY 1.0

#define MAX_FRAMES (MAX_TIME_SEC + TIME_FUTURE_SEC) / TIME_FRAME_SEC  //30min, every 5 sec;
#define PNT_PER_SEC SIZE_FRAME_PNT / TIME_FRAME_SEC
#define SEC_PER_PNT TIME_FRAME_SEC / SIZE_FRAME_PNT

#define TOTAL_TIME_SEC  (TIME_FUTURE_SEC + TIME_WINDOW_SEC + TIME_PAST_SEC)
#define TOTAL_FRAMES    TOTAL_TIME_SEC / TIME_FRAME_SEC

@interface TVXTimeLine () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIViewContainer *rightMask;
@property (weak, nonatomic) IBOutlet UIViewContainer *leftMask;

@property (nonatomic) NSInteger startSelectionLength;

@property (strong, nonatomic) NSDate *notifiedSelectionStartTime;
@property (strong, nonatomic) NSDate *notifiedSelectionEndTime;

@end

@implementation TVXTimeLine

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)awakeFromNib
{
    self.delegateTimeintervalSensitivity = DEFAULT_DELEGATE_TIME_INTERVAL_SENSITIVITY;
    
    [self.collectionView registerClass:[TVXTimeLineCell class] forCellWithReuseIdentifier:@"Cell"];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    ((UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout).scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    [self setClipSizeType:ClipSizeTypeM animate:NO];
    
    UIPanGestureRecognizer *gesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(onPan:)];
    gesture.delegate = self;
    [self.collectionView addGestureRecognizer:gesture];
    
    self.nowTimeOffset = 0.0f;    
    
    NSLog(@"Window time: %f frames %f", TIME_WINDOW_SEC, TIME_WINDOW_FRAMES);
}

- (void)update
{
    NSLog(@"Updating... TOTAL_TIME_SEC: %f TOTAL_FRAMES: %f", TOTAL_TIME_SEC, TOTAL_FRAMES);
    
    _nowTime = [NSDate dateWithTimeInterval:self.nowTimeOffset sinceDate:[NSDate date]];
    _startTime = [_nowTime dateByAddingTimeInterval:-TIME_PAST_SEC];
    _endTime = [_nowTime dateByAddingTimeInterval:(TIME_WINDOW_SEC + TIME_FUTURE_SEC)];
    
    NSLog(@"Now: %@ Start: %@ End: %@",_nowTime, _startTime, _endTime);
    
    [self.collectionView reloadData];
}

#pragma mark - Scroll To Time

-(BOOL)scrollToTime:(NSDate *)time animated:(BOOL)animated
{
    BOOL canScroll = YES;
    
    NSDate * screenEndTime = [NSDate dateWithTimeInterval:-TIME_WINDOW_SEC sinceDate:self.endTime];
    NSLog(@"scroll to: %@ start: %@ end: %@ screen: %@", time, self.startTime, self.endTime, screenEndTime);
    
    if ([screenEndTime timeIntervalSinceDate:time] < 0)
    {
        NSLog(@"scroll outside screen, updated time: %@", screenEndTime);
        canScroll = NO;
        time = screenEndTime;
    }
    
    NSTimeInterval diff = [time timeIntervalSinceDate:self.startTime];
    float offsetX = PNT_PER_SEC * diff;
    
    if (canScroll && self.isCentered)
        offsetX -= self.frame.size.width /2;
    
    [self.collectionView setContentOffset:CGPointMake(offsetX, 0) animated:animated];
    
    return canScroll;
}

-(BOOL)scrollToNow
{
    NSLog(@"scrollToNow...");
    return [self scrollToTime:[NSDate dateWithTimeInterval:self.nowTimeOffset sinceDate:[NSDate date]] animated:YES];
}

-(BOOL)scrollToEnd
{
    NSLog(@"scrollToEnd...");
    return [self scrollToTime:self.endTime animated:YES];
}

#pragma mark - ClipType

-(void)setClipSizeType:(ClipSizeType)clipSizeType
{
    [self setClipSizeType:clipSizeType animate:NO];
}

-(void)setClipSizeType:(ClipSizeType)clipSizeType animate:(BOOL)animate
{
    _clipSizeType = clipSizeType;

    if (_clipSizeType != ClipSizeTypeCustom)
        _selectionLength = _clipSizeType;
    
    CGRect frameLeft = self.leftMask.frame;
    CGRect frameRight = self.rightMask.frame;
    
    frameLeft = CGRectMake(0,0,(self.frame.size.width - PNT_PER_SEC * _selectionLength) / 2,frameLeft.size.height);
    frameRight = CGRectMake(self.frame.size.width - frameLeft.size.width, 0, frameLeft.size.width, frameRight.size.height);
    
    if (animate)
    {
        [UIView animateWithDuration:0.2 animations:^{
            self.leftMask.frame = frameLeft;
            self.rightMask.frame = frameRight;
            [self updateSelectionBounds];
        }];
    }
    else
    {
        self.leftMask.frame = frameLeft;
        self.rightMask.frame = frameRight;
        [self updateSelectionBounds];
    }
    
    if ([self.delegate respondsToSelector:@selector(timelineClipSizeTypeDidChange:)])
    {
        [self.delegate timelineClipSizeTypeDidChange:self];
    }
}


-(void)setCustomSelectionLength:(NSInteger)selectionLength
{
    if (selectionLength < MIN_SELECTION_TIME)
        selectionLength = MIN_SELECTION_TIME;
    
    if (selectionLength > MAX_SELECTION_TIME)
        selectionLength = MAX_SELECTION_TIME;
    
    _selectionLength = selectionLength;
   
    [self setClipSizeType:ClipSizeTypeCustom animate:YES];
}


#pragma mark - Bounds/Selection

-(NSDate *)getCenterTime
{
    float centerPt = self.collectionView.contentOffset.x + self.frame.size.width / 2;
    float centerFrames = centerPt / SIZE_FRAME_PNT;
    float centerTimeSec = centerFrames * TIME_FRAME_SEC;
    
    return [NSDate dateWithTimeInterval:centerTimeSec sinceDate:self.startTime];
}

-(void)updateCurrentBounds
{
    if (!self.startTime || !self.endTime)
        return;
    
    _currentCenterTime = [self getCenterTime];
    _currentStartTime = [NSDate dateWithTimeInterval:-TIME_WINDOW_SEC / 2 sinceDate:self.selectionCenterTime];
    _currentEndTime = [NSDate dateWithTimeInterval:TIME_WINDOW_SEC / 2 sinceDate:self.selectionCenterTime];
}

-(void)updateSelectionBounds
{
    if (!self.startTime || !self.endTime)
        return;
    
    _selectionCenterTime = [self getCenterTime];
    _selectionStartTime = [NSDate dateWithTimeInterval:-self.selectionLength / 2 sinceDate:self.selectionCenterTime];
    _selectionEndTime = [NSDate dateWithTimeInterval:self.selectionLength / 2 sinceDate:self.selectionCenterTime];

    //if bound change bigger then sensitivity
    if (!self.notifiedSelectionStartTime ||
        fabsf([self.notifiedSelectionStartTime timeIntervalSinceDate:self.selectionStartTime]) > self.delegateTimeintervalSensitivity ||
        !self.notifiedSelectionEndTime ||
        fabsf([self.notifiedSelectionEndTime timeIntervalSinceDate:self.selectionEndTime]) > self.delegateTimeintervalSensitivity)
    {
        self.notifiedSelectionStartTime = self.selectionStartTime;
        self.notifiedSelectionEndTime = self.selectionEndTime;
        
        if ([self.delegate respondsToSelector:@selector(timelineSelectionDidChange:)])
        {
            [self.delegate timelineSelectionDidChange:self];
        }
    }
}

#pragma mark - Gestures

-(void)onPan:(UIPanGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        self.startSelectionLength = self.selectionLength;
    }
    else if (gesture.state == UIGestureRecognizerStateChanged)
    {
        float pntOffset = [gesture translationInView:self.collectionView].y;
        NSTimeInterval timeOffset = pntOffset / 20;
        [self setCustomSelectionLength:self.startSelectionLength + timeOffset];
        
//        NSLog(@"pan: %f timeOffset: %f selection: %f" , pntOffset, timeOffset, self.startSelectionLength + timeOffset);
    }
    else if (gesture.state == UIGestureRecognizerStateEnded)
    {
        
    }
}

- (BOOL)gestureRecognizerShouldBegin:(UIPanGestureRecognizer *)panGestureRecognizer
{
    CGPoint translation = [panGestureRecognizer translationInView:self.collectionView];
    return fabs(translation.y) > fabs(translation.x);
}

#pragma mark - UICollectionView Delegate

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return TOTAL_FRAMES;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(SIZE_FRAME_PNT, SIZE_FRAME_PNT);
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TVXTimeLineCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSDate *date = [self.startTime dateByAddingTimeInterval:indexPath.row * TIME_FRAME_SEC];
    cell.frameDate = date;
    cell.retryInterval = TIME_FRAME_SEC / 2.0f;
    
    if (indexPath.row == TOTAL_FRAMES-1)
    {
        cell.backgroundColor = [UIColor colorWithWhite:1 alpha:0.2];
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor];
    }
    
    return cell;
}

#pragma mark - UIScrollView Delegate

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"scrollViewWillBeginDragging");
    if (!_isDragging)
    {
        _isDragging = YES;
        if ([self.delegate respondsToSelector:@selector(timelineDidBeginDragging)])
            [self.delegate timelineDidBeginDragging];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self updateSelectionBounds];
    [self updateCurrentBounds];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self onScrollEnded:scrollView];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self onScrollEnded:scrollView];
    
    if (_isDragging)
    {
        _isDragging = NO;
        if ([self.delegate respondsToSelector:@selector(timelineDidEndDragging)])
            [self.delegate timelineDidEndDragging];
    }
}

-(void)onScrollEnded:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x >= scrollView.contentSize.width - scrollView.frame.size.width)
    {
        NSDate *oldCenter = _currentCenterTime;
        [self update];
        [self scrollToTime:oldCenter animated:NO];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(timeline:didSelectFrameWithDate:)])
    {
        NSDate *date = [self.startTime dateByAddingTimeInterval:indexPath.row * TIME_FRAME_SEC];
        
        [self.delegate timeline:self didSelectFrameWithDate:date];
    }
}

@end
