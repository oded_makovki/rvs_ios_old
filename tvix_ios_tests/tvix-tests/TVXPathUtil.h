//
//  TVXPathUtil.h
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TVX_PATH_UTIL_DEFAULT_FOLDER @"http://173.199.212.18/rayv1demo"

@interface TVXPathUtil : NSObject
+(instancetype) sharedInstance;
-(NSString *)framePathFromFolder:(NSString *)framesFolder timeInterval:(NSTimeInterval)timeInterval;

@end
