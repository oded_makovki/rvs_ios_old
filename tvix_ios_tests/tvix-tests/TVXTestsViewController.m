//
//  TVXTestsViewController.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXTestsViewController.h"

@interface TVXTestsViewController ()
@property (strong, nonatomic) NSDictionary *settings;
@property (strong, nonatomic) NSArray *tests;
@end

@implementation TVXTestsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    NSString * settingsPath = [[NSBundle mainBundle] pathForResource:@"settings" ofType:@"plist"];
    self.settings = [NSDictionary dictionaryWithContentsOfFile:settingsPath];
    self.tests = [self.settings objectForKey:@"tests"];
    
    NSLog(@"tests: %@", self.tests);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.tests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"testCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *data = self.tests[indexPath.row];
    
    cell.detailTextLabel.text = [data objectForKey:@"detail"];
    cell.textLabel.text = [data objectForKey:@"title"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data = self.tests[indexPath.row];
    NSString *segue = [data objectForKey:@"segue"];
    if (segue)
    {
        [self performSegueWithIdentifier:segue sender:self];
    }
}

#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
