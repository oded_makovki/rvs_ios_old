//
//  TVXClipButtonViewController.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXClipButtonViewController.h"

@interface TVXClipButtonViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation TVXClipButtonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    self.button.enabled = YES;
    [self.spinner stopAnimating];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}
- (IBAction)clipPressed:(id)sender {
    
    self.button.enabled = NO;
    [self.spinner startAnimating];
    
    [self performSelector:@selector(complete) withObject:nil afterDelay:0.2];
}

-(void)complete
{
    [self.spinner stopAnimating];
    [self.delegate onTVXClipEmbededViewControllerComplete:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
