//
//  TVXTimeLineFotter.m
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXTimeLineFooter.h"
#import "UIView+NibLoading.h"

@implementation TVXTimeLineFooter

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        [self.spinner stopAnimating];
    }
    return self;
}

@end
