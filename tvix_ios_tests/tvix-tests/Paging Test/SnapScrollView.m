//
//  SnapScrollView.m
//  Version 1.0
//
//  Created by Barak Harel on 10/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "SnapScrollView.h"

typedef enum {
    ScrollDirectionNone,
    ScrollDirectionPrevious,
    ScrollDirectionNext
} ScrollDirection;


#pragma mark SnapScrollViewInternalDelegate

@class SnapScrollViewInternalDelegate;
@protocol SnapScrollViewInternalDelegateDataSource <NSObject>
-(ScrollSnapDirection)scrollSnapDirectionForDelegate:(SnapScrollViewInternalDelegate *)delegate;
-(CGFloat)startOffsetForDelegate:(SnapScrollViewInternalDelegate *)delegate;
-(NSArray *)snapPointsForDelegate:(SnapScrollViewInternalDelegate *)delegate;
@end

#pragma mark VSnapScrollViewPrivateDelegate

@interface SnapScrollViewInternalDelegate : NSObject <UIScrollViewDelegate>
@property (weak, nonatomic) id <SnapScrollViewInternalDelegateDataSource> dataSource;
@property (weak, nonatomic) id <UIScrollViewDelegate> userDelegate;

@property (nonatomic) NSInteger lastContentOffset;
@property (nonatomic) ScrollDirection scrollDirection;
@end


@implementation SnapScrollViewInternalDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    ScrollSnapDirection snapDirection = [self.dataSource scrollSnapDirectionForDelegate:self];
    
    CGFloat refValue = scrollView.contentOffset.y;
    if (snapDirection == ScrollSnapDirectionHorizontally)
        refValue = scrollView.contentOffset.x;
    
    if (self.lastContentOffset == refValue)
    {
        self.scrollDirection = ScrollDirectionNone;
    }
    else if (self.lastContentOffset < refValue)
    {
        self.scrollDirection = ScrollDirectionNext;
    }
    else
    {
        self.scrollDirection = ScrollDirectionPrevious;
    }
    
    self.lastContentOffset = refValue;
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    BOOL getNext = NO;
    
    if (self.scrollDirection == ScrollDirectionNext)
        getNext = YES;
    
    CGPoint point = [self getNearestPageOffsetTo:*targetContentOffset getNext:getNext];

    NSLog(@"velocity: %@ target: %@ snap: %@", [NSValue valueWithCGPoint:velocity], [NSValue valueWithCGPoint:*targetContentOffset], [NSValue valueWithCGPoint:point]);
    *targetContentOffset = point;
    
    if ([_userDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)])
        [_userDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
}

-(CGPoint)getNearestPageOffsetTo:(CGPoint)point getNext:(BOOL)getNext
{
    NSArray *snapPoints = [self.dataSource snapPointsForDelegate:self];
    if ([snapPoints count] <= 0)
        return point;
    
    ScrollSnapDirection scrollSnapDirection = [self.dataSource scrollSnapDirectionForDelegate:self];
    if (scrollSnapDirection == ScrollSnapDirectionNone)
        return point;
    
    //don't modify original
    CGPoint testPoint = point;
    
    //add start offset
    CGFloat startOffset = [self.dataSource startOffsetForDelegate:self];
    if (scrollSnapDirection == ScrollSnapDirectionHorizontally)
    {
        testPoint.x += startOffset;
    }
    else
    {
        testPoint.y +=startOffset;
    }
    
    //find snap point index
    NSInteger snapToIndex = 0;
    for (int i=0; i < [snapPoints count]; i++)
    {
        CGPoint snapPoint = [snapPoints[i] CGPointValue];
        if ((scrollSnapDirection == ScrollSnapDirectionHorizontally && testPoint.x >= snapPoint.x) ||
            (scrollSnapDirection == ScrollSnapDirectionVertically && testPoint.y >= snapPoint.y))
        {
            snapToIndex = i;
        }
        else
        {
            //snap points are sorted by value
            break;
        }
    }

    //get snap to point
    CGPoint snapToPoint = [snapPoints[snapToIndex] CGPointValue];
    
    //if getNext and there's a next point, get it.
    if (getNext && snapToIndex+1 < [snapPoints count])
        snapToPoint = [snapPoints[snapToIndex+1] CGPointValue];
    
    //remove start offset
    if (scrollSnapDirection == ScrollSnapDirectionHorizontally)
    {
        snapToPoint.x -= startOffset;
    }
    else
    {
        snapToPoint.y -= startOffset;
    }
    
    return snapToPoint;
}

#pragma mark Delegate Forward Invocation

- (BOOL)respondsToSelector:(SEL)selector
{
    return [_userDelegate respondsToSelector:selector] || [super respondsToSelector:selector];
}

- (void)forwardInvocation:(NSInvocation *)invocation {
    // This should only ever be called from `UIScrollView`, after it has verified
    // that `_userDelegate` responds to the selector by sending me
    // `respondsToSelector:`.  So I don't need to check again here.
    [invocation invokeWithTarget:_userDelegate];
}

@end

#pragma mark VSnapScrollView

@interface SnapScrollView() <SnapScrollViewInternalDelegateDataSource>
@property (strong, nonatomic) SnapScrollViewInternalDelegate *internalDelegate;
@end

@implementation SnapScrollView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initInternals];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initInternals];
        
    }
    return self;
}

- (void)initInternals
{
    _internalDelegate = [[SnapScrollViewInternalDelegate alloc] init];
    _internalDelegate.dataSource = self;
    [super setDelegate:_internalDelegate];
    self.pagingEnabled = NO;
    
    self.scrollSnapDirection = ScrollSnapDirectionVertically;
}

#pragma mark Functions

-(void)autoSnapPoints
{
    if (self.scrollSnapDirection == ScrollSnapDirectionNone)
    {
        self.snapPoints = nil;
        return;
    }
    
    BOOL wasShowsHorizontalScrollIndicator = self.showsHorizontalScrollIndicator;
    BOOL wasShowsVerticalScrollIndicator = self.showsVerticalScrollIndicator;
    
    self.showsVerticalScrollIndicator = NO;
    self.showsHorizontalScrollIndicator = NO;
    
    NSMutableArray *snapPoints = [NSMutableArray arrayWithCapacity:[self.subviews count]];
    
    for (UIView *subview in self.subviews)
    {
        [snapPoints addObject:[NSValue valueWithCGPoint:subview.frame.origin]];
    }
    
    self.showsVerticalScrollIndicator = wasShowsVerticalScrollIndicator;
    self.showsHorizontalScrollIndicator = wasShowsHorizontalScrollIndicator;
    
    self.snapPoints = snapPoints;
}

-(void)autoContentSize
{
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.subviews)
    {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.contentSize = contentRect.size;
}

#pragma mark - InternalDelegateDataSource

-(CGFloat)startOffsetForDelegate:(SnapScrollViewInternalDelegate *)delegate
{
    return self.startOffset;
}

-(ScrollSnapDirection)scrollSnapDirectionForDelegate:(SnapScrollViewInternalDelegate *)delegate
{
    return self.scrollSnapDirection;
}

-(NSArray *)snapPointsForDelegate:(SnapScrollViewInternalDelegate *)delegate
{
    return [self.snapPoints copy];
}

#pragma mark - Accessors

-(void)setSnapPoints:(NSArray *)snapPoints
{
    NSMutableArray *points = [snapPoints mutableCopy];
    [points sortUsingComparator:^NSComparisonResult(NSValue *point1, NSValue *point2) {
         if (self.scrollSnapDirection == ScrollSnapDirectionHorizontally)
         {
             return [@([point1 CGPointValue].x) compare:@([point2 CGPointValue].x)];
         }
         else
         {
             return [@([point1 CGPointValue].y) compare:@([point2 CGPointValue].y)];
         }
     }];
    
    _snapPoints = points;
    NSLog(@"Snap point: %@", _snapPoints);
}

-(void)setDelegate:(id<UIScrollViewDelegate>)delegate
{
    _internalDelegate.userDelegate = delegate;
    super.delegate = nil;
    super.delegate = _internalDelegate;
}

-(id<UIScrollViewDelegate>)delegate
{
    return _internalDelegate.userDelegate;
}


@end
