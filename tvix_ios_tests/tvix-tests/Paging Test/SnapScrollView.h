//
//  SnapScrollView.h
//  Version 1.0
//
//  Created by Barak Harel on 10/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    ScrollSnapDirectionNone,
    ScrollSnapDirectionVertically,
    ScrollSnapDirectionHorizontally
} ScrollSnapDirection;

@interface SnapScrollView : UIScrollView
@property (strong, nonatomic) NSArray *snapPoints;
@property (nonatomic) CGFloat startOffset;
@property (nonatomic) ScrollSnapDirection scrollSnapDirection;

-(void)autoSnapPoints;
-(void)autoContentSize;
-(void)setSnapPoints:(NSArray *)snapPoints;
@end
