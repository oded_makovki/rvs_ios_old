//
//  TVXPagingViewController.m
//  tvix-tests
//
//  Created by Barak Harel on 10/28/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXPagingViewController.h"
#import "SnapScrollView.h"

@interface TVXPagingViewController () <UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet SnapScrollView *scrollView;
@end

@implementation TVXPagingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.scrollView.delegate = self;    
    [super viewDidLoad];
}

-(void)viewWillLayoutSubviews
{
    self.scrollView.startOffset = [self.topLayoutGuide length];
    self.scrollView.scrollSnapDirection = ScrollSnapDirectionVertically;
    self.scrollView.decelerationRate = UIScrollViewDecelerationRateFast;
    [self.scrollView autoContentSize];
    [self.scrollView autoSnapPoints];
//    NSArray *customSnapPoints = [NSArray arrayWithObjects:
//                                 [NSValue valueWithCGPoint:CGPointMake(10, 100)],
//                                 [NSValue valueWithCGPoint:CGPointMake(100, 240)],
//                                 [NSValue valueWithCGPoint:CGPointMake(-100, 350)],
//                                 nil];
//    
//    self.scrollView.snapPoints = customSnapPoints;
}


@end
