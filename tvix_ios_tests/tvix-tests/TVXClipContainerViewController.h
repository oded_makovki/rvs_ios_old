//
//  ContainerViewController.h
//  EmbeddedSwapping
//
//  Created by Michael Luton on 11/13/12.
//  Copyright (c) 2012 Sandmoose Software. All rights reserved.
//

@interface TVXClipContainerViewController : UIViewController

- (void)swapViewControllers;

@end
