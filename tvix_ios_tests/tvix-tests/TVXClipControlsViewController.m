//
//  TVXClipControlsViewController.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXClipControlsViewController.h"
#import "TVXTimeLine.h"
#import "TVXPathUtil.h"

#import "UIImageView+AFNetworking.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"

@interface TVXClipControlsViewController () <TVXTimeLineDelegate>
@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segClipSize;
@property (weak, nonatomic) IBOutlet TVXTimeLine *timeline;
@end

@implementation TVXClipControlsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    self.previewButton.titleLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:60];
    [self.previewButton setTitle:[NSString fontAwesomeIconStringForEnum:FAIconPlay] forState:UIControlStateNormal];
    
    
    self.timeline.delegate = self;
    self.timeline.delegateTimeintervalSensitivity = 1.0f;
    [self.timeline update];
}


- (IBAction)clipSizeChanged:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.timeline setClipSizeType:ClipSizeTypeS animate:YES];
            break;
            
        case 1:
            [self.timeline setClipSizeType:ClipSizeTypeM animate:YES];
            break;
            
        case 2:
            [self.timeline setClipSizeType:ClipSizeTypeL animate:YES];
            break;
            
        case 3:
            [self.timeline setClipSizeType:ClipSizeTypeCustom animate:YES];
            break;
            
        default:
            break;
    }
}

-(void)timelineSelectionDidChange:(TVXTimeLine *)timeline
{
//    NSLog(@"start: %@ center: %@ end: %@ [length: %f]",timeline.startTime, timeline.selectionCenterTime, timeline.selectionEndTime, timeline.selectionLength);
    self.selectionStartLabel.text = [NSString stringWithFormat:@"Start: %@", timeline.selectionStartTime];
    self.selectionEndLabel.text = [NSString stringWithFormat:@"End: %@", timeline.selectionEndTime];
    
    [self updatePreviewImage];
}

-(void)timelineClipSizeTypeDidChange:(TVXTimeLine *)timeline
{
    switch (timeline.clipSizeType) {
        case ClipSizeTypeS:
            [self.segClipSize setSelectedSegmentIndex:0];
            break;
            
        case ClipSizeTypeM:
            [self.segClipSize setSelectedSegmentIndex:1];
            break;
            
        case ClipSizeTypeL:
            [self.segClipSize setSelectedSegmentIndex:2];
            break;
            
        case ClipSizeTypeCustom:
            [self.segClipSize setSelectedSegmentIndex:3];
            break;
            
        default:
            break;
    }
}

-(void)updatePreviewImage
{
    NSLog(@"updatePreviewImage");
    NSTimeInterval timeInterval = [self.timeline.selectionCenterTime timeIntervalSince1970];
    NSURL *url = [NSURL URLWithString:[[TVXPathUtil sharedInstance] framePathFromFolder:TVX_PATH_UTIL_DEFAULT_FOLDER timeInterval:timeInterval]];
    
    [self.previewImageView setImageWithURL:url];
}


@end
