//
//  TVXPathUtil.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXPathUtil.h"

@interface TVXPathUtil()
@property (nonatomic) NSInteger frameSampleInterval;
@property (nonatomic) NSInteger folderRotation;
@end

@implementation TVXPathUtil


+(instancetype)sharedInstance
{
    static dispatch_once_t pred;
    static TVXPathUtil *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[TVXPathUtil alloc] init];
        shared.frameSampleInterval = 5;
        shared.folderRotation = 4;
    });
    return shared;
}

-(NSString *)framePathFromFolder:(NSString *)framesFolder timeInterval:(NSTimeInterval)timeInterval
{
    //kill non-modolo frac.
    NSInteger fileFrac = timeInterval / self.frameSampleInterval;
    timeInterval = fileFrac * self.frameSampleInterval;
    
    NSString *timeFolder = [NSString stringWithFormat:@"%.0f", timeInterval];
    timeFolder = [timeFolder substringToIndex:timeFolder.length - self.folderRotation];
    for (int i=0; i < self.folderRotation; i++) {
        timeFolder = [NSString stringWithFormat:@"%@0", timeFolder];
    }

    return [NSString stringWithFormat:@"%@/%@/frame_%.0f.jpg",framesFolder,timeFolder,timeInterval];
}


@end
