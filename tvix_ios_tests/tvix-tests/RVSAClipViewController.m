//
//  RVSAClipViewController.m
//  tvix-tests
//
//  Created by Barak Harel on 10/23/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "RVSAClipViewController.h"
#import "TVXTimeLine.h"
#import "TVXPathUtil.h"

#import "UIImageView+AFNetworking.h"
#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"

#define RVSA_CLIP_NUMBER_OF_PANELS 2

@interface RVSAClipViewController () <TVXTimeLineDelegate, UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *parentScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *childScrollView;

@property (weak, nonatomic) IBOutlet UIView *videoContainer;
@property (weak, nonatomic) IBOutlet UIView *timelineContainer;
@property (weak, nonatomic) IBOutlet UIView *postContainer;
@property (weak, nonatomic) IBOutlet UIToolbar *editToolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonDoneEdit;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *buttonCancelEdit;

@property (weak, nonatomic) IBOutlet UIImageView *previewImageView;
@property (weak, nonatomic) IBOutlet UIButton *previewButton;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segClipSize;
@property (weak, nonatomic) IBOutlet TVXTimeLine *timeline;
@property (weak, nonatomic) IBOutlet UILabel *selectionStartLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectionEndLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectionCenterLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *snapButton;

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation RVSAClipViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.parentScrollView setContentOffset:CGPointMake(0, 0)];
    
    self.previewButton.titleLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:60];
    [self.previewButton setTitle:[NSString fontAwesomeIconStringForEnum:FAIconPlay] forState:UIControlStateNormal];
    
    self.timeline.delegate = self;
    self.timeline.delegateTimeintervalSensitivity = 1.0f;
    self.timeline.isCentered = YES;
    //set offset
    self.timeline.nowTimeOffset = -5;
    
    [self.timeline update];
    [self.timeline scrollToNow];
    
    NSMutableArray *items = [self.editToolbar.items mutableCopy];
    [items removeObject:self.buttonDoneEdit];
    [self.editToolbar setItems:items];
    
    self.textView.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

-(void)viewDidLayoutSubviews
{
    self.childScrollView.contentSize = CGSizeMake( self.childScrollView.frame.size.width * RVSA_CLIP_NUMBER_OF_PANELS, self.childScrollView.frame.size.height);
}


- (IBAction)clipSizeChanged:(UISegmentedControl *)sender {
    switch (sender.selectedSegmentIndex) {
        case 0:
            [self.timeline setClipSizeType:ClipSizeTypeS animate:YES];
            break;
            
        case 1:
            [self.timeline setClipSizeType:ClipSizeTypeM animate:YES];
            break;
            
        case 2:
            [self.timeline setClipSizeType:ClipSizeTypeL animate:YES];
            break;
            
        case 3:
            [self.timeline setClipSizeType:ClipSizeTypeCustom animate:YES];
            break;
            
        default:
            break;
    }
}

#pragma mark - Timeline Delegate

-(void)timelineDidBeginDragging
{
    self.childScrollView.scrollEnabled = NO;
}

-(void)timelineDidEndDragging
{
    self.childScrollView.scrollEnabled = YES;
}

-(void)timelineSelectionDidChange:(TVXTimeLine *)timeline
{
    //    NSLog(@"start: %@ center: %@ end: %@ [length: %f]",timeline.startTime, timeline.selectionCenterTime, timeline.selectionEndTime, timeline.selectionLength);
    self.selectionStartLabel.text = [NSString stringWithFormat:@"Start: %@", timeline.selectionStartTime];
    self.selectionEndLabel.text = [NSString stringWithFormat:@"End: %@", timeline.selectionEndTime];
    self.selectionCenterLabel.text = [NSString stringWithFormat:@"Center: %@", timeline.selectionCenterTime];
    
    [self updatePreviewImageFromDate:self.timeline.selectionCenterTime];
}

-(void)timelineClipSizeTypeDidChange:(TVXTimeLine *)timeline
{
    switch (timeline.clipSizeType) {
        case ClipSizeTypeS:
            [self.segClipSize setSelectedSegmentIndex:0];
            break;
            
        case ClipSizeTypeM:
            [self.segClipSize setSelectedSegmentIndex:1];
            break;
            
        case ClipSizeTypeL:
            [self.segClipSize setSelectedSegmentIndex:2];
            break;
            
        case ClipSizeTypeCustom:
            [self.segClipSize setSelectedSegmentIndex:3];
            break;
            
        default:
            break;
    }
}

-(void)timeline:(TVXTimeLine *)timeline didSelectFrameWithDate:(NSDate *)date
{
    [self updatePreviewImageFromDate:date];
}

-(void)updatePreviewImageFromDate:(NSDate *)date
{
//    NSLog(@"updatePreviewImage");
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSURL *url = [NSURL URLWithString:[[TVXPathUtil sharedInstance] framePathFromFolder:TVX_PATH_UTIL_DEFAULT_FOLDER timeInterval:timeInterval]];
    
    [self.previewImageView setImageWithURL:url];
}


#pragma mark - TextView delegate

- (void)keyboardWasShown:(NSNotification*)notification
{
    
}

- (void)keyboardWillBeHidden:(NSNotification*)notification {
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    NSMutableArray *items = [self.editToolbar.items mutableCopy];
    [items replaceObjectAtIndex:0 withObject:self.buttonDoneEdit];
    [self.editToolbar setItems:items animated:YES];
    
    [self.parentScrollView setContentOffset:CGPointMake(0, self.childScrollView.frame.origin.y) animated:YES];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    NSMutableArray *items = [self.editToolbar.items mutableCopy];
    [items replaceObjectAtIndex:0 withObject:self.buttonCancelEdit];
    [self.editToolbar setItems:items animated:YES];
    
    [self.parentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.childScrollView.scrollEnabled = YES;
}


#pragma mark - Clip steps

- (IBAction)doClip:(id)sender
{
    [self.childScrollView setContentOffset:CGPointMake(self.postContainer.frame.origin.x, 0) animated:YES];
}

- (IBAction)doneEdit:(UIBarButtonItem *)sender
{
    [self.textView resignFirstResponder];
}

- (IBAction)cancelClip:(UIBarButtonItem *)sender
{
    if ([self.textView isFirstResponder])
    {
        [self.textView resignFirstResponder];
        [self performSelector:@selector(scrollToClip) withObject:nil afterDelay:0.5];
    }
    else
    {
        [self scrollToClip];
    }
}

-(void)scrollToClip
{
    [self.childScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}
@end
