//
//  TVXTimeLine.h
//  tvix-tests
//
//  Created by Barak Harel on 10/17/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+NibLoading.h"

typedef enum
{
    ClipSizeTypeCustom = -1,
    ClipSizeTypeS = 10,
    ClipSizeTypeM = 20,
    ClipSizeTypeL = 30
}
ClipSizeType;

typedef enum
{
    AutoScrollTypeNone,
    AutoScrollTypeSmart,
    AutoScrollTypeAlways
}
AutoScrollType;

@class TVXTimeLine;

@protocol TVXTimeLineDelegate <NSObject>
@optional
-(void)timelineSelectionDidChange:(TVXTimeLine *)timeline;
-(void)timelineClipSizeTypeDidChange:(TVXTimeLine *)timeline;
-(void)timeline:(TVXTimeLine *)timeline didSelectFrameWithDate:(NSDate *)date;
-(void)timelineDidBeginDragging;
-(void)timelineDidEndDragging;
@end

@interface TVXTimeLine : NibLoadedView
@property (nonatomic) ClipSizeType clipSizeType;

@property (strong, nonatomic, readonly) NSDate *nowTime;
@property (strong, nonatomic, readonly) NSDate *startTime;
@property (strong, nonatomic, readonly) NSDate *endTime;

@property (strong, nonatomic, readonly) NSDate *currentStartTime;
@property (strong, nonatomic, readonly) NSDate *currentCenterTime;
@property (strong, nonatomic, readonly) NSDate *currentEndTime;

@property (strong, nonatomic, readonly) NSDate *selectionStartTime;
@property (strong, nonatomic, readonly) NSDate *selectionCenterTime;
@property (strong, nonatomic, readonly) NSDate *selectionEndTime;
@property (nonatomic, readonly) NSInteger selectionLength;
@property (nonatomic) NSInteger delegateTimeintervalSensitivity;
@property (nonatomic) NSTimeInterval nowTimeOffset;
@property (nonatomic) BOOL isCentered;
@property (nonatomic, readwrite) BOOL isDragging;

@property (weak, nonatomic) id <TVXTimeLineDelegate> delegate;

-(void)setClipSizeType:(ClipSizeType)clipSizeType animate:(BOOL)animate;
-(void)update;
-(BOOL)scrollToNow;
-(BOOL)scrollToEnd;
@end
