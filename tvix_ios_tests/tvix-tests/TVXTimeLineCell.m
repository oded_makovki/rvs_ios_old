//
//  TVXClipFrameCell.m
//  tvix-tests
//
//  Created by Barak Harel on 10/16/13.
//  Copyright (c) 2013 RayV. All rights reserved.
//

#import "TVXTimeLineCell.h"
#import "TVXPathUtil.h"
#import "UIImageView+AFNetworking.h"
#import "UIView+NibLoading.h"

#define DEF_BACK_IMAGE @"frame_back_lines"

@interface TVXTimeLineCell()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation TVXTimeLineCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    [self loadContentsFromNib];
    if (self) {
        self.clipsToBounds = YES;
        self.retryInterval = 2;
            }
    return self;
}

-(void)setFrameDate:(NSDate *)frameDate
{
    _frameDate = frameDate;
    self.imageView.image = [UIImage imageNamed:DEF_BACK_IMAGE];
    [self loadImageForDate:frameDate];
}

-(void)loadImageForDate:(NSDate *)date
{
    if (date != self.frameDate)
    {
//        NSLog(@"invalid, skip");
        return;
    }
    
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSURL *url = [NSURL URLWithString:[[TVXPathUtil sharedInstance] framePathFromFolder:TVX_PATH_UTIL_DEFAULT_FOLDER timeInterval:timeInterval]];
    
//    NSLog(@"load: %@", url);
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.imageView setImageWithURLRequest:[NSURLRequest requestWithURL:url]
                          placeholderImage:[UIImage imageNamed:DEF_BACK_IMAGE]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                       //cool
//                                       NSLog(@"ok");
                                       self.imageView.image = image;
                                   }
                                   failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                       NSLog(@"Failed, retrying in %.2f sec... %@", self.retryInterval, url);
                                       if (self.retryInterval > 0)
                                           [self performSelector:@selector(loadImageForDate:) withObject:date afterDelay:self.retryInterval];
                                   }];
}



@end
